# Paper

The following assumes a unix system with the `latex` command in the
path. The necessary packages can be found in
`installed-texlive-packages.txt`. It has been tested with texlive on
OS X 10.10.

To compile the paper and watch for changes:

```
cd $WORK_DIR
bash bin/watch.sh final
```

Where `$WORK_DIR` is the directory where this README appears. The
output will be in `tex/final.pdf`.
