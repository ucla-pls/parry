* TODO P.9, col.2: why do you refer to TLRW as ByteEager?
  Is there a precedent in previous papers?  Ask Mohsen for advice.
* DONE P.12, col.1, the first paragraph about ARMv7:  I think this paragraph
  is confusing.  I wish I could make suggestions for how to improve it,
  yet I actually don't know what the paragraph is trying to say :-).
  I suggest that you make the text much more explicit (and thereby
  probably twice as long) and then I will read again.
* DONE P.12, col.1, Section 7.5 Approximate fence insertion measurements:
  move to evaluation
* DONE P.10, col.2, "we make the macro placements abstract":  Please rephrase.
  Something like: "we remove those macros".
* DONE P.10, col.2: "according to the determined orders" ->
               "according to the specified orders".
  (I do realize that the orders have to be "determined" before
  they can be "specified", yet for that sentence what matters
  is that they are "specified", not where they came from.)
* DONE P.10, col.2: "Figure 7.3.1" -> "Figure 15".
* DONE P.11, col.1: "The performance benchmark results" ->
               "The performance results".
* DONE P.11, col.2: "The table in Figure 7.4.1" -> "Figure 16".
* TODO Sections 7.3 and 7.4:  I think we need to impose a further structure
  or style on those sections to enable a reader to see more easily
  what is going on and to compare across those two sections.
  I am thinking about this point.
* DONE Section 7.6:  Insert as many observations and insights as you
  possibly can.
* DONE P.11, col.1, Section 7.3.3:  I see the point; we can eliminate
  this section.  In addition to the text you have written about
  order elimination already in Section 7, I think we can have
  a paragraph in 7.6 Evaluation where you summarize the effect of
  order elimination (across all the experiments).  The goal of
  this additional paragraph will be to give a final answer to
  the question: "is order elimination worth the trouble?"
  perhaps merge that into Section 7.6 Evaluation.
* DONE P.12, col.1, Section 7.4.3:  same remark about order elimination as above.
* DONE P.10, col.1: "of the changes made by Parry"; please rephrase.
  The current text gives the impression that Parry makes changes
  to the baseline.  I think you might change the entire sentence
  to something simple like: "We also compare the performance."
* DONE P.10, col.1, the three TODOs:  Looks like you are almost done
  with those TODOs; add a citation and a link.
* DONE P.10, col.1, last sentence before "7.2":  add a sentence that
  explains _why_ this is important.  (I do realize that some
  readers can figure this out but better to be explicit.)
* DONE P.10, col.2:  "we work backward".  I suggest that you replace
  this with" "we first lift the macro placements to a higher level
  of abstraction ...".
* DONE P.10, col.2, Section 7.3.1:  The opening sentence is good
  yet we should say a little more (either here or elsewhere or both),
  namely that the fences inserted in the baseline do correspond closely
  to the orders listed in the table.  Such a statement seems entirely
  consistent with my previous comment about lifting macro placements to
  a higher level of abstraction.  However, one could imagine that
  the fences in the baseline and the orders don't match up.
  To me this is an interesting point; sometimes the world looks
  different at a different level of abstraction, but perhaps in our case
  the world looks similar both at the level of orders and at the level
  of fences.  Or maybe not?!  Perhaps the author of the macros
  was implicitly thinking about orders, hence the placement of macros
  that sometimes are used with fences and sometimes left empty,
  depending on the architecture.  We might make this into a bigger point.
* DONE P.11, col.1, the bar diagram:  the vertical text to left talks
  about "hand written", while the legend on the right talks
  about "Baseline".  I suggest that both talk about "Baseline".
* DONE P.11, col.2: "and implicit lock prefix" -> "an implicit lock prefix".

* DONE In the intro, sentence starting with "For algorithms based on ...", we can remove (or restate) "because there are many required fences." and join this and the next paragraph. The word "required" is not clear by whom. The algorithm or sequential consistency although we know it is the latter.
  - changed to "because it requires many fences", sequential
    consistency is the subject.

* DONE In the intro, at "... we show that Perry inserts one fewer fence ...". It is referring to the subsection 5.7, TL2 x86. There, it is clear that the compiler (control flow) analysis and the introduced temporary variables have led to the optimization. Maybe we highlight the significance of compiler analysis for fence insertion in the intro. This optimization is something that the expert algorithm designers cannot do. Until now we were saying that we are trying to be as good as them and now this is an extra achievement of our approach.
  - "For example, for the TL2 transactional memory algorithm
    \cite{dice2006transactional} on x86, we show that Parry inserts
    one fewer fence than the experts who implemented the algorithm."

* DONE In the intro, the modularity subsection, I do not understand it. Can you explain more.
  - Most algorithms assume that their data structures (used for book
    keeping etc) aren't being messed with by some other third party
    during execution. Our tool takes those assumptions as input as
    part of the orders (the orders are based on those assumptions) and
    this gives us the ability to local analysis on procedures without
    having to consider all the possibilities that a whole program
    analysis has to. We require more from the user but are able to do
    more and faster as a consequence. Note that Jens wrote this so I'm
    going on our own conversations about the topic.

* DONE In subsection 2.3, at "... from the store to lock and the store to tmp ..." change "from" to "between" or "and" to "to".
  - used "between"

* DONE In subsection 2.4, does the single paragraph need to be a subsection? The title of this subsection does not seem to be at the same semantics level as the title of the subsection 2.3 either. Maybe we remove the subsection title.
  - We tried to finish this tutorial section off with a sort of "call-to-action" where we say that these problems that we've highlighted with the manual insertion of fences can be significantly reduced by using declarative orders.

* DONE In subsection 3.2, at "... like IA64 Figure 3 ...", add "in" between "IA64" and "Figure".
  - added "in"

* DONE In section 4, at "... than one matching instruction per line ...", what is "matching instruction"?
  - "We safely account for the possibility of more than one instruction
    per line matching the event types of an order by including all
    matching instructions during the analysis."

* DONE In section 4, the last sentence of subsection "Architecture Rules". As explained in subsection 5.8, we do handle cmpxchg but this sentence suggests that do not. Also it seems that it can be merged to the previous paragraph. Further, why don't we include cmpxchg in Figure 3, now that we take it into account.
  - "Not included in Figure \ref{fig:arch-definitions} are
    instructions that exist in the LLVM IR, like \ttf{cmpxchg}, which
    result in hardware instructions with fence-like semantics. We do
    account for LLVM's \ttf{cmpxchg} as detailed in Section
    \ref{sec:tl2-measurements}."

* DONE In subsection 5.1, at "... four classic algorithms, ...", change "," to ".".
  - changed

* DONE In subsection 5.1, at "... an order according the description of the bug ...", add "to" between "according" and "the".
  - added "to"

* DONE In subsection 5.4, at "... an opportunity to compare those fence with ...", change "fence" to "fences".
  - changed to "fences"

* DONE In subsection 5.7, at "... in the appendix", you mean "in the accompanying code"?
  - "The orders are defined using line numbers which can be referenced
    in the code which accompanies our project \cite{repo}."

* TODO In Figures 11 and 14, the cells saying of TL2, TL2 Eager and ... should say execution order and TL2 and TL2 Eager are labels or titles for each table. As I mentioned before, it is more readable if we separate these tables from each other.
  - Still deciding what to do here.

* DONE In subsection 5.7, at the sentence "Note that the last order in TxCommit ...", revise to make it clear that you are talking about the difference of the baseline and our insertion line numbers rather than the line numbers of the two execution orders.
  - "The orders are defined using line numbers which can be referenced
    in the code which accompanies our project \cite{repo}."

* DONE In subsection 5.7, at "Also, the line number where Parry placed ...", is it earlier? The table does not seem to say that.
  - "Also, the line number where Parry placed the fence for the second
    order of \ttf{TxCommit} is seemingly before the source of the
    order. This is due to procedure inlining."

* DONE At Figure 11, it seemed that in the text, we never talked about the last order of TxCommit for TL2 Eager and x86 although we have been able to remove a fence there.
  - We have the following:

    "As noted earlier we were able to eliminate
    the final \stld order in \ttf{TxCommit} entirely due to Clang's IR
    output and the transitive order as derived in Figure
    \ref{fig:tl2-snd-derivation}. It's unlikely that an implementer
    would have enough information to make the same determination
    without the help of our tool."

* DONE In both Figure 13 and 16, we say percent but the axis is in the 1 scale.
  - I assumed that some portion of one and percent were effectively
    the same for most of our readers. Changing this requires monkeying
    with a bunch of images. I'm going to leave it.

* DONE In subsection 5.8, at "... importance of finding optimal placements and fences wherever possible", do you mean "fence selection" instead of "placements and fences"?
  - "We believe this highlights the importance of finding optimal
    placements and fences types wherever possible."
  - It's supposed to be highlighting the importance of both where the
    fence goes and which fence is selected.

* DONE In section 6, at "... worked on related assumptions", what assumptions?
  - removed

* DONE In Acknowledgements, as Rochester people helped us, it is good to acknowledge them.
  - We thank the Michael Scott, Mike Spear, Luke D'Alessandro for
    answering questions about their ByteEager TLRW algorithm
    implementation. We thank Todd Millstein and Matt Brown for their
    feedback on drafts of this paper.  We were partially supported by
    the NSF Expeditions in Computing Award CCF-0926127.
