\documentclass[nocopyrightspace,preprint,10pt]{sigplanconf}

\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{latexsym}
\usepackage{url}
\usepackage{trfrac}
\usepackage{tikz}

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  commentstyle=\color{gray},
  morecomment=[l][\color{gray}]{\#}
}


\lstset{escapechar=@,style=customc}

\title{Declarative Fence Insertion}
%\title{Fence Insertion for Concurrent Algorithms with Declared Execution Orders}
\authorinfo{John Bender}
           {University of California, \\ Los Angeles (UCLA)}
           {johnbender@cs.ucla.edu}
\authorinfo{Mohsen Lesani}
           {Massachusetts Institute \\ of Technology (MIT)}
           {lesani@csail.mit.edu}
\authorinfo{Jens Palsberg}
           {University of California, \\ Los Angeles (UCLA)}
           {palsberg@ucla.edu}

\newcommand{\ttf}[1]{{\ttfamily #1}}
\newcommand{\stldtso}{\ttf{mfence}}
\newcommand{\stldarm}{\ttf{dmb st}}
\newcommand{\ststarm}{\ttf{dmb st}}
\newcommand{\ldldarm}[1][\ ]{\ttf{dmb}#1}

\newcommand{\ldld}{load-load }
\newcommand{\stld}{store-load }
\newcommand{\stst}{store-store }


\makeatletter
\newcommand{\xRightarrow}[2][]{\ext@arrow 0359\Rightarrowfill@{#1}{#2}}
\makeatother

\newcommand{\mstldppo}[3][st,ld]{\preserved{#2}{#1}{#3}}
\newcommand{\mststppo}[3][st,st]{\preserved{#2}{#1}{#3}}
\newcommand{\mldldppo}[3][ld,ld]{\preserved{#2}{#1}{#3}}

\newcommand{\placeholder}{\star}
\newcommand{\absst}{\mathsf{st}}
\newcommand{\absld}{\mathsf{ld}}
\newcommand{\absfany}{\mathsf{fany}}
\newcommand{\absfstld}{\mathsf{fstore}}
\newcommand{\fstore}{\mathsf{fstore}}
\newcommand{\fload}{\mathsf{fload}}
\newcommand{\ppo}{\rightarrow}
\newcommand{\absppo}{\mapsto}
\newcommand{\deso}{\rightarrow}
\newcommand{\abspreserved}[3]{#1 \absppo #3}
\newcommand{\preserved}[3]{#1 \absppo #3}
\newcommand{\required}[3]{#1 \deso #3}


\newcommand{\stanyorder}[2]{$ #1 \xrightarrow{\absst,*} #2 $}
\newcommand{\stldorder}[2]{$ #1 \xrightarrow{\absst,\absld} #2 $}
\newcommand{\ststorder}[2]{$ #1 \xrightarrow{\absst,\absst} #2 $}
\newcommand{\ldldorder}[2]{$ #1 \xrightarrow{\absld,\absld} #2 $}

\newcommand{\mstldorder}[3][st,ld]{\required{#2}{#1}{#3}}
\newcommand{\mststorder}[3][st,st]{\required{#2}{#1}{#3}}
\newcommand{\mldldorder}[3][ld,ld]{\required{#2}{#1}{#3}}

\newcommand{\todo}{{\color{red}TODO} }
\newcommand{\done}{{\color{green}DONE} }


\def\paths{{\sf paths}}
\def\nodes{{\sf nodes}}
\def\graph#1{\langle #1 \rangle}


\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{problem}{Problem}

\def\qed{\mbox{}~\hfill~$\Box$}

\newcommand{\irule}[2]%
    {\mkern-2mu\displaystyle\frac{#1}{\vphantom{,}#2}\mkern-2mu}
\def\air{\hspace*{1.0cm}}

\def\bigmid{\;\;|\;\;}
\def\imp{\rightarrow}

\def\({\begin{eqnarray*}}
\def\){\end{eqnarray*}}

\def\Node{{\sf Node}}
\def\Graph{{\sf Graph}}
\def\Arch{{\sf Arch}}

\def\Insert{{\sf Insert}}
\def\Elim{{\sf Elim}}
\def\Cut{{\sf Cut}}
\def\Refine{{\sf Refine}}

\def\st{{\sf st}}
\def\ld{{\sf ld}}
\def\fany{{\sf fany}}

\def\paths{{\sf paths}}
\def\lift{{\sf lift}}

\begin{document}

\section{NP-completeness} \label{sec:fence-insertion}

Formulations of the fence insertion problem in terms of graphs \cite{AlglaveKNP13, Lee00, Shasha88}, including our own, are for fully general directed graphs. However, actual control flow graphs have some structural restrictions. Consider a star graph which has no straight forward mapping in control flow. Graphs like this make a reduction to control flow graphs for actual programs problematic. To establish a useful lower bound on the complexity of fence insertion we construct a reduction from partial satisfying assignment of \textit{negation free} 2SAT \cite{Delgrande96}, hereafter \textsf{MIN2SAT}, to the problem of fence insertion. We believe the full reduction to real program instructions is important to ensure that we do not inadvertently admit impossible control flow graphs.

In Definition \ref{def:min2sat} we use $\phi$ to range over negation-free CNF formulae with clauses of at most two variables, $\bar{x}$ to range over variables that appear in $\phi$, and $\bar{x} \vDash \phi$ to denote the satisfaction of $\phi$ by assigning true to each variable in $\bar{x}$.

In Definition \ref{def:fence} we denote a program of \textsf{st}, \textsf{ld}, \textsf{call}, \textsf{ret}, and \textsf{syscall} instructions as $P$, where \textsf{call} and \textsf{ret} have their standard control flow semantics and \textsf{syscall} denotes a call to exit the program. Then the corresponding control flow graph is $G_P$ and we use $s$ to denote an integer value for the number fence placements. Further, $A$ denotes a hypothetical architecture where stores can be seen to move past loads as with x86 and $\absfany$ orders all instructions.

\begin{definition} \label{def:min2sat} $\mathsf{MIN2SAT}$.
  \begin{equation*}
    \langle \phi, s\rangle \in \mathsf{MIN2SAT}
    \Leftrightarrow
    \exists \bar{x}.\bar{x} \vDash \phi \land |\bar{x}| \leq s
  \end{equation*}
\end{definition}

\begin{definition} \label{def:fence} $\mathsf{MINFENCE}$.
  \begin{equation*}
    \langle P, O, A, s\rangle \in \mathsf{MINFENCE}
    \Leftrightarrow
    \exists K.\Refine(K, G_p), A \vDash O \land |K| \leq s
  \end{equation*}
\end{definition}

\begin{lemma} $\mathsf{MIN2SAT} \leq_{p} \mathsf{MINFENCE}$ \end{lemma}

\begin{figure}
\begin{align*}
  f(\langle \phi, s \rangle) =\ & \langle [[ \phi ]], ord(\phi), A, s \rangle  \\
  [[ \varnothing ]] =\ & \mathsf{syscall} \\
  [[(x_m \lor x_n )_c \land \phi']] =\ & c_m \texttt{:} \mathsf{call}\ x_m \tag{1}\\
                                     & c_n \texttt{:} \mathsf{call}\ x_n \\
                                     & c_{done} \texttt{:} \\
                                     & [[ \phi' ]] \\
                                     & [[ x_m ]] \\
                                     & [[ x_n ]] \\
  [[ x_n ]] = \ & x_n\texttt{:} \tag{2} \\
            & \absst(x_n) \tag{3} \\
            & \absld(y_n) \tag{4} \\
            & \mathsf{ret} \\
  ord((x_m \lor x_n )_c \land \phi') =\ & \{\ \required{(\mathsf{st}(x_m)}{}{\mathsf{ld}(y_n)})_c\ \} \cup ord(\phi')
\end{align*}
\caption{Reduction $f$ from \textsf{MIN2SAT}}
\label{fig:reduction}
\end{figure}


\begin{proof}
  In Figure \ref{fig:reduction} we define a reduction $f$ from formulae $\langle \phi, s \rangle \in \mathsf{MINSAT}$ to programs and orders $\langle P,O,A,s \rangle \in \mathsf{MINFENCE}$ on x86 architecture

We map each variable in $\phi$ to a subroutine with a paired store and load each to a corresponding memory address and a trailing return instruction. We label the start of each pair with the variable name.

We map each clause of two variables to two \textsf{call} instructions each to one of the aforementioned variable labels and define an order between the store of the first variable and the load of the second in $ord$. Note that a fence is necessary due to the architecture the specification of $A$.

We map the entire formula to a series of the \textsf{call} instructions to the variable labels.

Clearly this mapping has complexity linear in the size of the formula. It remains to be shown that:
\begin{equation*}
\langle \phi, s \rangle \in \mathsf{MINSAT} \Leftrightarrow f \langle \phi, s \rangle \in \mathsf{MINFENCE}
\end{equation*}

$\Rightarrow$ :

\begin{proof}
We assume $\bar{x}$ of size $s$ such that $\bar{x} \vDash \phi$ and define a bijection from each variable $x_n$ in $\bar{x}$ to the edge $(\st(x_n), \ld(y_n))$ in $K$. Then $|K| = s$. Further, since each order in $f \langle \phi, s \rangle$ corresponds to the clauses of $\phi$ and all clauses of $\phi$ must be satisfied with some $x_n$, all orders will have at least one edge in $K$. Then by the definitions of $\Refine$, $\fany$ and $A$ we have $\Refine(K, G_p), A \vDash O$ and $|K| \leq s$ as required.
\end{proof}

$\Leftarrow$ :

\begin{enumerate}

\item \label{enum:np-assign} We assume $\langle P,O,A,s \rangle = f \langle \phi, s \rangle$ and $\langle P,O,A,s \rangle \in \mathsf{MINFENCE}$.

\item \label{enum:np-no-elim} By the definition of $f$ and $A$, $\Elim(G_p, A, O) = O$. That is, $A$ does not enforce the orders in $O$.

\item \label{enum:np-refine} By \ref{enum:np-assign} there exists some $K$ such that $\Refine(K, G_P), A \vDash O$ and $|K| \leq s$.

\item \label{enum:np-require-edge} By \ref{enum:np-no-elim} and \ref{enum:np-refine} it must be that $\forall i_1 \ppo i_2 \in O . \forall p \in paths(G_p, i_1, i_2). \exists k \in K . k \in p$. That is $K$ must contain at least one edge for all paths for all orders.

\item \label{enum:np-var} Note that there may be many of the same variable subroutine defined in the program. We assume that same variable subroutine $x_n\mathtt{:}$ will be the jump target for each \textsf{call} $x_n$. That is, for all calls of the form $\mathsf{call} x_n$ the instructions executed next, until the \textsf{ret} will always be the same instructions in the source program.

\item \label{enum:np-one-path} By the definition of $f$, we have that $\forall (x_m \lor x_n)_c \in \phi. |paths(G_p, c_m \texttt{:} \textsf{call}_m, c_{done} \texttt{:})| = 1$. That is, there is exactly one path in $G_p$ from the first call to the variable subroutine $x_m$ to the end of the variable subroutine $x_n$ for each clause. We call these \textit{clause paths}.

\item \label{enum:np-one-path-per} By \ref{enum:np-one-path} and the definition of $ord$, each order has one path.

\item \label{enum:np-four-edges} By \ref{enum:np-one-path-per} and the definition of $f$ each order has 4 possible placements in the program for fence between the store to the load that will enforce the corresponding order.

\item \label{enum:np-uniq-placement} By \ref{enum:np-one-path} we know that the edge $(c_m \texttt{:} \textsf{call} x_m, c_n \texttt{:} \textsf{call} x_n)$ in $G_p$ is unique to the clause indexed by $c$. See the edge in Figure \ref{fig:reduction} with source at (1).

\item \label{enum:np-call-store} By the definition of $f$ and \ref{enum:np-var} we know that if $(x_n \texttt{:}, \st(x_n))$ appears in some path it is due to the subroutine labeled with $x_n$. See the edge in Figure \ref{fig:reduction} with source at (2).

\item \label{enum:np-st-ld} By the definition of $f$ and \ref{enum:np-var} we know that if $(\st(x_n), \ld(y_n))$ appears in some path it is due to the subroutine labeled with $x_n$. See the edge in Figure \ref{fig:reduction} with source at (3).

\item By the definition of $f$ and \ref{enum:np-var} we know that if $(\ld(y_n), \mathsf{ret})$ appears in some path it is due to the subroutine labeled with $x_n$. See the edge in Figure \ref{fig:reduction} with source at (4).
\end{enumerate}

We must show under the assumption (\ref{enum:np-assign}) that there exists an $\bar{x}$ of size at most $s$ such that $\bar{x} \vDash \phi$.

Intuitively, each appearance of a subroutine in a clause path represents a set program edges that are shared with other clause paths (in the same way that variables are shared between clauses). Which means placing a fence in one of the variable subroutines may enforce many orders.

By \ref{enum:np-four-edges} we know that there are 4 places in the program to place a fence for each clause order $c$ and by \ref{enum:np-require-edge} we know that one of those edges must exist in $K$. If we can map that edge to a variable $x_n \in c \land x_n \in \bar{x}$ then clearly $\bar{x} \vDash \phi$ and by \ref{enum:np-refine}, $|\bar{x}| \leq s$. We consider each of the 4 possible program placements in turn. For clarity each number corresponds with the source of the program edge in Figure \ref{fig:reduction}.

\begin{enumerate}
  \item \textit{Between the} \texttt{ret} \textit{of the first variable subroutine and the} \texttt{call} \textit{to the second variable subroutine}. If this edge is selected by \ref{enum:np-uniq-placement} it implies one of two possibilities. The first is that the other edges in the path for this clause do not appear in any other clause path. Otherwise it can only be more advantageous to select any of the other 3 edges in the path and thereby satisfy other orders. The second is that to satisfy $s$, a more advantageous edge selection is unnecessary. In either case we can select any of the variables in the corresponding clause of $\phi$ for a truth assignment of size at most $s$.
  \item \textit{Between the} \textsf{call} \textit{to the second variable and its store}. By \ref{enum:np-call-store} this placement is unique to the corresponding variable and we can include the associated variable in the partial satisfying assignment.
  \item \textit{Between a store and load labeled by a variable}. By \ref{enum:np-st-ld} this placement is unique to the corresponding variable and maps directly to inclusion of the associated variable in the partial assignment.
  \item \textit{Between the load of the first variable and its} \texttt{ret}. By the same reasoning as placement two.
  \end{enumerate}
\end{proof}

\begin{lemma}$\mathsf{MINFENCE} \in \mathsf{NP}$\end{lemma}

\begin{proof}
  By \ref{enum:np-require-edge} and \ref{enum:np-one-path} we know that a fence will appear in each clause path. As a result, we can check a collection of fence placements by traversing the program along the standard execution path and we should expect to find at least one fence per order (clause). By the definition of $f$ both the size of the program and the verification of a set of fences placed in the program will be linear in the number clauses.
\end{proof}

\begin{theorem}$\mathsf{MINFENCE}$ is $\mathsf{NP}$-complete\end{theorem}

\begin{proof}
  By Lemmas 5.1 and 5.2.
\end{proof}

\end{document}
