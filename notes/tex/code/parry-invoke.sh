parry \
    # method where orders should be found
    TxCommit \
    # input source
    examples/tl2/src/tl2.c \
    # orders by line number and type
    760:store-1413:store,1413:store-1679:load \
    # architecture
    arm \
    # source with fences where necessary
    --output tl2-TxCommit-fenced.c
