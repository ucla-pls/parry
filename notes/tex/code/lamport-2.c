void thr2() {
 L1:
  b2 = 1;
  x = 2;
  if (y != 0) {
    b2 = 0;
    goto L1;
  }

  y = 2;
  if (x != 2) {
    b2 = 0;

    if (y != 2) {
      goto L1;
    }
  }
  // begin
  // end
  y = 0;
  b2 = 0;
}
