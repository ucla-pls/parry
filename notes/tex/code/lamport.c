int x;
int y;
int b1, b2; // N boolean flags

void* thr1(void * arg) {
  L0:
    b1 = 1;
    x = 1;
    if (y != 0) {
      b1 = 0;
      goto L0;
    }

    y = 1;
    if (x != 1) {
      b1 = 0;

      if (y != 1) {
        goto L0;
      }
    }
  // begin: critical section
  // end: critical section
  y = 0;
  b1 = 0;
}
