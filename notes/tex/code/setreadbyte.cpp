void set_read_byte(uint32_t id) {
#if defined(BASELINE)
  __sync_lock_test_and_set(&r[id], 1u)
#else
  // NOTE: no WBR macro
  r[id] = 1;
#endif
}
