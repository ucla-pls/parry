void park() {
  if (_counter > 0) {
    _counter = 0;
    // mfence needed here
    return;
  }
  if (mutex_trylock(&__unbuffered_mutex) != 0) return;
  if (_counter > 0) { // no wait needed
    _counter = 0;
    mutex_unlock(__unbuffered_mutex);
    return;
  }
  __unbuffered_did_park=1;
  cond_wait(__unbuffered_cond, __unbuffered_mutex);
  _counter = 0;
  mutex_unlock(__unbuffered_mutex);
}
