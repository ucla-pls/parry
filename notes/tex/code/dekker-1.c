void* thr1(void * arg) {
  flag1 = 1;
  while (flag2 >= 1) {
    if (turn != 0) {
      flag1 = 0;
      while (turn != 0) {};
      flag1 = 1;
    }
  }
  // begin: critical section
  //x = 0;
  //assert(x<=0);
  // end: critical section
  turn = 1;
  flag1 = 0;
}

void* thr2(void * arg) {
  flag2 = 1;
  while (flag1 >= 1) {
    if (turn != 1) {
      flag2 = 0;
      while (turn != 1) {};
      flag2 = 1;
    }
  }
  // begin: critical section
  //x = 1;
  //assert(x>=1);
  // end: critical section
  turn = 1;
  flag2 = 0;
}
