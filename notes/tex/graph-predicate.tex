% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%
\documentclass{llncs}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage{algpseudocode}

\usepackage{tikz}
\usepackage{tkz-graph}
\usetikzlibrary{arrows,%
                petri,%
                topaths}%
\usepackage{tkz-berge}
\usepackage{subfig}

\newtheorem{define}{Definition}

\newcommand{\IFor}[2]{%
    \State\algorithmicfor\ {#1}\ \algorithmicdo\ {#2} \algorithmicend\ \algorithmicfor%
}

\let\oldland\land
\renewcommand{\land}{\,\oldland\,}

\let\oldlor\lor
\renewcommand{\lor}{\,\oldlor\,}

\def\sound{{\sf sound}}
\def\rqrdpath{{\sf rqrdpath}}
\def\paths{{\sf paths}}
\def\nodes{{\sf nodes}}
\def\true{{\sf true}}
\def\false{{\sf false}}
\def\imp{\rightarrow}
\def\graph#1{\langle #1 \rangle}

% \newtheorem{theorem}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
% \newenvironment{proof}{\begin{trivlist}\item[]%\hspace{\parindent}%
% {\em Proof. }}{\qed\end{trivlist}}
\def\qed{\mbox{}~\hfill~$\Box$}


\begin{document}

\section{Soundness and Completeness}

Our goal is to take an abstract event graph, composed of program order edges from $po$ and preserved program order edges from $ppo$, that contains one or more sub-graphs composed of all paths from an orders source, $s$, to its sink, $t$, and where possible remove paths in $po$ paths where there is a corresponding $ppo$ path.

Initially it seems easiest to consider this problem by looking at order sub-graphs, so we can view this as a predicate $FNCREADY$ on each sub-graph $G'_{s,t}$ for some order $(s,t)$ and write:

\begin{equation*}
  FNCRDY(G'_{s,t}) \triangleq
    \forall p_1 \in paths_{po}(G'_{s,t}, s, t) . paths_{ppo}(p_1, s, t) = \varnothing
\end{equation*}

That is, for every $po$ path in the new graph there are no $ppo$ paths for its vertices. This seems correct at first but consider the following two graphs:

\begin{figure}
\centering
\subfloat[ex \& edges removable]{
\begin{tikzpicture}[scale=0.60,transform shape]
  \Vertex[x=2,y=10]{s}
  \Vertex[x=2,y=8]{e1}
  \Vertex[x=0,y=6]{ex}
  \Vertex[x=2,y=4]{e2}
  \Vertex[x=2,y=2]{t}

  % ppo edges
  \tikzstyle{EdgeStyle}=[dashed, post, bend left]
  \Edge[](s)(e1)
  \Edge[](e1)(ex)
  \tikzstyle{EdgeStyle}=[dashed, post, bend right]
  \Edge[](ex)(t)

  % po edges
  \tikzstyle{EdgeStyle}=[post]
  \Edge[](s)(e1)
  \Edge[](e1)(e2)
  \Edge[](e1)(ex)
  \Edge[](ex)(e2)
  \Edge[](e2)(t)

  % display width
  \node[minimum width=6cm] at (s){};
\end{tikzpicture}
}
%
\subfloat[e1 to ex must remain]{
\begin{tikzpicture}[scale=0.60,transform shape]
  \Vertex[x=2,y=10]{s}
  \Vertex[x=2,y=8]{e1}
  \Vertex[x=0,y=6]{ex}
  \Vertex[x=2,y=4]{e2}
  \Vertex[x=2,y=2]{t}

  \tikzstyle{VertexStyle}=[draw=green,shape=circle]
  \Vertex[x=0,y=8]{ey}


  % ppo ed1ges
  \tikzstyle{EdgeStyle}=[dashed, post, bend left]
  \Edge[](e1)(ex)
  \tikzstyle{EdgeStyle}=[dashed, post, bend right]
  \Edge[](ex)(t)
  \tikzstyle{EdgeStyle}=[dashed, post, bend right, draw=green]
  \Edge[](s)(ey)
  \Edge[](ey)(e1)

  \tikzstyle{EdgeStyle}=[post]
  \Edge[](s)(e1)
  \Edge[](e1)(e2)
  \Edge[](ex)(e2)
  \Edge[](e2)(t)
  \Edge[](e1)(ex)

  \tikzstyle{EdgeStyle}=[post, draw=green]
  \Edge[](s)(ey)
  \Edge[](ey)(ex)
  \Edge[](ey)(e1)

  % display width
  \node[minimum width=6cm] at (s){};
\end{tikzpicture}
}
\end{figure}

Here, the dashed edges are from $ppo$ and the solid edges are from $po$. In graph a it's clear that the $po$ path that corresponds to the $ppo$ path can be removed by removing ex and its incident egdes. In b though every edge and node plays a role in a path that does \textit{not} exhibit a $ppo$ path. It's not immediately clear whether the restricted graphs we're considering might prevent this situation but if the predicate (and eventually the algorithm) work well for general graphs it doesn't seem like it matters.

With that in mind I thought it best to define soundness and completeness for the output graph and then work from there:

\begin{define}
  $ELIM(G_{s,t})$ Soundness: if a path in $G_{s,t}$ is required then it is retained in $ELIM(G_{s,t})$.
\end{define}

\begin{define}
  $ELIM(G_{s,t})$ Completeness: if a path is retained in $ELIM(G_{s,t})$ it is required in $G$.
\end{define}

For soundness then the graph will contain all paths that require fences and possibly more paths that don't and for completeness all the paths in the graph require fences, though we may be missing some paths that require them.

Note that these definitions are for the \textit{difference} between the original and resulting graph. That is, we can't know if the graph was ruined unless we know how it looked in the first place. So now we need a definition for ``required'', preferably that only speaks to the graph directly. We can make that link to the external concept (``needs a fence'') later using our theorem on the transitivity of $ppo$.

\begin{align*}
  G'_{s,t} & = ELIM(G_{s,t}) \\
  RQR(p, G_{s,t}) & \triangleq paths_{ppo}(p, s, t) = \varnothing \\
  CMP(G_{s,t}, G'_{s,t}) &\triangleq \forall p. p \in paths(G'_{s,t}, s, t) \Rightarrow RQR(p, G_{s,t}) \\
  SND(G_{s,t}, G'_{s,t}) &\triangleq \forall p. RQR(p, G_{s,t}) \Rightarrow p \in paths(G'_{s,t}, s, t)
\end{align*}

The completeness criteria is hard to satisfy with this definition of $RQR$, since as we saw it's possible for all the edges and nodes of a $ppo$ path through $G_{s,t}$ to participate in $po$ paths without a corresponding $ppo$ path. That is the completeness requirement would mean an output graph without (s, ey, e1, ex, e1, t) since it exhibits a $ppo$ path.

\begin{align*}
  EPO(o, G_{s,t}) & \triangleq
    \exists p_{po} \in paths_{po}(G_{s,t}, s, t).
      o \in p_{po} \land paths_{ppo}(p_{po}, s, t) = \varnothing \\
  RQR'(p, G_{s,t}) & \triangleq
    \forall v \in p.
      EPO(G_{s,t}, v) \land \forall e \in p. EPO(G_{s,t}, e)
\end{align*}

Here each edge and vertex in the output graph participates in a $po$ path that does not exhibit a corresponding $ppo$ path. That is, each edge and vertex in the required paths must remain in the graph so that all the paths that do not have a corresponding $ppo$ path also remain.

It seems that satisfying $SND$ with $RQR$ should imply satisfying $SND$ with $RQR'$ for a given path since, if we are sure to include any path that doesn't have a corresponding $ppo$ path then each of its edges and nodes will meet the criteria for inclusion under $RQR'$. Additionally, since $RQR'$ doesn't explicitly exclude $ppo$ paths in the resulting graph it might allow us to satisfy $CMP$ more easily.

We can simplify to talking about edges and nodes only now:

\begin{align*}
  G'_{s,t} & = ELIM(G_{s,t}) \\
  EPO(o, G_{s,t}) & \triangleq
    \exists p_{po} \in paths_{po}(G_{s,t}, s, t).
      o \in p_{po} \land paths_{ppo}(p_{po}, s, t) = \varnothing \\
  CMP(G_{s,t}, G'_{s,t}) &\triangleq \forall o. o \in G'_{s,t} \Rightarrow EPO(o, G_{s,t}) \\
  SND(G_{s,t}, G'_{s,t}) &\triangleq \forall o. EPO(o, G_{s,t}) \Rightarrow o \in G'_{s,t}
\end{align*}

\newpage

We will use $E$ instead of $ppo$.

\begin{eqnarray*}
G &=& \graph{V,E} \\
po, X &\in& 2^{V \times V} \\
\\
\sound^{s,t}_{G,po} &:& 2^{V \times V} \imp {\sf boolean} \\
\sound^{s,t}_{G,po}(X) &:& \forall e\in X.\
                           \exists p\in \paths(\graph{V,po},s,t). \\
                       & & \hspace*{2cm}
                           e \in p \;\wedge\;
                           \paths(\graph{\nodes(p),E},s,t) = \emptyset
\end{eqnarray*}

\begin{theorem}
$\sound^{s,t}_{G,po}(po)$.
\end{theorem}

Counter example: when all paths in $po$ have a $ppo$ ($E$) path (sequential consistency). Under this soundness condition $X$ must be empty.

So instead, the existence of a $po$ path for $e$ where there is no corresponding $ppo$ ($E$) path should imply its inclusion in $X$:

\begin{eqnarray*}
  \sound^{s,t}_{G,po}(X) &:& \forall e. \\
  & & (\exists p\in \paths(\graph{V,po},s,t). e \in p \;\wedge\; \paths(\graph{\nodes(p),E},s,t) = \emptyset) \\
  & & \quad \Rightarrow e \in X
\end{eqnarray*}

That is, for all the edges in $po$ if they exhibit a $po$ path without a corresponding $ppo$ ($E$) path then they are definitely in $X$ but extra edges are also acceptable.

\begin{theorem}
  $\sound^{s,t}_{G,po}(po)$.
\end{theorem}

\begin{proof}
  Let $e$ be an arbitrary edge. We assume that an arbitrary path $p$ exists in $\graph{V, po}$ such that $e \in p$. We must show that $e$ is in $po$. Since $e$ is in $p$ and since $p$ is a path of vertices in $V$ and edges in $po$ we have that $e$ is in $po$ as required.
\end{proof}

\begin{theorem}
  If $\sound^{s,t}_{G,po}(X_1)$ and $\sound^{s,t}_{G,po}(X_2)$,
then $\sound^{s,t}_{G,po}(X_1 \cap X_2)$.
\end{theorem}

For convenience we'll define the required path predicate as $\rqrdpath$ and for clarity redefine $\sound$:
\begin{eqnarray*}
  \rqrdpath^{s,t}_{G,po}(e) &:& \exists p.p \in \paths(\graph{V,po},s,t) \land e \in p \;\wedge\; \paths(\graph{\nodes(p),E},s,t) = \emptyset \\
  \sound^{s,t}_{G,po}(X) &:& \forall e. \rqrdpath^{s,t}_{G,po}(e) \Rightarrow e \in X
\end{eqnarray*}

\begin{proof}
  Assume, $\sound^{s,t}_{G,po}(X_1)$ and $\sound^{s,t}_{G,po}(X_2)$. Then $(e_1 \in X_1)$ or $\lnot \rqrdpath^{s,t}_{G,po}(e_1)$ is for any edge $e_1$. Similarly so for $e_2$ and $X_2$. Further assume an arbitrary $e_3$. We must show that $\rqrdpath^{s,t}_{G,po}(e_3) \Rightarrow (e_3 \in X_1 \land e_3 \in X_2)$.

Since $(e_1 \in X_1)$ or $\lnot \rqrdpath^{s,t}_{G,po}(e_1)$ is true for any edge it is certainly true of $e_3$. Similarly so for $e_2$ and $X_2$. If both $(e_1 \in X_1)$ and $(e_2 \in X_2)$ then $(e_3 \in X_1 \land e_3 \in X_2)$. In any other case, for example $\lnot (e_1 \in X_1)$, then $\lnot (e_3 \in X_1 \land e_3 \in X_2)$ and we must show that $\lnot \rqrdpath^{s,t}_{G,po}(e_3)$, but since $\lnot (e_1 \in X_1)$ we also know that $\rqrdpath^{s,t}_{G,po}(e_1)$ and since $e_1 = e_3$ the condition is satisfied as required.
\end{proof}

\begin{corollary}
The set ${\; X \;\mid\; \sound^{s,t}_{G,po}(X) \;}$
has a $\subseteq$-least element.
\end{corollary}



\end{document}