# ==============================================================================
#
# Defines.common.mk
#
# ==============================================================================


CC       := clang
CFLAGS   += -Wall -pthread
CFLAGS   += -O3
CFLAGS   += -I$(LIB)
CPP      := clang
CPPFLAGS += $(CFLAGS)
LD       := clang
LIBS     += -lpthread -lm

# Remove these files when doing clean
OUTPUT +=

LIB := ../lib

STM := ../../../src

LOSTM := ../../OpenTM/lostm

# ==============================================================================
#
# End of Defines.common.mk
#
# ==============================================================================
