/* =============================================================================
 *
 * platform.h
 *
 * Platform-specific bindings
 *
 * =============================================================================
 */


#ifndef PLATFORM_H
#define PLATFORM_H 1


#if defined(SPARC) || defined(__sparc__)
#  include "platform_sparc.h"
#elif defined(__ARM_ARCH_7__) || defined(__arm__)
#  include "platform_armv7.h"
#else /* !SPARC (i.e., x86) && !arm */
#  include "platform_x86.h"
#endif


#define CAS(m,c,s)  cas((intptr_t)(s),(intptr_t)(c),(intptr_t*)(m))

typedef unsigned long long TL2_TIMER_T;


#endif /* PLATFORM_H */


/* =============================================================================
 *
 * End of platform.h
 *
 * =============================================================================
 */
