set -e

arch=$1

if [ -z "$arch" ]; then
  echo "Usage: $0 <arm|x86>"
  exit 1;
fi

echo "==> copying stamp benchmark suite to build directory"
cp -r stamp build/

echo "==> cleaning up any existing changes to tl2"
cp src/tl2_original.c src/tl2.c

echo "==> moving fence macro definitions into place"
cp src/platform_armv7_unopt.h src/platform_armv7.h
cp src/platform_x86_unopt.h src/platform_x86.h

echo "==> rebuild tl2"
cd src/
make clean; make
cd -

echo "==> run benchmarks for the normal tl2"
cd build/stamp*
bash bin/benchmarks.sh tl2 unopt $arch
cd -

echo "==> reset the platform to remove fences in macro definitions"
cp src/platform_armv7_original.h src/platform_armv7.h
cp src/platform_x86_original.h src/platform_x86.h

echo "==> recompiling tl2 with optimizations"
cp compiled/tl2-$arch.c src/tl2.c
cd src/
make clean; make
cd -

echo "==> run benchmarks for the updated tl2"
cd build/stamp*
bash bin/benchmarks.sh tl2 opt $arch
cd -
