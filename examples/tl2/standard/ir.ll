; ModuleID = 'tl2.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.sigaction = type { %union.anon, %struct.__sigset_t, i32, void ()* }
%union.anon = type { void (i32)* }
%struct.__sigset_t = type { [16 x i64] }
%struct._Log = type { %struct._AVPair*, %struct._AVPair*, %struct._AVPair*, %struct._AVPair*, i64, i32 }
%struct._AVPair = type { %struct._AVPair*, %struct._AVPair*, i64*, i64, i64*, i64, i8, %struct._Thread*, i64 }
%struct._Thread = type { i64, i64, i64, i64, i64, i64, i64, i32*, i32, i64, i64, i64, [1 x i64], i8*, %struct.tmalloc*, %struct.tmalloc*, %struct._Log, %struct._Log, %struct._Log, [1 x %struct.__jmp_buf_tag]* }
%struct.tmalloc = type { i64, i64, i8** }
%struct.__jmp_buf_tag = type { [8 x i64], i32, %struct.__sigset_t }
%struct.siginfo_t = type { i32, i32, i32, %union.anon.0 }
%union.anon.0 = type { %struct.anon.3, [80 x i8] }
%struct.anon.3 = type { i32, i32, i32, i64, i64 }

@StartTally = global i64 0, align 8
@AbortTally = global i64 0, align 8
@ReadOverflowTally = global i64 0, align 8
@WriteOverflowTally = global i64 0, align 8
@LocalOverflowTally = global i64 0, align 8
@LockTab = internal global [1048576 x i64] zeroinitializer, align 16
@.str = private unnamed_addr constant [25 x i8] c"TL2 system ready: GV=%s\0A\00", align 1
@.str1 = private unnamed_addr constant [4 x i8] c"GV4\00", align 1
@global_key_self = internal global i32 0, align 4
@.str2 = private unnamed_addr constant [90 x i8] c"TL2 system shutdown:\0A  GCLOCK=0x%lX Starts=%li Aborts=%li\0A  Overflows: R=%li W=%li L=%li\0A\00", align 1
@GClock = internal global [64 x i64] zeroinitializer, align 16
@.str3 = private unnamed_addr constant [2 x i8] c"t\00", align 1
@.str4 = private unnamed_addr constant [6 x i8] c"tl2.c\00", align 1
@__PRETTY_FUNCTION__.TxNewThread = private unnamed_addr constant [22 x i8] c"Thread *TxNewThread()\00", align 1
@.str5 = private unnamed_addr constant [12 x i8] c"t->allocPtr\00", align 1
@__PRETTY_FUNCTION__.TxInitThread = private unnamed_addr constant [34 x i8] c"void TxInitThread(Thread *, long)\00", align 1
@.str6 = private unnamed_addr constant [11 x i8] c"t->freePtr\00", align 1
@.str7 = private unnamed_addr constant [2 x i8] c"e\00", align 1
@__PRETTY_FUNCTION__.ExtendList = private unnamed_addr constant [29 x i8] c"AVPair *ExtendList(AVPair *)\00", align 1
@.str8 = private unnamed_addr constant [3 x i8] c"ap\00", align 1
@__PRETTY_FUNCTION__.MakeList = private unnamed_addr constant [33 x i8] c"AVPair *MakeList(long, Thread *)\00", align 1
@global_act_oldsigbus = internal global %struct.sigaction zeroinitializer, align 8
@.str9 = private unnamed_addr constant [40 x i8] c"Error: Failed to restore SIGBUS handler\00", align 1
@global_act_oldsigsegv = internal global %struct.sigaction zeroinitializer, align 8
@.str10 = private unnamed_addr constant [41 x i8] c"Error: Failed to restore SIGSEGV handler\00", align 1
@.str11 = private unnamed_addr constant [41 x i8] c"Error: Failed to register SIGBUS handler\00", align 1
@.str12 = private unnamed_addr constant [42 x i8] c"Error: Failed to register SIGSEGV handler\00", align 1

; Function Attrs: nounwind uwtable
define i64* @pslock(i64* %Addr) #0 {
  %1 = alloca i64*, align 8
  store i64* %Addr, i64** %1, align 8
  call void @llvm.dbg.declare(metadata !{i64** %1}, metadata !336), !dbg !337
  %2 = load i64** %1, align 8, !dbg !338
  %3 = ptrtoint i64* %2 to i64, !dbg !338
  %4 = add i64 %3, 128, !dbg !338
  %5 = lshr i64 %4, 3, !dbg !338
  %6 = and i64 %5, 1048575, !dbg !338
  %7 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %6, !dbg !338
  ret i64* %7, !dbg !338
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

; Function Attrs: noinline nounwind uwtable
define void @FreeList(%struct._Log* %k, i64 %sz) #2 {
  %1 = alloca %struct._Log*, align 8
  %2 = alloca i64, align 8
  %e = alloca %struct._AVPair*, align 8
  %tmp = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !339), !dbg !340
  store i64 %sz, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !341), !dbg !340
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !342), !dbg !343
  %3 = load %struct._Log** %1, align 8, !dbg !343
  %4 = getelementptr inbounds %struct._Log* %3, i32 0, i32 3, !dbg !343
  %5 = load %struct._AVPair** %4, align 8, !dbg !343
  store %struct._AVPair* %5, %struct._AVPair** %e, align 8, !dbg !343
  %6 = load %struct._AVPair** %e, align 8, !dbg !344
  %7 = icmp ne %struct._AVPair* %6, null, !dbg !344
  br i1 %7, label %8, label %23, !dbg !344

; <label>:8                                       ; preds = %0
  br label %9, !dbg !345

; <label>:9                                       ; preds = %15, %8
  %10 = load %struct._AVPair** %e, align 8, !dbg !345
  %11 = getelementptr inbounds %struct._AVPair* %10, i32 0, i32 8, !dbg !345
  %12 = load i64* %11, align 8, !dbg !345
  %13 = load i64* %2, align 8, !dbg !345
  %14 = icmp sge i64 %12, %13, !dbg !345
  br i1 %14, label %15, label %22, !dbg !345

; <label>:15                                      ; preds = %9
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %tmp}, metadata !347), !dbg !349
  %16 = load %struct._AVPair** %e, align 8, !dbg !349
  store %struct._AVPair* %16, %struct._AVPair** %tmp, align 8, !dbg !349
  %17 = load %struct._AVPair** %e, align 8, !dbg !350
  %18 = getelementptr inbounds %struct._AVPair* %17, i32 0, i32 1, !dbg !350
  %19 = load %struct._AVPair** %18, align 8, !dbg !350
  store %struct._AVPair* %19, %struct._AVPair** %e, align 8, !dbg !350
  %20 = load %struct._AVPair** %tmp, align 8, !dbg !351
  %21 = bitcast %struct._AVPair* %20 to i8*, !dbg !351
  call void @free(i8* %21) #6, !dbg !351
  br label %9, !dbg !352

; <label>:22                                      ; preds = %9
  br label %23, !dbg !353

; <label>:23                                      ; preds = %22, %0
  %24 = load %struct._Log** %1, align 8, !dbg !354
  %25 = getelementptr inbounds %struct._Log* %24, i32 0, i32 0, !dbg !354
  %26 = load %struct._AVPair** %25, align 8, !dbg !354
  %27 = bitcast %struct._AVPair* %26 to i8*, !dbg !354
  call void @free(i8* %27) #6, !dbg !354
  ret void, !dbg !355
}

; Function Attrs: nounwind
declare void @free(i8*) #3

; Function Attrs: nounwind uwtable
define void @TxOnce() #0 {
  %a = alloca [1 x i32], align 4
  call void @llvm.dbg.declare(metadata !{[1 x i32]* %a}, metadata !356), !dbg !359
  %1 = getelementptr inbounds [1 x i32]* %a, i32 0, i64 0, !dbg !359
  store i32 0, i32* %1, align 4, !dbg !359
  store volatile i64 0, i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !360
  %2 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([25 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str1, i32 0, i32 0)), !dbg !362
  %3 = call i32 @pthread_key_create(i32* @global_key_self, void (i8*)* null) #6, !dbg !363
  call void @registerUseAfterFreeHandler(), !dbg !364
  ret void, !dbg !365
}

declare i32 @printf(i8*, ...) #4

; Function Attrs: nounwind
declare i32 @pthread_key_create(i32*, void (i8*)*) #3

; Function Attrs: nounwind uwtable
define void @TxShutdown() #0 {
  %1 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !366
  %2 = load volatile i64* @StartTally, align 8, !dbg !366
  %3 = load volatile i64* @AbortTally, align 8, !dbg !366
  %4 = load volatile i64* @ReadOverflowTally, align 8, !dbg !366
  %5 = load volatile i64* @WriteOverflowTally, align 8, !dbg !366
  %6 = load volatile i64* @LocalOverflowTally, align 8, !dbg !366
  %7 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([90 x i8]* @.str2, i32 0, i32 0), i64 %1, i64 %2, i64 %3, i64 %4, i64 %5, i64 %6), !dbg !366
  %8 = load i32* @global_key_self, align 4, !dbg !367
  %9 = call i32 @pthread_key_delete(i32 %8) #6, !dbg !367
  call void @restoreUseAfterFreeHandler(), !dbg !368
  call void asm sideeffect "mfence", "~{dirflag},~{fpsr},~{flags}"() #6, !dbg !369, !srcloc !370
  ret void, !dbg !371
}

; Function Attrs: nounwind
declare i32 @pthread_key_delete(i32) #3

; Function Attrs: nounwind uwtable
define %struct._Thread* @TxNewThread() #0 {
  %t = alloca %struct._Thread*, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %t}, metadata !372), !dbg !373
  %1 = call noalias i8* @malloc(i64 280) #6, !dbg !373
  %2 = bitcast i8* %1 to %struct._Thread*, !dbg !373
  store %struct._Thread* %2, %struct._Thread** %t, align 8, !dbg !373
  %3 = load %struct._Thread** %t, align 8, !dbg !374
  %4 = icmp ne %struct._Thread* %3, null, !dbg !374
  br i1 %4, label %5, label %6, !dbg !374

; <label>:5                                       ; preds = %0
  br label %8, !dbg !374

; <label>:6                                       ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str3, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1067, i8* getelementptr inbounds ([22 x i8]* @__PRETTY_FUNCTION__.TxNewThread, i32 0, i32 0)) #7, !dbg !374
  unreachable, !dbg !374
                                                  ; No predecessors!
  br label %8, !dbg !374

; <label>:8                                       ; preds = %7, %5
  %9 = load %struct._Thread** %t, align 8, !dbg !375
  ret %struct._Thread* %9, !dbg !375
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #3

; Function Attrs: noreturn nounwind
declare void @__assert_fail(i8*, i8*, i32, i8*) #5

; Function Attrs: nounwind uwtable
define void @TxFreeThread(%struct._Thread* %t) #0 {
  %1 = alloca i64, align 8
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %prevVal.i.i10 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64, align 8
  %v.i11 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64, align 8
  %8 = alloca i64*, align 8
  %prevVal.i.i7 = alloca i64, align 8
  %9 = alloca i64*, align 8
  %10 = alloca i64, align 8
  %v.i8 = alloca i64, align 8
  %11 = alloca i64, align 8
  %12 = alloca i64, align 8
  %13 = alloca i64*, align 8
  %prevVal.i.i4 = alloca i64, align 8
  %14 = alloca i64*, align 8
  %15 = alloca i64, align 8
  %v.i5 = alloca i64, align 8
  %16 = alloca i64, align 8
  %17 = alloca i64, align 8
  %18 = alloca i64*, align 8
  %prevVal.i.i1 = alloca i64, align 8
  %19 = alloca i64*, align 8
  %20 = alloca i64, align 8
  %v.i2 = alloca i64, align 8
  %21 = alloca i64, align 8
  %22 = alloca i64, align 8
  %23 = alloca i64*, align 8
  %prevVal.i.i = alloca i64, align 8
  %24 = alloca i64*, align 8
  %25 = alloca i64, align 8
  %v.i = alloca i64, align 8
  %26 = alloca %struct._Thread*, align 8
  %wrSetOvf = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  store %struct._Thread* %t, %struct._Thread** %26, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %26}, metadata !376), !dbg !377
  %27 = load %struct._Thread** %26, align 8, !dbg !378
  %28 = getelementptr inbounds %struct._Thread* %27, i32 0, i32 16, !dbg !378
  %29 = getelementptr inbounds %struct._Log* %28, i32 0, i32 4, !dbg !378
  %30 = load i64* %29, align 8, !dbg !378
  store i64* @ReadOverflowTally, i64** %24, align 8
  call void @llvm.dbg.declare(metadata !{i64** %24}, metadata !379) #6, !dbg !380
  store i64 %30, i64* %25, align 8
  call void @llvm.dbg.declare(metadata !{i64* %25}, metadata !381) #6, !dbg !380
  call void @llvm.dbg.declare(metadata !{i64* %v.i}, metadata !382) #6, !dbg !383
  %31 = load i64** %24, align 8, !dbg !384
  %32 = load volatile i64* %31, align 8, !dbg !384
  store i64 %32, i64* %v.i, align 8, !dbg !384
  br label %33, !dbg !384

; <label>:33                                      ; preds = %46, %0
  %34 = load i64* %v.i, align 8, !dbg !386
  %35 = load i64* %25, align 8, !dbg !386
  %36 = add nsw i64 %34, %35, !dbg !386
  %37 = load i64* %v.i, align 8, !dbg !386
  %38 = load i64** %24, align 8, !dbg !386
  store i64 %36, i64* %21, align 8
  call void @llvm.dbg.declare(metadata !{i64* %21}, metadata !387) #6, !dbg !388
  store i64 %37, i64* %22, align 8
  call void @llvm.dbg.declare(metadata !{i64* %22}, metadata !389) #6, !dbg !388
  store i64* %38, i64** %23, align 8
  call void @llvm.dbg.declare(metadata !{i64** %23}, metadata !390) #6, !dbg !388
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i}, metadata !391) #6, !dbg !393
  %39 = load i64* %21, align 8, !dbg !394
  %40 = load i64** %23, align 8, !dbg !394
  %41 = load i64* %22, align 8, !dbg !394
  %42 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %39, i64* %40, i64 %41) #6, !dbg !394, !srcloc !395
  store i64 %42, i64* %prevVal.i.i, align 8, !dbg !394
  %43 = load i64* %prevVal.i.i, align 8, !dbg !396
  %44 = load i64* %v.i, align 8, !dbg !386
  %45 = icmp ne i64 %43, %44, !dbg !386
  br i1 %45, label %46, label %AtomicAdd.exit, !dbg !386

; <label>:46                                      ; preds = %33
  %47 = load i64** %24, align 8, !dbg !384
  %48 = load volatile i64* %47, align 8, !dbg !384
  store i64 %48, i64* %v.i, align 8, !dbg !384
  br label %33, !dbg !384

AtomicAdd.exit:                                   ; preds = %33
  %49 = load i64* %v.i, align 8, !dbg !397
  %50 = load i64* %25, align 8, !dbg !397
  %51 = add nsw i64 %49, %50, !dbg !397
  call void @llvm.dbg.declare(metadata !{i64* %wrSetOvf}, metadata !398), !dbg !399
  store i64 0, i64* %wrSetOvf, align 8, !dbg !399
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !400), !dbg !401
  %52 = load %struct._Thread** %26, align 8, !dbg !402
  %53 = getelementptr inbounds %struct._Thread* %52, i32 0, i32 17, !dbg !402
  store %struct._Log* %53, %struct._Log** %wr, align 8, !dbg !402
  %54 = load %struct._Log** %wr, align 8, !dbg !403
  %55 = getelementptr inbounds %struct._Log* %54, i32 0, i32 4, !dbg !403
  %56 = load i64* %55, align 8, !dbg !403
  %57 = load i64* %wrSetOvf, align 8, !dbg !403
  %58 = add nsw i64 %57, %56, !dbg !403
  store i64 %58, i64* %wrSetOvf, align 8, !dbg !403
  %59 = load i64* %wrSetOvf, align 8, !dbg !405
  store i64* @WriteOverflowTally, i64** %19, align 8
  call void @llvm.dbg.declare(metadata !{i64** %19}, metadata !379) #6, !dbg !406
  store i64 %59, i64* %20, align 8
  call void @llvm.dbg.declare(metadata !{i64* %20}, metadata !381) #6, !dbg !406
  call void @llvm.dbg.declare(metadata !{i64* %v.i2}, metadata !382) #6, !dbg !407
  %60 = load i64** %19, align 8, !dbg !408
  %61 = load volatile i64* %60, align 8, !dbg !408
  store i64 %61, i64* %v.i2, align 8, !dbg !408
  br label %62, !dbg !408

; <label>:62                                      ; preds = %75, %AtomicAdd.exit
  %63 = load i64* %v.i2, align 8, !dbg !409
  %64 = load i64* %20, align 8, !dbg !409
  %65 = add nsw i64 %63, %64, !dbg !409
  %66 = load i64* %v.i2, align 8, !dbg !409
  %67 = load i64** %19, align 8, !dbg !409
  store i64 %65, i64* %16, align 8
  call void @llvm.dbg.declare(metadata !{i64* %16}, metadata !387) #6, !dbg !410
  store i64 %66, i64* %17, align 8
  call void @llvm.dbg.declare(metadata !{i64* %17}, metadata !389) #6, !dbg !410
  store i64* %67, i64** %18, align 8
  call void @llvm.dbg.declare(metadata !{i64** %18}, metadata !390) #6, !dbg !410
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i1}, metadata !391) #6, !dbg !411
  %68 = load i64* %16, align 8, !dbg !412
  %69 = load i64** %18, align 8, !dbg !412
  %70 = load i64* %17, align 8, !dbg !412
  %71 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %68, i64* %69, i64 %70) #6, !dbg !412, !srcloc !395
  store i64 %71, i64* %prevVal.i.i1, align 8, !dbg !412
  %72 = load i64* %prevVal.i.i1, align 8, !dbg !413
  %73 = load i64* %v.i2, align 8, !dbg !409
  %74 = icmp ne i64 %72, %73, !dbg !409
  br i1 %74, label %75, label %AtomicAdd.exit3, !dbg !409

; <label>:75                                      ; preds = %62
  %76 = load i64** %19, align 8, !dbg !408
  %77 = load volatile i64* %76, align 8, !dbg !408
  store i64 %77, i64* %v.i2, align 8, !dbg !408
  br label %62, !dbg !408

AtomicAdd.exit3:                                  ; preds = %62
  %78 = load i64* %v.i2, align 8, !dbg !414
  %79 = load i64* %20, align 8, !dbg !414
  %80 = add nsw i64 %78, %79, !dbg !414
  %81 = load %struct._Thread** %26, align 8, !dbg !415
  %82 = getelementptr inbounds %struct._Thread* %81, i32 0, i32 18, !dbg !415
  %83 = getelementptr inbounds %struct._Log* %82, i32 0, i32 4, !dbg !415
  %84 = load i64* %83, align 8, !dbg !415
  store i64* @LocalOverflowTally, i64** %14, align 8
  call void @llvm.dbg.declare(metadata !{i64** %14}, metadata !379) #6, !dbg !416
  store i64 %84, i64* %15, align 8
  call void @llvm.dbg.declare(metadata !{i64* %15}, metadata !381) #6, !dbg !416
  call void @llvm.dbg.declare(metadata !{i64* %v.i5}, metadata !382) #6, !dbg !417
  %85 = load i64** %14, align 8, !dbg !418
  %86 = load volatile i64* %85, align 8, !dbg !418
  store i64 %86, i64* %v.i5, align 8, !dbg !418
  br label %87, !dbg !418

; <label>:87                                      ; preds = %100, %AtomicAdd.exit3
  %88 = load i64* %v.i5, align 8, !dbg !419
  %89 = load i64* %15, align 8, !dbg !419
  %90 = add nsw i64 %88, %89, !dbg !419
  %91 = load i64* %v.i5, align 8, !dbg !419
  %92 = load i64** %14, align 8, !dbg !419
  store i64 %90, i64* %11, align 8
  call void @llvm.dbg.declare(metadata !{i64* %11}, metadata !387) #6, !dbg !420
  store i64 %91, i64* %12, align 8
  call void @llvm.dbg.declare(metadata !{i64* %12}, metadata !389) #6, !dbg !420
  store i64* %92, i64** %13, align 8
  call void @llvm.dbg.declare(metadata !{i64** %13}, metadata !390) #6, !dbg !420
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i4}, metadata !391) #6, !dbg !421
  %93 = load i64* %11, align 8, !dbg !422
  %94 = load i64** %13, align 8, !dbg !422
  %95 = load i64* %12, align 8, !dbg !422
  %96 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %93, i64* %94, i64 %95) #6, !dbg !422, !srcloc !395
  store i64 %96, i64* %prevVal.i.i4, align 8, !dbg !422
  %97 = load i64* %prevVal.i.i4, align 8, !dbg !423
  %98 = load i64* %v.i5, align 8, !dbg !419
  %99 = icmp ne i64 %97, %98, !dbg !419
  br i1 %99, label %100, label %AtomicAdd.exit6, !dbg !419

; <label>:100                                     ; preds = %87
  %101 = load i64** %14, align 8, !dbg !418
  %102 = load volatile i64* %101, align 8, !dbg !418
  store i64 %102, i64* %v.i5, align 8, !dbg !418
  br label %87, !dbg !418

AtomicAdd.exit6:                                  ; preds = %87
  %103 = load i64* %v.i5, align 8, !dbg !424
  %104 = load i64* %15, align 8, !dbg !424
  %105 = add nsw i64 %103, %104, !dbg !424
  %106 = load %struct._Thread** %26, align 8, !dbg !425
  %107 = getelementptr inbounds %struct._Thread* %106, i32 0, i32 9, !dbg !425
  %108 = load i64* %107, align 8, !dbg !425
  store i64* @StartTally, i64** %9, align 8
  call void @llvm.dbg.declare(metadata !{i64** %9}, metadata !379) #6, !dbg !426
  store i64 %108, i64* %10, align 8
  call void @llvm.dbg.declare(metadata !{i64* %10}, metadata !381) #6, !dbg !426
  call void @llvm.dbg.declare(metadata !{i64* %v.i8}, metadata !382) #6, !dbg !427
  %109 = load i64** %9, align 8, !dbg !428
  %110 = load volatile i64* %109, align 8, !dbg !428
  store i64 %110, i64* %v.i8, align 8, !dbg !428
  br label %111, !dbg !428

; <label>:111                                     ; preds = %124, %AtomicAdd.exit6
  %112 = load i64* %v.i8, align 8, !dbg !429
  %113 = load i64* %10, align 8, !dbg !429
  %114 = add nsw i64 %112, %113, !dbg !429
  %115 = load i64* %v.i8, align 8, !dbg !429
  %116 = load i64** %9, align 8, !dbg !429
  store i64 %114, i64* %6, align 8
  call void @llvm.dbg.declare(metadata !{i64* %6}, metadata !387) #6, !dbg !430
  store i64 %115, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !389) #6, !dbg !430
  store i64* %116, i64** %8, align 8
  call void @llvm.dbg.declare(metadata !{i64** %8}, metadata !390) #6, !dbg !430
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i7}, metadata !391) #6, !dbg !431
  %117 = load i64* %6, align 8, !dbg !432
  %118 = load i64** %8, align 8, !dbg !432
  %119 = load i64* %7, align 8, !dbg !432
  %120 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %117, i64* %118, i64 %119) #6, !dbg !432, !srcloc !395
  store i64 %120, i64* %prevVal.i.i7, align 8, !dbg !432
  %121 = load i64* %prevVal.i.i7, align 8, !dbg !433
  %122 = load i64* %v.i8, align 8, !dbg !429
  %123 = icmp ne i64 %121, %122, !dbg !429
  br i1 %123, label %124, label %AtomicAdd.exit9, !dbg !429

; <label>:124                                     ; preds = %111
  %125 = load i64** %9, align 8, !dbg !428
  %126 = load volatile i64* %125, align 8, !dbg !428
  store i64 %126, i64* %v.i8, align 8, !dbg !428
  br label %111, !dbg !428

AtomicAdd.exit9:                                  ; preds = %111
  %127 = load i64* %v.i8, align 8, !dbg !434
  %128 = load i64* %10, align 8, !dbg !434
  %129 = add nsw i64 %127, %128, !dbg !434
  %130 = load %struct._Thread** %26, align 8, !dbg !435
  %131 = getelementptr inbounds %struct._Thread* %130, i32 0, i32 10, !dbg !435
  %132 = load i64* %131, align 8, !dbg !435
  store i64* @AbortTally, i64** %4, align 8
  call void @llvm.dbg.declare(metadata !{i64** %4}, metadata !379) #6, !dbg !436
  store i64 %132, i64* %5, align 8
  call void @llvm.dbg.declare(metadata !{i64* %5}, metadata !381) #6, !dbg !436
  call void @llvm.dbg.declare(metadata !{i64* %v.i11}, metadata !382) #6, !dbg !437
  %133 = load i64** %4, align 8, !dbg !438
  %134 = load volatile i64* %133, align 8, !dbg !438
  store i64 %134, i64* %v.i11, align 8, !dbg !438
  br label %135, !dbg !438

; <label>:135                                     ; preds = %148, %AtomicAdd.exit9
  %136 = load i64* %v.i11, align 8, !dbg !439
  %137 = load i64* %5, align 8, !dbg !439
  %138 = add nsw i64 %136, %137, !dbg !439
  %139 = load i64* %v.i11, align 8, !dbg !439
  %140 = load i64** %4, align 8, !dbg !439
  store i64 %138, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !387) #6, !dbg !440
  store i64 %139, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !389) #6, !dbg !440
  store i64* %140, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !390) #6, !dbg !440
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i10}, metadata !391) #6, !dbg !441
  %141 = load i64* %1, align 8, !dbg !442
  %142 = load i64** %3, align 8, !dbg !442
  %143 = load i64* %2, align 8, !dbg !442
  %144 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %141, i64* %142, i64 %143) #6, !dbg !442, !srcloc !395
  store i64 %144, i64* %prevVal.i.i10, align 8, !dbg !442
  %145 = load i64* %prevVal.i.i10, align 8, !dbg !443
  %146 = load i64* %v.i11, align 8, !dbg !439
  %147 = icmp ne i64 %145, %146, !dbg !439
  br i1 %147, label %148, label %AtomicAdd.exit12, !dbg !439

; <label>:148                                     ; preds = %135
  %149 = load i64** %4, align 8, !dbg !438
  %150 = load volatile i64* %149, align 8, !dbg !438
  store i64 %150, i64* %v.i11, align 8, !dbg !438
  br label %135, !dbg !438

AtomicAdd.exit12:                                 ; preds = %135
  %151 = load i64* %v.i11, align 8, !dbg !444
  %152 = load i64* %5, align 8, !dbg !444
  %153 = add nsw i64 %151, %152, !dbg !444
  %154 = load %struct._Thread** %26, align 8, !dbg !445
  %155 = getelementptr inbounds %struct._Thread* %154, i32 0, i32 14, !dbg !445
  %156 = load %struct.tmalloc** %155, align 8, !dbg !445
  call void @tmalloc_free(%struct.tmalloc* %156), !dbg !445
  %157 = load %struct._Thread** %26, align 8, !dbg !446
  %158 = getelementptr inbounds %struct._Thread* %157, i32 0, i32 15, !dbg !446
  %159 = load %struct.tmalloc** %158, align 8, !dbg !446
  call void @tmalloc_free(%struct.tmalloc* %159), !dbg !446
  %160 = load %struct._Thread** %26, align 8, !dbg !447
  %161 = getelementptr inbounds %struct._Thread* %160, i32 0, i32 16, !dbg !447
  call void @FreeList(%struct._Log* %161, i64 8192), !dbg !447
  %162 = load %struct._Thread** %26, align 8, !dbg !448
  %163 = getelementptr inbounds %struct._Thread* %162, i32 0, i32 17, !dbg !448
  call void @FreeList(%struct._Log* %163, i64 1024), !dbg !448
  %164 = load %struct._Thread** %26, align 8, !dbg !449
  %165 = getelementptr inbounds %struct._Thread* %164, i32 0, i32 18, !dbg !449
  call void @FreeList(%struct._Log* %165, i64 1024), !dbg !449
  %166 = load %struct._Thread** %26, align 8, !dbg !450
  %167 = bitcast %struct._Thread* %166 to i8*, !dbg !450
  call void @free(i8* %167) #6, !dbg !450
  ret void, !dbg !451
}

declare void @tmalloc_free(%struct.tmalloc*) #4

; Function Attrs: nounwind uwtable
define void @TxInitThread(%struct._Thread* %t, i64 %id) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %ap.i7 = alloca %struct._AVPair*, align 8
  %List.i8 = alloca %struct._AVPair*, align 8
  %Tail.i9 = alloca %struct._AVPair*, align 8
  %i.i10 = alloca i64, align 8
  %e.i11 = alloca %struct._AVPair*, align 8
  %3 = alloca i64, align 8
  %4 = alloca %struct._Thread*, align 8
  %ap.i1 = alloca %struct._AVPair*, align 8
  %List.i2 = alloca %struct._AVPair*, align 8
  %Tail.i3 = alloca %struct._AVPair*, align 8
  %i.i4 = alloca i64, align 8
  %e.i5 = alloca %struct._AVPair*, align 8
  %5 = alloca i64, align 8
  %6 = alloca %struct._Thread*, align 8
  %ap.i = alloca %struct._AVPair*, align 8
  %List.i = alloca %struct._AVPair*, align 8
  %Tail.i = alloca %struct._AVPair*, align 8
  %i.i = alloca i64, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %7 = alloca %struct._Thread*, align 8
  %8 = alloca i64, align 8
  store %struct._Thread* %t, %struct._Thread** %7, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %7}, metadata !452), !dbg !453
  store i64 %id, i64* %8, align 8
  call void @llvm.dbg.declare(metadata !{i64* %8}, metadata !454), !dbg !453
  %9 = load i32* @global_key_self, align 4, !dbg !455
  %10 = load %struct._Thread** %7, align 8, !dbg !455
  %11 = bitcast %struct._Thread* %10 to i8*, !dbg !455
  %12 = call i32 @pthread_setspecific(i32 %9, i8* %11) #6, !dbg !455
  %13 = load %struct._Thread** %7, align 8, !dbg !456
  %14 = bitcast %struct._Thread* %13 to i8*, !dbg !456
  call void @llvm.memset.p0i8.i64(i8* %14, i8 0, i64 280, i32 8, i1 false), !dbg !456
  %15 = load i64* %8, align 8, !dbg !457
  %16 = load %struct._Thread** %7, align 8, !dbg !457
  %17 = getelementptr inbounds %struct._Thread* %16, i32 0, i32 0, !dbg !457
  store i64 %15, i64* %17, align 8, !dbg !457
  %18 = load i64* %8, align 8, !dbg !458
  %19 = add nsw i64 %18, 1, !dbg !458
  %20 = load %struct._Thread** %7, align 8, !dbg !458
  %21 = getelementptr inbounds %struct._Thread* %20, i32 0, i32 11, !dbg !458
  store i64 %19, i64* %21, align 8, !dbg !458
  %22 = load %struct._Thread** %7, align 8, !dbg !459
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 11, !dbg !459
  %24 = load i64* %23, align 8, !dbg !459
  %25 = load %struct._Thread** %7, align 8, !dbg !459
  %26 = getelementptr inbounds %struct._Thread* %25, i32 0, i32 12, !dbg !459
  %27 = getelementptr inbounds [1 x i64]* %26, i32 0, i64 0, !dbg !459
  store i64 %24, i64* %27, align 8, !dbg !459
  %28 = load %struct._Thread** %7, align 8, !dbg !460
  store i64 1024, i64* %5, align 8
  call void @llvm.dbg.declare(metadata !{i64* %5}, metadata !461) #6, !dbg !462
  store %struct._Thread* %28, %struct._Thread** %6, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %6}, metadata !463) #6, !dbg !462
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %ap.i}, metadata !464) #6, !dbg !465
  %29 = load i64* %5, align 8, !dbg !465
  %30 = mul i64 72, %29, !dbg !465
  %31 = add i64 %30, 64, !dbg !465
  %32 = call noalias i8* @malloc(i64 %31) #6, !dbg !465
  %33 = bitcast i8* %32 to %struct._AVPair*, !dbg !465
  store %struct._AVPair* %33, %struct._AVPair** %ap.i, align 8, !dbg !465
  %34 = load %struct._AVPair** %ap.i, align 8, !dbg !466
  %35 = icmp ne %struct._AVPair* %34, null, !dbg !466
  br i1 %35, label %36, label %42, !dbg !466

; <label>:36                                      ; preds = %0
  %37 = load %struct._AVPair** %ap.i, align 8, !dbg !467
  %38 = bitcast %struct._AVPair* %37 to i8*, !dbg !467
  %39 = load i64* %5, align 8, !dbg !467
  %40 = mul i64 72, %39, !dbg !467
  call void @llvm.memset.p0i8.i64(i8* %38, i8 0, i64 %40, i32 8, i1 false) #6, !dbg !467
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %List.i}, metadata !468) #6, !dbg !469
  %41 = load %struct._AVPair** %ap.i, align 8, !dbg !469
  store %struct._AVPair* %41, %struct._AVPair** %List.i, align 8, !dbg !469
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %Tail.i}, metadata !470) #6, !dbg !471
  store %struct._AVPair* null, %struct._AVPair** %Tail.i, align 8, !dbg !471
  call void @llvm.dbg.declare(metadata !{i64* %i.i}, metadata !472) #6, !dbg !473
  store i64 0, i64* %i.i, align 8, !dbg !474
  br label %43, !dbg !474

; <label>:42                                      ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([3 x i8]* @.str8, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 591, i8* getelementptr inbounds ([33 x i8]* @__PRETTY_FUNCTION__.MakeList, i32 0, i32 0)) #7, !dbg !466
  unreachable, !dbg !466

; <label>:43                                      ; preds = %47, %36
  %44 = load i64* %i.i, align 8, !dbg !474
  %45 = load i64* %5, align 8, !dbg !474
  %46 = icmp slt i64 %44, %45, !dbg !474
  br i1 %46, label %47, label %MakeList.exit, !dbg !474

; <label>:47                                      ; preds = %43
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !476) #6, !dbg !478
  %48 = load %struct._AVPair** %ap.i, align 8, !dbg !478
  %49 = getelementptr inbounds %struct._AVPair* %48, i32 1, !dbg !478
  store %struct._AVPair* %49, %struct._AVPair** %ap.i, align 8, !dbg !478
  store %struct._AVPair* %48, %struct._AVPair** %e.i, align 8, !dbg !478
  %50 = load %struct._AVPair** %ap.i, align 8, !dbg !479
  %51 = load %struct._AVPair** %e.i, align 8, !dbg !479
  %52 = getelementptr inbounds %struct._AVPair* %51, i32 0, i32 0, !dbg !479
  store %struct._AVPair* %50, %struct._AVPair** %52, align 8, !dbg !479
  %53 = load %struct._AVPair** %Tail.i, align 8, !dbg !480
  %54 = load %struct._AVPair** %e.i, align 8, !dbg !480
  %55 = getelementptr inbounds %struct._AVPair* %54, i32 0, i32 1, !dbg !480
  store %struct._AVPair* %53, %struct._AVPair** %55, align 8, !dbg !480
  %56 = load %struct._Thread** %6, align 8, !dbg !481
  %57 = load %struct._AVPair** %e.i, align 8, !dbg !481
  %58 = getelementptr inbounds %struct._AVPair* %57, i32 0, i32 7, !dbg !481
  store %struct._Thread* %56, %struct._Thread** %58, align 8, !dbg !481
  %59 = load i64* %i.i, align 8, !dbg !482
  %60 = load %struct._AVPair** %e.i, align 8, !dbg !482
  %61 = getelementptr inbounds %struct._AVPair* %60, i32 0, i32 8, !dbg !482
  store i64 %59, i64* %61, align 8, !dbg !482
  %62 = load %struct._AVPair** %e.i, align 8, !dbg !483
  store %struct._AVPair* %62, %struct._AVPair** %Tail.i, align 8, !dbg !483
  %63 = load i64* %i.i, align 8, !dbg !474
  %64 = add nsw i64 %63, 1, !dbg !474
  store i64 %64, i64* %i.i, align 8, !dbg !474
  br label %43, !dbg !474

MakeList.exit:                                    ; preds = %43
  %65 = load %struct._AVPair** %Tail.i, align 8, !dbg !484
  %66 = getelementptr inbounds %struct._AVPair* %65, i32 0, i32 0, !dbg !484
  store %struct._AVPair* null, %struct._AVPair** %66, align 8, !dbg !484
  %67 = load %struct._AVPair** %List.i, align 8, !dbg !485
  %68 = load %struct._Thread** %7, align 8, !dbg !460
  %69 = getelementptr inbounds %struct._Thread* %68, i32 0, i32 17, !dbg !460
  %70 = getelementptr inbounds %struct._Log* %69, i32 0, i32 0, !dbg !460
  store %struct._AVPair* %67, %struct._AVPair** %70, align 8, !dbg !460
  %71 = load %struct._Thread** %7, align 8, !dbg !486
  %72 = getelementptr inbounds %struct._Thread* %71, i32 0, i32 17, !dbg !486
  %73 = getelementptr inbounds %struct._Log* %72, i32 0, i32 0, !dbg !486
  %74 = load %struct._AVPair** %73, align 8, !dbg !486
  %75 = load %struct._Thread** %7, align 8, !dbg !486
  %76 = getelementptr inbounds %struct._Thread* %75, i32 0, i32 17, !dbg !486
  %77 = getelementptr inbounds %struct._Log* %76, i32 0, i32 1, !dbg !486
  store %struct._AVPair* %74, %struct._AVPair** %77, align 8, !dbg !486
  %78 = load %struct._Thread** %7, align 8, !dbg !487
  store i64 8192, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !461) #6, !dbg !488
  store %struct._Thread* %78, %struct._Thread** %4, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %4}, metadata !463) #6, !dbg !488
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %ap.i1}, metadata !464) #6, !dbg !489
  %79 = load i64* %3, align 8, !dbg !489
  %80 = mul i64 72, %79, !dbg !489
  %81 = add i64 %80, 64, !dbg !489
  %82 = call noalias i8* @malloc(i64 %81) #6, !dbg !489
  %83 = bitcast i8* %82 to %struct._AVPair*, !dbg !489
  store %struct._AVPair* %83, %struct._AVPair** %ap.i1, align 8, !dbg !489
  %84 = load %struct._AVPair** %ap.i1, align 8, !dbg !490
  %85 = icmp ne %struct._AVPair* %84, null, !dbg !490
  br i1 %85, label %86, label %92, !dbg !490

; <label>:86                                      ; preds = %MakeList.exit
  %87 = load %struct._AVPair** %ap.i1, align 8, !dbg !491
  %88 = bitcast %struct._AVPair* %87 to i8*, !dbg !491
  %89 = load i64* %3, align 8, !dbg !491
  %90 = mul i64 72, %89, !dbg !491
  call void @llvm.memset.p0i8.i64(i8* %88, i8 0, i64 %90, i32 8, i1 false) #6, !dbg !491
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %List.i2}, metadata !468) #6, !dbg !492
  %91 = load %struct._AVPair** %ap.i1, align 8, !dbg !492
  store %struct._AVPair* %91, %struct._AVPair** %List.i2, align 8, !dbg !492
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %Tail.i3}, metadata !470) #6, !dbg !493
  store %struct._AVPair* null, %struct._AVPair** %Tail.i3, align 8, !dbg !493
  call void @llvm.dbg.declare(metadata !{i64* %i.i4}, metadata !472) #6, !dbg !494
  store i64 0, i64* %i.i4, align 8, !dbg !495
  br label %93, !dbg !495

; <label>:92                                      ; preds = %MakeList.exit
  call void @__assert_fail(i8* getelementptr inbounds ([3 x i8]* @.str8, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 591, i8* getelementptr inbounds ([33 x i8]* @__PRETTY_FUNCTION__.MakeList, i32 0, i32 0)) #7, !dbg !490
  unreachable, !dbg !490

; <label>:93                                      ; preds = %97, %86
  %94 = load i64* %i.i4, align 8, !dbg !495
  %95 = load i64* %3, align 8, !dbg !495
  %96 = icmp slt i64 %94, %95, !dbg !495
  br i1 %96, label %97, label %MakeList.exit6, !dbg !495

; <label>:97                                      ; preds = %93
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i5}, metadata !476) #6, !dbg !496
  %98 = load %struct._AVPair** %ap.i1, align 8, !dbg !496
  %99 = getelementptr inbounds %struct._AVPair* %98, i32 1, !dbg !496
  store %struct._AVPair* %99, %struct._AVPair** %ap.i1, align 8, !dbg !496
  store %struct._AVPair* %98, %struct._AVPair** %e.i5, align 8, !dbg !496
  %100 = load %struct._AVPair** %ap.i1, align 8, !dbg !497
  %101 = load %struct._AVPair** %e.i5, align 8, !dbg !497
  %102 = getelementptr inbounds %struct._AVPair* %101, i32 0, i32 0, !dbg !497
  store %struct._AVPair* %100, %struct._AVPair** %102, align 8, !dbg !497
  %103 = load %struct._AVPair** %Tail.i3, align 8, !dbg !498
  %104 = load %struct._AVPair** %e.i5, align 8, !dbg !498
  %105 = getelementptr inbounds %struct._AVPair* %104, i32 0, i32 1, !dbg !498
  store %struct._AVPair* %103, %struct._AVPair** %105, align 8, !dbg !498
  %106 = load %struct._Thread** %4, align 8, !dbg !499
  %107 = load %struct._AVPair** %e.i5, align 8, !dbg !499
  %108 = getelementptr inbounds %struct._AVPair* %107, i32 0, i32 7, !dbg !499
  store %struct._Thread* %106, %struct._Thread** %108, align 8, !dbg !499
  %109 = load i64* %i.i4, align 8, !dbg !500
  %110 = load %struct._AVPair** %e.i5, align 8, !dbg !500
  %111 = getelementptr inbounds %struct._AVPair* %110, i32 0, i32 8, !dbg !500
  store i64 %109, i64* %111, align 8, !dbg !500
  %112 = load %struct._AVPair** %e.i5, align 8, !dbg !501
  store %struct._AVPair* %112, %struct._AVPair** %Tail.i3, align 8, !dbg !501
  %113 = load i64* %i.i4, align 8, !dbg !495
  %114 = add nsw i64 %113, 1, !dbg !495
  store i64 %114, i64* %i.i4, align 8, !dbg !495
  br label %93, !dbg !495

MakeList.exit6:                                   ; preds = %93
  %115 = load %struct._AVPair** %Tail.i3, align 8, !dbg !502
  %116 = getelementptr inbounds %struct._AVPair* %115, i32 0, i32 0, !dbg !502
  store %struct._AVPair* null, %struct._AVPair** %116, align 8, !dbg !502
  %117 = load %struct._AVPair** %List.i2, align 8, !dbg !503
  %118 = load %struct._Thread** %7, align 8, !dbg !487
  %119 = getelementptr inbounds %struct._Thread* %118, i32 0, i32 16, !dbg !487
  %120 = getelementptr inbounds %struct._Log* %119, i32 0, i32 0, !dbg !487
  store %struct._AVPair* %117, %struct._AVPair** %120, align 8, !dbg !487
  %121 = load %struct._Thread** %7, align 8, !dbg !504
  %122 = getelementptr inbounds %struct._Thread* %121, i32 0, i32 16, !dbg !504
  %123 = getelementptr inbounds %struct._Log* %122, i32 0, i32 0, !dbg !504
  %124 = load %struct._AVPair** %123, align 8, !dbg !504
  %125 = load %struct._Thread** %7, align 8, !dbg !504
  %126 = getelementptr inbounds %struct._Thread* %125, i32 0, i32 16, !dbg !504
  %127 = getelementptr inbounds %struct._Log* %126, i32 0, i32 1, !dbg !504
  store %struct._AVPair* %124, %struct._AVPair** %127, align 8, !dbg !504
  %128 = load %struct._Thread** %7, align 8, !dbg !505
  store i64 1024, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !461) #6, !dbg !506
  store %struct._Thread* %128, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !463) #6, !dbg !506
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %ap.i7}, metadata !464) #6, !dbg !507
  %129 = load i64* %1, align 8, !dbg !507
  %130 = mul i64 72, %129, !dbg !507
  %131 = add i64 %130, 64, !dbg !507
  %132 = call noalias i8* @malloc(i64 %131) #6, !dbg !507
  %133 = bitcast i8* %132 to %struct._AVPair*, !dbg !507
  store %struct._AVPair* %133, %struct._AVPair** %ap.i7, align 8, !dbg !507
  %134 = load %struct._AVPair** %ap.i7, align 8, !dbg !508
  %135 = icmp ne %struct._AVPair* %134, null, !dbg !508
  br i1 %135, label %136, label %142, !dbg !508

; <label>:136                                     ; preds = %MakeList.exit6
  %137 = load %struct._AVPair** %ap.i7, align 8, !dbg !509
  %138 = bitcast %struct._AVPair* %137 to i8*, !dbg !509
  %139 = load i64* %1, align 8, !dbg !509
  %140 = mul i64 72, %139, !dbg !509
  call void @llvm.memset.p0i8.i64(i8* %138, i8 0, i64 %140, i32 8, i1 false) #6, !dbg !509
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %List.i8}, metadata !468) #6, !dbg !510
  %141 = load %struct._AVPair** %ap.i7, align 8, !dbg !510
  store %struct._AVPair* %141, %struct._AVPair** %List.i8, align 8, !dbg !510
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %Tail.i9}, metadata !470) #6, !dbg !511
  store %struct._AVPair* null, %struct._AVPair** %Tail.i9, align 8, !dbg !511
  call void @llvm.dbg.declare(metadata !{i64* %i.i10}, metadata !472) #6, !dbg !512
  store i64 0, i64* %i.i10, align 8, !dbg !513
  br label %143, !dbg !513

; <label>:142                                     ; preds = %MakeList.exit6
  call void @__assert_fail(i8* getelementptr inbounds ([3 x i8]* @.str8, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 591, i8* getelementptr inbounds ([33 x i8]* @__PRETTY_FUNCTION__.MakeList, i32 0, i32 0)) #7, !dbg !508
  unreachable, !dbg !508

; <label>:143                                     ; preds = %147, %136
  %144 = load i64* %i.i10, align 8, !dbg !513
  %145 = load i64* %1, align 8, !dbg !513
  %146 = icmp slt i64 %144, %145, !dbg !513
  br i1 %146, label %147, label %MakeList.exit12, !dbg !513

; <label>:147                                     ; preds = %143
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i11}, metadata !476) #6, !dbg !514
  %148 = load %struct._AVPair** %ap.i7, align 8, !dbg !514
  %149 = getelementptr inbounds %struct._AVPair* %148, i32 1, !dbg !514
  store %struct._AVPair* %149, %struct._AVPair** %ap.i7, align 8, !dbg !514
  store %struct._AVPair* %148, %struct._AVPair** %e.i11, align 8, !dbg !514
  %150 = load %struct._AVPair** %ap.i7, align 8, !dbg !515
  %151 = load %struct._AVPair** %e.i11, align 8, !dbg !515
  %152 = getelementptr inbounds %struct._AVPair* %151, i32 0, i32 0, !dbg !515
  store %struct._AVPair* %150, %struct._AVPair** %152, align 8, !dbg !515
  %153 = load %struct._AVPair** %Tail.i9, align 8, !dbg !516
  %154 = load %struct._AVPair** %e.i11, align 8, !dbg !516
  %155 = getelementptr inbounds %struct._AVPair* %154, i32 0, i32 1, !dbg !516
  store %struct._AVPair* %153, %struct._AVPair** %155, align 8, !dbg !516
  %156 = load %struct._Thread** %2, align 8, !dbg !517
  %157 = load %struct._AVPair** %e.i11, align 8, !dbg !517
  %158 = getelementptr inbounds %struct._AVPair* %157, i32 0, i32 7, !dbg !517
  store %struct._Thread* %156, %struct._Thread** %158, align 8, !dbg !517
  %159 = load i64* %i.i10, align 8, !dbg !518
  %160 = load %struct._AVPair** %e.i11, align 8, !dbg !518
  %161 = getelementptr inbounds %struct._AVPair* %160, i32 0, i32 8, !dbg !518
  store i64 %159, i64* %161, align 8, !dbg !518
  %162 = load %struct._AVPair** %e.i11, align 8, !dbg !519
  store %struct._AVPair* %162, %struct._AVPair** %Tail.i9, align 8, !dbg !519
  %163 = load i64* %i.i10, align 8, !dbg !513
  %164 = add nsw i64 %163, 1, !dbg !513
  store i64 %164, i64* %i.i10, align 8, !dbg !513
  br label %143, !dbg !513

MakeList.exit12:                                  ; preds = %143
  %165 = load %struct._AVPair** %Tail.i9, align 8, !dbg !520
  %166 = getelementptr inbounds %struct._AVPair* %165, i32 0, i32 0, !dbg !520
  store %struct._AVPair* null, %struct._AVPair** %166, align 8, !dbg !520
  %167 = load %struct._AVPair** %List.i8, align 8, !dbg !521
  %168 = load %struct._Thread** %7, align 8, !dbg !505
  %169 = getelementptr inbounds %struct._Thread* %168, i32 0, i32 18, !dbg !505
  %170 = getelementptr inbounds %struct._Log* %169, i32 0, i32 0, !dbg !505
  store %struct._AVPair* %167, %struct._AVPair** %170, align 8, !dbg !505
  %171 = load %struct._Thread** %7, align 8, !dbg !522
  %172 = getelementptr inbounds %struct._Thread* %171, i32 0, i32 18, !dbg !522
  %173 = getelementptr inbounds %struct._Log* %172, i32 0, i32 0, !dbg !522
  %174 = load %struct._AVPair** %173, align 8, !dbg !522
  %175 = load %struct._Thread** %7, align 8, !dbg !522
  %176 = getelementptr inbounds %struct._Thread* %175, i32 0, i32 18, !dbg !522
  %177 = getelementptr inbounds %struct._Log* %176, i32 0, i32 1, !dbg !522
  store %struct._AVPair* %174, %struct._AVPair** %177, align 8, !dbg !522
  %178 = call %struct.tmalloc* @tmalloc_alloc(i64 1), !dbg !523
  %179 = load %struct._Thread** %7, align 8, !dbg !523
  %180 = getelementptr inbounds %struct._Thread* %179, i32 0, i32 14, !dbg !523
  store %struct.tmalloc* %178, %struct.tmalloc** %180, align 8, !dbg !523
  %181 = load %struct._Thread** %7, align 8, !dbg !524
  %182 = getelementptr inbounds %struct._Thread* %181, i32 0, i32 14, !dbg !524
  %183 = load %struct.tmalloc** %182, align 8, !dbg !524
  %184 = icmp ne %struct.tmalloc* %183, null, !dbg !524
  br i1 %184, label %185, label %186, !dbg !524

; <label>:185                                     ; preds = %MakeList.exit12
  br label %188, !dbg !524

; <label>:186                                     ; preds = %MakeList.exit12
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8]* @.str5, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1151, i8* getelementptr inbounds ([34 x i8]* @__PRETTY_FUNCTION__.TxInitThread, i32 0, i32 0)) #7, !dbg !524
  unreachable, !dbg !524
                                                  ; No predecessors!
  br label %188, !dbg !524

; <label>:188                                     ; preds = %187, %185
  %189 = call %struct.tmalloc* @tmalloc_alloc(i64 1), !dbg !525
  %190 = load %struct._Thread** %7, align 8, !dbg !525
  %191 = getelementptr inbounds %struct._Thread* %190, i32 0, i32 15, !dbg !525
  store %struct.tmalloc* %189, %struct.tmalloc** %191, align 8, !dbg !525
  %192 = load %struct._Thread** %7, align 8, !dbg !526
  %193 = getelementptr inbounds %struct._Thread* %192, i32 0, i32 15, !dbg !526
  %194 = load %struct.tmalloc** %193, align 8, !dbg !526
  %195 = icmp ne %struct.tmalloc* %194, null, !dbg !526
  br i1 %195, label %196, label %197, !dbg !526

; <label>:196                                     ; preds = %188
  br label %199, !dbg !526

; <label>:197                                     ; preds = %188
  call void @__assert_fail(i8* getelementptr inbounds ([11 x i8]* @.str6, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1153, i8* getelementptr inbounds ([34 x i8]* @__PRETTY_FUNCTION__.TxInitThread, i32 0, i32 0)) #7, !dbg !526
  unreachable, !dbg !526
                                                  ; No predecessors!
  br label %199, !dbg !526

; <label>:199                                     ; preds = %198, %196
  ret void, !dbg !527
}

; Function Attrs: nounwind
declare i32 @pthread_setspecific(i32, i8*) #3

; Function Attrs: nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #6

declare %struct.tmalloc* @tmalloc_alloc(i64) #4

; Function Attrs: nounwind uwtable
define void @TxAbort(%struct._Thread* %Self) #0 {
  %1 = alloca %struct._Log*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Thread*, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %x.i.i.i = alloca i64, align 8
  %5 = alloca %struct._Thread*, align 8
  %6 = alloca %struct._Thread*, align 8
  %7 = alloca i64, align 8
  %stall.i = alloca i64, align 8
  %i.i = alloca i64, align 8
  %8 = alloca %struct._Thread*, align 8
  %wr.i = alloca %struct._Log*, align 8
  %p.i = alloca %struct._AVPair*, align 8
  %End.i = alloca %struct._AVPair*, align 8
  %9 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %9, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %9}, metadata !528), !dbg !529
  %10 = load %struct._Thread** %9, align 8, !dbg !530
  %11 = getelementptr inbounds %struct._Thread* %10, i32 0, i32 1, !dbg !530
  store volatile i64 5, i64* %11, align 8, !dbg !530
  %12 = load %struct._Thread** %9, align 8, !dbg !531
  %13 = getelementptr inbounds %struct._Thread* %12, i32 0, i32 2, !dbg !531
  %14 = load volatile i64* %13, align 8, !dbg !531
  %15 = icmp ne i64 %14, 0, !dbg !531
  br i1 %15, label %16, label %52, !dbg !531

; <label>:16                                      ; preds = %0
  %17 = load %struct._Thread** %9, align 8, !dbg !532
  store %struct._Thread* %17, %struct._Thread** %8, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %8}, metadata !534), !dbg !535
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr.i}, metadata !536), !dbg !537
  %18 = load %struct._Thread** %8, align 8, !dbg !537
  %19 = getelementptr inbounds %struct._Thread* %18, i32 0, i32 17, !dbg !537
  store %struct._Log* %19, %struct._Log** %wr.i, align 8, !dbg !537
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i}, metadata !538), !dbg !540
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i}, metadata !541), !dbg !543
  %20 = load %struct._Log** %wr.i, align 8, !dbg !543
  %21 = getelementptr inbounds %struct._Log* %20, i32 0, i32 1, !dbg !543
  %22 = load %struct._AVPair** %21, align 8, !dbg !543
  store %struct._AVPair* %22, %struct._AVPair** %End.i, align 8, !dbg !543
  %23 = load %struct._Log** %wr.i, align 8, !dbg !544
  %24 = getelementptr inbounds %struct._Log* %23, i32 0, i32 0, !dbg !544
  %25 = load %struct._AVPair** %24, align 8, !dbg !544
  store %struct._AVPair* %25, %struct._AVPair** %p.i, align 8, !dbg !544
  br label %26, !dbg !544

; <label>:26                                      ; preds = %46, %16
  %27 = load %struct._AVPair** %p.i, align 8, !dbg !544
  %28 = load %struct._AVPair** %End.i, align 8, !dbg !544
  %29 = icmp ne %struct._AVPair* %27, %28, !dbg !544
  br i1 %29, label %30, label %RestoreLocks.exit, !dbg !544

; <label>:30                                      ; preds = %26
  %31 = load %struct._AVPair** %p.i, align 8, !dbg !546
  %32 = getelementptr inbounds %struct._AVPair* %31, i32 0, i32 6, !dbg !546
  %33 = load i8* %32, align 1, !dbg !546
  %34 = zext i8 %33 to i32, !dbg !546
  %35 = icmp eq i32 %34, 0, !dbg !546
  br i1 %35, label %36, label %37, !dbg !546

; <label>:36                                      ; preds = %30
  br label %46, !dbg !548

; <label>:37                                      ; preds = %30
  %38 = load %struct._AVPair** %p.i, align 8, !dbg !550
  %39 = getelementptr inbounds %struct._AVPair* %38, i32 0, i32 6, !dbg !550
  store i8 0, i8* %39, align 1, !dbg !550
  %40 = load %struct._AVPair** %p.i, align 8, !dbg !551
  %41 = getelementptr inbounds %struct._AVPair* %40, i32 0, i32 5, !dbg !551
  %42 = load i64* %41, align 8, !dbg !551
  %43 = load %struct._AVPair** %p.i, align 8, !dbg !551
  %44 = getelementptr inbounds %struct._AVPair* %43, i32 0, i32 4, !dbg !551
  %45 = load i64** %44, align 8, !dbg !551
  store volatile i64 %42, i64* %45, align 8, !dbg !551
  br label %46, !dbg !552

; <label>:46                                      ; preds = %37, %36
  %47 = load %struct._AVPair** %p.i, align 8, !dbg !544
  %48 = getelementptr inbounds %struct._AVPair* %47, i32 0, i32 0, !dbg !544
  %49 = load %struct._AVPair** %48, align 8, !dbg !544
  store %struct._AVPair* %49, %struct._AVPair** %p.i, align 8, !dbg !544
  br label %26, !dbg !544

RestoreLocks.exit:                                ; preds = %26
  %50 = load %struct._Thread** %8, align 8, !dbg !553
  %51 = getelementptr inbounds %struct._Thread* %50, i32 0, i32 2, !dbg !553
  store volatile i64 0, i64* %51, align 8, !dbg !553
  br label %52, !dbg !554

; <label>:52                                      ; preds = %RestoreLocks.exit, %0
  %53 = load %struct._Thread** %9, align 8, !dbg !555
  %54 = getelementptr inbounds %struct._Thread* %53, i32 0, i32 18, !dbg !555
  %55 = getelementptr inbounds %struct._Log* %54, i32 0, i32 1, !dbg !555
  %56 = load %struct._AVPair** %55, align 8, !dbg !555
  %57 = load %struct._Thread** %9, align 8, !dbg !555
  %58 = getelementptr inbounds %struct._Thread* %57, i32 0, i32 18, !dbg !555
  %59 = getelementptr inbounds %struct._Log* %58, i32 0, i32 0, !dbg !555
  %60 = load %struct._AVPair** %59, align 8, !dbg !555
  %61 = icmp ne %struct._AVPair* %56, %60, !dbg !555
  br i1 %61, label %62, label %81, !dbg !555

; <label>:62                                      ; preds = %52
  %63 = load %struct._Thread** %9, align 8, !dbg !556
  %64 = getelementptr inbounds %struct._Thread* %63, i32 0, i32 18, !dbg !556
  store %struct._Log* %64, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !558), !dbg !559
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !560), !dbg !561
  %65 = load %struct._Log** %1, align 8, !dbg !562
  %66 = getelementptr inbounds %struct._Log* %65, i32 0, i32 2, !dbg !562
  %67 = load %struct._AVPair** %66, align 8, !dbg !562
  store %struct._AVPair* %67, %struct._AVPair** %e.i, align 8, !dbg !562
  br label %68, !dbg !562

; <label>:68                                      ; preds = %71, %62
  %69 = load %struct._AVPair** %e.i, align 8, !dbg !562
  %70 = icmp ne %struct._AVPair* %69, null, !dbg !562
  br i1 %70, label %71, label %WriteBackReverse.exit, !dbg !562

; <label>:71                                      ; preds = %68
  %72 = load %struct._AVPair** %e.i, align 8, !dbg !564
  %73 = getelementptr inbounds %struct._AVPair* %72, i32 0, i32 3, !dbg !564
  %74 = load i64* %73, align 8, !dbg !564
  %75 = load %struct._AVPair** %e.i, align 8, !dbg !564
  %76 = getelementptr inbounds %struct._AVPair* %75, i32 0, i32 2, !dbg !564
  %77 = load i64** %76, align 8, !dbg !564
  store volatile i64 %74, i64* %77, align 8, !dbg !564
  %78 = load %struct._AVPair** %e.i, align 8, !dbg !562
  %79 = getelementptr inbounds %struct._AVPair* %78, i32 0, i32 1, !dbg !562
  %80 = load %struct._AVPair** %79, align 8, !dbg !562
  store %struct._AVPair* %80, %struct._AVPair** %e.i, align 8, !dbg !562
  br label %68, !dbg !562

WriteBackReverse.exit:                            ; preds = %68
  br label %81, !dbg !566

; <label>:81                                      ; preds = %WriteBackReverse.exit, %52
  %82 = load %struct._Thread** %9, align 8, !dbg !567
  %83 = getelementptr inbounds %struct._Thread* %82, i32 0, i32 3, !dbg !567
  %84 = load volatile i64* %83, align 8, !dbg !567
  %85 = add nsw i64 %84, 1, !dbg !567
  store volatile i64 %85, i64* %83, align 8, !dbg !567
  %86 = load %struct._Thread** %9, align 8, !dbg !568
  %87 = getelementptr inbounds %struct._Thread* %86, i32 0, i32 10, !dbg !568
  %88 = load i64* %87, align 8, !dbg !568
  %89 = add nsw i64 %88, 1, !dbg !568
  store i64 %89, i64* %87, align 8, !dbg !568
  %90 = load %struct._Thread** %9, align 8, !dbg !569
  store %struct._Thread* %90, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !570), !dbg !571
  %91 = icmp ne i64 0, 0, !dbg !569
  br i1 %91, label %92, label %93, !dbg !569

; <label>:92                                      ; preds = %81
  br label %141, !dbg !572

; <label>:93                                      ; preds = %81
  %94 = load %struct._Thread** %9, align 8, !dbg !574
  %95 = getelementptr inbounds %struct._Thread* %94, i32 0, i32 3, !dbg !574
  %96 = load volatile i64* %95, align 8, !dbg !574
  %97 = icmp sgt i64 %96, 3, !dbg !574
  br i1 %97, label %98, label %140, !dbg !574

; <label>:98                                      ; preds = %93
  %99 = load %struct._Thread** %9, align 8, !dbg !575
  %100 = load %struct._Thread** %9, align 8, !dbg !575
  %101 = getelementptr inbounds %struct._Thread* %100, i32 0, i32 3, !dbg !575
  %102 = load volatile i64* %101, align 8, !dbg !575
  store %struct._Thread* %99, %struct._Thread** %6, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %6}, metadata !577), !dbg !578
  store i64 %102, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !579), !dbg !578
  call void @llvm.dbg.declare(metadata !{i64* %stall.i}, metadata !580), !dbg !581
  %103 = load %struct._Thread** %6, align 8, !dbg !582
  store %struct._Thread* %103, %struct._Thread** %5, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %5}, metadata !583), !dbg !584
  %104 = load %struct._Thread** %5, align 8, !dbg !585
  %105 = getelementptr inbounds %struct._Thread* %104, i32 0, i32 11, !dbg !585
  store i64* %105, i64** %4, align 8
  call void @llvm.dbg.declare(metadata !{i64** %4}, metadata !586), !dbg !587
  call void @llvm.dbg.declare(metadata !{i64* %x.i.i.i}, metadata !588), !dbg !589
  %106 = load i64** %4, align 8, !dbg !590
  %107 = load i64* %106, align 8, !dbg !590
  store i64 %107, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !591), !dbg !592
  %108 = load i64* %3, align 8, !dbg !593
  %109 = icmp eq i64 %108, 0, !dbg !593
  br i1 %109, label %110, label %TSRandom.exit.i, !dbg !593

; <label>:110                                     ; preds = %98
  store i64 1, i64* %3, align 8, !dbg !594
  br label %TSRandom.exit.i, !dbg !596

TSRandom.exit.i:                                  ; preds = %110, %98
  %111 = load i64* %3, align 8, !dbg !597
  %112 = shl i64 %111, 6, !dbg !597
  %113 = load i64* %3, align 8, !dbg !597
  %114 = xor i64 %113, %112, !dbg !597
  store i64 %114, i64* %3, align 8, !dbg !597
  %115 = load i64* %3, align 8, !dbg !598
  %116 = lshr i64 %115, 21, !dbg !598
  %117 = load i64* %3, align 8, !dbg !598
  %118 = xor i64 %117, %116, !dbg !598
  store i64 %118, i64* %3, align 8, !dbg !598
  %119 = load i64* %3, align 8, !dbg !599
  %120 = shl i64 %119, 7, !dbg !599
  %121 = load i64* %3, align 8, !dbg !599
  %122 = xor i64 %121, %120, !dbg !599
  store i64 %122, i64* %3, align 8, !dbg !599
  %123 = load i64* %3, align 8, !dbg !600
  store i64 %123, i64* %x.i.i.i, align 8, !dbg !590
  %124 = load i64* %x.i.i.i, align 8, !dbg !601
  %125 = load i64** %4, align 8, !dbg !601
  store i64 %124, i64* %125, align 8, !dbg !601
  %126 = load i64* %x.i.i.i, align 8, !dbg !602
  %127 = and i64 %126, 15, !dbg !582
  store i64 %127, i64* %stall.i, align 8, !dbg !582
  %128 = load i64* %7, align 8, !dbg !603
  %129 = ashr i64 %128, 2, !dbg !603
  %130 = load i64* %stall.i, align 8, !dbg !603
  %131 = add i64 %130, %129, !dbg !603
  store i64 %131, i64* %stall.i, align 8, !dbg !603
  %132 = load i64* %stall.i, align 8, !dbg !604
  %133 = mul i64 %132, 10, !dbg !604
  store i64 %133, i64* %stall.i, align 8, !dbg !604
  call void @llvm.dbg.declare(metadata !{i64* %i.i}, metadata !605), !dbg !607
  store volatile i64 0, i64* %i.i, align 8, !dbg !607
  br label %134, !dbg !608

; <label>:134                                     ; preds = %139, %TSRandom.exit.i
  %135 = load volatile i64* %i.i, align 8, !dbg !608
  %136 = add i64 %135, 1, !dbg !608
  store volatile i64 %136, i64* %i.i, align 8, !dbg !608
  %137 = load i64* %stall.i, align 8, !dbg !608
  %138 = icmp ult i64 %135, %137, !dbg !608
  br i1 %138, label %139, label %backoff.exit, !dbg !608

; <label>:139                                     ; preds = %134
  br label %134, !dbg !609

backoff.exit:                                     ; preds = %134
  br label %140, !dbg !611

; <label>:140                                     ; preds = %backoff.exit, %93
  br label %141, !dbg !611

; <label>:141                                     ; preds = %140, %92
  %142 = load %struct._Thread** %9, align 8, !dbg !612
  %143 = getelementptr inbounds %struct._Thread* %142, i32 0, i32 14, !dbg !612
  %144 = load %struct.tmalloc** %143, align 8, !dbg !612
  call void @tmalloc_releaseAllReverse(%struct.tmalloc* %144, void (i8*, i64)* null), !dbg !612
  %145 = load %struct._Thread** %9, align 8, !dbg !613
  %146 = getelementptr inbounds %struct._Thread* %145, i32 0, i32 15, !dbg !613
  %147 = load %struct.tmalloc** %146, align 8, !dbg !613
  call void @tmalloc_clear(%struct.tmalloc* %147), !dbg !613
  %148 = load %struct._Thread** %9, align 8, !dbg !614
  %149 = getelementptr inbounds %struct._Thread* %148, i32 0, i32 19, !dbg !614
  %150 = load [1 x %struct.__jmp_buf_tag]** %149, align 8, !dbg !614
  %151 = getelementptr inbounds [1 x %struct.__jmp_buf_tag]* %150, i32 0, i32 0, !dbg !614
  call void @siglongjmp(%struct.__jmp_buf_tag* %151, i32 1) #7, !dbg !614
  unreachable, !dbg !614
                                                  ; No predecessors!
  ret void, !dbg !615
}

declare void @tmalloc_releaseAllReverse(%struct.tmalloc*, void (i8*, i64)*) #4

declare void @tmalloc_clear(%struct.tmalloc*) #4

; Function Attrs: noreturn nounwind
declare void @siglongjmp(%struct.__jmp_buf_tag*, i32) #5

; Function Attrs: nounwind uwtable
define void @TxStore(%struct._Thread* %Self, i64* %addr, i64 %valu) #0 {
  %1 = alloca %struct._AVPair*, align 8
  %e.i.i1 = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Log*, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  %e.i2 = alloca %struct._AVPair*, align 8
  %6 = alloca %struct._AVPair*, align 8
  %e.i1.i = alloca %struct._AVPair*, align 8
  %7 = alloca i64, align 8
  %8 = alloca %struct._Thread*, align 8
  %rv.i.i = alloca i64, align 8
  %rd.i.i = alloca %struct._Log*, align 8
  %EndOfList.i.i = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %v.i.i = alloca i64, align 8
  %9 = alloca i32, align 4
  %10 = alloca %struct._Thread*, align 8
  %11 = alloca i64*, align 8
  %k.i = alloca %struct._Log*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %12 = alloca %struct._Thread*, align 8
  %13 = alloca i64*, align 8
  %14 = alloca i64, align 8
  %LockFor = alloca i64*, align 8
  %rdv = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._Thread* %Self, %struct._Thread** %12, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %12}, metadata !616), !dbg !617
  store i64* %addr, i64** %13, align 8
  call void @llvm.dbg.declare(metadata !{i64** %13}, metadata !618), !dbg !617
  store i64 %valu, i64* %14, align 8
  call void @llvm.dbg.declare(metadata !{i64* %14}, metadata !619), !dbg !617
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !620), !dbg !621
  call void @llvm.dbg.declare(metadata !{i64* %rdv}, metadata !622), !dbg !623
  %15 = load %struct._Thread** %12, align 8, !dbg !624
  %16 = getelementptr inbounds %struct._Thread* %15, i32 0, i32 8, !dbg !624
  %17 = load i32* %16, align 4, !dbg !624
  %18 = icmp ne i32 %17, 0, !dbg !624
  br i1 %18, label %19, label %24, !dbg !624

; <label>:19                                      ; preds = %0
  %20 = load %struct._Thread** %12, align 8, !dbg !625
  %21 = getelementptr inbounds %struct._Thread* %20, i32 0, i32 7, !dbg !625
  %22 = load i32** %21, align 8, !dbg !625
  store i32 0, i32* %22, align 4, !dbg !625
  %23 = load %struct._Thread** %12, align 8, !dbg !627
  call void @TxAbort(%struct._Thread* %23), !dbg !627
  br label %276, !dbg !628

; <label>:24                                      ; preds = %0
  %25 = load i64** %13, align 8, !dbg !629
  %26 = ptrtoint i64* %25 to i64, !dbg !629
  %27 = add i64 %26, 128, !dbg !629
  %28 = lshr i64 %27, 3, !dbg !629
  %29 = and i64 %28, 1048575, !dbg !629
  %30 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %29, !dbg !629
  store i64* %30, i64** %LockFor, align 8, !dbg !629
  %31 = load i64** %LockFor, align 8, !dbg !630
  %32 = load volatile i64* %31, align 8, !dbg !630
  store i64 %32, i64* %rdv, align 8, !dbg !630
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !631), !dbg !632
  %33 = load %struct._Thread** %12, align 8, !dbg !632
  %34 = getelementptr inbounds %struct._Thread* %33, i32 0, i32 17, !dbg !632
  store %struct._Log* %34, %struct._Log** %wr, align 8, !dbg !632
  %35 = load i64** %13, align 8, !dbg !633
  %36 = load volatile i64* %35, align 8, !dbg !633
  %37 = load i64* %14, align 8, !dbg !633
  %38 = icmp eq i64 %36, %37, !dbg !633
  br i1 %38, label %39, label %193, !dbg !633

; <label>:39                                      ; preds = %24
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !634), !dbg !636
  %40 = load %struct._Log** %wr, align 8, !dbg !637
  %41 = getelementptr inbounds %struct._Log* %40, i32 0, i32 2, !dbg !637
  %42 = load %struct._AVPair** %41, align 8, !dbg !637
  store %struct._AVPair* %42, %struct._AVPair** %e, align 8, !dbg !637
  br label %43, !dbg !637

; <label>:43                                      ; preds = %57, %39
  %44 = load %struct._AVPair** %e, align 8, !dbg !637
  %45 = icmp ne %struct._AVPair* %44, null, !dbg !637
  br i1 %45, label %46, label %61, !dbg !637

; <label>:46                                      ; preds = %43
  %47 = load %struct._AVPair** %e, align 8, !dbg !639
  %48 = getelementptr inbounds %struct._AVPair* %47, i32 0, i32 2, !dbg !639
  %49 = load i64** %48, align 8, !dbg !639
  %50 = load i64** %13, align 8, !dbg !639
  %51 = icmp eq i64* %49, %50, !dbg !639
  br i1 %51, label %52, label %56, !dbg !639

; <label>:52                                      ; preds = %46
  %53 = load i64* %14, align 8, !dbg !641
  %54 = load %struct._AVPair** %e, align 8, !dbg !641
  %55 = getelementptr inbounds %struct._AVPair* %54, i32 0, i32 3, !dbg !641
  store i64 %53, i64* %55, align 8, !dbg !641
  br label %276, !dbg !643

; <label>:56                                      ; preds = %46
  br label %57, !dbg !644

; <label>:57                                      ; preds = %56
  %58 = load %struct._AVPair** %e, align 8, !dbg !637
  %59 = getelementptr inbounds %struct._AVPair* %58, i32 0, i32 1, !dbg !637
  %60 = load %struct._AVPair** %59, align 8, !dbg !637
  store %struct._AVPair* %60, %struct._AVPair** %e, align 8, !dbg !637
  br label %43, !dbg !637

; <label>:61                                      ; preds = %43
  %62 = load i64* %rdv, align 8, !dbg !645
  %63 = and i64 %62, 1, !dbg !645
  %64 = icmp eq i64 %63, 0, !dbg !645
  br i1 %64, label %65, label %192, !dbg !645

; <label>:65                                      ; preds = %61
  %66 = load i64* %rdv, align 8, !dbg !645
  %67 = load %struct._Thread** %12, align 8, !dbg !645
  %68 = getelementptr inbounds %struct._Thread* %67, i32 0, i32 4, !dbg !645
  %69 = load volatile i64* %68, align 8, !dbg !645
  %70 = icmp ule i64 %66, %69, !dbg !645
  br i1 %70, label %71, label %192, !dbg !645

; <label>:71                                      ; preds = %65
  %72 = load i64** %LockFor, align 8, !dbg !645
  %73 = load volatile i64* %72, align 8, !dbg !645
  %74 = load i64* %rdv, align 8, !dbg !645
  %75 = icmp eq i64 %73, %74, !dbg !645
  br i1 %75, label %76, label %192, !dbg !645

; <label>:76                                      ; preds = %71
  %77 = load %struct._Thread** %12, align 8, !dbg !646
  %78 = load i64** %LockFor, align 8, !dbg !646
  store %struct._Thread* %77, %struct._Thread** %10, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %10}, metadata !648) #6, !dbg !649
  store i64* %78, i64** %11, align 8
  call void @llvm.dbg.declare(metadata !{i64** %11}, metadata !650) #6, !dbg !649
  call void @llvm.dbg.declare(metadata !{%struct._Log** %k.i}, metadata !651) #6, !dbg !652
  %79 = load %struct._Thread** %10, align 8, !dbg !652
  %80 = getelementptr inbounds %struct._Thread* %79, i32 0, i32 16, !dbg !652
  store %struct._Log* %80, %struct._Log** %k.i, align 8, !dbg !652
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !653) #6, !dbg !654
  %81 = load %struct._Log** %k.i, align 8, !dbg !654
  %82 = getelementptr inbounds %struct._Log* %81, i32 0, i32 1, !dbg !654
  %83 = load %struct._AVPair** %82, align 8, !dbg !654
  store %struct._AVPair* %83, %struct._AVPair** %e.i, align 8, !dbg !654
  %84 = load %struct._AVPair** %e.i, align 8, !dbg !655
  %85 = icmp eq %struct._AVPair* %84, null, !dbg !655
  br i1 %85, label %86, label %175, !dbg !655

; <label>:86                                      ; preds = %76
  %87 = load %struct._Thread** %10, align 8, !dbg !656
  store %struct._Thread* %87, %struct._Thread** %8, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %8}, metadata !658) #6, !dbg !659
  call void @llvm.dbg.declare(metadata !{i64* %rv.i.i}, metadata !660) #6, !dbg !661
  %88 = load %struct._Thread** %8, align 8, !dbg !661
  %89 = getelementptr inbounds %struct._Thread* %88, i32 0, i32 4, !dbg !661
  %90 = load volatile i64* %89, align 8, !dbg !661
  store i64 %90, i64* %rv.i.i, align 8, !dbg !661
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i.i}, metadata !662) #6, !dbg !664
  %91 = load %struct._Thread** %8, align 8, !dbg !664
  %92 = getelementptr inbounds %struct._Thread* %91, i32 0, i32 16, !dbg !664
  store %struct._Log* %92, %struct._Log** %rd.i.i, align 8, !dbg !664
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList.i.i}, metadata !665) #6, !dbg !666
  %93 = load %struct._Log** %rd.i.i, align 8, !dbg !666
  %94 = getelementptr inbounds %struct._Log* %93, i32 0, i32 1, !dbg !666
  %95 = load %struct._AVPair** %94, align 8, !dbg !666
  store %struct._AVPair* %95, %struct._AVPair** %EndOfList.i.i, align 8, !dbg !666
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !667) #6, !dbg !668
  %96 = load %struct._Log** %rd.i.i, align 8, !dbg !669
  %97 = getelementptr inbounds %struct._Log* %96, i32 0, i32 0, !dbg !669
  %98 = load %struct._AVPair** %97, align 8, !dbg !669
  store %struct._AVPair* %98, %struct._AVPair** %e.i.i, align 8, !dbg !669
  br label %99, !dbg !669

; <label>:99                                      ; preds = %129, %86
  %100 = load %struct._AVPair** %e.i.i, align 8, !dbg !669
  %101 = load %struct._AVPair** %EndOfList.i.i, align 8, !dbg !669
  %102 = icmp ne %struct._AVPair* %100, %101, !dbg !669
  br i1 %102, label %103, label %133, !dbg !669

; <label>:103                                     ; preds = %99
  call void @llvm.dbg.declare(metadata !{i64* %v.i.i}, metadata !671) #6, !dbg !673
  %104 = load %struct._AVPair** %e.i.i, align 8, !dbg !673
  %105 = getelementptr inbounds %struct._AVPair* %104, i32 0, i32 4, !dbg !673
  %106 = load i64** %105, align 8, !dbg !673
  %107 = load volatile i64* %106, align 8, !dbg !673
  store i64 %107, i64* %v.i.i, align 8, !dbg !673
  %108 = load i64* %v.i.i, align 8, !dbg !674
  %109 = and i64 %108, 1, !dbg !674
  %110 = icmp ne i64 %109, 0, !dbg !674
  br i1 %110, label %111, label %123, !dbg !674

; <label>:111                                     ; preds = %103
  %112 = load i64* %v.i.i, align 8, !dbg !675
  %113 = xor i64 %112, 1, !dbg !675
  %114 = inttoptr i64 %113 to %struct._AVPair*, !dbg !675
  %115 = getelementptr inbounds %struct._AVPair* %114, i32 0, i32 7, !dbg !675
  %116 = load %struct._Thread** %115, align 8, !dbg !675
  %117 = ptrtoint %struct._Thread* %116 to i64, !dbg !675
  %118 = load %struct._Thread** %8, align 8, !dbg !675
  %119 = ptrtoint %struct._Thread* %118 to i64, !dbg !675
  %120 = icmp ne i64 %117, %119, !dbg !675
  br i1 %120, label %121, label %122, !dbg !675

; <label>:121                                     ; preds = %111
  store i64 0, i64* %7, !dbg !677
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !677

; <label>:122                                     ; preds = %111
  br label %129, !dbg !679

; <label>:123                                     ; preds = %103
  %124 = load i64* %v.i.i, align 8, !dbg !680
  %125 = load i64* %rv.i.i, align 8, !dbg !680
  %126 = icmp ugt i64 %124, %125, !dbg !680
  br i1 %126, label %127, label %128, !dbg !680

; <label>:127                                     ; preds = %123
  store i64 0, i64* %7, !dbg !682
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !682

; <label>:128                                     ; preds = %123
  br label %129

; <label>:129                                     ; preds = %128, %122
  %130 = load %struct._AVPair** %e.i.i, align 8, !dbg !669
  %131 = getelementptr inbounds %struct._AVPair* %130, i32 0, i32 0, !dbg !669
  %132 = load %struct._AVPair** %131, align 8, !dbg !669
  store %struct._AVPair* %132, %struct._AVPair** %e.i.i, align 8, !dbg !669
  br label %99, !dbg !669

; <label>:133                                     ; preds = %99
  store i64 1, i64* %7, !dbg !684
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !684

ReadSetCoherentPessimistic.exit.i:                ; preds = %133, %127, %121
  %134 = load i64* %7, !dbg !684
  %135 = icmp ne i64 %134, 0, !dbg !656
  br i1 %135, label %137, label %136, !dbg !656

; <label>:136                                     ; preds = %ReadSetCoherentPessimistic.exit.i
  store i32 0, i32* %9, !dbg !685
  br label %TrackLoad.exit, !dbg !685

; <label>:137                                     ; preds = %ReadSetCoherentPessimistic.exit.i
  %138 = load %struct._Log** %k.i, align 8, !dbg !687
  %139 = getelementptr inbounds %struct._Log* %138, i32 0, i32 4, !dbg !687
  %140 = load i64* %139, align 8, !dbg !687
  %141 = add nsw i64 %140, 1, !dbg !687
  store i64 %141, i64* %139, align 8, !dbg !687
  %142 = load %struct._Log** %k.i, align 8, !dbg !688
  %143 = getelementptr inbounds %struct._Log* %142, i32 0, i32 2, !dbg !688
  %144 = load %struct._AVPair** %143, align 8, !dbg !688
  store %struct._AVPair* %144, %struct._AVPair** %6, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %6}, metadata !689) #6, !dbg !690
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i1.i}, metadata !691) #6, !dbg !692
  %145 = call noalias i8* @malloc(i64 72) #6, !dbg !692
  %146 = bitcast i8* %145 to %struct._AVPair*, !dbg !692
  store %struct._AVPair* %146, %struct._AVPair** %e.i1.i, align 8, !dbg !692
  %147 = load %struct._AVPair** %e.i1.i, align 8, !dbg !693
  %148 = icmp ne %struct._AVPair* %147, null, !dbg !693
  br i1 %148, label %ExtendList.exit.i, label %149, !dbg !693

; <label>:149                                     ; preds = %137
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !693
  unreachable, !dbg !693

ExtendList.exit.i:                                ; preds = %137
  %150 = load %struct._AVPair** %e.i1.i, align 8, !dbg !694
  %151 = bitcast %struct._AVPair* %150 to i8*, !dbg !694
  call void @llvm.memset.p0i8.i64(i8* %151, i8 0, i64 72, i32 8, i1 false) #6, !dbg !694
  %152 = load %struct._AVPair** %e.i1.i, align 8, !dbg !695
  %153 = load %struct._AVPair** %6, align 8, !dbg !695
  %154 = getelementptr inbounds %struct._AVPair* %153, i32 0, i32 0, !dbg !695
  store %struct._AVPair* %152, %struct._AVPair** %154, align 8, !dbg !695
  %155 = load %struct._AVPair** %6, align 8, !dbg !696
  %156 = load %struct._AVPair** %e.i1.i, align 8, !dbg !696
  %157 = getelementptr inbounds %struct._AVPair* %156, i32 0, i32 1, !dbg !696
  store %struct._AVPair* %155, %struct._AVPair** %157, align 8, !dbg !696
  %158 = load %struct._AVPair** %e.i1.i, align 8, !dbg !697
  %159 = getelementptr inbounds %struct._AVPair* %158, i32 0, i32 0, !dbg !697
  store %struct._AVPair* null, %struct._AVPair** %159, align 8, !dbg !697
  %160 = load %struct._AVPair** %6, align 8, !dbg !698
  %161 = getelementptr inbounds %struct._AVPair* %160, i32 0, i32 7, !dbg !698
  %162 = load %struct._Thread** %161, align 8, !dbg !698
  %163 = load %struct._AVPair** %e.i1.i, align 8, !dbg !698
  %164 = getelementptr inbounds %struct._AVPair* %163, i32 0, i32 7, !dbg !698
  store %struct._Thread* %162, %struct._Thread** %164, align 8, !dbg !698
  %165 = load %struct._AVPair** %6, align 8, !dbg !699
  %166 = getelementptr inbounds %struct._AVPair* %165, i32 0, i32 8, !dbg !699
  %167 = load i64* %166, align 8, !dbg !699
  %168 = add nsw i64 %167, 1, !dbg !699
  %169 = load %struct._AVPair** %e.i1.i, align 8, !dbg !699
  %170 = getelementptr inbounds %struct._AVPair* %169, i32 0, i32 8, !dbg !699
  store i64 %168, i64* %170, align 8, !dbg !699
  %171 = load %struct._AVPair** %e.i1.i, align 8, !dbg !700
  store %struct._AVPair* %171, %struct._AVPair** %e.i, align 8, !dbg !688
  %172 = load %struct._AVPair** %e.i, align 8, !dbg !701
  %173 = load %struct._Log** %k.i, align 8, !dbg !701
  %174 = getelementptr inbounds %struct._Log* %173, i32 0, i32 3, !dbg !701
  store %struct._AVPair* %172, %struct._AVPair** %174, align 8, !dbg !701
  br label %175, !dbg !702

; <label>:175                                     ; preds = %ExtendList.exit.i, %76
  %176 = load %struct._AVPair** %e.i, align 8, !dbg !703
  %177 = load %struct._Log** %k.i, align 8, !dbg !703
  %178 = getelementptr inbounds %struct._Log* %177, i32 0, i32 2, !dbg !703
  store %struct._AVPair* %176, %struct._AVPair** %178, align 8, !dbg !703
  %179 = load %struct._AVPair** %e.i, align 8, !dbg !704
  %180 = getelementptr inbounds %struct._AVPair* %179, i32 0, i32 0, !dbg !704
  %181 = load %struct._AVPair** %180, align 8, !dbg !704
  %182 = load %struct._Log** %k.i, align 8, !dbg !704
  %183 = getelementptr inbounds %struct._Log* %182, i32 0, i32 1, !dbg !704
  store %struct._AVPair* %181, %struct._AVPair** %183, align 8, !dbg !704
  %184 = load i64** %11, align 8, !dbg !705
  %185 = load %struct._AVPair** %e.i, align 8, !dbg !705
  %186 = getelementptr inbounds %struct._AVPair* %185, i32 0, i32 4, !dbg !705
  store i64* %184, i64** %186, align 8, !dbg !705
  store i32 1, i32* %9, !dbg !706
  br label %TrackLoad.exit, !dbg !706

TrackLoad.exit:                                   ; preds = %136, %175
  %187 = load i32* %9, !dbg !706
  %188 = icmp ne i32 %187, 0, !dbg !646
  br i1 %188, label %191, label %189, !dbg !646

; <label>:189                                     ; preds = %TrackLoad.exit
  %190 = load %struct._Thread** %12, align 8, !dbg !707
  call void @TxAbort(%struct._Thread* %190), !dbg !707
  br label %191, !dbg !709

; <label>:191                                     ; preds = %189, %TrackLoad.exit
  br label %276, !dbg !710

; <label>:192                                     ; preds = %71, %65, %61
  br label %193, !dbg !711

; <label>:193                                     ; preds = %192, %24
  %194 = load i64** %13, align 8, !dbg !712
  %195 = ptrtoint i64* %194 to i64, !dbg !712
  %196 = lshr i64 %195, 2, !dbg !712
  %197 = load i64** %13, align 8, !dbg !712
  %198 = ptrtoint i64* %197 to i64, !dbg !712
  %199 = lshr i64 %198, 5, !dbg !712
  %200 = xor i64 %196, %199, !dbg !712
  %201 = and i64 %200, 31, !dbg !712
  %202 = trunc i64 %201 to i32, !dbg !712
  %203 = shl i32 1, %202, !dbg !712
  %204 = load %struct._Log** %wr, align 8, !dbg !712
  %205 = getelementptr inbounds %struct._Log* %204, i32 0, i32 5, !dbg !712
  %206 = load i32* %205, align 4, !dbg !712
  %207 = or i32 %206, %203, !dbg !712
  store i32 %207, i32* %205, align 4, !dbg !712
  %208 = load %struct._Log** %wr, align 8, !dbg !713
  %209 = load i64** %13, align 8, !dbg !713
  %210 = load i64* %14, align 8, !dbg !713
  %211 = load i64** %LockFor, align 8, !dbg !713
  store %struct._Log* %208, %struct._Log** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %2}, metadata !714) #6, !dbg !715
  store i64* %209, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !716) #6, !dbg !715
  store i64 %210, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !717) #6, !dbg !715
  store i64* %211, i64** %5, align 8
  call void @llvm.dbg.declare(metadata !{i64** %5}, metadata !718) #6, !dbg !715
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i2}, metadata !719) #6, !dbg !720
  %212 = load %struct._Log** %2, align 8, !dbg !720
  %213 = getelementptr inbounds %struct._Log* %212, i32 0, i32 1, !dbg !720
  %214 = load %struct._AVPair** %213, align 8, !dbg !720
  store %struct._AVPair* %214, %struct._AVPair** %e.i2, align 8, !dbg !720
  %215 = load %struct._AVPair** %e.i2, align 8, !dbg !721
  %216 = icmp eq %struct._AVPair* %215, null, !dbg !721
  br i1 %216, label %217, label %RecordStore.exit, !dbg !721

; <label>:217                                     ; preds = %193
  %218 = load %struct._Log** %2, align 8, !dbg !722
  %219 = getelementptr inbounds %struct._Log* %218, i32 0, i32 4, !dbg !722
  %220 = load i64* %219, align 8, !dbg !722
  %221 = add nsw i64 %220, 1, !dbg !722
  store i64 %221, i64* %219, align 8, !dbg !722
  %222 = load %struct._Log** %2, align 8, !dbg !724
  %223 = getelementptr inbounds %struct._Log* %222, i32 0, i32 2, !dbg !724
  %224 = load %struct._AVPair** %223, align 8, !dbg !724
  store %struct._AVPair* %224, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !689) #6, !dbg !725
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i1}, metadata !691) #6, !dbg !726
  %225 = call noalias i8* @malloc(i64 72) #6, !dbg !726
  %226 = bitcast i8* %225 to %struct._AVPair*, !dbg !726
  store %struct._AVPair* %226, %struct._AVPair** %e.i.i1, align 8, !dbg !726
  %227 = load %struct._AVPair** %e.i.i1, align 8, !dbg !727
  %228 = icmp ne %struct._AVPair* %227, null, !dbg !727
  br i1 %228, label %ExtendList.exit.i3, label %229, !dbg !727

; <label>:229                                     ; preds = %217
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !727
  unreachable, !dbg !727

ExtendList.exit.i3:                               ; preds = %217
  %230 = load %struct._AVPair** %e.i.i1, align 8, !dbg !728
  %231 = bitcast %struct._AVPair* %230 to i8*, !dbg !728
  call void @llvm.memset.p0i8.i64(i8* %231, i8 0, i64 72, i32 8, i1 false) #6, !dbg !728
  %232 = load %struct._AVPair** %e.i.i1, align 8, !dbg !729
  %233 = load %struct._AVPair** %1, align 8, !dbg !729
  %234 = getelementptr inbounds %struct._AVPair* %233, i32 0, i32 0, !dbg !729
  store %struct._AVPair* %232, %struct._AVPair** %234, align 8, !dbg !729
  %235 = load %struct._AVPair** %1, align 8, !dbg !730
  %236 = load %struct._AVPair** %e.i.i1, align 8, !dbg !730
  %237 = getelementptr inbounds %struct._AVPair* %236, i32 0, i32 1, !dbg !730
  store %struct._AVPair* %235, %struct._AVPair** %237, align 8, !dbg !730
  %238 = load %struct._AVPair** %e.i.i1, align 8, !dbg !731
  %239 = getelementptr inbounds %struct._AVPair* %238, i32 0, i32 0, !dbg !731
  store %struct._AVPair* null, %struct._AVPair** %239, align 8, !dbg !731
  %240 = load %struct._AVPair** %1, align 8, !dbg !732
  %241 = getelementptr inbounds %struct._AVPair* %240, i32 0, i32 7, !dbg !732
  %242 = load %struct._Thread** %241, align 8, !dbg !732
  %243 = load %struct._AVPair** %e.i.i1, align 8, !dbg !732
  %244 = getelementptr inbounds %struct._AVPair* %243, i32 0, i32 7, !dbg !732
  store %struct._Thread* %242, %struct._Thread** %244, align 8, !dbg !732
  %245 = load %struct._AVPair** %1, align 8, !dbg !733
  %246 = getelementptr inbounds %struct._AVPair* %245, i32 0, i32 8, !dbg !733
  %247 = load i64* %246, align 8, !dbg !733
  %248 = add nsw i64 %247, 1, !dbg !733
  %249 = load %struct._AVPair** %e.i.i1, align 8, !dbg !733
  %250 = getelementptr inbounds %struct._AVPair* %249, i32 0, i32 8, !dbg !733
  store i64 %248, i64* %250, align 8, !dbg !733
  %251 = load %struct._AVPair** %e.i.i1, align 8, !dbg !734
  store %struct._AVPair* %251, %struct._AVPair** %e.i2, align 8, !dbg !724
  %252 = load %struct._AVPair** %e.i2, align 8, !dbg !735
  %253 = load %struct._Log** %2, align 8, !dbg !735
  %254 = getelementptr inbounds %struct._Log* %253, i32 0, i32 3, !dbg !735
  store %struct._AVPair* %252, %struct._AVPair** %254, align 8, !dbg !735
  br label %RecordStore.exit, !dbg !736

RecordStore.exit:                                 ; preds = %193, %ExtendList.exit.i3
  %255 = load %struct._AVPair** %e.i2, align 8, !dbg !737
  %256 = load %struct._Log** %2, align 8, !dbg !737
  %257 = getelementptr inbounds %struct._Log* %256, i32 0, i32 2, !dbg !737
  store %struct._AVPair* %255, %struct._AVPair** %257, align 8, !dbg !737
  %258 = load %struct._AVPair** %e.i2, align 8, !dbg !738
  %259 = getelementptr inbounds %struct._AVPair* %258, i32 0, i32 0, !dbg !738
  %260 = load %struct._AVPair** %259, align 8, !dbg !738
  %261 = load %struct._Log** %2, align 8, !dbg !738
  %262 = getelementptr inbounds %struct._Log* %261, i32 0, i32 1, !dbg !738
  store %struct._AVPair* %260, %struct._AVPair** %262, align 8, !dbg !738
  %263 = load i64** %3, align 8, !dbg !739
  %264 = load %struct._AVPair** %e.i2, align 8, !dbg !739
  %265 = getelementptr inbounds %struct._AVPair* %264, i32 0, i32 2, !dbg !739
  store i64* %263, i64** %265, align 8, !dbg !739
  %266 = load i64* %4, align 8, !dbg !740
  %267 = load %struct._AVPair** %e.i2, align 8, !dbg !740
  %268 = getelementptr inbounds %struct._AVPair* %267, i32 0, i32 3, !dbg !740
  store i64 %266, i64* %268, align 8, !dbg !740
  %269 = load i64** %5, align 8, !dbg !741
  %270 = load %struct._AVPair** %e.i2, align 8, !dbg !741
  %271 = getelementptr inbounds %struct._AVPair* %270, i32 0, i32 4, !dbg !741
  store i64* %269, i64** %271, align 8, !dbg !741
  %272 = load %struct._AVPair** %e.i2, align 8, !dbg !742
  %273 = getelementptr inbounds %struct._AVPair* %272, i32 0, i32 6, !dbg !742
  store i8 0, i8* %273, align 1, !dbg !742
  %274 = load %struct._AVPair** %e.i2, align 8, !dbg !743
  %275 = getelementptr inbounds %struct._AVPair* %274, i32 0, i32 5, !dbg !743
  store i64 1, i64* %275, align 8, !dbg !743
  br label %276, !dbg !713

; <label>:276                                     ; preds = %RecordStore.exit, %191, %52, %19
  ret void, !dbg !713
}

; Function Attrs: nounwind uwtable
define i64 @TxLoad(%struct._Thread* %Self, i64* %Addr) #0 {
  %1 = alloca %struct._AVPair*, align 8
  %e.i1.i = alloca %struct._AVPair*, align 8
  %2 = alloca i64, align 8
  %3 = alloca %struct._Thread*, align 8
  %rv.i.i = alloca i64, align 8
  %rd.i.i = alloca %struct._Log*, align 8
  %EndOfList.i.i = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %v.i.i = alloca i64, align 8
  %4 = alloca i32, align 4
  %5 = alloca %struct._Thread*, align 8
  %6 = alloca i64*, align 8
  %k.i = alloca %struct._Log*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %7 = alloca i64, align 8
  %8 = alloca %struct._Thread*, align 8
  %9 = alloca i64*, align 8
  %Valu = alloca i64, align 8
  %msk = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  %LockFor = alloca i64*, align 8
  %rdv = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %8, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %8}, metadata !744), !dbg !745
  store i64* %Addr, i64** %9, align 8
  call void @llvm.dbg.declare(metadata !{i64** %9}, metadata !746), !dbg !745
  call void @llvm.dbg.declare(metadata !{i64* %Valu}, metadata !747), !dbg !748
  call void @llvm.dbg.declare(metadata !{i64* %msk}, metadata !749), !dbg !750
  %10 = load i64** %9, align 8, !dbg !750
  %11 = ptrtoint i64* %10 to i64, !dbg !750
  %12 = lshr i64 %11, 2, !dbg !750
  %13 = load i64** %9, align 8, !dbg !750
  %14 = ptrtoint i64* %13 to i64, !dbg !750
  %15 = lshr i64 %14, 5, !dbg !750
  %16 = xor i64 %12, %15, !dbg !750
  %17 = and i64 %16, 31, !dbg !750
  %18 = trunc i64 %17 to i32, !dbg !750
  %19 = shl i32 1, %18, !dbg !750
  %20 = sext i32 %19 to i64, !dbg !750
  store i64 %20, i64* %msk, align 8, !dbg !750
  %21 = load %struct._Thread** %8, align 8, !dbg !751
  %22 = getelementptr inbounds %struct._Thread* %21, i32 0, i32 17, !dbg !751
  %23 = getelementptr inbounds %struct._Log* %22, i32 0, i32 5, !dbg !751
  %24 = load i32* %23, align 4, !dbg !751
  %25 = sext i32 %24 to i64, !dbg !751
  %26 = load i64* %msk, align 8, !dbg !751
  %27 = and i64 %25, %26, !dbg !751
  %28 = load i64* %msk, align 8, !dbg !751
  %29 = icmp eq i64 %27, %28, !dbg !751
  br i1 %29, label %30, label %55, !dbg !751

; <label>:30                                      ; preds = %0
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !752), !dbg !754
  %31 = load %struct._Thread** %8, align 8, !dbg !754
  %32 = getelementptr inbounds %struct._Thread* %31, i32 0, i32 17, !dbg !754
  store %struct._Log* %32, %struct._Log** %wr, align 8, !dbg !754
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !755), !dbg !756
  %33 = load %struct._Log** %wr, align 8, !dbg !757
  %34 = getelementptr inbounds %struct._Log* %33, i32 0, i32 2, !dbg !757
  %35 = load %struct._AVPair** %34, align 8, !dbg !757
  store %struct._AVPair* %35, %struct._AVPair** %e, align 8, !dbg !757
  br label %36, !dbg !757

; <label>:36                                      ; preds = %50, %30
  %37 = load %struct._AVPair** %e, align 8, !dbg !757
  %38 = icmp ne %struct._AVPair* %37, null, !dbg !757
  br i1 %38, label %39, label %54, !dbg !757

; <label>:39                                      ; preds = %36
  %40 = load %struct._AVPair** %e, align 8, !dbg !759
  %41 = getelementptr inbounds %struct._AVPair* %40, i32 0, i32 2, !dbg !759
  %42 = load i64** %41, align 8, !dbg !759
  %43 = load i64** %9, align 8, !dbg !759
  %44 = icmp eq i64* %42, %43, !dbg !759
  br i1 %44, label %45, label %49, !dbg !759

; <label>:45                                      ; preds = %39
  %46 = load %struct._AVPair** %e, align 8, !dbg !761
  %47 = getelementptr inbounds %struct._AVPair* %46, i32 0, i32 3, !dbg !761
  %48 = load i64* %47, align 8, !dbg !761
  store i64 %48, i64* %7, !dbg !761
  br label %205, !dbg !761

; <label>:49                                      ; preds = %39
  br label %50, !dbg !763

; <label>:50                                      ; preds = %49
  %51 = load %struct._AVPair** %e, align 8, !dbg !757
  %52 = getelementptr inbounds %struct._AVPair* %51, i32 0, i32 1, !dbg !757
  %53 = load %struct._AVPair** %52, align 8, !dbg !757
  store %struct._AVPair* %53, %struct._AVPair** %e, align 8, !dbg !757
  br label %36, !dbg !757

; <label>:54                                      ; preds = %36
  br label %55, !dbg !764

; <label>:55                                      ; preds = %54, %0
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !765), !dbg !766
  %56 = load i64** %9, align 8, !dbg !766
  %57 = ptrtoint i64* %56 to i64, !dbg !766
  %58 = add i64 %57, 128, !dbg !766
  %59 = lshr i64 %58, 3, !dbg !766
  %60 = and i64 %59, 1048575, !dbg !766
  %61 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %60, !dbg !766
  store i64* %61, i64** %LockFor, align 8, !dbg !766
  call void @llvm.dbg.declare(metadata !{i64* %rdv}, metadata !767), !dbg !768
  %62 = load i64** %LockFor, align 8, !dbg !768
  %63 = load volatile i64* %62, align 8, !dbg !768
  %64 = and i64 %63, -2, !dbg !768
  store i64 %64, i64* %rdv, align 8, !dbg !768
  %65 = load i64** %9, align 8, !dbg !769
  %66 = load volatile i64* %65, align 8, !dbg !769
  store i64 %66, i64* %Valu, align 8, !dbg !769
  %67 = load i64* %rdv, align 8, !dbg !770
  %68 = load %struct._Thread** %8, align 8, !dbg !770
  %69 = getelementptr inbounds %struct._Thread* %68, i32 0, i32 4, !dbg !770
  %70 = load volatile i64* %69, align 8, !dbg !770
  %71 = icmp ule i64 %67, %70, !dbg !770
  br i1 %71, label %72, label %200, !dbg !770

; <label>:72                                      ; preds = %55
  %73 = load i64** %LockFor, align 8, !dbg !770
  %74 = load volatile i64* %73, align 8, !dbg !770
  %75 = load i64* %rdv, align 8, !dbg !770
  %76 = icmp eq i64 %74, %75, !dbg !770
  br i1 %76, label %77, label %200, !dbg !770

; <label>:77                                      ; preds = %72
  %78 = load %struct._Thread** %8, align 8, !dbg !771
  %79 = getelementptr inbounds %struct._Thread* %78, i32 0, i32 8, !dbg !771
  %80 = load i32* %79, align 4, !dbg !771
  %81 = icmp ne i32 %80, 0, !dbg !771
  br i1 %81, label %198, label %82, !dbg !771

; <label>:82                                      ; preds = %77
  %83 = load %struct._Thread** %8, align 8, !dbg !773
  %84 = load i64** %LockFor, align 8, !dbg !773
  store %struct._Thread* %83, %struct._Thread** %5, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %5}, metadata !648) #6, !dbg !775
  store i64* %84, i64** %6, align 8
  call void @llvm.dbg.declare(metadata !{i64** %6}, metadata !650) #6, !dbg !775
  call void @llvm.dbg.declare(metadata !{%struct._Log** %k.i}, metadata !651) #6, !dbg !776
  %85 = load %struct._Thread** %5, align 8, !dbg !776
  %86 = getelementptr inbounds %struct._Thread* %85, i32 0, i32 16, !dbg !776
  store %struct._Log* %86, %struct._Log** %k.i, align 8, !dbg !776
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !653) #6, !dbg !777
  %87 = load %struct._Log** %k.i, align 8, !dbg !777
  %88 = getelementptr inbounds %struct._Log* %87, i32 0, i32 1, !dbg !777
  %89 = load %struct._AVPair** %88, align 8, !dbg !777
  store %struct._AVPair* %89, %struct._AVPair** %e.i, align 8, !dbg !777
  %90 = load %struct._AVPair** %e.i, align 8, !dbg !778
  %91 = icmp eq %struct._AVPair* %90, null, !dbg !778
  br i1 %91, label %92, label %181, !dbg !778

; <label>:92                                      ; preds = %82
  %93 = load %struct._Thread** %5, align 8, !dbg !779
  store %struct._Thread* %93, %struct._Thread** %3, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %3}, metadata !658) #6, !dbg !780
  call void @llvm.dbg.declare(metadata !{i64* %rv.i.i}, metadata !660) #6, !dbg !781
  %94 = load %struct._Thread** %3, align 8, !dbg !781
  %95 = getelementptr inbounds %struct._Thread* %94, i32 0, i32 4, !dbg !781
  %96 = load volatile i64* %95, align 8, !dbg !781
  store i64 %96, i64* %rv.i.i, align 8, !dbg !781
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i.i}, metadata !662) #6, !dbg !782
  %97 = load %struct._Thread** %3, align 8, !dbg !782
  %98 = getelementptr inbounds %struct._Thread* %97, i32 0, i32 16, !dbg !782
  store %struct._Log* %98, %struct._Log** %rd.i.i, align 8, !dbg !782
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList.i.i}, metadata !665) #6, !dbg !783
  %99 = load %struct._Log** %rd.i.i, align 8, !dbg !783
  %100 = getelementptr inbounds %struct._Log* %99, i32 0, i32 1, !dbg !783
  %101 = load %struct._AVPair** %100, align 8, !dbg !783
  store %struct._AVPair* %101, %struct._AVPair** %EndOfList.i.i, align 8, !dbg !783
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !667) #6, !dbg !784
  %102 = load %struct._Log** %rd.i.i, align 8, !dbg !785
  %103 = getelementptr inbounds %struct._Log* %102, i32 0, i32 0, !dbg !785
  %104 = load %struct._AVPair** %103, align 8, !dbg !785
  store %struct._AVPair* %104, %struct._AVPair** %e.i.i, align 8, !dbg !785
  br label %105, !dbg !785

; <label>:105                                     ; preds = %135, %92
  %106 = load %struct._AVPair** %e.i.i, align 8, !dbg !785
  %107 = load %struct._AVPair** %EndOfList.i.i, align 8, !dbg !785
  %108 = icmp ne %struct._AVPair* %106, %107, !dbg !785
  br i1 %108, label %109, label %139, !dbg !785

; <label>:109                                     ; preds = %105
  call void @llvm.dbg.declare(metadata !{i64* %v.i.i}, metadata !671) #6, !dbg !786
  %110 = load %struct._AVPair** %e.i.i, align 8, !dbg !786
  %111 = getelementptr inbounds %struct._AVPair* %110, i32 0, i32 4, !dbg !786
  %112 = load i64** %111, align 8, !dbg !786
  %113 = load volatile i64* %112, align 8, !dbg !786
  store i64 %113, i64* %v.i.i, align 8, !dbg !786
  %114 = load i64* %v.i.i, align 8, !dbg !787
  %115 = and i64 %114, 1, !dbg !787
  %116 = icmp ne i64 %115, 0, !dbg !787
  br i1 %116, label %117, label %129, !dbg !787

; <label>:117                                     ; preds = %109
  %118 = load i64* %v.i.i, align 8, !dbg !788
  %119 = xor i64 %118, 1, !dbg !788
  %120 = inttoptr i64 %119 to %struct._AVPair*, !dbg !788
  %121 = getelementptr inbounds %struct._AVPair* %120, i32 0, i32 7, !dbg !788
  %122 = load %struct._Thread** %121, align 8, !dbg !788
  %123 = ptrtoint %struct._Thread* %122 to i64, !dbg !788
  %124 = load %struct._Thread** %3, align 8, !dbg !788
  %125 = ptrtoint %struct._Thread* %124 to i64, !dbg !788
  %126 = icmp ne i64 %123, %125, !dbg !788
  br i1 %126, label %127, label %128, !dbg !788

; <label>:127                                     ; preds = %117
  store i64 0, i64* %2, !dbg !789
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !789

; <label>:128                                     ; preds = %117
  br label %135, !dbg !790

; <label>:129                                     ; preds = %109
  %130 = load i64* %v.i.i, align 8, !dbg !791
  %131 = load i64* %rv.i.i, align 8, !dbg !791
  %132 = icmp ugt i64 %130, %131, !dbg !791
  br i1 %132, label %133, label %134, !dbg !791

; <label>:133                                     ; preds = %129
  store i64 0, i64* %2, !dbg !792
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !792

; <label>:134                                     ; preds = %129
  br label %135

; <label>:135                                     ; preds = %134, %128
  %136 = load %struct._AVPair** %e.i.i, align 8, !dbg !785
  %137 = getelementptr inbounds %struct._AVPair* %136, i32 0, i32 0, !dbg !785
  %138 = load %struct._AVPair** %137, align 8, !dbg !785
  store %struct._AVPair* %138, %struct._AVPair** %e.i.i, align 8, !dbg !785
  br label %105, !dbg !785

; <label>:139                                     ; preds = %105
  store i64 1, i64* %2, !dbg !793
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !793

ReadSetCoherentPessimistic.exit.i:                ; preds = %139, %133, %127
  %140 = load i64* %2, !dbg !793
  %141 = icmp ne i64 %140, 0, !dbg !779
  br i1 %141, label %143, label %142, !dbg !779

; <label>:142                                     ; preds = %ReadSetCoherentPessimistic.exit.i
  store i32 0, i32* %4, !dbg !794
  br label %TrackLoad.exit, !dbg !794

; <label>:143                                     ; preds = %ReadSetCoherentPessimistic.exit.i
  %144 = load %struct._Log** %k.i, align 8, !dbg !795
  %145 = getelementptr inbounds %struct._Log* %144, i32 0, i32 4, !dbg !795
  %146 = load i64* %145, align 8, !dbg !795
  %147 = add nsw i64 %146, 1, !dbg !795
  store i64 %147, i64* %145, align 8, !dbg !795
  %148 = load %struct._Log** %k.i, align 8, !dbg !796
  %149 = getelementptr inbounds %struct._Log* %148, i32 0, i32 2, !dbg !796
  %150 = load %struct._AVPair** %149, align 8, !dbg !796
  store %struct._AVPair* %150, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !689) #6, !dbg !797
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i1.i}, metadata !691) #6, !dbg !798
  %151 = call noalias i8* @malloc(i64 72) #6, !dbg !798
  %152 = bitcast i8* %151 to %struct._AVPair*, !dbg !798
  store %struct._AVPair* %152, %struct._AVPair** %e.i1.i, align 8, !dbg !798
  %153 = load %struct._AVPair** %e.i1.i, align 8, !dbg !799
  %154 = icmp ne %struct._AVPair* %153, null, !dbg !799
  br i1 %154, label %ExtendList.exit.i, label %155, !dbg !799

; <label>:155                                     ; preds = %143
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !799
  unreachable, !dbg !799

ExtendList.exit.i:                                ; preds = %143
  %156 = load %struct._AVPair** %e.i1.i, align 8, !dbg !800
  %157 = bitcast %struct._AVPair* %156 to i8*, !dbg !800
  call void @llvm.memset.p0i8.i64(i8* %157, i8 0, i64 72, i32 8, i1 false) #6, !dbg !800
  %158 = load %struct._AVPair** %e.i1.i, align 8, !dbg !801
  %159 = load %struct._AVPair** %1, align 8, !dbg !801
  %160 = getelementptr inbounds %struct._AVPair* %159, i32 0, i32 0, !dbg !801
  store %struct._AVPair* %158, %struct._AVPair** %160, align 8, !dbg !801
  %161 = load %struct._AVPair** %1, align 8, !dbg !802
  %162 = load %struct._AVPair** %e.i1.i, align 8, !dbg !802
  %163 = getelementptr inbounds %struct._AVPair* %162, i32 0, i32 1, !dbg !802
  store %struct._AVPair* %161, %struct._AVPair** %163, align 8, !dbg !802
  %164 = load %struct._AVPair** %e.i1.i, align 8, !dbg !803
  %165 = getelementptr inbounds %struct._AVPair* %164, i32 0, i32 0, !dbg !803
  store %struct._AVPair* null, %struct._AVPair** %165, align 8, !dbg !803
  %166 = load %struct._AVPair** %1, align 8, !dbg !804
  %167 = getelementptr inbounds %struct._AVPair* %166, i32 0, i32 7, !dbg !804
  %168 = load %struct._Thread** %167, align 8, !dbg !804
  %169 = load %struct._AVPair** %e.i1.i, align 8, !dbg !804
  %170 = getelementptr inbounds %struct._AVPair* %169, i32 0, i32 7, !dbg !804
  store %struct._Thread* %168, %struct._Thread** %170, align 8, !dbg !804
  %171 = load %struct._AVPair** %1, align 8, !dbg !805
  %172 = getelementptr inbounds %struct._AVPair* %171, i32 0, i32 8, !dbg !805
  %173 = load i64* %172, align 8, !dbg !805
  %174 = add nsw i64 %173, 1, !dbg !805
  %175 = load %struct._AVPair** %e.i1.i, align 8, !dbg !805
  %176 = getelementptr inbounds %struct._AVPair* %175, i32 0, i32 8, !dbg !805
  store i64 %174, i64* %176, align 8, !dbg !805
  %177 = load %struct._AVPair** %e.i1.i, align 8, !dbg !806
  store %struct._AVPair* %177, %struct._AVPair** %e.i, align 8, !dbg !796
  %178 = load %struct._AVPair** %e.i, align 8, !dbg !807
  %179 = load %struct._Log** %k.i, align 8, !dbg !807
  %180 = getelementptr inbounds %struct._Log* %179, i32 0, i32 3, !dbg !807
  store %struct._AVPair* %178, %struct._AVPair** %180, align 8, !dbg !807
  br label %181, !dbg !808

; <label>:181                                     ; preds = %ExtendList.exit.i, %82
  %182 = load %struct._AVPair** %e.i, align 8, !dbg !809
  %183 = load %struct._Log** %k.i, align 8, !dbg !809
  %184 = getelementptr inbounds %struct._Log* %183, i32 0, i32 2, !dbg !809
  store %struct._AVPair* %182, %struct._AVPair** %184, align 8, !dbg !809
  %185 = load %struct._AVPair** %e.i, align 8, !dbg !810
  %186 = getelementptr inbounds %struct._AVPair* %185, i32 0, i32 0, !dbg !810
  %187 = load %struct._AVPair** %186, align 8, !dbg !810
  %188 = load %struct._Log** %k.i, align 8, !dbg !810
  %189 = getelementptr inbounds %struct._Log* %188, i32 0, i32 1, !dbg !810
  store %struct._AVPair* %187, %struct._AVPair** %189, align 8, !dbg !810
  %190 = load i64** %6, align 8, !dbg !811
  %191 = load %struct._AVPair** %e.i, align 8, !dbg !811
  %192 = getelementptr inbounds %struct._AVPair* %191, i32 0, i32 4, !dbg !811
  store i64* %190, i64** %192, align 8, !dbg !811
  store i32 1, i32* %4, !dbg !812
  br label %TrackLoad.exit, !dbg !812

TrackLoad.exit:                                   ; preds = %142, %181
  %193 = load i32* %4, !dbg !812
  %194 = icmp ne i32 %193, 0, !dbg !773
  br i1 %194, label %197, label %195, !dbg !773

; <label>:195                                     ; preds = %TrackLoad.exit
  %196 = load %struct._Thread** %8, align 8, !dbg !813
  call void @TxAbort(%struct._Thread* %196), !dbg !813
  br label %197, !dbg !815

; <label>:197                                     ; preds = %195, %TrackLoad.exit
  br label %198, !dbg !816

; <label>:198                                     ; preds = %197, %77
  %199 = load i64* %Valu, align 8, !dbg !817
  store i64 %199, i64* %7, !dbg !817
  br label %205, !dbg !817

; <label>:200                                     ; preds = %72, %55
  %201 = load i64* %rdv, align 8, !dbg !818
  %202 = load %struct._Thread** %8, align 8, !dbg !818
  %203 = getelementptr inbounds %struct._Thread* %202, i32 0, i32 6, !dbg !818
  store i64 %201, i64* %203, align 8, !dbg !818
  %204 = load %struct._Thread** %8, align 8, !dbg !819
  call void @TxAbort(%struct._Thread* %204), !dbg !819
  store i64 0, i64* %7, !dbg !820
  br label %205, !dbg !820

; <label>:205                                     ; preds = %200, %198, %45
  %206 = load i64* %7, !dbg !821
  ret i64 %206, !dbg !821
}

; Function Attrs: nounwind uwtable
define void @TxStoreLocal(%struct._Thread* %Self, i64* %Addr, i64 %Valu) #0 {
  %1 = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Log*, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %5 = alloca %struct._Thread*, align 8
  %6 = alloca i64*, align 8
  %7 = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %5, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %5}, metadata !822), !dbg !823
  store i64* %Addr, i64** %6, align 8
  call void @llvm.dbg.declare(metadata !{i64** %6}, metadata !824), !dbg !823
  store i64 %Valu, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !825), !dbg !823
  %8 = load %struct._Thread** %5, align 8, !dbg !826
  %9 = getelementptr inbounds %struct._Thread* %8, i32 0, i32 18, !dbg !826
  %10 = load i64** %6, align 8, !dbg !826
  %11 = load i64** %6, align 8, !dbg !826
  %12 = load volatile i64* %11, align 8, !dbg !826
  store %struct._Log* %9, %struct._Log** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %2}, metadata !827) #6, !dbg !828
  store i64* %10, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !829) #6, !dbg !828
  store i64 %12, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !830) #6, !dbg !828
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !831) #6, !dbg !832
  %13 = load %struct._Log** %2, align 8, !dbg !832
  %14 = getelementptr inbounds %struct._Log* %13, i32 0, i32 1, !dbg !832
  %15 = load %struct._AVPair** %14, align 8, !dbg !832
  store %struct._AVPair* %15, %struct._AVPair** %e.i, align 8, !dbg !832
  %16 = load %struct._AVPair** %e.i, align 8, !dbg !833
  %17 = icmp eq %struct._AVPair* %16, null, !dbg !833
  br i1 %17, label %18, label %SaveForRollBack.exit, !dbg !833

; <label>:18                                      ; preds = %0
  %19 = load %struct._Log** %2, align 8, !dbg !834
  %20 = getelementptr inbounds %struct._Log* %19, i32 0, i32 4, !dbg !834
  %21 = load i64* %20, align 8, !dbg !834
  %22 = add nsw i64 %21, 1, !dbg !834
  store i64 %22, i64* %20, align 8, !dbg !834
  %23 = load %struct._Log** %2, align 8, !dbg !836
  %24 = getelementptr inbounds %struct._Log* %23, i32 0, i32 2, !dbg !836
  %25 = load %struct._AVPair** %24, align 8, !dbg !836
  store %struct._AVPair* %25, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !689) #6, !dbg !837
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !691) #6, !dbg !838
  %26 = call noalias i8* @malloc(i64 72) #6, !dbg !838
  %27 = bitcast i8* %26 to %struct._AVPair*, !dbg !838
  store %struct._AVPair* %27, %struct._AVPair** %e.i.i, align 8, !dbg !838
  %28 = load %struct._AVPair** %e.i.i, align 8, !dbg !839
  %29 = icmp ne %struct._AVPair* %28, null, !dbg !839
  br i1 %29, label %ExtendList.exit.i, label %30, !dbg !839

; <label>:30                                      ; preds = %18
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !839
  unreachable, !dbg !839

ExtendList.exit.i:                                ; preds = %18
  %31 = load %struct._AVPair** %e.i.i, align 8, !dbg !840
  %32 = bitcast %struct._AVPair* %31 to i8*, !dbg !840
  call void @llvm.memset.p0i8.i64(i8* %32, i8 0, i64 72, i32 8, i1 false) #6, !dbg !840
  %33 = load %struct._AVPair** %e.i.i, align 8, !dbg !841
  %34 = load %struct._AVPair** %1, align 8, !dbg !841
  %35 = getelementptr inbounds %struct._AVPair* %34, i32 0, i32 0, !dbg !841
  store %struct._AVPair* %33, %struct._AVPair** %35, align 8, !dbg !841
  %36 = load %struct._AVPair** %1, align 8, !dbg !842
  %37 = load %struct._AVPair** %e.i.i, align 8, !dbg !842
  %38 = getelementptr inbounds %struct._AVPair* %37, i32 0, i32 1, !dbg !842
  store %struct._AVPair* %36, %struct._AVPair** %38, align 8, !dbg !842
  %39 = load %struct._AVPair** %e.i.i, align 8, !dbg !843
  %40 = getelementptr inbounds %struct._AVPair* %39, i32 0, i32 0, !dbg !843
  store %struct._AVPair* null, %struct._AVPair** %40, align 8, !dbg !843
  %41 = load %struct._AVPair** %1, align 8, !dbg !844
  %42 = getelementptr inbounds %struct._AVPair* %41, i32 0, i32 7, !dbg !844
  %43 = load %struct._Thread** %42, align 8, !dbg !844
  %44 = load %struct._AVPair** %e.i.i, align 8, !dbg !844
  %45 = getelementptr inbounds %struct._AVPair* %44, i32 0, i32 7, !dbg !844
  store %struct._Thread* %43, %struct._Thread** %45, align 8, !dbg !844
  %46 = load %struct._AVPair** %1, align 8, !dbg !845
  %47 = getelementptr inbounds %struct._AVPair* %46, i32 0, i32 8, !dbg !845
  %48 = load i64* %47, align 8, !dbg !845
  %49 = add nsw i64 %48, 1, !dbg !845
  %50 = load %struct._AVPair** %e.i.i, align 8, !dbg !845
  %51 = getelementptr inbounds %struct._AVPair* %50, i32 0, i32 8, !dbg !845
  store i64 %49, i64* %51, align 8, !dbg !845
  %52 = load %struct._AVPair** %e.i.i, align 8, !dbg !846
  store %struct._AVPair* %52, %struct._AVPair** %e.i, align 8, !dbg !836
  %53 = load %struct._AVPair** %e.i, align 8, !dbg !847
  %54 = load %struct._Log** %2, align 8, !dbg !847
  %55 = getelementptr inbounds %struct._Log* %54, i32 0, i32 3, !dbg !847
  store %struct._AVPair* %53, %struct._AVPair** %55, align 8, !dbg !847
  br label %SaveForRollBack.exit, !dbg !848

SaveForRollBack.exit:                             ; preds = %0, %ExtendList.exit.i
  %56 = load %struct._AVPair** %e.i, align 8, !dbg !849
  %57 = load %struct._Log** %2, align 8, !dbg !849
  %58 = getelementptr inbounds %struct._Log* %57, i32 0, i32 2, !dbg !849
  store %struct._AVPair* %56, %struct._AVPair** %58, align 8, !dbg !849
  %59 = load %struct._AVPair** %e.i, align 8, !dbg !850
  %60 = getelementptr inbounds %struct._AVPair* %59, i32 0, i32 0, !dbg !850
  %61 = load %struct._AVPair** %60, align 8, !dbg !850
  %62 = load %struct._Log** %2, align 8, !dbg !850
  %63 = getelementptr inbounds %struct._Log* %62, i32 0, i32 1, !dbg !850
  store %struct._AVPair* %61, %struct._AVPair** %63, align 8, !dbg !850
  %64 = load i64** %3, align 8, !dbg !851
  %65 = load %struct._AVPair** %e.i, align 8, !dbg !851
  %66 = getelementptr inbounds %struct._AVPair* %65, i32 0, i32 2, !dbg !851
  store i64* %64, i64** %66, align 8, !dbg !851
  %67 = load i64* %4, align 8, !dbg !852
  %68 = load %struct._AVPair** %e.i, align 8, !dbg !852
  %69 = getelementptr inbounds %struct._AVPair* %68, i32 0, i32 3, !dbg !852
  store i64 %67, i64* %69, align 8, !dbg !852
  %70 = load %struct._AVPair** %e.i, align 8, !dbg !853
  %71 = getelementptr inbounds %struct._AVPair* %70, i32 0, i32 4, !dbg !853
  store i64* null, i64** %71, align 8, !dbg !853
  %72 = load i64* %7, align 8, !dbg !854
  %73 = load i64** %6, align 8, !dbg !854
  store volatile i64 %72, i64* %73, align 8, !dbg !854
  ret void, !dbg !855
}

; Function Attrs: nounwind uwtable
define void @TxStart(%struct._Thread* %Self, [1 x %struct.__jmp_buf_tag]* %envPtr, i32* %ROFlag) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca %struct._Thread*, align 8
  %3 = alloca %struct._Thread*, align 8
  %4 = alloca [1 x %struct.__jmp_buf_tag]*, align 8
  %5 = alloca i32*, align 8
  store %struct._Thread* %Self, %struct._Thread** %3, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %3}, metadata !856), !dbg !857
  store [1 x %struct.__jmp_buf_tag]* %envPtr, [1 x %struct.__jmp_buf_tag]** %4, align 8
  call void @llvm.dbg.declare(metadata !{[1 x %struct.__jmp_buf_tag]** %4}, metadata !858), !dbg !857
  store i32* %ROFlag, i32** %5, align 8
  call void @llvm.dbg.declare(metadata !{i32** %5}, metadata !859), !dbg !857
  %6 = load %struct._Thread** %3, align 8, !dbg !860
  store %struct._Thread* %6, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !861), !dbg !862
  %7 = load %struct._Thread** %2, align 8, !dbg !863
  %8 = getelementptr inbounds %struct._Thread* %7, i32 0, i32 1, !dbg !863
  store volatile i64 0, i64* %8, align 8, !dbg !863
  %9 = load %struct._Thread** %2, align 8, !dbg !864
  %10 = getelementptr inbounds %struct._Thread* %9, i32 0, i32 17, !dbg !864
  %11 = getelementptr inbounds %struct._Log* %10, i32 0, i32 0, !dbg !864
  %12 = load %struct._AVPair** %11, align 8, !dbg !864
  %13 = load %struct._Thread** %2, align 8, !dbg !864
  %14 = getelementptr inbounds %struct._Thread* %13, i32 0, i32 17, !dbg !864
  %15 = getelementptr inbounds %struct._Log* %14, i32 0, i32 1, !dbg !864
  store %struct._AVPair* %12, %struct._AVPair** %15, align 8, !dbg !864
  %16 = load %struct._Thread** %2, align 8, !dbg !865
  %17 = getelementptr inbounds %struct._Thread* %16, i32 0, i32 17, !dbg !865
  %18 = getelementptr inbounds %struct._Log* %17, i32 0, i32 2, !dbg !865
  store %struct._AVPair* null, %struct._AVPair** %18, align 8, !dbg !865
  %19 = load %struct._Thread** %2, align 8, !dbg !866
  %20 = getelementptr inbounds %struct._Thread* %19, i32 0, i32 17, !dbg !866
  %21 = getelementptr inbounds %struct._Log* %20, i32 0, i32 5, !dbg !866
  store i32 0, i32* %21, align 4, !dbg !866
  %22 = load %struct._Thread** %2, align 8, !dbg !867
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 16, !dbg !867
  %24 = getelementptr inbounds %struct._Log* %23, i32 0, i32 0, !dbg !867
  %25 = load %struct._AVPair** %24, align 8, !dbg !867
  %26 = load %struct._Thread** %2, align 8, !dbg !867
  %27 = getelementptr inbounds %struct._Thread* %26, i32 0, i32 16, !dbg !867
  %28 = getelementptr inbounds %struct._Log* %27, i32 0, i32 1, !dbg !867
  store %struct._AVPair* %25, %struct._AVPair** %28, align 8, !dbg !867
  %29 = load %struct._Thread** %2, align 8, !dbg !868
  %30 = getelementptr inbounds %struct._Thread* %29, i32 0, i32 16, !dbg !868
  %31 = getelementptr inbounds %struct._Log* %30, i32 0, i32 2, !dbg !868
  store %struct._AVPair* null, %struct._AVPair** %31, align 8, !dbg !868
  %32 = load %struct._Thread** %2, align 8, !dbg !869
  %33 = getelementptr inbounds %struct._Thread* %32, i32 0, i32 18, !dbg !869
  %34 = getelementptr inbounds %struct._Log* %33, i32 0, i32 0, !dbg !869
  %35 = load %struct._AVPair** %34, align 8, !dbg !869
  %36 = load %struct._Thread** %2, align 8, !dbg !869
  %37 = getelementptr inbounds %struct._Thread* %36, i32 0, i32 18, !dbg !869
  %38 = getelementptr inbounds %struct._Log* %37, i32 0, i32 1, !dbg !869
  store %struct._AVPair* %35, %struct._AVPair** %38, align 8, !dbg !869
  %39 = load %struct._Thread** %2, align 8, !dbg !870
  %40 = getelementptr inbounds %struct._Thread* %39, i32 0, i32 18, !dbg !870
  %41 = getelementptr inbounds %struct._Log* %40, i32 0, i32 2, !dbg !870
  store %struct._AVPair* null, %struct._AVPair** %41, align 8, !dbg !870
  %42 = load %struct._Thread** %2, align 8, !dbg !871
  %43 = getelementptr inbounds %struct._Thread* %42, i32 0, i32 2, !dbg !871
  store volatile i64 0, i64* %43, align 8, !dbg !871
  %44 = load %struct._Thread** %3, align 8, !dbg !872
  store %struct._Thread* %44, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !873), !dbg !874
  %45 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !875
  %46 = load %struct._Thread** %3, align 8, !dbg !872
  %47 = getelementptr inbounds %struct._Thread* %46, i32 0, i32 4, !dbg !872
  store volatile i64 %45, i64* %47, align 8, !dbg !872
  %48 = load %struct._Thread** %3, align 8, !dbg !876
  %49 = getelementptr inbounds %struct._Thread* %48, i32 0, i32 1, !dbg !876
  store volatile i64 1, i64* %49, align 8, !dbg !876
  %50 = load i32** %5, align 8, !dbg !877
  %51 = load %struct._Thread** %3, align 8, !dbg !877
  %52 = getelementptr inbounds %struct._Thread* %51, i32 0, i32 7, !dbg !877
  store i32* %50, i32** %52, align 8, !dbg !877
  %53 = load i32** %5, align 8, !dbg !878
  %54 = icmp ne i32* %53, null, !dbg !878
  br i1 %54, label %55, label %58, !dbg !878

; <label>:55                                      ; preds = %0
  %56 = load i32** %5, align 8, !dbg !878
  %57 = load i32* %56, align 4, !dbg !878
  br label %59, !dbg !878

; <label>:58                                      ; preds = %0
  br label %59, !dbg !878

; <label>:59                                      ; preds = %58, %55
  %60 = phi i32 [ %57, %55 ], [ 0, %58 ], !dbg !878
  %61 = load %struct._Thread** %3, align 8, !dbg !878
  %62 = getelementptr inbounds %struct._Thread* %61, i32 0, i32 8, !dbg !878
  store i32 %60, i32* %62, align 4, !dbg !878
  %63 = load [1 x %struct.__jmp_buf_tag]** %4, align 8, !dbg !879
  %64 = load %struct._Thread** %3, align 8, !dbg !879
  %65 = getelementptr inbounds %struct._Thread* %64, i32 0, i32 19, !dbg !879
  store [1 x %struct.__jmp_buf_tag]* %63, [1 x %struct.__jmp_buf_tag]** %65, align 8, !dbg !879
  %66 = load %struct._Thread** %3, align 8, !dbg !880
  %67 = getelementptr inbounds %struct._Thread* %66, i32 0, i32 9, !dbg !880
  %68 = load i64* %67, align 8, !dbg !880
  %69 = add nsw i64 %68, 1, !dbg !880
  store i64 %69, i64* %67, align 8, !dbg !880
  ret void, !dbg !881
}

; Function Attrs: nounwind uwtable
define i32 @TxCommit(%struct._Thread* %Self) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca %struct._Thread*, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  %prevVal.i8.i = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64, align 8
  %8 = alloca i64*, align 8
  %prevVal.i.i = alloca i64, align 8
  %9 = alloca %struct._AVPair*, align 8
  %10 = alloca %struct._Log*, align 8
  %11 = alloca i64*, align 8
  %e.i5.i = alloca %struct._AVPair*, align 8
  %End.i6.i = alloca %struct._AVPair*, align 8
  %12 = alloca i64, align 8
  %13 = alloca i64, align 8
  %14 = alloca i64*, align 8
  %prevVal.i.i.i = alloca i64, align 8
  %15 = alloca %struct._Thread*, align 8
  %16 = alloca i64, align 8
  %gv.i.i = alloca i64, align 8
  %wv.i.i = alloca i64, align 8
  %k.i.i = alloca i64, align 8
  %17 = alloca %struct._Thread*, align 8
  %dx.i.i = alloca i64, align 8
  %rv.i.i = alloca i64, align 8
  %rd.i.i = alloca %struct._Log*, align 8
  %EndOfList.i.i = alloca %struct._AVPair*, align 8
  %e.i4.i = alloca %struct._AVPair*, align 8
  %v.i.i = alloca i64, align 8
  %18 = alloca %struct._Log*, align 8
  %e.i2.i = alloca %struct._AVPair*, align 8
  %End.i3.i = alloca %struct._AVPair*, align 8
  %19 = alloca %struct._Thread*, align 8
  %20 = alloca i64, align 8
  %wr.i.i = alloca %struct._Log*, align 8
  %p.i.i = alloca %struct._AVPair*, align 8
  %End.i1.i = alloca %struct._AVPair*, align 8
  %21 = alloca %struct._AVPair*, align 8
  %22 = alloca %struct._Log*, align 8
  %23 = alloca i64*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %End.i.i = alloca %struct._AVPair*, align 8
  %24 = alloca i8*, align 8
  %25 = alloca i64, align 8
  %26 = alloca %struct._Thread*, align 8
  %wr.i = alloca %struct._Log*, align 8
  %rd.i = alloca %struct._Log*, align 8
  %ctr.i = alloca i64, align 8
  %wv.i = alloca i64, align 8
  %maxv.i = alloca i64, align 8
  %p.i = alloca %struct._AVPair*, align 8
  %End.i = alloca %struct._AVPair*, align 8
  %LockFor.i = alloca i64*, align 8
  %cv.i = alloca i64, align 8
  %c.i = alloca i64, align 8
  %27 = alloca %struct._Thread*, align 8
  %28 = alloca %struct._Thread*, align 8
  %29 = alloca i32, align 4
  %30 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %30, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %30}, metadata !882), !dbg !883
  %31 = load %struct._Thread** %30, align 8, !dbg !884
  %32 = getelementptr inbounds %struct._Thread* %31, i32 0, i32 17, !dbg !884
  %33 = getelementptr inbounds %struct._Log* %32, i32 0, i32 1, !dbg !884
  %34 = load %struct._AVPair** %33, align 8, !dbg !884
  %35 = load %struct._Thread** %30, align 8, !dbg !884
  %36 = getelementptr inbounds %struct._Thread* %35, i32 0, i32 17, !dbg !884
  %37 = getelementptr inbounds %struct._Log* %36, i32 0, i32 0, !dbg !884
  %38 = load %struct._AVPair** %37, align 8, !dbg !884
  %39 = icmp eq %struct._AVPair* %34, %38, !dbg !884
  br i1 %39, label %40, label %88, !dbg !884

; <label>:40                                      ; preds = %0
  %41 = load %struct._Thread** %30, align 8, !dbg !885
  store %struct._Thread* %41, %struct._Thread** %28, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %28}, metadata !887), !dbg !888
  %42 = load %struct._Thread** %28, align 8, !dbg !889
  store %struct._Thread* %42, %struct._Thread** %27, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %27}, metadata !861), !dbg !890
  %43 = load %struct._Thread** %27, align 8, !dbg !891
  %44 = getelementptr inbounds %struct._Thread* %43, i32 0, i32 1, !dbg !891
  store volatile i64 0, i64* %44, align 8, !dbg !891
  %45 = load %struct._Thread** %27, align 8, !dbg !892
  %46 = getelementptr inbounds %struct._Thread* %45, i32 0, i32 17, !dbg !892
  %47 = getelementptr inbounds %struct._Log* %46, i32 0, i32 0, !dbg !892
  %48 = load %struct._AVPair** %47, align 8, !dbg !892
  %49 = load %struct._Thread** %27, align 8, !dbg !892
  %50 = getelementptr inbounds %struct._Thread* %49, i32 0, i32 17, !dbg !892
  %51 = getelementptr inbounds %struct._Log* %50, i32 0, i32 1, !dbg !892
  store %struct._AVPair* %48, %struct._AVPair** %51, align 8, !dbg !892
  %52 = load %struct._Thread** %27, align 8, !dbg !893
  %53 = getelementptr inbounds %struct._Thread* %52, i32 0, i32 17, !dbg !893
  %54 = getelementptr inbounds %struct._Log* %53, i32 0, i32 2, !dbg !893
  store %struct._AVPair* null, %struct._AVPair** %54, align 8, !dbg !893
  %55 = load %struct._Thread** %27, align 8, !dbg !894
  %56 = getelementptr inbounds %struct._Thread* %55, i32 0, i32 17, !dbg !894
  %57 = getelementptr inbounds %struct._Log* %56, i32 0, i32 5, !dbg !894
  store i32 0, i32* %57, align 4, !dbg !894
  %58 = load %struct._Thread** %27, align 8, !dbg !895
  %59 = getelementptr inbounds %struct._Thread* %58, i32 0, i32 16, !dbg !895
  %60 = getelementptr inbounds %struct._Log* %59, i32 0, i32 0, !dbg !895
  %61 = load %struct._AVPair** %60, align 8, !dbg !895
  %62 = load %struct._Thread** %27, align 8, !dbg !895
  %63 = getelementptr inbounds %struct._Thread* %62, i32 0, i32 16, !dbg !895
  %64 = getelementptr inbounds %struct._Log* %63, i32 0, i32 1, !dbg !895
  store %struct._AVPair* %61, %struct._AVPair** %64, align 8, !dbg !895
  %65 = load %struct._Thread** %27, align 8, !dbg !896
  %66 = getelementptr inbounds %struct._Thread* %65, i32 0, i32 16, !dbg !896
  %67 = getelementptr inbounds %struct._Log* %66, i32 0, i32 2, !dbg !896
  store %struct._AVPair* null, %struct._AVPair** %67, align 8, !dbg !896
  %68 = load %struct._Thread** %27, align 8, !dbg !897
  %69 = getelementptr inbounds %struct._Thread* %68, i32 0, i32 18, !dbg !897
  %70 = getelementptr inbounds %struct._Log* %69, i32 0, i32 0, !dbg !897
  %71 = load %struct._AVPair** %70, align 8, !dbg !897
  %72 = load %struct._Thread** %27, align 8, !dbg !897
  %73 = getelementptr inbounds %struct._Thread* %72, i32 0, i32 18, !dbg !897
  %74 = getelementptr inbounds %struct._Log* %73, i32 0, i32 1, !dbg !897
  store %struct._AVPair* %71, %struct._AVPair** %74, align 8, !dbg !897
  %75 = load %struct._Thread** %27, align 8, !dbg !898
  %76 = getelementptr inbounds %struct._Thread* %75, i32 0, i32 18, !dbg !898
  %77 = getelementptr inbounds %struct._Log* %76, i32 0, i32 2, !dbg !898
  store %struct._AVPair* null, %struct._AVPair** %77, align 8, !dbg !898
  %78 = load %struct._Thread** %27, align 8, !dbg !899
  %79 = getelementptr inbounds %struct._Thread* %78, i32 0, i32 2, !dbg !899
  store volatile i64 0, i64* %79, align 8, !dbg !899
  %80 = load %struct._Thread** %28, align 8, !dbg !900
  %81 = getelementptr inbounds %struct._Thread* %80, i32 0, i32 3, !dbg !900
  store volatile i64 0, i64* %81, align 8, !dbg !900
  %82 = load %struct._Thread** %30, align 8, !dbg !901
  %83 = getelementptr inbounds %struct._Thread* %82, i32 0, i32 14, !dbg !901
  %84 = load %struct.tmalloc** %83, align 8, !dbg !901
  call void @tmalloc_clear(%struct.tmalloc* %84), !dbg !901
  %85 = load %struct._Thread** %30, align 8, !dbg !902
  %86 = getelementptr inbounds %struct._Thread* %85, i32 0, i32 15, !dbg !902
  %87 = load %struct.tmalloc** %86, align 8, !dbg !902
  call void @tmalloc_releaseAllForward(%struct.tmalloc* %87, void (i8*, i64)* @txSterilize), !dbg !902
  store i32 1, i32* %29, !dbg !903
  br label %462, !dbg !903

; <label>:88                                      ; preds = %0
  %89 = load %struct._Thread** %30, align 8, !dbg !904
  store %struct._Thread* %89, %struct._Thread** %26, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %26}, metadata !905) #6, !dbg !906
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr.i}, metadata !907) #6, !dbg !908
  %90 = load %struct._Thread** %26, align 8, !dbg !908
  %91 = getelementptr inbounds %struct._Thread* %90, i32 0, i32 17, !dbg !908
  store %struct._Log* %91, %struct._Log** %wr.i, align 8, !dbg !908
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i}, metadata !909) #6, !dbg !910
  %92 = load %struct._Thread** %26, align 8, !dbg !910
  %93 = getelementptr inbounds %struct._Thread* %92, i32 0, i32 16, !dbg !910
  store %struct._Log* %93, %struct._Log** %rd.i, align 8, !dbg !910
  call void @llvm.dbg.declare(metadata !{i64* %ctr.i}, metadata !911) #6, !dbg !912
  call void @llvm.dbg.declare(metadata !{i64* %wv.i}, metadata !913) #6, !dbg !914
  %94 = load %struct._Thread** %26, align 8, !dbg !915
  %95 = getelementptr inbounds %struct._Thread* %94, i32 0, i32 2, !dbg !915
  store volatile i64 1, i64* %95, align 8, !dbg !915
  store i64 1000, i64* %ctr.i, align 8, !dbg !916
  call void @llvm.dbg.declare(metadata !{i64* %maxv.i}, metadata !917) #6, !dbg !918
  store i64 0, i64* %maxv.i, align 8, !dbg !918
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i}, metadata !919) #6, !dbg !920
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i}, metadata !921) #6, !dbg !923
  %96 = load %struct._Log** %wr.i, align 8, !dbg !923
  %97 = getelementptr inbounds %struct._Log* %96, i32 0, i32 1, !dbg !923
  %98 = load %struct._AVPair** %97, align 8, !dbg !923
  store %struct._AVPair* %98, %struct._AVPair** %End.i, align 8, !dbg !923
  %99 = load %struct._Log** %wr.i, align 8, !dbg !924
  %100 = getelementptr inbounds %struct._Log* %99, i32 0, i32 0, !dbg !924
  %101 = load %struct._AVPair** %100, align 8, !dbg !924
  store %struct._AVPair* %101, %struct._AVPair** %p.i, align 8, !dbg !924
  br label %102, !dbg !924

; <label>:102                                     ; preds = %275, %88
  %103 = load %struct._AVPair** %p.i, align 8, !dbg !924
  %104 = load %struct._AVPair** %End.i, align 8, !dbg !924
  %105 = icmp ne %struct._AVPair* %103, %104, !dbg !924
  br i1 %105, label %106, label %279, !dbg !924

; <label>:106                                     ; preds = %102
  call void @llvm.dbg.declare(metadata !{i64** %LockFor.i}, metadata !926) #6, !dbg !929
  %107 = load %struct._AVPair** %p.i, align 8, !dbg !929
  %108 = getelementptr inbounds %struct._AVPair* %107, i32 0, i32 4, !dbg !929
  %109 = load i64** %108, align 8, !dbg !929
  store i64* %109, i64** %LockFor.i, align 8, !dbg !929
  call void @llvm.dbg.declare(metadata !{i64* %cv.i}, metadata !930) #6, !dbg !931
  %110 = load i64** %LockFor.i, align 8, !dbg !932
  %111 = bitcast i64* %110 to i8*, !dbg !932
  store i8* %111, i8** %24, align 8
  call void @llvm.dbg.declare(metadata !{i8** %24}, metadata !933) #6, !dbg !934
  %112 = load i64** %LockFor.i, align 8, !dbg !935
  %113 = load volatile i64* %112, align 8, !dbg !935
  store i64 %113, i64* %cv.i, align 8, !dbg !935
  %114 = load i64* %cv.i, align 8, !dbg !936
  %115 = and i64 %114, 1, !dbg !936
  %116 = icmp ne i64 %115, 0, !dbg !936
  br i1 %116, label %117, label %169, !dbg !936

; <label>:117                                     ; preds = %106
  %118 = load i64* %cv.i, align 8, !dbg !936
  %119 = xor i64 %118, 1, !dbg !936
  %120 = inttoptr i64 %119 to %struct._AVPair*, !dbg !936
  %121 = getelementptr inbounds %struct._AVPair* %120, i32 0, i32 7, !dbg !936
  %122 = load %struct._Thread** %121, align 8, !dbg !936
  %123 = load %struct._Thread** %26, align 8, !dbg !936
  %124 = icmp eq %struct._Thread* %122, %123, !dbg !936
  br i1 %124, label %125, label %169, !dbg !936

; <label>:125                                     ; preds = %117
  %126 = load %struct._Log** %rd.i, align 8, !dbg !937
  %127 = load i64** %LockFor.i, align 8, !dbg !937
  store %struct._Log* %126, %struct._Log** %22, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %22}, metadata !939) #6, !dbg !940
  store i64* %127, i64** %23, align 8
  call void @llvm.dbg.declare(metadata !{i64** %23}, metadata !941) #6, !dbg !940
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !942) #6, !dbg !944
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i.i}, metadata !945) #6, !dbg !946
  %128 = load %struct._Log** %22, align 8, !dbg !946
  %129 = getelementptr inbounds %struct._Log* %128, i32 0, i32 1, !dbg !946
  %130 = load %struct._AVPair** %129, align 8, !dbg !946
  store %struct._AVPair* %130, %struct._AVPair** %End.i.i, align 8, !dbg !946
  %131 = load %struct._Log** %22, align 8, !dbg !947
  %132 = getelementptr inbounds %struct._Log* %131, i32 0, i32 0, !dbg !947
  %133 = load %struct._AVPair** %132, align 8, !dbg !947
  store %struct._AVPair* %133, %struct._AVPair** %e.i.i, align 8, !dbg !947
  br label %134, !dbg !947

; <label>:134                                     ; preds = %146, %125
  %135 = load %struct._AVPair** %e.i.i, align 8, !dbg !947
  %136 = load %struct._AVPair** %End.i.i, align 8, !dbg !947
  %137 = icmp ne %struct._AVPair* %135, %136, !dbg !947
  br i1 %137, label %138, label %150, !dbg !947

; <label>:138                                     ; preds = %134
  %139 = load %struct._AVPair** %e.i.i, align 8, !dbg !949
  %140 = getelementptr inbounds %struct._AVPair* %139, i32 0, i32 4, !dbg !949
  %141 = load i64** %140, align 8, !dbg !949
  %142 = load i64** %23, align 8, !dbg !949
  %143 = icmp eq i64* %141, %142, !dbg !949
  br i1 %143, label %144, label %146, !dbg !949

; <label>:144                                     ; preds = %138
  %145 = load %struct._AVPair** %e.i.i, align 8, !dbg !951
  store %struct._AVPair* %145, %struct._AVPair** %21, !dbg !951
  br label %FindFirst.exit.i, !dbg !951

; <label>:146                                     ; preds = %138
  %147 = load %struct._AVPair** %e.i.i, align 8, !dbg !947
  %148 = getelementptr inbounds %struct._AVPair* %147, i32 0, i32 0, !dbg !947
  %149 = load %struct._AVPair** %148, align 8, !dbg !947
  store %struct._AVPair* %149, %struct._AVPair** %e.i.i, align 8, !dbg !947
  br label %134, !dbg !947

; <label>:150                                     ; preds = %134
  store %struct._AVPair* null, %struct._AVPair** %21, !dbg !953
  br label %FindFirst.exit.i, !dbg !953

FindFirst.exit.i:                                 ; preds = %150, %144
  %151 = load %struct._AVPair** %21, !dbg !954
  %152 = icmp ne %struct._AVPair* %151, null, !dbg !937
  br i1 %152, label %153, label %168, !dbg !937

; <label>:153                                     ; preds = %FindFirst.exit.i
  %154 = load i64* %cv.i, align 8, !dbg !955
  %155 = xor i64 %154, 1, !dbg !955
  %156 = inttoptr i64 %155 to %struct._AVPair*, !dbg !955
  %157 = getelementptr inbounds %struct._AVPair* %156, i32 0, i32 5, !dbg !955
  %158 = load i64* %157, align 8, !dbg !955
  %159 = load %struct._Thread** %26, align 8, !dbg !955
  %160 = getelementptr inbounds %struct._Thread* %159, i32 0, i32 4, !dbg !955
  %161 = load volatile i64* %160, align 8, !dbg !955
  %162 = icmp ugt i64 %158, %161, !dbg !955
  br i1 %162, label %163, label %167, !dbg !955

; <label>:163                                     ; preds = %153
  %164 = load i64* %cv.i, align 8, !dbg !957
  %165 = load %struct._Thread** %26, align 8, !dbg !957
  %166 = getelementptr inbounds %struct._Thread* %165, i32 0, i32 6, !dbg !957
  store i64 %164, i64* %166, align 8, !dbg !957
  store i64 0, i64* %25, !dbg !959
  br label %TryFastUpdate.exit, !dbg !959

; <label>:167                                     ; preds = %153
  br label %168, !dbg !960

; <label>:168                                     ; preds = %167, %FindFirst.exit.i
  br label %275, !dbg !961

; <label>:169                                     ; preds = %117, %106
  %170 = load %struct._Log** %rd.i, align 8, !dbg !962
  %171 = load i64** %LockFor.i, align 8, !dbg !962
  store %struct._Log* %170, %struct._Log** %10, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %10}, metadata !939) #6, !dbg !963
  store i64* %171, i64** %11, align 8
  call void @llvm.dbg.declare(metadata !{i64** %11}, metadata !941) #6, !dbg !963
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i5.i}, metadata !942) #6, !dbg !964
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i6.i}, metadata !945) #6, !dbg !965
  %172 = load %struct._Log** %10, align 8, !dbg !965
  %173 = getelementptr inbounds %struct._Log* %172, i32 0, i32 1, !dbg !965
  %174 = load %struct._AVPair** %173, align 8, !dbg !965
  store %struct._AVPair* %174, %struct._AVPair** %End.i6.i, align 8, !dbg !965
  %175 = load %struct._Log** %10, align 8, !dbg !966
  %176 = getelementptr inbounds %struct._Log* %175, i32 0, i32 0, !dbg !966
  %177 = load %struct._AVPair** %176, align 8, !dbg !966
  store %struct._AVPair* %177, %struct._AVPair** %e.i5.i, align 8, !dbg !966
  br label %178, !dbg !966

; <label>:178                                     ; preds = %190, %169
  %179 = load %struct._AVPair** %e.i5.i, align 8, !dbg !966
  %180 = load %struct._AVPair** %End.i6.i, align 8, !dbg !966
  %181 = icmp ne %struct._AVPair* %179, %180, !dbg !966
  br i1 %181, label %182, label %194, !dbg !966

; <label>:182                                     ; preds = %178
  %183 = load %struct._AVPair** %e.i5.i, align 8, !dbg !967
  %184 = getelementptr inbounds %struct._AVPair* %183, i32 0, i32 4, !dbg !967
  %185 = load i64** %184, align 8, !dbg !967
  %186 = load i64** %11, align 8, !dbg !967
  %187 = icmp eq i64* %185, %186, !dbg !967
  br i1 %187, label %188, label %190, !dbg !967

; <label>:188                                     ; preds = %182
  %189 = load %struct._AVPair** %e.i5.i, align 8, !dbg !968
  store %struct._AVPair* %189, %struct._AVPair** %9, !dbg !968
  br label %FindFirst.exit7.i, !dbg !968

; <label>:190                                     ; preds = %182
  %191 = load %struct._AVPair** %e.i5.i, align 8, !dbg !966
  %192 = getelementptr inbounds %struct._AVPair* %191, i32 0, i32 0, !dbg !966
  %193 = load %struct._AVPair** %192, align 8, !dbg !966
  store %struct._AVPair* %193, %struct._AVPair** %e.i5.i, align 8, !dbg !966
  br label %178, !dbg !966

; <label>:194                                     ; preds = %178
  store %struct._AVPair* null, %struct._AVPair** %9, !dbg !969
  br label %FindFirst.exit7.i, !dbg !969

FindFirst.exit7.i:                                ; preds = %194, %188
  %195 = load %struct._AVPair** %9, !dbg !970
  %196 = icmp ne %struct._AVPair* %195, null, !dbg !962
  br i1 %196, label %197, label %236, !dbg !962

; <label>:197                                     ; preds = %FindFirst.exit7.i
  %198 = load i64* %cv.i, align 8, !dbg !971
  %199 = and i64 %198, 1, !dbg !971
  %200 = icmp eq i64 %199, 0, !dbg !971
  br i1 %200, label %201, label %232, !dbg !971

; <label>:201                                     ; preds = %197
  %202 = load i64* %cv.i, align 8, !dbg !971
  %203 = load %struct._Thread** %26, align 8, !dbg !971
  %204 = getelementptr inbounds %struct._Thread* %203, i32 0, i32 4, !dbg !971
  %205 = load volatile i64* %204, align 8, !dbg !971
  %206 = icmp ule i64 %202, %205, !dbg !971
  br i1 %206, label %207, label %232, !dbg !971

; <label>:207                                     ; preds = %201
  %208 = load %struct._AVPair** %p.i, align 8, !dbg !973
  %209 = ptrtoint %struct._AVPair* %208 to i64, !dbg !973
  %210 = or i64 %209, 1, !dbg !973
  %211 = load i64* %cv.i, align 8, !dbg !973
  %212 = load i64** %LockFor.i, align 8, !dbg !973
  store i64 %210, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !387) #6, !dbg !974
  store i64 %211, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !389) #6, !dbg !974
  store i64* %212, i64** %5, align 8
  call void @llvm.dbg.declare(metadata !{i64** %5}, metadata !390) #6, !dbg !974
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i8.i}, metadata !391) #6, !dbg !975
  %213 = load i64* %3, align 8, !dbg !976
  %214 = load i64** %5, align 8, !dbg !976
  %215 = load i64* %4, align 8, !dbg !976
  %216 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %213, i64* %214, i64 %215) #6, !dbg !976, !srcloc !395
  store i64 %216, i64* %prevVal.i8.i, align 8, !dbg !976
  %217 = load i64* %prevVal.i8.i, align 8, !dbg !977
  %218 = load i64* %cv.i, align 8, !dbg !973
  %219 = icmp eq i64 %217, %218, !dbg !973
  br i1 %219, label %220, label %232, !dbg !973

; <label>:220                                     ; preds = %207
  %221 = load i64* %cv.i, align 8, !dbg !978
  %222 = load i64* %maxv.i, align 8, !dbg !978
  %223 = icmp ugt i64 %221, %222, !dbg !978
  br i1 %223, label %224, label %226, !dbg !978

; <label>:224                                     ; preds = %220
  %225 = load i64* %cv.i, align 8, !dbg !980
  store i64 %225, i64* %maxv.i, align 8, !dbg !980
  br label %226, !dbg !982

; <label>:226                                     ; preds = %224, %220
  %227 = load i64* %cv.i, align 8, !dbg !983
  %228 = load %struct._AVPair** %p.i, align 8, !dbg !983
  %229 = getelementptr inbounds %struct._AVPair* %228, i32 0, i32 5, !dbg !983
  store i64 %227, i64* %229, align 8, !dbg !983
  %230 = load %struct._AVPair** %p.i, align 8, !dbg !984
  %231 = getelementptr inbounds %struct._AVPair* %230, i32 0, i32 6, !dbg !984
  store i8 1, i8* %231, align 1, !dbg !984
  br label %275, !dbg !985

; <label>:232                                     ; preds = %207, %201, %197
  %233 = load i64* %cv.i, align 8, !dbg !986
  %234 = load %struct._Thread** %26, align 8, !dbg !986
  %235 = getelementptr inbounds %struct._Thread* %234, i32 0, i32 6, !dbg !986
  store i64 %233, i64* %235, align 8, !dbg !986
  store i64 0, i64* %25, !dbg !987
  br label %TryFastUpdate.exit, !dbg !987

; <label>:236                                     ; preds = %FindFirst.exit7.i
  call void @llvm.dbg.declare(metadata !{i64* %c.i}, metadata !988) #6, !dbg !990
  %237 = load i64* %ctr.i, align 8, !dbg !990
  store i64 %237, i64* %c.i, align 8, !dbg !990
  br label %238, !dbg !991

; <label>:238                                     ; preds = %274, %236
  %239 = load i64** %LockFor.i, align 8, !dbg !993
  %240 = load volatile i64* %239, align 8, !dbg !993
  store i64 %240, i64* %cv.i, align 8, !dbg !993
  %241 = load i64* %cv.i, align 8, !dbg !995
  %242 = and i64 %241, 1, !dbg !995
  %243 = icmp eq i64 %242, 0, !dbg !995
  br i1 %243, label %244, label %269, !dbg !995

; <label>:244                                     ; preds = %238
  %245 = load %struct._AVPair** %p.i, align 8, !dbg !996
  %246 = ptrtoint %struct._AVPair* %245 to i64, !dbg !996
  %247 = or i64 %246, 1, !dbg !996
  %248 = load i64* %cv.i, align 8, !dbg !996
  %249 = load i64** %LockFor.i, align 8, !dbg !996
  store i64 %247, i64* %6, align 8
  call void @llvm.dbg.declare(metadata !{i64* %6}, metadata !387) #6, !dbg !997
  store i64 %248, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !389) #6, !dbg !997
  store i64* %249, i64** %8, align 8
  call void @llvm.dbg.declare(metadata !{i64** %8}, metadata !390) #6, !dbg !997
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i}, metadata !391) #6, !dbg !998
  %250 = load i64* %6, align 8, !dbg !999
  %251 = load i64** %8, align 8, !dbg !999
  %252 = load i64* %7, align 8, !dbg !999
  %253 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %250, i64* %251, i64 %252) #6, !dbg !999, !srcloc !395
  store i64 %253, i64* %prevVal.i.i, align 8, !dbg !999
  %254 = load i64* %prevVal.i.i, align 8, !dbg !1000
  %255 = load i64* %cv.i, align 8, !dbg !996
  %256 = icmp eq i64 %254, %255, !dbg !996
  br i1 %256, label %257, label %269, !dbg !996

; <label>:257                                     ; preds = %244
  %258 = load i64* %cv.i, align 8, !dbg !1001
  %259 = load i64* %maxv.i, align 8, !dbg !1001
  %260 = icmp ugt i64 %258, %259, !dbg !1001
  br i1 %260, label %261, label %263, !dbg !1001

; <label>:261                                     ; preds = %257
  %262 = load i64* %cv.i, align 8, !dbg !1003
  store i64 %262, i64* %maxv.i, align 8, !dbg !1003
  br label %263, !dbg !1005

; <label>:263                                     ; preds = %261, %257
  %264 = load i64* %cv.i, align 8, !dbg !1006
  %265 = load %struct._AVPair** %p.i, align 8, !dbg !1006
  %266 = getelementptr inbounds %struct._AVPair* %265, i32 0, i32 5, !dbg !1006
  store i64 %264, i64* %266, align 8, !dbg !1006
  %267 = load %struct._AVPair** %p.i, align 8, !dbg !1007
  %268 = getelementptr inbounds %struct._AVPair* %267, i32 0, i32 6, !dbg !1007
  store i8 1, i8* %268, align 1, !dbg !1007
  br label %275, !dbg !1008

; <label>:269                                     ; preds = %244, %238
  %270 = load i64* %c.i, align 8, !dbg !1009
  %271 = add nsw i64 %270, -1, !dbg !1009
  store i64 %271, i64* %c.i, align 8, !dbg !1009
  %272 = icmp slt i64 %271, 0, !dbg !1009
  br i1 %272, label %273, label %274, !dbg !1009

; <label>:273                                     ; preds = %269
  store i64 0, i64* %25, !dbg !1010
  br label %TryFastUpdate.exit, !dbg !1010

; <label>:274                                     ; preds = %269
  br label %238, !dbg !1012

; <label>:275                                     ; preds = %263, %226, %168
  %276 = load %struct._AVPair** %p.i, align 8, !dbg !924
  %277 = getelementptr inbounds %struct._AVPair* %276, i32 0, i32 0, !dbg !924
  %278 = load %struct._AVPair** %277, align 8, !dbg !924
  store %struct._AVPair* %278, %struct._AVPair** %p.i, align 8, !dbg !924
  br label %102, !dbg !924

; <label>:279                                     ; preds = %102
  %280 = load %struct._Thread** %26, align 8, !dbg !1013
  %281 = load i64* %maxv.i, align 8, !dbg !1013
  store %struct._Thread* %280, %struct._Thread** %15, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %15}, metadata !1014) #6, !dbg !1015
  store i64 %281, i64* %16, align 8
  call void @llvm.dbg.declare(metadata !{i64* %16}, metadata !1016) #6, !dbg !1015
  call void @llvm.dbg.declare(metadata !{i64* %gv.i.i}, metadata !1017) #6, !dbg !1018
  %282 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !1018
  store i64 %282, i64* %gv.i.i, align 8, !dbg !1018
  call void @llvm.dbg.declare(metadata !{i64* %wv.i.i}, metadata !1019) #6, !dbg !1020
  %283 = load i64* %gv.i.i, align 8, !dbg !1020
  %284 = add i64 %283, 2, !dbg !1020
  store i64 %284, i64* %wv.i.i, align 8, !dbg !1020
  call void @llvm.dbg.declare(metadata !{i64* %k.i.i}, metadata !1021) #6, !dbg !1022
  %285 = load i64* %wv.i.i, align 8, !dbg !1023
  %286 = load i64* %gv.i.i, align 8, !dbg !1023
  store i64 %285, i64* %12, align 8
  call void @llvm.dbg.declare(metadata !{i64* %12}, metadata !387) #6, !dbg !1024
  store i64 %286, i64* %13, align 8
  call void @llvm.dbg.declare(metadata !{i64* %13}, metadata !389) #6, !dbg !1024
  store i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), i64** %14, align 8
  call void @llvm.dbg.declare(metadata !{i64** %14}, metadata !390) #6, !dbg !1024
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i.i}, metadata !391) #6, !dbg !1025
  %287 = load i64* %12, align 8, !dbg !1026
  %288 = load i64** %14, align 8, !dbg !1026
  %289 = load i64* %13, align 8, !dbg !1026
  %290 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %287, i64* %288, i64 %289) #6, !dbg !1026, !srcloc !395
  store i64 %290, i64* %prevVal.i.i.i, align 8, !dbg !1026
  %291 = load i64* %prevVal.i.i.i, align 8, !dbg !1027
  store i64 %291, i64* %k.i.i, align 8, !dbg !1023
  %292 = load i64* %k.i.i, align 8, !dbg !1028
  %293 = load i64* %gv.i.i, align 8, !dbg !1028
  %294 = icmp ne i64 %292, %293, !dbg !1028
  br i1 %294, label %295, label %GVGenerateWV_GV4.exit.i, !dbg !1028

; <label>:295                                     ; preds = %279
  %296 = load i64* %k.i.i, align 8, !dbg !1029
  store i64 %296, i64* %wv.i.i, align 8, !dbg !1029
  br label %GVGenerateWV_GV4.exit.i, !dbg !1031

GVGenerateWV_GV4.exit.i:                          ; preds = %295, %279
  %297 = load i64* %wv.i.i, align 8, !dbg !1032
  %298 = load %struct._Thread** %15, align 8, !dbg !1032
  %299 = getelementptr inbounds %struct._Thread* %298, i32 0, i32 5, !dbg !1032
  store i64 %297, i64* %299, align 8, !dbg !1032
  %300 = load i64* %wv.i.i, align 8, !dbg !1033
  store i64 %300, i64* %wv.i, align 8, !dbg !1013
  %301 = load %struct._Thread** %26, align 8, !dbg !1034
  store %struct._Thread* %301, %struct._Thread** %17, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %17}, metadata !1035) #6, !dbg !1036
  call void @llvm.dbg.declare(metadata !{i64* %dx.i.i}, metadata !1037) #6, !dbg !1038
  store i64 0, i64* %dx.i.i, align 8, !dbg !1038
  call void @llvm.dbg.declare(metadata !{i64* %rv.i.i}, metadata !1039) #6, !dbg !1040
  %302 = load %struct._Thread** %17, align 8, !dbg !1040
  %303 = getelementptr inbounds %struct._Thread* %302, i32 0, i32 4, !dbg !1040
  %304 = load volatile i64* %303, align 8, !dbg !1040
  store i64 %304, i64* %rv.i.i, align 8, !dbg !1040
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i.i}, metadata !1041) #6, !dbg !1042
  %305 = load %struct._Thread** %17, align 8, !dbg !1042
  %306 = getelementptr inbounds %struct._Thread* %305, i32 0, i32 16, !dbg !1042
  store %struct._Log* %306, %struct._Log** %rd.i.i, align 8, !dbg !1042
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList.i.i}, metadata !1043) #6, !dbg !1044
  %307 = load %struct._Log** %rd.i.i, align 8, !dbg !1044
  %308 = getelementptr inbounds %struct._Log* %307, i32 0, i32 1, !dbg !1044
  %309 = load %struct._AVPair** %308, align 8, !dbg !1044
  store %struct._AVPair* %309, %struct._AVPair** %EndOfList.i.i, align 8, !dbg !1044
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i4.i}, metadata !1045) #6, !dbg !1046
  %310 = load %struct._Log** %rd.i.i, align 8, !dbg !1047
  %311 = getelementptr inbounds %struct._Log* %310, i32 0, i32 0, !dbg !1047
  %312 = load %struct._AVPair** %311, align 8, !dbg !1047
  store %struct._AVPair* %312, %struct._AVPair** %e.i4.i, align 8, !dbg !1047
  br label %313, !dbg !1047

; <label>:313                                     ; preds = %345, %GVGenerateWV_GV4.exit.i
  %314 = load %struct._AVPair** %e.i4.i, align 8, !dbg !1047
  %315 = load %struct._AVPair** %EndOfList.i.i, align 8, !dbg !1047
  %316 = icmp ne %struct._AVPair* %314, %315, !dbg !1047
  br i1 %316, label %317, label %ReadSetCoherent.exit.i, !dbg !1047

; <label>:317                                     ; preds = %313
  call void @llvm.dbg.declare(metadata !{i64* %v.i.i}, metadata !1049) #6, !dbg !1051
  %318 = load %struct._AVPair** %e.i4.i, align 8, !dbg !1051
  %319 = getelementptr inbounds %struct._AVPair* %318, i32 0, i32 4, !dbg !1051
  %320 = load i64** %319, align 8, !dbg !1051
  %321 = load volatile i64* %320, align 8, !dbg !1051
  store i64 %321, i64* %v.i.i, align 8, !dbg !1051
  %322 = load i64* %v.i.i, align 8, !dbg !1052
  %323 = and i64 %322, 1, !dbg !1052
  %324 = icmp ne i64 %323, 0, !dbg !1052
  br i1 %324, label %325, label %337, !dbg !1052

; <label>:325                                     ; preds = %317
  %326 = load i64* %v.i.i, align 8, !dbg !1053
  %327 = and i64 %326, -2, !dbg !1053
  %328 = inttoptr i64 %327 to %struct._AVPair*, !dbg !1053
  %329 = getelementptr inbounds %struct._AVPair* %328, i32 0, i32 7, !dbg !1053
  %330 = load %struct._Thread** %329, align 8, !dbg !1053
  %331 = ptrtoint %struct._Thread* %330 to i64, !dbg !1053
  %332 = load %struct._Thread** %17, align 8, !dbg !1053
  %333 = ptrtoint %struct._Thread* %332 to i64, !dbg !1053
  %334 = xor i64 %331, %333, !dbg !1053
  %335 = load i64* %dx.i.i, align 8, !dbg !1053
  %336 = or i64 %335, %334, !dbg !1053
  store i64 %336, i64* %dx.i.i, align 8, !dbg !1053
  br label %345, !dbg !1055

; <label>:337                                     ; preds = %317
  %338 = load i64* %v.i.i, align 8, !dbg !1056
  %339 = load i64* %rv.i.i, align 8, !dbg !1056
  %340 = icmp ugt i64 %338, %339, !dbg !1056
  %341 = zext i1 %340 to i32, !dbg !1056
  %342 = sext i32 %341 to i64, !dbg !1056
  %343 = load i64* %dx.i.i, align 8, !dbg !1056
  %344 = or i64 %343, %342, !dbg !1056
  store i64 %344, i64* %dx.i.i, align 8, !dbg !1056
  br label %345

; <label>:345                                     ; preds = %337, %325
  %346 = load %struct._AVPair** %e.i4.i, align 8, !dbg !1047
  %347 = getelementptr inbounds %struct._AVPair* %346, i32 0, i32 0, !dbg !1047
  %348 = load %struct._AVPair** %347, align 8, !dbg !1047
  store %struct._AVPair* %348, %struct._AVPair** %e.i4.i, align 8, !dbg !1047
  br label %313, !dbg !1047

ReadSetCoherent.exit.i:                           ; preds = %313
  %349 = load i64* %dx.i.i, align 8, !dbg !1058
  %350 = icmp eq i64 %349, 0, !dbg !1058
  %351 = zext i1 %350 to i32, !dbg !1058
  %352 = sext i32 %351 to i64, !dbg !1058
  br i1 %350, label %354, label %353, !dbg !1034

; <label>:353                                     ; preds = %ReadSetCoherent.exit.i
  store i64 0, i64* %25, !dbg !1059
  br label %TryFastUpdate.exit, !dbg !1059

; <label>:354                                     ; preds = %ReadSetCoherent.exit.i
  %355 = load %struct._Log** %wr.i, align 8, !dbg !1061
  store %struct._Log* %355, %struct._Log** %18, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %18}, metadata !1063) #6, !dbg !1064
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i2.i}, metadata !1065) #6, !dbg !1066
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i3.i}, metadata !1067) #6, !dbg !1068
  %356 = load %struct._Log** %18, align 8, !dbg !1068
  %357 = getelementptr inbounds %struct._Log* %356, i32 0, i32 1, !dbg !1068
  %358 = load %struct._AVPair** %357, align 8, !dbg !1068
  store %struct._AVPair* %358, %struct._AVPair** %End.i3.i, align 8, !dbg !1068
  %359 = load %struct._Log** %18, align 8, !dbg !1069
  %360 = getelementptr inbounds %struct._Log* %359, i32 0, i32 0, !dbg !1069
  %361 = load %struct._AVPair** %360, align 8, !dbg !1069
  store %struct._AVPair* %361, %struct._AVPair** %e.i2.i, align 8, !dbg !1069
  br label %362, !dbg !1069

; <label>:362                                     ; preds = %366, %354
  %363 = load %struct._AVPair** %e.i2.i, align 8, !dbg !1069
  %364 = load %struct._AVPair** %End.i3.i, align 8, !dbg !1069
  %365 = icmp ne %struct._AVPair* %363, %364, !dbg !1069
  br i1 %365, label %366, label %WriteBackForward.exit.i, !dbg !1069

; <label>:366                                     ; preds = %362
  %367 = load %struct._AVPair** %e.i2.i, align 8, !dbg !1071
  %368 = getelementptr inbounds %struct._AVPair* %367, i32 0, i32 3, !dbg !1071
  %369 = load i64* %368, align 8, !dbg !1071
  %370 = load %struct._AVPair** %e.i2.i, align 8, !dbg !1071
  %371 = getelementptr inbounds %struct._AVPair* %370, i32 0, i32 2, !dbg !1071
  %372 = load i64** %371, align 8, !dbg !1071
  store volatile i64 %369, i64* %372, align 8, !dbg !1071
  %373 = load %struct._AVPair** %e.i2.i, align 8, !dbg !1069
  %374 = getelementptr inbounds %struct._AVPair* %373, i32 0, i32 0, !dbg !1069
  %375 = load %struct._AVPair** %374, align 8, !dbg !1069
  store %struct._AVPair* %375, %struct._AVPair** %e.i2.i, align 8, !dbg !1069
  br label %362, !dbg !1069

WriteBackForward.exit.i:                          ; preds = %362
  %376 = load %struct._Thread** %26, align 8, !dbg !1073
  %377 = load i64* %wv.i, align 8, !dbg !1073
  store %struct._Thread* %376, %struct._Thread** %19, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %19}, metadata !1074) #6, !dbg !1075
  store i64 %377, i64* %20, align 8
  call void @llvm.dbg.declare(metadata !{i64* %20}, metadata !1076) #6, !dbg !1075
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr.i.i}, metadata !1077) #6, !dbg !1078
  %378 = load %struct._Thread** %19, align 8, !dbg !1078
  %379 = getelementptr inbounds %struct._Thread* %378, i32 0, i32 17, !dbg !1078
  store %struct._Log* %379, %struct._Log** %wr.i.i, align 8, !dbg !1078
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i.i}, metadata !1079) #6, !dbg !1081
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i1.i}, metadata !1082) #6, !dbg !1083
  %380 = load %struct._Log** %wr.i.i, align 8, !dbg !1083
  %381 = getelementptr inbounds %struct._Log* %380, i32 0, i32 1, !dbg !1083
  %382 = load %struct._AVPair** %381, align 8, !dbg !1083
  store %struct._AVPair* %382, %struct._AVPair** %End.i1.i, align 8, !dbg !1083
  %383 = load %struct._Log** %wr.i.i, align 8, !dbg !1084
  %384 = getelementptr inbounds %struct._Log* %383, i32 0, i32 0, !dbg !1084
  %385 = load %struct._AVPair** %384, align 8, !dbg !1084
  store %struct._AVPair* %385, %struct._AVPair** %p.i.i, align 8, !dbg !1084
  br label %386, !dbg !1084

; <label>:386                                     ; preds = %404, %WriteBackForward.exit.i
  %387 = load %struct._AVPair** %p.i.i, align 8, !dbg !1084
  %388 = load %struct._AVPair** %End.i1.i, align 8, !dbg !1084
  %389 = icmp ne %struct._AVPair* %387, %388, !dbg !1084
  br i1 %389, label %390, label %DropLocks.exit.i, !dbg !1084

; <label>:390                                     ; preds = %386
  %391 = load %struct._AVPair** %p.i.i, align 8, !dbg !1086
  %392 = getelementptr inbounds %struct._AVPair* %391, i32 0, i32 6, !dbg !1086
  %393 = load i8* %392, align 1, !dbg !1086
  %394 = zext i8 %393 to i32, !dbg !1086
  %395 = icmp eq i32 %394, 0, !dbg !1086
  br i1 %395, label %396, label %397, !dbg !1086

; <label>:396                                     ; preds = %390
  br label %404, !dbg !1088

; <label>:397                                     ; preds = %390
  %398 = load %struct._AVPair** %p.i.i, align 8, !dbg !1090
  %399 = getelementptr inbounds %struct._AVPair* %398, i32 0, i32 6, !dbg !1090
  store i8 0, i8* %399, align 1, !dbg !1090
  %400 = load i64* %20, align 8, !dbg !1091
  %401 = load %struct._AVPair** %p.i.i, align 8, !dbg !1091
  %402 = getelementptr inbounds %struct._AVPair* %401, i32 0, i32 4, !dbg !1091
  %403 = load i64** %402, align 8, !dbg !1091
  store volatile i64 %400, i64* %403, align 8, !dbg !1091
  br label %404, !dbg !1092

; <label>:404                                     ; preds = %397, %396
  %405 = load %struct._AVPair** %p.i.i, align 8, !dbg !1084
  %406 = getelementptr inbounds %struct._AVPair* %405, i32 0, i32 0, !dbg !1084
  %407 = load %struct._AVPair** %406, align 8, !dbg !1084
  store %struct._AVPair* %407, %struct._AVPair** %p.i.i, align 8, !dbg !1084
  br label %386, !dbg !1084

DropLocks.exit.i:                                 ; preds = %386
  %408 = load %struct._Thread** %19, align 8, !dbg !1093
  %409 = getelementptr inbounds %struct._Thread* %408, i32 0, i32 2, !dbg !1093
  store volatile i64 0, i64* %409, align 8, !dbg !1093
  call void asm sideeffect "mfence", "~{dirflag},~{fpsr},~{flags}"() #6, !dbg !1094, !srcloc !1095
  store i64 1, i64* %25, !dbg !1096
  br label %TryFastUpdate.exit, !dbg !1096

TryFastUpdate.exit:                               ; preds = %163, %232, %273, %353, %DropLocks.exit.i
  %410 = load i64* %25, !dbg !1096
  %411 = icmp ne i64 %410, 0, !dbg !904
  br i1 %411, label %412, label %460, !dbg !904

; <label>:412                                     ; preds = %TryFastUpdate.exit
  %413 = load %struct._Thread** %30, align 8, !dbg !1097
  store %struct._Thread* %413, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !887), !dbg !1099
  %414 = load %struct._Thread** %2, align 8, !dbg !1100
  store %struct._Thread* %414, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !861), !dbg !1101
  %415 = load %struct._Thread** %1, align 8, !dbg !1102
  %416 = getelementptr inbounds %struct._Thread* %415, i32 0, i32 1, !dbg !1102
  store volatile i64 0, i64* %416, align 8, !dbg !1102
  %417 = load %struct._Thread** %1, align 8, !dbg !1103
  %418 = getelementptr inbounds %struct._Thread* %417, i32 0, i32 17, !dbg !1103
  %419 = getelementptr inbounds %struct._Log* %418, i32 0, i32 0, !dbg !1103
  %420 = load %struct._AVPair** %419, align 8, !dbg !1103
  %421 = load %struct._Thread** %1, align 8, !dbg !1103
  %422 = getelementptr inbounds %struct._Thread* %421, i32 0, i32 17, !dbg !1103
  %423 = getelementptr inbounds %struct._Log* %422, i32 0, i32 1, !dbg !1103
  store %struct._AVPair* %420, %struct._AVPair** %423, align 8, !dbg !1103
  %424 = load %struct._Thread** %1, align 8, !dbg !1104
  %425 = getelementptr inbounds %struct._Thread* %424, i32 0, i32 17, !dbg !1104
  %426 = getelementptr inbounds %struct._Log* %425, i32 0, i32 2, !dbg !1104
  store %struct._AVPair* null, %struct._AVPair** %426, align 8, !dbg !1104
  %427 = load %struct._Thread** %1, align 8, !dbg !1105
  %428 = getelementptr inbounds %struct._Thread* %427, i32 0, i32 17, !dbg !1105
  %429 = getelementptr inbounds %struct._Log* %428, i32 0, i32 5, !dbg !1105
  store i32 0, i32* %429, align 4, !dbg !1105
  %430 = load %struct._Thread** %1, align 8, !dbg !1106
  %431 = getelementptr inbounds %struct._Thread* %430, i32 0, i32 16, !dbg !1106
  %432 = getelementptr inbounds %struct._Log* %431, i32 0, i32 0, !dbg !1106
  %433 = load %struct._AVPair** %432, align 8, !dbg !1106
  %434 = load %struct._Thread** %1, align 8, !dbg !1106
  %435 = getelementptr inbounds %struct._Thread* %434, i32 0, i32 16, !dbg !1106
  %436 = getelementptr inbounds %struct._Log* %435, i32 0, i32 1, !dbg !1106
  store %struct._AVPair* %433, %struct._AVPair** %436, align 8, !dbg !1106
  %437 = load %struct._Thread** %1, align 8, !dbg !1107
  %438 = getelementptr inbounds %struct._Thread* %437, i32 0, i32 16, !dbg !1107
  %439 = getelementptr inbounds %struct._Log* %438, i32 0, i32 2, !dbg !1107
  store %struct._AVPair* null, %struct._AVPair** %439, align 8, !dbg !1107
  %440 = load %struct._Thread** %1, align 8, !dbg !1108
  %441 = getelementptr inbounds %struct._Thread* %440, i32 0, i32 18, !dbg !1108
  %442 = getelementptr inbounds %struct._Log* %441, i32 0, i32 0, !dbg !1108
  %443 = load %struct._AVPair** %442, align 8, !dbg !1108
  %444 = load %struct._Thread** %1, align 8, !dbg !1108
  %445 = getelementptr inbounds %struct._Thread* %444, i32 0, i32 18, !dbg !1108
  %446 = getelementptr inbounds %struct._Log* %445, i32 0, i32 1, !dbg !1108
  store %struct._AVPair* %443, %struct._AVPair** %446, align 8, !dbg !1108
  %447 = load %struct._Thread** %1, align 8, !dbg !1109
  %448 = getelementptr inbounds %struct._Thread* %447, i32 0, i32 18, !dbg !1109
  %449 = getelementptr inbounds %struct._Log* %448, i32 0, i32 2, !dbg !1109
  store %struct._AVPair* null, %struct._AVPair** %449, align 8, !dbg !1109
  %450 = load %struct._Thread** %1, align 8, !dbg !1110
  %451 = getelementptr inbounds %struct._Thread* %450, i32 0, i32 2, !dbg !1110
  store volatile i64 0, i64* %451, align 8, !dbg !1110
  %452 = load %struct._Thread** %2, align 8, !dbg !1111
  %453 = getelementptr inbounds %struct._Thread* %452, i32 0, i32 3, !dbg !1111
  store volatile i64 0, i64* %453, align 8, !dbg !1111
  %454 = load %struct._Thread** %30, align 8, !dbg !1112
  %455 = getelementptr inbounds %struct._Thread* %454, i32 0, i32 14, !dbg !1112
  %456 = load %struct.tmalloc** %455, align 8, !dbg !1112
  call void @tmalloc_clear(%struct.tmalloc* %456), !dbg !1112
  %457 = load %struct._Thread** %30, align 8, !dbg !1113
  %458 = getelementptr inbounds %struct._Thread* %457, i32 0, i32 15, !dbg !1113
  %459 = load %struct.tmalloc** %458, align 8, !dbg !1113
  call void @tmalloc_releaseAllForward(%struct.tmalloc* %459, void (i8*, i64)* @txSterilize), !dbg !1113
  store i32 1, i32* %29, !dbg !1114
  br label %462, !dbg !1114

; <label>:460                                     ; preds = %TryFastUpdate.exit
  %461 = load %struct._Thread** %30, align 8, !dbg !1115
  call void @TxAbort(%struct._Thread* %461), !dbg !1115
  store i32 0, i32* %29, !dbg !1116
  br label %462, !dbg !1116

; <label>:462                                     ; preds = %460, %412, %40
  %463 = load i32* %29, !dbg !1116
  ret i32 %463, !dbg !1116
}

declare void @tmalloc_releaseAllForward(%struct.tmalloc*, void (i8*, i64)*) #4

; Function Attrs: nounwind uwtable
define internal void @txSterilize(i8* %Base, i64 %Length) #0 {
  %1 = alloca i64, align 8
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %prevVal.i = alloca i64, align 8
  %4 = alloca i8*, align 8
  %5 = alloca i64, align 8
  %Addr = alloca i64*, align 8
  %End = alloca i64*, align 8
  %Lock = alloca i64*, align 8
  %val = alloca i64, align 8
  store i8* %Base, i8** %4, align 8
  call void @llvm.dbg.declare(metadata !{i8** %4}, metadata !1117), !dbg !1118
  store i64 %Length, i64* %5, align 8
  call void @llvm.dbg.declare(metadata !{i64* %5}, metadata !1119), !dbg !1118
  call void @llvm.dbg.declare(metadata !{i64** %Addr}, metadata !1120), !dbg !1123
  %6 = load i8** %4, align 8, !dbg !1123
  %7 = bitcast i8* %6 to i64*, !dbg !1123
  store i64* %7, i64** %Addr, align 8, !dbg !1123
  call void @llvm.dbg.declare(metadata !{i64** %End}, metadata !1124), !dbg !1125
  %8 = load i64** %Addr, align 8, !dbg !1125
  %9 = load i64* %5, align 8, !dbg !1125
  %10 = getelementptr inbounds i64* %8, i64 %9, !dbg !1125
  store i64* %10, i64** %End, align 8, !dbg !1125
  br label %11, !dbg !1126

; <label>:11                                      ; preds = %15, %0
  %12 = load i64** %Addr, align 8, !dbg !1126
  %13 = load i64** %End, align 8, !dbg !1126
  %14 = icmp ult i64* %12, %13, !dbg !1126
  br i1 %14, label %15, label %35, !dbg !1126

; <label>:15                                      ; preds = %11
  call void @llvm.dbg.declare(metadata !{i64** %Lock}, metadata !1127), !dbg !1129
  %16 = load i64** %Addr, align 8, !dbg !1129
  %17 = ptrtoint i64* %16 to i64, !dbg !1129
  %18 = add i64 %17, 128, !dbg !1129
  %19 = lshr i64 %18, 3, !dbg !1129
  %20 = and i64 %19, 1048575, !dbg !1129
  %21 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %20, !dbg !1129
  store i64* %21, i64** %Lock, align 8, !dbg !1129
  call void @llvm.dbg.declare(metadata !{i64* %val}, metadata !1130), !dbg !1131
  %22 = load i64** %Lock, align 8, !dbg !1131
  %23 = load volatile i64* %22, align 8, !dbg !1131
  store i64 %23, i64* %val, align 8, !dbg !1131
  %24 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !1132
  %25 = and i64 %24, -2, !dbg !1132
  %26 = load i64* %val, align 8, !dbg !1132
  %27 = load i64** %Lock, align 8, !dbg !1132
  store i64 %25, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !387) #6, !dbg !1133
  store i64 %26, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !389) #6, !dbg !1133
  store i64* %27, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !390) #6, !dbg !1133
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i}, metadata !391) #6, !dbg !1134
  %28 = load i64* %1, align 8, !dbg !1135
  %29 = load i64** %3, align 8, !dbg !1135
  %30 = load i64* %2, align 8, !dbg !1135
  %31 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %28, i64* %29, i64 %30) #6, !dbg !1135, !srcloc !395
  store i64 %31, i64* %prevVal.i, align 8, !dbg !1135
  %32 = load i64* %prevVal.i, align 8, !dbg !1136
  %33 = load i64** %Addr, align 8, !dbg !1137
  %34 = getelementptr inbounds i64* %33, i32 1, !dbg !1137
  store i64* %34, i64** %Addr, align 8, !dbg !1137
  br label %11, !dbg !1138

; <label>:35                                      ; preds = %11
  %36 = load i8** %4, align 8, !dbg !1139
  %37 = load i64* %5, align 8, !dbg !1139
  call void @llvm.memset.p0i8.i64(i8* %36, i8 -1, i64 %37, i32 1, i1 false), !dbg !1139
  ret void, !dbg !1140
}

; Function Attrs: nounwind uwtable
define i8* @TxAlloc(%struct._Thread* %Self, i64 %size) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64, align 8
  %ptr = alloca i8*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !1141), !dbg !1142
  store i64 %size, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !1143), !dbg !1142
  call void @llvm.dbg.declare(metadata !{i8** %ptr}, metadata !1144), !dbg !1145
  %3 = load i64* %2, align 8, !dbg !1145
  %4 = call i8* @tmalloc_reserve(i64 %3), !dbg !1145
  store i8* %4, i8** %ptr, align 8, !dbg !1145
  %5 = load i8** %ptr, align 8, !dbg !1146
  %6 = icmp ne i8* %5, null, !dbg !1146
  br i1 %6, label %7, label %13, !dbg !1146

; <label>:7                                       ; preds = %0
  %8 = load %struct._Thread** %1, align 8, !dbg !1147
  %9 = getelementptr inbounds %struct._Thread* %8, i32 0, i32 14, !dbg !1147
  %10 = load %struct.tmalloc** %9, align 8, !dbg !1147
  %11 = load i8** %ptr, align 8, !dbg !1147
  %12 = call i64 @tmalloc_append(%struct.tmalloc* %10, i8* %11), !dbg !1147
  br label %13, !dbg !1149

; <label>:13                                      ; preds = %7, %0
  %14 = load i8** %ptr, align 8, !dbg !1150
  ret i8* %14, !dbg !1150
}

declare i8* @tmalloc_reserve(i64) #4

declare i64 @tmalloc_append(%struct.tmalloc*, i8*) #4

; Function Attrs: nounwind uwtable
define void @TxFree(%struct._Thread* %Self, i8* %ptr) #0 {
  %1 = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Log*, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %6 = alloca %struct._Thread*, align 8
  %7 = alloca i8*, align 8
  %LockFor = alloca i64*, align 8
  %wr = alloca %struct._Log*, align 8
  store %struct._Thread* %Self, %struct._Thread** %6, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %6}, metadata !1151), !dbg !1152
  store i8* %ptr, i8** %7, align 8
  call void @llvm.dbg.declare(metadata !{i8** %7}, metadata !1153), !dbg !1152
  %8 = load %struct._Thread** %6, align 8, !dbg !1154
  %9 = getelementptr inbounds %struct._Thread* %8, i32 0, i32 15, !dbg !1154
  %10 = load %struct.tmalloc** %9, align 8, !dbg !1154
  %11 = load i8** %7, align 8, !dbg !1154
  %12 = call i64 @tmalloc_append(%struct.tmalloc* %10, i8* %11), !dbg !1154
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !1155), !dbg !1156
  %13 = load i8** %7, align 8, !dbg !1156
  %14 = ptrtoint i8* %13 to i64, !dbg !1156
  %15 = add i64 %14, 128, !dbg !1156
  %16 = lshr i64 %15, 3, !dbg !1156
  %17 = and i64 %16, 1048575, !dbg !1156
  %18 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %17, !dbg !1156
  store i64* %18, i64** %LockFor, align 8, !dbg !1156
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !1157), !dbg !1158
  %19 = load %struct._Thread** %6, align 8, !dbg !1158
  %20 = getelementptr inbounds %struct._Thread* %19, i32 0, i32 17, !dbg !1158
  store %struct._Log* %20, %struct._Log** %wr, align 8, !dbg !1158
  %21 = load %struct._Log** %wr, align 8, !dbg !1159
  %22 = load i8** %7, align 8, !dbg !1159
  %23 = bitcast i8* %22 to i64*, !dbg !1159
  %24 = load i64** %LockFor, align 8, !dbg !1159
  store %struct._Log* %21, %struct._Log** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %2}, metadata !714) #6, !dbg !1160
  store i64* %23, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !716) #6, !dbg !1160
  store i64 0, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !717) #6, !dbg !1160
  store i64* %24, i64** %5, align 8
  call void @llvm.dbg.declare(metadata !{i64** %5}, metadata !718) #6, !dbg !1160
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !719) #6, !dbg !1161
  %25 = load %struct._Log** %2, align 8, !dbg !1161
  %26 = getelementptr inbounds %struct._Log* %25, i32 0, i32 1, !dbg !1161
  %27 = load %struct._AVPair** %26, align 8, !dbg !1161
  store %struct._AVPair* %27, %struct._AVPair** %e.i, align 8, !dbg !1161
  %28 = load %struct._AVPair** %e.i, align 8, !dbg !1162
  %29 = icmp eq %struct._AVPair* %28, null, !dbg !1162
  br i1 %29, label %30, label %RecordStore.exit, !dbg !1162

; <label>:30                                      ; preds = %0
  %31 = load %struct._Log** %2, align 8, !dbg !1163
  %32 = getelementptr inbounds %struct._Log* %31, i32 0, i32 4, !dbg !1163
  %33 = load i64* %32, align 8, !dbg !1163
  %34 = add nsw i64 %33, 1, !dbg !1163
  store i64 %34, i64* %32, align 8, !dbg !1163
  %35 = load %struct._Log** %2, align 8, !dbg !1164
  %36 = getelementptr inbounds %struct._Log* %35, i32 0, i32 2, !dbg !1164
  %37 = load %struct._AVPair** %36, align 8, !dbg !1164
  store %struct._AVPair* %37, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !689) #6, !dbg !1165
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !691) #6, !dbg !1166
  %38 = call noalias i8* @malloc(i64 72) #6, !dbg !1166
  %39 = bitcast i8* %38 to %struct._AVPair*, !dbg !1166
  store %struct._AVPair* %39, %struct._AVPair** %e.i.i, align 8, !dbg !1166
  %40 = load %struct._AVPair** %e.i.i, align 8, !dbg !1167
  %41 = icmp ne %struct._AVPair* %40, null, !dbg !1167
  br i1 %41, label %ExtendList.exit.i, label %42, !dbg !1167

; <label>:42                                      ; preds = %30
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !1167
  unreachable, !dbg !1167

ExtendList.exit.i:                                ; preds = %30
  %43 = load %struct._AVPair** %e.i.i, align 8, !dbg !1168
  %44 = bitcast %struct._AVPair* %43 to i8*, !dbg !1168
  call void @llvm.memset.p0i8.i64(i8* %44, i8 0, i64 72, i32 8, i1 false) #6, !dbg !1168
  %45 = load %struct._AVPair** %e.i.i, align 8, !dbg !1169
  %46 = load %struct._AVPair** %1, align 8, !dbg !1169
  %47 = getelementptr inbounds %struct._AVPair* %46, i32 0, i32 0, !dbg !1169
  store %struct._AVPair* %45, %struct._AVPair** %47, align 8, !dbg !1169
  %48 = load %struct._AVPair** %1, align 8, !dbg !1170
  %49 = load %struct._AVPair** %e.i.i, align 8, !dbg !1170
  %50 = getelementptr inbounds %struct._AVPair* %49, i32 0, i32 1, !dbg !1170
  store %struct._AVPair* %48, %struct._AVPair** %50, align 8, !dbg !1170
  %51 = load %struct._AVPair** %e.i.i, align 8, !dbg !1171
  %52 = getelementptr inbounds %struct._AVPair* %51, i32 0, i32 0, !dbg !1171
  store %struct._AVPair* null, %struct._AVPair** %52, align 8, !dbg !1171
  %53 = load %struct._AVPair** %1, align 8, !dbg !1172
  %54 = getelementptr inbounds %struct._AVPair* %53, i32 0, i32 7, !dbg !1172
  %55 = load %struct._Thread** %54, align 8, !dbg !1172
  %56 = load %struct._AVPair** %e.i.i, align 8, !dbg !1172
  %57 = getelementptr inbounds %struct._AVPair* %56, i32 0, i32 7, !dbg !1172
  store %struct._Thread* %55, %struct._Thread** %57, align 8, !dbg !1172
  %58 = load %struct._AVPair** %1, align 8, !dbg !1173
  %59 = getelementptr inbounds %struct._AVPair* %58, i32 0, i32 8, !dbg !1173
  %60 = load i64* %59, align 8, !dbg !1173
  %61 = add nsw i64 %60, 1, !dbg !1173
  %62 = load %struct._AVPair** %e.i.i, align 8, !dbg !1173
  %63 = getelementptr inbounds %struct._AVPair* %62, i32 0, i32 8, !dbg !1173
  store i64 %61, i64* %63, align 8, !dbg !1173
  %64 = load %struct._AVPair** %e.i.i, align 8, !dbg !1174
  store %struct._AVPair* %64, %struct._AVPair** %e.i, align 8, !dbg !1164
  %65 = load %struct._AVPair** %e.i, align 8, !dbg !1175
  %66 = load %struct._Log** %2, align 8, !dbg !1175
  %67 = getelementptr inbounds %struct._Log* %66, i32 0, i32 3, !dbg !1175
  store %struct._AVPair* %65, %struct._AVPair** %67, align 8, !dbg !1175
  br label %RecordStore.exit, !dbg !1176

RecordStore.exit:                                 ; preds = %0, %ExtendList.exit.i
  %68 = load %struct._AVPair** %e.i, align 8, !dbg !1177
  %69 = load %struct._Log** %2, align 8, !dbg !1177
  %70 = getelementptr inbounds %struct._Log* %69, i32 0, i32 2, !dbg !1177
  store %struct._AVPair* %68, %struct._AVPair** %70, align 8, !dbg !1177
  %71 = load %struct._AVPair** %e.i, align 8, !dbg !1178
  %72 = getelementptr inbounds %struct._AVPair* %71, i32 0, i32 0, !dbg !1178
  %73 = load %struct._AVPair** %72, align 8, !dbg !1178
  %74 = load %struct._Log** %2, align 8, !dbg !1178
  %75 = getelementptr inbounds %struct._Log* %74, i32 0, i32 1, !dbg !1178
  store %struct._AVPair* %73, %struct._AVPair** %75, align 8, !dbg !1178
  %76 = load i64** %3, align 8, !dbg !1179
  %77 = load %struct._AVPair** %e.i, align 8, !dbg !1179
  %78 = getelementptr inbounds %struct._AVPair* %77, i32 0, i32 2, !dbg !1179
  store i64* %76, i64** %78, align 8, !dbg !1179
  %79 = load i64* %4, align 8, !dbg !1180
  %80 = load %struct._AVPair** %e.i, align 8, !dbg !1180
  %81 = getelementptr inbounds %struct._AVPair* %80, i32 0, i32 3, !dbg !1180
  store i64 %79, i64* %81, align 8, !dbg !1180
  %82 = load i64** %5, align 8, !dbg !1181
  %83 = load %struct._AVPair** %e.i, align 8, !dbg !1181
  %84 = getelementptr inbounds %struct._AVPair* %83, i32 0, i32 4, !dbg !1181
  store i64* %82, i64** %84, align 8, !dbg !1181
  %85 = load %struct._AVPair** %e.i, align 8, !dbg !1182
  %86 = getelementptr inbounds %struct._AVPair* %85, i32 0, i32 6, !dbg !1182
  store i8 0, i8* %86, align 1, !dbg !1182
  %87 = load %struct._AVPair** %e.i, align 8, !dbg !1183
  %88 = getelementptr inbounds %struct._AVPair* %87, i32 0, i32 5, !dbg !1183
  store i64 1, i64* %88, align 8, !dbg !1183
  ret void, !dbg !1184
}

; Function Attrs: nounwind uwtable
define internal void @restoreUseAfterFreeHandler() #0 {
  %1 = call i32 @sigaction(i32 7, %struct.sigaction* @global_act_oldsigbus, %struct.sigaction* null) #6, !dbg !1185
  %2 = icmp ne i32 %1, 0, !dbg !1185
  br i1 %2, label %3, label %4, !dbg !1185

; <label>:3                                       ; preds = %0
  call void @perror(i8* getelementptr inbounds ([40 x i8]* @.str9, i32 0, i32 0)), !dbg !1186
  call void @exit(i32 1) #7, !dbg !1188
  unreachable, !dbg !1188

; <label>:4                                       ; preds = %0
  %5 = call i32 @sigaction(i32 11, %struct.sigaction* @global_act_oldsigsegv, %struct.sigaction* null) #6, !dbg !1189
  %6 = icmp ne i32 %5, 0, !dbg !1189
  br i1 %6, label %7, label %8, !dbg !1189

; <label>:7                                       ; preds = %4
  call void @perror(i8* getelementptr inbounds ([41 x i8]* @.str10, i32 0, i32 0)), !dbg !1190
  call void @exit(i32 1) #7, !dbg !1192
  unreachable, !dbg !1192

; <label>:8                                       ; preds = %4
  ret void, !dbg !1193
}

; Function Attrs: nounwind
declare i32 @sigaction(i32, %struct.sigaction*, %struct.sigaction*) #3

declare void @perror(i8*) #4

; Function Attrs: noreturn nounwind
declare void @exit(i32) #5

; Function Attrs: nounwind uwtable
define internal void @registerUseAfterFreeHandler() #0 {
  %act = alloca %struct.sigaction, align 8
  call void @llvm.dbg.declare(metadata !{%struct.sigaction* %act}, metadata !1194), !dbg !1195
  %1 = bitcast %struct.sigaction* %act to i8*, !dbg !1196
  call void @llvm.memset.p0i8.i64(i8* %1, i8 -104, i64 0, i32 8, i1 false), !dbg !1196
  %2 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 0, !dbg !1197
  %3 = bitcast %union.anon* %2 to void (i32, %struct.siginfo_t*, i8*)**, !dbg !1197
  store void (i32, %struct.siginfo_t*, i8*)* @useAfterFreeHandler, void (i32, %struct.siginfo_t*, i8*)** %3, align 8, !dbg !1197
  %4 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 1, !dbg !1198
  %5 = call i32 @sigemptyset(%struct.__sigset_t* %4) #6, !dbg !1198
  %6 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 2, !dbg !1199
  store i32 268435460, i32* %6, align 4, !dbg !1199
  %7 = call i32 @sigaction(i32 7, %struct.sigaction* %act, %struct.sigaction* @global_act_oldsigbus) #6, !dbg !1200
  %8 = icmp ne i32 %7, 0, !dbg !1200
  br i1 %8, label %9, label %10, !dbg !1200

; <label>:9                                       ; preds = %0
  call void @perror(i8* getelementptr inbounds ([41 x i8]* @.str11, i32 0, i32 0)), !dbg !1201
  call void @exit(i32 1) #7, !dbg !1203
  unreachable, !dbg !1203

; <label>:10                                      ; preds = %0
  %11 = call i32 @sigaction(i32 11, %struct.sigaction* %act, %struct.sigaction* @global_act_oldsigsegv) #6, !dbg !1204
  %12 = icmp ne i32 %11, 0, !dbg !1204
  br i1 %12, label %13, label %14, !dbg !1204

; <label>:13                                      ; preds = %10
  call void @perror(i8* getelementptr inbounds ([42 x i8]* @.str12, i32 0, i32 0)), !dbg !1205
  call void @exit(i32 1) #7, !dbg !1207
  unreachable, !dbg !1207

; <label>:14                                      ; preds = %10
  ret void, !dbg !1208
}

; Function Attrs: nounwind uwtable
define internal void @useAfterFreeHandler(i32 %signum, %struct.siginfo_t* %siginfo, i8* %context) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %rv.i = alloca i64, align 8
  %rd.i = alloca %struct._Log*, align 8
  %EndOfList.i = alloca %struct._AVPair*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %v.i = alloca i64, align 8
  %3 = alloca i32, align 4
  %4 = alloca %struct.siginfo_t*, align 8
  %5 = alloca i8*, align 8
  %Self = alloca %struct._Thread*, align 8
  store i32 %signum, i32* %3, align 4
  call void @llvm.dbg.declare(metadata !{i32* %3}, metadata !1209), !dbg !1210
  store %struct.siginfo_t* %siginfo, %struct.siginfo_t** %4, align 8
  call void @llvm.dbg.declare(metadata !{%struct.siginfo_t** %4}, metadata !1211), !dbg !1210
  store i8* %context, i8** %5, align 8
  call void @llvm.dbg.declare(metadata !{i8** %5}, metadata !1212), !dbg !1210
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %Self}, metadata !1213), !dbg !1214
  %6 = load i32* @global_key_self, align 4, !dbg !1214
  %7 = call i8* @pthread_getspecific(i32 %6) #6, !dbg !1214
  %8 = bitcast i8* %7 to %struct._Thread*, !dbg !1214
  store %struct._Thread* %8, %struct._Thread** %Self, align 8, !dbg !1214
  %9 = load %struct._Thread** %Self, align 8, !dbg !1215
  %10 = icmp eq %struct._Thread* %9, null, !dbg !1215
  br i1 %10, label %16, label %11, !dbg !1215

; <label>:11                                      ; preds = %0
  %12 = load %struct._Thread** %Self, align 8, !dbg !1215
  %13 = getelementptr inbounds %struct._Thread* %12, i32 0, i32 1, !dbg !1215
  %14 = load volatile i64* %13, align 8, !dbg !1215
  %15 = icmp eq i64 %14, 0, !dbg !1215
  br i1 %15, label %16, label %21, !dbg !1215

; <label>:16                                      ; preds = %11, %0
  %17 = load i32* %3, align 4, !dbg !1216
  call void @psignal(i32 %17, i8* null), !dbg !1216
  %18 = load %struct.siginfo_t** %4, align 8, !dbg !1218
  %19 = getelementptr inbounds %struct.siginfo_t* %18, i32 0, i32 1, !dbg !1218
  %20 = load i32* %19, align 4, !dbg !1218
  call void @exit(i32 %20) #7, !dbg !1218
  unreachable, !dbg !1218

; <label>:21                                      ; preds = %11
  %22 = load %struct._Thread** %Self, align 8, !dbg !1219
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 1, !dbg !1219
  %24 = load volatile i64* %23, align 8, !dbg !1219
  %25 = icmp eq i64 %24, 1, !dbg !1219
  br i1 %25, label %26, label %79, !dbg !1219

; <label>:26                                      ; preds = %21
  %27 = load %struct._Thread** %Self, align 8, !dbg !1220
  store %struct._Thread* %27, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !658), !dbg !1222
  call void @llvm.dbg.declare(metadata !{i64* %rv.i}, metadata !660), !dbg !1223
  %28 = load %struct._Thread** %2, align 8, !dbg !1223
  %29 = getelementptr inbounds %struct._Thread* %28, i32 0, i32 4, !dbg !1223
  %30 = load volatile i64* %29, align 8, !dbg !1223
  store i64 %30, i64* %rv.i, align 8, !dbg !1223
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i}, metadata !662), !dbg !1224
  %31 = load %struct._Thread** %2, align 8, !dbg !1224
  %32 = getelementptr inbounds %struct._Thread* %31, i32 0, i32 16, !dbg !1224
  store %struct._Log* %32, %struct._Log** %rd.i, align 8, !dbg !1224
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList.i}, metadata !665), !dbg !1225
  %33 = load %struct._Log** %rd.i, align 8, !dbg !1225
  %34 = getelementptr inbounds %struct._Log* %33, i32 0, i32 1, !dbg !1225
  %35 = load %struct._AVPair** %34, align 8, !dbg !1225
  store %struct._AVPair* %35, %struct._AVPair** %EndOfList.i, align 8, !dbg !1225
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !667), !dbg !1226
  %36 = load %struct._Log** %rd.i, align 8, !dbg !1227
  %37 = getelementptr inbounds %struct._Log* %36, i32 0, i32 0, !dbg !1227
  %38 = load %struct._AVPair** %37, align 8, !dbg !1227
  store %struct._AVPair* %38, %struct._AVPair** %e.i, align 8, !dbg !1227
  br label %39, !dbg !1227

; <label>:39                                      ; preds = %69, %26
  %40 = load %struct._AVPair** %e.i, align 8, !dbg !1227
  %41 = load %struct._AVPair** %EndOfList.i, align 8, !dbg !1227
  %42 = icmp ne %struct._AVPair* %40, %41, !dbg !1227
  br i1 %42, label %43, label %73, !dbg !1227

; <label>:43                                      ; preds = %39
  call void @llvm.dbg.declare(metadata !{i64* %v.i}, metadata !671), !dbg !1228
  %44 = load %struct._AVPair** %e.i, align 8, !dbg !1228
  %45 = getelementptr inbounds %struct._AVPair* %44, i32 0, i32 4, !dbg !1228
  %46 = load i64** %45, align 8, !dbg !1228
  %47 = load volatile i64* %46, align 8, !dbg !1228
  store i64 %47, i64* %v.i, align 8, !dbg !1228
  %48 = load i64* %v.i, align 8, !dbg !1229
  %49 = and i64 %48, 1, !dbg !1229
  %50 = icmp ne i64 %49, 0, !dbg !1229
  br i1 %50, label %51, label %63, !dbg !1229

; <label>:51                                      ; preds = %43
  %52 = load i64* %v.i, align 8, !dbg !1230
  %53 = xor i64 %52, 1, !dbg !1230
  %54 = inttoptr i64 %53 to %struct._AVPair*, !dbg !1230
  %55 = getelementptr inbounds %struct._AVPair* %54, i32 0, i32 7, !dbg !1230
  %56 = load %struct._Thread** %55, align 8, !dbg !1230
  %57 = ptrtoint %struct._Thread* %56 to i64, !dbg !1230
  %58 = load %struct._Thread** %2, align 8, !dbg !1230
  %59 = ptrtoint %struct._Thread* %58 to i64, !dbg !1230
  %60 = icmp ne i64 %57, %59, !dbg !1230
  br i1 %60, label %61, label %62, !dbg !1230

; <label>:61                                      ; preds = %51
  store i64 0, i64* %1, !dbg !1231
  br label %ReadSetCoherentPessimistic.exit, !dbg !1231

; <label>:62                                      ; preds = %51
  br label %69, !dbg !1232

; <label>:63                                      ; preds = %43
  %64 = load i64* %v.i, align 8, !dbg !1233
  %65 = load i64* %rv.i, align 8, !dbg !1233
  %66 = icmp ugt i64 %64, %65, !dbg !1233
  br i1 %66, label %67, label %68, !dbg !1233

; <label>:67                                      ; preds = %63
  store i64 0, i64* %1, !dbg !1234
  br label %ReadSetCoherentPessimistic.exit, !dbg !1234

; <label>:68                                      ; preds = %63
  br label %69

; <label>:69                                      ; preds = %68, %62
  %70 = load %struct._AVPair** %e.i, align 8, !dbg !1227
  %71 = getelementptr inbounds %struct._AVPair* %70, i32 0, i32 0, !dbg !1227
  %72 = load %struct._AVPair** %71, align 8, !dbg !1227
  store %struct._AVPair* %72, %struct._AVPair** %e.i, align 8, !dbg !1227
  br label %39, !dbg !1227

; <label>:73                                      ; preds = %39
  store i64 1, i64* %1, !dbg !1235
  br label %ReadSetCoherentPessimistic.exit, !dbg !1235

ReadSetCoherentPessimistic.exit:                  ; preds = %61, %67, %73
  %74 = load i64* %1, !dbg !1235
  %75 = icmp ne i64 %74, 0, !dbg !1220
  br i1 %75, label %78, label %76, !dbg !1220

; <label>:76                                      ; preds = %ReadSetCoherentPessimistic.exit
  %77 = load %struct._Thread** %Self, align 8, !dbg !1236
  call void @TxAbort(%struct._Thread* %77), !dbg !1236
  br label %78, !dbg !1238

; <label>:78                                      ; preds = %76, %ReadSetCoherentPessimistic.exit
  br label %79, !dbg !1239

; <label>:79                                      ; preds = %78, %21
  %80 = load i32* %3, align 4, !dbg !1240
  call void @psignal(i32 %80, i8* null), !dbg !1240
  call void @abort() #7, !dbg !1241
  unreachable, !dbg !1241
                                                  ; No predecessors!
  ret void, !dbg !1242
}

; Function Attrs: nounwind
declare i32 @sigemptyset(%struct.__sigset_t*) #3

; Function Attrs: nounwind
declare i8* @pthread_getspecific(i32) #3

declare void @psignal(i32, i8*) #4

; Function Attrs: noreturn nounwind
declare void @abort() #5

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { noinline nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { noreturn nounwind }

!llvm.dbg.cu = !{!0}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.3-16ubuntu1 (branches/release_33) (based on LLVM 3.3)", i1 false, metadata !"", i32 0, metadata !2, metadata !19, metadata !20, metadata !302, metadata !19, metadata !""} ; [ DW_TAG_compile_unit ] [/vagrant/stamp-tl2-x86/tl2.c] [DW_LANG_C99]
!1 = metadata !{metadata !"tl2.c", metadata !"/vagrant/stamp-tl2-x86"}
!2 = metadata !{metadata !3, metadata !8, metadata !15}
!3 = metadata !{i32 786436, metadata !1, null, metadata !"tl2_config", i32 50, i64 32, i64 32, i32 0, i32 0, null, metadata !4, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [tl2_config] [line 50, size 32, align 32, offset 0] [from ]
!4 = metadata !{metadata !5, metadata !6, metadata !7}
!5 = metadata !{i32 786472, metadata !"TL2_INIT_WRSET_NUM_ENTRY", i64 1024} ; [ DW_TAG_enumerator ] [TL2_INIT_WRSET_NUM_ENTRY :: 1024]
!6 = metadata !{i32 786472, metadata !"TL2_INIT_RDSET_NUM_ENTRY", i64 8192} ; [ DW_TAG_enumerator ] [TL2_INIT_RDSET_NUM_ENTRY :: 8192]
!7 = metadata !{i32 786472, metadata !"TL2_INIT_LOCAL_NUM_ENTRY", i64 1024} ; [ DW_TAG_enumerator ] [TL2_INIT_LOCAL_NUM_ENTRY :: 1024]
!8 = metadata !{i32 786436, metadata !1, null, metadata !"", i32 73, i64 32, i64 32, i32 0, i32 0, null, metadata !9, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [line 73, size 32, align 32, offset 0] [from ]
!9 = metadata !{metadata !10, metadata !11, metadata !12, metadata !13, metadata !14}
!10 = metadata !{i32 786472, metadata !"TIDLE", i64 0} ; [ DW_TAG_enumerator ] [TIDLE :: 0]
!11 = metadata !{i32 786472, metadata !"TTXN", i64 1} ; [ DW_TAG_enumerator ] [TTXN :: 1]
!12 = metadata !{i32 786472, metadata !"TABORTING", i64 3} ; [ DW_TAG_enumerator ] [TABORTING :: 3]
!13 = metadata !{i32 786472, metadata !"TABORTED", i64 5} ; [ DW_TAG_enumerator ] [TABORTED :: 5]
!14 = metadata !{i32 786472, metadata !"TCOMMITTING", i64 7} ; [ DW_TAG_enumerator ] [TCOMMITTING :: 7]
!15 = metadata !{i32 786436, metadata !1, null, metadata !"", i32 81, i64 32, i64 32, i32 0, i32 0, null, metadata !16, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [line 81, size 32, align 32, offset 0] [from ]
!16 = metadata !{metadata !17, metadata !18}
!17 = metadata !{i32 786472, metadata !"LOCKBIT", i64 1} ; [ DW_TAG_enumerator ] [LOCKBIT :: 1]
!18 = metadata !{i32 786472, metadata !"NADA", i64 2} ; [ DW_TAG_enumerator ] [NADA :: 2]
!19 = metadata !{i32 0}
!20 = metadata !{metadata !21, metadata !34, metadata !125, metadata !128, metadata !129, metadata !134, metadata !137, metadata !140, metadata !141, metadata !144, metadata !147, metadata !148, metadata !151, metadata !154, metadata !158, metadata !161, metadata !164, metadata !167, metadata !170, metadata !171, metadata !174, metadata !179, metadata !182, metadata !187, metadata !190, metadata !191, metadata !194, metadata !195, metadata !198, metadata !201, metadata !204, metadata !207, metadata !208, metadata !209, metadata !212, metadata !216, metadata !219, metadata !220, metadata !221, metadata !222, metadata !225, metadata !228, metadata !229, metadata !230, metadata !301}
!21 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"pslock", metadata !"pslock", metadata !"", i32 574, metadata !23, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64* (i64*)* @pslock, null, null, metadata !19, i32 575} ; [ DW_TAG_subprogram ] [line 574] [def] [scope 575] [pslock]
!22 = metadata !{i32 786473, metadata !1}         ; [ DW_TAG_file_type ] [/vagrant/stamp-tl2-x86/tl2.c]
!23 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !24, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!24 = metadata !{metadata !25, metadata !30}
!25 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !26} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!26 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !27} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from vwLock]
!27 = metadata !{i32 786454, metadata !1, null, metadata !"vwLock", i32 109, i64 0, i64 0, i64 0, i32 0, metadata !28} ; [ DW_TAG_typedef ] [vwLock] [line 109, size 0, align 0, offset 0] [from uintptr_t]
!28 = metadata !{i32 786454, metadata !1, null, metadata !"uintptr_t", i32 122, i64 0, i64 0, i64 0, i32 0, metadata !29} ; [ DW_TAG_typedef ] [uintptr_t] [line 122, size 0, align 0, offset 0] [from long unsigned int]
!29 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!30 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !31} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!31 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !32} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from intptr_t]
!32 = metadata !{i32 786454, metadata !1, null, metadata !"intptr_t", i32 119, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_typedef ] [intptr_t] [line 119, size 0, align 0, offset 0] [from long int]
!33 = metadata !{i32 786468, null, null, metadata !"long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!34 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"FreeList", metadata !"FreeList", metadata !"", i32 616, metadata !35, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Log*, i64)* @FreeList, null, null, metadata !19, i32 617} ; [ DW_TAG_subprogram ] [line 616] [def] [scope 617] [FreeList]
!35 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !36, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!36 = metadata !{null, metadata !37, metadata !33}
!37 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !38} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from Log]
!38 = metadata !{i32 786454, metadata !1, null, metadata !"Log", i32 136, i64 0, i64 0, i64 0, i32 0, metadata !39} ; [ DW_TAG_typedef ] [Log] [line 136, size 0, align 0, offset 0] [from _Log]
!39 = metadata !{i32 786451, metadata !1, null, metadata !"_Log", i32 127, i64 384, i64 64, i32 0, i32 0, null, metadata !40, i32 0, null, null} ; [ DW_TAG_structure_type ] [_Log] [line 127, size 384, align 64, offset 0] [from ]
!40 = metadata !{metadata !41, metadata !119, metadata !120, metadata !121, metadata !122, metadata !123}
!41 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"List", i32 128, i64 64, i64 64, i64 0, i32 0, metadata !42} ; [ DW_TAG_member ] [List] [line 128, size 64, align 64, offset 0] [from ]
!42 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !43} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from AVPair]
!43 = metadata !{i32 786454, metadata !1, null, metadata !"AVPair", i32 125, i64 0, i64 0, i64 0, i32 0, metadata !44} ; [ DW_TAG_typedef ] [AVPair] [line 125, size 0, align 0, offset 0] [from _AVPair]
!44 = metadata !{i32 786451, metadata !1, null, metadata !"_AVPair", i32 113, i64 576, i64 64, i32 0, i32 0, null, metadata !45, i32 0, null, null} ; [ DW_TAG_structure_type ] [_AVPair] [line 113, size 576, align 64, offset 0] [from ]
!45 = metadata !{metadata !46, metadata !48, metadata !49, metadata !50, metadata !51, metadata !52, metadata !53, metadata !56, metadata !118}
!46 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Next", i32 114, i64 64, i64 64, i64 0, i32 0, metadata !47} ; [ DW_TAG_member ] [Next] [line 114, size 64, align 64, offset 0] [from ]
!47 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !44} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from _AVPair]
!48 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Prev", i32 115, i64 64, i64 64, i64 64, i32 0, metadata !47} ; [ DW_TAG_member ] [Prev] [line 115, size 64, align 64, offset 64] [from ]
!49 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Addr", i32 116, i64 64, i64 64, i64 128, i32 0, metadata !30} ; [ DW_TAG_member ] [Addr] [line 116, size 64, align 64, offset 128] [from ]
!50 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Valu", i32 117, i64 64, i64 64, i64 192, i32 0, metadata !32} ; [ DW_TAG_member ] [Valu] [line 117, size 64, align 64, offset 192] [from intptr_t]
!51 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"LockFor", i32 118, i64 64, i64 64, i64 256, i32 0, metadata !25} ; [ DW_TAG_member ] [LockFor] [line 118, size 64, align 64, offset 256] [from ]
!52 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"rdv", i32 119, i64 64, i64 64, i64 320, i32 0, metadata !27} ; [ DW_TAG_member ] [rdv] [line 119, size 64, align 64, offset 320] [from vwLock]
!53 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Held", i32 121, i64 8, i64 8, i64 384, i32 0, metadata !54} ; [ DW_TAG_member ] [Held] [line 121, size 8, align 8, offset 384] [from byte]
!54 = metadata !{i32 786454, metadata !1, null, metadata !"byte", i32 110, i64 0, i64 0, i64 0, i32 0, metadata !55} ; [ DW_TAG_typedef ] [byte] [line 110, size 0, align 0, offset 0] [from unsigned char]
!55 = metadata !{i32 786468, null, null, metadata !"unsigned char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ] [unsigned char] [line 0, size 8, align 8, offset 0, enc DW_ATE_unsigned_char]
!56 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Owner", i32 123, i64 64, i64 64, i64 448, i32 0, metadata !57} ; [ DW_TAG_member ] [Owner] [line 123, size 64, align 64, offset 448] [from ]
!57 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !58} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from _Thread]
!58 = metadata !{i32 786451, metadata !1, null, metadata !"_Thread", i32 147, i64 2240, i64 64, i32 0, i32 0, null, metadata !59, i32 0, null, null} ; [ DW_TAG_structure_type ] [_Thread] [line 147, size 2240, align 64, offset 0] [from ]
!59 = metadata !{metadata !60, metadata !61, metadata !63, metadata !64, metadata !65, metadata !66, metadata !67, metadata !68, metadata !71, metadata !72, metadata !73, metadata !74, metadata !76, metadata !80, metadata !82, metadata !92, metadata !93, metadata !94, metadata !95, metadata !96}
!60 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"UniqID", i32 148, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [UniqID] [line 148, size 64, align 64, offset 0] [from long int]
!61 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Mode", i32 149, i64 64, i64 64, i64 64, i32 0, metadata !62} ; [ DW_TAG_member ] [Mode] [line 149, size 64, align 64, offset 64] [from ]
!62 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from long int]
!63 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"HoldsLocks", i32 150, i64 64, i64 64, i64 128, i32 0, metadata !62} ; [ DW_TAG_member ] [HoldsLocks] [line 150, size 64, align 64, offset 128] [from ]
!64 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Retries", i32 151, i64 64, i64 64, i64 192, i32 0, metadata !62} ; [ DW_TAG_member ] [Retries] [line 151, size 64, align 64, offset 192] [from ]
!65 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"rv", i32 152, i64 64, i64 64, i64 256, i32 0, metadata !26} ; [ DW_TAG_member ] [rv] [line 152, size 64, align 64, offset 256] [from ]
!66 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"wv", i32 153, i64 64, i64 64, i64 320, i32 0, metadata !27} ; [ DW_TAG_member ] [wv] [line 153, size 64, align 64, offset 320] [from vwLock]
!67 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"abv", i32 154, i64 64, i64 64, i64 384, i32 0, metadata !27} ; [ DW_TAG_member ] [abv] [line 154, size 64, align 64, offset 384] [from vwLock]
!68 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"ROFlag", i32 159, i64 64, i64 64, i64 448, i32 0, metadata !69} ; [ DW_TAG_member ] [ROFlag] [line 159, size 64, align 64, offset 448] [from ]
!69 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !70} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from int]
!70 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!71 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"IsRO", i32 160, i64 32, i64 32, i64 512, i32 0, metadata !70} ; [ DW_TAG_member ] [IsRO] [line 160, size 32, align 32, offset 512] [from int]
!72 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Starts", i32 161, i64 64, i64 64, i64 576, i32 0, metadata !33} ; [ DW_TAG_member ] [Starts] [line 161, size 64, align 64, offset 576] [from long int]
!73 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Aborts", i32 162, i64 64, i64 64, i64 640, i32 0, metadata !33} ; [ DW_TAG_member ] [Aborts] [line 162, size 64, align 64, offset 640] [from long int]
!74 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"rng", i32 163, i64 64, i64 64, i64 704, i32 0, metadata !75} ; [ DW_TAG_member ] [rng] [line 163, size 64, align 64, offset 704] [from long long unsigned int]
!75 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!76 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"xorrng", i32 164, i64 64, i64 64, i64 768, i32 0, metadata !77} ; [ DW_TAG_member ] [xorrng] [line 164, size 64, align 64, offset 768] [from ]
!77 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 64, i64 64, i32 0, i32 0, metadata !75, metadata !78, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 64, align 64, offset 0] [from long long unsigned int]
!78 = metadata !{metadata !79}
!79 = metadata !{i32 786465, i64 0, i64 1}        ; [ DW_TAG_subrange_type ] [0, 0]
!80 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"memCache", i32 165, i64 64, i64 64, i64 832, i32 0, metadata !81} ; [ DW_TAG_member ] [memCache] [line 165, size 64, align 64, offset 832] [from ]
!81 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!82 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"allocPtr", i32 166, i64 64, i64 64, i64 896, i32 0, metadata !83} ; [ DW_TAG_member ] [allocPtr] [line 166, size 64, align 64, offset 896] [from ]
!83 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !84} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from tmalloc_t]
!84 = metadata !{i32 786454, metadata !1, null, metadata !"tmalloc_t", i32 31, i64 0, i64 0, i64 0, i32 0, metadata !85} ; [ DW_TAG_typedef ] [tmalloc_t] [line 31, size 0, align 0, offset 0] [from tmalloc]
!85 = metadata !{i32 786451, metadata !86, null, metadata !"tmalloc", i32 27, i64 192, i64 64, i32 0, i32 0, null, metadata !87, i32 0, null, null} ; [ DW_TAG_structure_type ] [tmalloc] [line 27, size 192, align 64, offset 0] [from ]
!86 = metadata !{metadata !"./tmalloc.h", metadata !"/vagrant/stamp-tl2-x86"}
!87 = metadata !{metadata !88, metadata !89, metadata !90}
!88 = metadata !{i32 786445, metadata !86, metadata !85, metadata !"size", i32 28, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [size] [line 28, size 64, align 64, offset 0] [from long int]
!89 = metadata !{i32 786445, metadata !86, metadata !85, metadata !"capacity", i32 29, i64 64, i64 64, i64 64, i32 0, metadata !33} ; [ DW_TAG_member ] [capacity] [line 29, size 64, align 64, offset 64] [from long int]
!90 = metadata !{i32 786445, metadata !86, metadata !85, metadata !"elements", i32 30, i64 64, i64 64, i64 128, i32 0, metadata !91} ; [ DW_TAG_member ] [elements] [line 30, size 64, align 64, offset 128] [from ]
!91 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!92 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"freePtr", i32 167, i64 64, i64 64, i64 960, i32 0, metadata !83} ; [ DW_TAG_member ] [freePtr] [line 167, size 64, align 64, offset 960] [from ]
!93 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"rdSet", i32 168, i64 384, i64 64, i64 1024, i32 0, metadata !38} ; [ DW_TAG_member ] [rdSet] [line 168, size 384, align 64, offset 1024] [from Log]
!94 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"wrSet", i32 172, i64 384, i64 64, i64 1408, i32 0, metadata !38} ; [ DW_TAG_member ] [wrSet] [line 172, size 384, align 64, offset 1408] [from Log]
!95 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"LocalUndo", i32 174, i64 384, i64 64, i64 1792, i32 0, metadata !38} ; [ DW_TAG_member ] [LocalUndo] [line 174, size 384, align 64, offset 1792] [from Log]
!96 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"envPtr", i32 175, i64 64, i64 64, i64 2176, i32 0, metadata !97} ; [ DW_TAG_member ] [envPtr] [line 175, size 64, align 64, offset 2176] [from ]
!97 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !98} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from sigjmp_buf]
!98 = metadata !{i32 786454, metadata !1, null, metadata !"sigjmp_buf", i32 92, i64 0, i64 0, i64 0, i32 0, metadata !99} ; [ DW_TAG_typedef ] [sigjmp_buf] [line 92, size 0, align 0, offset 0] [from ]
!99 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1600, i64 64, i32 0, i32 0, metadata !100, metadata !78, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 1600, align 64, offset 0] [from __jmp_buf_tag]
!100 = metadata !{i32 786451, metadata !101, null, metadata !"__jmp_buf_tag", i32 34, i64 1600, i64 64, i32 0, i32 0, null, metadata !102, i32 0, null, null} ; [ DW_TAG_structure_type ] [__jmp_buf_tag] [line 34, size 1600, align 64, offset 0] [from ]
!101 = metadata !{metadata !"/usr/include/setjmp.h", metadata !"/vagrant/stamp-tl2-x86"}
!102 = metadata !{metadata !103, metadata !108, metadata !109}
!103 = metadata !{i32 786445, metadata !101, metadata !100, metadata !"__jmpbuf", i32 40, i64 512, i64 64, i64 0, i32 0, metadata !104} ; [ DW_TAG_member ] [__jmpbuf] [line 40, size 512, align 64, offset 0] [from __jmp_buf]
!104 = metadata !{i32 786454, metadata !101, null, metadata !"__jmp_buf", i32 31, i64 0, i64 0, i64 0, i32 0, metadata !105} ; [ DW_TAG_typedef ] [__jmp_buf] [line 31, size 0, align 0, offset 0] [from ]
!105 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 512, i64 64, i32 0, i32 0, metadata !33, metadata !106, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 512, align 64, offset 0] [from long int]
!106 = metadata !{metadata !107}
!107 = metadata !{i32 786465, i64 0, i64 8}       ; [ DW_TAG_subrange_type ] [0, 7]
!108 = metadata !{i32 786445, metadata !101, metadata !100, metadata !"__mask_was_saved", i32 41, i64 32, i64 32, i64 512, i32 0, metadata !70} ; [ DW_TAG_member ] [__mask_was_saved] [line 41, size 32, align 32, offset 512] [from int]
!109 = metadata !{i32 786445, metadata !101, metadata !100, metadata !"__saved_mask", i32 42, i64 1024, i64 64, i64 576, i32 0, metadata !110} ; [ DW_TAG_member ] [__saved_mask] [line 42, size 1024, align 64, offset 576] [from __sigset_t]
!110 = metadata !{i32 786454, metadata !101, null, metadata !"__sigset_t", i32 30, i64 0, i64 0, i64 0, i32 0, metadata !111} ; [ DW_TAG_typedef ] [__sigset_t] [line 30, size 0, align 0, offset 0] [from ]
!111 = metadata !{i32 786451, metadata !112, null, metadata !"", i32 27, i64 1024, i64 64, i32 0, i32 0, null, metadata !113, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 27, size 1024, align 64, offset 0] [from ]
!112 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/sigset.h", metadata !"/vagrant/stamp-tl2-x86"}
!113 = metadata !{metadata !114}
!114 = metadata !{i32 786445, metadata !112, metadata !111, metadata !"__val", i32 29, i64 1024, i64 64, i64 0, i32 0, metadata !115} ; [ DW_TAG_member ] [__val] [line 29, size 1024, align 64, offset 0] [from ]
!115 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1024, i64 64, i32 0, i32 0, metadata !29, metadata !116, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 1024, align 64, offset 0] [from long unsigned int]
!116 = metadata !{metadata !117}
!117 = metadata !{i32 786465, i64 0, i64 16}      ; [ DW_TAG_subrange_type ] [0, 15]
!118 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Ordinal", i32 124, i64 64, i64 64, i64 512, i32 0, metadata !33} ; [ DW_TAG_member ] [Ordinal] [line 124, size 64, align 64, offset 512] [from long int]
!119 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"put", i32 129, i64 64, i64 64, i64 64, i32 0, metadata !42} ; [ DW_TAG_member ] [put] [line 129, size 64, align 64, offset 64] [from ]
!120 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"tail", i32 130, i64 64, i64 64, i64 128, i32 0, metadata !42} ; [ DW_TAG_member ] [tail] [line 130, size 64, align 64, offset 128] [from ]
!121 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"end", i32 131, i64 64, i64 64, i64 192, i32 0, metadata !42} ; [ DW_TAG_member ] [end] [line 131, size 64, align 64, offset 192] [from ]
!122 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"ovf", i32 132, i64 64, i64 64, i64 256, i32 0, metadata !33} ; [ DW_TAG_member ] [ovf] [line 132, size 64, align 64, offset 256] [from long int]
!123 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"BloomFilter", i32 134, i64 32, i64 32, i64 320, i32 0, metadata !124} ; [ DW_TAG_member ] [BloomFilter] [line 134, size 32, align 32, offset 320] [from BitMap]
!124 = metadata !{i32 786454, metadata !1, null, metadata !"BitMap", i32 108, i64 0, i64 0, i64 0, i32 0, metadata !70} ; [ DW_TAG_typedef ] [BitMap] [line 108, size 0, align 0, offset 0] [from int]
!125 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxOnce", metadata !"TxOnce", metadata !"", i32 1014, metadata !126, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @TxOnce, null, null, metadata !19, i32 1015} ; [ DW_TAG_subprogram ] [line 1014] [def] [scope 1015] [TxOnce]
!126 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !127, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!127 = metadata !{null}
!128 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxShutdown", metadata !"TxShutdown", metadata !"", i32 1032, metadata !126, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @TxShutdown, null, null, metadata !19, i32 1033} ; [ DW_TAG_subprogram ] [line 1032] [def] [scope 1033] [TxShutdown]
!129 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxNewThread", metadata !"TxNewThread", metadata !"", i32 1062, metadata !130, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, %struct._Thread* ()* @TxNewThread, null, null, metadata !19, i32 1063} ; [ DW_TAG_subprogram ] [line 1062] [def] [scope 1063] [TxNewThread]
!130 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !131, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!131 = metadata !{metadata !132}
!132 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !133} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from Thread]
!133 = metadata !{i32 786454, metadata !1, null, metadata !"Thread", i32 35, i64 0, i64 0, i64 0, i32 0, metadata !58} ; [ DW_TAG_typedef ] [Thread] [line 35, size 0, align 0, offset 0] [from _Thread]
!134 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxFreeThread", metadata !"TxFreeThread", metadata !"", i32 1082, metadata !135, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @TxFreeThread, null, null, metadata !19, i32 1083} ; [ DW_TAG_subprogram ] [line 1082] [def] [scope 1083] [TxFreeThread]
!135 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !136, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!136 = metadata !{null, metadata !132}
!137 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxInitThread", metadata !"TxInitThread", metadata !"", i32 1126, metadata !138, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64)* @TxInitThread, null, null, metadata !19, i32 1127} ; [ DW_TAG_subprogram ] [line 1126] [def] [scope 1127] [TxInitThread]
!138 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !139, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!139 = metadata !{null, metadata !132, metadata !33}
!140 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxAbort", metadata !"TxAbort", metadata !"", i32 1704, metadata !135, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @TxAbort, null, null, metadata !19, i32 1705} ; [ DW_TAG_subprogram ] [line 1704] [def] [scope 1705] [TxAbort]
!141 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStore", metadata !"TxStore", metadata !"", i32 1845, metadata !142, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64*, i64)* @TxStore, null, null, metadata !19, i32 1846} ; [ DW_TAG_subprogram ] [line 1845] [def] [scope 1846] [TxStore]
!142 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !143, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!143 = metadata !{null, metadata !132, metadata !30, metadata !32}
!144 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxLoad", metadata !"TxLoad", metadata !"", i32 2025, metadata !145, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*, i64*)* @TxLoad, null, null, metadata !19, i32 2026} ; [ DW_TAG_subprogram ] [line 2025] [def] [scope 2026] [TxLoad]
!145 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !146, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!146 = metadata !{metadata !32, metadata !132, metadata !30}
!147 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStoreLocal", metadata !"TxStoreLocal", metadata !"", i32 2147, metadata !142, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64*, i64)* @TxStoreLocal, null, null, metadata !19, i32 2148} ; [ DW_TAG_subprogram ] [line 2147] [def] [scope 2148] [TxStoreLocal]
!148 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStart", metadata !"TxStart", metadata !"", i32 2163, metadata !149, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, [1 x %struct.__jmp_buf_tag]*, i32*)* @TxStart, null, null, metadata !19, i32 2164} ; [ DW_TAG_subprogram ] [line 2163] [def] [scope 2164] [TxStart]
!149 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !150, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!150 = metadata !{null, metadata !132, metadata !97, metadata !69}
!151 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxCommit", metadata !"TxCommit", metadata !"", i32 2195, metadata !152, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (%struct._Thread*)* @TxCommit, null, null, metadata !19, i32 2196} ; [ DW_TAG_subprogram ] [line 2195] [def] [scope 2196] [TxCommit]
!152 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !153, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!153 = metadata !{metadata !70, metadata !132}
!154 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxAlloc", metadata !"TxAlloc", metadata !"", i32 2265, metadata !155, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (%struct._Thread*, i64)* @TxAlloc, null, null, metadata !19, i32 2266} ; [ DW_TAG_subprogram ] [line 2265] [def] [scope 2266] [TxAlloc]
!155 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !156, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!156 = metadata !{metadata !81, metadata !132, metadata !157}
!157 = metadata !{i32 786454, metadata !1, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !29} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!158 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxFree", metadata !"TxFree", metadata !"", i32 2283, metadata !159, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i8*)* @TxFree, null, null, metadata !19, i32 2284} ; [ DW_TAG_subprogram ] [line 2283] [def] [scope 2284] [TxFree]
!159 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !160, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!160 = metadata !{null, metadata !132, metadata !81}
!161 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TryFastUpdate", metadata !"TryFastUpdate", metadata !"", i32 1457, metadata !162, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1458} ; [ DW_TAG_subprogram ] [line 1457] [local] [def] [scope 1458] [TryFastUpdate]
!162 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !163, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!163 = metadata !{metadata !33, metadata !132}
!164 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"DropLocks", metadata !"DropLocks", metadata !"", i32 1381, metadata !165, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1382} ; [ DW_TAG_subprogram ] [line 1381] [local] [def] [scope 1382] [DropLocks]
!165 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !166, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!166 = metadata !{null, metadata !132, metadata !27}
!167 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"WriteBackForward", metadata !"WriteBackForward", metadata !"", i32 755, metadata !168, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 756} ; [ DW_TAG_subprogram ] [line 755] [local] [def] [scope 756] [WriteBackForward]
!168 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !169, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!169 = metadata !{null, metadata !37}
!170 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ReadSetCoherent", metadata !"ReadSetCoherent", metadata !"", i32 1254, metadata !162, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1255} ; [ DW_TAG_subprogram ] [line 1254] [local] [def] [scope 1255] [ReadSetCoherent]
!171 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVGenerateWV_GV4", metadata !"GVGenerateWV_GV4", metadata !"", i32 381, metadata !172, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 382} ; [ DW_TAG_subprogram ] [line 381] [local] [def] [scope 382] [GVGenerateWV_GV4]
!172 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !173, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!173 = metadata !{metadata !27, metadata !132, metadata !27}
!174 = metadata !{i32 786478, metadata !175, metadata !176, metadata !"cas", metadata !"cas", metadata !"", i32 42, metadata !177, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 43} ; [ DW_TAG_subprogram ] [line 42] [local] [def] [scope 43] [cas]
!175 = metadata !{metadata !"./platform_x86.h", metadata !"/vagrant/stamp-tl2-x86"}
!176 = metadata !{i32 786473, metadata !175}      ; [ DW_TAG_file_type ] [/vagrant/stamp-tl2-x86/./platform_x86.h]
!177 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !178, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!178 = metadata !{metadata !32, metadata !32, metadata !32, metadata !30}
!179 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"FindFirst", metadata !"FindFirst", metadata !"", i32 788, metadata !180, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 789} ; [ DW_TAG_subprogram ] [line 788] [local] [def] [scope 789] [FindFirst]
!180 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !181, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!181 = metadata !{metadata !42, metadata !37, metadata !25}
!182 = metadata !{i32 786478, metadata !175, metadata !176, metadata !"prefetchw", metadata !"prefetchw", metadata !"", i32 85, metadata !183, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 86} ; [ DW_TAG_subprogram ] [line 85] [local] [def] [scope 86] [prefetchw]
!183 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !184, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!184 = metadata !{null, metadata !185}
!185 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !186} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!186 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from ]
!187 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txSterilize", metadata !"txSterilize", metadata !"", i32 2120, metadata !188, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i8*, i64)* @txSterilize, null, null, metadata !19, i32 2121} ; [ DW_TAG_subprogram ] [line 2120] [local] [def] [scope 2121] [txSterilize]
!188 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !189, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!189 = metadata !{null, metadata !81, metadata !157}
!190 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txCommitReset", metadata !"txCommitReset", metadata !"", i32 1206, metadata !135, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1207} ; [ DW_TAG_subprogram ] [line 1206] [local] [def] [scope 1207] [txCommitReset]
!191 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVRead", metadata !"GVRead", metadata !"", i32 313, metadata !192, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 314} ; [ DW_TAG_subprogram ] [line 313] [local] [def] [scope 314] [GVRead]
!192 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !193, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!193 = metadata !{metadata !27, metadata !132}
!194 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txReset", metadata !"txReset", metadata !"", i32 1166, metadata !135, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1167} ; [ DW_TAG_subprogram ] [line 1166] [local] [def] [scope 1167] [txReset]
!195 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"SaveForRollBack", metadata !"SaveForRollBack", metadata !"", i32 866, metadata !196, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 867} ; [ DW_TAG_subprogram ] [line 866] [local] [def] [scope 867] [SaveForRollBack]
!196 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !197, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!197 = metadata !{null, metadata !37, metadata !30, metadata !32}
!198 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ExtendList", metadata !"ExtendList", metadata !"", i32 641, metadata !199, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 642} ; [ DW_TAG_subprogram ] [line 641] [local] [def] [scope 642] [ExtendList]
!199 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !200, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!200 = metadata !{metadata !42, metadata !42}
!201 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"RecordStore", metadata !"RecordStore", metadata !"", i32 832, metadata !202, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 833} ; [ DW_TAG_subprogram ] [line 832] [local] [def] [scope 833] [RecordStore]
!202 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !203, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!203 = metadata !{null, metadata !37, metadata !30, metadata !32, metadata !25}
!204 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TrackLoad", metadata !"TrackLoad", metadata !"", i32 887, metadata !205, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 888} ; [ DW_TAG_subprogram ] [line 887] [local] [def] [scope 888] [TrackLoad]
!205 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !206, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!206 = metadata !{metadata !70, metadata !132, metadata !25}
!207 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ReadSetCoherentPessimistic", metadata !"ReadSetCoherentPessimistic", metadata !"", i32 1296, metadata !162, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1297} ; [ DW_TAG_subprogram ] [line 1296] [local] [def] [scope 1297] [ReadSetCoherentPessimistic]
!208 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"backoff", metadata !"backoff", metadata !"", i32 1427, metadata !138, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1428} ; [ DW_TAG_subprogram ] [line 1427] [local] [def] [scope 1428] [backoff]
!209 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TSRandom", metadata !"TSRandom", metadata !"", i32 243, metadata !210, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 244} ; [ DW_TAG_subprogram ] [line 243] [local] [def] [scope 244] [TSRandom]
!210 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !211, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!211 = metadata !{metadata !75, metadata !132}
!212 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MarsagliaXOR", metadata !"MarsagliaXOR", metadata !"", i32 230, metadata !213, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 231} ; [ DW_TAG_subprogram ] [line 230] [local] [def] [scope 231] [MarsagliaXOR]
!213 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !214, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!214 = metadata !{metadata !75, metadata !215}
!215 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !75} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from long long unsigned int]
!216 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MarsagliaXORV", metadata !"MarsagliaXORV", metadata !"", i32 210, metadata !217, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 211} ; [ DW_TAG_subprogram ] [line 210] [local] [def] [scope 211] [MarsagliaXORV]
!217 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !218, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!218 = metadata !{metadata !75, metadata !75}
!219 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVAbort", metadata !"GVAbort", metadata !"", i32 475, metadata !162, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 476} ; [ DW_TAG_subprogram ] [line 475] [local] [def] [scope 476] [GVAbort]
!220 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"WriteBackReverse", metadata !"WriteBackReverse", metadata !"", i32 772, metadata !168, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 773} ; [ DW_TAG_subprogram ] [line 772] [local] [def] [scope 773] [WriteBackReverse]
!221 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"RestoreLocks", metadata !"RestoreLocks", metadata !"", i32 1339, metadata !135, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1340} ; [ DW_TAG_subprogram ] [line 1339] [local] [def] [scope 1340] [RestoreLocks]
!222 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MakeList", metadata !"MakeList", metadata !"", i32 588, metadata !223, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 589} ; [ DW_TAG_subprogram ] [line 588] [local] [def] [scope 589] [MakeList]
!223 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !224, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!224 = metadata !{metadata !42, metadata !33, metadata !132}
!225 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"AtomicAdd", metadata !"AtomicAdd", metadata !"", i32 254, metadata !226, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 255} ; [ DW_TAG_subprogram ] [line 254] [local] [def] [scope 255] [AtomicAdd]
!226 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !227, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!227 = metadata !{metadata !32, metadata !30, metadata !32}
!228 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"restoreUseAfterFreeHandler", metadata !"restoreUseAfterFreeHandler", metadata !"", i32 993, metadata !126, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @restoreUseAfterFreeHandler, null, null, metadata !19, i32 994} ; [ DW_TAG_subprogram ] [line 993] [local] [def] [scope 994] [restoreUseAfterFreeHandler]
!229 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"registerUseAfterFreeHandler", metadata !"registerUseAfterFreeHandler", metadata !"", i32 967, metadata !126, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @registerUseAfterFreeHandler, null, null, metadata !19, i32 968} ; [ DW_TAG_subprogram ] [line 967] [local] [def] [scope 968] [registerUseAfterFreeHandler]
!230 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"useAfterFreeHandler", metadata !"useAfterFreeHandler", metadata !"", i32 942, metadata !231, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, %struct.siginfo_t*, i8*)* @useAfterFreeHandler, null, null, metadata !19, i32 943} ; [ DW_TAG_subprogram ] [line 942] [local] [def] [scope 943] [useAfterFreeHandler]
!231 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !232, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!232 = metadata !{null, metadata !70, metadata !233, metadata !81}
!233 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !234} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from siginfo_t]
!234 = metadata !{i32 786454, metadata !235, null, metadata !"siginfo_t", i32 128, i64 0, i64 0, i64 0, i32 0, metadata !236} ; [ DW_TAG_typedef ] [siginfo_t] [line 128, size 0, align 0, offset 0] [from ]
!235 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/sigaction.h", metadata !"/vagrant/stamp-tl2-x86"}
!236 = metadata !{i32 786451, metadata !237, null, metadata !"", i32 62, i64 1024, i64 64, i32 0, i32 0, null, metadata !238, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 62, size 1024, align 64, offset 0] [from ]
!237 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/siginfo.h", metadata !"/vagrant/stamp-tl2-x86"}
!238 = metadata !{metadata !239, metadata !240, metadata !241, metadata !242}
!239 = metadata !{i32 786445, metadata !237, metadata !236, metadata !"si_signo", i32 64, i64 32, i64 32, i64 0, i32 0, metadata !70} ; [ DW_TAG_member ] [si_signo] [line 64, size 32, align 32, offset 0] [from int]
!240 = metadata !{i32 786445, metadata !237, metadata !236, metadata !"si_errno", i32 65, i64 32, i64 32, i64 32, i32 0, metadata !70} ; [ DW_TAG_member ] [si_errno] [line 65, size 32, align 32, offset 32] [from int]
!241 = metadata !{i32 786445, metadata !237, metadata !236, metadata !"si_code", i32 67, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [si_code] [line 67, size 32, align 32, offset 64] [from int]
!242 = metadata !{i32 786445, metadata !237, metadata !236, metadata !"_sifields", i32 127, i64 896, i64 64, i64 128, i32 0, metadata !243} ; [ DW_TAG_member ] [_sifields] [line 127, size 896, align 64, offset 128] [from ]
!243 = metadata !{i32 786455, metadata !237, metadata !236, metadata !"", i32 69, i64 896, i64 64, i64 0, i32 0, null, metadata !244, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [line 69, size 896, align 64, offset 0] [from ]
!244 = metadata !{metadata !245, metadata !249, metadata !257, metadata !268, metadata !274, metadata !284, metadata !290, metadata !295}
!245 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_pad", i32 71, i64 896, i64 32, i64 0, i32 0, metadata !246} ; [ DW_TAG_member ] [_pad] [line 71, size 896, align 32, offset 0] [from ]
!246 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 896, i64 32, i32 0, i32 0, metadata !70, metadata !247, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 896, align 32, offset 0] [from int]
!247 = metadata !{metadata !248}
!248 = metadata !{i32 786465, i64 0, i64 28}      ; [ DW_TAG_subrange_type ] [0, 27]
!249 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_kill", i32 78, i64 64, i64 32, i64 0, i32 0, metadata !250} ; [ DW_TAG_member ] [_kill] [line 78, size 64, align 32, offset 0] [from ]
!250 = metadata !{i32 786451, metadata !237, metadata !243, metadata !"", i32 74, i64 64, i64 32, i32 0, i32 0, null, metadata !251, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 74, size 64, align 32, offset 0] [from ]
!251 = metadata !{metadata !252, metadata !254}
!252 = metadata !{i32 786445, metadata !237, metadata !250, metadata !"si_pid", i32 76, i64 32, i64 32, i64 0, i32 0, metadata !253} ; [ DW_TAG_member ] [si_pid] [line 76, size 32, align 32, offset 0] [from __pid_t]
!253 = metadata !{i32 786454, metadata !237, null, metadata !"__pid_t", i32 133, i64 0, i64 0, i64 0, i32 0, metadata !70} ; [ DW_TAG_typedef ] [__pid_t] [line 133, size 0, align 0, offset 0] [from int]
!254 = metadata !{i32 786445, metadata !237, metadata !250, metadata !"si_uid", i32 77, i64 32, i64 32, i64 32, i32 0, metadata !255} ; [ DW_TAG_member ] [si_uid] [line 77, size 32, align 32, offset 32] [from __uid_t]
!255 = metadata !{i32 786454, metadata !237, null, metadata !"__uid_t", i32 125, i64 0, i64 0, i64 0, i32 0, metadata !256} ; [ DW_TAG_typedef ] [__uid_t] [line 125, size 0, align 0, offset 0] [from unsigned int]
!256 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!257 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_timer", i32 86, i64 128, i64 64, i64 0, i32 0, metadata !258} ; [ DW_TAG_member ] [_timer] [line 86, size 128, align 64, offset 0] [from ]
!258 = metadata !{i32 786451, metadata !237, metadata !243, metadata !"", i32 81, i64 128, i64 64, i32 0, i32 0, null, metadata !259, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 81, size 128, align 64, offset 0] [from ]
!259 = metadata !{metadata !260, metadata !261, metadata !262}
!260 = metadata !{i32 786445, metadata !237, metadata !258, metadata !"si_tid", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !70} ; [ DW_TAG_member ] [si_tid] [line 83, size 32, align 32, offset 0] [from int]
!261 = metadata !{i32 786445, metadata !237, metadata !258, metadata !"si_overrun", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !70} ; [ DW_TAG_member ] [si_overrun] [line 84, size 32, align 32, offset 32] [from int]
!262 = metadata !{i32 786445, metadata !237, metadata !258, metadata !"si_sigval", i32 85, i64 64, i64 64, i64 64, i32 0, metadata !263} ; [ DW_TAG_member ] [si_sigval] [line 85, size 64, align 64, offset 64] [from sigval_t]
!263 = metadata !{i32 786454, metadata !237, null, metadata !"sigval_t", i32 36, i64 0, i64 0, i64 0, i32 0, metadata !264} ; [ DW_TAG_typedef ] [sigval_t] [line 36, size 0, align 0, offset 0] [from sigval]
!264 = metadata !{i32 786455, metadata !237, null, metadata !"sigval", i32 32, i64 64, i64 64, i64 0, i32 0, null, metadata !265, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [sigval] [line 32, size 64, align 64, offset 0] [from ]
!265 = metadata !{metadata !266, metadata !267}
!266 = metadata !{i32 786445, metadata !237, metadata !264, metadata !"sival_int", i32 34, i64 32, i64 32, i64 0, i32 0, metadata !70} ; [ DW_TAG_member ] [sival_int] [line 34, size 32, align 32, offset 0] [from int]
!267 = metadata !{i32 786445, metadata !237, metadata !264, metadata !"sival_ptr", i32 35, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_member ] [sival_ptr] [line 35, size 64, align 64, offset 0] [from ]
!268 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_rt", i32 94, i64 128, i64 64, i64 0, i32 0, metadata !269} ; [ DW_TAG_member ] [_rt] [line 94, size 128, align 64, offset 0] [from ]
!269 = metadata !{i32 786451, metadata !237, metadata !243, metadata !"", i32 89, i64 128, i64 64, i32 0, i32 0, null, metadata !270, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 89, size 128, align 64, offset 0] [from ]
!270 = metadata !{metadata !271, metadata !272, metadata !273}
!271 = metadata !{i32 786445, metadata !237, metadata !269, metadata !"si_pid", i32 91, i64 32, i64 32, i64 0, i32 0, metadata !253} ; [ DW_TAG_member ] [si_pid] [line 91, size 32, align 32, offset 0] [from __pid_t]
!272 = metadata !{i32 786445, metadata !237, metadata !269, metadata !"si_uid", i32 92, i64 32, i64 32, i64 32, i32 0, metadata !255} ; [ DW_TAG_member ] [si_uid] [line 92, size 32, align 32, offset 32] [from __uid_t]
!273 = metadata !{i32 786445, metadata !237, metadata !269, metadata !"si_sigval", i32 93, i64 64, i64 64, i64 64, i32 0, metadata !263} ; [ DW_TAG_member ] [si_sigval] [line 93, size 64, align 64, offset 64] [from sigval_t]
!274 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_sigchld", i32 104, i64 256, i64 64, i64 0, i32 0, metadata !275} ; [ DW_TAG_member ] [_sigchld] [line 104, size 256, align 64, offset 0] [from ]
!275 = metadata !{i32 786451, metadata !237, metadata !243, metadata !"", i32 97, i64 256, i64 64, i32 0, i32 0, null, metadata !276, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 97, size 256, align 64, offset 0] [from ]
!276 = metadata !{metadata !277, metadata !278, metadata !279, metadata !280, metadata !283}
!277 = metadata !{i32 786445, metadata !237, metadata !275, metadata !"si_pid", i32 99, i64 32, i64 32, i64 0, i32 0, metadata !253} ; [ DW_TAG_member ] [si_pid] [line 99, size 32, align 32, offset 0] [from __pid_t]
!278 = metadata !{i32 786445, metadata !237, metadata !275, metadata !"si_uid", i32 100, i64 32, i64 32, i64 32, i32 0, metadata !255} ; [ DW_TAG_member ] [si_uid] [line 100, size 32, align 32, offset 32] [from __uid_t]
!279 = metadata !{i32 786445, metadata !237, metadata !275, metadata !"si_status", i32 101, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [si_status] [line 101, size 32, align 32, offset 64] [from int]
!280 = metadata !{i32 786445, metadata !237, metadata !275, metadata !"si_utime", i32 102, i64 64, i64 64, i64 128, i32 0, metadata !281} ; [ DW_TAG_member ] [si_utime] [line 102, size 64, align 64, offset 128] [from __sigchld_clock_t]
!281 = metadata !{i32 786454, metadata !237, null, metadata !"__sigchld_clock_t", i32 58, i64 0, i64 0, i64 0, i32 0, metadata !282} ; [ DW_TAG_typedef ] [__sigchld_clock_t] [line 58, size 0, align 0, offset 0] [from __clock_t]
!282 = metadata !{i32 786454, metadata !237, null, metadata !"__clock_t", i32 135, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_typedef ] [__clock_t] [line 135, size 0, align 0, offset 0] [from long int]
!283 = metadata !{i32 786445, metadata !237, metadata !275, metadata !"si_stime", i32 103, i64 64, i64 64, i64 192, i32 0, metadata !281} ; [ DW_TAG_member ] [si_stime] [line 103, size 64, align 64, offset 192] [from __sigchld_clock_t]
!284 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_sigfault", i32 111, i64 128, i64 64, i64 0, i32 0, metadata !285} ; [ DW_TAG_member ] [_sigfault] [line 111, size 128, align 64, offset 0] [from ]
!285 = metadata !{i32 786451, metadata !237, metadata !243, metadata !"", i32 107, i64 128, i64 64, i32 0, i32 0, null, metadata !286, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 107, size 128, align 64, offset 0] [from ]
!286 = metadata !{metadata !287, metadata !288}
!287 = metadata !{i32 786445, metadata !237, metadata !285, metadata !"si_addr", i32 109, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_member ] [si_addr] [line 109, size 64, align 64, offset 0] [from ]
!288 = metadata !{i32 786445, metadata !237, metadata !285, metadata !"si_addr_lsb", i32 110, i64 16, i64 16, i64 64, i32 0, metadata !289} ; [ DW_TAG_member ] [si_addr_lsb] [line 110, size 16, align 16, offset 64] [from short]
!289 = metadata !{i32 786468, null, null, metadata !"short", i32 0, i64 16, i64 16, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [short] [line 0, size 16, align 16, offset 0, enc DW_ATE_signed]
!290 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_sigpoll", i32 118, i64 128, i64 64, i64 0, i32 0, metadata !291} ; [ DW_TAG_member ] [_sigpoll] [line 118, size 128, align 64, offset 0] [from ]
!291 = metadata !{i32 786451, metadata !237, metadata !243, metadata !"", i32 114, i64 128, i64 64, i32 0, i32 0, null, metadata !292, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 114, size 128, align 64, offset 0] [from ]
!292 = metadata !{metadata !293, metadata !294}
!293 = metadata !{i32 786445, metadata !237, metadata !291, metadata !"si_band", i32 116, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [si_band] [line 116, size 64, align 64, offset 0] [from long int]
!294 = metadata !{i32 786445, metadata !237, metadata !291, metadata !"si_fd", i32 117, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [si_fd] [line 117, size 32, align 32, offset 64] [from int]
!295 = metadata !{i32 786445, metadata !237, metadata !243, metadata !"_sigsys", i32 126, i64 128, i64 64, i64 0, i32 0, metadata !296} ; [ DW_TAG_member ] [_sigsys] [line 126, size 128, align 64, offset 0] [from ]
!296 = metadata !{i32 786451, metadata !237, metadata !243, metadata !"", i32 121, i64 128, i64 64, i32 0, i32 0, null, metadata !297, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 121, size 128, align 64, offset 0] [from ]
!297 = metadata !{metadata !298, metadata !299, metadata !300}
!298 = metadata !{i32 786445, metadata !237, metadata !296, metadata !"_call_addr", i32 123, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_member ] [_call_addr] [line 123, size 64, align 64, offset 0] [from ]
!299 = metadata !{i32 786445, metadata !237, metadata !296, metadata !"_syscall", i32 124, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [_syscall] [line 124, size 32, align 32, offset 64] [from int]
!300 = metadata !{i32 786445, metadata !237, metadata !296, metadata !"_arch", i32 125, i64 32, i64 32, i64 96, i32 0, metadata !256} ; [ DW_TAG_member ] [_arch] [line 125, size 32, align 32, offset 96] [from unsigned int]
!301 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVInit", metadata !"GVInit", metadata !"", i32 302, metadata !126, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, null, null, null, metadata !19, i32 303} ; [ DW_TAG_subprogram ] [line 302] [local] [def] [scope 303] [GVInit]
!302 = metadata !{metadata !303, metadata !304, metadata !305, metadata !306, metadata !307, metadata !308, metadata !310, metadata !314, metadata !318, metadata !335}
!303 = metadata !{i32 786484, i32 0, null, metadata !"StartTally", metadata !"StartTally", metadata !"", metadata !22, i32 498, metadata !62, i32 0, i32 1, i64* @StartTally, null} ; [ DW_TAG_variable ] [StartTally] [line 498] [def]
!304 = metadata !{i32 786484, i32 0, null, metadata !"AbortTally", metadata !"AbortTally", metadata !"", metadata !22, i32 499, metadata !62, i32 0, i32 1, i64* @AbortTally, null} ; [ DW_TAG_variable ] [AbortTally] [line 499] [def]
!305 = metadata !{i32 786484, i32 0, null, metadata !"ReadOverflowTally", metadata !"ReadOverflowTally", metadata !"", metadata !22, i32 500, metadata !62, i32 0, i32 1, i64* @ReadOverflowTally, null} ; [ DW_TAG_variable ] [ReadOverflowTally] [line 500] [def]
!306 = metadata !{i32 786484, i32 0, null, metadata !"WriteOverflowTally", metadata !"WriteOverflowTally", metadata !"", metadata !22, i32 501, metadata !62, i32 0, i32 1, i64* @WriteOverflowTally, null} ; [ DW_TAG_variable ] [WriteOverflowTally] [line 501] [def]
!307 = metadata !{i32 786484, i32 0, null, metadata !"LocalOverflowTally", metadata !"LocalOverflowTally", metadata !"", metadata !22, i32 502, metadata !62, i32 0, i32 1, i64* @LocalOverflowTally, null} ; [ DW_TAG_variable ] [LocalOverflowTally] [line 502] [def]
!308 = metadata !{i32 786484, i32 0, null, metadata !"global_key_self", metadata !"global_key_self", metadata !"", metadata !22, i32 189, metadata !309, i32 1, i32 1, i32* @global_key_self, null} ; [ DW_TAG_variable ] [global_key_self] [line 189] [local] [def]
!309 = metadata !{i32 786454, metadata !1, null, metadata !"pthread_key_t", i32 163, i64 0, i64 0, i64 0, i32 0, metadata !256} ; [ DW_TAG_typedef ] [pthread_key_t] [line 163, size 0, align 0, offset 0] [from unsigned int]
!310 = metadata !{i32 786484, i32 0, null, metadata !"LockTab", metadata !"LockTab", metadata !"", metadata !22, i32 285, metadata !311, i32 1, i32 1, [1048576 x i64]* @LockTab, null} ; [ DW_TAG_variable ] [LockTab] [line 285] [local] [def]
!311 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 67108864, i64 64, i32 0, i32 0, metadata !26, metadata !312, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 67108864, align 64, offset 0] [from ]
!312 = metadata !{metadata !313}
!313 = metadata !{i32 786465, i64 0, i64 1048576} ; [ DW_TAG_subrange_type ] [0, 1048575]
!314 = metadata !{i32 786484, i32 0, null, metadata !"GClock", metadata !"GClock", metadata !"", metadata !22, i32 293, metadata !315, i32 1, i32 1, [64 x i64]* @GClock, null} ; [ DW_TAG_variable ] [GClock] [line 293] [local] [def]
!315 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 4096, i64 64, i32 0, i32 0, metadata !26, metadata !316, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 4096, align 64, offset 0] [from ]
!316 = metadata !{metadata !317}
!317 = metadata !{i32 786465, i64 0, i64 64}      ; [ DW_TAG_subrange_type ] [0, 63]
!318 = metadata !{i32 786484, i32 0, null, metadata !"global_act_oldsigsegv", metadata !"global_act_oldsigsegv", metadata !"", metadata !22, i32 191, metadata !319, i32 1, i32 1, %struct.sigaction* @global_act_oldsigsegv, null} ; [ DW_TAG_variable ] [global_act_oldsigsegv] [line 191] [local] [def]
!319 = metadata !{i32 786451, metadata !235, null, metadata !"sigaction", i32 24, i64 1216, i64 64, i32 0, i32 0, null, metadata !320, i32 0, null, null} ; [ DW_TAG_structure_type ] [sigaction] [line 24, size 1216, align 64, offset 0] [from ]
!320 = metadata !{metadata !321, metadata !331, metadata !332, metadata !333}
!321 = metadata !{i32 786445, metadata !235, metadata !319, metadata !"__sigaction_handler", i32 35, i64 64, i64 64, i64 0, i32 0, metadata !322} ; [ DW_TAG_member ] [__sigaction_handler] [line 35, size 64, align 64, offset 0] [from ]
!322 = metadata !{i32 786455, metadata !235, metadata !319, metadata !"", i32 28, i64 64, i64 64, i64 0, i32 0, null, metadata !323, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [line 28, size 64, align 64, offset 0] [from ]
!323 = metadata !{metadata !324, metadata !329}
!324 = metadata !{i32 786445, metadata !235, metadata !322, metadata !"sa_handler", i32 31, i64 64, i64 64, i64 0, i32 0, metadata !325} ; [ DW_TAG_member ] [sa_handler] [line 31, size 64, align 64, offset 0] [from __sighandler_t]
!325 = metadata !{i32 786454, metadata !235, null, metadata !"__sighandler_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !326} ; [ DW_TAG_typedef ] [__sighandler_t] [line 85, size 0, align 0, offset 0] [from ]
!326 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !327} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!327 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !328, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!328 = metadata !{null, metadata !70}
!329 = metadata !{i32 786445, metadata !235, metadata !322, metadata !"sa_sigaction", i32 33, i64 64, i64 64, i64 0, i32 0, metadata !330} ; [ DW_TAG_member ] [sa_sigaction] [line 33, size 64, align 64, offset 0] [from ]
!330 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !231} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!331 = metadata !{i32 786445, metadata !235, metadata !319, metadata !"sa_mask", i32 43, i64 1024, i64 64, i64 64, i32 0, metadata !110} ; [ DW_TAG_member ] [sa_mask] [line 43, size 1024, align 64, offset 64] [from __sigset_t]
!332 = metadata !{i32 786445, metadata !235, metadata !319, metadata !"sa_flags", i32 46, i64 32, i64 32, i64 1088, i32 0, metadata !70} ; [ DW_TAG_member ] [sa_flags] [line 46, size 32, align 32, offset 1088] [from int]
!333 = metadata !{i32 786445, metadata !235, metadata !319, metadata !"sa_restorer", i32 49, i64 64, i64 64, i64 1152, i32 0, metadata !334} ; [ DW_TAG_member ] [sa_restorer] [line 49, size 64, align 64, offset 1152] [from ]
!334 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !126} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!335 = metadata !{i32 786484, i32 0, null, metadata !"global_act_oldsigbus", metadata !"global_act_oldsigbus", metadata !"", metadata !22, i32 190, metadata !319, i32 1, i32 1, %struct.sigaction* @global_act_oldsigbus, null} ; [ DW_TAG_variable ] [global_act_oldsigbus] [line 190] [local] [def]
!336 = metadata !{i32 786689, metadata !21, metadata !"Addr", metadata !22, i32 16777790, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 574]
!337 = metadata !{i32 574, i32 0, metadata !21, null}
!338 = metadata !{i32 576, i32 0, metadata !21, null}
!339 = metadata !{i32 786689, metadata !34, metadata !"k", metadata !22, i32 16777832, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 616]
!340 = metadata !{i32 616, i32 0, metadata !34, null}
!341 = metadata !{i32 786689, metadata !34, metadata !"sz", metadata !22, i32 33555048, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [sz] [line 616]
!342 = metadata !{i32 786688, metadata !34, metadata !"e", metadata !22, i32 619, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 619]
!343 = metadata !{i32 619, i32 0, metadata !34, null}
!344 = metadata !{i32 620, i32 0, metadata !34, null}
!345 = metadata !{i32 621, i32 0, metadata !346, null}
!346 = metadata !{i32 786443, metadata !1, metadata !34, i32 620, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!347 = metadata !{i32 786688, metadata !348, metadata !"tmp", metadata !22, i32 622, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [tmp] [line 622]
!348 = metadata !{i32 786443, metadata !1, metadata !346, i32 621, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!349 = metadata !{i32 622, i32 0, metadata !348, null}
!350 = metadata !{i32 623, i32 0, metadata !348, null}
!351 = metadata !{i32 624, i32 0, metadata !348, null}
!352 = metadata !{i32 625, i32 0, metadata !348, null}
!353 = metadata !{i32 626, i32 0, metadata !346, null}
!354 = metadata !{i32 629, i32 0, metadata !34, null}
!355 = metadata !{i32 630, i32 0, metadata !34, null}
!356 = metadata !{i32 786688, metadata !357, metadata !"a", metadata !22, i32 1016, metadata !358, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 1016]
!357 = metadata !{i32 786443, metadata !1, metadata !125, i32 1016, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!358 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 32, i64 32, i32 0, i32 0, metadata !70, metadata !78, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 32, align 32, offset 0] [from int]
!359 = metadata !{i32 1016, i32 0, metadata !357, null}
!360 = metadata !{i32 304, i32 0, metadata !301, metadata !361}
!361 = metadata !{i32 1018, i32 0, metadata !125, null}
!362 = metadata !{i32 1019, i32 0, metadata !125, null}
!363 = metadata !{i32 1021, i32 0, metadata !125, null}
!364 = metadata !{i32 1022, i32 0, metadata !125, null}
!365 = metadata !{i32 1024, i32 0, metadata !125, null}
!366 = metadata !{i32 1034, i32 0, metadata !128, null}
!367 = metadata !{i32 1049, i32 0, metadata !128, null}
!368 = metadata !{i32 1051, i32 0, metadata !128, null}
!369 = metadata !{i32 1053, i32 0, metadata !128, null}
!370 = metadata !{i32 -2146797225}
!371 = metadata !{i32 1054, i32 0, metadata !128, null}
!372 = metadata !{i32 786688, metadata !129, metadata !"t", metadata !22, i32 1066, metadata !132, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [t] [line 1066]
!373 = metadata !{i32 1066, i32 0, metadata !129, null}
!374 = metadata !{i32 1067, i32 0, metadata !129, null}
!375 = metadata !{i32 1071, i32 0, metadata !129, null}
!376 = metadata !{i32 786689, metadata !134, metadata !"t", metadata !22, i32 16778298, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [t] [line 1082]
!377 = metadata !{i32 1082, i32 0, metadata !134, null}
!378 = metadata !{i32 1084, i32 0, metadata !134, null}
!379 = metadata !{i32 786689, metadata !225, metadata !"addr", metadata !22, i32 16777470, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [addr] [line 254]
!380 = metadata !{i32 254, i32 0, metadata !225, metadata !378}
!381 = metadata !{i32 786689, metadata !225, metadata !"dx", metadata !22, i32 33554686, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dx] [line 254]
!382 = metadata !{i32 786688, metadata !225, metadata !"v", metadata !22, i32 256, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 256]
!383 = metadata !{i32 256, i32 0, metadata !225, metadata !378}
!384 = metadata !{i32 257, i32 0, metadata !385, metadata !378}
!385 = metadata !{i32 786443, metadata !1, metadata !225, i32 257, i32 0, i32 77} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!386 = metadata !{i32 257, i32 21, metadata !385, metadata !378}
!387 = metadata !{i32 786689, metadata !174, metadata !"newVal", metadata !176, i32 16777258, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [newVal] [line 42]
!388 = metadata !{i32 42, i32 0, metadata !174, metadata !386}
!389 = metadata !{i32 786689, metadata !174, metadata !"oldVal", metadata !176, i32 33554474, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [oldVal] [line 42]
!390 = metadata !{i32 786689, metadata !174, metadata !"ptr", metadata !176, i32 50331690, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ptr] [line 42]
!391 = metadata !{i32 786688, metadata !392, metadata !"prevVal", metadata !176, i32 44, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [prevVal] [line 44]
!392 = metadata !{i32 786443, metadata !175, metadata !174} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/./platform_x86.h]
!393 = metadata !{i32 44, i32 0, metadata !392, metadata !386}
!394 = metadata !{i32 46, i32 0, metadata !392, metadata !386}
!395 = metadata !{i32 584193, i32 584228}
!396 = metadata !{i32 58, i32 0, metadata !392, metadata !386} ; [ DW_TAG_imported_module ]
!397 = metadata !{i32 258, i32 0, metadata !225, metadata !378}
!398 = metadata !{i32 786688, metadata !134, metadata !"wrSetOvf", metadata !22, i32 1086, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wrSetOvf] [line 1086]
!399 = metadata !{i32 1086, i32 0, metadata !134, null}
!400 = metadata !{i32 786688, metadata !134, metadata !"wr", metadata !22, i32 1087, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1087]
!401 = metadata !{i32 1087, i32 0, metadata !134, null}
!402 = metadata !{i32 1094, i32 0, metadata !134, null}
!403 = metadata !{i32 1097, i32 0, metadata !404, null}
!404 = metadata !{i32 786443, metadata !1, metadata !134, i32 1096, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!405 = metadata !{i32 1099, i32 0, metadata !134, null}
!406 = metadata !{i32 254, i32 0, metadata !225, metadata !405}
!407 = metadata !{i32 256, i32 0, metadata !225, metadata !405}
!408 = metadata !{i32 257, i32 0, metadata !385, metadata !405}
!409 = metadata !{i32 257, i32 21, metadata !385, metadata !405}
!410 = metadata !{i32 42, i32 0, metadata !174, metadata !409}
!411 = metadata !{i32 44, i32 0, metadata !392, metadata !409}
!412 = metadata !{i32 46, i32 0, metadata !392, metadata !409}
!413 = metadata !{i32 58, i32 0, metadata !392, metadata !409} ; [ DW_TAG_imported_module ]
!414 = metadata !{i32 258, i32 0, metadata !225, metadata !405}
!415 = metadata !{i32 1101, i32 0, metadata !134, null}
!416 = metadata !{i32 254, i32 0, metadata !225, metadata !415}
!417 = metadata !{i32 256, i32 0, metadata !225, metadata !415}
!418 = metadata !{i32 257, i32 0, metadata !385, metadata !415}
!419 = metadata !{i32 257, i32 21, metadata !385, metadata !415}
!420 = metadata !{i32 42, i32 0, metadata !174, metadata !419}
!421 = metadata !{i32 44, i32 0, metadata !392, metadata !419}
!422 = metadata !{i32 46, i32 0, metadata !392, metadata !419}
!423 = metadata !{i32 58, i32 0, metadata !392, metadata !419} ; [ DW_TAG_imported_module ]
!424 = metadata !{i32 258, i32 0, metadata !225, metadata !415}
!425 = metadata !{i32 1103, i32 0, metadata !134, null}
!426 = metadata !{i32 254, i32 0, metadata !225, metadata !425}
!427 = metadata !{i32 256, i32 0, metadata !225, metadata !425}
!428 = metadata !{i32 257, i32 0, metadata !385, metadata !425}
!429 = metadata !{i32 257, i32 21, metadata !385, metadata !425}
!430 = metadata !{i32 42, i32 0, metadata !174, metadata !429}
!431 = metadata !{i32 44, i32 0, metadata !392, metadata !429}
!432 = metadata !{i32 46, i32 0, metadata !392, metadata !429}
!433 = metadata !{i32 58, i32 0, metadata !392, metadata !429} ; [ DW_TAG_imported_module ]
!434 = metadata !{i32 258, i32 0, metadata !225, metadata !425}
!435 = metadata !{i32 1104, i32 0, metadata !134, null}
!436 = metadata !{i32 254, i32 0, metadata !225, metadata !435}
!437 = metadata !{i32 256, i32 0, metadata !225, metadata !435}
!438 = metadata !{i32 257, i32 0, metadata !385, metadata !435}
!439 = metadata !{i32 257, i32 21, metadata !385, metadata !435}
!440 = metadata !{i32 42, i32 0, metadata !174, metadata !439}
!441 = metadata !{i32 44, i32 0, metadata !392, metadata !439}
!442 = metadata !{i32 46, i32 0, metadata !392, metadata !439}
!443 = metadata !{i32 58, i32 0, metadata !392, metadata !439} ; [ DW_TAG_imported_module ]
!444 = metadata !{i32 258, i32 0, metadata !225, metadata !435}
!445 = metadata !{i32 1106, i32 0, metadata !134, null}
!446 = metadata !{i32 1107, i32 0, metadata !134, null}
!447 = metadata !{i32 1109, i32 0, metadata !134, null}
!448 = metadata !{i32 1113, i32 0, metadata !134, null}
!449 = metadata !{i32 1115, i32 0, metadata !134, null}
!450 = metadata !{i32 1117, i32 0, metadata !134, null}
!451 = metadata !{i32 1118, i32 0, metadata !134, null}
!452 = metadata !{i32 786689, metadata !137, metadata !"t", metadata !22, i32 16778342, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [t] [line 1126]
!453 = metadata !{i32 1126, i32 0, metadata !137, null}
!454 = metadata !{i32 786689, metadata !137, metadata !"id", metadata !22, i32 33555558, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [id] [line 1126]
!455 = metadata !{i32 1129, i32 0, metadata !137, null}
!456 = metadata !{i32 1131, i32 0, metadata !137, null}
!457 = metadata !{i32 1133, i32 0, metadata !137, null}
!458 = metadata !{i32 1134, i32 0, metadata !137, null}
!459 = metadata !{i32 1135, i32 0, metadata !137, null}
!460 = metadata !{i32 1140, i32 21, metadata !137, null}
!461 = metadata !{i32 786689, metadata !222, metadata !"sz", metadata !22, i32 16777804, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [sz] [line 588]
!462 = metadata !{i32 588, i32 0, metadata !222, metadata !460}
!463 = metadata !{i32 786689, metadata !222, metadata !"Self", metadata !22, i32 33555020, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 588]
!464 = metadata !{i32 786688, metadata !222, metadata !"ap", metadata !22, i32 590, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ap] [line 590]
!465 = metadata !{i32 590, i32 0, metadata !222, metadata !460}
!466 = metadata !{i32 591, i32 0, metadata !222, metadata !460}
!467 = metadata !{i32 592, i32 0, metadata !222, metadata !460}
!468 = metadata !{i32 786688, metadata !222, metadata !"List", metadata !22, i32 593, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [List] [line 593]
!469 = metadata !{i32 593, i32 0, metadata !222, metadata !460}
!470 = metadata !{i32 786688, metadata !222, metadata !"Tail", metadata !22, i32 594, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Tail] [line 594]
!471 = metadata !{i32 594, i32 0, metadata !222, metadata !460}
!472 = metadata !{i32 786688, metadata !222, metadata !"i", metadata !22, i32 595, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 595]
!473 = metadata !{i32 595, i32 0, metadata !222, metadata !460}
!474 = metadata !{i32 596, i32 0, metadata !475, metadata !460}
!475 = metadata !{i32 786443, metadata !1, metadata !222, i32 596, i32 0, i32 75} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!476 = metadata !{i32 786688, metadata !477, metadata !"e", metadata !22, i32 597, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 597]
!477 = metadata !{i32 786443, metadata !1, metadata !475, i32 596, i32 0, i32 76} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!478 = metadata !{i32 597, i32 0, metadata !477, metadata !460}
!479 = metadata !{i32 598, i32 0, metadata !477, metadata !460}
!480 = metadata !{i32 599, i32 0, metadata !477, metadata !460}
!481 = metadata !{i32 600, i32 0, metadata !477, metadata !460}
!482 = metadata !{i32 601, i32 0, metadata !477, metadata !460}
!483 = metadata !{i32 602, i32 0, metadata !477, metadata !460}
!484 = metadata !{i32 604, i32 0, metadata !222, metadata !460}
!485 = metadata !{i32 606, i32 0, metadata !222, metadata !460}
!486 = metadata !{i32 1141, i32 0, metadata !137, null}
!487 = metadata !{i32 1144, i32 21, metadata !137, null}
!488 = metadata !{i32 588, i32 0, metadata !222, metadata !487}
!489 = metadata !{i32 590, i32 0, metadata !222, metadata !487}
!490 = metadata !{i32 591, i32 0, metadata !222, metadata !487}
!491 = metadata !{i32 592, i32 0, metadata !222, metadata !487}
!492 = metadata !{i32 593, i32 0, metadata !222, metadata !487}
!493 = metadata !{i32 594, i32 0, metadata !222, metadata !487}
!494 = metadata !{i32 595, i32 0, metadata !222, metadata !487}
!495 = metadata !{i32 596, i32 0, metadata !475, metadata !487}
!496 = metadata !{i32 597, i32 0, metadata !477, metadata !487}
!497 = metadata !{i32 598, i32 0, metadata !477, metadata !487}
!498 = metadata !{i32 599, i32 0, metadata !477, metadata !487}
!499 = metadata !{i32 600, i32 0, metadata !477, metadata !487}
!500 = metadata !{i32 601, i32 0, metadata !477, metadata !487}
!501 = metadata !{i32 602, i32 0, metadata !477, metadata !487}
!502 = metadata !{i32 604, i32 0, metadata !222, metadata !487}
!503 = metadata !{i32 606, i32 0, metadata !222, metadata !487}
!504 = metadata !{i32 1145, i32 0, metadata !137, null}
!505 = metadata !{i32 1147, i32 25, metadata !137, null}
!506 = metadata !{i32 588, i32 0, metadata !222, metadata !505}
!507 = metadata !{i32 590, i32 0, metadata !222, metadata !505}
!508 = metadata !{i32 591, i32 0, metadata !222, metadata !505}
!509 = metadata !{i32 592, i32 0, metadata !222, metadata !505}
!510 = metadata !{i32 593, i32 0, metadata !222, metadata !505}
!511 = metadata !{i32 594, i32 0, metadata !222, metadata !505}
!512 = metadata !{i32 595, i32 0, metadata !222, metadata !505}
!513 = metadata !{i32 596, i32 0, metadata !475, metadata !505}
!514 = metadata !{i32 597, i32 0, metadata !477, metadata !505}
!515 = metadata !{i32 598, i32 0, metadata !477, metadata !505}
!516 = metadata !{i32 599, i32 0, metadata !477, metadata !505}
!517 = metadata !{i32 600, i32 0, metadata !477, metadata !505}
!518 = metadata !{i32 601, i32 0, metadata !477, metadata !505}
!519 = metadata !{i32 602, i32 0, metadata !477, metadata !505}
!520 = metadata !{i32 604, i32 0, metadata !222, metadata !505}
!521 = metadata !{i32 606, i32 0, metadata !222, metadata !505}
!522 = metadata !{i32 1148, i32 0, metadata !137, null}
!523 = metadata !{i32 1150, i32 0, metadata !137, null}
!524 = metadata !{i32 1151, i32 0, metadata !137, null}
!525 = metadata !{i32 1152, i32 0, metadata !137, null}
!526 = metadata !{i32 1153, i32 0, metadata !137, null}
!527 = metadata !{i32 1158, i32 0, metadata !137, null}
!528 = metadata !{i32 786689, metadata !140, metadata !"Self", metadata !22, i32 16778920, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1704]
!529 = metadata !{i32 1704, i32 0, metadata !140, null}
!530 = metadata !{i32 1708, i32 0, metadata !140, null}
!531 = metadata !{i32 1714, i32 0, metadata !140, null}
!532 = metadata !{i32 1715, i32 0, metadata !533, null}
!533 = metadata !{i32 786443, metadata !1, metadata !140, i32 1714, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!534 = metadata !{i32 786689, metadata !221, metadata !"Self", metadata !22, i32 16778555, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1339]
!535 = metadata !{i32 1339, i32 0, metadata !221, metadata !532}
!536 = metadata !{i32 786688, metadata !221, metadata !"wr", metadata !22, i32 1348, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1348]
!537 = metadata !{i32 1348, i32 0, metadata !221, metadata !532}
!538 = metadata !{i32 786688, metadata !539, metadata !"p", metadata !22, i32 1351, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1351]
!539 = metadata !{i32 786443, metadata !1, metadata !221, i32 1350, i32 0, i32 71} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!540 = metadata !{i32 1351, i32 0, metadata !539, metadata !532}
!541 = metadata !{i32 786688, metadata !539, metadata !"End", metadata !22, i32 1352, metadata !542, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1352]
!542 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !42} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!543 = metadata !{i32 1352, i32 0, metadata !539, metadata !532}
!544 = metadata !{i32 1353, i32 0, metadata !545, metadata !532}
!545 = metadata !{i32 786443, metadata !1, metadata !539, i32 1353, i32 0, i32 72} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!546 = metadata !{i32 1357, i32 0, metadata !547, metadata !532}
!547 = metadata !{i32 786443, metadata !1, metadata !545, i32 1353, i32 0, i32 73} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!548 = metadata !{i32 1358, i32 0, metadata !549, metadata !532}
!549 = metadata !{i32 786443, metadata !1, metadata !547, i32 1357, i32 0, i32 74} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!550 = metadata !{i32 1365, i32 0, metadata !547, metadata !532}
!551 = metadata !{i32 1367, i32 0, metadata !547, metadata !532}
!552 = metadata !{i32 1368, i32 0, metadata !547, metadata !532}
!553 = metadata !{i32 1371, i32 0, metadata !221, metadata !532}
!554 = metadata !{i32 1716, i32 0, metadata !533, null}
!555 = metadata !{i32 1720, i32 0, metadata !140, null}
!556 = metadata !{i32 1721, i32 0, metadata !557, null}
!557 = metadata !{i32 786443, metadata !1, metadata !140, i32 1720, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!558 = metadata !{i32 786689, metadata !220, metadata !"k", metadata !22, i32 16777988, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 772]
!559 = metadata !{i32 772, i32 0, metadata !220, metadata !556}
!560 = metadata !{i32 786688, metadata !220, metadata !"e", metadata !22, i32 774, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 774]
!561 = metadata !{i32 774, i32 0, metadata !220, metadata !556}
!562 = metadata !{i32 775, i32 0, metadata !563, metadata !556}
!563 = metadata !{i32 786443, metadata !1, metadata !220, i32 775, i32 0, i32 69} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!564 = metadata !{i32 776, i32 0, metadata !565, metadata !556}
!565 = metadata !{i32 786443, metadata !1, metadata !563, i32 775, i32 0, i32 70} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!566 = metadata !{i32 1722, i32 0, metadata !557, null}
!567 = metadata !{i32 1724, i32 0, metadata !140, null}
!568 = metadata !{i32 1725, i32 0, metadata !140, null}
!569 = metadata !{i32 1727, i32 9, metadata !140, null}
!570 = metadata !{i32 786689, metadata !219, metadata !"Self", metadata !22, i32 16777691, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 475]
!571 = metadata !{i32 475, i32 0, metadata !219, metadata !569}
!572 = metadata !{i32 1729, i32 0, metadata !573, null}
!573 = metadata !{i32 786443, metadata !1, metadata !140, i32 1727, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!574 = metadata !{i32 1749, i32 0, metadata !140, null}
!575 = metadata !{i32 1750, i32 0, metadata !576, null}
!576 = metadata !{i32 786443, metadata !1, metadata !140, i32 1749, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!577 = metadata !{i32 786689, metadata !208, metadata !"Self", metadata !22, i32 16778643, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1427]
!578 = metadata !{i32 1427, i32 0, metadata !208, metadata !575}
!579 = metadata !{i32 786689, metadata !208, metadata !"attempt", metadata !22, i32 33555859, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [attempt] [line 1427]
!580 = metadata !{i32 786688, metadata !208, metadata !"stall", metadata !22, i32 1433, metadata !75, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [stall] [line 1433]
!581 = metadata !{i32 1433, i32 0, metadata !208, metadata !575}
!582 = metadata !{i32 1433, i32 32, metadata !208, metadata !575}
!583 = metadata !{i32 786689, metadata !209, metadata !"Self", metadata !22, i32 16777459, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 243]
!584 = metadata !{i32 243, i32 0, metadata !209, metadata !582}
!585 = metadata !{i32 245, i32 12, metadata !209, metadata !582}
!586 = metadata !{i32 786689, metadata !212, metadata !"seed", metadata !22, i32 16777446, metadata !215, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [seed] [line 230]
!587 = metadata !{i32 230, i32 0, metadata !212, metadata !585}
!588 = metadata !{i32 786688, metadata !212, metadata !"x", metadata !22, i32 232, metadata !75, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 232]
!589 = metadata !{i32 232, i32 0, metadata !212, metadata !585}
!590 = metadata !{i32 232, i32 28, metadata !212, metadata !585}
!591 = metadata !{i32 786689, metadata !216, metadata !"x", metadata !22, i32 16777426, metadata !75, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [x] [line 210]
!592 = metadata !{i32 210, i32 0, metadata !216, metadata !590}
!593 = metadata !{i32 212, i32 0, metadata !216, metadata !590}
!594 = metadata !{i32 213, i32 0, metadata !595, metadata !590}
!595 = metadata !{i32 786443, metadata !1, metadata !216, i32 212, i32 0, i32 68} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!596 = metadata !{i32 214, i32 0, metadata !595, metadata !590}
!597 = metadata !{i32 215, i32 0, metadata !216, metadata !590}
!598 = metadata !{i32 216, i32 0, metadata !216, metadata !590}
!599 = metadata !{i32 217, i32 0, metadata !216, metadata !590}
!600 = metadata !{i32 218, i32 0, metadata !216, metadata !590}
!601 = metadata !{i32 233, i32 0, metadata !212, metadata !585}
!602 = metadata !{i32 234, i32 0, metadata !212, metadata !585}
!603 = metadata !{i32 1434, i32 0, metadata !208, metadata !575}
!604 = metadata !{i32 1435, i32 0, metadata !208, metadata !575}
!605 = metadata !{i32 786688, metadata !208, metadata !"i", metadata !22, i32 1444, metadata !606, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 1444]
!606 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !75} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from long long unsigned int]
!607 = metadata !{i32 1444, i32 0, metadata !208, metadata !575}
!608 = metadata !{i32 1445, i32 0, metadata !208, metadata !575}
!609 = metadata !{i32 1447, i32 0, metadata !610, metadata !575}
!610 = metadata !{i32 786443, metadata !1, metadata !208, i32 1445, i32 0, i32 67} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!611 = metadata !{i32 1751, i32 0, metadata !576, null}
!612 = metadata !{i32 1756, i32 0, metadata !140, null}
!613 = metadata !{i32 1757, i32 0, metadata !140, null}
!614 = metadata !{i32 1760, i32 0, metadata !140, null}
!615 = metadata !{i32 1762, i32 0, metadata !140, null}
!616 = metadata !{i32 786689, metadata !141, metadata !"Self", metadata !22, i32 16779061, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1845]
!617 = metadata !{i32 1845, i32 0, metadata !141, null}
!618 = metadata !{i32 786689, metadata !141, metadata !"addr", metadata !22, i32 33556277, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [addr] [line 1845]
!619 = metadata !{i32 786689, metadata !141, metadata !"valu", metadata !22, i32 50333493, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [valu] [line 1845]
!620 = metadata !{i32 786688, metadata !141, metadata !"LockFor", metadata !22, i32 1849, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 1849]
!621 = metadata !{i32 1849, i32 0, metadata !141, null}
!622 = metadata !{i32 786688, metadata !141, metadata !"rdv", metadata !22, i32 1850, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rdv] [line 1850]
!623 = metadata !{i32 1850, i32 0, metadata !141, null}
!624 = metadata !{i32 1858, i32 0, metadata !141, null}
!625 = metadata !{i32 1859, i32 0, metadata !626, null}
!626 = metadata !{i32 786443, metadata !1, metadata !141, i32 1858, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!627 = metadata !{i32 1861, i32 0, metadata !626, null}
!628 = metadata !{i32 1862, i32 0, metadata !626, null}
!629 = metadata !{i32 1869, i32 0, metadata !141, null}
!630 = metadata !{i32 1885, i32 0, metadata !141, null}
!631 = metadata !{i32 786688, metadata !141, metadata !"wr", metadata !22, i32 1912, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1912]
!632 = metadata !{i32 1912, i32 0, metadata !141, null}
!633 = metadata !{i32 1922, i32 0, metadata !141, null}
!634 = metadata !{i32 786688, metadata !635, metadata !"e", metadata !22, i32 1923, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1923]
!635 = metadata !{i32 786443, metadata !1, metadata !141, i32 1922, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!636 = metadata !{i32 1923, i32 0, metadata !635, null}
!637 = metadata !{i32 1924, i32 0, metadata !638, null}
!638 = metadata !{i32 786443, metadata !1, metadata !635, i32 1924, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!639 = metadata !{i32 1926, i32 0, metadata !640, null}
!640 = metadata !{i32 786443, metadata !1, metadata !638, i32 1924, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!641 = metadata !{i32 1928, i32 0, metadata !642, null}
!642 = metadata !{i32 786443, metadata !1, metadata !640, i32 1926, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!643 = metadata !{i32 1930, i32 0, metadata !642, null}
!644 = metadata !{i32 1932, i32 0, metadata !640, null}
!645 = metadata !{i32 1934, i32 0, metadata !635, null}
!646 = metadata !{i32 1935, i32 18, metadata !647, null}
!647 = metadata !{i32 786443, metadata !1, metadata !635, i32 1934, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!648 = metadata !{i32 786689, metadata !204, metadata !"Self", metadata !22, i32 16778103, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 887]
!649 = metadata !{i32 887, i32 0, metadata !204, metadata !646}
!650 = metadata !{i32 786689, metadata !204, metadata !"LockFor", metadata !22, i32 33555319, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [LockFor] [line 887]
!651 = metadata !{i32 786688, metadata !204, metadata !"k", metadata !22, i32 889, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [k] [line 889]
!652 = metadata !{i32 889, i32 0, metadata !204, metadata !646}
!653 = metadata !{i32 786688, metadata !204, metadata !"e", metadata !22, i32 910, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 910]
!654 = metadata !{i32 910, i32 0, metadata !204, metadata !646}
!655 = metadata !{i32 911, i32 0, metadata !204, metadata !646}
!656 = metadata !{i32 912, i32 14, metadata !657, metadata !646}
!657 = metadata !{i32 786443, metadata !1, metadata !204, i32 911, i32 0, i32 59} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!658 = metadata !{i32 786689, metadata !207, metadata !"Self", metadata !22, i32 16778512, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1296]
!659 = metadata !{i32 1296, i32 0, metadata !207, metadata !656}
!660 = metadata !{i32 786688, metadata !207, metadata !"rv", metadata !22, i32 1298, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rv] [line 1298]
!661 = metadata !{i32 1298, i32 0, metadata !207, metadata !656}
!662 = metadata !{i32 786688, metadata !207, metadata !"rd", metadata !22, i32 1299, metadata !663, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1299]
!663 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !37} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!664 = metadata !{i32 1299, i32 0, metadata !207, metadata !656}
!665 = metadata !{i32 786688, metadata !207, metadata !"EndOfList", metadata !22, i32 1300, metadata !542, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [EndOfList] [line 1300]
!666 = metadata !{i32 1300, i32 0, metadata !207, metadata !656}
!667 = metadata !{i32 786688, metadata !207, metadata !"e", metadata !22, i32 1301, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1301]
!668 = metadata !{i32 1301, i32 0, metadata !207, metadata !656}
!669 = metadata !{i32 1305, i32 0, metadata !670, metadata !656}
!670 = metadata !{i32 786443, metadata !1, metadata !207, i32 1305, i32 0, i32 61} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!671 = metadata !{i32 786688, metadata !672, metadata !"v", metadata !22, i32 1307, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 1307]
!672 = metadata !{i32 786443, metadata !1, metadata !670, i32 1305, i32 0, i32 62} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!673 = metadata !{i32 1307, i32 0, metadata !672, metadata !656}
!674 = metadata !{i32 1308, i32 0, metadata !672, metadata !656}
!675 = metadata !{i32 1319, i32 0, metadata !676, metadata !656}
!676 = metadata !{i32 786443, metadata !1, metadata !672, i32 1308, i32 0, i32 63} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!677 = metadata !{i32 1320, i32 0, metadata !678, metadata !656}
!678 = metadata !{i32 786443, metadata !1, metadata !676, i32 1319, i32 0, i32 64} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!679 = metadata !{i32 1323, i32 0, metadata !676, metadata !656}
!680 = metadata !{i32 1324, i32 0, metadata !681, metadata !656}
!681 = metadata !{i32 786443, metadata !1, metadata !672, i32 1323, i32 0, i32 65} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!682 = metadata !{i32 1325, i32 0, metadata !683, metadata !656}
!683 = metadata !{i32 786443, metadata !1, metadata !681, i32 1324, i32 0, i32 66} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!684 = metadata !{i32 1330, i32 0, metadata !207, metadata !656}
!685 = metadata !{i32 913, i32 0, metadata !686, metadata !646}
!686 = metadata !{i32 786443, metadata !1, metadata !657, i32 912, i32 0, i32 60} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!687 = metadata !{i32 915, i32 0, metadata !657, metadata !646}
!688 = metadata !{i32 916, i32 13, metadata !657, metadata !646}
!689 = metadata !{i32 786689, metadata !198, metadata !"tail", metadata !22, i32 16777857, metadata !42, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [tail] [line 641]
!690 = metadata !{i32 641, i32 0, metadata !198, metadata !688}
!691 = metadata !{i32 786688, metadata !198, metadata !"e", metadata !22, i32 643, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 643]
!692 = metadata !{i32 643, i32 0, metadata !198, metadata !688}
!693 = metadata !{i32 644, i32 0, metadata !198, metadata !688}
!694 = metadata !{i32 645, i32 0, metadata !198, metadata !688}
!695 = metadata !{i32 646, i32 0, metadata !198, metadata !688}
!696 = metadata !{i32 647, i32 0, metadata !198, metadata !688}
!697 = metadata !{i32 648, i32 0, metadata !198, metadata !688}
!698 = metadata !{i32 649, i32 0, metadata !198, metadata !688}
!699 = metadata !{i32 650, i32 0, metadata !198, metadata !688}
!700 = metadata !{i32 652, i32 0, metadata !198, metadata !688}
!701 = metadata !{i32 917, i32 0, metadata !657, metadata !646}
!702 = metadata !{i32 918, i32 0, metadata !657, metadata !646}
!703 = metadata !{i32 920, i32 0, metadata !204, metadata !646}
!704 = metadata !{i32 921, i32 0, metadata !204, metadata !646}
!705 = metadata !{i32 922, i32 0, metadata !204, metadata !646}
!706 = metadata !{i32 925, i32 0, metadata !204, metadata !646}
!707 = metadata !{i32 1937, i32 0, metadata !708, null}
!708 = metadata !{i32 786443, metadata !1, metadata !647, i32 1935, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!709 = metadata !{i32 1938, i32 0, metadata !708, null}
!710 = metadata !{i32 1940, i32 0, metadata !647, null}
!711 = metadata !{i32 1942, i32 0, metadata !635, null}
!712 = metadata !{i32 1947, i32 0, metadata !141, null}
!713 = metadata !{i32 1955, i32 0, metadata !141, null}
!714 = metadata !{i32 786689, metadata !201, metadata !"k", metadata !22, i32 16778048, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 832]
!715 = metadata !{i32 832, i32 0, metadata !201, metadata !713}
!716 = metadata !{i32 786689, metadata !201, metadata !"Addr", metadata !22, i32 33555264, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 832]
!717 = metadata !{i32 786689, metadata !201, metadata !"Valu", metadata !22, i32 50332480, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 832]
!718 = metadata !{i32 786689, metadata !201, metadata !"Lock", metadata !22, i32 67109696, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Lock] [line 832]
!719 = metadata !{i32 786688, metadata !201, metadata !"e", metadata !22, i32 843, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 843]
!720 = metadata !{i32 843, i32 0, metadata !201, metadata !713}
!721 = metadata !{i32 844, i32 0, metadata !201, metadata !713}
!722 = metadata !{i32 845, i32 0, metadata !723, metadata !713}
!723 = metadata !{i32 786443, metadata !1, metadata !201, i32 844, i32 0, i32 58} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!724 = metadata !{i32 846, i32 13, metadata !723, metadata !713}
!725 = metadata !{i32 641, i32 0, metadata !198, metadata !724}
!726 = metadata !{i32 643, i32 0, metadata !198, metadata !724}
!727 = metadata !{i32 644, i32 0, metadata !198, metadata !724}
!728 = metadata !{i32 645, i32 0, metadata !198, metadata !724}
!729 = metadata !{i32 646, i32 0, metadata !198, metadata !724}
!730 = metadata !{i32 647, i32 0, metadata !198, metadata !724}
!731 = metadata !{i32 648, i32 0, metadata !198, metadata !724}
!732 = metadata !{i32 649, i32 0, metadata !198, metadata !724}
!733 = metadata !{i32 650, i32 0, metadata !198, metadata !724}
!734 = metadata !{i32 652, i32 0, metadata !198, metadata !724}
!735 = metadata !{i32 847, i32 0, metadata !723, metadata !713}
!736 = metadata !{i32 848, i32 0, metadata !723, metadata !713}
!737 = metadata !{i32 850, i32 0, metadata !201, metadata !713}
!738 = metadata !{i32 851, i32 0, metadata !201, metadata !713}
!739 = metadata !{i32 852, i32 0, metadata !201, metadata !713}
!740 = metadata !{i32 853, i32 0, metadata !201, metadata !713}
!741 = metadata !{i32 854, i32 0, metadata !201, metadata !713}
!742 = metadata !{i32 855, i32 0, metadata !201, metadata !713}
!743 = metadata !{i32 856, i32 0, metadata !201, metadata !713}
!744 = metadata !{i32 786689, metadata !144, metadata !"Self", metadata !22, i32 16779241, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2025]
!745 = metadata !{i32 2025, i32 0, metadata !144, null}
!746 = metadata !{i32 786689, metadata !144, metadata !"Addr", metadata !22, i32 33556457, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 2025]
!747 = metadata !{i32 786688, metadata !144, metadata !"Valu", metadata !22, i32 2029, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Valu] [line 2029]
!748 = metadata !{i32 2029, i32 0, metadata !144, null}
!749 = metadata !{i32 786688, metadata !144, metadata !"msk", metadata !22, i32 2046, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [msk] [line 2046]
!750 = metadata !{i32 2046, i32 0, metadata !144, null}
!751 = metadata !{i32 2047, i32 0, metadata !144, null}
!752 = metadata !{i32 786688, metadata !753, metadata !"wr", metadata !22, i32 2051, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 2051]
!753 = metadata !{i32 786443, metadata !1, metadata !144, i32 2047, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!754 = metadata !{i32 2051, i32 0, metadata !753, null}
!755 = metadata !{i32 786688, metadata !753, metadata !"e", metadata !22, i32 2053, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 2053]
!756 = metadata !{i32 2053, i32 0, metadata !753, null}
!757 = metadata !{i32 2054, i32 0, metadata !758, null}
!758 = metadata !{i32 786443, metadata !1, metadata !753, i32 2054, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!759 = metadata !{i32 2056, i32 0, metadata !760, null}
!760 = metadata !{i32 786443, metadata !1, metadata !758, i32 2054, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!761 = metadata !{i32 2058, i32 0, metadata !762, null}
!762 = metadata !{i32 786443, metadata !1, metadata !760, i32 2056, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!763 = metadata !{i32 2060, i32 0, metadata !760, null}
!764 = metadata !{i32 2061, i32 0, metadata !753, null}
!765 = metadata !{i32 786688, metadata !144, metadata !"LockFor", metadata !22, i32 2076, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 2076]
!766 = metadata !{i32 2076, i32 0, metadata !144, null}
!767 = metadata !{i32 786688, metadata !144, metadata !"rdv", metadata !22, i32 2077, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rdv] [line 2077]
!768 = metadata !{i32 2077, i32 0, metadata !144, null}
!769 = metadata !{i32 2079, i32 0, metadata !144, null}
!770 = metadata !{i32 2081, i32 0, metadata !144, null}
!771 = metadata !{i32 2082, i32 0, metadata !772, null}
!772 = metadata !{i32 786443, metadata !1, metadata !144, i32 2081, i32 0, i32 19} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!773 = metadata !{i32 2083, i32 18, metadata !774, null}
!774 = metadata !{i32 786443, metadata !1, metadata !772, i32 2082, i32 0, i32 20} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!775 = metadata !{i32 887, i32 0, metadata !204, metadata !773}
!776 = metadata !{i32 889, i32 0, metadata !204, metadata !773}
!777 = metadata !{i32 910, i32 0, metadata !204, metadata !773}
!778 = metadata !{i32 911, i32 0, metadata !204, metadata !773}
!779 = metadata !{i32 912, i32 14, metadata !657, metadata !773}
!780 = metadata !{i32 1296, i32 0, metadata !207, metadata !779}
!781 = metadata !{i32 1298, i32 0, metadata !207, metadata !779}
!782 = metadata !{i32 1299, i32 0, metadata !207, metadata !779}
!783 = metadata !{i32 1300, i32 0, metadata !207, metadata !779}
!784 = metadata !{i32 1301, i32 0, metadata !207, metadata !779}
!785 = metadata !{i32 1305, i32 0, metadata !670, metadata !779}
!786 = metadata !{i32 1307, i32 0, metadata !672, metadata !779}
!787 = metadata !{i32 1308, i32 0, metadata !672, metadata !779}
!788 = metadata !{i32 1319, i32 0, metadata !676, metadata !779}
!789 = metadata !{i32 1320, i32 0, metadata !678, metadata !779}
!790 = metadata !{i32 1323, i32 0, metadata !676, metadata !779}
!791 = metadata !{i32 1324, i32 0, metadata !681, metadata !779}
!792 = metadata !{i32 1325, i32 0, metadata !683, metadata !779}
!793 = metadata !{i32 1330, i32 0, metadata !207, metadata !779}
!794 = metadata !{i32 913, i32 0, metadata !686, metadata !773}
!795 = metadata !{i32 915, i32 0, metadata !657, metadata !773}
!796 = metadata !{i32 916, i32 13, metadata !657, metadata !773}
!797 = metadata !{i32 641, i32 0, metadata !198, metadata !796}
!798 = metadata !{i32 643, i32 0, metadata !198, metadata !796}
!799 = metadata !{i32 644, i32 0, metadata !198, metadata !796}
!800 = metadata !{i32 645, i32 0, metadata !198, metadata !796}
!801 = metadata !{i32 646, i32 0, metadata !198, metadata !796}
!802 = metadata !{i32 647, i32 0, metadata !198, metadata !796}
!803 = metadata !{i32 648, i32 0, metadata !198, metadata !796}
!804 = metadata !{i32 649, i32 0, metadata !198, metadata !796}
!805 = metadata !{i32 650, i32 0, metadata !198, metadata !796}
!806 = metadata !{i32 652, i32 0, metadata !198, metadata !796}
!807 = metadata !{i32 917, i32 0, metadata !657, metadata !773}
!808 = metadata !{i32 918, i32 0, metadata !657, metadata !773}
!809 = metadata !{i32 920, i32 0, metadata !204, metadata !773}
!810 = metadata !{i32 921, i32 0, metadata !204, metadata !773}
!811 = metadata !{i32 922, i32 0, metadata !204, metadata !773}
!812 = metadata !{i32 925, i32 0, metadata !204, metadata !773}
!813 = metadata !{i32 2085, i32 0, metadata !814, null}
!814 = metadata !{i32 786443, metadata !1, metadata !774, i32 2083, i32 0, i32 21} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!815 = metadata !{i32 2086, i32 0, metadata !814, null}
!816 = metadata !{i32 2087, i32 0, metadata !774, null}
!817 = metadata !{i32 2089, i32 0, metadata !772, null}
!818 = metadata !{i32 2101, i32 0, metadata !144, null}
!819 = metadata !{i32 2103, i32 0, metadata !144, null}
!820 = metadata !{i32 2106, i32 0, metadata !144, null}
!821 = metadata !{i32 2107, i32 0, metadata !144, null}
!822 = metadata !{i32 786689, metadata !147, metadata !"Self", metadata !22, i32 16779363, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2147]
!823 = metadata !{i32 2147, i32 0, metadata !147, null}
!824 = metadata !{i32 786689, metadata !147, metadata !"Addr", metadata !22, i32 33556579, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 2147]
!825 = metadata !{i32 786689, metadata !147, metadata !"Valu", metadata !22, i32 50333795, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 2147]
!826 = metadata !{i32 2151, i32 0, metadata !147, null}
!827 = metadata !{i32 786689, metadata !195, metadata !"k", metadata !22, i32 16778082, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 866]
!828 = metadata !{i32 866, i32 0, metadata !195, metadata !826}
!829 = metadata !{i32 786689, metadata !195, metadata !"Addr", metadata !22, i32 33555298, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 866]
!830 = metadata !{i32 786689, metadata !195, metadata !"Valu", metadata !22, i32 50332514, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 866]
!831 = metadata !{i32 786688, metadata !195, metadata !"e", metadata !22, i32 868, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 868]
!832 = metadata !{i32 868, i32 0, metadata !195, metadata !826}
!833 = metadata !{i32 869, i32 0, metadata !195, metadata !826}
!834 = metadata !{i32 870, i32 0, metadata !835, metadata !826}
!835 = metadata !{i32 786443, metadata !1, metadata !195, i32 869, i32 0, i32 57} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!836 = metadata !{i32 871, i32 13, metadata !835, metadata !826}
!837 = metadata !{i32 641, i32 0, metadata !198, metadata !836}
!838 = metadata !{i32 643, i32 0, metadata !198, metadata !836}
!839 = metadata !{i32 644, i32 0, metadata !198, metadata !836}
!840 = metadata !{i32 645, i32 0, metadata !198, metadata !836}
!841 = metadata !{i32 646, i32 0, metadata !198, metadata !836}
!842 = metadata !{i32 647, i32 0, metadata !198, metadata !836}
!843 = metadata !{i32 648, i32 0, metadata !198, metadata !836}
!844 = metadata !{i32 649, i32 0, metadata !198, metadata !836}
!845 = metadata !{i32 650, i32 0, metadata !198, metadata !836}
!846 = metadata !{i32 652, i32 0, metadata !198, metadata !836}
!847 = metadata !{i32 872, i32 0, metadata !835, metadata !826}
!848 = metadata !{i32 873, i32 0, metadata !835, metadata !826}
!849 = metadata !{i32 874, i32 0, metadata !195, metadata !826}
!850 = metadata !{i32 875, i32 0, metadata !195, metadata !826}
!851 = metadata !{i32 876, i32 0, metadata !195, metadata !826}
!852 = metadata !{i32 877, i32 0, metadata !195, metadata !826}
!853 = metadata !{i32 878, i32 0, metadata !195, metadata !826}
!854 = metadata !{i32 2152, i32 0, metadata !147, null}
!855 = metadata !{i32 2155, i32 0, metadata !147, null}
!856 = metadata !{i32 786689, metadata !148, metadata !"Self", metadata !22, i32 16779379, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2163]
!857 = metadata !{i32 2163, i32 0, metadata !148, null}
!858 = metadata !{i32 786689, metadata !148, metadata !"envPtr", metadata !22, i32 33556595, metadata !97, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [envPtr] [line 2163]
!859 = metadata !{i32 786689, metadata !148, metadata !"ROFlag", metadata !22, i32 50333811, metadata !69, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ROFlag] [line 2163]
!860 = metadata !{i32 2168, i32 0, metadata !148, null}
!861 = metadata !{i32 786689, metadata !194, metadata !"Self", metadata !22, i32 16778382, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1166]
!862 = metadata !{i32 1166, i32 0, metadata !194, metadata !860}
!863 = metadata !{i32 1168, i32 0, metadata !194, metadata !860}
!864 = metadata !{i32 1183, i32 0, metadata !194, metadata !860}
!865 = metadata !{i32 1184, i32 0, metadata !194, metadata !860}
!866 = metadata !{i32 1187, i32 0, metadata !194, metadata !860}
!867 = metadata !{i32 1188, i32 0, metadata !194, metadata !860}
!868 = metadata !{i32 1189, i32 0, metadata !194, metadata !860}
!869 = metadata !{i32 1191, i32 0, metadata !194, metadata !860}
!870 = metadata !{i32 1192, i32 0, metadata !194, metadata !860}
!871 = metadata !{i32 1193, i32 0, metadata !194, metadata !860}
!872 = metadata !{i32 2170, i32 16, metadata !148, null}
!873 = metadata !{i32 786689, metadata !191, metadata !"Self", metadata !22, i32 16777529, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 313]
!874 = metadata !{i32 313, i32 0, metadata !191, metadata !872}
!875 = metadata !{i32 316, i32 0, metadata !191, metadata !872}
!876 = metadata !{i32 2174, i32 0, metadata !148, null}
!877 = metadata !{i32 2175, i32 0, metadata !148, null}
!878 = metadata !{i32 2176, i32 0, metadata !148, null}
!879 = metadata !{i32 2177, i32 0, metadata !148, null}
!880 = metadata !{i32 2182, i32 0, metadata !148, null}
!881 = metadata !{i32 2187, i32 0, metadata !148, null}
!882 = metadata !{i32 786689, metadata !151, metadata !"Self", metadata !22, i32 16779411, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2195]
!883 = metadata !{i32 2195, i32 0, metadata !151, null}
!884 = metadata !{i32 2205, i32 0, metadata !151, null}
!885 = metadata !{i32 2209, i32 0, metadata !886, null}
!886 = metadata !{i32 786443, metadata !1, metadata !151, i32 2207, i32 0, i32 22} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!887 = metadata !{i32 786689, metadata !190, metadata !"Self", metadata !22, i32 16778422, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1206]
!888 = metadata !{i32 1206, i32 0, metadata !190, metadata !885}
!889 = metadata !{i32 1208, i32 0, metadata !190, metadata !885}
!890 = metadata !{i32 1166, i32 0, metadata !194, metadata !889}
!891 = metadata !{i32 1168, i32 0, metadata !194, metadata !889}
!892 = metadata !{i32 1183, i32 0, metadata !194, metadata !889}
!893 = metadata !{i32 1184, i32 0, metadata !194, metadata !889}
!894 = metadata !{i32 1187, i32 0, metadata !194, metadata !889}
!895 = metadata !{i32 1188, i32 0, metadata !194, metadata !889}
!896 = metadata !{i32 1189, i32 0, metadata !194, metadata !889}
!897 = metadata !{i32 1191, i32 0, metadata !194, metadata !889}
!898 = metadata !{i32 1192, i32 0, metadata !194, metadata !889}
!899 = metadata !{i32 1193, i32 0, metadata !194, metadata !889}
!900 = metadata !{i32 1209, i32 0, metadata !190, metadata !885}
!901 = metadata !{i32 2210, i32 0, metadata !886, null}
!902 = metadata !{i32 2211, i32 0, metadata !886, null}
!903 = metadata !{i32 2223, i32 0, metadata !886, null}
!904 = metadata !{i32 2226, i32 9, metadata !151, null}
!905 = metadata !{i32 786689, metadata !161, metadata !"Self", metadata !22, i32 16778673, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1457]
!906 = metadata !{i32 1457, i32 0, metadata !161, metadata !904}
!907 = metadata !{i32 786688, metadata !161, metadata !"wr", metadata !22, i32 1470, metadata !663, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1470]
!908 = metadata !{i32 1470, i32 0, metadata !161, metadata !904}
!909 = metadata !{i32 786688, metadata !161, metadata !"rd", metadata !22, i32 1472, metadata !663, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1472]
!910 = metadata !{i32 1472, i32 0, metadata !161, metadata !904}
!911 = metadata !{i32 786688, metadata !161, metadata !"ctr", metadata !22, i32 1473, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ctr] [line 1473]
!912 = metadata !{i32 1473, i32 0, metadata !161, metadata !904}
!913 = metadata !{i32 786688, metadata !161, metadata !"wv", metadata !22, i32 1475, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wv] [line 1475]
!914 = metadata !{i32 1475, i32 0, metadata !161, metadata !904}
!915 = metadata !{i32 1517, i32 0, metadata !161, metadata !904}
!916 = metadata !{i32 1518, i32 0, metadata !161, metadata !904}
!917 = metadata !{i32 786688, metadata !161, metadata !"maxv", metadata !22, i32 1519, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [maxv] [line 1519]
!918 = metadata !{i32 1519, i32 0, metadata !161, metadata !904}
!919 = metadata !{i32 786688, metadata !161, metadata !"p", metadata !22, i32 1520, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1520]
!920 = metadata !{i32 1520, i32 0, metadata !161, metadata !904}
!921 = metadata !{i32 786688, metadata !922, metadata !"End", metadata !22, i32 1525, metadata !542, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1525]
!922 = metadata !{i32 786443, metadata !1, metadata !161, i32 1524, i32 0, i32 25} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!923 = metadata !{i32 1525, i32 0, metadata !922, metadata !904}
!924 = metadata !{i32 1526, i32 0, metadata !925, metadata !904}
!925 = metadata !{i32 786443, metadata !1, metadata !922, i32 1526, i32 0, i32 26} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!926 = metadata !{i32 786688, metadata !927, metadata !"LockFor", metadata !22, i32 1527, metadata !928, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 1527]
!927 = metadata !{i32 786443, metadata !1, metadata !925, i32 1526, i32 0, i32 27} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!928 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !25} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!929 = metadata !{i32 1527, i32 0, metadata !927, metadata !904}
!930 = metadata !{i32 786688, metadata !927, metadata !"cv", metadata !22, i32 1528, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [cv] [line 1528]
!931 = metadata !{i32 1528, i32 0, metadata !927, metadata !904}
!932 = metadata !{i32 1534, i32 0, metadata !927, metadata !904}
!933 = metadata !{i32 786689, metadata !182, metadata !"x", metadata !176, i32 16777301, metadata !185, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [x] [line 85]
!934 = metadata !{i32 85, i32 0, metadata !182, metadata !932}
!935 = metadata !{i32 1535, i32 0, metadata !927, metadata !904}
!936 = metadata !{i32 1536, i32 0, metadata !927, metadata !904}
!937 = metadata !{i32 1538, i32 21, metadata !938, metadata !904}
!938 = metadata !{i32 786443, metadata !1, metadata !927, i32 1536, i32 0, i32 28} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!939 = metadata !{i32 786689, metadata !179, metadata !"k", metadata !22, i32 16778004, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 788]
!940 = metadata !{i32 788, i32 0, metadata !179, metadata !937}
!941 = metadata !{i32 786689, metadata !179, metadata !"Lock", metadata !22, i32 33555220, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Lock] [line 788]
!942 = metadata !{i32 786688, metadata !943, metadata !"e", metadata !22, i32 790, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 790]
!943 = metadata !{i32 786443, metadata !1, metadata !179} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!944 = metadata !{i32 790, i32 0, metadata !943, metadata !937}
!945 = metadata !{i32 786688, metadata !943, metadata !"End", metadata !22, i32 791, metadata !542, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 791]
!946 = metadata !{i32 791, i32 0, metadata !943, metadata !937}
!947 = metadata !{i32 792, i32 0, metadata !948, metadata !937}
!948 = metadata !{i32 786443, metadata !1, metadata !943, i32 792, i32 0, i32 53} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!949 = metadata !{i32 793, i32 0, metadata !950, metadata !937}
!950 = metadata !{i32 786443, metadata !1, metadata !948, i32 792, i32 0, i32 54} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!951 = metadata !{i32 794, i32 0, metadata !952, metadata !937}
!952 = metadata !{i32 786443, metadata !1, metadata !950, i32 793, i32 0, i32 55} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!953 = metadata !{i32 797, i32 0, metadata !943, metadata !937}
!954 = metadata !{i32 798, i32 0, metadata !943, metadata !937}
!955 = metadata !{i32 1539, i32 0, metadata !956, metadata !904}
!956 = metadata !{i32 786443, metadata !1, metadata !938, i32 1538, i32 0, i32 29} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!957 = metadata !{i32 1540, i32 0, metadata !958, metadata !904}
!958 = metadata !{i32 786443, metadata !1, metadata !956, i32 1539, i32 0, i32 30} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!959 = metadata !{i32 1541, i32 0, metadata !958, metadata !904}
!960 = metadata !{i32 1543, i32 0, metadata !956, metadata !904}
!961 = metadata !{i32 1545, i32 0, metadata !938, metadata !904}
!962 = metadata !{i32 1549, i32 17, metadata !927, metadata !904}
!963 = metadata !{i32 788, i32 0, metadata !179, metadata !962}
!964 = metadata !{i32 790, i32 0, metadata !943, metadata !962}
!965 = metadata !{i32 791, i32 0, metadata !943, metadata !962}
!966 = metadata !{i32 792, i32 0, metadata !948, metadata !962}
!967 = metadata !{i32 793, i32 0, metadata !950, metadata !962}
!968 = metadata !{i32 794, i32 0, metadata !952, metadata !962}
!969 = metadata !{i32 797, i32 0, metadata !943, metadata !962}
!970 = metadata !{i32 798, i32 0, metadata !943, metadata !962}
!971 = metadata !{i32 1553, i32 0, metadata !972, metadata !904}
!972 = metadata !{i32 786443, metadata !1, metadata !927, i32 1549, i32 0, i32 31} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!973 = metadata !{i32 1555, i32 21, metadata !972, metadata !904}
!974 = metadata !{i32 42, i32 0, metadata !174, metadata !973}
!975 = metadata !{i32 44, i32 0, metadata !392, metadata !973}
!976 = metadata !{i32 46, i32 0, metadata !392, metadata !973}
!977 = metadata !{i32 58, i32 0, metadata !392, metadata !973} ; [ DW_TAG_imported_module ]
!978 = metadata !{i32 1557, i32 0, metadata !979, metadata !904}
!979 = metadata !{i32 786443, metadata !1, metadata !972, i32 1556, i32 0, i32 32} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!980 = metadata !{i32 1558, i32 0, metadata !981, metadata !904}
!981 = metadata !{i32 786443, metadata !1, metadata !979, i32 1557, i32 0, i32 33} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!982 = metadata !{i32 1559, i32 0, metadata !981, metadata !904}
!983 = metadata !{i32 1560, i32 0, metadata !979, metadata !904}
!984 = metadata !{i32 1561, i32 0, metadata !979, metadata !904}
!985 = metadata !{i32 1562, i32 0, metadata !979, metadata !904}
!986 = metadata !{i32 1571, i32 0, metadata !972, metadata !904}
!987 = metadata !{i32 1572, i32 0, metadata !972, metadata !904}
!988 = metadata !{i32 786688, metadata !989, metadata !"c", metadata !22, i32 1590, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [c] [line 1590]
!989 = metadata !{i32 786443, metadata !1, metadata !927, i32 1574, i32 0, i32 34} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!990 = metadata !{i32 1590, i32 0, metadata !989, metadata !904}
!991 = metadata !{i32 1592, i32 0, metadata !992, metadata !904}
!992 = metadata !{i32 786443, metadata !1, metadata !989, i32 1592, i32 0, i32 35} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!993 = metadata !{i32 1593, i32 0, metadata !994, metadata !904}
!994 = metadata !{i32 786443, metadata !1, metadata !992, i32 1592, i32 0, i32 36} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!995 = metadata !{i32 1595, i32 0, metadata !994, metadata !904}
!996 = metadata !{i32 1596, i32 25, metadata !994, metadata !904}
!997 = metadata !{i32 42, i32 0, metadata !174, metadata !996}
!998 = metadata !{i32 44, i32 0, metadata !392, metadata !996}
!999 = metadata !{i32 46, i32 0, metadata !392, metadata !996}
!1000 = metadata !{i32 58, i32 0, metadata !392, metadata !996} ; [ DW_TAG_imported_module ]
!1001 = metadata !{i32 1598, i32 0, metadata !1002, metadata !904}
!1002 = metadata !{i32 786443, metadata !1, metadata !994, i32 1597, i32 0, i32 37} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1003 = metadata !{i32 1599, i32 0, metadata !1004, metadata !904}
!1004 = metadata !{i32 786443, metadata !1, metadata !1002, i32 1598, i32 0, i32 38} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1005 = metadata !{i32 1600, i32 0, metadata !1004, metadata !904}
!1006 = metadata !{i32 1601, i32 0, metadata !1002, metadata !904}
!1007 = metadata !{i32 1602, i32 0, metadata !1002, metadata !904}
!1008 = metadata !{i32 1616, i32 0, metadata !927, metadata !904}
!1009 = metadata !{i32 1605, i32 0, metadata !994, metadata !904}
!1010 = metadata !{i32 1607, i32 0, metadata !1011, metadata !904}
!1011 = metadata !{i32 786443, metadata !1, metadata !994, i32 1605, i32 0, i32 39} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1012 = metadata !{i32 1614, i32 0, metadata !994, metadata !904}
!1013 = metadata !{i32 1625, i32 10, metadata !161, metadata !904}
!1014 = metadata !{i32 786689, metadata !171, metadata !"Self", metadata !22, i32 16777597, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 381]
!1015 = metadata !{i32 381, i32 0, metadata !171, metadata !1013}
!1016 = metadata !{i32 786689, metadata !171, metadata !"maxv", metadata !22, i32 33554813, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [maxv] [line 381]
!1017 = metadata !{i32 786688, metadata !171, metadata !"gv", metadata !22, i32 383, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [gv] [line 383]
!1018 = metadata !{i32 383, i32 0, metadata !171, metadata !1013}
!1019 = metadata !{i32 786688, metadata !171, metadata !"wv", metadata !22, i32 384, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wv] [line 384]
!1020 = metadata !{i32 384, i32 0, metadata !171, metadata !1013}
!1021 = metadata !{i32 786688, metadata !171, metadata !"k", metadata !22, i32 385, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [k] [line 385]
!1022 = metadata !{i32 385, i32 0, metadata !171, metadata !1013}
!1023 = metadata !{i32 385, i32 16, metadata !171, metadata !1013}
!1024 = metadata !{i32 42, i32 0, metadata !174, metadata !1023}
!1025 = metadata !{i32 44, i32 0, metadata !392, metadata !1023}
!1026 = metadata !{i32 46, i32 0, metadata !392, metadata !1023}
!1027 = metadata !{i32 58, i32 0, metadata !392, metadata !1023} ; [ DW_TAG_imported_module ]
!1028 = metadata !{i32 386, i32 0, metadata !171, metadata !1013}
!1029 = metadata !{i32 387, i32 0, metadata !1030, metadata !1013}
!1030 = metadata !{i32 786443, metadata !1, metadata !171, i32 386, i32 0, i32 52} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1031 = metadata !{i32 388, i32 0, metadata !1030, metadata !1013}
!1032 = metadata !{i32 394, i32 0, metadata !171, metadata !1013}
!1033 = metadata !{i32 395, i32 0, metadata !171, metadata !1013}
!1034 = metadata !{i32 1646, i32 10, metadata !161, metadata !904}
!1035 = metadata !{i32 786689, metadata !170, metadata !"Self", metadata !22, i32 16778470, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1254]
!1036 = metadata !{i32 1254, i32 0, metadata !170, metadata !1034}
!1037 = metadata !{i32 786688, metadata !170, metadata !"dx", metadata !22, i32 1256, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dx] [line 1256]
!1038 = metadata !{i32 1256, i32 0, metadata !170, metadata !1034}
!1039 = metadata !{i32 786688, metadata !170, metadata !"rv", metadata !22, i32 1257, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rv] [line 1257]
!1040 = metadata !{i32 1257, i32 0, metadata !170, metadata !1034}
!1041 = metadata !{i32 786688, metadata !170, metadata !"rd", metadata !22, i32 1258, metadata !663, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1258]
!1042 = metadata !{i32 1258, i32 0, metadata !170, metadata !1034}
!1043 = metadata !{i32 786688, metadata !170, metadata !"EndOfList", metadata !22, i32 1259, metadata !542, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [EndOfList] [line 1259]
!1044 = metadata !{i32 1259, i32 0, metadata !170, metadata !1034}
!1045 = metadata !{i32 786688, metadata !170, metadata !"e", metadata !22, i32 1260, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1260]
!1046 = metadata !{i32 1260, i32 0, metadata !170, metadata !1034}
!1047 = metadata !{i32 1264, i32 0, metadata !1048, metadata !1034}
!1048 = metadata !{i32 786443, metadata !1, metadata !170, i32 1264, i32 0, i32 48} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1049 = metadata !{i32 786688, metadata !1050, metadata !"v", metadata !22, i32 1266, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 1266]
!1050 = metadata !{i32 786443, metadata !1, metadata !1048, i32 1264, i32 0, i32 49} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1051 = metadata !{i32 1266, i32 0, metadata !1050, metadata !1034}
!1052 = metadata !{i32 1267, i32 0, metadata !1050, metadata !1034}
!1053 = metadata !{i32 1278, i32 0, metadata !1054, metadata !1034}
!1054 = metadata !{i32 786443, metadata !1, metadata !1050, i32 1267, i32 0, i32 50} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1055 = metadata !{i32 1280, i32 0, metadata !1054, metadata !1034}
!1056 = metadata !{i32 1281, i32 0, metadata !1057, metadata !1034}
!1057 = metadata !{i32 786443, metadata !1, metadata !1050, i32 1280, i32 0, i32 51} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1058 = metadata !{i32 1285, i32 0, metadata !170, metadata !1034}
!1059 = metadata !{i32 1653, i32 0, metadata !1060, metadata !904}
!1060 = metadata !{i32 786443, metadata !1, metadata !161, i32 1646, i32 0, i32 40} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1061 = metadata !{i32 1665, i32 0, metadata !1062, metadata !904}
!1062 = metadata !{i32 786443, metadata !1, metadata !161, i32 1664, i32 0, i32 41} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1063 = metadata !{i32 786689, metadata !167, metadata !"k", metadata !22, i32 16777971, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 755]
!1064 = metadata !{i32 755, i32 0, metadata !167, metadata !1061}
!1065 = metadata !{i32 786688, metadata !167, metadata !"e", metadata !22, i32 757, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 757]
!1066 = metadata !{i32 757, i32 0, metadata !167, metadata !1061}
!1067 = metadata !{i32 786688, metadata !167, metadata !"End", metadata !22, i32 758, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 758]
!1068 = metadata !{i32 758, i32 0, metadata !167, metadata !1061}
!1069 = metadata !{i32 759, i32 0, metadata !1070, metadata !1061}
!1070 = metadata !{i32 786443, metadata !1, metadata !167, i32 759, i32 0, i32 46} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1071 = metadata !{i32 760, i32 0, metadata !1072, metadata !1061}
!1072 = metadata !{i32 786443, metadata !1, metadata !1070, i32 759, i32 0, i32 47} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1073 = metadata !{i32 1669, i32 0, metadata !161, metadata !904}
!1074 = metadata !{i32 786689, metadata !164, metadata !"Self", metadata !22, i32 16778597, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1381]
!1075 = metadata !{i32 1381, i32 0, metadata !164, metadata !1073}
!1076 = metadata !{i32 786689, metadata !164, metadata !"wv", metadata !22, i32 33555813, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [wv] [line 1381]
!1077 = metadata !{i32 786688, metadata !164, metadata !"wr", metadata !22, i32 1391, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1391]
!1078 = metadata !{i32 1391, i32 0, metadata !164, metadata !1073}
!1079 = metadata !{i32 786688, metadata !1080, metadata !"p", metadata !22, i32 1394, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1394]
!1080 = metadata !{i32 786443, metadata !1, metadata !164, i32 1393, i32 0, i32 42} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1081 = metadata !{i32 1394, i32 0, metadata !1080, metadata !1073}
!1082 = metadata !{i32 786688, metadata !1080, metadata !"End", metadata !22, i32 1395, metadata !542, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1395]
!1083 = metadata !{i32 1395, i32 0, metadata !1080, metadata !1073}
!1084 = metadata !{i32 1396, i32 0, metadata !1085, metadata !1073}
!1085 = metadata !{i32 786443, metadata !1, metadata !1080, i32 1396, i32 0, i32 43} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1086 = metadata !{i32 1400, i32 0, metadata !1087, metadata !1073}
!1087 = metadata !{i32 786443, metadata !1, metadata !1085, i32 1396, i32 0, i32 44} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1088 = metadata !{i32 1401, i32 0, metadata !1089, metadata !1073}
!1089 = metadata !{i32 786443, metadata !1, metadata !1087, i32 1400, i32 0, i32 45} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1090 = metadata !{i32 1403, i32 0, metadata !1087, metadata !1073}
!1091 = metadata !{i32 1413, i32 0, metadata !1087, metadata !1073}
!1092 = metadata !{i32 1414, i32 0, metadata !1087, metadata !1073}
!1093 = metadata !{i32 1417, i32 0, metadata !164, metadata !1073}
!1094 = metadata !{i32 1677, i32 0, metadata !161, metadata !904}
!1095 = metadata !{i32 -2146795103}
!1096 = metadata !{i32 1679, i32 0, metadata !161, metadata !904}
!1097 = metadata !{i32 2227, i32 0, metadata !1098, null}
!1098 = metadata !{i32 786443, metadata !1, metadata !151, i32 2226, i32 0, i32 23} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1099 = metadata !{i32 1206, i32 0, metadata !190, metadata !1097}
!1100 = metadata !{i32 1208, i32 0, metadata !190, metadata !1097}
!1101 = metadata !{i32 1166, i32 0, metadata !194, metadata !1100}
!1102 = metadata !{i32 1168, i32 0, metadata !194, metadata !1100}
!1103 = metadata !{i32 1183, i32 0, metadata !194, metadata !1100}
!1104 = metadata !{i32 1184, i32 0, metadata !194, metadata !1100}
!1105 = metadata !{i32 1187, i32 0, metadata !194, metadata !1100}
!1106 = metadata !{i32 1188, i32 0, metadata !194, metadata !1100}
!1107 = metadata !{i32 1189, i32 0, metadata !194, metadata !1100}
!1108 = metadata !{i32 1191, i32 0, metadata !194, metadata !1100}
!1109 = metadata !{i32 1192, i32 0, metadata !194, metadata !1100}
!1110 = metadata !{i32 1193, i32 0, metadata !194, metadata !1100}
!1111 = metadata !{i32 1209, i32 0, metadata !190, metadata !1097}
!1112 = metadata !{i32 2228, i32 0, metadata !1098, null}
!1113 = metadata !{i32 2229, i32 0, metadata !1098, null}
!1114 = metadata !{i32 2243, i32 0, metadata !1098, null}
!1115 = metadata !{i32 2247, i32 0, metadata !151, null}
!1116 = metadata !{i32 2250, i32 0, metadata !151, null}
!1117 = metadata !{i32 786689, metadata !187, metadata !"Base", metadata !22, i32 16779336, metadata !81, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Base] [line 2120]
!1118 = metadata !{i32 2120, i32 0, metadata !187, null}
!1119 = metadata !{i32 786689, metadata !187, metadata !"Length", metadata !22, i32 33556552, metadata !157, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Length] [line 2120]
!1120 = metadata !{i32 786688, metadata !1121, metadata !"Addr", metadata !22, i32 2124, metadata !1122, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Addr] [line 2124]
!1121 = metadata !{i32 786443, metadata !1, metadata !187} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1122 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !32} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from intptr_t]
!1123 = metadata !{i32 2124, i32 0, metadata !1121, null}
!1124 = metadata !{i32 786688, metadata !1121, metadata !"End", metadata !22, i32 2125, metadata !1122, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 2125]
!1125 = metadata !{i32 2125, i32 0, metadata !1121, null}
!1126 = metadata !{i32 2127, i32 0, metadata !1121, null}
!1127 = metadata !{i32 786688, metadata !1128, metadata !"Lock", metadata !22, i32 2128, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Lock] [line 2128]
!1128 = metadata !{i32 786443, metadata !1, metadata !1121, i32 2127, i32 0, i32 56} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1129 = metadata !{i32 2128, i32 0, metadata !1128, null}
!1130 = metadata !{i32 786688, metadata !1128, metadata !"val", metadata !22, i32 2129, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [val] [line 2129]
!1131 = metadata !{i32 2129, i32 0, metadata !1128, null}
!1132 = metadata !{i32 2131, i32 0, metadata !1128, null}
!1133 = metadata !{i32 42, i32 0, metadata !174, metadata !1132}
!1134 = metadata !{i32 44, i32 0, metadata !392, metadata !1132}
!1135 = metadata !{i32 46, i32 0, metadata !392, metadata !1132}
!1136 = metadata !{i32 58, i32 0, metadata !392, metadata !1132} ; [ DW_TAG_imported_module ]
!1137 = metadata !{i32 2132, i32 0, metadata !1128, null}
!1138 = metadata !{i32 2133, i32 0, metadata !1128, null}
!1139 = metadata !{i32 2134, i32 0, metadata !1121, null}
!1140 = metadata !{i32 2137, i32 0, metadata !1121, null}
!1141 = metadata !{i32 786689, metadata !154, metadata !"Self", metadata !22, i32 16779481, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2265]
!1142 = metadata !{i32 2265, i32 0, metadata !154, null}
!1143 = metadata !{i32 786689, metadata !154, metadata !"size", metadata !22, i32 33556697, metadata !157, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [size] [line 2265]
!1144 = metadata !{i32 786688, metadata !154, metadata !"ptr", metadata !22, i32 2267, metadata !81, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ptr] [line 2267]
!1145 = metadata !{i32 2267, i32 0, metadata !154, null}
!1146 = metadata !{i32 2268, i32 0, metadata !154, null}
!1147 = metadata !{i32 2269, i32 0, metadata !1148, null}
!1148 = metadata !{i32 786443, metadata !1, metadata !154, i32 2268, i32 0, i32 24} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1149 = metadata !{i32 2270, i32 0, metadata !1148, null}
!1150 = metadata !{i32 2272, i32 0, metadata !154, null}
!1151 = metadata !{i32 786689, metadata !158, metadata !"Self", metadata !22, i32 16779499, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2283]
!1152 = metadata !{i32 2283, i32 0, metadata !158, null}
!1153 = metadata !{i32 786689, metadata !158, metadata !"ptr", metadata !22, i32 33556715, metadata !81, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ptr] [line 2283]
!1154 = metadata !{i32 2285, i32 0, metadata !158, null}
!1155 = metadata !{i32 786688, metadata !158, metadata !"LockFor", metadata !22, i32 2297, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 2297]
!1156 = metadata !{i32 2297, i32 0, metadata !158, null}
!1157 = metadata !{i32 786688, metadata !158, metadata !"wr", metadata !22, i32 2304, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 2304]
!1158 = metadata !{i32 2304, i32 0, metadata !158, null}
!1159 = metadata !{i32 2306, i32 0, metadata !158, null}
!1160 = metadata !{i32 832, i32 0, metadata !201, metadata !1159}
!1161 = metadata !{i32 843, i32 0, metadata !201, metadata !1159}
!1162 = metadata !{i32 844, i32 0, metadata !201, metadata !1159}
!1163 = metadata !{i32 845, i32 0, metadata !723, metadata !1159}
!1164 = metadata !{i32 846, i32 13, metadata !723, metadata !1159}
!1165 = metadata !{i32 641, i32 0, metadata !198, metadata !1164}
!1166 = metadata !{i32 643, i32 0, metadata !198, metadata !1164}
!1167 = metadata !{i32 644, i32 0, metadata !198, metadata !1164}
!1168 = metadata !{i32 645, i32 0, metadata !198, metadata !1164}
!1169 = metadata !{i32 646, i32 0, metadata !198, metadata !1164}
!1170 = metadata !{i32 647, i32 0, metadata !198, metadata !1164}
!1171 = metadata !{i32 648, i32 0, metadata !198, metadata !1164}
!1172 = metadata !{i32 649, i32 0, metadata !198, metadata !1164}
!1173 = metadata !{i32 650, i32 0, metadata !198, metadata !1164}
!1174 = metadata !{i32 652, i32 0, metadata !198, metadata !1164}
!1175 = metadata !{i32 847, i32 0, metadata !723, metadata !1159}
!1176 = metadata !{i32 848, i32 0, metadata !723, metadata !1159}
!1177 = metadata !{i32 850, i32 0, metadata !201, metadata !1159}
!1178 = metadata !{i32 851, i32 0, metadata !201, metadata !1159}
!1179 = metadata !{i32 852, i32 0, metadata !201, metadata !1159}
!1180 = metadata !{i32 853, i32 0, metadata !201, metadata !1159}
!1181 = metadata !{i32 854, i32 0, metadata !201, metadata !1159}
!1182 = metadata !{i32 855, i32 0, metadata !201, metadata !1159}
!1183 = metadata !{i32 856, i32 0, metadata !201, metadata !1159}
!1184 = metadata !{i32 2308, i32 0, metadata !158, null}
!1185 = metadata !{i32 995, i32 0, metadata !228, null}
!1186 = metadata !{i32 996, i32 0, metadata !1187, null}
!1187 = metadata !{i32 786443, metadata !1, metadata !228, i32 995, i32 0, i32 79} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1188 = metadata !{i32 997, i32 0, metadata !1187, null}
!1189 = metadata !{i32 1000, i32 0, metadata !228, null}
!1190 = metadata !{i32 1001, i32 0, metadata !1191, null}
!1191 = metadata !{i32 786443, metadata !1, metadata !228, i32 1000, i32 0, i32 80} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1192 = metadata !{i32 1002, i32 0, metadata !1191, null}
!1193 = metadata !{i32 1004, i32 0, metadata !228, null}
!1194 = metadata !{i32 786688, metadata !229, metadata !"act", metadata !22, i32 969, metadata !319, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [act] [line 969]
!1195 = metadata !{i32 969, i32 0, metadata !229, null}
!1196 = metadata !{i32 971, i32 0, metadata !229, null}
!1197 = metadata !{i32 972, i32 0, metadata !229, null}
!1198 = metadata !{i32 973, i32 0, metadata !229, null}
!1199 = metadata !{i32 974, i32 0, metadata !229, null}
!1200 = metadata !{i32 976, i32 0, metadata !229, null}
!1201 = metadata !{i32 977, i32 0, metadata !1202, null}
!1202 = metadata !{i32 786443, metadata !1, metadata !229, i32 976, i32 0, i32 81} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1203 = metadata !{i32 978, i32 0, metadata !1202, null}
!1204 = metadata !{i32 981, i32 0, metadata !229, null}
!1205 = metadata !{i32 982, i32 0, metadata !1206, null}
!1206 = metadata !{i32 786443, metadata !1, metadata !229, i32 981, i32 0, i32 82} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1207 = metadata !{i32 983, i32 0, metadata !1206, null}
!1208 = metadata !{i32 985, i32 0, metadata !229, null}
!1209 = metadata !{i32 786689, metadata !230, metadata !"signum", metadata !22, i32 16778158, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [signum] [line 942]
!1210 = metadata !{i32 942, i32 0, metadata !230, null}
!1211 = metadata !{i32 786689, metadata !230, metadata !"siginfo", metadata !22, i32 33555374, metadata !233, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [siginfo] [line 942]
!1212 = metadata !{i32 786689, metadata !230, metadata !"context", metadata !22, i32 50332590, metadata !81, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [context] [line 942]
!1213 = metadata !{i32 786688, metadata !230, metadata !"Self", metadata !22, i32 944, metadata !132, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Self] [line 944]
!1214 = metadata !{i32 944, i32 0, metadata !230, null}
!1215 = metadata !{i32 946, i32 0, metadata !230, null}
!1216 = metadata !{i32 947, i32 0, metadata !1217, null}
!1217 = metadata !{i32 786443, metadata !1, metadata !230, i32 946, i32 0, i32 83} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1218 = metadata !{i32 948, i32 0, metadata !1217, null}
!1219 = metadata !{i32 951, i32 0, metadata !230, null}
!1220 = metadata !{i32 952, i32 14, metadata !1221, null}
!1221 = metadata !{i32 786443, metadata !1, metadata !230, i32 951, i32 0, i32 84} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1222 = metadata !{i32 1296, i32 0, metadata !207, metadata !1220}
!1223 = metadata !{i32 1298, i32 0, metadata !207, metadata !1220}
!1224 = metadata !{i32 1299, i32 0, metadata !207, metadata !1220}
!1225 = metadata !{i32 1300, i32 0, metadata !207, metadata !1220}
!1226 = metadata !{i32 1301, i32 0, metadata !207, metadata !1220}
!1227 = metadata !{i32 1305, i32 0, metadata !670, metadata !1220}
!1228 = metadata !{i32 1307, i32 0, metadata !672, metadata !1220}
!1229 = metadata !{i32 1308, i32 0, metadata !672, metadata !1220}
!1230 = metadata !{i32 1319, i32 0, metadata !676, metadata !1220}
!1231 = metadata !{i32 1320, i32 0, metadata !678, metadata !1220}
!1232 = metadata !{i32 1323, i32 0, metadata !676, metadata !1220}
!1233 = metadata !{i32 1324, i32 0, metadata !681, metadata !1220}
!1234 = metadata !{i32 1325, i32 0, metadata !683, metadata !1220}
!1235 = metadata !{i32 1330, i32 0, metadata !207, metadata !1220}
!1236 = metadata !{i32 953, i32 0, metadata !1237, null}
!1237 = metadata !{i32 786443, metadata !1, metadata !1221, i32 952, i32 0, i32 85} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1238 = metadata !{i32 954, i32 0, metadata !1237, null}
!1239 = metadata !{i32 955, i32 0, metadata !1221, null}
!1240 = metadata !{i32 957, i32 0, metadata !230, null}
!1241 = metadata !{i32 958, i32 0, metadata !230, null}
!1242 = metadata !{i32 959, i32 0, metadata !230, null}
