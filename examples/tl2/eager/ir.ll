; ModuleID = 'tl2.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.sigaction = type { %union.anon, %struct.__sigset_t, i32, void ()* }
%union.anon = type { void (i32)* }
%struct.__sigset_t = type { [16 x i64] }
%struct._Log = type { %struct._AVPair*, %struct._AVPair*, %struct._AVPair*, %struct._AVPair*, i64, i32 }
%struct._AVPair = type { %struct._AVPair*, %struct._AVPair*, i64*, i64, i64*, i64, %struct._Thread*, i64 }
%struct._Thread = type { i64, i64, i64, i64, i64, i64, i64, i64, %struct._AVPair, i32*, i32, i64, i64, i64, [1 x i64], i8*, %struct.tmalloc*, %struct.tmalloc*, %struct._Log, %struct._Log, %struct._Log, [1 x %struct.__jmp_buf_tag]* }
%struct.tmalloc = type { i64, i64, i8** }
%struct.__jmp_buf_tag = type { [8 x i64], i32, %struct.__sigset_t }
%struct.siginfo_t = type { i32, i32, i32, %union.anon.0 }
%union.anon.0 = type { %struct.anon.3, [80 x i8] }
%struct.anon.3 = type { i32, i32, i32, i64, i64 }

@StartTally = global i64 0, align 8
@AbortTally = global i64 0, align 8
@ReadOverflowTally = global i64 0, align 8
@WriteOverflowTally = global i64 0, align 8
@LocalOverflowTally = global i64 0, align 8
@LockTab = internal global [1048576 x i64] zeroinitializer, align 16
@.str = private unnamed_addr constant [25 x i8] c"TL2 system ready: GV=%s\0A\00", align 1
@.str1 = private unnamed_addr constant [4 x i8] c"GV4\00", align 1
@global_key_self = internal global i32 0, align 4
@.str2 = private unnamed_addr constant [90 x i8] c"TL2 system shutdown:\0A  GCLOCK=0x%lX Starts=%li Aborts=%li\0A  Overflows: R=%li W=%li L=%li\0A\00", align 1
@GClock = internal global [64 x i64] zeroinitializer, align 16
@.str3 = private unnamed_addr constant [2 x i8] c"t\00", align 1
@.str4 = private unnamed_addr constant [6 x i8] c"tl2.c\00", align 1
@__PRETTY_FUNCTION__.TxNewThread = private unnamed_addr constant [22 x i8] c"Thread *TxNewThread()\00", align 1
@.str5 = private unnamed_addr constant [12 x i8] c"t->allocPtr\00", align 1
@__PRETTY_FUNCTION__.TxInitThread = private unnamed_addr constant [34 x i8] c"void TxInitThread(Thread *, long)\00", align 1
@.str6 = private unnamed_addr constant [11 x i8] c"t->freePtr\00", align 1
@.str7 = private unnamed_addr constant [2 x i8] c"e\00", align 1
@__PRETTY_FUNCTION__.ExtendList = private unnamed_addr constant [29 x i8] c"AVPair *ExtendList(AVPair *)\00", align 1
@.str8 = private unnamed_addr constant [3 x i8] c"ap\00", align 1
@__PRETTY_FUNCTION__.MakeList = private unnamed_addr constant [33 x i8] c"AVPair *MakeList(long, Thread *)\00", align 1
@global_act_oldsigbus = internal global %struct.sigaction zeroinitializer, align 8
@.str9 = private unnamed_addr constant [40 x i8] c"Error: Failed to restore SIGBUS handler\00", align 1
@global_act_oldsigsegv = internal global %struct.sigaction zeroinitializer, align 8
@.str10 = private unnamed_addr constant [41 x i8] c"Error: Failed to restore SIGSEGV handler\00", align 1
@.str11 = private unnamed_addr constant [41 x i8] c"Error: Failed to register SIGBUS handler\00", align 1
@.str12 = private unnamed_addr constant [42 x i8] c"Error: Failed to register SIGSEGV handler\00", align 1

; Function Attrs: nounwind uwtable
define i64* @pslock(i64* %Addr) #0 {
  %1 = alloca i64*, align 8
  store i64* %Addr, i64** %1, align 8
  call void @llvm.dbg.declare(metadata !{i64** %1}, metadata !326), !dbg !327
  %2 = load i64** %1, align 8, !dbg !328
  %3 = ptrtoint i64* %2 to i64, !dbg !328
  %4 = add i64 %3, 128, !dbg !328
  %5 = lshr i64 %4, 3, !dbg !328
  %6 = and i64 %5, 1048575, !dbg !328
  %7 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %6, !dbg !328
  ret i64* %7, !dbg !328
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

; Function Attrs: noinline nounwind uwtable
define void @FreeList(%struct._Log* %k, i64 %sz) #2 {
  %1 = alloca %struct._Log*, align 8
  %2 = alloca i64, align 8
  %e = alloca %struct._AVPair*, align 8
  %tmp = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !329), !dbg !330
  store i64 %sz, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !331), !dbg !330
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !332), !dbg !333
  %3 = load %struct._Log** %1, align 8, !dbg !333
  %4 = getelementptr inbounds %struct._Log* %3, i32 0, i32 3, !dbg !333
  %5 = load %struct._AVPair** %4, align 8, !dbg !333
  store %struct._AVPair* %5, %struct._AVPair** %e, align 8, !dbg !333
  %6 = load %struct._AVPair** %e, align 8, !dbg !334
  %7 = icmp ne %struct._AVPair* %6, null, !dbg !334
  br i1 %7, label %8, label %23, !dbg !334

; <label>:8                                       ; preds = %0
  br label %9, !dbg !335

; <label>:9                                       ; preds = %15, %8
  %10 = load %struct._AVPair** %e, align 8, !dbg !335
  %11 = getelementptr inbounds %struct._AVPair* %10, i32 0, i32 7, !dbg !335
  %12 = load i64* %11, align 8, !dbg !335
  %13 = load i64* %2, align 8, !dbg !335
  %14 = icmp sge i64 %12, %13, !dbg !335
  br i1 %14, label %15, label %22, !dbg !335

; <label>:15                                      ; preds = %9
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %tmp}, metadata !337), !dbg !339
  %16 = load %struct._AVPair** %e, align 8, !dbg !339
  store %struct._AVPair* %16, %struct._AVPair** %tmp, align 8, !dbg !339
  %17 = load %struct._AVPair** %e, align 8, !dbg !340
  %18 = getelementptr inbounds %struct._AVPair* %17, i32 0, i32 1, !dbg !340
  %19 = load %struct._AVPair** %18, align 8, !dbg !340
  store %struct._AVPair* %19, %struct._AVPair** %e, align 8, !dbg !340
  %20 = load %struct._AVPair** %tmp, align 8, !dbg !341
  %21 = bitcast %struct._AVPair* %20 to i8*, !dbg !341
  call void @free(i8* %21) #6, !dbg !341
  br label %9, !dbg !342

; <label>:22                                      ; preds = %9
  br label %23, !dbg !343

; <label>:23                                      ; preds = %22, %0
  %24 = load %struct._Log** %1, align 8, !dbg !344
  %25 = getelementptr inbounds %struct._Log* %24, i32 0, i32 0, !dbg !344
  %26 = load %struct._AVPair** %25, align 8, !dbg !344
  %27 = bitcast %struct._AVPair* %26 to i8*, !dbg !344
  call void @free(i8* %27) #6, !dbg !344
  ret void, !dbg !345
}

; Function Attrs: nounwind
declare void @free(i8*) #3

; Function Attrs: nounwind uwtable
define void @TxOnce() #0 {
  %a = alloca [1 x i32], align 4
  call void @llvm.dbg.declare(metadata !{[1 x i32]* %a}, metadata !346), !dbg !349
  %1 = getelementptr inbounds [1 x i32]* %a, i32 0, i64 0, !dbg !349
  store i32 0, i32* %1, align 4, !dbg !349
  store volatile i64 0, i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !350
  %2 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([25 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str1, i32 0, i32 0)), !dbg !352
  %3 = call i32 @pthread_key_create(i32* @global_key_self, void (i8*)* null) #6, !dbg !353
  call void @registerUseAfterFreeHandler(), !dbg !354
  ret void, !dbg !355
}

declare i32 @printf(i8*, ...) #4

; Function Attrs: nounwind
declare i32 @pthread_key_create(i32*, void (i8*)*) #3

; Function Attrs: nounwind uwtable
define void @TxShutdown() #0 {
  %1 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !356
  %2 = load volatile i64* @StartTally, align 8, !dbg !356
  %3 = load volatile i64* @AbortTally, align 8, !dbg !356
  %4 = load volatile i64* @ReadOverflowTally, align 8, !dbg !356
  %5 = load volatile i64* @WriteOverflowTally, align 8, !dbg !356
  %6 = load volatile i64* @LocalOverflowTally, align 8, !dbg !356
  %7 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([90 x i8]* @.str2, i32 0, i32 0), i64 %1, i64 %2, i64 %3, i64 %4, i64 %5, i64 %6), !dbg !356
  %8 = load i32* @global_key_self, align 4, !dbg !357
  %9 = call i32 @pthread_key_delete(i32 %8) #6, !dbg !357
  call void @restoreUseAfterFreeHandler(), !dbg !358
  call void asm sideeffect "mfence", "~{dirflag},~{fpsr},~{flags}"() #6, !dbg !359, !srcloc !360
  ret void, !dbg !361
}

; Function Attrs: nounwind
declare i32 @pthread_key_delete(i32) #3

; Function Attrs: nounwind uwtable
define %struct._Thread* @TxNewThread() #0 {
  %t = alloca %struct._Thread*, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %t}, metadata !362), !dbg !363
  %1 = call noalias i8* @malloc(i64 352) #6, !dbg !363
  %2 = bitcast i8* %1 to %struct._Thread*, !dbg !363
  store %struct._Thread* %2, %struct._Thread** %t, align 8, !dbg !363
  %3 = load %struct._Thread** %t, align 8, !dbg !364
  %4 = icmp ne %struct._Thread* %3, null, !dbg !364
  br i1 %4, label %5, label %6, !dbg !364

; <label>:5                                       ; preds = %0
  br label %8, !dbg !364

; <label>:6                                       ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str3, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1067, i8* getelementptr inbounds ([22 x i8]* @__PRETTY_FUNCTION__.TxNewThread, i32 0, i32 0)) #7, !dbg !364
  unreachable, !dbg !364
                                                  ; No predecessors!
  br label %8, !dbg !364

; <label>:8                                       ; preds = %7, %5
  %9 = load %struct._Thread** %t, align 8, !dbg !365
  ret %struct._Thread* %9, !dbg !365
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #3

; Function Attrs: noreturn nounwind
declare void @__assert_fail(i8*, i8*, i32, i8*) #5

; Function Attrs: nounwind uwtable
define void @TxFreeThread(%struct._Thread* %t) #0 {
  %1 = alloca i64, align 8
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %prevVal.i.i10 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %5 = alloca i64, align 8
  %v.i11 = alloca i64, align 8
  %6 = alloca i64, align 8
  %7 = alloca i64, align 8
  %8 = alloca i64*, align 8
  %prevVal.i.i7 = alloca i64, align 8
  %9 = alloca i64*, align 8
  %10 = alloca i64, align 8
  %v.i8 = alloca i64, align 8
  %11 = alloca i64, align 8
  %12 = alloca i64, align 8
  %13 = alloca i64*, align 8
  %prevVal.i.i4 = alloca i64, align 8
  %14 = alloca i64*, align 8
  %15 = alloca i64, align 8
  %v.i5 = alloca i64, align 8
  %16 = alloca i64, align 8
  %17 = alloca i64, align 8
  %18 = alloca i64*, align 8
  %prevVal.i.i1 = alloca i64, align 8
  %19 = alloca i64*, align 8
  %20 = alloca i64, align 8
  %v.i2 = alloca i64, align 8
  %21 = alloca i64, align 8
  %22 = alloca i64, align 8
  %23 = alloca i64*, align 8
  %prevVal.i.i = alloca i64, align 8
  %24 = alloca i64*, align 8
  %25 = alloca i64, align 8
  %v.i = alloca i64, align 8
  %26 = alloca %struct._Thread*, align 8
  %wrSetOvf = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  store %struct._Thread* %t, %struct._Thread** %26, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %26}, metadata !366), !dbg !367
  %27 = load %struct._Thread** %26, align 8, !dbg !368
  %28 = getelementptr inbounds %struct._Thread* %27, i32 0, i32 18, !dbg !368
  %29 = getelementptr inbounds %struct._Log* %28, i32 0, i32 4, !dbg !368
  %30 = load i64* %29, align 8, !dbg !368
  store i64* @ReadOverflowTally, i64** %24, align 8
  call void @llvm.dbg.declare(metadata !{i64** %24}, metadata !369) #6, !dbg !370
  store i64 %30, i64* %25, align 8
  call void @llvm.dbg.declare(metadata !{i64* %25}, metadata !371) #6, !dbg !370
  call void @llvm.dbg.declare(metadata !{i64* %v.i}, metadata !372) #6, !dbg !373
  %31 = load i64** %24, align 8, !dbg !374
  %32 = load volatile i64* %31, align 8, !dbg !374
  store i64 %32, i64* %v.i, align 8, !dbg !374
  br label %33, !dbg !374

; <label>:33                                      ; preds = %46, %0
  %34 = load i64* %v.i, align 8, !dbg !376
  %35 = load i64* %25, align 8, !dbg !376
  %36 = add nsw i64 %34, %35, !dbg !376
  %37 = load i64* %v.i, align 8, !dbg !376
  %38 = load i64** %24, align 8, !dbg !376
  store i64 %36, i64* %21, align 8
  call void @llvm.dbg.declare(metadata !{i64* %21}, metadata !377) #6, !dbg !378
  store i64 %37, i64* %22, align 8
  call void @llvm.dbg.declare(metadata !{i64* %22}, metadata !379) #6, !dbg !378
  store i64* %38, i64** %23, align 8
  call void @llvm.dbg.declare(metadata !{i64** %23}, metadata !380) #6, !dbg !378
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i}, metadata !381) #6, !dbg !383
  %39 = load i64* %21, align 8, !dbg !384
  %40 = load i64** %23, align 8, !dbg !384
  %41 = load i64* %22, align 8, !dbg !384
  %42 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %39, i64* %40, i64 %41) #6, !dbg !384, !srcloc !385
  store i64 %42, i64* %prevVal.i.i, align 8, !dbg !384
  %43 = load i64* %prevVal.i.i, align 8, !dbg !386
  %44 = load i64* %v.i, align 8, !dbg !376
  %45 = icmp ne i64 %43, %44, !dbg !376
  br i1 %45, label %46, label %AtomicAdd.exit, !dbg !376

; <label>:46                                      ; preds = %33
  %47 = load i64** %24, align 8, !dbg !374
  %48 = load volatile i64* %47, align 8, !dbg !374
  store i64 %48, i64* %v.i, align 8, !dbg !374
  br label %33, !dbg !374

AtomicAdd.exit:                                   ; preds = %33
  %49 = load i64* %v.i, align 8, !dbg !387
  %50 = load i64* %25, align 8, !dbg !387
  %51 = add nsw i64 %49, %50, !dbg !387
  call void @llvm.dbg.declare(metadata !{i64* %wrSetOvf}, metadata !388), !dbg !389
  store i64 0, i64* %wrSetOvf, align 8, !dbg !389
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !390), !dbg !391
  %52 = load %struct._Thread** %26, align 8, !dbg !392
  %53 = getelementptr inbounds %struct._Thread* %52, i32 0, i32 19, !dbg !392
  store %struct._Log* %53, %struct._Log** %wr, align 8, !dbg !392
  %54 = load %struct._Log** %wr, align 8, !dbg !393
  %55 = getelementptr inbounds %struct._Log* %54, i32 0, i32 4, !dbg !393
  %56 = load i64* %55, align 8, !dbg !393
  %57 = load i64* %wrSetOvf, align 8, !dbg !393
  %58 = add nsw i64 %57, %56, !dbg !393
  store i64 %58, i64* %wrSetOvf, align 8, !dbg !393
  %59 = load i64* %wrSetOvf, align 8, !dbg !395
  store i64* @WriteOverflowTally, i64** %19, align 8
  call void @llvm.dbg.declare(metadata !{i64** %19}, metadata !369) #6, !dbg !396
  store i64 %59, i64* %20, align 8
  call void @llvm.dbg.declare(metadata !{i64* %20}, metadata !371) #6, !dbg !396
  call void @llvm.dbg.declare(metadata !{i64* %v.i2}, metadata !372) #6, !dbg !397
  %60 = load i64** %19, align 8, !dbg !398
  %61 = load volatile i64* %60, align 8, !dbg !398
  store i64 %61, i64* %v.i2, align 8, !dbg !398
  br label %62, !dbg !398

; <label>:62                                      ; preds = %75, %AtomicAdd.exit
  %63 = load i64* %v.i2, align 8, !dbg !399
  %64 = load i64* %20, align 8, !dbg !399
  %65 = add nsw i64 %63, %64, !dbg !399
  %66 = load i64* %v.i2, align 8, !dbg !399
  %67 = load i64** %19, align 8, !dbg !399
  store i64 %65, i64* %16, align 8
  call void @llvm.dbg.declare(metadata !{i64* %16}, metadata !377) #6, !dbg !400
  store i64 %66, i64* %17, align 8
  call void @llvm.dbg.declare(metadata !{i64* %17}, metadata !379) #6, !dbg !400
  store i64* %67, i64** %18, align 8
  call void @llvm.dbg.declare(metadata !{i64** %18}, metadata !380) #6, !dbg !400
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i1}, metadata !381) #6, !dbg !401
  %68 = load i64* %16, align 8, !dbg !402
  %69 = load i64** %18, align 8, !dbg !402
  %70 = load i64* %17, align 8, !dbg !402
  %71 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %68, i64* %69, i64 %70) #6, !dbg !402, !srcloc !385
  store i64 %71, i64* %prevVal.i.i1, align 8, !dbg !402
  %72 = load i64* %prevVal.i.i1, align 8, !dbg !403
  %73 = load i64* %v.i2, align 8, !dbg !399
  %74 = icmp ne i64 %72, %73, !dbg !399
  br i1 %74, label %75, label %AtomicAdd.exit3, !dbg !399

; <label>:75                                      ; preds = %62
  %76 = load i64** %19, align 8, !dbg !398
  %77 = load volatile i64* %76, align 8, !dbg !398
  store i64 %77, i64* %v.i2, align 8, !dbg !398
  br label %62, !dbg !398

AtomicAdd.exit3:                                  ; preds = %62
  %78 = load i64* %v.i2, align 8, !dbg !404
  %79 = load i64* %20, align 8, !dbg !404
  %80 = add nsw i64 %78, %79, !dbg !404
  %81 = load %struct._Thread** %26, align 8, !dbg !405
  %82 = getelementptr inbounds %struct._Thread* %81, i32 0, i32 20, !dbg !405
  %83 = getelementptr inbounds %struct._Log* %82, i32 0, i32 4, !dbg !405
  %84 = load i64* %83, align 8, !dbg !405
  store i64* @LocalOverflowTally, i64** %14, align 8
  call void @llvm.dbg.declare(metadata !{i64** %14}, metadata !369) #6, !dbg !406
  store i64 %84, i64* %15, align 8
  call void @llvm.dbg.declare(metadata !{i64* %15}, metadata !371) #6, !dbg !406
  call void @llvm.dbg.declare(metadata !{i64* %v.i5}, metadata !372) #6, !dbg !407
  %85 = load i64** %14, align 8, !dbg !408
  %86 = load volatile i64* %85, align 8, !dbg !408
  store i64 %86, i64* %v.i5, align 8, !dbg !408
  br label %87, !dbg !408

; <label>:87                                      ; preds = %100, %AtomicAdd.exit3
  %88 = load i64* %v.i5, align 8, !dbg !409
  %89 = load i64* %15, align 8, !dbg !409
  %90 = add nsw i64 %88, %89, !dbg !409
  %91 = load i64* %v.i5, align 8, !dbg !409
  %92 = load i64** %14, align 8, !dbg !409
  store i64 %90, i64* %11, align 8
  call void @llvm.dbg.declare(metadata !{i64* %11}, metadata !377) #6, !dbg !410
  store i64 %91, i64* %12, align 8
  call void @llvm.dbg.declare(metadata !{i64* %12}, metadata !379) #6, !dbg !410
  store i64* %92, i64** %13, align 8
  call void @llvm.dbg.declare(metadata !{i64** %13}, metadata !380) #6, !dbg !410
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i4}, metadata !381) #6, !dbg !411
  %93 = load i64* %11, align 8, !dbg !412
  %94 = load i64** %13, align 8, !dbg !412
  %95 = load i64* %12, align 8, !dbg !412
  %96 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %93, i64* %94, i64 %95) #6, !dbg !412, !srcloc !385
  store i64 %96, i64* %prevVal.i.i4, align 8, !dbg !412
  %97 = load i64* %prevVal.i.i4, align 8, !dbg !413
  %98 = load i64* %v.i5, align 8, !dbg !409
  %99 = icmp ne i64 %97, %98, !dbg !409
  br i1 %99, label %100, label %AtomicAdd.exit6, !dbg !409

; <label>:100                                     ; preds = %87
  %101 = load i64** %14, align 8, !dbg !408
  %102 = load volatile i64* %101, align 8, !dbg !408
  store i64 %102, i64* %v.i5, align 8, !dbg !408
  br label %87, !dbg !408

AtomicAdd.exit6:                                  ; preds = %87
  %103 = load i64* %v.i5, align 8, !dbg !414
  %104 = load i64* %15, align 8, !dbg !414
  %105 = add nsw i64 %103, %104, !dbg !414
  %106 = load %struct._Thread** %26, align 8, !dbg !415
  %107 = getelementptr inbounds %struct._Thread* %106, i32 0, i32 11, !dbg !415
  %108 = load i64* %107, align 8, !dbg !415
  store i64* @StartTally, i64** %9, align 8
  call void @llvm.dbg.declare(metadata !{i64** %9}, metadata !369) #6, !dbg !416
  store i64 %108, i64* %10, align 8
  call void @llvm.dbg.declare(metadata !{i64* %10}, metadata !371) #6, !dbg !416
  call void @llvm.dbg.declare(metadata !{i64* %v.i8}, metadata !372) #6, !dbg !417
  %109 = load i64** %9, align 8, !dbg !418
  %110 = load volatile i64* %109, align 8, !dbg !418
  store i64 %110, i64* %v.i8, align 8, !dbg !418
  br label %111, !dbg !418

; <label>:111                                     ; preds = %124, %AtomicAdd.exit6
  %112 = load i64* %v.i8, align 8, !dbg !419
  %113 = load i64* %10, align 8, !dbg !419
  %114 = add nsw i64 %112, %113, !dbg !419
  %115 = load i64* %v.i8, align 8, !dbg !419
  %116 = load i64** %9, align 8, !dbg !419
  store i64 %114, i64* %6, align 8
  call void @llvm.dbg.declare(metadata !{i64* %6}, metadata !377) #6, !dbg !420
  store i64 %115, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !379) #6, !dbg !420
  store i64* %116, i64** %8, align 8
  call void @llvm.dbg.declare(metadata !{i64** %8}, metadata !380) #6, !dbg !420
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i7}, metadata !381) #6, !dbg !421
  %117 = load i64* %6, align 8, !dbg !422
  %118 = load i64** %8, align 8, !dbg !422
  %119 = load i64* %7, align 8, !dbg !422
  %120 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %117, i64* %118, i64 %119) #6, !dbg !422, !srcloc !385
  store i64 %120, i64* %prevVal.i.i7, align 8, !dbg !422
  %121 = load i64* %prevVal.i.i7, align 8, !dbg !423
  %122 = load i64* %v.i8, align 8, !dbg !419
  %123 = icmp ne i64 %121, %122, !dbg !419
  br i1 %123, label %124, label %AtomicAdd.exit9, !dbg !419

; <label>:124                                     ; preds = %111
  %125 = load i64** %9, align 8, !dbg !418
  %126 = load volatile i64* %125, align 8, !dbg !418
  store i64 %126, i64* %v.i8, align 8, !dbg !418
  br label %111, !dbg !418

AtomicAdd.exit9:                                  ; preds = %111
  %127 = load i64* %v.i8, align 8, !dbg !424
  %128 = load i64* %10, align 8, !dbg !424
  %129 = add nsw i64 %127, %128, !dbg !424
  %130 = load %struct._Thread** %26, align 8, !dbg !425
  %131 = getelementptr inbounds %struct._Thread* %130, i32 0, i32 12, !dbg !425
  %132 = load i64* %131, align 8, !dbg !425
  store i64* @AbortTally, i64** %4, align 8
  call void @llvm.dbg.declare(metadata !{i64** %4}, metadata !369) #6, !dbg !426
  store i64 %132, i64* %5, align 8
  call void @llvm.dbg.declare(metadata !{i64* %5}, metadata !371) #6, !dbg !426
  call void @llvm.dbg.declare(metadata !{i64* %v.i11}, metadata !372) #6, !dbg !427
  %133 = load i64** %4, align 8, !dbg !428
  %134 = load volatile i64* %133, align 8, !dbg !428
  store i64 %134, i64* %v.i11, align 8, !dbg !428
  br label %135, !dbg !428

; <label>:135                                     ; preds = %148, %AtomicAdd.exit9
  %136 = load i64* %v.i11, align 8, !dbg !429
  %137 = load i64* %5, align 8, !dbg !429
  %138 = add nsw i64 %136, %137, !dbg !429
  %139 = load i64* %v.i11, align 8, !dbg !429
  %140 = load i64** %4, align 8, !dbg !429
  store i64 %138, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !377) #6, !dbg !430
  store i64 %139, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !379) #6, !dbg !430
  store i64* %140, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !380) #6, !dbg !430
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i10}, metadata !381) #6, !dbg !431
  %141 = load i64* %1, align 8, !dbg !432
  %142 = load i64** %3, align 8, !dbg !432
  %143 = load i64* %2, align 8, !dbg !432
  %144 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %141, i64* %142, i64 %143) #6, !dbg !432, !srcloc !385
  store i64 %144, i64* %prevVal.i.i10, align 8, !dbg !432
  %145 = load i64* %prevVal.i.i10, align 8, !dbg !433
  %146 = load i64* %v.i11, align 8, !dbg !429
  %147 = icmp ne i64 %145, %146, !dbg !429
  br i1 %147, label %148, label %AtomicAdd.exit12, !dbg !429

; <label>:148                                     ; preds = %135
  %149 = load i64** %4, align 8, !dbg !428
  %150 = load volatile i64* %149, align 8, !dbg !428
  store i64 %150, i64* %v.i11, align 8, !dbg !428
  br label %135, !dbg !428

AtomicAdd.exit12:                                 ; preds = %135
  %151 = load i64* %v.i11, align 8, !dbg !434
  %152 = load i64* %5, align 8, !dbg !434
  %153 = add nsw i64 %151, %152, !dbg !434
  %154 = load %struct._Thread** %26, align 8, !dbg !435
  %155 = getelementptr inbounds %struct._Thread* %154, i32 0, i32 16, !dbg !435
  %156 = load %struct.tmalloc** %155, align 8, !dbg !435
  call void @tmalloc_free(%struct.tmalloc* %156), !dbg !435
  %157 = load %struct._Thread** %26, align 8, !dbg !436
  %158 = getelementptr inbounds %struct._Thread* %157, i32 0, i32 17, !dbg !436
  %159 = load %struct.tmalloc** %158, align 8, !dbg !436
  call void @tmalloc_free(%struct.tmalloc* %159), !dbg !436
  %160 = load %struct._Thread** %26, align 8, !dbg !437
  %161 = getelementptr inbounds %struct._Thread* %160, i32 0, i32 18, !dbg !437
  call void @FreeList(%struct._Log* %161, i64 8192), !dbg !437
  %162 = load %struct._Thread** %26, align 8, !dbg !438
  %163 = getelementptr inbounds %struct._Thread* %162, i32 0, i32 19, !dbg !438
  call void @FreeList(%struct._Log* %163, i64 1024), !dbg !438
  %164 = load %struct._Thread** %26, align 8, !dbg !439
  %165 = getelementptr inbounds %struct._Thread* %164, i32 0, i32 20, !dbg !439
  call void @FreeList(%struct._Log* %165, i64 1024), !dbg !439
  %166 = load %struct._Thread** %26, align 8, !dbg !440
  %167 = bitcast %struct._Thread* %166 to i8*, !dbg !440
  call void @free(i8* %167) #6, !dbg !440
  ret void, !dbg !441
}

declare void @tmalloc_free(%struct.tmalloc*) #4

; Function Attrs: nounwind uwtable
define void @TxInitThread(%struct._Thread* %t, i64 %id) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %ap.i7 = alloca %struct._AVPair*, align 8
  %List.i8 = alloca %struct._AVPair*, align 8
  %Tail.i9 = alloca %struct._AVPair*, align 8
  %i.i10 = alloca i64, align 8
  %e.i11 = alloca %struct._AVPair*, align 8
  %3 = alloca i64, align 8
  %4 = alloca %struct._Thread*, align 8
  %ap.i1 = alloca %struct._AVPair*, align 8
  %List.i2 = alloca %struct._AVPair*, align 8
  %Tail.i3 = alloca %struct._AVPair*, align 8
  %i.i4 = alloca i64, align 8
  %e.i5 = alloca %struct._AVPair*, align 8
  %5 = alloca i64, align 8
  %6 = alloca %struct._Thread*, align 8
  %ap.i = alloca %struct._AVPair*, align 8
  %List.i = alloca %struct._AVPair*, align 8
  %Tail.i = alloca %struct._AVPair*, align 8
  %i.i = alloca i64, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %7 = alloca %struct._Thread*, align 8
  %8 = alloca i64, align 8
  store %struct._Thread* %t, %struct._Thread** %7, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %7}, metadata !442), !dbg !443
  store i64 %id, i64* %8, align 8
  call void @llvm.dbg.declare(metadata !{i64* %8}, metadata !444), !dbg !443
  %9 = load i32* @global_key_self, align 4, !dbg !445
  %10 = load %struct._Thread** %7, align 8, !dbg !445
  %11 = bitcast %struct._Thread* %10 to i8*, !dbg !445
  %12 = call i32 @pthread_setspecific(i32 %9, i8* %11) #6, !dbg !445
  %13 = load %struct._Thread** %7, align 8, !dbg !446
  %14 = bitcast %struct._Thread* %13 to i8*, !dbg !446
  call void @llvm.memset.p0i8.i64(i8* %14, i8 0, i64 352, i32 8, i1 false), !dbg !446
  %15 = load i64* %8, align 8, !dbg !447
  %16 = load %struct._Thread** %7, align 8, !dbg !447
  %17 = getelementptr inbounds %struct._Thread* %16, i32 0, i32 0, !dbg !447
  store i64 %15, i64* %17, align 8, !dbg !447
  %18 = load i64* %8, align 8, !dbg !448
  %19 = add nsw i64 %18, 1, !dbg !448
  %20 = load %struct._Thread** %7, align 8, !dbg !448
  %21 = getelementptr inbounds %struct._Thread* %20, i32 0, i32 13, !dbg !448
  store i64 %19, i64* %21, align 8, !dbg !448
  %22 = load %struct._Thread** %7, align 8, !dbg !449
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 13, !dbg !449
  %24 = load i64* %23, align 8, !dbg !449
  %25 = load %struct._Thread** %7, align 8, !dbg !449
  %26 = getelementptr inbounds %struct._Thread* %25, i32 0, i32 14, !dbg !449
  %27 = getelementptr inbounds [1 x i64]* %26, i32 0, i64 0, !dbg !449
  store i64 %24, i64* %27, align 8, !dbg !449
  %28 = load %struct._Thread** %7, align 8, !dbg !450
  store i64 1024, i64* %5, align 8
  call void @llvm.dbg.declare(metadata !{i64* %5}, metadata !451) #6, !dbg !452
  store %struct._Thread* %28, %struct._Thread** %6, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %6}, metadata !453) #6, !dbg !452
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %ap.i}, metadata !454) #6, !dbg !455
  %29 = load i64* %5, align 8, !dbg !455
  %30 = mul i64 64, %29, !dbg !455
  %31 = add i64 %30, 64, !dbg !455
  %32 = call noalias i8* @malloc(i64 %31) #6, !dbg !455
  %33 = bitcast i8* %32 to %struct._AVPair*, !dbg !455
  store %struct._AVPair* %33, %struct._AVPair** %ap.i, align 8, !dbg !455
  %34 = load %struct._AVPair** %ap.i, align 8, !dbg !456
  %35 = icmp ne %struct._AVPair* %34, null, !dbg !456
  br i1 %35, label %36, label %42, !dbg !456

; <label>:36                                      ; preds = %0
  %37 = load %struct._AVPair** %ap.i, align 8, !dbg !457
  %38 = bitcast %struct._AVPair* %37 to i8*, !dbg !457
  %39 = load i64* %5, align 8, !dbg !457
  %40 = mul i64 64, %39, !dbg !457
  call void @llvm.memset.p0i8.i64(i8* %38, i8 0, i64 %40, i32 8, i1 false) #6, !dbg !457
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %List.i}, metadata !458) #6, !dbg !459
  %41 = load %struct._AVPair** %ap.i, align 8, !dbg !459
  store %struct._AVPair* %41, %struct._AVPair** %List.i, align 8, !dbg !459
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %Tail.i}, metadata !460) #6, !dbg !461
  store %struct._AVPair* null, %struct._AVPair** %Tail.i, align 8, !dbg !461
  call void @llvm.dbg.declare(metadata !{i64* %i.i}, metadata !462) #6, !dbg !463
  store i64 0, i64* %i.i, align 8, !dbg !464
  br label %43, !dbg !464

; <label>:42                                      ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([3 x i8]* @.str8, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 591, i8* getelementptr inbounds ([33 x i8]* @__PRETTY_FUNCTION__.MakeList, i32 0, i32 0)) #7, !dbg !456
  unreachable, !dbg !456

; <label>:43                                      ; preds = %47, %36
  %44 = load i64* %i.i, align 8, !dbg !464
  %45 = load i64* %5, align 8, !dbg !464
  %46 = icmp slt i64 %44, %45, !dbg !464
  br i1 %46, label %47, label %MakeList.exit, !dbg !464

; <label>:47                                      ; preds = %43
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !466) #6, !dbg !468
  %48 = load %struct._AVPair** %ap.i, align 8, !dbg !468
  %49 = getelementptr inbounds %struct._AVPair* %48, i32 1, !dbg !468
  store %struct._AVPair* %49, %struct._AVPair** %ap.i, align 8, !dbg !468
  store %struct._AVPair* %48, %struct._AVPair** %e.i, align 8, !dbg !468
  %50 = load %struct._AVPair** %ap.i, align 8, !dbg !469
  %51 = load %struct._AVPair** %e.i, align 8, !dbg !469
  %52 = getelementptr inbounds %struct._AVPair* %51, i32 0, i32 0, !dbg !469
  store %struct._AVPair* %50, %struct._AVPair** %52, align 8, !dbg !469
  %53 = load %struct._AVPair** %Tail.i, align 8, !dbg !470
  %54 = load %struct._AVPair** %e.i, align 8, !dbg !470
  %55 = getelementptr inbounds %struct._AVPair* %54, i32 0, i32 1, !dbg !470
  store %struct._AVPair* %53, %struct._AVPair** %55, align 8, !dbg !470
  %56 = load %struct._Thread** %6, align 8, !dbg !471
  %57 = load %struct._AVPair** %e.i, align 8, !dbg !471
  %58 = getelementptr inbounds %struct._AVPair* %57, i32 0, i32 6, !dbg !471
  store %struct._Thread* %56, %struct._Thread** %58, align 8, !dbg !471
  %59 = load i64* %i.i, align 8, !dbg !472
  %60 = load %struct._AVPair** %e.i, align 8, !dbg !472
  %61 = getelementptr inbounds %struct._AVPair* %60, i32 0, i32 7, !dbg !472
  store i64 %59, i64* %61, align 8, !dbg !472
  %62 = load %struct._AVPair** %e.i, align 8, !dbg !473
  store %struct._AVPair* %62, %struct._AVPair** %Tail.i, align 8, !dbg !473
  %63 = load i64* %i.i, align 8, !dbg !464
  %64 = add nsw i64 %63, 1, !dbg !464
  store i64 %64, i64* %i.i, align 8, !dbg !464
  br label %43, !dbg !464

MakeList.exit:                                    ; preds = %43
  %65 = load %struct._AVPair** %Tail.i, align 8, !dbg !474
  %66 = getelementptr inbounds %struct._AVPair* %65, i32 0, i32 0, !dbg !474
  store %struct._AVPair* null, %struct._AVPair** %66, align 8, !dbg !474
  %67 = load %struct._AVPair** %List.i, align 8, !dbg !475
  %68 = load %struct._Thread** %7, align 8, !dbg !450
  %69 = getelementptr inbounds %struct._Thread* %68, i32 0, i32 19, !dbg !450
  %70 = getelementptr inbounds %struct._Log* %69, i32 0, i32 0, !dbg !450
  store %struct._AVPair* %67, %struct._AVPair** %70, align 8, !dbg !450
  %71 = load %struct._Thread** %7, align 8, !dbg !476
  %72 = getelementptr inbounds %struct._Thread* %71, i32 0, i32 19, !dbg !476
  %73 = getelementptr inbounds %struct._Log* %72, i32 0, i32 0, !dbg !476
  %74 = load %struct._AVPair** %73, align 8, !dbg !476
  %75 = load %struct._Thread** %7, align 8, !dbg !476
  %76 = getelementptr inbounds %struct._Thread* %75, i32 0, i32 19, !dbg !476
  %77 = getelementptr inbounds %struct._Log* %76, i32 0, i32 1, !dbg !476
  store %struct._AVPair* %74, %struct._AVPair** %77, align 8, !dbg !476
  %78 = load %struct._Thread** %7, align 8, !dbg !477
  store i64 8192, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !451) #6, !dbg !478
  store %struct._Thread* %78, %struct._Thread** %4, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %4}, metadata !453) #6, !dbg !478
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %ap.i1}, metadata !454) #6, !dbg !479
  %79 = load i64* %3, align 8, !dbg !479
  %80 = mul i64 64, %79, !dbg !479
  %81 = add i64 %80, 64, !dbg !479
  %82 = call noalias i8* @malloc(i64 %81) #6, !dbg !479
  %83 = bitcast i8* %82 to %struct._AVPair*, !dbg !479
  store %struct._AVPair* %83, %struct._AVPair** %ap.i1, align 8, !dbg !479
  %84 = load %struct._AVPair** %ap.i1, align 8, !dbg !480
  %85 = icmp ne %struct._AVPair* %84, null, !dbg !480
  br i1 %85, label %86, label %92, !dbg !480

; <label>:86                                      ; preds = %MakeList.exit
  %87 = load %struct._AVPair** %ap.i1, align 8, !dbg !481
  %88 = bitcast %struct._AVPair* %87 to i8*, !dbg !481
  %89 = load i64* %3, align 8, !dbg !481
  %90 = mul i64 64, %89, !dbg !481
  call void @llvm.memset.p0i8.i64(i8* %88, i8 0, i64 %90, i32 8, i1 false) #6, !dbg !481
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %List.i2}, metadata !458) #6, !dbg !482
  %91 = load %struct._AVPair** %ap.i1, align 8, !dbg !482
  store %struct._AVPair* %91, %struct._AVPair** %List.i2, align 8, !dbg !482
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %Tail.i3}, metadata !460) #6, !dbg !483
  store %struct._AVPair* null, %struct._AVPair** %Tail.i3, align 8, !dbg !483
  call void @llvm.dbg.declare(metadata !{i64* %i.i4}, metadata !462) #6, !dbg !484
  store i64 0, i64* %i.i4, align 8, !dbg !485
  br label %93, !dbg !485

; <label>:92                                      ; preds = %MakeList.exit
  call void @__assert_fail(i8* getelementptr inbounds ([3 x i8]* @.str8, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 591, i8* getelementptr inbounds ([33 x i8]* @__PRETTY_FUNCTION__.MakeList, i32 0, i32 0)) #7, !dbg !480
  unreachable, !dbg !480

; <label>:93                                      ; preds = %97, %86
  %94 = load i64* %i.i4, align 8, !dbg !485
  %95 = load i64* %3, align 8, !dbg !485
  %96 = icmp slt i64 %94, %95, !dbg !485
  br i1 %96, label %97, label %MakeList.exit6, !dbg !485

; <label>:97                                      ; preds = %93
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i5}, metadata !466) #6, !dbg !486
  %98 = load %struct._AVPair** %ap.i1, align 8, !dbg !486
  %99 = getelementptr inbounds %struct._AVPair* %98, i32 1, !dbg !486
  store %struct._AVPair* %99, %struct._AVPair** %ap.i1, align 8, !dbg !486
  store %struct._AVPair* %98, %struct._AVPair** %e.i5, align 8, !dbg !486
  %100 = load %struct._AVPair** %ap.i1, align 8, !dbg !487
  %101 = load %struct._AVPair** %e.i5, align 8, !dbg !487
  %102 = getelementptr inbounds %struct._AVPair* %101, i32 0, i32 0, !dbg !487
  store %struct._AVPair* %100, %struct._AVPair** %102, align 8, !dbg !487
  %103 = load %struct._AVPair** %Tail.i3, align 8, !dbg !488
  %104 = load %struct._AVPair** %e.i5, align 8, !dbg !488
  %105 = getelementptr inbounds %struct._AVPair* %104, i32 0, i32 1, !dbg !488
  store %struct._AVPair* %103, %struct._AVPair** %105, align 8, !dbg !488
  %106 = load %struct._Thread** %4, align 8, !dbg !489
  %107 = load %struct._AVPair** %e.i5, align 8, !dbg !489
  %108 = getelementptr inbounds %struct._AVPair* %107, i32 0, i32 6, !dbg !489
  store %struct._Thread* %106, %struct._Thread** %108, align 8, !dbg !489
  %109 = load i64* %i.i4, align 8, !dbg !490
  %110 = load %struct._AVPair** %e.i5, align 8, !dbg !490
  %111 = getelementptr inbounds %struct._AVPair* %110, i32 0, i32 7, !dbg !490
  store i64 %109, i64* %111, align 8, !dbg !490
  %112 = load %struct._AVPair** %e.i5, align 8, !dbg !491
  store %struct._AVPair* %112, %struct._AVPair** %Tail.i3, align 8, !dbg !491
  %113 = load i64* %i.i4, align 8, !dbg !485
  %114 = add nsw i64 %113, 1, !dbg !485
  store i64 %114, i64* %i.i4, align 8, !dbg !485
  br label %93, !dbg !485

MakeList.exit6:                                   ; preds = %93
  %115 = load %struct._AVPair** %Tail.i3, align 8, !dbg !492
  %116 = getelementptr inbounds %struct._AVPair* %115, i32 0, i32 0, !dbg !492
  store %struct._AVPair* null, %struct._AVPair** %116, align 8, !dbg !492
  %117 = load %struct._AVPair** %List.i2, align 8, !dbg !493
  %118 = load %struct._Thread** %7, align 8, !dbg !477
  %119 = getelementptr inbounds %struct._Thread* %118, i32 0, i32 18, !dbg !477
  %120 = getelementptr inbounds %struct._Log* %119, i32 0, i32 0, !dbg !477
  store %struct._AVPair* %117, %struct._AVPair** %120, align 8, !dbg !477
  %121 = load %struct._Thread** %7, align 8, !dbg !494
  %122 = getelementptr inbounds %struct._Thread* %121, i32 0, i32 18, !dbg !494
  %123 = getelementptr inbounds %struct._Log* %122, i32 0, i32 0, !dbg !494
  %124 = load %struct._AVPair** %123, align 8, !dbg !494
  %125 = load %struct._Thread** %7, align 8, !dbg !494
  %126 = getelementptr inbounds %struct._Thread* %125, i32 0, i32 18, !dbg !494
  %127 = getelementptr inbounds %struct._Log* %126, i32 0, i32 1, !dbg !494
  store %struct._AVPair* %124, %struct._AVPair** %127, align 8, !dbg !494
  %128 = load %struct._Thread** %7, align 8, !dbg !495
  store i64 1024, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !451) #6, !dbg !496
  store %struct._Thread* %128, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !453) #6, !dbg !496
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %ap.i7}, metadata !454) #6, !dbg !497
  %129 = load i64* %1, align 8, !dbg !497
  %130 = mul i64 64, %129, !dbg !497
  %131 = add i64 %130, 64, !dbg !497
  %132 = call noalias i8* @malloc(i64 %131) #6, !dbg !497
  %133 = bitcast i8* %132 to %struct._AVPair*, !dbg !497
  store %struct._AVPair* %133, %struct._AVPair** %ap.i7, align 8, !dbg !497
  %134 = load %struct._AVPair** %ap.i7, align 8, !dbg !498
  %135 = icmp ne %struct._AVPair* %134, null, !dbg !498
  br i1 %135, label %136, label %142, !dbg !498

; <label>:136                                     ; preds = %MakeList.exit6
  %137 = load %struct._AVPair** %ap.i7, align 8, !dbg !499
  %138 = bitcast %struct._AVPair* %137 to i8*, !dbg !499
  %139 = load i64* %1, align 8, !dbg !499
  %140 = mul i64 64, %139, !dbg !499
  call void @llvm.memset.p0i8.i64(i8* %138, i8 0, i64 %140, i32 8, i1 false) #6, !dbg !499
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %List.i8}, metadata !458) #6, !dbg !500
  %141 = load %struct._AVPair** %ap.i7, align 8, !dbg !500
  store %struct._AVPair* %141, %struct._AVPair** %List.i8, align 8, !dbg !500
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %Tail.i9}, metadata !460) #6, !dbg !501
  store %struct._AVPair* null, %struct._AVPair** %Tail.i9, align 8, !dbg !501
  call void @llvm.dbg.declare(metadata !{i64* %i.i10}, metadata !462) #6, !dbg !502
  store i64 0, i64* %i.i10, align 8, !dbg !503
  br label %143, !dbg !503

; <label>:142                                     ; preds = %MakeList.exit6
  call void @__assert_fail(i8* getelementptr inbounds ([3 x i8]* @.str8, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 591, i8* getelementptr inbounds ([33 x i8]* @__PRETTY_FUNCTION__.MakeList, i32 0, i32 0)) #7, !dbg !498
  unreachable, !dbg !498

; <label>:143                                     ; preds = %147, %136
  %144 = load i64* %i.i10, align 8, !dbg !503
  %145 = load i64* %1, align 8, !dbg !503
  %146 = icmp slt i64 %144, %145, !dbg !503
  br i1 %146, label %147, label %MakeList.exit12, !dbg !503

; <label>:147                                     ; preds = %143
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i11}, metadata !466) #6, !dbg !504
  %148 = load %struct._AVPair** %ap.i7, align 8, !dbg !504
  %149 = getelementptr inbounds %struct._AVPair* %148, i32 1, !dbg !504
  store %struct._AVPair* %149, %struct._AVPair** %ap.i7, align 8, !dbg !504
  store %struct._AVPair* %148, %struct._AVPair** %e.i11, align 8, !dbg !504
  %150 = load %struct._AVPair** %ap.i7, align 8, !dbg !505
  %151 = load %struct._AVPair** %e.i11, align 8, !dbg !505
  %152 = getelementptr inbounds %struct._AVPair* %151, i32 0, i32 0, !dbg !505
  store %struct._AVPair* %150, %struct._AVPair** %152, align 8, !dbg !505
  %153 = load %struct._AVPair** %Tail.i9, align 8, !dbg !506
  %154 = load %struct._AVPair** %e.i11, align 8, !dbg !506
  %155 = getelementptr inbounds %struct._AVPair* %154, i32 0, i32 1, !dbg !506
  store %struct._AVPair* %153, %struct._AVPair** %155, align 8, !dbg !506
  %156 = load %struct._Thread** %2, align 8, !dbg !507
  %157 = load %struct._AVPair** %e.i11, align 8, !dbg !507
  %158 = getelementptr inbounds %struct._AVPair* %157, i32 0, i32 6, !dbg !507
  store %struct._Thread* %156, %struct._Thread** %158, align 8, !dbg !507
  %159 = load i64* %i.i10, align 8, !dbg !508
  %160 = load %struct._AVPair** %e.i11, align 8, !dbg !508
  %161 = getelementptr inbounds %struct._AVPair* %160, i32 0, i32 7, !dbg !508
  store i64 %159, i64* %161, align 8, !dbg !508
  %162 = load %struct._AVPair** %e.i11, align 8, !dbg !509
  store %struct._AVPair* %162, %struct._AVPair** %Tail.i9, align 8, !dbg !509
  %163 = load i64* %i.i10, align 8, !dbg !503
  %164 = add nsw i64 %163, 1, !dbg !503
  store i64 %164, i64* %i.i10, align 8, !dbg !503
  br label %143, !dbg !503

MakeList.exit12:                                  ; preds = %143
  %165 = load %struct._AVPair** %Tail.i9, align 8, !dbg !510
  %166 = getelementptr inbounds %struct._AVPair* %165, i32 0, i32 0, !dbg !510
  store %struct._AVPair* null, %struct._AVPair** %166, align 8, !dbg !510
  %167 = load %struct._AVPair** %List.i8, align 8, !dbg !511
  %168 = load %struct._Thread** %7, align 8, !dbg !495
  %169 = getelementptr inbounds %struct._Thread* %168, i32 0, i32 20, !dbg !495
  %170 = getelementptr inbounds %struct._Log* %169, i32 0, i32 0, !dbg !495
  store %struct._AVPair* %167, %struct._AVPair** %170, align 8, !dbg !495
  %171 = load %struct._Thread** %7, align 8, !dbg !512
  %172 = getelementptr inbounds %struct._Thread* %171, i32 0, i32 20, !dbg !512
  %173 = getelementptr inbounds %struct._Log* %172, i32 0, i32 0, !dbg !512
  %174 = load %struct._AVPair** %173, align 8, !dbg !512
  %175 = load %struct._Thread** %7, align 8, !dbg !512
  %176 = getelementptr inbounds %struct._Thread* %175, i32 0, i32 20, !dbg !512
  %177 = getelementptr inbounds %struct._Log* %176, i32 0, i32 1, !dbg !512
  store %struct._AVPair* %174, %struct._AVPair** %177, align 8, !dbg !512
  %178 = call %struct.tmalloc* @tmalloc_alloc(i64 1), !dbg !513
  %179 = load %struct._Thread** %7, align 8, !dbg !513
  %180 = getelementptr inbounds %struct._Thread* %179, i32 0, i32 16, !dbg !513
  store %struct.tmalloc* %178, %struct.tmalloc** %180, align 8, !dbg !513
  %181 = load %struct._Thread** %7, align 8, !dbg !514
  %182 = getelementptr inbounds %struct._Thread* %181, i32 0, i32 16, !dbg !514
  %183 = load %struct.tmalloc** %182, align 8, !dbg !514
  %184 = icmp ne %struct.tmalloc* %183, null, !dbg !514
  br i1 %184, label %185, label %186, !dbg !514

; <label>:185                                     ; preds = %MakeList.exit12
  br label %188, !dbg !514

; <label>:186                                     ; preds = %MakeList.exit12
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8]* @.str5, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1151, i8* getelementptr inbounds ([34 x i8]* @__PRETTY_FUNCTION__.TxInitThread, i32 0, i32 0)) #7, !dbg !514
  unreachable, !dbg !514
                                                  ; No predecessors!
  br label %188, !dbg !514

; <label>:188                                     ; preds = %187, %185
  %189 = call %struct.tmalloc* @tmalloc_alloc(i64 1), !dbg !515
  %190 = load %struct._Thread** %7, align 8, !dbg !515
  %191 = getelementptr inbounds %struct._Thread* %190, i32 0, i32 17, !dbg !515
  store %struct.tmalloc* %189, %struct.tmalloc** %191, align 8, !dbg !515
  %192 = load %struct._Thread** %7, align 8, !dbg !516
  %193 = getelementptr inbounds %struct._Thread* %192, i32 0, i32 17, !dbg !516
  %194 = load %struct.tmalloc** %193, align 8, !dbg !516
  %195 = icmp ne %struct.tmalloc* %194, null, !dbg !516
  br i1 %195, label %196, label %197, !dbg !516

; <label>:196                                     ; preds = %188
  br label %199, !dbg !516

; <label>:197                                     ; preds = %188
  call void @__assert_fail(i8* getelementptr inbounds ([11 x i8]* @.str6, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1153, i8* getelementptr inbounds ([34 x i8]* @__PRETTY_FUNCTION__.TxInitThread, i32 0, i32 0)) #7, !dbg !516
  unreachable, !dbg !516
                                                  ; No predecessors!
  br label %199, !dbg !516

; <label>:199                                     ; preds = %198, %196
  %200 = load %struct._Thread** %7, align 8, !dbg !517
  %201 = load %struct._Thread** %7, align 8, !dbg !517
  %202 = getelementptr inbounds %struct._Thread* %201, i32 0, i32 8, !dbg !517
  %203 = getelementptr inbounds %struct._AVPair* %202, i32 0, i32 6, !dbg !517
  store %struct._Thread* %200, %struct._Thread** %203, align 8, !dbg !517
  ret void, !dbg !518
}

; Function Attrs: nounwind
declare i32 @pthread_setspecific(i32, i8*) #3

; Function Attrs: nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #6

declare %struct.tmalloc* @tmalloc_alloc(i64) #4

; Function Attrs: nounwind uwtable
define void @TxAbort(%struct._Thread* %Self) #0 {
  %1 = alloca %struct._Thread*, align 8
  %wr.i = alloca %struct._Log*, align 8
  %p.i = alloca %struct._AVPair*, align 8
  %End.i = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Log*, align 8
  %e.i1 = alloca %struct._AVPair*, align 8
  %3 = alloca %struct._Thread*, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  %x.i.i.i = alloca i64, align 8
  %6 = alloca %struct._Thread*, align 8
  %7 = alloca %struct._Thread*, align 8
  %8 = alloca i64, align 8
  %stall.i = alloca i64, align 8
  %i.i = alloca i64, align 8
  %9 = alloca %struct._Log*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %10 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %10, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %10}, metadata !519), !dbg !520
  %11 = load %struct._Thread** %10, align 8, !dbg !521
  %12 = getelementptr inbounds %struct._Thread* %11, i32 0, i32 1, !dbg !521
  store volatile i64 5, i64* %12, align 8, !dbg !521
  %13 = load %struct._Thread** %10, align 8, !dbg !522
  %14 = getelementptr inbounds %struct._Thread* %13, i32 0, i32 19, !dbg !522
  store %struct._Log* %14, %struct._Log** %9, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %9}, metadata !523), !dbg !524
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !525), !dbg !526
  %15 = load %struct._Log** %9, align 8, !dbg !527
  %16 = getelementptr inbounds %struct._Log* %15, i32 0, i32 2, !dbg !527
  %17 = load %struct._AVPair** %16, align 8, !dbg !527
  store %struct._AVPair* %17, %struct._AVPair** %e.i, align 8, !dbg !527
  br label %18, !dbg !527

; <label>:18                                      ; preds = %21, %0
  %19 = load %struct._AVPair** %e.i, align 8, !dbg !527
  %20 = icmp ne %struct._AVPair* %19, null, !dbg !527
  br i1 %20, label %21, label %WriteBackReverse.exit, !dbg !527

; <label>:21                                      ; preds = %18
  %22 = load %struct._AVPair** %e.i, align 8, !dbg !529
  %23 = getelementptr inbounds %struct._AVPair* %22, i32 0, i32 3, !dbg !529
  %24 = load i64* %23, align 8, !dbg !529
  %25 = load %struct._AVPair** %e.i, align 8, !dbg !529
  %26 = getelementptr inbounds %struct._AVPair* %25, i32 0, i32 2, !dbg !529
  %27 = load i64** %26, align 8, !dbg !529
  store volatile i64 %24, i64* %27, align 8, !dbg !529
  %28 = load %struct._AVPair** %e.i, align 8, !dbg !527
  %29 = getelementptr inbounds %struct._AVPair* %28, i32 0, i32 1, !dbg !527
  %30 = load %struct._AVPair** %29, align 8, !dbg !527
  store %struct._AVPair* %30, %struct._AVPair** %e.i, align 8, !dbg !527
  br label %18, !dbg !527

WriteBackReverse.exit:                            ; preds = %18
  %31 = load %struct._Thread** %10, align 8, !dbg !531
  store %struct._Thread* %31, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !532), !dbg !533
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr.i}, metadata !534), !dbg !535
  %32 = load %struct._Thread** %1, align 8, !dbg !535
  %33 = getelementptr inbounds %struct._Thread* %32, i32 0, i32 19, !dbg !535
  store %struct._Log* %33, %struct._Log** %wr.i, align 8, !dbg !535
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i}, metadata !536), !dbg !538
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i}, metadata !539), !dbg !541
  %34 = load %struct._Log** %wr.i, align 8, !dbg !541
  %35 = getelementptr inbounds %struct._Log* %34, i32 0, i32 1, !dbg !541
  %36 = load %struct._AVPair** %35, align 8, !dbg !541
  store %struct._AVPair* %36, %struct._AVPair** %End.i, align 8, !dbg !541
  %37 = load %struct._Log** %wr.i, align 8, !dbg !542
  %38 = getelementptr inbounds %struct._Log* %37, i32 0, i32 0, !dbg !542
  %39 = load %struct._AVPair** %38, align 8, !dbg !542
  store %struct._AVPair* %39, %struct._AVPair** %p.i, align 8, !dbg !542
  br label %40, !dbg !542

; <label>:40                                      ; preds = %44, %WriteBackReverse.exit
  %41 = load %struct._AVPair** %p.i, align 8, !dbg !542
  %42 = load %struct._AVPair** %End.i, align 8, !dbg !542
  %43 = icmp ne %struct._AVPair* %41, %42, !dbg !542
  br i1 %43, label %44, label %RestoreLocks.exit, !dbg !542

; <label>:44                                      ; preds = %40
  %45 = load %struct._AVPair** %p.i, align 8, !dbg !544
  %46 = getelementptr inbounds %struct._AVPair* %45, i32 0, i32 5, !dbg !544
  %47 = load i64* %46, align 8, !dbg !544
  %48 = load %struct._AVPair** %p.i, align 8, !dbg !544
  %49 = getelementptr inbounds %struct._AVPair* %48, i32 0, i32 4, !dbg !544
  %50 = load i64** %49, align 8, !dbg !544
  store volatile i64 %47, i64* %50, align 8, !dbg !544
  %51 = load %struct._AVPair** %p.i, align 8, !dbg !542
  %52 = getelementptr inbounds %struct._AVPair* %51, i32 0, i32 0, !dbg !542
  %53 = load %struct._AVPair** %52, align 8, !dbg !542
  store %struct._AVPair* %53, %struct._AVPair** %p.i, align 8, !dbg !542
  br label %40, !dbg !542

RestoreLocks.exit:                                ; preds = %40
  %54 = load %struct._Thread** %10, align 8, !dbg !546
  %55 = getelementptr inbounds %struct._Thread* %54, i32 0, i32 20, !dbg !546
  %56 = getelementptr inbounds %struct._Log* %55, i32 0, i32 1, !dbg !546
  %57 = load %struct._AVPair** %56, align 8, !dbg !546
  %58 = load %struct._Thread** %10, align 8, !dbg !546
  %59 = getelementptr inbounds %struct._Thread* %58, i32 0, i32 20, !dbg !546
  %60 = getelementptr inbounds %struct._Log* %59, i32 0, i32 0, !dbg !546
  %61 = load %struct._AVPair** %60, align 8, !dbg !546
  %62 = icmp ne %struct._AVPair* %57, %61, !dbg !546
  br i1 %62, label %63, label %82, !dbg !546

; <label>:63                                      ; preds = %RestoreLocks.exit
  %64 = load %struct._Thread** %10, align 8, !dbg !547
  %65 = getelementptr inbounds %struct._Thread* %64, i32 0, i32 20, !dbg !547
  store %struct._Log* %65, %struct._Log** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %2}, metadata !523), !dbg !549
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i1}, metadata !525), !dbg !550
  %66 = load %struct._Log** %2, align 8, !dbg !551
  %67 = getelementptr inbounds %struct._Log* %66, i32 0, i32 2, !dbg !551
  %68 = load %struct._AVPair** %67, align 8, !dbg !551
  store %struct._AVPair* %68, %struct._AVPair** %e.i1, align 8, !dbg !551
  br label %69, !dbg !551

; <label>:69                                      ; preds = %72, %63
  %70 = load %struct._AVPair** %e.i1, align 8, !dbg !551
  %71 = icmp ne %struct._AVPair* %70, null, !dbg !551
  br i1 %71, label %72, label %WriteBackReverse.exit2, !dbg !551

; <label>:72                                      ; preds = %69
  %73 = load %struct._AVPair** %e.i1, align 8, !dbg !552
  %74 = getelementptr inbounds %struct._AVPair* %73, i32 0, i32 3, !dbg !552
  %75 = load i64* %74, align 8, !dbg !552
  %76 = load %struct._AVPair** %e.i1, align 8, !dbg !552
  %77 = getelementptr inbounds %struct._AVPair* %76, i32 0, i32 2, !dbg !552
  %78 = load i64** %77, align 8, !dbg !552
  store volatile i64 %75, i64* %78, align 8, !dbg !552
  %79 = load %struct._AVPair** %e.i1, align 8, !dbg !551
  %80 = getelementptr inbounds %struct._AVPair* %79, i32 0, i32 1, !dbg !551
  %81 = load %struct._AVPair** %80, align 8, !dbg !551
  store %struct._AVPair* %81, %struct._AVPair** %e.i1, align 8, !dbg !551
  br label %69, !dbg !551

WriteBackReverse.exit2:                           ; preds = %69
  br label %82, !dbg !553

; <label>:82                                      ; preds = %WriteBackReverse.exit2, %RestoreLocks.exit
  %83 = load %struct._Thread** %10, align 8, !dbg !554
  %84 = getelementptr inbounds %struct._Thread* %83, i32 0, i32 3, !dbg !554
  %85 = load volatile i64* %84, align 8, !dbg !554
  %86 = add nsw i64 %85, 1, !dbg !554
  store volatile i64 %86, i64* %84, align 8, !dbg !554
  %87 = load %struct._Thread** %10, align 8, !dbg !555
  %88 = getelementptr inbounds %struct._Thread* %87, i32 0, i32 12, !dbg !555
  %89 = load i64* %88, align 8, !dbg !555
  %90 = add nsw i64 %89, 1, !dbg !555
  store i64 %90, i64* %88, align 8, !dbg !555
  %91 = load %struct._Thread** %10, align 8, !dbg !556
  store %struct._Thread* %91, %struct._Thread** %3, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %3}, metadata !557), !dbg !558
  %92 = icmp ne i64 0, 0, !dbg !556
  br i1 %92, label %93, label %94, !dbg !556

; <label>:93                                      ; preds = %82
  br label %142, !dbg !559

; <label>:94                                      ; preds = %82
  %95 = load %struct._Thread** %10, align 8, !dbg !561
  %96 = getelementptr inbounds %struct._Thread* %95, i32 0, i32 3, !dbg !561
  %97 = load volatile i64* %96, align 8, !dbg !561
  %98 = icmp sgt i64 %97, 3, !dbg !561
  br i1 %98, label %99, label %141, !dbg !561

; <label>:99                                      ; preds = %94
  %100 = load %struct._Thread** %10, align 8, !dbg !562
  %101 = load %struct._Thread** %10, align 8, !dbg !562
  %102 = getelementptr inbounds %struct._Thread* %101, i32 0, i32 3, !dbg !562
  %103 = load volatile i64* %102, align 8, !dbg !562
  store %struct._Thread* %100, %struct._Thread** %7, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %7}, metadata !564), !dbg !565
  store i64 %103, i64* %8, align 8
  call void @llvm.dbg.declare(metadata !{i64* %8}, metadata !566), !dbg !565
  call void @llvm.dbg.declare(metadata !{i64* %stall.i}, metadata !567), !dbg !569
  %104 = load %struct._Thread** %7, align 8, !dbg !570
  store %struct._Thread* %104, %struct._Thread** %6, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %6}, metadata !571), !dbg !572
  %105 = load %struct._Thread** %6, align 8, !dbg !573
  %106 = getelementptr inbounds %struct._Thread* %105, i32 0, i32 13, !dbg !573
  store i64* %106, i64** %5, align 8
  call void @llvm.dbg.declare(metadata !{i64** %5}, metadata !574), !dbg !575
  call void @llvm.dbg.declare(metadata !{i64* %x.i.i.i}, metadata !576), !dbg !577
  %107 = load i64** %5, align 8, !dbg !578
  %108 = load i64* %107, align 8, !dbg !578
  store i64 %108, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !579), !dbg !580
  %109 = load i64* %4, align 8, !dbg !581
  %110 = icmp eq i64 %109, 0, !dbg !581
  br i1 %110, label %111, label %TSRandom.exit.i, !dbg !581

; <label>:111                                     ; preds = %99
  store i64 1, i64* %4, align 8, !dbg !582
  br label %TSRandom.exit.i, !dbg !584

TSRandom.exit.i:                                  ; preds = %111, %99
  %112 = load i64* %4, align 8, !dbg !585
  %113 = shl i64 %112, 6, !dbg !585
  %114 = load i64* %4, align 8, !dbg !585
  %115 = xor i64 %114, %113, !dbg !585
  store i64 %115, i64* %4, align 8, !dbg !585
  %116 = load i64* %4, align 8, !dbg !586
  %117 = lshr i64 %116, 21, !dbg !586
  %118 = load i64* %4, align 8, !dbg !586
  %119 = xor i64 %118, %117, !dbg !586
  store i64 %119, i64* %4, align 8, !dbg !586
  %120 = load i64* %4, align 8, !dbg !587
  %121 = shl i64 %120, 7, !dbg !587
  %122 = load i64* %4, align 8, !dbg !587
  %123 = xor i64 %122, %121, !dbg !587
  store i64 %123, i64* %4, align 8, !dbg !587
  %124 = load i64* %4, align 8, !dbg !588
  store i64 %124, i64* %x.i.i.i, align 8, !dbg !578
  %125 = load i64* %x.i.i.i, align 8, !dbg !589
  %126 = load i64** %5, align 8, !dbg !589
  store i64 %125, i64* %126, align 8, !dbg !589
  %127 = load i64* %x.i.i.i, align 8, !dbg !590
  %128 = and i64 %127, 15, !dbg !570
  store i64 %128, i64* %stall.i, align 8, !dbg !570
  %129 = load i64* %8, align 8, !dbg !591
  %130 = ashr i64 %129, 2, !dbg !591
  %131 = load i64* %stall.i, align 8, !dbg !591
  %132 = add i64 %131, %130, !dbg !591
  store i64 %132, i64* %stall.i, align 8, !dbg !591
  %133 = load i64* %stall.i, align 8, !dbg !592
  %134 = mul i64 %133, 10, !dbg !592
  store i64 %134, i64* %stall.i, align 8, !dbg !592
  call void @llvm.dbg.declare(metadata !{i64* %i.i}, metadata !593), !dbg !595
  store volatile i64 0, i64* %i.i, align 8, !dbg !595
  br label %135, !dbg !596

; <label>:135                                     ; preds = %140, %TSRandom.exit.i
  %136 = load volatile i64* %i.i, align 8, !dbg !596
  %137 = add i64 %136, 1, !dbg !596
  store volatile i64 %137, i64* %i.i, align 8, !dbg !596
  %138 = load i64* %stall.i, align 8, !dbg !596
  %139 = icmp ult i64 %136, %138, !dbg !596
  br i1 %139, label %140, label %backoff.exit, !dbg !596

; <label>:140                                     ; preds = %135
  br label %135, !dbg !597

backoff.exit:                                     ; preds = %135
  br label %141, !dbg !599

; <label>:141                                     ; preds = %backoff.exit, %94
  br label %142, !dbg !599

; <label>:142                                     ; preds = %141, %93
  %143 = load %struct._Thread** %10, align 8, !dbg !600
  %144 = getelementptr inbounds %struct._Thread* %143, i32 0, i32 16, !dbg !600
  %145 = load %struct.tmalloc** %144, align 8, !dbg !600
  call void @tmalloc_releaseAllReverse(%struct.tmalloc* %145, void (i8*, i64)* null), !dbg !600
  %146 = load %struct._Thread** %10, align 8, !dbg !601
  %147 = getelementptr inbounds %struct._Thread* %146, i32 0, i32 17, !dbg !601
  %148 = load %struct.tmalloc** %147, align 8, !dbg !601
  call void @tmalloc_clear(%struct.tmalloc* %148), !dbg !601
  %149 = load %struct._Thread** %10, align 8, !dbg !602
  %150 = getelementptr inbounds %struct._Thread* %149, i32 0, i32 21, !dbg !602
  %151 = load [1 x %struct.__jmp_buf_tag]** %150, align 8, !dbg !602
  %152 = getelementptr inbounds [1 x %struct.__jmp_buf_tag]* %151, i32 0, i32 0, !dbg !602
  call void @siglongjmp(%struct.__jmp_buf_tag* %152, i32 1) #7, !dbg !602
  unreachable, !dbg !602
                                                  ; No predecessors!
  ret void, !dbg !603
}

declare void @tmalloc_releaseAllReverse(%struct.tmalloc*, void (i8*, i64)*) #4

declare void @tmalloc_clear(%struct.tmalloc*) #4

; Function Attrs: noreturn nounwind
declare void @siglongjmp(%struct.__jmp_buf_tag*, i32) #5

; Function Attrs: nounwind uwtable
define void @TxStore(%struct._Thread* %Self, i64* %addr, i64 %valu) #0 {
  %1 = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Log*, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64, align 8
  %5 = alloca i64*, align 8
  %6 = alloca i64, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %7 = alloca i64, align 8
  %8 = alloca i64, align 8
  %9 = alloca i64*, align 8
  %prevVal.i = alloca i64, align 8
  %10 = alloca %struct._Thread*, align 8
  %11 = alloca i64*, align 8
  %12 = alloca i64, align 8
  %LockFor = alloca i64*, align 8
  %cv = alloca i64, align 8
  %c = alloca i64, align 8
  %p = alloca %struct._AVPair*, align 8
  %wr = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._Thread* %Self, %struct._Thread** %10, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %10}, metadata !604), !dbg !605
  store i64* %addr, i64** %11, align 8
  call void @llvm.dbg.declare(metadata !{i64** %11}, metadata !606), !dbg !605
  store i64 %valu, i64* %12, align 8
  call void @llvm.dbg.declare(metadata !{i64* %12}, metadata !607), !dbg !605
  %13 = load %struct._Thread** %10, align 8, !dbg !608
  %14 = getelementptr inbounds %struct._Thread* %13, i32 0, i32 10, !dbg !608
  %15 = load i32* %14, align 4, !dbg !608
  %16 = icmp ne i32 %15, 0, !dbg !608
  br i1 %16, label %17, label %22, !dbg !608

; <label>:17                                      ; preds = %0
  %18 = load %struct._Thread** %10, align 8, !dbg !609
  %19 = getelementptr inbounds %struct._Thread* %18, i32 0, i32 9, !dbg !609
  %20 = load i32** %19, align 8, !dbg !609
  store i32 0, i32* %20, align 4, !dbg !609
  %21 = load %struct._Thread** %10, align 8, !dbg !611
  call void @TxAbort(%struct._Thread* %21), !dbg !611
  br label %22, !dbg !612

; <label>:22                                      ; preds = %17, %0
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !613), !dbg !614
  %23 = load i64** %11, align 8, !dbg !614
  %24 = ptrtoint i64* %23 to i64, !dbg !614
  %25 = add i64 %24, 128, !dbg !614
  %26 = lshr i64 %25, 3, !dbg !614
  %27 = and i64 %26, 1048575, !dbg !614
  %28 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %27, !dbg !614
  store i64* %28, i64** %LockFor, align 8, !dbg !614
  call void @llvm.dbg.declare(metadata !{i64* %cv}, metadata !615), !dbg !616
  %29 = load i64** %LockFor, align 8, !dbg !616
  %30 = load volatile i64* %29, align 8, !dbg !616
  store i64 %30, i64* %cv, align 8, !dbg !616
  %31 = load i64* %cv, align 8, !dbg !617
  %32 = and i64 %31, 1, !dbg !617
  %33 = icmp ne i64 %32, 0, !dbg !617
  br i1 %33, label %34, label %48, !dbg !617

; <label>:34                                      ; preds = %22
  %35 = load i64* %cv, align 8, !dbg !617
  %36 = xor i64 %35, 1, !dbg !617
  %37 = inttoptr i64 %36 to %struct._AVPair*, !dbg !617
  %38 = getelementptr inbounds %struct._AVPair* %37, i32 0, i32 6, !dbg !617
  %39 = load %struct._Thread** %38, align 8, !dbg !617
  %40 = load %struct._Thread** %10, align 8, !dbg !617
  %41 = icmp eq %struct._Thread* %39, %40, !dbg !617
  br i1 %41, label %42, label %48, !dbg !617

; <label>:42                                      ; preds = %34
  %43 = load i64* %cv, align 8, !dbg !618
  %44 = xor i64 %43, 1, !dbg !618
  %45 = inttoptr i64 %44 to %struct._AVPair*, !dbg !618
  %46 = getelementptr inbounds %struct._AVPair* %45, i32 0, i32 5, !dbg !618
  %47 = load i64* %46, align 8, !dbg !618
  store i64 %47, i64* %cv, align 8, !dbg !618
  br label %79, !dbg !620

; <label>:48                                      ; preds = %34, %22
  call void @llvm.dbg.declare(metadata !{i64* %c}, metadata !621), !dbg !623
  store i64 100, i64* %c, align 8, !dbg !623
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p}, metadata !624), !dbg !625
  %49 = load %struct._Thread** %10, align 8, !dbg !625
  %50 = getelementptr inbounds %struct._Thread* %49, i32 0, i32 8, !dbg !625
  store %struct._AVPair* %50, %struct._AVPair** %p, align 8, !dbg !625
  br label %51, !dbg !626

; <label>:51                                      ; preds = %77, %48
  %52 = load i64** %LockFor, align 8, !dbg !628
  %53 = load volatile i64* %52, align 8, !dbg !628
  store i64 %53, i64* %cv, align 8, !dbg !628
  %54 = load i64* %cv, align 8, !dbg !630
  %55 = and i64 %54, 1, !dbg !630
  %56 = icmp eq i64 %55, 0, !dbg !630
  br i1 %56, label %57, label %71, !dbg !630

; <label>:57                                      ; preds = %51
  %58 = load %struct._AVPair** %p, align 8, !dbg !631
  %59 = ptrtoint %struct._AVPair* %58 to i64, !dbg !631
  %60 = or i64 %59, 1, !dbg !631
  %61 = load i64* %cv, align 8, !dbg !631
  %62 = load i64** %LockFor, align 8, !dbg !631
  store i64 %60, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !377) #6, !dbg !632
  store i64 %61, i64* %8, align 8
  call void @llvm.dbg.declare(metadata !{i64* %8}, metadata !379) #6, !dbg !632
  store i64* %62, i64** %9, align 8
  call void @llvm.dbg.declare(metadata !{i64** %9}, metadata !380) #6, !dbg !632
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i}, metadata !381) #6, !dbg !633
  %63 = load i64* %7, align 8, !dbg !634
  %64 = load i64** %9, align 8, !dbg !634
  %65 = load i64* %8, align 8, !dbg !634
  %66 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %63, i64* %64, i64 %65) #6, !dbg !634, !srcloc !385
  store i64 %66, i64* %prevVal.i, align 8, !dbg !634
  %67 = load i64* %prevVal.i, align 8, !dbg !635
  %68 = load i64* %cv, align 8, !dbg !631
  %69 = icmp eq i64 %67, %68, !dbg !631
  br i1 %69, label %70, label %71, !dbg !631

; <label>:70                                      ; preds = %57
  br label %78, !dbg !636

; <label>:71                                      ; preds = %57, %51
  %72 = load i64* %c, align 8, !dbg !638
  %73 = add nsw i64 %72, -1, !dbg !638
  store i64 %73, i64* %c, align 8, !dbg !638
  %74 = icmp slt i64 %73, 0, !dbg !638
  br i1 %74, label %75, label %77, !dbg !638

; <label>:75                                      ; preds = %71
  %76 = load %struct._Thread** %10, align 8, !dbg !639
  call void @TxAbort(%struct._Thread* %76), !dbg !639
  br label %77, !dbg !641

; <label>:77                                      ; preds = %75, %71
  br label %51, !dbg !642

; <label>:78                                      ; preds = %70
  br label %79

; <label>:79                                      ; preds = %78, %42
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !643), !dbg !644
  %80 = load %struct._Thread** %10, align 8, !dbg !644
  %81 = getelementptr inbounds %struct._Thread* %80, i32 0, i32 19, !dbg !644
  store %struct._Log* %81, %struct._Log** %wr, align 8, !dbg !644
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !645), !dbg !646
  %82 = load %struct._Log** %wr, align 8, !dbg !647
  %83 = load i64** %11, align 8, !dbg !647
  %84 = load i64** %11, align 8, !dbg !647
  %85 = load volatile i64* %84, align 8, !dbg !647
  %86 = load i64** %LockFor, align 8, !dbg !647
  %87 = load i64* %cv, align 8, !dbg !647
  store %struct._Log* %82, %struct._Log** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %2}, metadata !648) #6, !dbg !649
  store i64* %83, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !650) #6, !dbg !651
  store i64 %85, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !652) #6, !dbg !653
  store i64* %86, i64** %5, align 8
  call void @llvm.dbg.declare(metadata !{i64** %5}, metadata !654) #6, !dbg !655
  store i64 %87, i64* %6, align 8
  call void @llvm.dbg.declare(metadata !{i64* %6}, metadata !656) #6, !dbg !657
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !658) #6, !dbg !659
  %88 = load %struct._Log** %2, align 8, !dbg !659
  %89 = getelementptr inbounds %struct._Log* %88, i32 0, i32 1, !dbg !659
  %90 = load %struct._AVPair** %89, align 8, !dbg !659
  store %struct._AVPair* %90, %struct._AVPair** %e.i, align 8, !dbg !659
  %91 = load %struct._AVPair** %e.i, align 8, !dbg !660
  %92 = icmp eq %struct._AVPair* %91, null, !dbg !660
  br i1 %92, label %93, label %RecordStore.exit, !dbg !660

; <label>:93                                      ; preds = %79
  %94 = load %struct._Log** %2, align 8, !dbg !661
  %95 = getelementptr inbounds %struct._Log* %94, i32 0, i32 4, !dbg !661
  %96 = load i64* %95, align 8, !dbg !661
  %97 = add nsw i64 %96, 1, !dbg !661
  store i64 %97, i64* %95, align 8, !dbg !661
  %98 = load %struct._Log** %2, align 8, !dbg !663
  %99 = getelementptr inbounds %struct._Log* %98, i32 0, i32 2, !dbg !663
  %100 = load %struct._AVPair** %99, align 8, !dbg !663
  store %struct._AVPair* %100, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !664) #6, !dbg !665
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !666) #6, !dbg !667
  %101 = call noalias i8* @malloc(i64 64) #6, !dbg !667
  %102 = bitcast i8* %101 to %struct._AVPair*, !dbg !667
  store %struct._AVPair* %102, %struct._AVPair** %e.i.i, align 8, !dbg !667
  %103 = load %struct._AVPair** %e.i.i, align 8, !dbg !668
  %104 = icmp ne %struct._AVPair* %103, null, !dbg !668
  br i1 %104, label %ExtendList.exit.i, label %105, !dbg !668

; <label>:105                                     ; preds = %93
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !668
  unreachable, !dbg !668

ExtendList.exit.i:                                ; preds = %93
  %106 = load %struct._AVPair** %e.i.i, align 8, !dbg !669
  %107 = bitcast %struct._AVPair* %106 to i8*, !dbg !669
  call void @llvm.memset.p0i8.i64(i8* %107, i8 0, i64 64, i32 8, i1 false) #6, !dbg !669
  %108 = load %struct._AVPair** %e.i.i, align 8, !dbg !670
  %109 = load %struct._AVPair** %1, align 8, !dbg !670
  %110 = getelementptr inbounds %struct._AVPair* %109, i32 0, i32 0, !dbg !670
  store %struct._AVPair* %108, %struct._AVPair** %110, align 8, !dbg !670
  %111 = load %struct._AVPair** %1, align 8, !dbg !671
  %112 = load %struct._AVPair** %e.i.i, align 8, !dbg !671
  %113 = getelementptr inbounds %struct._AVPair* %112, i32 0, i32 1, !dbg !671
  store %struct._AVPair* %111, %struct._AVPair** %113, align 8, !dbg !671
  %114 = load %struct._AVPair** %e.i.i, align 8, !dbg !672
  %115 = getelementptr inbounds %struct._AVPair* %114, i32 0, i32 0, !dbg !672
  store %struct._AVPair* null, %struct._AVPair** %115, align 8, !dbg !672
  %116 = load %struct._AVPair** %1, align 8, !dbg !673
  %117 = getelementptr inbounds %struct._AVPair* %116, i32 0, i32 6, !dbg !673
  %118 = load %struct._Thread** %117, align 8, !dbg !673
  %119 = load %struct._AVPair** %e.i.i, align 8, !dbg !673
  %120 = getelementptr inbounds %struct._AVPair* %119, i32 0, i32 6, !dbg !673
  store %struct._Thread* %118, %struct._Thread** %120, align 8, !dbg !673
  %121 = load %struct._AVPair** %1, align 8, !dbg !674
  %122 = getelementptr inbounds %struct._AVPair* %121, i32 0, i32 7, !dbg !674
  %123 = load i64* %122, align 8, !dbg !674
  %124 = add nsw i64 %123, 1, !dbg !674
  %125 = load %struct._AVPair** %e.i.i, align 8, !dbg !674
  %126 = getelementptr inbounds %struct._AVPair* %125, i32 0, i32 7, !dbg !674
  store i64 %124, i64* %126, align 8, !dbg !674
  %127 = load %struct._AVPair** %e.i.i, align 8, !dbg !675
  store %struct._AVPair* %127, %struct._AVPair** %e.i, align 8, !dbg !663
  %128 = load %struct._AVPair** %e.i, align 8, !dbg !676
  %129 = load %struct._Log** %2, align 8, !dbg !676
  %130 = getelementptr inbounds %struct._Log* %129, i32 0, i32 3, !dbg !676
  store %struct._AVPair* %128, %struct._AVPair** %130, align 8, !dbg !676
  br label %RecordStore.exit, !dbg !677

RecordStore.exit:                                 ; preds = %79, %ExtendList.exit.i
  %131 = load %struct._AVPair** %e.i, align 8, !dbg !678
  %132 = load %struct._Log** %2, align 8, !dbg !678
  %133 = getelementptr inbounds %struct._Log* %132, i32 0, i32 2, !dbg !678
  store %struct._AVPair* %131, %struct._AVPair** %133, align 8, !dbg !678
  %134 = load %struct._AVPair** %e.i, align 8, !dbg !679
  %135 = getelementptr inbounds %struct._AVPair* %134, i32 0, i32 0, !dbg !679
  %136 = load %struct._AVPair** %135, align 8, !dbg !679
  %137 = load %struct._Log** %2, align 8, !dbg !679
  %138 = getelementptr inbounds %struct._Log* %137, i32 0, i32 1, !dbg !679
  store %struct._AVPair* %136, %struct._AVPair** %138, align 8, !dbg !679
  %139 = load i64** %3, align 8, !dbg !680
  %140 = load %struct._AVPair** %e.i, align 8, !dbg !680
  %141 = getelementptr inbounds %struct._AVPair* %140, i32 0, i32 2, !dbg !680
  store i64* %139, i64** %141, align 8, !dbg !680
  %142 = load i64* %4, align 8, !dbg !681
  %143 = load %struct._AVPair** %e.i, align 8, !dbg !681
  %144 = getelementptr inbounds %struct._AVPair* %143, i32 0, i32 3, !dbg !681
  store i64 %142, i64* %144, align 8, !dbg !681
  %145 = load i64** %5, align 8, !dbg !682
  %146 = load %struct._AVPair** %e.i, align 8, !dbg !682
  %147 = getelementptr inbounds %struct._AVPair* %146, i32 0, i32 4, !dbg !682
  store i64* %145, i64** %147, align 8, !dbg !682
  %148 = load i64* %6, align 8, !dbg !683
  %149 = load %struct._AVPair** %e.i, align 8, !dbg !683
  %150 = getelementptr inbounds %struct._AVPair* %149, i32 0, i32 5, !dbg !683
  store i64 %148, i64* %150, align 8, !dbg !683
  %151 = load %struct._AVPair** %e.i, align 8, !dbg !684
  store %struct._AVPair* %151, %struct._AVPair** %e, align 8, !dbg !647
  %152 = load i64* %cv, align 8, !dbg !685
  %153 = load %struct._Thread** %10, align 8, !dbg !685
  %154 = getelementptr inbounds %struct._Thread* %153, i32 0, i32 7, !dbg !685
  %155 = load i64* %154, align 8, !dbg !685
  %156 = icmp ugt i64 %152, %155, !dbg !685
  br i1 %156, label %157, label %161, !dbg !685

; <label>:157                                     ; preds = %RecordStore.exit
  %158 = load i64* %cv, align 8, !dbg !686
  %159 = load %struct._Thread** %10, align 8, !dbg !686
  %160 = getelementptr inbounds %struct._Thread* %159, i32 0, i32 7, !dbg !686
  store i64 %158, i64* %160, align 8, !dbg !686
  br label %161, !dbg !688

; <label>:161                                     ; preds = %157, %RecordStore.exit
  %162 = load %struct._AVPair** %e, align 8, !dbg !689
  %163 = ptrtoint %struct._AVPair* %162 to i64, !dbg !689
  %164 = or i64 %163, 1, !dbg !689
  %165 = load i64** %LockFor, align 8, !dbg !689
  store volatile i64 %164, i64* %165, align 8, !dbg !689
  %166 = load i64* %12, align 8, !dbg !690
  %167 = load i64** %11, align 8, !dbg !690
  store volatile i64 %166, i64* %167, align 8, !dbg !690
  ret void, !dbg !691
}

; Function Attrs: nounwind uwtable
define i64 @TxLoad(%struct._Thread* %Self, i64* %Addr) #0 {
  %1 = alloca %struct._AVPair*, align 8
  %e.i1.i = alloca %struct._AVPair*, align 8
  %2 = alloca i64, align 8
  %3 = alloca %struct._Thread*, align 8
  %rv.i.i = alloca i64, align 8
  %rd.i.i = alloca %struct._Log*, align 8
  %EndOfList.i.i = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %v.i.i = alloca i64, align 8
  %p.i.i = alloca %struct._AVPair*, align 8
  %4 = alloca i32, align 4
  %5 = alloca %struct._Thread*, align 8
  %6 = alloca i64*, align 8
  %k.i = alloca %struct._Log*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %7 = alloca i64, align 8
  %8 = alloca %struct._Thread*, align 8
  %9 = alloca i64*, align 8
  %Valu = alloca i64, align 8
  %LockFor = alloca i64*, align 8
  %cv = alloca i64, align 8
  %rdv = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %8, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %8}, metadata !692), !dbg !693
  store i64* %Addr, i64** %9, align 8
  call void @llvm.dbg.declare(metadata !{i64** %9}, metadata !694), !dbg !693
  call void @llvm.dbg.declare(metadata !{i64* %Valu}, metadata !695), !dbg !696
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !697), !dbg !698
  %10 = load i64** %9, align 8, !dbg !698
  %11 = ptrtoint i64* %10 to i64, !dbg !698
  %12 = add i64 %11, 128, !dbg !698
  %13 = lshr i64 %12, 3, !dbg !698
  %14 = and i64 %13, 1048575, !dbg !698
  %15 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %14, !dbg !698
  store i64* %15, i64** %LockFor, align 8, !dbg !698
  call void @llvm.dbg.declare(metadata !{i64* %cv}, metadata !699), !dbg !700
  %16 = load i64** %LockFor, align 8, !dbg !700
  %17 = load volatile i64* %16, align 8, !dbg !700
  store i64 %17, i64* %cv, align 8, !dbg !700
  call void @llvm.dbg.declare(metadata !{i64* %rdv}, metadata !701), !dbg !702
  %18 = load i64* %cv, align 8, !dbg !702
  %19 = and i64 %18, -2, !dbg !702
  store i64 %19, i64* %rdv, align 8, !dbg !702
  %20 = load i64** %9, align 8, !dbg !703
  %21 = load volatile i64* %20, align 8, !dbg !703
  store i64 %21, i64* %Valu, align 8, !dbg !703
  %22 = load i64* %rdv, align 8, !dbg !704
  %23 = load %struct._Thread** %8, align 8, !dbg !704
  %24 = getelementptr inbounds %struct._Thread* %23, i32 0, i32 4, !dbg !704
  %25 = load volatile i64* %24, align 8, !dbg !704
  %26 = icmp ule i64 %22, %25, !dbg !704
  br i1 %26, label %27, label %32, !dbg !704

; <label>:27                                      ; preds = %0
  %28 = load i64** %LockFor, align 8, !dbg !704
  %29 = load volatile i64* %28, align 8, !dbg !704
  %30 = load i64* %rdv, align 8, !dbg !704
  %31 = icmp eq i64 %29, %30, !dbg !704
  br i1 %31, label %43, label %32, !dbg !704

; <label>:32                                      ; preds = %27, %0
  %33 = load i64* %cv, align 8, !dbg !704
  %34 = and i64 %33, 1, !dbg !704
  %35 = icmp ne i64 %34, 0, !dbg !704
  br i1 %35, label %36, label %172, !dbg !704

; <label>:36                                      ; preds = %32
  %37 = load i64* %rdv, align 8, !dbg !704
  %38 = inttoptr i64 %37 to %struct._AVPair*, !dbg !704
  %39 = getelementptr inbounds %struct._AVPair* %38, i32 0, i32 6, !dbg !704
  %40 = load %struct._Thread** %39, align 8, !dbg !704
  %41 = load %struct._Thread** %8, align 8, !dbg !704
  %42 = icmp eq %struct._Thread* %40, %41, !dbg !704
  br i1 %42, label %43, label %172, !dbg !704

; <label>:43                                      ; preds = %36, %27
  %44 = load %struct._Thread** %8, align 8, !dbg !705
  %45 = getelementptr inbounds %struct._Thread* %44, i32 0, i32 10, !dbg !705
  %46 = load i32* %45, align 4, !dbg !705
  %47 = icmp ne i32 %46, 0, !dbg !705
  br i1 %47, label %170, label %48, !dbg !705

; <label>:48                                      ; preds = %43
  %49 = load %struct._Thread** %8, align 8, !dbg !707
  %50 = load i64** %LockFor, align 8, !dbg !707
  store %struct._Thread* %49, %struct._Thread** %5, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %5}, metadata !709) #6, !dbg !710
  store i64* %50, i64** %6, align 8
  call void @llvm.dbg.declare(metadata !{i64** %6}, metadata !711) #6, !dbg !710
  call void @llvm.dbg.declare(metadata !{%struct._Log** %k.i}, metadata !712) #6, !dbg !713
  %51 = load %struct._Thread** %5, align 8, !dbg !713
  %52 = getelementptr inbounds %struct._Thread* %51, i32 0, i32 18, !dbg !713
  store %struct._Log* %52, %struct._Log** %k.i, align 8, !dbg !713
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !714) #6, !dbg !715
  %53 = load %struct._Log** %k.i, align 8, !dbg !715
  %54 = getelementptr inbounds %struct._Log* %53, i32 0, i32 1, !dbg !715
  %55 = load %struct._AVPair** %54, align 8, !dbg !715
  store %struct._AVPair* %55, %struct._AVPair** %e.i, align 8, !dbg !715
  %56 = load %struct._AVPair** %e.i, align 8, !dbg !716
  %57 = icmp eq %struct._AVPair* %56, null, !dbg !716
  br i1 %57, label %58, label %153, !dbg !716

; <label>:58                                      ; preds = %48
  %59 = load %struct._Thread** %5, align 8, !dbg !717
  store %struct._Thread* %59, %struct._Thread** %3, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %3}, metadata !719) #6, !dbg !720
  call void @llvm.dbg.declare(metadata !{i64* %rv.i.i}, metadata !721) #6, !dbg !722
  %60 = load %struct._Thread** %3, align 8, !dbg !722
  %61 = getelementptr inbounds %struct._Thread* %60, i32 0, i32 4, !dbg !722
  %62 = load volatile i64* %61, align 8, !dbg !722
  store i64 %62, i64* %rv.i.i, align 8, !dbg !722
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i.i}, metadata !723) #6, !dbg !725
  %63 = load %struct._Thread** %3, align 8, !dbg !725
  %64 = getelementptr inbounds %struct._Thread* %63, i32 0, i32 18, !dbg !725
  store %struct._Log* %64, %struct._Log** %rd.i.i, align 8, !dbg !725
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList.i.i}, metadata !726) #6, !dbg !727
  %65 = load %struct._Log** %rd.i.i, align 8, !dbg !727
  %66 = getelementptr inbounds %struct._Log* %65, i32 0, i32 1, !dbg !727
  %67 = load %struct._AVPair** %66, align 8, !dbg !727
  store %struct._AVPair* %67, %struct._AVPair** %EndOfList.i.i, align 8, !dbg !727
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !728) #6, !dbg !729
  %68 = load %struct._Log** %rd.i.i, align 8, !dbg !730
  %69 = getelementptr inbounds %struct._Log* %68, i32 0, i32 0, !dbg !730
  %70 = load %struct._AVPair** %69, align 8, !dbg !730
  store %struct._AVPair* %70, %struct._AVPair** %e.i.i, align 8, !dbg !730
  br label %71, !dbg !730

; <label>:71                                      ; preds = %107, %58
  %72 = load %struct._AVPair** %e.i.i, align 8, !dbg !730
  %73 = load %struct._AVPair** %EndOfList.i.i, align 8, !dbg !730
  %74 = icmp ne %struct._AVPair* %72, %73, !dbg !730
  br i1 %74, label %75, label %111, !dbg !730

; <label>:75                                      ; preds = %71
  call void @llvm.dbg.declare(metadata !{i64* %v.i.i}, metadata !732) #6, !dbg !734
  %76 = load %struct._AVPair** %e.i.i, align 8, !dbg !734
  %77 = getelementptr inbounds %struct._AVPair* %76, i32 0, i32 4, !dbg !734
  %78 = load i64** %77, align 8, !dbg !734
  %79 = load volatile i64* %78, align 8, !dbg !734
  store i64 %79, i64* %v.i.i, align 8, !dbg !734
  %80 = load i64* %v.i.i, align 8, !dbg !735
  %81 = and i64 %80, 1, !dbg !735
  %82 = icmp ne i64 %81, 0, !dbg !735
  br i1 %82, label %83, label %101, !dbg !735

; <label>:83                                      ; preds = %75
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i.i}, metadata !736) #6, !dbg !738
  %84 = load i64* %v.i.i, align 8, !dbg !738
  %85 = and i64 %84, -2, !dbg !738
  %86 = inttoptr i64 %85 to %struct._AVPair*, !dbg !738
  store %struct._AVPair* %86, %struct._AVPair** %p.i.i, align 8, !dbg !738
  %87 = load %struct._AVPair** %p.i.i, align 8, !dbg !739
  %88 = getelementptr inbounds %struct._AVPair* %87, i32 0, i32 6, !dbg !739
  %89 = load %struct._Thread** %88, align 8, !dbg !739
  %90 = load %struct._Thread** %3, align 8, !dbg !739
  %91 = icmp eq %struct._Thread* %89, %90, !dbg !739
  br i1 %91, label %92, label %100, !dbg !739

; <label>:92                                      ; preds = %83
  %93 = load %struct._AVPair** %p.i.i, align 8, !dbg !740
  %94 = getelementptr inbounds %struct._AVPair* %93, i32 0, i32 5, !dbg !740
  %95 = load i64* %94, align 8, !dbg !740
  %96 = load i64* %rv.i.i, align 8, !dbg !740
  %97 = icmp ugt i64 %95, %96, !dbg !740
  br i1 %97, label %98, label %99, !dbg !740

; <label>:98                                      ; preds = %92
  store i64 0, i64* %2, !dbg !742
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !742

; <label>:99                                      ; preds = %92
  br label %107, !dbg !744

; <label>:100                                     ; preds = %83
  store i64 0, i64* %2, !dbg !745
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !745

; <label>:101                                     ; preds = %75
  %102 = load i64* %v.i.i, align 8, !dbg !747
  %103 = load i64* %rv.i.i, align 8, !dbg !747
  %104 = icmp ugt i64 %102, %103, !dbg !747
  br i1 %104, label %105, label %106, !dbg !747

; <label>:105                                     ; preds = %101
  store i64 0, i64* %2, !dbg !749
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !749

; <label>:106                                     ; preds = %101
  br label %107

; <label>:107                                     ; preds = %106, %99
  %108 = load %struct._AVPair** %e.i.i, align 8, !dbg !730
  %109 = getelementptr inbounds %struct._AVPair* %108, i32 0, i32 0, !dbg !730
  %110 = load %struct._AVPair** %109, align 8, !dbg !730
  store %struct._AVPair* %110, %struct._AVPair** %e.i.i, align 8, !dbg !730
  br label %71, !dbg !730

; <label>:111                                     ; preds = %71
  store i64 1, i64* %2, !dbg !751
  br label %ReadSetCoherentPessimistic.exit.i, !dbg !751

ReadSetCoherentPessimistic.exit.i:                ; preds = %111, %105, %100, %98
  %112 = load i64* %2, !dbg !751
  %113 = icmp ne i64 %112, 0, !dbg !717
  br i1 %113, label %115, label %114, !dbg !717

; <label>:114                                     ; preds = %ReadSetCoherentPessimistic.exit.i
  store i32 0, i32* %4, !dbg !752
  br label %TrackLoad.exit, !dbg !752

; <label>:115                                     ; preds = %ReadSetCoherentPessimistic.exit.i
  %116 = load %struct._Log** %k.i, align 8, !dbg !754
  %117 = getelementptr inbounds %struct._Log* %116, i32 0, i32 4, !dbg !754
  %118 = load i64* %117, align 8, !dbg !754
  %119 = add nsw i64 %118, 1, !dbg !754
  store i64 %119, i64* %117, align 8, !dbg !754
  %120 = load %struct._Log** %k.i, align 8, !dbg !755
  %121 = getelementptr inbounds %struct._Log* %120, i32 0, i32 2, !dbg !755
  %122 = load %struct._AVPair** %121, align 8, !dbg !755
  store %struct._AVPair* %122, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !664) #6, !dbg !756
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i1.i}, metadata !666) #6, !dbg !757
  %123 = call noalias i8* @malloc(i64 64) #6, !dbg !757
  %124 = bitcast i8* %123 to %struct._AVPair*, !dbg !757
  store %struct._AVPair* %124, %struct._AVPair** %e.i1.i, align 8, !dbg !757
  %125 = load %struct._AVPair** %e.i1.i, align 8, !dbg !758
  %126 = icmp ne %struct._AVPair* %125, null, !dbg !758
  br i1 %126, label %ExtendList.exit.i, label %127, !dbg !758

; <label>:127                                     ; preds = %115
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !758
  unreachable, !dbg !758

ExtendList.exit.i:                                ; preds = %115
  %128 = load %struct._AVPair** %e.i1.i, align 8, !dbg !759
  %129 = bitcast %struct._AVPair* %128 to i8*, !dbg !759
  call void @llvm.memset.p0i8.i64(i8* %129, i8 0, i64 64, i32 8, i1 false) #6, !dbg !759
  %130 = load %struct._AVPair** %e.i1.i, align 8, !dbg !760
  %131 = load %struct._AVPair** %1, align 8, !dbg !760
  %132 = getelementptr inbounds %struct._AVPair* %131, i32 0, i32 0, !dbg !760
  store %struct._AVPair* %130, %struct._AVPair** %132, align 8, !dbg !760
  %133 = load %struct._AVPair** %1, align 8, !dbg !761
  %134 = load %struct._AVPair** %e.i1.i, align 8, !dbg !761
  %135 = getelementptr inbounds %struct._AVPair* %134, i32 0, i32 1, !dbg !761
  store %struct._AVPair* %133, %struct._AVPair** %135, align 8, !dbg !761
  %136 = load %struct._AVPair** %e.i1.i, align 8, !dbg !762
  %137 = getelementptr inbounds %struct._AVPair* %136, i32 0, i32 0, !dbg !762
  store %struct._AVPair* null, %struct._AVPair** %137, align 8, !dbg !762
  %138 = load %struct._AVPair** %1, align 8, !dbg !763
  %139 = getelementptr inbounds %struct._AVPair* %138, i32 0, i32 6, !dbg !763
  %140 = load %struct._Thread** %139, align 8, !dbg !763
  %141 = load %struct._AVPair** %e.i1.i, align 8, !dbg !763
  %142 = getelementptr inbounds %struct._AVPair* %141, i32 0, i32 6, !dbg !763
  store %struct._Thread* %140, %struct._Thread** %142, align 8, !dbg !763
  %143 = load %struct._AVPair** %1, align 8, !dbg !764
  %144 = getelementptr inbounds %struct._AVPair* %143, i32 0, i32 7, !dbg !764
  %145 = load i64* %144, align 8, !dbg !764
  %146 = add nsw i64 %145, 1, !dbg !764
  %147 = load %struct._AVPair** %e.i1.i, align 8, !dbg !764
  %148 = getelementptr inbounds %struct._AVPair* %147, i32 0, i32 7, !dbg !764
  store i64 %146, i64* %148, align 8, !dbg !764
  %149 = load %struct._AVPair** %e.i1.i, align 8, !dbg !765
  store %struct._AVPair* %149, %struct._AVPair** %e.i, align 8, !dbg !755
  %150 = load %struct._AVPair** %e.i, align 8, !dbg !766
  %151 = load %struct._Log** %k.i, align 8, !dbg !766
  %152 = getelementptr inbounds %struct._Log* %151, i32 0, i32 3, !dbg !766
  store %struct._AVPair* %150, %struct._AVPair** %152, align 8, !dbg !766
  br label %153, !dbg !767

; <label>:153                                     ; preds = %ExtendList.exit.i, %48
  %154 = load %struct._AVPair** %e.i, align 8, !dbg !768
  %155 = load %struct._Log** %k.i, align 8, !dbg !768
  %156 = getelementptr inbounds %struct._Log* %155, i32 0, i32 2, !dbg !768
  store %struct._AVPair* %154, %struct._AVPair** %156, align 8, !dbg !768
  %157 = load %struct._AVPair** %e.i, align 8, !dbg !769
  %158 = getelementptr inbounds %struct._AVPair* %157, i32 0, i32 0, !dbg !769
  %159 = load %struct._AVPair** %158, align 8, !dbg !769
  %160 = load %struct._Log** %k.i, align 8, !dbg !769
  %161 = getelementptr inbounds %struct._Log* %160, i32 0, i32 1, !dbg !769
  store %struct._AVPair* %159, %struct._AVPair** %161, align 8, !dbg !769
  %162 = load i64** %6, align 8, !dbg !770
  %163 = load %struct._AVPair** %e.i, align 8, !dbg !770
  %164 = getelementptr inbounds %struct._AVPair* %163, i32 0, i32 4, !dbg !770
  store i64* %162, i64** %164, align 8, !dbg !770
  store i32 1, i32* %4, !dbg !771
  br label %TrackLoad.exit, !dbg !771

TrackLoad.exit:                                   ; preds = %114, %153
  %165 = load i32* %4, !dbg !771
  %166 = icmp ne i32 %165, 0, !dbg !707
  br i1 %166, label %169, label %167, !dbg !707

; <label>:167                                     ; preds = %TrackLoad.exit
  %168 = load %struct._Thread** %8, align 8, !dbg !772
  call void @TxAbort(%struct._Thread* %168), !dbg !772
  br label %169, !dbg !774

; <label>:169                                     ; preds = %167, %TrackLoad.exit
  br label %170, !dbg !775

; <label>:170                                     ; preds = %169, %43
  %171 = load i64* %Valu, align 8, !dbg !776
  store i64 %171, i64* %7, !dbg !776
  br label %177, !dbg !776

; <label>:172                                     ; preds = %36, %32
  %173 = load i64* %rdv, align 8, !dbg !777
  %174 = load %struct._Thread** %8, align 8, !dbg !777
  %175 = getelementptr inbounds %struct._Thread* %174, i32 0, i32 6, !dbg !777
  store i64 %173, i64* %175, align 8, !dbg !777
  %176 = load %struct._Thread** %8, align 8, !dbg !778
  call void @TxAbort(%struct._Thread* %176), !dbg !778
  store i64 0, i64* %7, !dbg !779
  br label %177, !dbg !779

; <label>:177                                     ; preds = %172, %170
  %178 = load i64* %7, !dbg !780
  ret i64 %178, !dbg !780
}

; Function Attrs: nounwind uwtable
define void @TxStoreLocal(%struct._Thread* %Self, i64* %Addr, i64 %Valu) #0 {
  %1 = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Log*, align 8
  %3 = alloca i64*, align 8
  %4 = alloca i64, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %5 = alloca %struct._Thread*, align 8
  %6 = alloca i64*, align 8
  %7 = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %5, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %5}, metadata !781), !dbg !782
  store i64* %Addr, i64** %6, align 8
  call void @llvm.dbg.declare(metadata !{i64** %6}, metadata !783), !dbg !782
  store i64 %Valu, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !784), !dbg !782
  %8 = load %struct._Thread** %5, align 8, !dbg !785
  %9 = getelementptr inbounds %struct._Thread* %8, i32 0, i32 20, !dbg !785
  %10 = load i64** %6, align 8, !dbg !785
  %11 = load i64** %6, align 8, !dbg !785
  %12 = load volatile i64* %11, align 8, !dbg !785
  store %struct._Log* %9, %struct._Log** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %2}, metadata !786) #6, !dbg !787
  store i64* %10, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !788) #6, !dbg !787
  store i64 %12, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !789) #6, !dbg !787
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !790) #6, !dbg !791
  %13 = load %struct._Log** %2, align 8, !dbg !791
  %14 = getelementptr inbounds %struct._Log* %13, i32 0, i32 1, !dbg !791
  %15 = load %struct._AVPair** %14, align 8, !dbg !791
  store %struct._AVPair* %15, %struct._AVPair** %e.i, align 8, !dbg !791
  %16 = load %struct._AVPair** %e.i, align 8, !dbg !792
  %17 = icmp eq %struct._AVPair* %16, null, !dbg !792
  br i1 %17, label %18, label %SaveForRollBack.exit, !dbg !792

; <label>:18                                      ; preds = %0
  %19 = load %struct._Log** %2, align 8, !dbg !793
  %20 = getelementptr inbounds %struct._Log* %19, i32 0, i32 4, !dbg !793
  %21 = load i64* %20, align 8, !dbg !793
  %22 = add nsw i64 %21, 1, !dbg !793
  store i64 %22, i64* %20, align 8, !dbg !793
  %23 = load %struct._Log** %2, align 8, !dbg !795
  %24 = getelementptr inbounds %struct._Log* %23, i32 0, i32 2, !dbg !795
  %25 = load %struct._AVPair** %24, align 8, !dbg !795
  store %struct._AVPair* %25, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !664) #6, !dbg !796
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !666) #6, !dbg !797
  %26 = call noalias i8* @malloc(i64 64) #6, !dbg !797
  %27 = bitcast i8* %26 to %struct._AVPair*, !dbg !797
  store %struct._AVPair* %27, %struct._AVPair** %e.i.i, align 8, !dbg !797
  %28 = load %struct._AVPair** %e.i.i, align 8, !dbg !798
  %29 = icmp ne %struct._AVPair* %28, null, !dbg !798
  br i1 %29, label %ExtendList.exit.i, label %30, !dbg !798

; <label>:30                                      ; preds = %18
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #7, !dbg !798
  unreachable, !dbg !798

ExtendList.exit.i:                                ; preds = %18
  %31 = load %struct._AVPair** %e.i.i, align 8, !dbg !799
  %32 = bitcast %struct._AVPair* %31 to i8*, !dbg !799
  call void @llvm.memset.p0i8.i64(i8* %32, i8 0, i64 64, i32 8, i1 false) #6, !dbg !799
  %33 = load %struct._AVPair** %e.i.i, align 8, !dbg !800
  %34 = load %struct._AVPair** %1, align 8, !dbg !800
  %35 = getelementptr inbounds %struct._AVPair* %34, i32 0, i32 0, !dbg !800
  store %struct._AVPair* %33, %struct._AVPair** %35, align 8, !dbg !800
  %36 = load %struct._AVPair** %1, align 8, !dbg !801
  %37 = load %struct._AVPair** %e.i.i, align 8, !dbg !801
  %38 = getelementptr inbounds %struct._AVPair* %37, i32 0, i32 1, !dbg !801
  store %struct._AVPair* %36, %struct._AVPair** %38, align 8, !dbg !801
  %39 = load %struct._AVPair** %e.i.i, align 8, !dbg !802
  %40 = getelementptr inbounds %struct._AVPair* %39, i32 0, i32 0, !dbg !802
  store %struct._AVPair* null, %struct._AVPair** %40, align 8, !dbg !802
  %41 = load %struct._AVPair** %1, align 8, !dbg !803
  %42 = getelementptr inbounds %struct._AVPair* %41, i32 0, i32 6, !dbg !803
  %43 = load %struct._Thread** %42, align 8, !dbg !803
  %44 = load %struct._AVPair** %e.i.i, align 8, !dbg !803
  %45 = getelementptr inbounds %struct._AVPair* %44, i32 0, i32 6, !dbg !803
  store %struct._Thread* %43, %struct._Thread** %45, align 8, !dbg !803
  %46 = load %struct._AVPair** %1, align 8, !dbg !804
  %47 = getelementptr inbounds %struct._AVPair* %46, i32 0, i32 7, !dbg !804
  %48 = load i64* %47, align 8, !dbg !804
  %49 = add nsw i64 %48, 1, !dbg !804
  %50 = load %struct._AVPair** %e.i.i, align 8, !dbg !804
  %51 = getelementptr inbounds %struct._AVPair* %50, i32 0, i32 7, !dbg !804
  store i64 %49, i64* %51, align 8, !dbg !804
  %52 = load %struct._AVPair** %e.i.i, align 8, !dbg !805
  store %struct._AVPair* %52, %struct._AVPair** %e.i, align 8, !dbg !795
  %53 = load %struct._AVPair** %e.i, align 8, !dbg !806
  %54 = load %struct._Log** %2, align 8, !dbg !806
  %55 = getelementptr inbounds %struct._Log* %54, i32 0, i32 3, !dbg !806
  store %struct._AVPair* %53, %struct._AVPair** %55, align 8, !dbg !806
  br label %SaveForRollBack.exit, !dbg !807

SaveForRollBack.exit:                             ; preds = %0, %ExtendList.exit.i
  %56 = load %struct._AVPair** %e.i, align 8, !dbg !808
  %57 = load %struct._Log** %2, align 8, !dbg !808
  %58 = getelementptr inbounds %struct._Log* %57, i32 0, i32 2, !dbg !808
  store %struct._AVPair* %56, %struct._AVPair** %58, align 8, !dbg !808
  %59 = load %struct._AVPair** %e.i, align 8, !dbg !809
  %60 = getelementptr inbounds %struct._AVPair* %59, i32 0, i32 0, !dbg !809
  %61 = load %struct._AVPair** %60, align 8, !dbg !809
  %62 = load %struct._Log** %2, align 8, !dbg !809
  %63 = getelementptr inbounds %struct._Log* %62, i32 0, i32 1, !dbg !809
  store %struct._AVPair* %61, %struct._AVPair** %63, align 8, !dbg !809
  %64 = load i64** %3, align 8, !dbg !810
  %65 = load %struct._AVPair** %e.i, align 8, !dbg !810
  %66 = getelementptr inbounds %struct._AVPair* %65, i32 0, i32 2, !dbg !810
  store i64* %64, i64** %66, align 8, !dbg !810
  %67 = load i64* %4, align 8, !dbg !811
  %68 = load %struct._AVPair** %e.i, align 8, !dbg !811
  %69 = getelementptr inbounds %struct._AVPair* %68, i32 0, i32 3, !dbg !811
  store i64 %67, i64* %69, align 8, !dbg !811
  %70 = load %struct._AVPair** %e.i, align 8, !dbg !812
  %71 = getelementptr inbounds %struct._AVPair* %70, i32 0, i32 4, !dbg !812
  store i64* null, i64** %71, align 8, !dbg !812
  %72 = load i64* %7, align 8, !dbg !813
  %73 = load i64** %6, align 8, !dbg !813
  store volatile i64 %72, i64* %73, align 8, !dbg !813
  ret void, !dbg !814
}

; Function Attrs: nounwind uwtable
define void @TxStart(%struct._Thread* %Self, [1 x %struct.__jmp_buf_tag]* %envPtr, i32* %ROFlag) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca %struct._Thread*, align 8
  %3 = alloca %struct._Thread*, align 8
  %4 = alloca [1 x %struct.__jmp_buf_tag]*, align 8
  %5 = alloca i32*, align 8
  store %struct._Thread* %Self, %struct._Thread** %3, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %3}, metadata !815), !dbg !816
  store [1 x %struct.__jmp_buf_tag]* %envPtr, [1 x %struct.__jmp_buf_tag]** %4, align 8
  call void @llvm.dbg.declare(metadata !{[1 x %struct.__jmp_buf_tag]** %4}, metadata !817), !dbg !816
  store i32* %ROFlag, i32** %5, align 8
  call void @llvm.dbg.declare(metadata !{i32** %5}, metadata !818), !dbg !816
  %6 = load %struct._Thread** %3, align 8, !dbg !819
  store %struct._Thread* %6, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !820), !dbg !821
  %7 = load %struct._Thread** %2, align 8, !dbg !822
  %8 = getelementptr inbounds %struct._Thread* %7, i32 0, i32 1, !dbg !822
  store volatile i64 0, i64* %8, align 8, !dbg !822
  %9 = load %struct._Thread** %2, align 8, !dbg !823
  %10 = getelementptr inbounds %struct._Thread* %9, i32 0, i32 19, !dbg !823
  %11 = getelementptr inbounds %struct._Log* %10, i32 0, i32 0, !dbg !823
  %12 = load %struct._AVPair** %11, align 8, !dbg !823
  %13 = load %struct._Thread** %2, align 8, !dbg !823
  %14 = getelementptr inbounds %struct._Thread* %13, i32 0, i32 19, !dbg !823
  %15 = getelementptr inbounds %struct._Log* %14, i32 0, i32 1, !dbg !823
  store %struct._AVPair* %12, %struct._AVPair** %15, align 8, !dbg !823
  %16 = load %struct._Thread** %2, align 8, !dbg !824
  %17 = getelementptr inbounds %struct._Thread* %16, i32 0, i32 19, !dbg !824
  %18 = getelementptr inbounds %struct._Log* %17, i32 0, i32 2, !dbg !824
  store %struct._AVPair* null, %struct._AVPair** %18, align 8, !dbg !824
  %19 = load %struct._Thread** %2, align 8, !dbg !825
  %20 = getelementptr inbounds %struct._Thread* %19, i32 0, i32 19, !dbg !825
  %21 = getelementptr inbounds %struct._Log* %20, i32 0, i32 5, !dbg !825
  store i32 0, i32* %21, align 4, !dbg !825
  %22 = load %struct._Thread** %2, align 8, !dbg !826
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 18, !dbg !826
  %24 = getelementptr inbounds %struct._Log* %23, i32 0, i32 0, !dbg !826
  %25 = load %struct._AVPair** %24, align 8, !dbg !826
  %26 = load %struct._Thread** %2, align 8, !dbg !826
  %27 = getelementptr inbounds %struct._Thread* %26, i32 0, i32 18, !dbg !826
  %28 = getelementptr inbounds %struct._Log* %27, i32 0, i32 1, !dbg !826
  store %struct._AVPair* %25, %struct._AVPair** %28, align 8, !dbg !826
  %29 = load %struct._Thread** %2, align 8, !dbg !827
  %30 = getelementptr inbounds %struct._Thread* %29, i32 0, i32 18, !dbg !827
  %31 = getelementptr inbounds %struct._Log* %30, i32 0, i32 2, !dbg !827
  store %struct._AVPair* null, %struct._AVPair** %31, align 8, !dbg !827
  %32 = load %struct._Thread** %2, align 8, !dbg !828
  %33 = getelementptr inbounds %struct._Thread* %32, i32 0, i32 20, !dbg !828
  %34 = getelementptr inbounds %struct._Log* %33, i32 0, i32 0, !dbg !828
  %35 = load %struct._AVPair** %34, align 8, !dbg !828
  %36 = load %struct._Thread** %2, align 8, !dbg !828
  %37 = getelementptr inbounds %struct._Thread* %36, i32 0, i32 20, !dbg !828
  %38 = getelementptr inbounds %struct._Log* %37, i32 0, i32 1, !dbg !828
  store %struct._AVPair* %35, %struct._AVPair** %38, align 8, !dbg !828
  %39 = load %struct._Thread** %2, align 8, !dbg !829
  %40 = getelementptr inbounds %struct._Thread* %39, i32 0, i32 20, !dbg !829
  %41 = getelementptr inbounds %struct._Log* %40, i32 0, i32 2, !dbg !829
  store %struct._AVPair* null, %struct._AVPair** %41, align 8, !dbg !829
  %42 = load %struct._Thread** %2, align 8, !dbg !830
  %43 = getelementptr inbounds %struct._Thread* %42, i32 0, i32 2, !dbg !830
  store volatile i64 0, i64* %43, align 8, !dbg !830
  %44 = load %struct._Thread** %2, align 8, !dbg !831
  %45 = getelementptr inbounds %struct._Thread* %44, i32 0, i32 7, !dbg !831
  store i64 0, i64* %45, align 8, !dbg !831
  %46 = load %struct._Thread** %3, align 8, !dbg !832
  store %struct._Thread* %46, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !833), !dbg !834
  %47 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !835
  %48 = load %struct._Thread** %3, align 8, !dbg !832
  %49 = getelementptr inbounds %struct._Thread* %48, i32 0, i32 4, !dbg !832
  store volatile i64 %47, i64* %49, align 8, !dbg !832
  %50 = load %struct._Thread** %3, align 8, !dbg !836
  %51 = getelementptr inbounds %struct._Thread* %50, i32 0, i32 1, !dbg !836
  store volatile i64 1, i64* %51, align 8, !dbg !836
  %52 = load i32** %5, align 8, !dbg !837
  %53 = load %struct._Thread** %3, align 8, !dbg !837
  %54 = getelementptr inbounds %struct._Thread* %53, i32 0, i32 9, !dbg !837
  store i32* %52, i32** %54, align 8, !dbg !837
  %55 = load i32** %5, align 8, !dbg !838
  %56 = icmp ne i32* %55, null, !dbg !838
  br i1 %56, label %57, label %60, !dbg !838

; <label>:57                                      ; preds = %0
  %58 = load i32** %5, align 8, !dbg !838
  %59 = load i32* %58, align 4, !dbg !838
  br label %61, !dbg !838

; <label>:60                                      ; preds = %0
  br label %61, !dbg !838

; <label>:61                                      ; preds = %60, %57
  %62 = phi i32 [ %59, %57 ], [ 0, %60 ], !dbg !838
  %63 = load %struct._Thread** %3, align 8, !dbg !838
  %64 = getelementptr inbounds %struct._Thread* %63, i32 0, i32 10, !dbg !838
  store i32 %62, i32* %64, align 4, !dbg !838
  %65 = load [1 x %struct.__jmp_buf_tag]** %4, align 8, !dbg !839
  %66 = load %struct._Thread** %3, align 8, !dbg !839
  %67 = getelementptr inbounds %struct._Thread* %66, i32 0, i32 21, !dbg !839
  store [1 x %struct.__jmp_buf_tag]* %65, [1 x %struct.__jmp_buf_tag]** %67, align 8, !dbg !839
  %68 = load %struct._Thread** %3, align 8, !dbg !840
  %69 = getelementptr inbounds %struct._Thread* %68, i32 0, i32 11, !dbg !840
  %70 = load i64* %69, align 8, !dbg !840
  %71 = add nsw i64 %70, 1, !dbg !840
  store i64 %71, i64* %69, align 8, !dbg !840
  ret void, !dbg !841
}

; Function Attrs: nounwind uwtable
define i32 @TxCommit(%struct._Thread* %Self) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca %struct._Thread*, align 8
  %3 = alloca %struct._Thread*, align 8
  %4 = alloca i64, align 8
  %wr.i.i = alloca %struct._Log*, align 8
  %p.i1.i = alloca %struct._AVPair*, align 8
  %End.i.i = alloca %struct._AVPair*, align 8
  %5 = alloca i64, align 8
  %6 = alloca %struct._Thread*, align 8
  %dx.i.i = alloca i64, align 8
  %rv.i.i = alloca i64, align 8
  %rd.i.i = alloca %struct._Log*, align 8
  %EndOfList.i.i = alloca %struct._AVPair*, align 8
  %e.i.i = alloca %struct._AVPair*, align 8
  %v.i.i = alloca i64, align 8
  %p.i.i = alloca %struct._AVPair*, align 8
  %7 = alloca i64, align 8
  %8 = alloca i64, align 8
  %9 = alloca i64*, align 8
  %prevVal.i.i.i = alloca i64, align 8
  %10 = alloca %struct._Thread*, align 8
  %11 = alloca i64, align 8
  %gv.i.i = alloca i64, align 8
  %wv.i.i = alloca i64, align 8
  %k.i.i = alloca i64, align 8
  %12 = alloca i64, align 8
  %13 = alloca %struct._Thread*, align 8
  %wv.i = alloca i64, align 8
  %14 = alloca %struct._Thread*, align 8
  %15 = alloca %struct._Thread*, align 8
  %16 = alloca i32, align 4
  %17 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %17, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %17}, metadata !842), !dbg !843
  %18 = load %struct._Thread** %17, align 8, !dbg !844
  %19 = getelementptr inbounds %struct._Thread* %18, i32 0, i32 19, !dbg !844
  %20 = getelementptr inbounds %struct._Log* %19, i32 0, i32 1, !dbg !844
  %21 = load %struct._AVPair** %20, align 8, !dbg !844
  %22 = load %struct._Thread** %17, align 8, !dbg !844
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 19, !dbg !844
  %24 = getelementptr inbounds %struct._Log* %23, i32 0, i32 0, !dbg !844
  %25 = load %struct._AVPair** %24, align 8, !dbg !844
  %26 = icmp eq %struct._AVPair* %21, %25, !dbg !844
  br i1 %26, label %27, label %77, !dbg !844

; <label>:27                                      ; preds = %0
  %28 = load %struct._Thread** %17, align 8, !dbg !845
  store %struct._Thread* %28, %struct._Thread** %15, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %15}, metadata !847), !dbg !848
  %29 = load %struct._Thread** %15, align 8, !dbg !849
  store %struct._Thread* %29, %struct._Thread** %14, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %14}, metadata !820), !dbg !850
  %30 = load %struct._Thread** %14, align 8, !dbg !851
  %31 = getelementptr inbounds %struct._Thread* %30, i32 0, i32 1, !dbg !851
  store volatile i64 0, i64* %31, align 8, !dbg !851
  %32 = load %struct._Thread** %14, align 8, !dbg !852
  %33 = getelementptr inbounds %struct._Thread* %32, i32 0, i32 19, !dbg !852
  %34 = getelementptr inbounds %struct._Log* %33, i32 0, i32 0, !dbg !852
  %35 = load %struct._AVPair** %34, align 8, !dbg !852
  %36 = load %struct._Thread** %14, align 8, !dbg !852
  %37 = getelementptr inbounds %struct._Thread* %36, i32 0, i32 19, !dbg !852
  %38 = getelementptr inbounds %struct._Log* %37, i32 0, i32 1, !dbg !852
  store %struct._AVPair* %35, %struct._AVPair** %38, align 8, !dbg !852
  %39 = load %struct._Thread** %14, align 8, !dbg !853
  %40 = getelementptr inbounds %struct._Thread* %39, i32 0, i32 19, !dbg !853
  %41 = getelementptr inbounds %struct._Log* %40, i32 0, i32 2, !dbg !853
  store %struct._AVPair* null, %struct._AVPair** %41, align 8, !dbg !853
  %42 = load %struct._Thread** %14, align 8, !dbg !854
  %43 = getelementptr inbounds %struct._Thread* %42, i32 0, i32 19, !dbg !854
  %44 = getelementptr inbounds %struct._Log* %43, i32 0, i32 5, !dbg !854
  store i32 0, i32* %44, align 4, !dbg !854
  %45 = load %struct._Thread** %14, align 8, !dbg !855
  %46 = getelementptr inbounds %struct._Thread* %45, i32 0, i32 18, !dbg !855
  %47 = getelementptr inbounds %struct._Log* %46, i32 0, i32 0, !dbg !855
  %48 = load %struct._AVPair** %47, align 8, !dbg !855
  %49 = load %struct._Thread** %14, align 8, !dbg !855
  %50 = getelementptr inbounds %struct._Thread* %49, i32 0, i32 18, !dbg !855
  %51 = getelementptr inbounds %struct._Log* %50, i32 0, i32 1, !dbg !855
  store %struct._AVPair* %48, %struct._AVPair** %51, align 8, !dbg !855
  %52 = load %struct._Thread** %14, align 8, !dbg !856
  %53 = getelementptr inbounds %struct._Thread* %52, i32 0, i32 18, !dbg !856
  %54 = getelementptr inbounds %struct._Log* %53, i32 0, i32 2, !dbg !856
  store %struct._AVPair* null, %struct._AVPair** %54, align 8, !dbg !856
  %55 = load %struct._Thread** %14, align 8, !dbg !857
  %56 = getelementptr inbounds %struct._Thread* %55, i32 0, i32 20, !dbg !857
  %57 = getelementptr inbounds %struct._Log* %56, i32 0, i32 0, !dbg !857
  %58 = load %struct._AVPair** %57, align 8, !dbg !857
  %59 = load %struct._Thread** %14, align 8, !dbg !857
  %60 = getelementptr inbounds %struct._Thread* %59, i32 0, i32 20, !dbg !857
  %61 = getelementptr inbounds %struct._Log* %60, i32 0, i32 1, !dbg !857
  store %struct._AVPair* %58, %struct._AVPair** %61, align 8, !dbg !857
  %62 = load %struct._Thread** %14, align 8, !dbg !858
  %63 = getelementptr inbounds %struct._Thread* %62, i32 0, i32 20, !dbg !858
  %64 = getelementptr inbounds %struct._Log* %63, i32 0, i32 2, !dbg !858
  store %struct._AVPair* null, %struct._AVPair** %64, align 8, !dbg !858
  %65 = load %struct._Thread** %14, align 8, !dbg !859
  %66 = getelementptr inbounds %struct._Thread* %65, i32 0, i32 2, !dbg !859
  store volatile i64 0, i64* %66, align 8, !dbg !859
  %67 = load %struct._Thread** %14, align 8, !dbg !860
  %68 = getelementptr inbounds %struct._Thread* %67, i32 0, i32 7, !dbg !860
  store i64 0, i64* %68, align 8, !dbg !860
  %69 = load %struct._Thread** %15, align 8, !dbg !861
  %70 = getelementptr inbounds %struct._Thread* %69, i32 0, i32 3, !dbg !861
  store volatile i64 0, i64* %70, align 8, !dbg !861
  %71 = load %struct._Thread** %17, align 8, !dbg !862
  %72 = getelementptr inbounds %struct._Thread* %71, i32 0, i32 16, !dbg !862
  %73 = load %struct.tmalloc** %72, align 8, !dbg !862
  call void @tmalloc_clear(%struct.tmalloc* %73), !dbg !862
  %74 = load %struct._Thread** %17, align 8, !dbg !863
  %75 = getelementptr inbounds %struct._Thread* %74, i32 0, i32 17, !dbg !863
  %76 = load %struct.tmalloc** %75, align 8, !dbg !863
  call void @tmalloc_releaseAllForward(%struct.tmalloc* %76, void (i8*, i64)* @txSterilize), !dbg !863
  store i32 1, i32* %16, !dbg !864
  br label %241, !dbg !864

; <label>:77                                      ; preds = %0
  %78 = load %struct._Thread** %17, align 8, !dbg !865
  store %struct._Thread* %78, %struct._Thread** %13, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %13}, metadata !866) #6, !dbg !867
  call void @llvm.dbg.declare(metadata !{i64* %wv.i}, metadata !868) #6, !dbg !869
  %79 = load %struct._Thread** %13, align 8, !dbg !870
  %80 = load %struct._Thread** %13, align 8, !dbg !870
  %81 = getelementptr inbounds %struct._Thread* %80, i32 0, i32 7, !dbg !870
  %82 = load i64* %81, align 8, !dbg !870
  store %struct._Thread* %79, %struct._Thread** %10, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %10}, metadata !871) #6, !dbg !872
  store i64 %82, i64* %11, align 8
  call void @llvm.dbg.declare(metadata !{i64* %11}, metadata !873) #6, !dbg !872
  call void @llvm.dbg.declare(metadata !{i64* %gv.i.i}, metadata !874) #6, !dbg !875
  %83 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !875
  store i64 %83, i64* %gv.i.i, align 8, !dbg !875
  call void @llvm.dbg.declare(metadata !{i64* %wv.i.i}, metadata !876) #6, !dbg !877
  %84 = load i64* %gv.i.i, align 8, !dbg !877
  %85 = add i64 %84, 2, !dbg !877
  store i64 %85, i64* %wv.i.i, align 8, !dbg !877
  call void @llvm.dbg.declare(metadata !{i64* %k.i.i}, metadata !878) #6, !dbg !879
  %86 = load i64* %wv.i.i, align 8, !dbg !880
  %87 = load i64* %gv.i.i, align 8, !dbg !880
  store i64 %86, i64* %7, align 8
  call void @llvm.dbg.declare(metadata !{i64* %7}, metadata !377) #6, !dbg !881
  store i64 %87, i64* %8, align 8
  call void @llvm.dbg.declare(metadata !{i64* %8}, metadata !379) #6, !dbg !881
  store i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), i64** %9, align 8
  call void @llvm.dbg.declare(metadata !{i64** %9}, metadata !380) #6, !dbg !881
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i.i.i}, metadata !381) #6, !dbg !882
  %88 = load i64* %7, align 8, !dbg !883
  %89 = load i64** %9, align 8, !dbg !883
  %90 = load i64* %8, align 8, !dbg !883
  %91 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %88, i64* %89, i64 %90) #6, !dbg !883, !srcloc !385
  store i64 %91, i64* %prevVal.i.i.i, align 8, !dbg !883
  %92 = load i64* %prevVal.i.i.i, align 8, !dbg !884
  store i64 %92, i64* %k.i.i, align 8, !dbg !880
  %93 = load i64* %k.i.i, align 8, !dbg !885
  %94 = load i64* %gv.i.i, align 8, !dbg !885
  %95 = icmp ne i64 %93, %94, !dbg !885
  br i1 %95, label %96, label %GVGenerateWV_GV4.exit.i, !dbg !885

; <label>:96                                      ; preds = %77
  %97 = load i64* %k.i.i, align 8, !dbg !886
  store i64 %97, i64* %wv.i.i, align 8, !dbg !886
  br label %GVGenerateWV_GV4.exit.i, !dbg !888

GVGenerateWV_GV4.exit.i:                          ; preds = %96, %77
  %98 = load i64* %wv.i.i, align 8, !dbg !889
  %99 = load %struct._Thread** %10, align 8, !dbg !889
  %100 = getelementptr inbounds %struct._Thread* %99, i32 0, i32 5, !dbg !889
  store i64 %98, i64* %100, align 8, !dbg !889
  %101 = load i64* %wv.i.i, align 8, !dbg !890
  store i64 %101, i64* %wv.i, align 8, !dbg !870
  %102 = load %struct._Thread** %13, align 8, !dbg !891
  store %struct._Thread* %102, %struct._Thread** %6, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %6}, metadata !892) #6, !dbg !893
  call void @llvm.dbg.declare(metadata !{i64* %dx.i.i}, metadata !894) #6, !dbg !895
  store i64 0, i64* %dx.i.i, align 8, !dbg !895
  call void @llvm.dbg.declare(metadata !{i64* %rv.i.i}, metadata !896) #6, !dbg !897
  %103 = load %struct._Thread** %6, align 8, !dbg !897
  %104 = getelementptr inbounds %struct._Thread* %103, i32 0, i32 4, !dbg !897
  %105 = load volatile i64* %104, align 8, !dbg !897
  store i64 %105, i64* %rv.i.i, align 8, !dbg !897
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i.i}, metadata !898) #6, !dbg !899
  %106 = load %struct._Thread** %6, align 8, !dbg !899
  %107 = getelementptr inbounds %struct._Thread* %106, i32 0, i32 18, !dbg !899
  store %struct._Log* %107, %struct._Log** %rd.i.i, align 8, !dbg !899
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList.i.i}, metadata !900) #6, !dbg !901
  %108 = load %struct._Log** %rd.i.i, align 8, !dbg !901
  %109 = getelementptr inbounds %struct._Log* %108, i32 0, i32 1, !dbg !901
  %110 = load %struct._AVPair** %109, align 8, !dbg !901
  store %struct._AVPair* %110, %struct._AVPair** %EndOfList.i.i, align 8, !dbg !901
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i.i}, metadata !902) #6, !dbg !903
  %111 = load %struct._Log** %rd.i.i, align 8, !dbg !904
  %112 = getelementptr inbounds %struct._Log* %111, i32 0, i32 0, !dbg !904
  %113 = load %struct._AVPair** %112, align 8, !dbg !904
  store %struct._AVPair* %113, %struct._AVPair** %e.i.i, align 8, !dbg !904
  br label %114, !dbg !904

; <label>:114                                     ; preds = %152, %GVGenerateWV_GV4.exit.i
  %115 = load %struct._AVPair** %e.i.i, align 8, !dbg !904
  %116 = load %struct._AVPair** %EndOfList.i.i, align 8, !dbg !904
  %117 = icmp ne %struct._AVPair* %115, %116, !dbg !904
  br i1 %117, label %118, label %156, !dbg !904

; <label>:118                                     ; preds = %114
  call void @llvm.dbg.declare(metadata !{i64* %v.i.i}, metadata !906) #6, !dbg !908
  %119 = load %struct._AVPair** %e.i.i, align 8, !dbg !908
  %120 = getelementptr inbounds %struct._AVPair* %119, i32 0, i32 4, !dbg !908
  %121 = load i64** %120, align 8, !dbg !908
  %122 = load volatile i64* %121, align 8, !dbg !908
  store i64 %122, i64* %v.i.i, align 8, !dbg !908
  %123 = load i64* %v.i.i, align 8, !dbg !909
  %124 = and i64 %123, 1, !dbg !909
  %125 = icmp ne i64 %124, 0, !dbg !909
  br i1 %125, label %126, label %144, !dbg !909

; <label>:126                                     ; preds = %118
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i.i}, metadata !910) #6, !dbg !912
  %127 = load i64* %v.i.i, align 8, !dbg !912
  %128 = and i64 %127, -2, !dbg !912
  %129 = inttoptr i64 %128 to %struct._AVPair*, !dbg !912
  store %struct._AVPair* %129, %struct._AVPair** %p.i.i, align 8, !dbg !912
  %130 = load %struct._AVPair** %p.i.i, align 8, !dbg !913
  %131 = getelementptr inbounds %struct._AVPair* %130, i32 0, i32 6, !dbg !913
  %132 = load %struct._Thread** %131, align 8, !dbg !913
  %133 = load %struct._Thread** %6, align 8, !dbg !913
  %134 = icmp eq %struct._Thread* %132, %133, !dbg !913
  br i1 %134, label %135, label %143, !dbg !913

; <label>:135                                     ; preds = %126
  %136 = load %struct._AVPair** %p.i.i, align 8, !dbg !914
  %137 = getelementptr inbounds %struct._AVPair* %136, i32 0, i32 5, !dbg !914
  %138 = load i64* %137, align 8, !dbg !914
  %139 = load i64* %rv.i.i, align 8, !dbg !914
  %140 = icmp ugt i64 %138, %139, !dbg !914
  br i1 %140, label %141, label %142, !dbg !914

; <label>:141                                     ; preds = %135
  store i64 0, i64* %5, !dbg !916
  br label %ReadSetCoherent.exit.i, !dbg !916

; <label>:142                                     ; preds = %135
  br label %152, !dbg !918

; <label>:143                                     ; preds = %126
  store i64 0, i64* %5, !dbg !919
  br label %ReadSetCoherent.exit.i, !dbg !919

; <label>:144                                     ; preds = %118
  %145 = load i64* %v.i.i, align 8, !dbg !921
  %146 = load i64* %rv.i.i, align 8, !dbg !921
  %147 = icmp ugt i64 %145, %146, !dbg !921
  %148 = zext i1 %147 to i32, !dbg !921
  %149 = sext i32 %148 to i64, !dbg !921
  %150 = load i64* %dx.i.i, align 8, !dbg !921
  %151 = or i64 %150, %149, !dbg !921
  store i64 %151, i64* %dx.i.i, align 8, !dbg !921
  br label %152

; <label>:152                                     ; preds = %144, %142
  %153 = load %struct._AVPair** %e.i.i, align 8, !dbg !904
  %154 = getelementptr inbounds %struct._AVPair* %153, i32 0, i32 0, !dbg !904
  %155 = load %struct._AVPair** %154, align 8, !dbg !904
  store %struct._AVPair* %155, %struct._AVPair** %e.i.i, align 8, !dbg !904
  br label %114, !dbg !904

; <label>:156                                     ; preds = %114
  %157 = load i64* %dx.i.i, align 8, !dbg !923
  %158 = icmp eq i64 %157, 0, !dbg !923
  %159 = zext i1 %158 to i32, !dbg !923
  %160 = sext i32 %159 to i64, !dbg !923
  store i64 %160, i64* %5, !dbg !923
  br label %ReadSetCoherent.exit.i, !dbg !923

ReadSetCoherent.exit.i:                           ; preds = %156, %143, %141
  %161 = load i64* %5, !dbg !924
  %162 = icmp ne i64 %161, 0, !dbg !891
  br i1 %162, label %164, label %163, !dbg !891

; <label>:163                                     ; preds = %ReadSetCoherent.exit.i
  store i64 0, i64* %12, !dbg !925
  br label %TryFastUpdate.exit, !dbg !925

; <label>:164                                     ; preds = %ReadSetCoherent.exit.i
  %165 = load %struct._Thread** %13, align 8, !dbg !927
  %166 = load i64* %wv.i, align 8, !dbg !927
  store %struct._Thread* %165, %struct._Thread** %3, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %3}, metadata !928) #6, !dbg !929
  store i64 %166, i64* %4, align 8
  call void @llvm.dbg.declare(metadata !{i64* %4}, metadata !930) #6, !dbg !929
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr.i.i}, metadata !931) #6, !dbg !932
  %167 = load %struct._Thread** %3, align 8, !dbg !932
  %168 = getelementptr inbounds %struct._Thread* %167, i32 0, i32 19, !dbg !932
  store %struct._Log* %168, %struct._Log** %wr.i.i, align 8, !dbg !932
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i1.i}, metadata !933) #6, !dbg !935
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End.i.i}, metadata !936) #6, !dbg !937
  %169 = load %struct._Log** %wr.i.i, align 8, !dbg !937
  %170 = getelementptr inbounds %struct._Log* %169, i32 0, i32 1, !dbg !937
  %171 = load %struct._AVPair** %170, align 8, !dbg !937
  store %struct._AVPair* %171, %struct._AVPair** %End.i.i, align 8, !dbg !937
  %172 = load %struct._Log** %wr.i.i, align 8, !dbg !938
  %173 = getelementptr inbounds %struct._Log* %172, i32 0, i32 0, !dbg !938
  %174 = load %struct._AVPair** %173, align 8, !dbg !938
  store %struct._AVPair* %174, %struct._AVPair** %p.i1.i, align 8, !dbg !938
  br label %175, !dbg !938

; <label>:175                                     ; preds = %179, %164
  %176 = load %struct._AVPair** %p.i1.i, align 8, !dbg !938
  %177 = load %struct._AVPair** %End.i.i, align 8, !dbg !938
  %178 = icmp ne %struct._AVPair* %176, %177, !dbg !938
  br i1 %178, label %179, label %DropLocks.exit.i, !dbg !938

; <label>:179                                     ; preds = %175
  %180 = load i64* %4, align 8, !dbg !940
  %181 = load %struct._AVPair** %p.i1.i, align 8, !dbg !940
  %182 = getelementptr inbounds %struct._AVPair* %181, i32 0, i32 4, !dbg !940
  %183 = load i64** %182, align 8, !dbg !940
  store volatile i64 %180, i64* %183, align 8, !dbg !940
  %184 = load %struct._AVPair** %p.i1.i, align 8, !dbg !938
  %185 = getelementptr inbounds %struct._AVPair* %184, i32 0, i32 0, !dbg !938
  %186 = load %struct._AVPair** %185, align 8, !dbg !938
  store %struct._AVPair* %186, %struct._AVPair** %p.i1.i, align 8, !dbg !938
  br label %175, !dbg !938

DropLocks.exit.i:                                 ; preds = %175
  call void asm sideeffect "mfence", "~{dirflag},~{fpsr},~{flags}"() #6, !dbg !942, !srcloc !943
  store i64 1, i64* %12, !dbg !944
  br label %TryFastUpdate.exit, !dbg !944

TryFastUpdate.exit:                               ; preds = %163, %DropLocks.exit.i
  %187 = load i64* %12, !dbg !944
  %188 = icmp ne i64 %187, 0, !dbg !865
  br i1 %188, label %189, label %239, !dbg !865

; <label>:189                                     ; preds = %TryFastUpdate.exit
  %190 = load %struct._Thread** %17, align 8, !dbg !945
  store %struct._Thread* %190, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !847), !dbg !947
  %191 = load %struct._Thread** %2, align 8, !dbg !948
  store %struct._Thread* %191, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !820), !dbg !949
  %192 = load %struct._Thread** %1, align 8, !dbg !950
  %193 = getelementptr inbounds %struct._Thread* %192, i32 0, i32 1, !dbg !950
  store volatile i64 0, i64* %193, align 8, !dbg !950
  %194 = load %struct._Thread** %1, align 8, !dbg !951
  %195 = getelementptr inbounds %struct._Thread* %194, i32 0, i32 19, !dbg !951
  %196 = getelementptr inbounds %struct._Log* %195, i32 0, i32 0, !dbg !951
  %197 = load %struct._AVPair** %196, align 8, !dbg !951
  %198 = load %struct._Thread** %1, align 8, !dbg !951
  %199 = getelementptr inbounds %struct._Thread* %198, i32 0, i32 19, !dbg !951
  %200 = getelementptr inbounds %struct._Log* %199, i32 0, i32 1, !dbg !951
  store %struct._AVPair* %197, %struct._AVPair** %200, align 8, !dbg !951
  %201 = load %struct._Thread** %1, align 8, !dbg !952
  %202 = getelementptr inbounds %struct._Thread* %201, i32 0, i32 19, !dbg !952
  %203 = getelementptr inbounds %struct._Log* %202, i32 0, i32 2, !dbg !952
  store %struct._AVPair* null, %struct._AVPair** %203, align 8, !dbg !952
  %204 = load %struct._Thread** %1, align 8, !dbg !953
  %205 = getelementptr inbounds %struct._Thread* %204, i32 0, i32 19, !dbg !953
  %206 = getelementptr inbounds %struct._Log* %205, i32 0, i32 5, !dbg !953
  store i32 0, i32* %206, align 4, !dbg !953
  %207 = load %struct._Thread** %1, align 8, !dbg !954
  %208 = getelementptr inbounds %struct._Thread* %207, i32 0, i32 18, !dbg !954
  %209 = getelementptr inbounds %struct._Log* %208, i32 0, i32 0, !dbg !954
  %210 = load %struct._AVPair** %209, align 8, !dbg !954
  %211 = load %struct._Thread** %1, align 8, !dbg !954
  %212 = getelementptr inbounds %struct._Thread* %211, i32 0, i32 18, !dbg !954
  %213 = getelementptr inbounds %struct._Log* %212, i32 0, i32 1, !dbg !954
  store %struct._AVPair* %210, %struct._AVPair** %213, align 8, !dbg !954
  %214 = load %struct._Thread** %1, align 8, !dbg !955
  %215 = getelementptr inbounds %struct._Thread* %214, i32 0, i32 18, !dbg !955
  %216 = getelementptr inbounds %struct._Log* %215, i32 0, i32 2, !dbg !955
  store %struct._AVPair* null, %struct._AVPair** %216, align 8, !dbg !955
  %217 = load %struct._Thread** %1, align 8, !dbg !956
  %218 = getelementptr inbounds %struct._Thread* %217, i32 0, i32 20, !dbg !956
  %219 = getelementptr inbounds %struct._Log* %218, i32 0, i32 0, !dbg !956
  %220 = load %struct._AVPair** %219, align 8, !dbg !956
  %221 = load %struct._Thread** %1, align 8, !dbg !956
  %222 = getelementptr inbounds %struct._Thread* %221, i32 0, i32 20, !dbg !956
  %223 = getelementptr inbounds %struct._Log* %222, i32 0, i32 1, !dbg !956
  store %struct._AVPair* %220, %struct._AVPair** %223, align 8, !dbg !956
  %224 = load %struct._Thread** %1, align 8, !dbg !957
  %225 = getelementptr inbounds %struct._Thread* %224, i32 0, i32 20, !dbg !957
  %226 = getelementptr inbounds %struct._Log* %225, i32 0, i32 2, !dbg !957
  store %struct._AVPair* null, %struct._AVPair** %226, align 8, !dbg !957
  %227 = load %struct._Thread** %1, align 8, !dbg !958
  %228 = getelementptr inbounds %struct._Thread* %227, i32 0, i32 2, !dbg !958
  store volatile i64 0, i64* %228, align 8, !dbg !958
  %229 = load %struct._Thread** %1, align 8, !dbg !959
  %230 = getelementptr inbounds %struct._Thread* %229, i32 0, i32 7, !dbg !959
  store i64 0, i64* %230, align 8, !dbg !959
  %231 = load %struct._Thread** %2, align 8, !dbg !960
  %232 = getelementptr inbounds %struct._Thread* %231, i32 0, i32 3, !dbg !960
  store volatile i64 0, i64* %232, align 8, !dbg !960
  %233 = load %struct._Thread** %17, align 8, !dbg !961
  %234 = getelementptr inbounds %struct._Thread* %233, i32 0, i32 16, !dbg !961
  %235 = load %struct.tmalloc** %234, align 8, !dbg !961
  call void @tmalloc_clear(%struct.tmalloc* %235), !dbg !961
  %236 = load %struct._Thread** %17, align 8, !dbg !962
  %237 = getelementptr inbounds %struct._Thread* %236, i32 0, i32 17, !dbg !962
  %238 = load %struct.tmalloc** %237, align 8, !dbg !962
  call void @tmalloc_releaseAllForward(%struct.tmalloc* %238, void (i8*, i64)* @txSterilize), !dbg !962
  store i32 1, i32* %16, !dbg !963
  br label %241, !dbg !963

; <label>:239                                     ; preds = %TryFastUpdate.exit
  %240 = load %struct._Thread** %17, align 8, !dbg !964
  call void @TxAbort(%struct._Thread* %240), !dbg !964
  store i32 0, i32* %16, !dbg !965
  br label %241, !dbg !965

; <label>:241                                     ; preds = %239, %189, %27
  %242 = load i32* %16, !dbg !965
  ret i32 %242, !dbg !965
}

declare void @tmalloc_releaseAllForward(%struct.tmalloc*, void (i8*, i64)*) #4

; Function Attrs: nounwind uwtable
define internal void @txSterilize(i8* %Base, i64 %Length) #0 {
  %1 = alloca i64, align 8
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %prevVal.i = alloca i64, align 8
  %4 = alloca i8*, align 8
  %5 = alloca i64, align 8
  %Addr = alloca i64*, align 8
  %End = alloca i64*, align 8
  %Lock = alloca i64*, align 8
  %val = alloca i64, align 8
  store i8* %Base, i8** %4, align 8
  call void @llvm.dbg.declare(metadata !{i8** %4}, metadata !966), !dbg !967
  store i64 %Length, i64* %5, align 8
  call void @llvm.dbg.declare(metadata !{i64* %5}, metadata !968), !dbg !967
  call void @llvm.dbg.declare(metadata !{i64** %Addr}, metadata !969), !dbg !971
  %6 = load i8** %4, align 8, !dbg !971
  %7 = bitcast i8* %6 to i64*, !dbg !971
  store i64* %7, i64** %Addr, align 8, !dbg !971
  call void @llvm.dbg.declare(metadata !{i64** %End}, metadata !972), !dbg !973
  %8 = load i64** %Addr, align 8, !dbg !973
  %9 = load i64* %5, align 8, !dbg !973
  %10 = getelementptr inbounds i64* %8, i64 %9, !dbg !973
  store i64* %10, i64** %End, align 8, !dbg !973
  br label %11, !dbg !974

; <label>:11                                      ; preds = %15, %0
  %12 = load i64** %Addr, align 8, !dbg !974
  %13 = load i64** %End, align 8, !dbg !974
  %14 = icmp ult i64* %12, %13, !dbg !974
  br i1 %14, label %15, label %35, !dbg !974

; <label>:15                                      ; preds = %11
  call void @llvm.dbg.declare(metadata !{i64** %Lock}, metadata !975), !dbg !977
  %16 = load i64** %Addr, align 8, !dbg !977
  %17 = ptrtoint i64* %16 to i64, !dbg !977
  %18 = add i64 %17, 128, !dbg !977
  %19 = lshr i64 %18, 3, !dbg !977
  %20 = and i64 %19, 1048575, !dbg !977
  %21 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %20, !dbg !977
  store i64* %21, i64** %Lock, align 8, !dbg !977
  call void @llvm.dbg.declare(metadata !{i64* %val}, metadata !978), !dbg !979
  %22 = load i64** %Lock, align 8, !dbg !979
  %23 = load volatile i64* %22, align 8, !dbg !979
  store i64 %23, i64* %val, align 8, !dbg !979
  %24 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !980
  %25 = and i64 %24, -2, !dbg !980
  %26 = load i64* %val, align 8, !dbg !980
  %27 = load i64** %Lock, align 8, !dbg !980
  store i64 %25, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !377) #6, !dbg !981
  store i64 %26, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !379) #6, !dbg !981
  store i64* %27, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !380) #6, !dbg !981
  call void @llvm.dbg.declare(metadata !{i64* %prevVal.i}, metadata !381) #6, !dbg !982
  %28 = load i64* %1, align 8, !dbg !983
  %29 = load i64** %3, align 8, !dbg !983
  %30 = load i64* %2, align 8, !dbg !983
  %31 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %28, i64* %29, i64 %30) #6, !dbg !983, !srcloc !385
  store i64 %31, i64* %prevVal.i, align 8, !dbg !983
  %32 = load i64* %prevVal.i, align 8, !dbg !984
  %33 = load i64** %Addr, align 8, !dbg !985
  %34 = getelementptr inbounds i64* %33, i32 1, !dbg !985
  store i64* %34, i64** %Addr, align 8, !dbg !985
  br label %11, !dbg !986

; <label>:35                                      ; preds = %11
  %36 = load i8** %4, align 8, !dbg !987
  %37 = load i64* %5, align 8, !dbg !987
  call void @llvm.memset.p0i8.i64(i8* %36, i8 -1, i64 %37, i32 1, i1 false), !dbg !987
  ret void, !dbg !988
}

; Function Attrs: nounwind uwtable
define i8* @TxAlloc(%struct._Thread* %Self, i64 %size) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64, align 8
  %ptr = alloca i8*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !989), !dbg !990
  store i64 %size, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !991), !dbg !990
  call void @llvm.dbg.declare(metadata !{i8** %ptr}, metadata !992), !dbg !993
  %3 = load i64* %2, align 8, !dbg !993
  %4 = call i8* @tmalloc_reserve(i64 %3), !dbg !993
  store i8* %4, i8** %ptr, align 8, !dbg !993
  %5 = load i8** %ptr, align 8, !dbg !994
  %6 = icmp ne i8* %5, null, !dbg !994
  br i1 %6, label %7, label %13, !dbg !994

; <label>:7                                       ; preds = %0
  %8 = load %struct._Thread** %1, align 8, !dbg !995
  %9 = getelementptr inbounds %struct._Thread* %8, i32 0, i32 16, !dbg !995
  %10 = load %struct.tmalloc** %9, align 8, !dbg !995
  %11 = load i8** %ptr, align 8, !dbg !995
  %12 = call i64 @tmalloc_append(%struct.tmalloc* %10, i8* %11), !dbg !995
  br label %13, !dbg !997

; <label>:13                                      ; preds = %7, %0
  %14 = load i8** %ptr, align 8, !dbg !998
  ret i8* %14, !dbg !998
}

declare i8* @tmalloc_reserve(i64) #4

declare i64 @tmalloc_append(%struct.tmalloc*, i8*) #4

; Function Attrs: nounwind uwtable
define void @TxFree(%struct._Thread* %Self, i8* %ptr) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i8*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !999), !dbg !1000
  store i8* %ptr, i8** %2, align 8
  call void @llvm.dbg.declare(metadata !{i8** %2}, metadata !1001), !dbg !1000
  %3 = load %struct._Thread** %1, align 8, !dbg !1002
  %4 = getelementptr inbounds %struct._Thread* %3, i32 0, i32 17, !dbg !1002
  %5 = load %struct.tmalloc** %4, align 8, !dbg !1002
  %6 = load i8** %2, align 8, !dbg !1002
  %7 = call i64 @tmalloc_append(%struct.tmalloc* %5, i8* %6), !dbg !1002
  %8 = load %struct._Thread** %1, align 8, !dbg !1003
  %9 = load i8** %2, align 8, !dbg !1003
  %10 = bitcast i8* %9 to i64*, !dbg !1003
  call void @TxStore(%struct._Thread* %8, i64* %10, i64 0), !dbg !1003
  ret void, !dbg !1004
}

; Function Attrs: nounwind uwtable
define internal void @restoreUseAfterFreeHandler() #0 {
  %1 = call i32 @sigaction(i32 7, %struct.sigaction* @global_act_oldsigbus, %struct.sigaction* null) #6, !dbg !1005
  %2 = icmp ne i32 %1, 0, !dbg !1005
  br i1 %2, label %3, label %4, !dbg !1005

; <label>:3                                       ; preds = %0
  call void @perror(i8* getelementptr inbounds ([40 x i8]* @.str9, i32 0, i32 0)), !dbg !1006
  call void @exit(i32 1) #7, !dbg !1008
  unreachable, !dbg !1008

; <label>:4                                       ; preds = %0
  %5 = call i32 @sigaction(i32 11, %struct.sigaction* @global_act_oldsigsegv, %struct.sigaction* null) #6, !dbg !1009
  %6 = icmp ne i32 %5, 0, !dbg !1009
  br i1 %6, label %7, label %8, !dbg !1009

; <label>:7                                       ; preds = %4
  call void @perror(i8* getelementptr inbounds ([41 x i8]* @.str10, i32 0, i32 0)), !dbg !1010
  call void @exit(i32 1) #7, !dbg !1012
  unreachable, !dbg !1012

; <label>:8                                       ; preds = %4
  ret void, !dbg !1013
}

; Function Attrs: nounwind
declare i32 @sigaction(i32, %struct.sigaction*, %struct.sigaction*) #3

declare void @perror(i8*) #4

; Function Attrs: noreturn nounwind
declare void @exit(i32) #5

; Function Attrs: nounwind uwtable
define internal void @registerUseAfterFreeHandler() #0 {
  %act = alloca %struct.sigaction, align 8
  call void @llvm.dbg.declare(metadata !{%struct.sigaction* %act}, metadata !1014), !dbg !1015
  %1 = bitcast %struct.sigaction* %act to i8*, !dbg !1016
  call void @llvm.memset.p0i8.i64(i8* %1, i8 -104, i64 0, i32 8, i1 false), !dbg !1016
  %2 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 0, !dbg !1017
  %3 = bitcast %union.anon* %2 to void (i32, %struct.siginfo_t*, i8*)**, !dbg !1017
  store void (i32, %struct.siginfo_t*, i8*)* @useAfterFreeHandler, void (i32, %struct.siginfo_t*, i8*)** %3, align 8, !dbg !1017
  %4 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 1, !dbg !1018
  %5 = call i32 @sigemptyset(%struct.__sigset_t* %4) #6, !dbg !1018
  %6 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 2, !dbg !1019
  store i32 268435460, i32* %6, align 4, !dbg !1019
  %7 = call i32 @sigaction(i32 7, %struct.sigaction* %act, %struct.sigaction* @global_act_oldsigbus) #6, !dbg !1020
  %8 = icmp ne i32 %7, 0, !dbg !1020
  br i1 %8, label %9, label %10, !dbg !1020

; <label>:9                                       ; preds = %0
  call void @perror(i8* getelementptr inbounds ([41 x i8]* @.str11, i32 0, i32 0)), !dbg !1021
  call void @exit(i32 1) #7, !dbg !1023
  unreachable, !dbg !1023

; <label>:10                                      ; preds = %0
  %11 = call i32 @sigaction(i32 11, %struct.sigaction* %act, %struct.sigaction* @global_act_oldsigsegv) #6, !dbg !1024
  %12 = icmp ne i32 %11, 0, !dbg !1024
  br i1 %12, label %13, label %14, !dbg !1024

; <label>:13                                      ; preds = %10
  call void @perror(i8* getelementptr inbounds ([42 x i8]* @.str12, i32 0, i32 0)), !dbg !1025
  call void @exit(i32 1) #7, !dbg !1027
  unreachable, !dbg !1027

; <label>:14                                      ; preds = %10
  ret void, !dbg !1028
}

; Function Attrs: nounwind uwtable
define internal void @useAfterFreeHandler(i32 %signum, %struct.siginfo_t* %siginfo, i8* %context) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %rv.i = alloca i64, align 8
  %rd.i = alloca %struct._Log*, align 8
  %EndOfList.i = alloca %struct._AVPair*, align 8
  %e.i = alloca %struct._AVPair*, align 8
  %v.i = alloca i64, align 8
  %p.i = alloca %struct._AVPair*, align 8
  %3 = alloca i32, align 4
  %4 = alloca %struct.siginfo_t*, align 8
  %5 = alloca i8*, align 8
  %Self = alloca %struct._Thread*, align 8
  store i32 %signum, i32* %3, align 4
  call void @llvm.dbg.declare(metadata !{i32* %3}, metadata !1029), !dbg !1030
  store %struct.siginfo_t* %siginfo, %struct.siginfo_t** %4, align 8
  call void @llvm.dbg.declare(metadata !{%struct.siginfo_t** %4}, metadata !1031), !dbg !1030
  store i8* %context, i8** %5, align 8
  call void @llvm.dbg.declare(metadata !{i8** %5}, metadata !1032), !dbg !1030
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %Self}, metadata !1033), !dbg !1034
  %6 = load i32* @global_key_self, align 4, !dbg !1034
  %7 = call i8* @pthread_getspecific(i32 %6) #6, !dbg !1034
  %8 = bitcast i8* %7 to %struct._Thread*, !dbg !1034
  store %struct._Thread* %8, %struct._Thread** %Self, align 8, !dbg !1034
  %9 = load %struct._Thread** %Self, align 8, !dbg !1035
  %10 = icmp eq %struct._Thread* %9, null, !dbg !1035
  br i1 %10, label %16, label %11, !dbg !1035

; <label>:11                                      ; preds = %0
  %12 = load %struct._Thread** %Self, align 8, !dbg !1035
  %13 = getelementptr inbounds %struct._Thread* %12, i32 0, i32 1, !dbg !1035
  %14 = load volatile i64* %13, align 8, !dbg !1035
  %15 = icmp eq i64 %14, 0, !dbg !1035
  br i1 %15, label %16, label %21, !dbg !1035

; <label>:16                                      ; preds = %11, %0
  %17 = load i32* %3, align 4, !dbg !1036
  call void @psignal(i32 %17, i8* null), !dbg !1036
  %18 = load %struct.siginfo_t** %4, align 8, !dbg !1038
  %19 = getelementptr inbounds %struct.siginfo_t* %18, i32 0, i32 1, !dbg !1038
  %20 = load i32* %19, align 4, !dbg !1038
  call void @exit(i32 %20) #7, !dbg !1038
  unreachable, !dbg !1038

; <label>:21                                      ; preds = %11
  %22 = load %struct._Thread** %Self, align 8, !dbg !1039
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 1, !dbg !1039
  %24 = load volatile i64* %23, align 8, !dbg !1039
  %25 = icmp eq i64 %24, 1, !dbg !1039
  br i1 %25, label %26, label %85, !dbg !1039

; <label>:26                                      ; preds = %21
  %27 = load %struct._Thread** %Self, align 8, !dbg !1040
  store %struct._Thread* %27, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !719), !dbg !1042
  call void @llvm.dbg.declare(metadata !{i64* %rv.i}, metadata !721), !dbg !1043
  %28 = load %struct._Thread** %2, align 8, !dbg !1043
  %29 = getelementptr inbounds %struct._Thread* %28, i32 0, i32 4, !dbg !1043
  %30 = load volatile i64* %29, align 8, !dbg !1043
  store i64 %30, i64* %rv.i, align 8, !dbg !1043
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd.i}, metadata !723), !dbg !1044
  %31 = load %struct._Thread** %2, align 8, !dbg !1044
  %32 = getelementptr inbounds %struct._Thread* %31, i32 0, i32 18, !dbg !1044
  store %struct._Log* %32, %struct._Log** %rd.i, align 8, !dbg !1044
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList.i}, metadata !726), !dbg !1045
  %33 = load %struct._Log** %rd.i, align 8, !dbg !1045
  %34 = getelementptr inbounds %struct._Log* %33, i32 0, i32 1, !dbg !1045
  %35 = load %struct._AVPair** %34, align 8, !dbg !1045
  store %struct._AVPair* %35, %struct._AVPair** %EndOfList.i, align 8, !dbg !1045
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e.i}, metadata !728), !dbg !1046
  %36 = load %struct._Log** %rd.i, align 8, !dbg !1047
  %37 = getelementptr inbounds %struct._Log* %36, i32 0, i32 0, !dbg !1047
  %38 = load %struct._AVPair** %37, align 8, !dbg !1047
  store %struct._AVPair* %38, %struct._AVPair** %e.i, align 8, !dbg !1047
  br label %39, !dbg !1047

; <label>:39                                      ; preds = %75, %26
  %40 = load %struct._AVPair** %e.i, align 8, !dbg !1047
  %41 = load %struct._AVPair** %EndOfList.i, align 8, !dbg !1047
  %42 = icmp ne %struct._AVPair* %40, %41, !dbg !1047
  br i1 %42, label %43, label %79, !dbg !1047

; <label>:43                                      ; preds = %39
  call void @llvm.dbg.declare(metadata !{i64* %v.i}, metadata !732), !dbg !1048
  %44 = load %struct._AVPair** %e.i, align 8, !dbg !1048
  %45 = getelementptr inbounds %struct._AVPair* %44, i32 0, i32 4, !dbg !1048
  %46 = load i64** %45, align 8, !dbg !1048
  %47 = load volatile i64* %46, align 8, !dbg !1048
  store i64 %47, i64* %v.i, align 8, !dbg !1048
  %48 = load i64* %v.i, align 8, !dbg !1049
  %49 = and i64 %48, 1, !dbg !1049
  %50 = icmp ne i64 %49, 0, !dbg !1049
  br i1 %50, label %51, label %69, !dbg !1049

; <label>:51                                      ; preds = %43
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p.i}, metadata !736), !dbg !1050
  %52 = load i64* %v.i, align 8, !dbg !1050
  %53 = and i64 %52, -2, !dbg !1050
  %54 = inttoptr i64 %53 to %struct._AVPair*, !dbg !1050
  store %struct._AVPair* %54, %struct._AVPair** %p.i, align 8, !dbg !1050
  %55 = load %struct._AVPair** %p.i, align 8, !dbg !1051
  %56 = getelementptr inbounds %struct._AVPair* %55, i32 0, i32 6, !dbg !1051
  %57 = load %struct._Thread** %56, align 8, !dbg !1051
  %58 = load %struct._Thread** %2, align 8, !dbg !1051
  %59 = icmp eq %struct._Thread* %57, %58, !dbg !1051
  br i1 %59, label %60, label %68, !dbg !1051

; <label>:60                                      ; preds = %51
  %61 = load %struct._AVPair** %p.i, align 8, !dbg !1052
  %62 = getelementptr inbounds %struct._AVPair* %61, i32 0, i32 5, !dbg !1052
  %63 = load i64* %62, align 8, !dbg !1052
  %64 = load i64* %rv.i, align 8, !dbg !1052
  %65 = icmp ugt i64 %63, %64, !dbg !1052
  br i1 %65, label %66, label %67, !dbg !1052

; <label>:66                                      ; preds = %60
  store i64 0, i64* %1, !dbg !1053
  br label %ReadSetCoherentPessimistic.exit, !dbg !1053

; <label>:67                                      ; preds = %60
  br label %75, !dbg !1054

; <label>:68                                      ; preds = %51
  store i64 0, i64* %1, !dbg !1055
  br label %ReadSetCoherentPessimistic.exit, !dbg !1055

; <label>:69                                      ; preds = %43
  %70 = load i64* %v.i, align 8, !dbg !1056
  %71 = load i64* %rv.i, align 8, !dbg !1056
  %72 = icmp ugt i64 %70, %71, !dbg !1056
  br i1 %72, label %73, label %74, !dbg !1056

; <label>:73                                      ; preds = %69
  store i64 0, i64* %1, !dbg !1057
  br label %ReadSetCoherentPessimistic.exit, !dbg !1057

; <label>:74                                      ; preds = %69
  br label %75

; <label>:75                                      ; preds = %74, %67
  %76 = load %struct._AVPair** %e.i, align 8, !dbg !1047
  %77 = getelementptr inbounds %struct._AVPair* %76, i32 0, i32 0, !dbg !1047
  %78 = load %struct._AVPair** %77, align 8, !dbg !1047
  store %struct._AVPair* %78, %struct._AVPair** %e.i, align 8, !dbg !1047
  br label %39, !dbg !1047

; <label>:79                                      ; preds = %39
  store i64 1, i64* %1, !dbg !1058
  br label %ReadSetCoherentPessimistic.exit, !dbg !1058

ReadSetCoherentPessimistic.exit:                  ; preds = %66, %68, %73, %79
  %80 = load i64* %1, !dbg !1058
  %81 = icmp ne i64 %80, 0, !dbg !1040
  br i1 %81, label %84, label %82, !dbg !1040

; <label>:82                                      ; preds = %ReadSetCoherentPessimistic.exit
  %83 = load %struct._Thread** %Self, align 8, !dbg !1059
  call void @TxAbort(%struct._Thread* %83), !dbg !1059
  br label %84, !dbg !1061

; <label>:84                                      ; preds = %82, %ReadSetCoherentPessimistic.exit
  br label %85, !dbg !1062

; <label>:85                                      ; preds = %84, %21
  %86 = load i32* %3, align 4, !dbg !1063
  call void @psignal(i32 %86, i8* null), !dbg !1063
  call void @abort() #7, !dbg !1064
  unreachable, !dbg !1064
                                                  ; No predecessors!
  ret void, !dbg !1065
}

; Function Attrs: nounwind
declare i32 @sigemptyset(%struct.__sigset_t*) #3

; Function Attrs: nounwind
declare i8* @pthread_getspecific(i32) #3

declare void @psignal(i32, i8*) #4

; Function Attrs: noreturn nounwind
declare void @abort() #5

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { noinline nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { noreturn nounwind }

!llvm.dbg.cu = !{!0}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.3-16ubuntu1 (branches/release_33) (based on LLVM 3.3)", i1 false, metadata !"", i32 0, metadata !2, metadata !19, metadata !20, metadata !292, metadata !19, metadata !""} ; [ DW_TAG_compile_unit ] [/vagrant/stamp-tl2-x86/tl2.c] [DW_LANG_C99]
!1 = metadata !{metadata !"tl2.c", metadata !"/vagrant/stamp-tl2-x86"}
!2 = metadata !{metadata !3, metadata !8, metadata !15}
!3 = metadata !{i32 786436, metadata !1, null, metadata !"tl2_config", i32 50, i64 32, i64 32, i32 0, i32 0, null, metadata !4, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [tl2_config] [line 50, size 32, align 32, offset 0] [from ]
!4 = metadata !{metadata !5, metadata !6, metadata !7}
!5 = metadata !{i32 786472, metadata !"TL2_INIT_WRSET_NUM_ENTRY", i64 1024} ; [ DW_TAG_enumerator ] [TL2_INIT_WRSET_NUM_ENTRY :: 1024]
!6 = metadata !{i32 786472, metadata !"TL2_INIT_RDSET_NUM_ENTRY", i64 8192} ; [ DW_TAG_enumerator ] [TL2_INIT_RDSET_NUM_ENTRY :: 8192]
!7 = metadata !{i32 786472, metadata !"TL2_INIT_LOCAL_NUM_ENTRY", i64 1024} ; [ DW_TAG_enumerator ] [TL2_INIT_LOCAL_NUM_ENTRY :: 1024]
!8 = metadata !{i32 786436, metadata !1, null, metadata !"", i32 73, i64 32, i64 32, i32 0, i32 0, null, metadata !9, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [line 73, size 32, align 32, offset 0] [from ]
!9 = metadata !{metadata !10, metadata !11, metadata !12, metadata !13, metadata !14}
!10 = metadata !{i32 786472, metadata !"TIDLE", i64 0} ; [ DW_TAG_enumerator ] [TIDLE :: 0]
!11 = metadata !{i32 786472, metadata !"TTXN", i64 1} ; [ DW_TAG_enumerator ] [TTXN :: 1]
!12 = metadata !{i32 786472, metadata !"TABORTING", i64 3} ; [ DW_TAG_enumerator ] [TABORTING :: 3]
!13 = metadata !{i32 786472, metadata !"TABORTED", i64 5} ; [ DW_TAG_enumerator ] [TABORTED :: 5]
!14 = metadata !{i32 786472, metadata !"TCOMMITTING", i64 7} ; [ DW_TAG_enumerator ] [TCOMMITTING :: 7]
!15 = metadata !{i32 786436, metadata !1, null, metadata !"", i32 81, i64 32, i64 32, i32 0, i32 0, null, metadata !16, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [line 81, size 32, align 32, offset 0] [from ]
!16 = metadata !{metadata !17, metadata !18}
!17 = metadata !{i32 786472, metadata !"LOCKBIT", i64 1} ; [ DW_TAG_enumerator ] [LOCKBIT :: 1]
!18 = metadata !{i32 786472, metadata !"NADA", i64 2} ; [ DW_TAG_enumerator ] [NADA :: 2]
!19 = metadata !{i32 0}
!20 = metadata !{metadata !21, metadata !34, metadata !124, metadata !127, metadata !128, metadata !133, metadata !136, metadata !139, metadata !140, metadata !143, metadata !146, metadata !147, metadata !150, metadata !153, metadata !157, metadata !160, metadata !163, metadata !166, metadata !167, metadata !170, metadata !173, metadata !174, metadata !177, metadata !178, metadata !181, metadata !184, metadata !187, metadata !188, metadata !191, metadata !196, metadata !197, metadata !200, metadata !204, metadata !207, metadata !208, metadata !209, metadata !212, metadata !215, metadata !218, metadata !219, metadata !220, metadata !291}
!21 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"pslock", metadata !"pslock", metadata !"", i32 574, metadata !23, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64* (i64*)* @pslock, null, null, metadata !19, i32 575} ; [ DW_TAG_subprogram ] [line 574] [def] [scope 575] [pslock]
!22 = metadata !{i32 786473, metadata !1}         ; [ DW_TAG_file_type ] [/vagrant/stamp-tl2-x86/tl2.c]
!23 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !24, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!24 = metadata !{metadata !25, metadata !30}
!25 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !26} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!26 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !27} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from vwLock]
!27 = metadata !{i32 786454, metadata !1, null, metadata !"vwLock", i32 109, i64 0, i64 0, i64 0, i32 0, metadata !28} ; [ DW_TAG_typedef ] [vwLock] [line 109, size 0, align 0, offset 0] [from uintptr_t]
!28 = metadata !{i32 786454, metadata !1, null, metadata !"uintptr_t", i32 122, i64 0, i64 0, i64 0, i32 0, metadata !29} ; [ DW_TAG_typedef ] [uintptr_t] [line 122, size 0, align 0, offset 0] [from long unsigned int]
!29 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!30 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !31} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!31 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !32} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from intptr_t]
!32 = metadata !{i32 786454, metadata !1, null, metadata !"intptr_t", i32 119, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_typedef ] [intptr_t] [line 119, size 0, align 0, offset 0] [from long int]
!33 = metadata !{i32 786468, null, null, metadata !"long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!34 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"FreeList", metadata !"FreeList", metadata !"", i32 616, metadata !35, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Log*, i64)* @FreeList, null, null, metadata !19, i32 617} ; [ DW_TAG_subprogram ] [line 616] [def] [scope 617] [FreeList]
!35 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !36, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!36 = metadata !{null, metadata !37, metadata !33}
!37 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !38} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from Log]
!38 = metadata !{i32 786454, metadata !1, null, metadata !"Log", i32 136, i64 0, i64 0, i64 0, i32 0, metadata !39} ; [ DW_TAG_typedef ] [Log] [line 136, size 0, align 0, offset 0] [from _Log]
!39 = metadata !{i32 786451, metadata !1, null, metadata !"_Log", i32 127, i64 384, i64 64, i32 0, i32 0, null, metadata !40, i32 0, null, null} ; [ DW_TAG_structure_type ] [_Log] [line 127, size 384, align 64, offset 0] [from ]
!40 = metadata !{metadata !41, metadata !118, metadata !119, metadata !120, metadata !121, metadata !122}
!41 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"List", i32 128, i64 64, i64 64, i64 0, i32 0, metadata !42} ; [ DW_TAG_member ] [List] [line 128, size 64, align 64, offset 0] [from ]
!42 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !43} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from AVPair]
!43 = metadata !{i32 786454, metadata !1, null, metadata !"AVPair", i32 125, i64 0, i64 0, i64 0, i32 0, metadata !44} ; [ DW_TAG_typedef ] [AVPair] [line 125, size 0, align 0, offset 0] [from _AVPair]
!44 = metadata !{i32 786451, metadata !1, null, metadata !"_AVPair", i32 113, i64 512, i64 64, i32 0, i32 0, null, metadata !45, i32 0, null, null} ; [ DW_TAG_structure_type ] [_AVPair] [line 113, size 512, align 64, offset 0] [from ]
!45 = metadata !{metadata !46, metadata !48, metadata !49, metadata !50, metadata !51, metadata !52, metadata !53, metadata !117}
!46 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Next", i32 114, i64 64, i64 64, i64 0, i32 0, metadata !47} ; [ DW_TAG_member ] [Next] [line 114, size 64, align 64, offset 0] [from ]
!47 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !44} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from _AVPair]
!48 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Prev", i32 115, i64 64, i64 64, i64 64, i32 0, metadata !47} ; [ DW_TAG_member ] [Prev] [line 115, size 64, align 64, offset 64] [from ]
!49 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Addr", i32 116, i64 64, i64 64, i64 128, i32 0, metadata !30} ; [ DW_TAG_member ] [Addr] [line 116, size 64, align 64, offset 128] [from ]
!50 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Valu", i32 117, i64 64, i64 64, i64 192, i32 0, metadata !32} ; [ DW_TAG_member ] [Valu] [line 117, size 64, align 64, offset 192] [from intptr_t]
!51 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"LockFor", i32 118, i64 64, i64 64, i64 256, i32 0, metadata !25} ; [ DW_TAG_member ] [LockFor] [line 118, size 64, align 64, offset 256] [from ]
!52 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"rdv", i32 119, i64 64, i64 64, i64 320, i32 0, metadata !27} ; [ DW_TAG_member ] [rdv] [line 119, size 64, align 64, offset 320] [from vwLock]
!53 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Owner", i32 123, i64 64, i64 64, i64 384, i32 0, metadata !54} ; [ DW_TAG_member ] [Owner] [line 123, size 64, align 64, offset 384] [from ]
!54 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !55} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from _Thread]
!55 = metadata !{i32 786451, metadata !1, null, metadata !"_Thread", i32 147, i64 2816, i64 64, i32 0, i32 0, null, metadata !56, i32 0, null, null} ; [ DW_TAG_structure_type ] [_Thread] [line 147, size 2816, align 64, offset 0] [from ]
!56 = metadata !{metadata !57, metadata !58, metadata !60, metadata !61, metadata !62, metadata !63, metadata !64, metadata !65, metadata !66, metadata !67, metadata !70, metadata !71, metadata !72, metadata !73, metadata !75, metadata !79, metadata !81, metadata !91, metadata !92, metadata !93, metadata !94, metadata !95}
!57 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"UniqID", i32 148, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [UniqID] [line 148, size 64, align 64, offset 0] [from long int]
!58 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"Mode", i32 149, i64 64, i64 64, i64 64, i32 0, metadata !59} ; [ DW_TAG_member ] [Mode] [line 149, size 64, align 64, offset 64] [from ]
!59 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from long int]
!60 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"HoldsLocks", i32 150, i64 64, i64 64, i64 128, i32 0, metadata !59} ; [ DW_TAG_member ] [HoldsLocks] [line 150, size 64, align 64, offset 128] [from ]
!61 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"Retries", i32 151, i64 64, i64 64, i64 192, i32 0, metadata !59} ; [ DW_TAG_member ] [Retries] [line 151, size 64, align 64, offset 192] [from ]
!62 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"rv", i32 152, i64 64, i64 64, i64 256, i32 0, metadata !26} ; [ DW_TAG_member ] [rv] [line 152, size 64, align 64, offset 256] [from ]
!63 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"wv", i32 153, i64 64, i64 64, i64 320, i32 0, metadata !27} ; [ DW_TAG_member ] [wv] [line 153, size 64, align 64, offset 320] [from vwLock]
!64 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"abv", i32 154, i64 64, i64 64, i64 384, i32 0, metadata !27} ; [ DW_TAG_member ] [abv] [line 154, size 64, align 64, offset 384] [from vwLock]
!65 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"maxv", i32 156, i64 64, i64 64, i64 448, i32 0, metadata !27} ; [ DW_TAG_member ] [maxv] [line 156, size 64, align 64, offset 448] [from vwLock]
!66 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"tmpLockEntry", i32 157, i64 512, i64 64, i64 512, i32 0, metadata !43} ; [ DW_TAG_member ] [tmpLockEntry] [line 157, size 512, align 64, offset 512] [from AVPair]
!67 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"ROFlag", i32 159, i64 64, i64 64, i64 1024, i32 0, metadata !68} ; [ DW_TAG_member ] [ROFlag] [line 159, size 64, align 64, offset 1024] [from ]
!68 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !69} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from int]
!69 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!70 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"IsRO", i32 160, i64 32, i64 32, i64 1088, i32 0, metadata !69} ; [ DW_TAG_member ] [IsRO] [line 160, size 32, align 32, offset 1088] [from int]
!71 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"Starts", i32 161, i64 64, i64 64, i64 1152, i32 0, metadata !33} ; [ DW_TAG_member ] [Starts] [line 161, size 64, align 64, offset 1152] [from long int]
!72 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"Aborts", i32 162, i64 64, i64 64, i64 1216, i32 0, metadata !33} ; [ DW_TAG_member ] [Aborts] [line 162, size 64, align 64, offset 1216] [from long int]
!73 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"rng", i32 163, i64 64, i64 64, i64 1280, i32 0, metadata !74} ; [ DW_TAG_member ] [rng] [line 163, size 64, align 64, offset 1280] [from long long unsigned int]
!74 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!75 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"xorrng", i32 164, i64 64, i64 64, i64 1344, i32 0, metadata !76} ; [ DW_TAG_member ] [xorrng] [line 164, size 64, align 64, offset 1344] [from ]
!76 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 64, i64 64, i32 0, i32 0, metadata !74, metadata !77, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 64, align 64, offset 0] [from long long unsigned int]
!77 = metadata !{metadata !78}
!78 = metadata !{i32 786465, i64 0, i64 1}        ; [ DW_TAG_subrange_type ] [0, 0]
!79 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"memCache", i32 165, i64 64, i64 64, i64 1408, i32 0, metadata !80} ; [ DW_TAG_member ] [memCache] [line 165, size 64, align 64, offset 1408] [from ]
!80 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!81 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"allocPtr", i32 166, i64 64, i64 64, i64 1472, i32 0, metadata !82} ; [ DW_TAG_member ] [allocPtr] [line 166, size 64, align 64, offset 1472] [from ]
!82 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !83} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from tmalloc_t]
!83 = metadata !{i32 786454, metadata !1, null, metadata !"tmalloc_t", i32 31, i64 0, i64 0, i64 0, i32 0, metadata !84} ; [ DW_TAG_typedef ] [tmalloc_t] [line 31, size 0, align 0, offset 0] [from tmalloc]
!84 = metadata !{i32 786451, metadata !85, null, metadata !"tmalloc", i32 27, i64 192, i64 64, i32 0, i32 0, null, metadata !86, i32 0, null, null} ; [ DW_TAG_structure_type ] [tmalloc] [line 27, size 192, align 64, offset 0] [from ]
!85 = metadata !{metadata !"./tmalloc.h", metadata !"/vagrant/stamp-tl2-x86"}
!86 = metadata !{metadata !87, metadata !88, metadata !89}
!87 = metadata !{i32 786445, metadata !85, metadata !84, metadata !"size", i32 28, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [size] [line 28, size 64, align 64, offset 0] [from long int]
!88 = metadata !{i32 786445, metadata !85, metadata !84, metadata !"capacity", i32 29, i64 64, i64 64, i64 64, i32 0, metadata !33} ; [ DW_TAG_member ] [capacity] [line 29, size 64, align 64, offset 64] [from long int]
!89 = metadata !{i32 786445, metadata !85, metadata !84, metadata !"elements", i32 30, i64 64, i64 64, i64 128, i32 0, metadata !90} ; [ DW_TAG_member ] [elements] [line 30, size 64, align 64, offset 128] [from ]
!90 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !80} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!91 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"freePtr", i32 167, i64 64, i64 64, i64 1536, i32 0, metadata !82} ; [ DW_TAG_member ] [freePtr] [line 167, size 64, align 64, offset 1536] [from ]
!92 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"rdSet", i32 168, i64 384, i64 64, i64 1600, i32 0, metadata !38} ; [ DW_TAG_member ] [rdSet] [line 168, size 384, align 64, offset 1600] [from Log]
!93 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"wrSet", i32 172, i64 384, i64 64, i64 1984, i32 0, metadata !38} ; [ DW_TAG_member ] [wrSet] [line 172, size 384, align 64, offset 1984] [from Log]
!94 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"LocalUndo", i32 174, i64 384, i64 64, i64 2368, i32 0, metadata !38} ; [ DW_TAG_member ] [LocalUndo] [line 174, size 384, align 64, offset 2368] [from Log]
!95 = metadata !{i32 786445, metadata !1, metadata !55, metadata !"envPtr", i32 175, i64 64, i64 64, i64 2752, i32 0, metadata !96} ; [ DW_TAG_member ] [envPtr] [line 175, size 64, align 64, offset 2752] [from ]
!96 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !97} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from sigjmp_buf]
!97 = metadata !{i32 786454, metadata !1, null, metadata !"sigjmp_buf", i32 92, i64 0, i64 0, i64 0, i32 0, metadata !98} ; [ DW_TAG_typedef ] [sigjmp_buf] [line 92, size 0, align 0, offset 0] [from ]
!98 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1600, i64 64, i32 0, i32 0, metadata !99, metadata !77, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 1600, align 64, offset 0] [from __jmp_buf_tag]
!99 = metadata !{i32 786451, metadata !100, null, metadata !"__jmp_buf_tag", i32 34, i64 1600, i64 64, i32 0, i32 0, null, metadata !101, i32 0, null, null} ; [ DW_TAG_structure_type ] [__jmp_buf_tag] [line 34, size 1600, align 64, offset 0] [from ]
!100 = metadata !{metadata !"/usr/include/setjmp.h", metadata !"/vagrant/stamp-tl2-x86"}
!101 = metadata !{metadata !102, metadata !107, metadata !108}
!102 = metadata !{i32 786445, metadata !100, metadata !99, metadata !"__jmpbuf", i32 40, i64 512, i64 64, i64 0, i32 0, metadata !103} ; [ DW_TAG_member ] [__jmpbuf] [line 40, size 512, align 64, offset 0] [from __jmp_buf]
!103 = metadata !{i32 786454, metadata !100, null, metadata !"__jmp_buf", i32 31, i64 0, i64 0, i64 0, i32 0, metadata !104} ; [ DW_TAG_typedef ] [__jmp_buf] [line 31, size 0, align 0, offset 0] [from ]
!104 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 512, i64 64, i32 0, i32 0, metadata !33, metadata !105, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 512, align 64, offset 0] [from long int]
!105 = metadata !{metadata !106}
!106 = metadata !{i32 786465, i64 0, i64 8}       ; [ DW_TAG_subrange_type ] [0, 7]
!107 = metadata !{i32 786445, metadata !100, metadata !99, metadata !"__mask_was_saved", i32 41, i64 32, i64 32, i64 512, i32 0, metadata !69} ; [ DW_TAG_member ] [__mask_was_saved] [line 41, size 32, align 32, offset 512] [from int]
!108 = metadata !{i32 786445, metadata !100, metadata !99, metadata !"__saved_mask", i32 42, i64 1024, i64 64, i64 576, i32 0, metadata !109} ; [ DW_TAG_member ] [__saved_mask] [line 42, size 1024, align 64, offset 576] [from __sigset_t]
!109 = metadata !{i32 786454, metadata !100, null, metadata !"__sigset_t", i32 30, i64 0, i64 0, i64 0, i32 0, metadata !110} ; [ DW_TAG_typedef ] [__sigset_t] [line 30, size 0, align 0, offset 0] [from ]
!110 = metadata !{i32 786451, metadata !111, null, metadata !"", i32 27, i64 1024, i64 64, i32 0, i32 0, null, metadata !112, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 27, size 1024, align 64, offset 0] [from ]
!111 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/sigset.h", metadata !"/vagrant/stamp-tl2-x86"}
!112 = metadata !{metadata !113}
!113 = metadata !{i32 786445, metadata !111, metadata !110, metadata !"__val", i32 29, i64 1024, i64 64, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__val] [line 29, size 1024, align 64, offset 0] [from ]
!114 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1024, i64 64, i32 0, i32 0, metadata !29, metadata !115, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 1024, align 64, offset 0] [from long unsigned int]
!115 = metadata !{metadata !116}
!116 = metadata !{i32 786465, i64 0, i64 16}      ; [ DW_TAG_subrange_type ] [0, 15]
!117 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Ordinal", i32 124, i64 64, i64 64, i64 448, i32 0, metadata !33} ; [ DW_TAG_member ] [Ordinal] [line 124, size 64, align 64, offset 448] [from long int]
!118 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"put", i32 129, i64 64, i64 64, i64 64, i32 0, metadata !42} ; [ DW_TAG_member ] [put] [line 129, size 64, align 64, offset 64] [from ]
!119 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"tail", i32 130, i64 64, i64 64, i64 128, i32 0, metadata !42} ; [ DW_TAG_member ] [tail] [line 130, size 64, align 64, offset 128] [from ]
!120 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"end", i32 131, i64 64, i64 64, i64 192, i32 0, metadata !42} ; [ DW_TAG_member ] [end] [line 131, size 64, align 64, offset 192] [from ]
!121 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"ovf", i32 132, i64 64, i64 64, i64 256, i32 0, metadata !33} ; [ DW_TAG_member ] [ovf] [line 132, size 64, align 64, offset 256] [from long int]
!122 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"BloomFilter", i32 134, i64 32, i64 32, i64 320, i32 0, metadata !123} ; [ DW_TAG_member ] [BloomFilter] [line 134, size 32, align 32, offset 320] [from BitMap]
!123 = metadata !{i32 786454, metadata !1, null, metadata !"BitMap", i32 108, i64 0, i64 0, i64 0, i32 0, metadata !69} ; [ DW_TAG_typedef ] [BitMap] [line 108, size 0, align 0, offset 0] [from int]
!124 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxOnce", metadata !"TxOnce", metadata !"", i32 1014, metadata !125, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @TxOnce, null, null, metadata !19, i32 1015} ; [ DW_TAG_subprogram ] [line 1014] [def] [scope 1015] [TxOnce]
!125 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !126, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!126 = metadata !{null}
!127 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxShutdown", metadata !"TxShutdown", metadata !"", i32 1032, metadata !125, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @TxShutdown, null, null, metadata !19, i32 1033} ; [ DW_TAG_subprogram ] [line 1032] [def] [scope 1033] [TxShutdown]
!128 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxNewThread", metadata !"TxNewThread", metadata !"", i32 1062, metadata !129, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, %struct._Thread* ()* @TxNewThread, null, null, metadata !19, i32 1063} ; [ DW_TAG_subprogram ] [line 1062] [def] [scope 1063] [TxNewThread]
!129 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !130, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!130 = metadata !{metadata !131}
!131 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !132} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from Thread]
!132 = metadata !{i32 786454, metadata !1, null, metadata !"Thread", i32 35, i64 0, i64 0, i64 0, i32 0, metadata !55} ; [ DW_TAG_typedef ] [Thread] [line 35, size 0, align 0, offset 0] [from _Thread]
!133 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxFreeThread", metadata !"TxFreeThread", metadata !"", i32 1082, metadata !134, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @TxFreeThread, null, null, metadata !19, i32 1083} ; [ DW_TAG_subprogram ] [line 1082] [def] [scope 1083] [TxFreeThread]
!134 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !135, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!135 = metadata !{null, metadata !131}
!136 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxInitThread", metadata !"TxInitThread", metadata !"", i32 1126, metadata !137, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64)* @TxInitThread, null, null, metadata !19, i32 1127} ; [ DW_TAG_subprogram ] [line 1126] [def] [scope 1127] [TxInitThread]
!137 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !138, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!138 = metadata !{null, metadata !131, metadata !33}
!139 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxAbort", metadata !"TxAbort", metadata !"", i32 1704, metadata !134, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @TxAbort, null, null, metadata !19, i32 1705} ; [ DW_TAG_subprogram ] [line 1704] [def] [scope 1705] [TxAbort]
!140 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStore", metadata !"TxStore", metadata !"", i32 1771, metadata !141, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64*, i64)* @TxStore, null, null, metadata !19, i32 1772} ; [ DW_TAG_subprogram ] [line 1771] [def] [scope 1772] [TxStore]
!141 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !142, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!142 = metadata !{null, metadata !131, metadata !30, metadata !32}
!143 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxLoad", metadata !"TxLoad", metadata !"", i32 1975, metadata !144, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*, i64*)* @TxLoad, null, null, metadata !19, i32 1976} ; [ DW_TAG_subprogram ] [line 1975] [def] [scope 1976] [TxLoad]
!144 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !145, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!145 = metadata !{metadata !32, metadata !131, metadata !30}
!146 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStoreLocal", metadata !"TxStoreLocal", metadata !"", i32 2147, metadata !141, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64*, i64)* @TxStoreLocal, null, null, metadata !19, i32 2148} ; [ DW_TAG_subprogram ] [line 2147] [def] [scope 2148] [TxStoreLocal]
!147 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStart", metadata !"TxStart", metadata !"", i32 2163, metadata !148, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, [1 x %struct.__jmp_buf_tag]*, i32*)* @TxStart, null, null, metadata !19, i32 2164} ; [ DW_TAG_subprogram ] [line 2163] [def] [scope 2164] [TxStart]
!148 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !149, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!149 = metadata !{null, metadata !131, metadata !96, metadata !68}
!150 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxCommit", metadata !"TxCommit", metadata !"", i32 2195, metadata !151, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (%struct._Thread*)* @TxCommit, null, null, metadata !19, i32 2196} ; [ DW_TAG_subprogram ] [line 2195] [def] [scope 2196] [TxCommit]
!151 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !152, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!152 = metadata !{metadata !69, metadata !131}
!153 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxAlloc", metadata !"TxAlloc", metadata !"", i32 2265, metadata !154, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (%struct._Thread*, i64)* @TxAlloc, null, null, metadata !19, i32 2266} ; [ DW_TAG_subprogram ] [line 2265] [def] [scope 2266] [TxAlloc]
!154 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !155, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!155 = metadata !{metadata !80, metadata !131, metadata !156}
!156 = metadata !{i32 786454, metadata !1, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !29} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!157 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxFree", metadata !"TxFree", metadata !"", i32 2283, metadata !158, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i8*)* @TxFree, null, null, metadata !19, i32 2284} ; [ DW_TAG_subprogram ] [line 2283] [def] [scope 2284] [TxFree]
!158 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !159, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!159 = metadata !{null, metadata !131, metadata !80}
!160 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TryFastUpdate", metadata !"TryFastUpdate", metadata !"", i32 1457, metadata !161, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1458} ; [ DW_TAG_subprogram ] [line 1457] [local] [def] [scope 1458] [TryFastUpdate]
!161 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !162, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!162 = metadata !{metadata !33, metadata !131}
!163 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"DropLocks", metadata !"DropLocks", metadata !"", i32 1381, metadata !164, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1382} ; [ DW_TAG_subprogram ] [line 1381] [local] [def] [scope 1382] [DropLocks]
!164 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !165, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!165 = metadata !{null, metadata !131, metadata !27}
!166 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ReadSetCoherent", metadata !"ReadSetCoherent", metadata !"", i32 1254, metadata !161, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1255} ; [ DW_TAG_subprogram ] [line 1254] [local] [def] [scope 1255] [ReadSetCoherent]
!167 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVGenerateWV_GV4", metadata !"GVGenerateWV_GV4", metadata !"", i32 381, metadata !168, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 382} ; [ DW_TAG_subprogram ] [line 381] [local] [def] [scope 382] [GVGenerateWV_GV4]
!168 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !169, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!169 = metadata !{metadata !27, metadata !131, metadata !27}
!170 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txSterilize", metadata !"txSterilize", metadata !"", i32 2120, metadata !171, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i8*, i64)* @txSterilize, null, null, metadata !19, i32 2121} ; [ DW_TAG_subprogram ] [line 2120] [local] [def] [scope 2121] [txSterilize]
!171 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !172, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!172 = metadata !{null, metadata !80, metadata !156}
!173 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txCommitReset", metadata !"txCommitReset", metadata !"", i32 1206, metadata !134, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1207} ; [ DW_TAG_subprogram ] [line 1206] [local] [def] [scope 1207] [txCommitReset]
!174 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVRead", metadata !"GVRead", metadata !"", i32 313, metadata !175, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 314} ; [ DW_TAG_subprogram ] [line 313] [local] [def] [scope 314] [GVRead]
!175 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !176, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!176 = metadata !{metadata !27, metadata !131}
!177 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txReset", metadata !"txReset", metadata !"", i32 1166, metadata !134, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1167} ; [ DW_TAG_subprogram ] [line 1166] [local] [def] [scope 1167] [txReset]
!178 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"SaveForRollBack", metadata !"SaveForRollBack", metadata !"", i32 866, metadata !179, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 867} ; [ DW_TAG_subprogram ] [line 866] [local] [def] [scope 867] [SaveForRollBack]
!179 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !180, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!180 = metadata !{null, metadata !37, metadata !30, metadata !32}
!181 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ExtendList", metadata !"ExtendList", metadata !"", i32 641, metadata !182, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 642} ; [ DW_TAG_subprogram ] [line 641] [local] [def] [scope 642] [ExtendList]
!182 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !183, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!183 = metadata !{metadata !42, metadata !42}
!184 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TrackLoad", metadata !"TrackLoad", metadata !"", i32 887, metadata !185, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 888} ; [ DW_TAG_subprogram ] [line 887] [local] [def] [scope 888] [TrackLoad]
!185 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !186, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!186 = metadata !{metadata !69, metadata !131, metadata !25}
!187 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ReadSetCoherentPessimistic", metadata !"ReadSetCoherentPessimistic", metadata !"", i32 1296, metadata !161, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1297} ; [ DW_TAG_subprogram ] [line 1296] [local] [def] [scope 1297] [ReadSetCoherentPessimistic]
!188 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"RecordStore", metadata !"RecordStore", metadata !"", i32 808, metadata !189, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 813} ; [ DW_TAG_subprogram ] [line 808] [local] [def] [scope 813] [RecordStore]
!189 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !190, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!190 = metadata !{metadata !42, metadata !37, metadata !30, metadata !32, metadata !25, metadata !27}
!191 = metadata !{i32 786478, metadata !192, metadata !193, metadata !"cas", metadata !"cas", metadata !"", i32 42, metadata !194, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 43} ; [ DW_TAG_subprogram ] [line 42] [local] [def] [scope 43] [cas]
!192 = metadata !{metadata !"./platform_x86.h", metadata !"/vagrant/stamp-tl2-x86"}
!193 = metadata !{i32 786473, metadata !192}      ; [ DW_TAG_file_type ] [/vagrant/stamp-tl2-x86/./platform_x86.h]
!194 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !195, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!195 = metadata !{metadata !32, metadata !32, metadata !32, metadata !30}
!196 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"backoff", metadata !"backoff", metadata !"", i32 1427, metadata !137, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1428} ; [ DW_TAG_subprogram ] [line 1427] [local] [def] [scope 1428] [backoff]
!197 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TSRandom", metadata !"TSRandom", metadata !"", i32 243, metadata !198, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 244} ; [ DW_TAG_subprogram ] [line 243] [local] [def] [scope 244] [TSRandom]
!198 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !199, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!199 = metadata !{metadata !74, metadata !131}
!200 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MarsagliaXOR", metadata !"MarsagliaXOR", metadata !"", i32 230, metadata !201, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 231} ; [ DW_TAG_subprogram ] [line 230] [local] [def] [scope 231] [MarsagliaXOR]
!201 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !202, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!202 = metadata !{metadata !74, metadata !203}
!203 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !74} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from long long unsigned int]
!204 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MarsagliaXORV", metadata !"MarsagliaXORV", metadata !"", i32 210, metadata !205, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 211} ; [ DW_TAG_subprogram ] [line 210] [local] [def] [scope 211] [MarsagliaXORV]
!205 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !206, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!206 = metadata !{metadata !74, metadata !74}
!207 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVAbort", metadata !"GVAbort", metadata !"", i32 475, metadata !161, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 476} ; [ DW_TAG_subprogram ] [line 475] [local] [def] [scope 476] [GVAbort]
!208 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"RestoreLocks", metadata !"RestoreLocks", metadata !"", i32 1339, metadata !134, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 1340} ; [ DW_TAG_subprogram ] [line 1339] [local] [def] [scope 1340] [RestoreLocks]
!209 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"WriteBackReverse", metadata !"WriteBackReverse", metadata !"", i32 772, metadata !210, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 773} ; [ DW_TAG_subprogram ] [line 772] [local] [def] [scope 773] [WriteBackReverse]
!210 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !211, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!211 = metadata !{null, metadata !37}
!212 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MakeList", metadata !"MakeList", metadata !"", i32 588, metadata !213, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 589} ; [ DW_TAG_subprogram ] [line 588] [local] [def] [scope 589] [MakeList]
!213 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !214, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!214 = metadata !{metadata !42, metadata !33, metadata !131}
!215 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"AtomicAdd", metadata !"AtomicAdd", metadata !"", i32 254, metadata !216, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !19, i32 255} ; [ DW_TAG_subprogram ] [line 254] [local] [def] [scope 255] [AtomicAdd]
!216 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !217, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!217 = metadata !{metadata !32, metadata !30, metadata !32}
!218 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"restoreUseAfterFreeHandler", metadata !"restoreUseAfterFreeHandler", metadata !"", i32 993, metadata !125, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @restoreUseAfterFreeHandler, null, null, metadata !19, i32 994} ; [ DW_TAG_subprogram ] [line 993] [local] [def] [scope 994] [restoreUseAfterFreeHandler]
!219 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"registerUseAfterFreeHandler", metadata !"registerUseAfterFreeHandler", metadata !"", i32 967, metadata !125, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @registerUseAfterFreeHandler, null, null, metadata !19, i32 968} ; [ DW_TAG_subprogram ] [line 967] [local] [def] [scope 968] [registerUseAfterFreeHandler]
!220 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"useAfterFreeHandler", metadata !"useAfterFreeHandler", metadata !"", i32 942, metadata !221, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, %struct.siginfo_t*, i8*)* @useAfterFreeHandler, null, null, metadata !19, i32 943} ; [ DW_TAG_subprogram ] [line 942] [local] [def] [scope 943] [useAfterFreeHandler]
!221 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !222, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!222 = metadata !{null, metadata !69, metadata !223, metadata !80}
!223 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !224} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from siginfo_t]
!224 = metadata !{i32 786454, metadata !225, null, metadata !"siginfo_t", i32 128, i64 0, i64 0, i64 0, i32 0, metadata !226} ; [ DW_TAG_typedef ] [siginfo_t] [line 128, size 0, align 0, offset 0] [from ]
!225 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/sigaction.h", metadata !"/vagrant/stamp-tl2-x86"}
!226 = metadata !{i32 786451, metadata !227, null, metadata !"", i32 62, i64 1024, i64 64, i32 0, i32 0, null, metadata !228, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 62, size 1024, align 64, offset 0] [from ]
!227 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/siginfo.h", metadata !"/vagrant/stamp-tl2-x86"}
!228 = metadata !{metadata !229, metadata !230, metadata !231, metadata !232}
!229 = metadata !{i32 786445, metadata !227, metadata !226, metadata !"si_signo", i32 64, i64 32, i64 32, i64 0, i32 0, metadata !69} ; [ DW_TAG_member ] [si_signo] [line 64, size 32, align 32, offset 0] [from int]
!230 = metadata !{i32 786445, metadata !227, metadata !226, metadata !"si_errno", i32 65, i64 32, i64 32, i64 32, i32 0, metadata !69} ; [ DW_TAG_member ] [si_errno] [line 65, size 32, align 32, offset 32] [from int]
!231 = metadata !{i32 786445, metadata !227, metadata !226, metadata !"si_code", i32 67, i64 32, i64 32, i64 64, i32 0, metadata !69} ; [ DW_TAG_member ] [si_code] [line 67, size 32, align 32, offset 64] [from int]
!232 = metadata !{i32 786445, metadata !227, metadata !226, metadata !"_sifields", i32 127, i64 896, i64 64, i64 128, i32 0, metadata !233} ; [ DW_TAG_member ] [_sifields] [line 127, size 896, align 64, offset 128] [from ]
!233 = metadata !{i32 786455, metadata !227, metadata !226, metadata !"", i32 69, i64 896, i64 64, i64 0, i32 0, null, metadata !234, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [line 69, size 896, align 64, offset 0] [from ]
!234 = metadata !{metadata !235, metadata !239, metadata !247, metadata !258, metadata !264, metadata !274, metadata !280, metadata !285}
!235 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_pad", i32 71, i64 896, i64 32, i64 0, i32 0, metadata !236} ; [ DW_TAG_member ] [_pad] [line 71, size 896, align 32, offset 0] [from ]
!236 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 896, i64 32, i32 0, i32 0, metadata !69, metadata !237, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 896, align 32, offset 0] [from int]
!237 = metadata !{metadata !238}
!238 = metadata !{i32 786465, i64 0, i64 28}      ; [ DW_TAG_subrange_type ] [0, 27]
!239 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_kill", i32 78, i64 64, i64 32, i64 0, i32 0, metadata !240} ; [ DW_TAG_member ] [_kill] [line 78, size 64, align 32, offset 0] [from ]
!240 = metadata !{i32 786451, metadata !227, metadata !233, metadata !"", i32 74, i64 64, i64 32, i32 0, i32 0, null, metadata !241, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 74, size 64, align 32, offset 0] [from ]
!241 = metadata !{metadata !242, metadata !244}
!242 = metadata !{i32 786445, metadata !227, metadata !240, metadata !"si_pid", i32 76, i64 32, i64 32, i64 0, i32 0, metadata !243} ; [ DW_TAG_member ] [si_pid] [line 76, size 32, align 32, offset 0] [from __pid_t]
!243 = metadata !{i32 786454, metadata !227, null, metadata !"__pid_t", i32 133, i64 0, i64 0, i64 0, i32 0, metadata !69} ; [ DW_TAG_typedef ] [__pid_t] [line 133, size 0, align 0, offset 0] [from int]
!244 = metadata !{i32 786445, metadata !227, metadata !240, metadata !"si_uid", i32 77, i64 32, i64 32, i64 32, i32 0, metadata !245} ; [ DW_TAG_member ] [si_uid] [line 77, size 32, align 32, offset 32] [from __uid_t]
!245 = metadata !{i32 786454, metadata !227, null, metadata !"__uid_t", i32 125, i64 0, i64 0, i64 0, i32 0, metadata !246} ; [ DW_TAG_typedef ] [__uid_t] [line 125, size 0, align 0, offset 0] [from unsigned int]
!246 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!247 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_timer", i32 86, i64 128, i64 64, i64 0, i32 0, metadata !248} ; [ DW_TAG_member ] [_timer] [line 86, size 128, align 64, offset 0] [from ]
!248 = metadata !{i32 786451, metadata !227, metadata !233, metadata !"", i32 81, i64 128, i64 64, i32 0, i32 0, null, metadata !249, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 81, size 128, align 64, offset 0] [from ]
!249 = metadata !{metadata !250, metadata !251, metadata !252}
!250 = metadata !{i32 786445, metadata !227, metadata !248, metadata !"si_tid", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !69} ; [ DW_TAG_member ] [si_tid] [line 83, size 32, align 32, offset 0] [from int]
!251 = metadata !{i32 786445, metadata !227, metadata !248, metadata !"si_overrun", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !69} ; [ DW_TAG_member ] [si_overrun] [line 84, size 32, align 32, offset 32] [from int]
!252 = metadata !{i32 786445, metadata !227, metadata !248, metadata !"si_sigval", i32 85, i64 64, i64 64, i64 64, i32 0, metadata !253} ; [ DW_TAG_member ] [si_sigval] [line 85, size 64, align 64, offset 64] [from sigval_t]
!253 = metadata !{i32 786454, metadata !227, null, metadata !"sigval_t", i32 36, i64 0, i64 0, i64 0, i32 0, metadata !254} ; [ DW_TAG_typedef ] [sigval_t] [line 36, size 0, align 0, offset 0] [from sigval]
!254 = metadata !{i32 786455, metadata !227, null, metadata !"sigval", i32 32, i64 64, i64 64, i64 0, i32 0, null, metadata !255, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [sigval] [line 32, size 64, align 64, offset 0] [from ]
!255 = metadata !{metadata !256, metadata !257}
!256 = metadata !{i32 786445, metadata !227, metadata !254, metadata !"sival_int", i32 34, i64 32, i64 32, i64 0, i32 0, metadata !69} ; [ DW_TAG_member ] [sival_int] [line 34, size 32, align 32, offset 0] [from int]
!257 = metadata !{i32 786445, metadata !227, metadata !254, metadata !"sival_ptr", i32 35, i64 64, i64 64, i64 0, i32 0, metadata !80} ; [ DW_TAG_member ] [sival_ptr] [line 35, size 64, align 64, offset 0] [from ]
!258 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_rt", i32 94, i64 128, i64 64, i64 0, i32 0, metadata !259} ; [ DW_TAG_member ] [_rt] [line 94, size 128, align 64, offset 0] [from ]
!259 = metadata !{i32 786451, metadata !227, metadata !233, metadata !"", i32 89, i64 128, i64 64, i32 0, i32 0, null, metadata !260, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 89, size 128, align 64, offset 0] [from ]
!260 = metadata !{metadata !261, metadata !262, metadata !263}
!261 = metadata !{i32 786445, metadata !227, metadata !259, metadata !"si_pid", i32 91, i64 32, i64 32, i64 0, i32 0, metadata !243} ; [ DW_TAG_member ] [si_pid] [line 91, size 32, align 32, offset 0] [from __pid_t]
!262 = metadata !{i32 786445, metadata !227, metadata !259, metadata !"si_uid", i32 92, i64 32, i64 32, i64 32, i32 0, metadata !245} ; [ DW_TAG_member ] [si_uid] [line 92, size 32, align 32, offset 32] [from __uid_t]
!263 = metadata !{i32 786445, metadata !227, metadata !259, metadata !"si_sigval", i32 93, i64 64, i64 64, i64 64, i32 0, metadata !253} ; [ DW_TAG_member ] [si_sigval] [line 93, size 64, align 64, offset 64] [from sigval_t]
!264 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_sigchld", i32 104, i64 256, i64 64, i64 0, i32 0, metadata !265} ; [ DW_TAG_member ] [_sigchld] [line 104, size 256, align 64, offset 0] [from ]
!265 = metadata !{i32 786451, metadata !227, metadata !233, metadata !"", i32 97, i64 256, i64 64, i32 0, i32 0, null, metadata !266, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 97, size 256, align 64, offset 0] [from ]
!266 = metadata !{metadata !267, metadata !268, metadata !269, metadata !270, metadata !273}
!267 = metadata !{i32 786445, metadata !227, metadata !265, metadata !"si_pid", i32 99, i64 32, i64 32, i64 0, i32 0, metadata !243} ; [ DW_TAG_member ] [si_pid] [line 99, size 32, align 32, offset 0] [from __pid_t]
!268 = metadata !{i32 786445, metadata !227, metadata !265, metadata !"si_uid", i32 100, i64 32, i64 32, i64 32, i32 0, metadata !245} ; [ DW_TAG_member ] [si_uid] [line 100, size 32, align 32, offset 32] [from __uid_t]
!269 = metadata !{i32 786445, metadata !227, metadata !265, metadata !"si_status", i32 101, i64 32, i64 32, i64 64, i32 0, metadata !69} ; [ DW_TAG_member ] [si_status] [line 101, size 32, align 32, offset 64] [from int]
!270 = metadata !{i32 786445, metadata !227, metadata !265, metadata !"si_utime", i32 102, i64 64, i64 64, i64 128, i32 0, metadata !271} ; [ DW_TAG_member ] [si_utime] [line 102, size 64, align 64, offset 128] [from __sigchld_clock_t]
!271 = metadata !{i32 786454, metadata !227, null, metadata !"__sigchld_clock_t", i32 58, i64 0, i64 0, i64 0, i32 0, metadata !272} ; [ DW_TAG_typedef ] [__sigchld_clock_t] [line 58, size 0, align 0, offset 0] [from __clock_t]
!272 = metadata !{i32 786454, metadata !227, null, metadata !"__clock_t", i32 135, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_typedef ] [__clock_t] [line 135, size 0, align 0, offset 0] [from long int]
!273 = metadata !{i32 786445, metadata !227, metadata !265, metadata !"si_stime", i32 103, i64 64, i64 64, i64 192, i32 0, metadata !271} ; [ DW_TAG_member ] [si_stime] [line 103, size 64, align 64, offset 192] [from __sigchld_clock_t]
!274 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_sigfault", i32 111, i64 128, i64 64, i64 0, i32 0, metadata !275} ; [ DW_TAG_member ] [_sigfault] [line 111, size 128, align 64, offset 0] [from ]
!275 = metadata !{i32 786451, metadata !227, metadata !233, metadata !"", i32 107, i64 128, i64 64, i32 0, i32 0, null, metadata !276, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 107, size 128, align 64, offset 0] [from ]
!276 = metadata !{metadata !277, metadata !278}
!277 = metadata !{i32 786445, metadata !227, metadata !275, metadata !"si_addr", i32 109, i64 64, i64 64, i64 0, i32 0, metadata !80} ; [ DW_TAG_member ] [si_addr] [line 109, size 64, align 64, offset 0] [from ]
!278 = metadata !{i32 786445, metadata !227, metadata !275, metadata !"si_addr_lsb", i32 110, i64 16, i64 16, i64 64, i32 0, metadata !279} ; [ DW_TAG_member ] [si_addr_lsb] [line 110, size 16, align 16, offset 64] [from short]
!279 = metadata !{i32 786468, null, null, metadata !"short", i32 0, i64 16, i64 16, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [short] [line 0, size 16, align 16, offset 0, enc DW_ATE_signed]
!280 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_sigpoll", i32 118, i64 128, i64 64, i64 0, i32 0, metadata !281} ; [ DW_TAG_member ] [_sigpoll] [line 118, size 128, align 64, offset 0] [from ]
!281 = metadata !{i32 786451, metadata !227, metadata !233, metadata !"", i32 114, i64 128, i64 64, i32 0, i32 0, null, metadata !282, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 114, size 128, align 64, offset 0] [from ]
!282 = metadata !{metadata !283, metadata !284}
!283 = metadata !{i32 786445, metadata !227, metadata !281, metadata !"si_band", i32 116, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [si_band] [line 116, size 64, align 64, offset 0] [from long int]
!284 = metadata !{i32 786445, metadata !227, metadata !281, metadata !"si_fd", i32 117, i64 32, i64 32, i64 64, i32 0, metadata !69} ; [ DW_TAG_member ] [si_fd] [line 117, size 32, align 32, offset 64] [from int]
!285 = metadata !{i32 786445, metadata !227, metadata !233, metadata !"_sigsys", i32 126, i64 128, i64 64, i64 0, i32 0, metadata !286} ; [ DW_TAG_member ] [_sigsys] [line 126, size 128, align 64, offset 0] [from ]
!286 = metadata !{i32 786451, metadata !227, metadata !233, metadata !"", i32 121, i64 128, i64 64, i32 0, i32 0, null, metadata !287, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 121, size 128, align 64, offset 0] [from ]
!287 = metadata !{metadata !288, metadata !289, metadata !290}
!288 = metadata !{i32 786445, metadata !227, metadata !286, metadata !"_call_addr", i32 123, i64 64, i64 64, i64 0, i32 0, metadata !80} ; [ DW_TAG_member ] [_call_addr] [line 123, size 64, align 64, offset 0] [from ]
!289 = metadata !{i32 786445, metadata !227, metadata !286, metadata !"_syscall", i32 124, i64 32, i64 32, i64 64, i32 0, metadata !69} ; [ DW_TAG_member ] [_syscall] [line 124, size 32, align 32, offset 64] [from int]
!290 = metadata !{i32 786445, metadata !227, metadata !286, metadata !"_arch", i32 125, i64 32, i64 32, i64 96, i32 0, metadata !246} ; [ DW_TAG_member ] [_arch] [line 125, size 32, align 32, offset 96] [from unsigned int]
!291 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVInit", metadata !"GVInit", metadata !"", i32 302, metadata !125, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, null, null, null, metadata !19, i32 303} ; [ DW_TAG_subprogram ] [line 302] [local] [def] [scope 303] [GVInit]
!292 = metadata !{metadata !293, metadata !294, metadata !295, metadata !296, metadata !297, metadata !298, metadata !300, metadata !304, metadata !308, metadata !325}
!293 = metadata !{i32 786484, i32 0, null, metadata !"StartTally", metadata !"StartTally", metadata !"", metadata !22, i32 498, metadata !59, i32 0, i32 1, i64* @StartTally, null} ; [ DW_TAG_variable ] [StartTally] [line 498] [def]
!294 = metadata !{i32 786484, i32 0, null, metadata !"AbortTally", metadata !"AbortTally", metadata !"", metadata !22, i32 499, metadata !59, i32 0, i32 1, i64* @AbortTally, null} ; [ DW_TAG_variable ] [AbortTally] [line 499] [def]
!295 = metadata !{i32 786484, i32 0, null, metadata !"ReadOverflowTally", metadata !"ReadOverflowTally", metadata !"", metadata !22, i32 500, metadata !59, i32 0, i32 1, i64* @ReadOverflowTally, null} ; [ DW_TAG_variable ] [ReadOverflowTally] [line 500] [def]
!296 = metadata !{i32 786484, i32 0, null, metadata !"WriteOverflowTally", metadata !"WriteOverflowTally", metadata !"", metadata !22, i32 501, metadata !59, i32 0, i32 1, i64* @WriteOverflowTally, null} ; [ DW_TAG_variable ] [WriteOverflowTally] [line 501] [def]
!297 = metadata !{i32 786484, i32 0, null, metadata !"LocalOverflowTally", metadata !"LocalOverflowTally", metadata !"", metadata !22, i32 502, metadata !59, i32 0, i32 1, i64* @LocalOverflowTally, null} ; [ DW_TAG_variable ] [LocalOverflowTally] [line 502] [def]
!298 = metadata !{i32 786484, i32 0, null, metadata !"global_key_self", metadata !"global_key_self", metadata !"", metadata !22, i32 189, metadata !299, i32 1, i32 1, i32* @global_key_self, null} ; [ DW_TAG_variable ] [global_key_self] [line 189] [local] [def]
!299 = metadata !{i32 786454, metadata !1, null, metadata !"pthread_key_t", i32 163, i64 0, i64 0, i64 0, i32 0, metadata !246} ; [ DW_TAG_typedef ] [pthread_key_t] [line 163, size 0, align 0, offset 0] [from unsigned int]
!300 = metadata !{i32 786484, i32 0, null, metadata !"LockTab", metadata !"LockTab", metadata !"", metadata !22, i32 285, metadata !301, i32 1, i32 1, [1048576 x i64]* @LockTab, null} ; [ DW_TAG_variable ] [LockTab] [line 285] [local] [def]
!301 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 67108864, i64 64, i32 0, i32 0, metadata !26, metadata !302, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 67108864, align 64, offset 0] [from ]
!302 = metadata !{metadata !303}
!303 = metadata !{i32 786465, i64 0, i64 1048576} ; [ DW_TAG_subrange_type ] [0, 1048575]
!304 = metadata !{i32 786484, i32 0, null, metadata !"GClock", metadata !"GClock", metadata !"", metadata !22, i32 293, metadata !305, i32 1, i32 1, [64 x i64]* @GClock, null} ; [ DW_TAG_variable ] [GClock] [line 293] [local] [def]
!305 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 4096, i64 64, i32 0, i32 0, metadata !26, metadata !306, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 4096, align 64, offset 0] [from ]
!306 = metadata !{metadata !307}
!307 = metadata !{i32 786465, i64 0, i64 64}      ; [ DW_TAG_subrange_type ] [0, 63]
!308 = metadata !{i32 786484, i32 0, null, metadata !"global_act_oldsigsegv", metadata !"global_act_oldsigsegv", metadata !"", metadata !22, i32 191, metadata !309, i32 1, i32 1, %struct.sigaction* @global_act_oldsigsegv, null} ; [ DW_TAG_variable ] [global_act_oldsigsegv] [line 191] [local] [def]
!309 = metadata !{i32 786451, metadata !225, null, metadata !"sigaction", i32 24, i64 1216, i64 64, i32 0, i32 0, null, metadata !310, i32 0, null, null} ; [ DW_TAG_structure_type ] [sigaction] [line 24, size 1216, align 64, offset 0] [from ]
!310 = metadata !{metadata !311, metadata !321, metadata !322, metadata !323}
!311 = metadata !{i32 786445, metadata !225, metadata !309, metadata !"__sigaction_handler", i32 35, i64 64, i64 64, i64 0, i32 0, metadata !312} ; [ DW_TAG_member ] [__sigaction_handler] [line 35, size 64, align 64, offset 0] [from ]
!312 = metadata !{i32 786455, metadata !225, metadata !309, metadata !"", i32 28, i64 64, i64 64, i64 0, i32 0, null, metadata !313, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [line 28, size 64, align 64, offset 0] [from ]
!313 = metadata !{metadata !314, metadata !319}
!314 = metadata !{i32 786445, metadata !225, metadata !312, metadata !"sa_handler", i32 31, i64 64, i64 64, i64 0, i32 0, metadata !315} ; [ DW_TAG_member ] [sa_handler] [line 31, size 64, align 64, offset 0] [from __sighandler_t]
!315 = metadata !{i32 786454, metadata !225, null, metadata !"__sighandler_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !316} ; [ DW_TAG_typedef ] [__sighandler_t] [line 85, size 0, align 0, offset 0] [from ]
!316 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !317} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!317 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !318, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!318 = metadata !{null, metadata !69}
!319 = metadata !{i32 786445, metadata !225, metadata !312, metadata !"sa_sigaction", i32 33, i64 64, i64 64, i64 0, i32 0, metadata !320} ; [ DW_TAG_member ] [sa_sigaction] [line 33, size 64, align 64, offset 0] [from ]
!320 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !221} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!321 = metadata !{i32 786445, metadata !225, metadata !309, metadata !"sa_mask", i32 43, i64 1024, i64 64, i64 64, i32 0, metadata !109} ; [ DW_TAG_member ] [sa_mask] [line 43, size 1024, align 64, offset 64] [from __sigset_t]
!322 = metadata !{i32 786445, metadata !225, metadata !309, metadata !"sa_flags", i32 46, i64 32, i64 32, i64 1088, i32 0, metadata !69} ; [ DW_TAG_member ] [sa_flags] [line 46, size 32, align 32, offset 1088] [from int]
!323 = metadata !{i32 786445, metadata !225, metadata !309, metadata !"sa_restorer", i32 49, i64 64, i64 64, i64 1152, i32 0, metadata !324} ; [ DW_TAG_member ] [sa_restorer] [line 49, size 64, align 64, offset 1152] [from ]
!324 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !125} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!325 = metadata !{i32 786484, i32 0, null, metadata !"global_act_oldsigbus", metadata !"global_act_oldsigbus", metadata !"", metadata !22, i32 190, metadata !309, i32 1, i32 1, %struct.sigaction* @global_act_oldsigbus, null} ; [ DW_TAG_variable ] [global_act_oldsigbus] [line 190] [local] [def]
!326 = metadata !{i32 786689, metadata !21, metadata !"Addr", metadata !22, i32 16777790, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 574]
!327 = metadata !{i32 574, i32 0, metadata !21, null}
!328 = metadata !{i32 576, i32 0, metadata !21, null}
!329 = metadata !{i32 786689, metadata !34, metadata !"k", metadata !22, i32 16777832, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 616]
!330 = metadata !{i32 616, i32 0, metadata !34, null}
!331 = metadata !{i32 786689, metadata !34, metadata !"sz", metadata !22, i32 33555048, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [sz] [line 616]
!332 = metadata !{i32 786688, metadata !34, metadata !"e", metadata !22, i32 619, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 619]
!333 = metadata !{i32 619, i32 0, metadata !34, null}
!334 = metadata !{i32 620, i32 0, metadata !34, null}
!335 = metadata !{i32 621, i32 0, metadata !336, null}
!336 = metadata !{i32 786443, metadata !1, metadata !34, i32 620, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!337 = metadata !{i32 786688, metadata !338, metadata !"tmp", metadata !22, i32 622, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [tmp] [line 622]
!338 = metadata !{i32 786443, metadata !1, metadata !336, i32 621, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!339 = metadata !{i32 622, i32 0, metadata !338, null}
!340 = metadata !{i32 623, i32 0, metadata !338, null}
!341 = metadata !{i32 624, i32 0, metadata !338, null}
!342 = metadata !{i32 625, i32 0, metadata !338, null}
!343 = metadata !{i32 626, i32 0, metadata !336, null}
!344 = metadata !{i32 629, i32 0, metadata !34, null}
!345 = metadata !{i32 630, i32 0, metadata !34, null}
!346 = metadata !{i32 786688, metadata !347, metadata !"a", metadata !22, i32 1016, metadata !348, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 1016]
!347 = metadata !{i32 786443, metadata !1, metadata !124, i32 1016, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!348 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 32, i64 32, i32 0, i32 0, metadata !69, metadata !77, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 32, align 32, offset 0] [from int]
!349 = metadata !{i32 1016, i32 0, metadata !347, null}
!350 = metadata !{i32 304, i32 0, metadata !291, metadata !351}
!351 = metadata !{i32 1018, i32 0, metadata !124, null}
!352 = metadata !{i32 1019, i32 0, metadata !124, null}
!353 = metadata !{i32 1021, i32 0, metadata !124, null}
!354 = metadata !{i32 1022, i32 0, metadata !124, null}
!355 = metadata !{i32 1024, i32 0, metadata !124, null}
!356 = metadata !{i32 1034, i32 0, metadata !127, null}
!357 = metadata !{i32 1049, i32 0, metadata !127, null}
!358 = metadata !{i32 1051, i32 0, metadata !127, null}
!359 = metadata !{i32 1053, i32 0, metadata !127, null}
!360 = metadata !{i32 -2146797205}
!361 = metadata !{i32 1054, i32 0, metadata !127, null}
!362 = metadata !{i32 786688, metadata !128, metadata !"t", metadata !22, i32 1066, metadata !131, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [t] [line 1066]
!363 = metadata !{i32 1066, i32 0, metadata !128, null}
!364 = metadata !{i32 1067, i32 0, metadata !128, null}
!365 = metadata !{i32 1071, i32 0, metadata !128, null}
!366 = metadata !{i32 786689, metadata !133, metadata !"t", metadata !22, i32 16778298, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [t] [line 1082]
!367 = metadata !{i32 1082, i32 0, metadata !133, null}
!368 = metadata !{i32 1084, i32 0, metadata !133, null}
!369 = metadata !{i32 786689, metadata !215, metadata !"addr", metadata !22, i32 16777470, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [addr] [line 254]
!370 = metadata !{i32 254, i32 0, metadata !215, metadata !368}
!371 = metadata !{i32 786689, metadata !215, metadata !"dx", metadata !22, i32 33554686, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dx] [line 254]
!372 = metadata !{i32 786688, metadata !215, metadata !"v", metadata !22, i32 256, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 256]
!373 = metadata !{i32 256, i32 0, metadata !215, metadata !368}
!374 = metadata !{i32 257, i32 0, metadata !375, metadata !368}
!375 = metadata !{i32 786443, metadata !1, metadata !215, i32 257, i32 0, i32 55} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!376 = metadata !{i32 257, i32 21, metadata !375, metadata !368}
!377 = metadata !{i32 786689, metadata !191, metadata !"newVal", metadata !193, i32 16777258, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [newVal] [line 42]
!378 = metadata !{i32 42, i32 0, metadata !191, metadata !376}
!379 = metadata !{i32 786689, metadata !191, metadata !"oldVal", metadata !193, i32 33554474, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [oldVal] [line 42]
!380 = metadata !{i32 786689, metadata !191, metadata !"ptr", metadata !193, i32 50331690, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ptr] [line 42]
!381 = metadata !{i32 786688, metadata !382, metadata !"prevVal", metadata !193, i32 44, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [prevVal] [line 44]
!382 = metadata !{i32 786443, metadata !192, metadata !191} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/./platform_x86.h]
!383 = metadata !{i32 44, i32 0, metadata !382, metadata !376}
!384 = metadata !{i32 46, i32 0, metadata !382, metadata !376}
!385 = metadata !{i32 584213, i32 584248}
!386 = metadata !{i32 58, i32 0, metadata !382, metadata !376} ; [ DW_TAG_imported_module ]
!387 = metadata !{i32 258, i32 0, metadata !215, metadata !368}
!388 = metadata !{i32 786688, metadata !133, metadata !"wrSetOvf", metadata !22, i32 1086, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wrSetOvf] [line 1086]
!389 = metadata !{i32 1086, i32 0, metadata !133, null}
!390 = metadata !{i32 786688, metadata !133, metadata !"wr", metadata !22, i32 1087, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1087]
!391 = metadata !{i32 1087, i32 0, metadata !133, null}
!392 = metadata !{i32 1094, i32 0, metadata !133, null}
!393 = metadata !{i32 1097, i32 0, metadata !394, null}
!394 = metadata !{i32 786443, metadata !1, metadata !133, i32 1096, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!395 = metadata !{i32 1099, i32 0, metadata !133, null}
!396 = metadata !{i32 254, i32 0, metadata !215, metadata !395}
!397 = metadata !{i32 256, i32 0, metadata !215, metadata !395}
!398 = metadata !{i32 257, i32 0, metadata !375, metadata !395}
!399 = metadata !{i32 257, i32 21, metadata !375, metadata !395}
!400 = metadata !{i32 42, i32 0, metadata !191, metadata !399}
!401 = metadata !{i32 44, i32 0, metadata !382, metadata !399}
!402 = metadata !{i32 46, i32 0, metadata !382, metadata !399}
!403 = metadata !{i32 58, i32 0, metadata !382, metadata !399} ; [ DW_TAG_imported_module ]
!404 = metadata !{i32 258, i32 0, metadata !215, metadata !395}
!405 = metadata !{i32 1101, i32 0, metadata !133, null}
!406 = metadata !{i32 254, i32 0, metadata !215, metadata !405}
!407 = metadata !{i32 256, i32 0, metadata !215, metadata !405}
!408 = metadata !{i32 257, i32 0, metadata !375, metadata !405}
!409 = metadata !{i32 257, i32 21, metadata !375, metadata !405}
!410 = metadata !{i32 42, i32 0, metadata !191, metadata !409}
!411 = metadata !{i32 44, i32 0, metadata !382, metadata !409}
!412 = metadata !{i32 46, i32 0, metadata !382, metadata !409}
!413 = metadata !{i32 58, i32 0, metadata !382, metadata !409} ; [ DW_TAG_imported_module ]
!414 = metadata !{i32 258, i32 0, metadata !215, metadata !405}
!415 = metadata !{i32 1103, i32 0, metadata !133, null}
!416 = metadata !{i32 254, i32 0, metadata !215, metadata !415}
!417 = metadata !{i32 256, i32 0, metadata !215, metadata !415}
!418 = metadata !{i32 257, i32 0, metadata !375, metadata !415}
!419 = metadata !{i32 257, i32 21, metadata !375, metadata !415}
!420 = metadata !{i32 42, i32 0, metadata !191, metadata !419}
!421 = metadata !{i32 44, i32 0, metadata !382, metadata !419}
!422 = metadata !{i32 46, i32 0, metadata !382, metadata !419}
!423 = metadata !{i32 58, i32 0, metadata !382, metadata !419} ; [ DW_TAG_imported_module ]
!424 = metadata !{i32 258, i32 0, metadata !215, metadata !415}
!425 = metadata !{i32 1104, i32 0, metadata !133, null}
!426 = metadata !{i32 254, i32 0, metadata !215, metadata !425}
!427 = metadata !{i32 256, i32 0, metadata !215, metadata !425}
!428 = metadata !{i32 257, i32 0, metadata !375, metadata !425}
!429 = metadata !{i32 257, i32 21, metadata !375, metadata !425}
!430 = metadata !{i32 42, i32 0, metadata !191, metadata !429}
!431 = metadata !{i32 44, i32 0, metadata !382, metadata !429}
!432 = metadata !{i32 46, i32 0, metadata !382, metadata !429}
!433 = metadata !{i32 58, i32 0, metadata !382, metadata !429} ; [ DW_TAG_imported_module ]
!434 = metadata !{i32 258, i32 0, metadata !215, metadata !425}
!435 = metadata !{i32 1106, i32 0, metadata !133, null}
!436 = metadata !{i32 1107, i32 0, metadata !133, null}
!437 = metadata !{i32 1109, i32 0, metadata !133, null}
!438 = metadata !{i32 1113, i32 0, metadata !133, null}
!439 = metadata !{i32 1115, i32 0, metadata !133, null}
!440 = metadata !{i32 1117, i32 0, metadata !133, null}
!441 = metadata !{i32 1118, i32 0, metadata !133, null}
!442 = metadata !{i32 786689, metadata !136, metadata !"t", metadata !22, i32 16778342, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [t] [line 1126]
!443 = metadata !{i32 1126, i32 0, metadata !136, null}
!444 = metadata !{i32 786689, metadata !136, metadata !"id", metadata !22, i32 33555558, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [id] [line 1126]
!445 = metadata !{i32 1129, i32 0, metadata !136, null}
!446 = metadata !{i32 1131, i32 0, metadata !136, null}
!447 = metadata !{i32 1133, i32 0, metadata !136, null}
!448 = metadata !{i32 1134, i32 0, metadata !136, null}
!449 = metadata !{i32 1135, i32 0, metadata !136, null}
!450 = metadata !{i32 1140, i32 21, metadata !136, null}
!451 = metadata !{i32 786689, metadata !212, metadata !"sz", metadata !22, i32 16777804, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [sz] [line 588]
!452 = metadata !{i32 588, i32 0, metadata !212, metadata !450}
!453 = metadata !{i32 786689, metadata !212, metadata !"Self", metadata !22, i32 33555020, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 588]
!454 = metadata !{i32 786688, metadata !212, metadata !"ap", metadata !22, i32 590, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ap] [line 590]
!455 = metadata !{i32 590, i32 0, metadata !212, metadata !450}
!456 = metadata !{i32 591, i32 0, metadata !212, metadata !450}
!457 = metadata !{i32 592, i32 0, metadata !212, metadata !450}
!458 = metadata !{i32 786688, metadata !212, metadata !"List", metadata !22, i32 593, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [List] [line 593]
!459 = metadata !{i32 593, i32 0, metadata !212, metadata !450}
!460 = metadata !{i32 786688, metadata !212, metadata !"Tail", metadata !22, i32 594, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Tail] [line 594]
!461 = metadata !{i32 594, i32 0, metadata !212, metadata !450}
!462 = metadata !{i32 786688, metadata !212, metadata !"i", metadata !22, i32 595, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 595]
!463 = metadata !{i32 595, i32 0, metadata !212, metadata !450}
!464 = metadata !{i32 596, i32 0, metadata !465, metadata !450}
!465 = metadata !{i32 786443, metadata !1, metadata !212, i32 596, i32 0, i32 53} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!466 = metadata !{i32 786688, metadata !467, metadata !"e", metadata !22, i32 597, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 597]
!467 = metadata !{i32 786443, metadata !1, metadata !465, i32 596, i32 0, i32 54} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!468 = metadata !{i32 597, i32 0, metadata !467, metadata !450}
!469 = metadata !{i32 598, i32 0, metadata !467, metadata !450}
!470 = metadata !{i32 599, i32 0, metadata !467, metadata !450}
!471 = metadata !{i32 600, i32 0, metadata !467, metadata !450}
!472 = metadata !{i32 601, i32 0, metadata !467, metadata !450}
!473 = metadata !{i32 602, i32 0, metadata !467, metadata !450}
!474 = metadata !{i32 604, i32 0, metadata !212, metadata !450}
!475 = metadata !{i32 606, i32 0, metadata !212, metadata !450}
!476 = metadata !{i32 1141, i32 0, metadata !136, null}
!477 = metadata !{i32 1144, i32 21, metadata !136, null}
!478 = metadata !{i32 588, i32 0, metadata !212, metadata !477}
!479 = metadata !{i32 590, i32 0, metadata !212, metadata !477}
!480 = metadata !{i32 591, i32 0, metadata !212, metadata !477}
!481 = metadata !{i32 592, i32 0, metadata !212, metadata !477}
!482 = metadata !{i32 593, i32 0, metadata !212, metadata !477}
!483 = metadata !{i32 594, i32 0, metadata !212, metadata !477}
!484 = metadata !{i32 595, i32 0, metadata !212, metadata !477}
!485 = metadata !{i32 596, i32 0, metadata !465, metadata !477}
!486 = metadata !{i32 597, i32 0, metadata !467, metadata !477}
!487 = metadata !{i32 598, i32 0, metadata !467, metadata !477}
!488 = metadata !{i32 599, i32 0, metadata !467, metadata !477}
!489 = metadata !{i32 600, i32 0, metadata !467, metadata !477}
!490 = metadata !{i32 601, i32 0, metadata !467, metadata !477}
!491 = metadata !{i32 602, i32 0, metadata !467, metadata !477}
!492 = metadata !{i32 604, i32 0, metadata !212, metadata !477}
!493 = metadata !{i32 606, i32 0, metadata !212, metadata !477}
!494 = metadata !{i32 1145, i32 0, metadata !136, null}
!495 = metadata !{i32 1147, i32 25, metadata !136, null}
!496 = metadata !{i32 588, i32 0, metadata !212, metadata !495}
!497 = metadata !{i32 590, i32 0, metadata !212, metadata !495}
!498 = metadata !{i32 591, i32 0, metadata !212, metadata !495}
!499 = metadata !{i32 592, i32 0, metadata !212, metadata !495}
!500 = metadata !{i32 593, i32 0, metadata !212, metadata !495}
!501 = metadata !{i32 594, i32 0, metadata !212, metadata !495}
!502 = metadata !{i32 595, i32 0, metadata !212, metadata !495}
!503 = metadata !{i32 596, i32 0, metadata !465, metadata !495}
!504 = metadata !{i32 597, i32 0, metadata !467, metadata !495}
!505 = metadata !{i32 598, i32 0, metadata !467, metadata !495}
!506 = metadata !{i32 599, i32 0, metadata !467, metadata !495}
!507 = metadata !{i32 600, i32 0, metadata !467, metadata !495}
!508 = metadata !{i32 601, i32 0, metadata !467, metadata !495}
!509 = metadata !{i32 602, i32 0, metadata !467, metadata !495}
!510 = metadata !{i32 604, i32 0, metadata !212, metadata !495}
!511 = metadata !{i32 606, i32 0, metadata !212, metadata !495}
!512 = metadata !{i32 1148, i32 0, metadata !136, null}
!513 = metadata !{i32 1150, i32 0, metadata !136, null}
!514 = metadata !{i32 1151, i32 0, metadata !136, null}
!515 = metadata !{i32 1152, i32 0, metadata !136, null}
!516 = metadata !{i32 1153, i32 0, metadata !136, null}
!517 = metadata !{i32 1156, i32 0, metadata !136, null}
!518 = metadata !{i32 1158, i32 0, metadata !136, null}
!519 = metadata !{i32 786689, metadata !139, metadata !"Self", metadata !22, i32 16778920, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1704]
!520 = metadata !{i32 1704, i32 0, metadata !139, null}
!521 = metadata !{i32 1708, i32 0, metadata !139, null}
!522 = metadata !{i32 1711, i32 0, metadata !139, null}
!523 = metadata !{i32 786689, metadata !209, metadata !"k", metadata !22, i32 16777988, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 772]
!524 = metadata !{i32 772, i32 0, metadata !209, metadata !522}
!525 = metadata !{i32 786688, metadata !209, metadata !"e", metadata !22, i32 774, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 774]
!526 = metadata !{i32 774, i32 0, metadata !209, metadata !522}
!527 = metadata !{i32 775, i32 0, metadata !528, metadata !522}
!528 = metadata !{i32 786443, metadata !1, metadata !209, i32 775, i32 0, i32 51} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!529 = metadata !{i32 776, i32 0, metadata !530, metadata !522}
!530 = metadata !{i32 786443, metadata !1, metadata !528, i32 775, i32 0, i32 52} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!531 = metadata !{i32 1712, i32 0, metadata !139, null}
!532 = metadata !{i32 786689, metadata !208, metadata !"Self", metadata !22, i32 16778555, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1339]
!533 = metadata !{i32 1339, i32 0, metadata !208, metadata !531}
!534 = metadata !{i32 786688, metadata !208, metadata !"wr", metadata !22, i32 1348, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1348]
!535 = metadata !{i32 1348, i32 0, metadata !208, metadata !531}
!536 = metadata !{i32 786688, metadata !537, metadata !"p", metadata !22, i32 1351, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1351]
!537 = metadata !{i32 786443, metadata !1, metadata !208, i32 1350, i32 0, i32 48} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!538 = metadata !{i32 1351, i32 0, metadata !537, metadata !531}
!539 = metadata !{i32 786688, metadata !537, metadata !"End", metadata !22, i32 1352, metadata !540, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1352]
!540 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !42} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!541 = metadata !{i32 1352, i32 0, metadata !537, metadata !531}
!542 = metadata !{i32 1353, i32 0, metadata !543, metadata !531}
!543 = metadata !{i32 786443, metadata !1, metadata !537, i32 1353, i32 0, i32 49} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!544 = metadata !{i32 1367, i32 0, metadata !545, metadata !531}
!545 = metadata !{i32 786443, metadata !1, metadata !543, i32 1353, i32 0, i32 50} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!546 = metadata !{i32 1720, i32 0, metadata !139, null}
!547 = metadata !{i32 1721, i32 0, metadata !548, null}
!548 = metadata !{i32 786443, metadata !1, metadata !139, i32 1720, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!549 = metadata !{i32 772, i32 0, metadata !209, metadata !547}
!550 = metadata !{i32 774, i32 0, metadata !209, metadata !547}
!551 = metadata !{i32 775, i32 0, metadata !528, metadata !547}
!552 = metadata !{i32 776, i32 0, metadata !530, metadata !547}
!553 = metadata !{i32 1722, i32 0, metadata !548, null}
!554 = metadata !{i32 1724, i32 0, metadata !139, null}
!555 = metadata !{i32 1725, i32 0, metadata !139, null}
!556 = metadata !{i32 1727, i32 9, metadata !139, null}
!557 = metadata !{i32 786689, metadata !207, metadata !"Self", metadata !22, i32 16777691, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 475]
!558 = metadata !{i32 475, i32 0, metadata !207, metadata !556}
!559 = metadata !{i32 1729, i32 0, metadata !560, null}
!560 = metadata !{i32 786443, metadata !1, metadata !139, i32 1727, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!561 = metadata !{i32 1749, i32 0, metadata !139, null}
!562 = metadata !{i32 1750, i32 0, metadata !563, null}
!563 = metadata !{i32 786443, metadata !1, metadata !139, i32 1749, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!564 = metadata !{i32 786689, metadata !196, metadata !"Self", metadata !22, i32 16778643, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1427]
!565 = metadata !{i32 1427, i32 0, metadata !196, metadata !562}
!566 = metadata !{i32 786689, metadata !196, metadata !"attempt", metadata !22, i32 33555859, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [attempt] [line 1427]
!567 = metadata !{i32 786688, metadata !568, metadata !"stall", metadata !22, i32 1433, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [stall] [line 1433]
!568 = metadata !{i32 786443, metadata !1, metadata !196} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!569 = metadata !{i32 1433, i32 0, metadata !568, metadata !562}
!570 = metadata !{i32 1433, i32 32, metadata !568, metadata !562}
!571 = metadata !{i32 786689, metadata !197, metadata !"Self", metadata !22, i32 16777459, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 243]
!572 = metadata !{i32 243, i32 0, metadata !197, metadata !570}
!573 = metadata !{i32 245, i32 12, metadata !197, metadata !570}
!574 = metadata !{i32 786689, metadata !200, metadata !"seed", metadata !22, i32 16777446, metadata !203, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [seed] [line 230]
!575 = metadata !{i32 230, i32 0, metadata !200, metadata !573}
!576 = metadata !{i32 786688, metadata !200, metadata !"x", metadata !22, i32 232, metadata !74, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 232]
!577 = metadata !{i32 232, i32 0, metadata !200, metadata !573}
!578 = metadata !{i32 232, i32 28, metadata !200, metadata !573}
!579 = metadata !{i32 786689, metadata !204, metadata !"x", metadata !22, i32 16777426, metadata !74, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [x] [line 210]
!580 = metadata !{i32 210, i32 0, metadata !204, metadata !578}
!581 = metadata !{i32 212, i32 0, metadata !204, metadata !578}
!582 = metadata !{i32 213, i32 0, metadata !583, metadata !578}
!583 = metadata !{i32 786443, metadata !1, metadata !204, i32 212, i32 0, i32 47} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!584 = metadata !{i32 214, i32 0, metadata !583, metadata !578}
!585 = metadata !{i32 215, i32 0, metadata !204, metadata !578}
!586 = metadata !{i32 216, i32 0, metadata !204, metadata !578}
!587 = metadata !{i32 217, i32 0, metadata !204, metadata !578}
!588 = metadata !{i32 218, i32 0, metadata !204, metadata !578}
!589 = metadata !{i32 233, i32 0, metadata !200, metadata !573}
!590 = metadata !{i32 234, i32 0, metadata !200, metadata !573}
!591 = metadata !{i32 1434, i32 0, metadata !568, metadata !562}
!592 = metadata !{i32 1435, i32 0, metadata !568, metadata !562}
!593 = metadata !{i32 786688, metadata !568, metadata !"i", metadata !22, i32 1444, metadata !594, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 1444]
!594 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !74} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from long long unsigned int]
!595 = metadata !{i32 1444, i32 0, metadata !568, metadata !562}
!596 = metadata !{i32 1445, i32 0, metadata !568, metadata !562}
!597 = metadata !{i32 1447, i32 0, metadata !598, metadata !562}
!598 = metadata !{i32 786443, metadata !1, metadata !568, i32 1445, i32 0, i32 46} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!599 = metadata !{i32 1751, i32 0, metadata !563, null}
!600 = metadata !{i32 1756, i32 0, metadata !139, null}
!601 = metadata !{i32 1757, i32 0, metadata !139, null}
!602 = metadata !{i32 1760, i32 0, metadata !139, null}
!603 = metadata !{i32 1762, i32 0, metadata !139, null}
!604 = metadata !{i32 786689, metadata !140, metadata !"Self", metadata !22, i32 16778987, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1771]
!605 = metadata !{i32 1771, i32 0, metadata !140, null}
!606 = metadata !{i32 786689, metadata !140, metadata !"addr", metadata !22, i32 33556203, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [addr] [line 1771]
!607 = metadata !{i32 786689, metadata !140, metadata !"valu", metadata !22, i32 50333419, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [valu] [line 1771]
!608 = metadata !{i32 1776, i32 0, metadata !140, null}
!609 = metadata !{i32 1777, i32 0, metadata !610, null}
!610 = metadata !{i32 786443, metadata !1, metadata !140, i32 1776, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!611 = metadata !{i32 1779, i32 0, metadata !610, null}
!612 = metadata !{i32 1781, i32 0, metadata !610, null}
!613 = metadata !{i32 786688, metadata !140, metadata !"LockFor", metadata !22, i32 1795, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 1795]
!614 = metadata !{i32 1795, i32 0, metadata !140, null}
!615 = metadata !{i32 786688, metadata !140, metadata !"cv", metadata !22, i32 1796, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [cv] [line 1796]
!616 = metadata !{i32 1796, i32 0, metadata !140, null}
!617 = metadata !{i32 1798, i32 0, metadata !140, null}
!618 = metadata !{i32 1802, i32 0, metadata !619, null}
!619 = metadata !{i32 786443, metadata !1, metadata !140, i32 1798, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!620 = metadata !{i32 1803, i32 0, metadata !619, null}
!621 = metadata !{i32 786688, metadata !622, metadata !"c", metadata !22, i32 1812, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [c] [line 1812]
!622 = metadata !{i32 786443, metadata !1, metadata !140, i32 1803, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!623 = metadata !{i32 1812, i32 0, metadata !622, null}
!624 = metadata !{i32 786688, metadata !622, metadata !"p", metadata !22, i32 1813, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1813]
!625 = metadata !{i32 1813, i32 0, metadata !622, null}
!626 = metadata !{i32 1814, i32 0, metadata !627, null}
!627 = metadata !{i32 786443, metadata !1, metadata !622, i32 1814, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!628 = metadata !{i32 1815, i32 0, metadata !629, null}
!629 = metadata !{i32 786443, metadata !1, metadata !627, i32 1814, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!630 = metadata !{i32 1816, i32 0, metadata !629, null}
!631 = metadata !{i32 1817, i32 17, metadata !629, null}
!632 = metadata !{i32 42, i32 0, metadata !191, metadata !631}
!633 = metadata !{i32 44, i32 0, metadata !382, metadata !631}
!634 = metadata !{i32 46, i32 0, metadata !382, metadata !631}
!635 = metadata !{i32 58, i32 0, metadata !382, metadata !631} ; [ DW_TAG_imported_module ]
!636 = metadata !{i32 1819, i32 0, metadata !637, null}
!637 = metadata !{i32 786443, metadata !1, metadata !629, i32 1818, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!638 = metadata !{i32 1821, i32 0, metadata !629, null}
!639 = metadata !{i32 1823, i32 0, metadata !640, null}
!640 = metadata !{i32 786443, metadata !1, metadata !629, i32 1821, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!641 = metadata !{i32 1825, i32 0, metadata !640, null}
!642 = metadata !{i32 1826, i32 0, metadata !629, null}
!643 = metadata !{i32 786688, metadata !140, metadata !"wr", metadata !22, i32 1831, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1831]
!644 = metadata !{i32 1831, i32 0, metadata !140, null}
!645 = metadata !{i32 786688, metadata !140, metadata !"e", metadata !22, i32 1832, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1832]
!646 = metadata !{i32 1832, i32 0, metadata !140, null}
!647 = metadata !{i32 1832, i32 17, metadata !140, null}
!648 = metadata !{i32 786689, metadata !188, metadata !"k", metadata !22, i32 16778024, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 808]
!649 = metadata !{i32 808, i32 0, metadata !188, metadata !647}
!650 = metadata !{i32 786689, metadata !188, metadata !"Addr", metadata !22, i32 33555241, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 809]
!651 = metadata !{i32 809, i32 0, metadata !188, metadata !647}
!652 = metadata !{i32 786689, metadata !188, metadata !"Valu", metadata !22, i32 50332458, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 810]
!653 = metadata !{i32 810, i32 0, metadata !188, metadata !647}
!654 = metadata !{i32 786689, metadata !188, metadata !"Lock", metadata !22, i32 67109675, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Lock] [line 811]
!655 = metadata !{i32 811, i32 0, metadata !188, metadata !647}
!656 = metadata !{i32 786689, metadata !188, metadata !"cv", metadata !22, i32 83886892, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [cv] [line 812]
!657 = metadata !{i32 812, i32 0, metadata !188, metadata !647}
!658 = metadata !{i32 786688, metadata !188, metadata !"e", metadata !22, i32 814, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 814]
!659 = metadata !{i32 814, i32 0, metadata !188, metadata !647}
!660 = metadata !{i32 815, i32 0, metadata !188, metadata !647}
!661 = metadata !{i32 816, i32 0, metadata !662, metadata !647}
!662 = metadata !{i32 786443, metadata !1, metadata !188, i32 815, i32 0, i32 45} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!663 = metadata !{i32 817, i32 13, metadata !662, metadata !647}
!664 = metadata !{i32 786689, metadata !181, metadata !"tail", metadata !22, i32 16777857, metadata !42, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [tail] [line 641]
!665 = metadata !{i32 641, i32 0, metadata !181, metadata !663}
!666 = metadata !{i32 786688, metadata !181, metadata !"e", metadata !22, i32 643, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 643]
!667 = metadata !{i32 643, i32 0, metadata !181, metadata !663}
!668 = metadata !{i32 644, i32 0, metadata !181, metadata !663}
!669 = metadata !{i32 645, i32 0, metadata !181, metadata !663}
!670 = metadata !{i32 646, i32 0, metadata !181, metadata !663}
!671 = metadata !{i32 647, i32 0, metadata !181, metadata !663}
!672 = metadata !{i32 648, i32 0, metadata !181, metadata !663}
!673 = metadata !{i32 649, i32 0, metadata !181, metadata !663}
!674 = metadata !{i32 650, i32 0, metadata !181, metadata !663}
!675 = metadata !{i32 652, i32 0, metadata !181, metadata !663}
!676 = metadata !{i32 818, i32 0, metadata !662, metadata !647}
!677 = metadata !{i32 819, i32 0, metadata !662, metadata !647}
!678 = metadata !{i32 821, i32 0, metadata !188, metadata !647}
!679 = metadata !{i32 822, i32 0, metadata !188, metadata !647}
!680 = metadata !{i32 823, i32 0, metadata !188, metadata !647}
!681 = metadata !{i32 824, i32 0, metadata !188, metadata !647}
!682 = metadata !{i32 825, i32 0, metadata !188, metadata !647}
!683 = metadata !{i32 826, i32 0, metadata !188, metadata !647}
!684 = metadata !{i32 828, i32 0, metadata !188, metadata !647}
!685 = metadata !{i32 1833, i32 0, metadata !140, null}
!686 = metadata !{i32 1834, i32 0, metadata !687, null}
!687 = metadata !{i32 786443, metadata !1, metadata !140, i32 1833, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!688 = metadata !{i32 1835, i32 0, metadata !687, null}
!689 = metadata !{i32 1837, i32 0, metadata !140, null}
!690 = metadata !{i32 1839, i32 0, metadata !140, null}
!691 = metadata !{i32 1842, i32 0, metadata !140, null}
!692 = metadata !{i32 786689, metadata !143, metadata !"Self", metadata !22, i32 16779191, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1975]
!693 = metadata !{i32 1975, i32 0, metadata !143, null}
!694 = metadata !{i32 786689, metadata !143, metadata !"Addr", metadata !22, i32 33556407, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 1975]
!695 = metadata !{i32 786688, metadata !143, metadata !"Valu", metadata !22, i32 1979, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Valu] [line 1979]
!696 = metadata !{i32 1979, i32 0, metadata !143, null}
!697 = metadata !{i32 786688, metadata !143, metadata !"LockFor", metadata !22, i32 1988, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 1988]
!698 = metadata !{i32 1988, i32 0, metadata !143, null}
!699 = metadata !{i32 786688, metadata !143, metadata !"cv", metadata !22, i32 1989, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [cv] [line 1989]
!700 = metadata !{i32 1989, i32 0, metadata !143, null}
!701 = metadata !{i32 786688, metadata !143, metadata !"rdv", metadata !22, i32 1990, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rdv] [line 1990]
!702 = metadata !{i32 1990, i32 0, metadata !143, null}
!703 = metadata !{i32 1992, i32 0, metadata !143, null}
!704 = metadata !{i32 1994, i32 0, metadata !143, null}
!705 = metadata !{i32 1997, i32 0, metadata !706, null}
!706 = metadata !{i32 786443, metadata !1, metadata !143, i32 1996, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!707 = metadata !{i32 1998, i32 18, metadata !708, null}
!708 = metadata !{i32 786443, metadata !1, metadata !706, i32 1997, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!709 = metadata !{i32 786689, metadata !184, metadata !"Self", metadata !22, i32 16778103, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 887]
!710 = metadata !{i32 887, i32 0, metadata !184, metadata !707}
!711 = metadata !{i32 786689, metadata !184, metadata !"LockFor", metadata !22, i32 33555319, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [LockFor] [line 887]
!712 = metadata !{i32 786688, metadata !184, metadata !"k", metadata !22, i32 889, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [k] [line 889]
!713 = metadata !{i32 889, i32 0, metadata !184, metadata !707}
!714 = metadata !{i32 786688, metadata !184, metadata !"e", metadata !22, i32 910, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 910]
!715 = metadata !{i32 910, i32 0, metadata !184, metadata !707}
!716 = metadata !{i32 911, i32 0, metadata !184, metadata !707}
!717 = metadata !{i32 912, i32 14, metadata !718, metadata !707}
!718 = metadata !{i32 786443, metadata !1, metadata !184, i32 911, i32 0, i32 35} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!719 = metadata !{i32 786689, metadata !187, metadata !"Self", metadata !22, i32 16778512, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1296]
!720 = metadata !{i32 1296, i32 0, metadata !187, metadata !717}
!721 = metadata !{i32 786688, metadata !187, metadata !"rv", metadata !22, i32 1298, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rv] [line 1298]
!722 = metadata !{i32 1298, i32 0, metadata !187, metadata !717}
!723 = metadata !{i32 786688, metadata !187, metadata !"rd", metadata !22, i32 1299, metadata !724, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1299]
!724 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !37} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!725 = metadata !{i32 1299, i32 0, metadata !187, metadata !717}
!726 = metadata !{i32 786688, metadata !187, metadata !"EndOfList", metadata !22, i32 1300, metadata !540, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [EndOfList] [line 1300]
!727 = metadata !{i32 1300, i32 0, metadata !187, metadata !717}
!728 = metadata !{i32 786688, metadata !187, metadata !"e", metadata !22, i32 1301, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1301]
!729 = metadata !{i32 1301, i32 0, metadata !187, metadata !717}
!730 = metadata !{i32 1305, i32 0, metadata !731, metadata !717}
!731 = metadata !{i32 786443, metadata !1, metadata !187, i32 1305, i32 0, i32 37} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!732 = metadata !{i32 786688, metadata !733, metadata !"v", metadata !22, i32 1307, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 1307]
!733 = metadata !{i32 786443, metadata !1, metadata !731, i32 1305, i32 0, i32 38} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!734 = metadata !{i32 1307, i32 0, metadata !733, metadata !717}
!735 = metadata !{i32 1308, i32 0, metadata !733, metadata !717}
!736 = metadata !{i32 786688, metadata !737, metadata !"p", metadata !22, i32 1310, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1310]
!737 = metadata !{i32 786443, metadata !1, metadata !733, i32 1308, i32 0, i32 39} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!738 = metadata !{i32 1310, i32 0, metadata !737, metadata !717}
!739 = metadata !{i32 1311, i32 0, metadata !737, metadata !717}
!740 = metadata !{i32 1312, i32 0, metadata !741, metadata !717}
!741 = metadata !{i32 786443, metadata !1, metadata !737, i32 1311, i32 0, i32 40} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!742 = metadata !{i32 1313, i32 0, metadata !743, metadata !717}
!743 = metadata !{i32 786443, metadata !1, metadata !741, i32 1312, i32 0, i32 41} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!744 = metadata !{i32 1323, i32 0, metadata !737, metadata !717}
!745 = metadata !{i32 1316, i32 0, metadata !746, metadata !717}
!746 = metadata !{i32 786443, metadata !1, metadata !737, i32 1315, i32 0, i32 42} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!747 = metadata !{i32 1324, i32 0, metadata !748, metadata !717}
!748 = metadata !{i32 786443, metadata !1, metadata !733, i32 1323, i32 0, i32 43} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!749 = metadata !{i32 1325, i32 0, metadata !750, metadata !717}
!750 = metadata !{i32 786443, metadata !1, metadata !748, i32 1324, i32 0, i32 44} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!751 = metadata !{i32 1330, i32 0, metadata !187, metadata !717}
!752 = metadata !{i32 913, i32 0, metadata !753, metadata !707}
!753 = metadata !{i32 786443, metadata !1, metadata !718, i32 912, i32 0, i32 36} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!754 = metadata !{i32 915, i32 0, metadata !718, metadata !707}
!755 = metadata !{i32 916, i32 13, metadata !718, metadata !707}
!756 = metadata !{i32 641, i32 0, metadata !181, metadata !755}
!757 = metadata !{i32 643, i32 0, metadata !181, metadata !755}
!758 = metadata !{i32 644, i32 0, metadata !181, metadata !755}
!759 = metadata !{i32 645, i32 0, metadata !181, metadata !755}
!760 = metadata !{i32 646, i32 0, metadata !181, metadata !755}
!761 = metadata !{i32 647, i32 0, metadata !181, metadata !755}
!762 = metadata !{i32 648, i32 0, metadata !181, metadata !755}
!763 = metadata !{i32 649, i32 0, metadata !181, metadata !755}
!764 = metadata !{i32 650, i32 0, metadata !181, metadata !755}
!765 = metadata !{i32 652, i32 0, metadata !181, metadata !755}
!766 = metadata !{i32 917, i32 0, metadata !718, metadata !707}
!767 = metadata !{i32 918, i32 0, metadata !718, metadata !707}
!768 = metadata !{i32 920, i32 0, metadata !184, metadata !707}
!769 = metadata !{i32 921, i32 0, metadata !184, metadata !707}
!770 = metadata !{i32 922, i32 0, metadata !184, metadata !707}
!771 = metadata !{i32 925, i32 0, metadata !184, metadata !707}
!772 = metadata !{i32 2000, i32 0, metadata !773, null}
!773 = metadata !{i32 786443, metadata !1, metadata !708, i32 1998, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!774 = metadata !{i32 2001, i32 0, metadata !773, null}
!775 = metadata !{i32 2002, i32 0, metadata !708, null}
!776 = metadata !{i32 2004, i32 0, metadata !706, null}
!777 = metadata !{i32 2016, i32 0, metadata !143, null}
!778 = metadata !{i32 2018, i32 0, metadata !143, null}
!779 = metadata !{i32 2021, i32 0, metadata !143, null}
!780 = metadata !{i32 2022, i32 0, metadata !143, null}
!781 = metadata !{i32 786689, metadata !146, metadata !"Self", metadata !22, i32 16779363, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2147]
!782 = metadata !{i32 2147, i32 0, metadata !146, null}
!783 = metadata !{i32 786689, metadata !146, metadata !"Addr", metadata !22, i32 33556579, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 2147]
!784 = metadata !{i32 786689, metadata !146, metadata !"Valu", metadata !22, i32 50333795, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 2147]
!785 = metadata !{i32 2151, i32 0, metadata !146, null}
!786 = metadata !{i32 786689, metadata !178, metadata !"k", metadata !22, i32 16778082, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 866]
!787 = metadata !{i32 866, i32 0, metadata !178, metadata !785}
!788 = metadata !{i32 786689, metadata !178, metadata !"Addr", metadata !22, i32 33555298, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 866]
!789 = metadata !{i32 786689, metadata !178, metadata !"Valu", metadata !22, i32 50332514, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 866]
!790 = metadata !{i32 786688, metadata !178, metadata !"e", metadata !22, i32 868, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 868]
!791 = metadata !{i32 868, i32 0, metadata !178, metadata !785}
!792 = metadata !{i32 869, i32 0, metadata !178, metadata !785}
!793 = metadata !{i32 870, i32 0, metadata !794, metadata !785}
!794 = metadata !{i32 786443, metadata !1, metadata !178, i32 869, i32 0, i32 34} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!795 = metadata !{i32 871, i32 13, metadata !794, metadata !785}
!796 = metadata !{i32 641, i32 0, metadata !181, metadata !795}
!797 = metadata !{i32 643, i32 0, metadata !181, metadata !795}
!798 = metadata !{i32 644, i32 0, metadata !181, metadata !795}
!799 = metadata !{i32 645, i32 0, metadata !181, metadata !795}
!800 = metadata !{i32 646, i32 0, metadata !181, metadata !795}
!801 = metadata !{i32 647, i32 0, metadata !181, metadata !795}
!802 = metadata !{i32 648, i32 0, metadata !181, metadata !795}
!803 = metadata !{i32 649, i32 0, metadata !181, metadata !795}
!804 = metadata !{i32 650, i32 0, metadata !181, metadata !795}
!805 = metadata !{i32 652, i32 0, metadata !181, metadata !795}
!806 = metadata !{i32 872, i32 0, metadata !794, metadata !785}
!807 = metadata !{i32 873, i32 0, metadata !794, metadata !785}
!808 = metadata !{i32 874, i32 0, metadata !178, metadata !785}
!809 = metadata !{i32 875, i32 0, metadata !178, metadata !785}
!810 = metadata !{i32 876, i32 0, metadata !178, metadata !785}
!811 = metadata !{i32 877, i32 0, metadata !178, metadata !785}
!812 = metadata !{i32 878, i32 0, metadata !178, metadata !785}
!813 = metadata !{i32 2152, i32 0, metadata !146, null}
!814 = metadata !{i32 2155, i32 0, metadata !146, null}
!815 = metadata !{i32 786689, metadata !147, metadata !"Self", metadata !22, i32 16779379, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2163]
!816 = metadata !{i32 2163, i32 0, metadata !147, null}
!817 = metadata !{i32 786689, metadata !147, metadata !"envPtr", metadata !22, i32 33556595, metadata !96, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [envPtr] [line 2163]
!818 = metadata !{i32 786689, metadata !147, metadata !"ROFlag", metadata !22, i32 50333811, metadata !68, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ROFlag] [line 2163]
!819 = metadata !{i32 2168, i32 0, metadata !147, null}
!820 = metadata !{i32 786689, metadata !177, metadata !"Self", metadata !22, i32 16778382, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1166]
!821 = metadata !{i32 1166, i32 0, metadata !177, metadata !819}
!822 = metadata !{i32 1168, i32 0, metadata !177, metadata !819}
!823 = metadata !{i32 1183, i32 0, metadata !177, metadata !819}
!824 = metadata !{i32 1184, i32 0, metadata !177, metadata !819}
!825 = metadata !{i32 1187, i32 0, metadata !177, metadata !819}
!826 = metadata !{i32 1188, i32 0, metadata !177, metadata !819}
!827 = metadata !{i32 1189, i32 0, metadata !177, metadata !819}
!828 = metadata !{i32 1191, i32 0, metadata !177, metadata !819}
!829 = metadata !{i32 1192, i32 0, metadata !177, metadata !819}
!830 = metadata !{i32 1193, i32 0, metadata !177, metadata !819}
!831 = metadata !{i32 1196, i32 0, metadata !177, metadata !819}
!832 = metadata !{i32 2170, i32 16, metadata !147, null}
!833 = metadata !{i32 786689, metadata !174, metadata !"Self", metadata !22, i32 16777529, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 313]
!834 = metadata !{i32 313, i32 0, metadata !174, metadata !832}
!835 = metadata !{i32 316, i32 0, metadata !174, metadata !832}
!836 = metadata !{i32 2174, i32 0, metadata !147, null}
!837 = metadata !{i32 2175, i32 0, metadata !147, null}
!838 = metadata !{i32 2176, i32 0, metadata !147, null}
!839 = metadata !{i32 2177, i32 0, metadata !147, null}
!840 = metadata !{i32 2182, i32 0, metadata !147, null}
!841 = metadata !{i32 2187, i32 0, metadata !147, null}
!842 = metadata !{i32 786689, metadata !150, metadata !"Self", metadata !22, i32 16779411, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2195]
!843 = metadata !{i32 2195, i32 0, metadata !150, null}
!844 = metadata !{i32 2205, i32 0, metadata !150, null}
!845 = metadata !{i32 2209, i32 0, metadata !846, null}
!846 = metadata !{i32 786443, metadata !1, metadata !150, i32 2207, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!847 = metadata !{i32 786689, metadata !173, metadata !"Self", metadata !22, i32 16778422, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1206]
!848 = metadata !{i32 1206, i32 0, metadata !173, metadata !845}
!849 = metadata !{i32 1208, i32 0, metadata !173, metadata !845}
!850 = metadata !{i32 1166, i32 0, metadata !177, metadata !849}
!851 = metadata !{i32 1168, i32 0, metadata !177, metadata !849}
!852 = metadata !{i32 1183, i32 0, metadata !177, metadata !849}
!853 = metadata !{i32 1184, i32 0, metadata !177, metadata !849}
!854 = metadata !{i32 1187, i32 0, metadata !177, metadata !849}
!855 = metadata !{i32 1188, i32 0, metadata !177, metadata !849}
!856 = metadata !{i32 1189, i32 0, metadata !177, metadata !849}
!857 = metadata !{i32 1191, i32 0, metadata !177, metadata !849}
!858 = metadata !{i32 1192, i32 0, metadata !177, metadata !849}
!859 = metadata !{i32 1193, i32 0, metadata !177, metadata !849}
!860 = metadata !{i32 1196, i32 0, metadata !177, metadata !849}
!861 = metadata !{i32 1209, i32 0, metadata !173, metadata !845}
!862 = metadata !{i32 2210, i32 0, metadata !846, null}
!863 = metadata !{i32 2211, i32 0, metadata !846, null}
!864 = metadata !{i32 2223, i32 0, metadata !846, null}
!865 = metadata !{i32 2226, i32 9, metadata !150, null}
!866 = metadata !{i32 786689, metadata !160, metadata !"Self", metadata !22, i32 16778673, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1457]
!867 = metadata !{i32 1457, i32 0, metadata !160, metadata !865}
!868 = metadata !{i32 786688, metadata !160, metadata !"wv", metadata !22, i32 1475, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wv] [line 1475]
!869 = metadata !{i32 1475, i32 0, metadata !160, metadata !865}
!870 = metadata !{i32 1623, i32 10, metadata !160, metadata !865}
!871 = metadata !{i32 786689, metadata !167, metadata !"Self", metadata !22, i32 16777597, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 381]
!872 = metadata !{i32 381, i32 0, metadata !167, metadata !870}
!873 = metadata !{i32 786689, metadata !167, metadata !"maxv", metadata !22, i32 33554813, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [maxv] [line 381]
!874 = metadata !{i32 786688, metadata !167, metadata !"gv", metadata !22, i32 383, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [gv] [line 383]
!875 = metadata !{i32 383, i32 0, metadata !167, metadata !870}
!876 = metadata !{i32 786688, metadata !167, metadata !"wv", metadata !22, i32 384, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wv] [line 384]
!877 = metadata !{i32 384, i32 0, metadata !167, metadata !870}
!878 = metadata !{i32 786688, metadata !167, metadata !"k", metadata !22, i32 385, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [k] [line 385]
!879 = metadata !{i32 385, i32 0, metadata !167, metadata !870}
!880 = metadata !{i32 385, i32 16, metadata !167, metadata !870}
!881 = metadata !{i32 42, i32 0, metadata !191, metadata !880}
!882 = metadata !{i32 44, i32 0, metadata !382, metadata !880}
!883 = metadata !{i32 46, i32 0, metadata !382, metadata !880}
!884 = metadata !{i32 58, i32 0, metadata !382, metadata !880} ; [ DW_TAG_imported_module ]
!885 = metadata !{i32 386, i32 0, metadata !167, metadata !870}
!886 = metadata !{i32 387, i32 0, metadata !887, metadata !870}
!887 = metadata !{i32 786443, metadata !1, metadata !167, i32 386, i32 0, i32 32} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!888 = metadata !{i32 388, i32 0, metadata !887, metadata !870}
!889 = metadata !{i32 394, i32 0, metadata !167, metadata !870}
!890 = metadata !{i32 395, i32 0, metadata !167, metadata !870}
!891 = metadata !{i32 1646, i32 10, metadata !160, metadata !865}
!892 = metadata !{i32 786689, metadata !166, metadata !"Self", metadata !22, i32 16778470, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1254]
!893 = metadata !{i32 1254, i32 0, metadata !166, metadata !891}
!894 = metadata !{i32 786688, metadata !166, metadata !"dx", metadata !22, i32 1256, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dx] [line 1256]
!895 = metadata !{i32 1256, i32 0, metadata !166, metadata !891}
!896 = metadata !{i32 786688, metadata !166, metadata !"rv", metadata !22, i32 1257, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rv] [line 1257]
!897 = metadata !{i32 1257, i32 0, metadata !166, metadata !891}
!898 = metadata !{i32 786688, metadata !166, metadata !"rd", metadata !22, i32 1258, metadata !724, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1258]
!899 = metadata !{i32 1258, i32 0, metadata !166, metadata !891}
!900 = metadata !{i32 786688, metadata !166, metadata !"EndOfList", metadata !22, i32 1259, metadata !540, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [EndOfList] [line 1259]
!901 = metadata !{i32 1259, i32 0, metadata !166, metadata !891}
!902 = metadata !{i32 786688, metadata !166, metadata !"e", metadata !22, i32 1260, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1260]
!903 = metadata !{i32 1260, i32 0, metadata !166, metadata !891}
!904 = metadata !{i32 1264, i32 0, metadata !905, metadata !891}
!905 = metadata !{i32 786443, metadata !1, metadata !166, i32 1264, i32 0, i32 25} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!906 = metadata !{i32 786688, metadata !907, metadata !"v", metadata !22, i32 1266, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 1266]
!907 = metadata !{i32 786443, metadata !1, metadata !905, i32 1264, i32 0, i32 26} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!908 = metadata !{i32 1266, i32 0, metadata !907, metadata !891}
!909 = metadata !{i32 1267, i32 0, metadata !907, metadata !891}
!910 = metadata !{i32 786688, metadata !911, metadata !"p", metadata !22, i32 1269, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1269]
!911 = metadata !{i32 786443, metadata !1, metadata !907, i32 1267, i32 0, i32 27} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!912 = metadata !{i32 1269, i32 0, metadata !911, metadata !891}
!913 = metadata !{i32 1270, i32 0, metadata !911, metadata !891}
!914 = metadata !{i32 1271, i32 0, metadata !915, metadata !891}
!915 = metadata !{i32 786443, metadata !1, metadata !911, i32 1270, i32 0, i32 28} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!916 = metadata !{i32 1272, i32 0, metadata !917, metadata !891}
!917 = metadata !{i32 786443, metadata !1, metadata !915, i32 1271, i32 0, i32 29} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!918 = metadata !{i32 1280, i32 0, metadata !911, metadata !891}
!919 = metadata !{i32 1275, i32 0, metadata !920, metadata !891}
!920 = metadata !{i32 786443, metadata !1, metadata !911, i32 1274, i32 0, i32 30} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!921 = metadata !{i32 1281, i32 0, metadata !922, metadata !891}
!922 = metadata !{i32 786443, metadata !1, metadata !907, i32 1280, i32 0, i32 31} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!923 = metadata !{i32 1285, i32 0, metadata !166, metadata !891}
!924 = metadata !{i32 1286, i32 0, metadata !166, metadata !891}
!925 = metadata !{i32 1653, i32 0, metadata !926, metadata !865}
!926 = metadata !{i32 786443, metadata !1, metadata !160, i32 1646, i32 0, i32 21} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!927 = metadata !{i32 1669, i32 0, metadata !160, metadata !865}
!928 = metadata !{i32 786689, metadata !163, metadata !"Self", metadata !22, i32 16778597, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1381]
!929 = metadata !{i32 1381, i32 0, metadata !163, metadata !927}
!930 = metadata !{i32 786689, metadata !163, metadata !"wv", metadata !22, i32 33555813, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [wv] [line 1381]
!931 = metadata !{i32 786688, metadata !163, metadata !"wr", metadata !22, i32 1391, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1391]
!932 = metadata !{i32 1391, i32 0, metadata !163, metadata !927}
!933 = metadata !{i32 786688, metadata !934, metadata !"p", metadata !22, i32 1394, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1394]
!934 = metadata !{i32 786443, metadata !1, metadata !163, i32 1393, i32 0, i32 22} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!935 = metadata !{i32 1394, i32 0, metadata !934, metadata !927}
!936 = metadata !{i32 786688, metadata !934, metadata !"End", metadata !22, i32 1395, metadata !540, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1395]
!937 = metadata !{i32 1395, i32 0, metadata !934, metadata !927}
!938 = metadata !{i32 1396, i32 0, metadata !939, metadata !927}
!939 = metadata !{i32 786443, metadata !1, metadata !934, i32 1396, i32 0, i32 23} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!940 = metadata !{i32 1413, i32 0, metadata !941, metadata !927}
!941 = metadata !{i32 786443, metadata !1, metadata !939, i32 1396, i32 0, i32 24} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!942 = metadata !{i32 1677, i32 0, metadata !160, metadata !865}
!943 = metadata !{i32 -2146795976}
!944 = metadata !{i32 1679, i32 0, metadata !160, metadata !865}
!945 = metadata !{i32 2227, i32 0, metadata !946, null}
!946 = metadata !{i32 786443, metadata !1, metadata !150, i32 2226, i32 0, i32 19} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!947 = metadata !{i32 1206, i32 0, metadata !173, metadata !945}
!948 = metadata !{i32 1208, i32 0, metadata !173, metadata !945}
!949 = metadata !{i32 1166, i32 0, metadata !177, metadata !948}
!950 = metadata !{i32 1168, i32 0, metadata !177, metadata !948}
!951 = metadata !{i32 1183, i32 0, metadata !177, metadata !948}
!952 = metadata !{i32 1184, i32 0, metadata !177, metadata !948}
!953 = metadata !{i32 1187, i32 0, metadata !177, metadata !948}
!954 = metadata !{i32 1188, i32 0, metadata !177, metadata !948}
!955 = metadata !{i32 1189, i32 0, metadata !177, metadata !948}
!956 = metadata !{i32 1191, i32 0, metadata !177, metadata !948}
!957 = metadata !{i32 1192, i32 0, metadata !177, metadata !948}
!958 = metadata !{i32 1193, i32 0, metadata !177, metadata !948}
!959 = metadata !{i32 1196, i32 0, metadata !177, metadata !948}
!960 = metadata !{i32 1209, i32 0, metadata !173, metadata !945}
!961 = metadata !{i32 2228, i32 0, metadata !946, null}
!962 = metadata !{i32 2229, i32 0, metadata !946, null}
!963 = metadata !{i32 2243, i32 0, metadata !946, null}
!964 = metadata !{i32 2247, i32 0, metadata !150, null}
!965 = metadata !{i32 2250, i32 0, metadata !150, null}
!966 = metadata !{i32 786689, metadata !170, metadata !"Base", metadata !22, i32 16779336, metadata !80, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Base] [line 2120]
!967 = metadata !{i32 2120, i32 0, metadata !170, null}
!968 = metadata !{i32 786689, metadata !170, metadata !"Length", metadata !22, i32 33556552, metadata !156, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Length] [line 2120]
!969 = metadata !{i32 786688, metadata !170, metadata !"Addr", metadata !22, i32 2124, metadata !970, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Addr] [line 2124]
!970 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !32} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from intptr_t]
!971 = metadata !{i32 2124, i32 0, metadata !170, null}
!972 = metadata !{i32 786688, metadata !170, metadata !"End", metadata !22, i32 2125, metadata !970, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 2125]
!973 = metadata !{i32 2125, i32 0, metadata !170, null}
!974 = metadata !{i32 2127, i32 0, metadata !170, null}
!975 = metadata !{i32 786688, metadata !976, metadata !"Lock", metadata !22, i32 2128, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Lock] [line 2128]
!976 = metadata !{i32 786443, metadata !1, metadata !170, i32 2127, i32 0, i32 33} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!977 = metadata !{i32 2128, i32 0, metadata !976, null}
!978 = metadata !{i32 786688, metadata !976, metadata !"val", metadata !22, i32 2129, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [val] [line 2129]
!979 = metadata !{i32 2129, i32 0, metadata !976, null}
!980 = metadata !{i32 2131, i32 0, metadata !976, null}
!981 = metadata !{i32 42, i32 0, metadata !191, metadata !980}
!982 = metadata !{i32 44, i32 0, metadata !382, metadata !980}
!983 = metadata !{i32 46, i32 0, metadata !382, metadata !980}
!984 = metadata !{i32 58, i32 0, metadata !382, metadata !980} ; [ DW_TAG_imported_module ]
!985 = metadata !{i32 2132, i32 0, metadata !976, null}
!986 = metadata !{i32 2133, i32 0, metadata !976, null}
!987 = metadata !{i32 2134, i32 0, metadata !170, null}
!988 = metadata !{i32 2137, i32 0, metadata !170, null}
!989 = metadata !{i32 786689, metadata !153, metadata !"Self", metadata !22, i32 16779481, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2265]
!990 = metadata !{i32 2265, i32 0, metadata !153, null}
!991 = metadata !{i32 786689, metadata !153, metadata !"size", metadata !22, i32 33556697, metadata !156, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [size] [line 2265]
!992 = metadata !{i32 786688, metadata !153, metadata !"ptr", metadata !22, i32 2267, metadata !80, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ptr] [line 2267]
!993 = metadata !{i32 2267, i32 0, metadata !153, null}
!994 = metadata !{i32 2268, i32 0, metadata !153, null}
!995 = metadata !{i32 2269, i32 0, metadata !996, null}
!996 = metadata !{i32 786443, metadata !1, metadata !153, i32 2268, i32 0, i32 20} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!997 = metadata !{i32 2270, i32 0, metadata !996, null}
!998 = metadata !{i32 2272, i32 0, metadata !153, null}
!999 = metadata !{i32 786689, metadata !157, metadata !"Self", metadata !22, i32 16779499, metadata !131, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2283]
!1000 = metadata !{i32 2283, i32 0, metadata !157, null}
!1001 = metadata !{i32 786689, metadata !157, metadata !"ptr", metadata !22, i32 33556715, metadata !80, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ptr] [line 2283]
!1002 = metadata !{i32 2285, i32 0, metadata !157, null}
!1003 = metadata !{i32 2295, i32 0, metadata !157, null}
!1004 = metadata !{i32 2308, i32 0, metadata !157, null}
!1005 = metadata !{i32 995, i32 0, metadata !218, null}
!1006 = metadata !{i32 996, i32 0, metadata !1007, null}
!1007 = metadata !{i32 786443, metadata !1, metadata !218, i32 995, i32 0, i32 57} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1008 = metadata !{i32 997, i32 0, metadata !1007, null}
!1009 = metadata !{i32 1000, i32 0, metadata !218, null}
!1010 = metadata !{i32 1001, i32 0, metadata !1011, null}
!1011 = metadata !{i32 786443, metadata !1, metadata !218, i32 1000, i32 0, i32 58} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1012 = metadata !{i32 1002, i32 0, metadata !1011, null}
!1013 = metadata !{i32 1004, i32 0, metadata !218, null}
!1014 = metadata !{i32 786688, metadata !219, metadata !"act", metadata !22, i32 969, metadata !309, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [act] [line 969]
!1015 = metadata !{i32 969, i32 0, metadata !219, null}
!1016 = metadata !{i32 971, i32 0, metadata !219, null}
!1017 = metadata !{i32 972, i32 0, metadata !219, null}
!1018 = metadata !{i32 973, i32 0, metadata !219, null}
!1019 = metadata !{i32 974, i32 0, metadata !219, null}
!1020 = metadata !{i32 976, i32 0, metadata !219, null}
!1021 = metadata !{i32 977, i32 0, metadata !1022, null}
!1022 = metadata !{i32 786443, metadata !1, metadata !219, i32 976, i32 0, i32 59} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1023 = metadata !{i32 978, i32 0, metadata !1022, null}
!1024 = metadata !{i32 981, i32 0, metadata !219, null}
!1025 = metadata !{i32 982, i32 0, metadata !1026, null}
!1026 = metadata !{i32 786443, metadata !1, metadata !219, i32 981, i32 0, i32 60} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1027 = metadata !{i32 983, i32 0, metadata !1026, null}
!1028 = metadata !{i32 985, i32 0, metadata !219, null}
!1029 = metadata !{i32 786689, metadata !220, metadata !"signum", metadata !22, i32 16778158, metadata !69, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [signum] [line 942]
!1030 = metadata !{i32 942, i32 0, metadata !220, null}
!1031 = metadata !{i32 786689, metadata !220, metadata !"siginfo", metadata !22, i32 33555374, metadata !223, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [siginfo] [line 942]
!1032 = metadata !{i32 786689, metadata !220, metadata !"context", metadata !22, i32 50332590, metadata !80, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [context] [line 942]
!1033 = metadata !{i32 786688, metadata !220, metadata !"Self", metadata !22, i32 944, metadata !131, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Self] [line 944]
!1034 = metadata !{i32 944, i32 0, metadata !220, null}
!1035 = metadata !{i32 946, i32 0, metadata !220, null}
!1036 = metadata !{i32 947, i32 0, metadata !1037, null}
!1037 = metadata !{i32 786443, metadata !1, metadata !220, i32 946, i32 0, i32 61} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1038 = metadata !{i32 948, i32 0, metadata !1037, null}
!1039 = metadata !{i32 951, i32 0, metadata !220, null}
!1040 = metadata !{i32 952, i32 14, metadata !1041, null}
!1041 = metadata !{i32 786443, metadata !1, metadata !220, i32 951, i32 0, i32 62} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1042 = metadata !{i32 1296, i32 0, metadata !187, metadata !1040}
!1043 = metadata !{i32 1298, i32 0, metadata !187, metadata !1040}
!1044 = metadata !{i32 1299, i32 0, metadata !187, metadata !1040}
!1045 = metadata !{i32 1300, i32 0, metadata !187, metadata !1040}
!1046 = metadata !{i32 1301, i32 0, metadata !187, metadata !1040}
!1047 = metadata !{i32 1305, i32 0, metadata !731, metadata !1040}
!1048 = metadata !{i32 1307, i32 0, metadata !733, metadata !1040}
!1049 = metadata !{i32 1308, i32 0, metadata !733, metadata !1040}
!1050 = metadata !{i32 1310, i32 0, metadata !737, metadata !1040}
!1051 = metadata !{i32 1311, i32 0, metadata !737, metadata !1040}
!1052 = metadata !{i32 1312, i32 0, metadata !741, metadata !1040}
!1053 = metadata !{i32 1313, i32 0, metadata !743, metadata !1040}
!1054 = metadata !{i32 1323, i32 0, metadata !737, metadata !1040}
!1055 = metadata !{i32 1316, i32 0, metadata !746, metadata !1040}
!1056 = metadata !{i32 1324, i32 0, metadata !748, metadata !1040}
!1057 = metadata !{i32 1325, i32 0, metadata !750, metadata !1040}
!1058 = metadata !{i32 1330, i32 0, metadata !187, metadata !1040}
!1059 = metadata !{i32 953, i32 0, metadata !1060, null}
!1060 = metadata !{i32 786443, metadata !1, metadata !1041, i32 952, i32 0, i32 63} ; [ DW_TAG_lexical_block ] [/vagrant/stamp-tl2-x86/tl2.c]
!1061 = metadata !{i32 954, i32 0, metadata !1060, null}
!1062 = metadata !{i32 955, i32 0, metadata !1041, null}
!1063 = metadata !{i32 957, i32 0, metadata !220, null}
!1064 = metadata !{i32 958, i32 0, metadata !220, null}
!1065 = metadata !{i32 959, i32 0, metadata !220, null}
