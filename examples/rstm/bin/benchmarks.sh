set -e
stm_postfix="$1"
arch="$2"
if [ -z "$1" ] || [ -z "$2" ] ; then
  echo "Usage: $0 <STM64|STM32> <arm|x86>"
  exit 1;
fi

echo "==> building the unoptimized version"
mkdir -p build
cp src/libstm/algs/byteeager-original.cpp src/libstm/algs/byteeager.cpp
cd build
# tell cmake that we want to use the original configuration of the algos
export STM_HAND_ROLLED=true
cmake -DCMAKE_CXX_COMPILER_ID=/usr/bin/clang ../src
make
unset STM_HAND_ROLLED
cd -

echo "==> copying benchmark script"
cp -R src/stamp-0.9.10/bin build/stamp-0.9.10/
cp -R src/stamp-0.9.10/kmeans/inputs build/stamp-0.9.10/kmeans/
cp -R src/stamp-0.9.10/labyrinth/inputs build/stamp-0.9.10/labyrinth/

cd build/stamp*
STM_POSTFIX="$stm_postfix" bash bin/benchmarks.sh rstm unopt $arch
cd -

mkdir -p build
cp compiled/byteeager-$arch.cpp src/libstm/algs/byteeager.cpp
cd build
# tell cmake that we want to use the original configuration of the algos
unset STM_HAND_ROLLED
rm CMakeCache.txt
rm -r CMakeFiles
cmake -DCMAKE_CXX_COMPILER_ID=/usr/bin/clang ../src
make
cd -

cd build/stamp*
STM_POSTFIX="$stm_postfix" bash bin/benchmarks.sh rstm opt $arch
cd -
