algo=$1
opt=$2
arch=$3

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
  echo "Usage benchmark.sh <tl2|rstm> <opt|unopt> <x86|arm>"
  exit 0;
fi

function log {
  echo "==> $@..."
}

function build {
  log "building for $1"
  cd $1
  # make -f Makefile.stm clean; make -f Makefile.stm
  cd -
}

# make sure all the binaries use ByteEager
export STM_CONFIG=ByteEager

build kmeans
log "running kmeans"
dir="notes/benchmarks/$algo/$arch/$opt/kmeans/"
mkdir -p "$dir"
./bin/avg.sh ./kmeans/kmeans$STM_POSTFIX -m40 -n40 -t0.00001 -i kmeans/inputs/random-n16384-d24-c16.txt > "$dir/100-m40-n40-t0.00001-irandom-n16384-d24-c16.txt"

build labyrinth
log "running labyrinth"
dir="notes/benchmarks/$algo/$arch/$opt/labyrinth/"
mkdir -p "$dir"
./bin/avg.sh ./labyrinth/labyrinth$STM_POSTFIX -i labyrinth/inputs/random-x256-y256-z3-n256.txt > "$dir/random-x256-y256-z3-n256.txt"

build intruder
log "running intruder"
dir="notes/benchmarks/$algo/$arch/$opt/intruder/"
mkdir -p "$dir"
./bin/avg.sh ./intruder/intruder$STM_POSTFIX -a10 -l128 -n26214 -s1 -t2 > "$dir/a10-l128-n26214-s1.txt"

build vacation
log "running vacation"
dir="notes/benchmarks/$algo/$arch/$opt/vacation/"
mkdir -p "$dir"
./bin/avg.sh ./vacation/vacation$STM_POSTFIX -n2 -q90 -u98 -r104857 -t419434 > "$dir/low-n2-q90-u98-r104857-t419434.txt"
./bin/avg.sh ./vacation/vacation$STM_POSTFIX -n4 -q60 -u90 -r104857 -t419434 > "$dir/high-n4-q60-u90-r104857-t419434.txt"

build ssca2
log "running ssca2"
dir="notes/benchmarks/$algo/$arch/$opt/ssca2/"
mkdir -p "$dir"
# assumes 1.* results in sed
./bin/avg.sh "./ssca2/ssca2$STM_POSTFIX -s17 -i1.0 -u1.0 -l4 -p3 -t2 | grep 'kernel' | sed 's/1//'" > "$dir/100-s17-i1.0-u1.0-l4-p3.txt"
