cmd_with_args="$@"
output=/tmp/build.out

if [ -z "$runs" ]; then
  runs=100
fi

# empty the temp file
echo -n > "$output"

handle_sigint(){
  exit 1;
}

trap handle_sigint SIGINT;

# run the fence test
for i in $(seq 1 $runs); do
  eval "$cmd_with_args | grep -i \"Time\" | sed 's/[^0-9]*\([0-9\.]*\).*/\1/' >> $output"
  # $cmd_with_args | grep -i "Time" | sed 's/[^0-9]*\([0-9\.]*\).*/\1/' >> "$output"
  last=$(tail -1 "$output")
  echo "$last"
done

# get the average of the runs
# cat "$output" | awk '{s+=$1}END{print "avg:",s/NR}'
