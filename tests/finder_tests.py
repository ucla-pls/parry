from parry.finder import Finder

class TestFinder:
    def setup(self):
        self.finder = Finder("tests/parsed/test.c")

    def test_find_labels(self):
        labels = self.finder.find_labels( "ord" )

        assert( {'line' : 14, 'name' : "ord_a_1"} in labels )
        assert( {'line' : 19, 'name' : "ord_a_2"} in labels )

        assert( {'line' : 16, 'name' : "ord_b_1"} in labels )
        assert( {'line' : 21, 'name' : "ord_b_2"} in labels )
