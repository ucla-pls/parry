from nose.tools import *
from graph_tool import Graph
import parry.algorithms.bounded_radius_cut as brc
from graph_tool.topology import shortest_path

class TestBoundedRadiusCut():
    def setup(self):
        self.graph = graph = self._create_graph()
        self.orders = self._orders()

    def test_fixed_graph(self):
        graph, vertices, edges = self._prep_graph()

        vertex_ids = [str(v) for v in vertices]
        expected_ids = [1, 6, 7, 8, 9, 10 ,2]

        for i, vid in enumerate(vertex_ids):
            eq_(vid, str(expected_ids[i]))

        eq_(len(edges), 6)

        for e in edges:
            d = graph.edge_properties['distance'][e]
            eq_(d, 1.0/6)

    def test_cut_at_distance(self):
        graph, vertices, edges = self._prep_graph()

        # use the new fixed graph
        cut = brc.cut_at_distance(graph, graph.vertex(0),  1.0/6)

        eq_(len(cut), 1)

        # the cut should be in the second edge along the path from v1 to v2
        # since the first edge is 1/6 and the path can be as long as the distance
        eq_(cut[0], edges[1])

    def test_candidate_cuts(self):
        graph, vertices, edges = self._prep_graph()

        v1 = graph.vertex(1)
        v2 = graph.vertex(2)

        cuts = brc.candidate_cuts(graph, graph.vertex(0), graph.vertex(3))

        # distances from one third to two thirds incremented by 1/6
        eq_(2.0/6, cuts[0][0])
        eq_(3.0/6, cuts[1][0])
        eq_(4.0/6, cuts[2][0])

        # s1  s2
        # |  /    <--- distance 0
        # v1
        # |  <--- distance 1
        # v2
        # | \     <--- distance 0
        # t1  t2

        # changes during graph prep to the following

        # s1  s2
        # |  /    <--- distance 0
        # v1
        # |  <--- distance 1/6
        # v6
        # |  <--- distance 1/6
        # v7                     <-- distance 2/6
        # |  <--- distance 1/6   <-- past 2/6, viable cut for 2/6
        # v8                     <-- distance 3/6
        # |  <--- distance 1/6   <-- past 3/6, viable cut for 3/6
        # v9                     <-- distance 4/6
        # |  <--- distance 1/6   <-- past 4/6, viable cut for 4/6
        # v10
        # |  <--- distance 1/6
        # v2
        # | \     <--- distance 0
        # t1  t2

        # the size of the cut is sure to be 1 in each case so
        # just grab the edge
        cut_edge_lists = [c for d, c in cuts]

        eq_(len(cut_edge_lists), 3)

        eq_(graph.edge(graph.vertex(7), graph.vertex(8)), cut_edge_lists[0][0])
        eq_(graph.edge(graph.vertex(8), graph.vertex(9)), cut_edge_lists[1][0])
        eq_(graph.edge(graph.vertex(9), graph.vertex(10)), cut_edge_lists[2][0])

    def test_cut_disconnected(self):
        graph, vertices, edges = self._prep_graph()

        disc_graph = graph.copy()
        va = disc_graph.add_vertex()
        vb = disc_graph.add_vertex()
        vc = disc_graph.add_vertex()

        disc_graph.vertex_properties['id'][va] = str(va)
        disc_graph.vertex_properties['id'][vb] = str(vb)
        disc_graph.vertex_properties['id'][vc] = str(vc)

        other = disc_graph.add_edge(va, vb)
        cut = disc_graph.add_edge(vb, vc)

        disc_graph.edge_properties['distance'][other] = 1.0
        disc_graph.edge_properties['distance'][cut] = 1.0

        orders = [(disc_graph.vertex(0), disc_graph.vertex(3)), (va, vc)]

        cuts, graph = brc.bounded_radius_cut(disc_graph, orders)

        # there should be two cuts one for each
        # disconnected bit of the graph
        eq_(len(cuts), 2)

    def test_cut_simple_diamond(self):
        graph = self._create_graph()

        second_cut = graph.add_edge(graph.vertex(4), graph.vertex(5))
        graph.edge_properties['distance'][second_cut] = 1

        # before fixing the edges with length > 1/6
        # s1  s2
        # |  / |<-- distance 1 (from assignment above)
        # v1   |
        # |<-- distance 1 (from _create_graph)
        # v2   |
        # | \  |
        # t1  t2

        # fix the graph so that the distance one edges are sequences of
        # edges with distance 1/6.
        # orders, (v1,t1) and (s2,v1)
        orders = [(graph.vertex(4), graph.vertex(5))]

        # after fixing the edges with length > 1/6
        # s1  s2
        # |  / |
        # v1   |
        # | <- | <--- distance 1/6
        # v6  v11
        # | <- | <--- distance 1/6
        # v7  v12
        # | <- | <--- distance 1/6, past 2/6, viable cut for 2/6
        # v8  v13
        # | <- | <--- distance 1/6, past 3/6, viable cut for 3/6
        # v9  v14
        # | <- | <--- distance 1/6, past 4/6, viable cut for 4/6f
        # v10 v15
        # |    |
        # v2   |
        # | \  |
        # t1  t2

        cuts, graph = brc.bounded_radius_cut(graph, orders)

        # two edges
        eq_(len(cuts), 2)

        # from above 12,13 is the first viable cut
        eq_(cuts[0], graph.edge(graph.vertex(12), graph.vertex(13)))

        # from above 7,8 too on the other path
        eq_(cuts[1], graph.edge(graph.vertex(7), graph.vertex(8)))

    def _prep_graph(self):
        graph = brc.fix_graph(self.graph)

        # NOTE important to ask the new graph for what should be the original vertices
        # otherwise the results of the shortest path will be borked
        (vertices, edges) = shortest_path(graph, graph.vertex(1), graph.vertex(2))

        return (graph, vertices, edges)

    def _create_graph(self):
        graph = Graph()

        s1 = graph.add_vertex()
        self.v1 = v1 = graph.add_vertex()
        self.v2 = v2 = graph.add_vertex()
        t1 = graph.add_vertex()

        s2 = graph.add_vertex()
        t2 = graph.add_vertex()

        # s1  s2
        # |  /
        # v1
        # |
        # v2
        # | \
        # t1  t2

        graph.add_edge(s1, v1)
        cut = graph.add_edge(v1, v2)
        graph.add_edge(v2, t1)
        graph.add_edge(s2, v1)
        graph.add_edge(v2, t2)

        graph.edge_properties['distance'] = graph.new_edge_property('float')

        for e in graph.edges():
            graph.edge_properties['distance'][e] = 0.0

        graph.edge_properties['distance'][cut] = 1

        graph.vertex_properties['id'] = graph.new_vertex_property('string')
        graph.vertex_properties['label'] = graph.new_vertex_property('string')

        for i,v in enumerate(graph.vertices()):
            graph.vertex_properties['id'][v] = str(i)
            graph.vertex_properties['label'][v] = str(i)

        return graph

    def _orders(self, orders=None):
        if not orders:
            orders = [(self.graph.vertex(0), self.graph.vertex(3))]

        return orders
