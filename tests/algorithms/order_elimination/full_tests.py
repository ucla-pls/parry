from nose.tools import *
from graph_tool import Graph
from parry.algorithms.order_elimination.full import dfs_paths, full_elimination
from parry.architectures.tso import TSO
import parry.util.graph as gutil
import parry.util.debug as dutil

class TestFullElimination():
    def setup(self):
        self.graph = graph = Graph()
        self.event = gutil.vprop(self.graph, 'event', 'string')
        self.variable = gutil.vprop(self.graph, 'variable', 'string')

        self.s0 = s0 = graph.add_vertex()
        self.v0 = v0 = graph.add_vertex()
        self.v1 = v1 = graph.add_vertex()
        self.v2 = v2 = graph.add_vertex()
        self.t0 = t0 = graph.add_vertex()
        self.v3 = v3 = graph.add_vertex()

        # s0
        # |
        # v0
        # |  \
        # v1  v3
        # |  /
        # v2
        # |
        # t0

        self.e1 = graph.add_edge(s0, v0)
        self.e2 = graph.add_edge(v0, v1)
        self.e5 = graph.add_edge(v0, v3)
        self.e3 = graph.add_edge(v1, v2)
        self.e6 = graph.add_edge(v3, v2)
        self.e4 = graph.add_edge(v2, t0)

    def test_path_enumeration(self):
        paths = dfs_paths(self.graph, self.s0, self.t0)
        self.assert_simple_paths(paths)


    def test_back_edge_enumeration(self):
        self.e7 = self.graph.add_edge(self.v3, self.s0)

        # s0 <-
        # |    |
        # v0   |
        # |  \ |
        # v1  v3
        # |  /
        # v2
        # |
        # t0

        paths = dfs_paths(self.graph, self.s0, self.t0)
        self.assert_simple_paths(paths)

    def test_complex_edge_enumeration(self):
        e7 = self.graph.add_edge(self.s0, self.v2)
        e8 = self.graph.add_edge(self.v3, self.t0)

        # four total paths from s0 to t0
        #
        #  -- s0
        # |   |
        # |   v0
        # |   |  \
        # |   v1  v3
        # |   |  / |
        #  -> v2   |
        #     |    |
        #     t0 <-

        paths = dfs_paths(self.graph, self.s0, self.t0)
        self.assert_simple_paths(paths, 4)

        # first path
        assert([
            self.s0,
            self.v2,
            self.t0
        ] in paths)

        # second path
        assert([
            self.s0,
            self.v0,
            self.v3,
            self.t0
        ] in paths)

    def test_back_edge_branch_enumeration(self):
        self.v4 = v4 = self.graph.add_vertex()
        self.v5 = v5 = self.graph.add_vertex()
        self.e8 = self.graph.add_edge(self.v3, self.v5)
        self.e9 = self.graph.add_edge(self.v5, self.v4)
        self.e10 = self.graph.add_edge(self.v4, self.t0)
        self.e11 = self.graph.add_edge(self.v4, self.s0)

        self.variable[self.v5] = "v5"

        # v5 <-
        # |    |
        # v4 - | -
        # |    |  |
        # s0   |  |
        # |    |  |
        # v0   |  |
        # |  \ |  |
        # v1  v3  |
        # |  /    |
        # v2      |
        # |       |
        # t0 <----

        paths = dfs_paths(self.graph, self.s0, self.t0)
        self.assert_simple_paths(paths, 3)

    def test_back_edge_branch_enumeration_with_cycle(self):
        # TODO sort out the reasoning behind back edge path enumeration
        return

        self.v4 = v4 = self.graph.add_vertex()
        self.e7 = self.graph.add_edge(self.s0, self.v4)
        self.e8 = self.graph.add_edge(self.t0, self.s0)

        self.variable[self.v0] = "v0"
        self.variable[self.v1] = "v1"
        self.variable[self.v2] = "v2"
        self.variable[self.v3] = "v3"
        self.variable[self.v4] = "v4"
        self.variable[self.t0] = "t0"
        self.variable[self.s0] = "s0"

        #  -> s0 ---> v4 (sink)
        # |   |
        # |   v0
        # |   |  \
        # |   v1  v3 (source)
        # |   |  /
        # |   v2
        # |   |
        #  -- t0

        paths = dfs_paths(self.graph, self.v3, self.v4)

        eq_([
            'v3',
            'v2',
            't0',
            's0',
            'v0',
            'v1',
            'v2',
            't0',
            's0',
            'v4'
        ], [ self.graph.vp['variable'][vert] for vert in paths[0] ])

        eq_([
            'v3',
            'v2',
            't0',
            's0',
            'v4'
        ], [ self.graph.vp['variable'][vert] for vert in paths[1] ])

        eq_(len(paths), 2)

    def test_back_edge_branch_enumeration_with_more_cycles(self):
        # TODO sort out the reasoning behind back edge path enumeration
        return

        self.v4 = v4 = self.graph.add_vertex()
        self.e7 = self.graph.add_edge(self.s0, self.v4)
        self.e8 = self.graph.add_edge(self.t0, self.s0)
        self.e9 = self.graph.add_edge(self.v1, self.t0)

        self.variable[self.v0] = "v0"
        self.variable[self.v1] = "v1"
        self.variable[self.v2] = "v2"
        self.variable[self.v3] = "v3"
        self.variable[self.v4] = "v4"
        self.variable[self.t0] = "t0"
        self.variable[self.s0] = "s0"

        #  -> s0 ---> v4 (sink)
        # |   |
        # |   v0
        # |   |  \
        # |  -v1  v3 (source)
        # | | |  /
        # | | v2
        # | | |
        #  -- t0

        paths = dfs_paths(self.graph, self.v3, self.v4)
        var_paths = [[ self.graph.vp['variable'][v] for v in p] for p in paths]

        # assert([
        #     'v3',
        #     'v2',
        #     't0',
        #     's0',
        #     'v0',
        #     'v1',
        #     'v2',
        #     't0',
        #     's0',
        #     'v4'
        # ] in var_paths)

        # assert([
        #     'v3',
        #     'v2',
        #     't0',
        #     's0',
        #     'v0',
        #     'v1',
        #     't0',
        #     's0',
        #     'v4'
        # ] in var_paths)

        # assert([
        #     'v3',
        #     'v2',
        #     't0',
        #     's0',
        #     'v4'
        # ] in var_paths)

        for p in paths:
            print [self.graph.vp['variable'][v] for v in p]

        eq_(len(paths), 5)

    def assert_simple_paths(self, paths, length=2):

        # first path
        assert([
            self.s0,
            self.v0,
            self.v1,
            self.v2,
            self.t0
        ] in paths)

        # second path
        assert([
            self.s0,
            self.v0,
            self.v3,
            self.v2,
            self.t0
        ] in paths)

    def test_all_viable_paths(self):
        # s0
        # |
        # v0
        # |  \
        # v1  v3
        # |  /
        # v2
        # |
        # t0

        self.base_events_variables()
        paths = full_elimination(self.graph, self.s0, self.t0, TSO())
        eq_(len(paths), 2)

    def test_one_viable_path(self):
        # s0
        # |
        # v0
        # |  \
        # v1  v3
        # |  /
        # v2
        # |
        # t0

        self.base_events_variables()

        # prevents right path
        self.variable[self.v3] = 'a'

        paths = full_elimination(self.graph, self.s0, self.t0, TSO())
        eq_(len(paths), 1)

    def test_no_viable_paths(self):
        # s0
        # |
        # v0
        # |  \
        # v1  v3
        # |  /
        # v2
        # |
        # t0

        self.base_events_variables()

        # prevents right path
        self.variable[self.v3] = 'a'

        # prevents left path
        self.variable[self.v2] = 'a'

        paths = full_elimination(self.graph, self.s0, self.t0, TSO())
        eq_(len(paths), 0)

    def base_events_variables(self):
        self.event[self.s0] = 'store'
        self.event[self.v0] = 'load'
        self.event[self.v1] = 'load'
        self.event[self.v2] = 'load'
        self.event[self.v3] = 'load'
        self.event[self.t0] = 'load'

        self.variable[self.s0] = 'a'
        self.variable[self.v0] = 'b'
        self.variable[self.v1] = 'c'
        self.variable[self.v2] = 'd'
        self.variable[self.v3] = 'e'
        self.variable[self.t0] = 'f'
