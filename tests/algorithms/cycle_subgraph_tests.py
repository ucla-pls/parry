from nose.tools import *
from graph_tool import Graph
from parry.algorithms.heavy_cycles import CycleSubgraphSearch
from graph_tool.search import bfs_search

class TestCycleSubgraphSearch():
    def setup(self):
        self.graph = graph = Graph()

        self.s1 = s1 = graph.add_vertex()
        self.v1 = v1 = graph.add_vertex()
        self.v2 = v2 = graph.add_vertex()
        self.t1 = t1 = graph.add_vertex()

        # s1 <-
        # | <--|-- e1
        # v1   | <-- back
        # | <--|-- e2
        # v2 --
        # |
        # t1

        self.e1 = graph.add_edge(s1, v1)
        self.e2 = graph.add_edge(v1, v2)
        self.back = graph.add_edge(v2, s1)
        self.e3 = graph.add_edge(v2, t1)

    def test_find_cycle_subgraph(self):
        cycle_search = CycleSubgraphSearch(self.graph, self.back)
        bfs_search(self.graph, self.s1, cycle_search)

        cycle_edges = cycle_search.cycle_edges()

        eq_(len(cycle_edges), 3)
        eq_(cycle_edges[0], self.e1)
        eq_(cycle_edges[1], self.e2)
        eq_(cycle_edges[2], self.back)

        s,t = cycle_edges[2]
        eq_(s, self.v2)
        eq_(t, self.s1)

    def test_nested_subgraphs(self):

        # s0 <---
        # |      |
        # s1 <-  |
        # |   |  |
        # v1  |  |
        # |   |  |
        # v2 -   |
        # |      |
        # t1     |
        # |      |
        # t0 ----

        s0 = self.graph.add_vertex()
        t0 = self.graph.add_vertex()

        e0 = self.graph.add_edge(s0, self.s1)
        et = self.graph.add_edge(self.t1, t0)
        back2 = self.graph.add_edge(t0, s0)

        cycle_search = CycleSubgraphSearch(self.graph, back2)
        bfs_search(self.graph, s0, cycle_search)

        cycle_edges = cycle_search.cycle_edges()

        eq_(len(cycle_edges), 7)
        eq_(cycle_edges[0], e0)
        eq_(cycle_edges[1], self.e1)
        eq_(cycle_edges[2], self.e2)
        eq_(cycle_edges[3], self.back)
        eq_(cycle_edges[4], self.e3)
        eq_(cycle_edges[5], et)
        eq_(cycle_edges[6], back2)
