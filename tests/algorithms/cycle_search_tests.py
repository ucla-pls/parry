from nose.tools import *
from graph_tool import Graph
from parry.algorithms.heavy_cycles import CycleSearch
from graph_tool.search import dfs_search

class TestCycleSearch():
    def setup(self):
        self.graph = graph = Graph()

        self.s1 = s1 = graph.add_vertex()
        self.v1 = v1 = graph.add_vertex()
        self.v2 = v2 = graph.add_vertex()
        self.t1 = t1 = graph.add_vertex()

        # s1 <-
        # |   |
        # v1  |
        # |   |
        # v2 -
        # |
        # t1

        graph.add_edge(s1, v1)
        graph.add_edge(v1, v2)
        self.back = graph.add_edge(v2, s1)
        graph.add_edge(v2, t1)

    def test_find_back_edge(self):
        cycle_search = CycleSearch()
        dfs_search(self.graph, self.s1, cycle_search)

        back_edges = cycle_search.back_edges()

        eq_(len(back_edges), 1)
        eq_(back_edges[0], self.back)

        s,t = back_edges[0]
        eq_(s, self.v2)
        eq_(t, self.s1)

    # NOTE it's important that the external back edges are identified
    # in the list first so that their internal capacities can be increased
    # each internal loop should have ever increasing capacity
    def test_nested_back_edges(self):
        #  _____
        # v     |
        # s1 <- |
        # |   | |
        # v1  | |
        # |   | |
        # v2 -  |
        # |     |
        # t1 ----

        self.first_back = self.graph.add_edge(self.t1, self.s1)

        cycle_search = CycleSearch()
        dfs_search(self.graph, self.s1, cycle_search)

        back_edges = cycle_search.back_edges()

        eq_(len(back_edges), 2)
        eq_(back_edges[0], self.first_back)
        eq_(back_edges[1], self.back)

        s,t = back_edges[1]
        eq_(s, self.v2)
        eq_(t, self.s1)
