from nose.tools import *
from graph_tool import Graph
from parry.algorithms.subgraph import subgraph

class TestSubgraph():
    def setup(self):
        self.graph = graph = Graph()

        self.s0 = s0 = graph.add_vertex()
        self.s1 = s1 = graph.add_vertex()
        self.v1 = v1 = graph.add_vertex()
        self.v2 = v2 = graph.add_vertex()
        self.t1 = t1 = graph.add_vertex()
        self.v3 = v3 = graph.add_vertex()

        # s0
        # |
        # s1
        # | \
        # v1 \
        # |   v3
        # v2
        # |
        # t1

        self.e1 = graph.add_edge(s0, s1)

        self.e1 = graph.add_edge(s1, v1)
        self.e2 = graph.add_edge(v1, v2)
        self.e3 = graph.add_edge(v2, t1)

        self.e3 = graph.add_edge(s1, v3)


    def test_edge_exclusion(self):
        graph = subgraph(self.graph, self.s1, self.t1)

        edges = [e for e in graph.edges()]

        # should not in clude s1,v3
        eq_(len(edges), 3)

        eq_(str(edges[0]), "(1, 2)")
        eq_(str(edges[1]), "(2, 3)")
        eq_(str(edges[2]), "(3, 4)")

    def test_single_edge_graph(self):
        sg = subgraph(self.graph, self.s1, self.v1)

        edges = [e for e in sg.edges()]

        eq_(len(edges), 1)
        eq_(str(edges[0]), "(1, 2)")

    def test_second_edge_graph(self):
        sg = subgraph(self.graph, self.s1, self.v2)

        edges = [e for e in sg.edges()]

        eq_(len(edges), 2)
        eq_(str(edges[0]), "(1, 2)")
        eq_(str(edges[1]), "(2, 3)")
