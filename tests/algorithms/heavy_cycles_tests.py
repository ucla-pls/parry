from nose.tools import *
from graph_tool import Graph
from parry.algorithms.heavy_cycles import deter_cycle_cut
from graph_tool.search import bfs_search

class TestHeavyCycles:
    def setup(self):
        self.graph = graph = Graph()

        self.s1 = s1 = graph.add_vertex()
        self.v1 = v1 = graph.add_vertex()
        self.v2 = v2 = graph.add_vertex()
        self.t1 = t1 = graph.add_vertex()

        # s1 <-
        # | <--|-- e1
        # v1   | <-- back
        # | <--|-- e2
        # v2 --
        # |
        # t1

        self.e1 = graph.add_edge(s1, v1)
        self.e2 = graph.add_edge(v1, v2)
        self.back = graph.add_edge(v2, s1)
        graph.add_edge(v2, t1)
        graph.edge_properties['capacity'] = graph.new_edge_property('int')

    def test_simple_capacity(self):
        graph = deter_cycle_cut(self.graph)

        eq_(self.get_capacity(graph, self.e1), 3)
        eq_(self.get_capacity(graph, self.e2), 3)
        eq_(self.get_capacity(graph, self.back), 3)


    def test_nested_subgraphs(self):

        # v
        # |
        # s0 <---
        # |      |
        # s1 <-  |
        # |   |  |
        # v1  |  |
        # |   |  |
        # v2 -   |
        # |      |
        # t1     |
        # |      |
        # t0 ----

        v = self.graph.add_vertex()
        s0 = self.graph.add_vertex()
        t0 = self.graph.add_vertex()

        self.graph.add_edge(v, s0)
        self.graph.add_edge(s0, self.s1)
        self.graph.add_edge(self.t1, t0)
        back2 = self.graph.add_edge(t0, s0)

        graph = deter_cycle_cut(self.graph, v)

        # starting at edge weight 1, 2x + 1 = 3
        eq_(self.get_capacity(graph, back2), 3)

        # starting at edge weight 3, 2x + 1 = 7
        eq_(self.get_capacity(graph, self.back), 7)

    def get_capacity(self, graph, e):
        return graph.edge_properties["capacity"][e]
