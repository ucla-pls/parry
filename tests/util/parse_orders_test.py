from nose.tools import *
import parry.util.orders as outil

class TestOrderParsing():
    def test_single_order(self):
        result = outil.parse_orders("1:foo-2:bar")
        eq_(result, [((1, "foo"), (2, "bar"))])

    def test_multiple_orders(self):
        result = outil.parse_orders("1:foo-2:bar,3:bak-4:baz")

        eq_(len(result), 2)

        assert(((1, "foo"), (2, "bar")) in result)
        assert(((3, "bak"), (4, "baz")) in result)

    @raises(outil.InvalidOrderException)
    def test_bad_pair_exception(self):
        result = outil.parse_orders("1:foo-2:bar,3:bak")

    @raises(outil.InvalidOrderException)
    def test_bad_node_exception(self):
        result = outil.parse_orders("1:foo-2:bar,3:bak-bz")
