from nose.tools import *
import parry.util.orders as outil

class TestOrderGrouping():
    def test_many_order_sets(self):
        orders = [
            ((1,'store'), (2, 'load')),
            ((3,'store'), (4, 'load')),
            ((1,'load'), (2, 'load')),
            ((4,'store'), (5, 'store'))
        ]

        groups = outil.group_orders(orders)

        eq_(groups[('store', 'load')], [(1,2), (3,4)])
        eq_(groups[('load', 'load')], [(1,2)])
        eq_(groups[('store', 'store')], [(4,5)])
