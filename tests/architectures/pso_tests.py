from nose.tools import *
from parry.architectures.pso import PSO

class TestPSO():
    def setup(self):
        self.pso = PSO()

    def test_all(self):
        assert self.pso.is_weak(("store", "x"), ("store", "y"))
        assert self.pso.is_weak(("store", "x"), ("load", "y"))

        assert self.pso.is_preserved(("store", "x"), ("store", "x"))
        assert self.pso.is_preserved(("store", "x"), ("load", "x"))
        assert self.pso.is_preserved(("load", "x"), ("store", "y"))
        assert self.pso.is_preserved(("load", "x"), ("load", "x"))
