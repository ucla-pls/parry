from nose.tools import *
from parry.architectures.tso import TSO

class TestTSO():
    def setup(self):
        self.tso = TSO()

    def test_filter_orders(self):
        filtered = self.tso.filter_orders({
            ('store', 'load'): [(1,2), (3,4)],
            ('load', 'load'): [(1,2)],
            ('store', 'store'): [(4,5)]
        })

        eq_(len(filtered), 1)
        eq_(filtered[('store', 'load')], [(1,2), (3,4)])
