from nose.tools import *
from parry.graphs.preserved_program_order import PPOGraph
from tests.graphs.ppo.helpers import Helpers

class TestComplexPPOGraph(Helpers):
    def setup(self):
        self._complex_setup_shared()

    # none of the edges returned should involve e2 and e3 since there's
    # no path from either to the sink t
    def test_ignore_edges(self):
        graph = PPOGraph(self.graph, self.tso, clear=True).graph()

        edges = self._get_edge_pairs(graph)

        assert( (self.e1, self.t) not in edges )
        assert( (self.e2, self.t) not in edges )
        assert( (self.e3, self.t) not in edges )
        assert( (self.e1, self.e2) not in edges )
        assert( (self.e1, self.e3) not in edges )

    def test_cross_path_edges(self):
        graph = self._graph_all_vertices_relevant()
        edges = self._get_edge_pairs(graph)

        # these edges shouldn't in the ppo graph because they fall across paths
        assert((self.e1, self.e2) not in edges)
        assert((self.e2, self.e1) not in edges)

        assert((self.e3, self.e1) not in edges)
        assert((self.e1, self.e3) not in edges)

    def test_back_edges(self):
        # TODO check that all edges point "downstream"
        pass

    def test_ppo_edges(self):
        graph = self._graph_all_vertices_relevant()
        edges = self._get_edge_pairs(graph)

        # ensure we know exactly what is in the ppo graph
        assert(len(edges) == 7)

        # these edges should have a ppo relationship
        # the only vertex that is a 'W' is e1
        assert((self.s, self.e1) in edges) # (R, W)
        assert((self.s, self.e2) in edges) # (R, R)
        assert((self.s, self.e3) in edges) # (R, R)
        assert((self.s, self.t) in edges) # (R, R)

        assert((self.e2, self.e3) in edges) # (R, R)
        assert((self.e2, self.t) in edges) # (R, R)

        assert((self.e3, self.t) in edges) # (R, R)

    def _graph_all_vertices_relevant(self):
        # s
        # |  \
        # e1  e2
        # |    \
        # t -- e3

        # now all nodes should be considered, all on paths from s to t
        self.graph.add_edge(self.e3, self.t)

        return PPOGraph(self.graph, self.tso, clear=True).graph()
