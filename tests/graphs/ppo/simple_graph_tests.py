from nose.tools import *
from parry.graphs.preserved_program_order import PPOGraph
from tests.graphs.ppo.helpers import Helpers

class TestSimplePPOGraph(Helpers):
    def setup(self):
        self._setup_shared()

    def test_no_path(self):
        graph = PPOGraph(self.edgeless_graph, self.sc).graph()
        assert(graph.num_edges() == 0)

    def test_tso(self):
        graph = PPOGraph(self.graph, self.tso, clear=True).graph()

        # NOTE this is done so we can compare vertices directly
        edges = self._get_edge_pairs(graph)

        # expected edges: s -> e, s -> t
        eq_(len(edges), 2)
        assert((self.s, self.e1) in edges)
        assert((self.s, self.t) in edges)
