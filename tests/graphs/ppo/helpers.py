from nose.tools import *
from parry.graphs.preserved_program_order import PPOGraph
from parry.architectures.tso import TSO
from parry.architectures.sc import SC
from graph_tool import Graph

class Helpers():
    def _setup_shared(self):
        # directed by default
        graph = Graph()
        self.s = graph.add_vertex()
        self.e1 = graph.add_vertex()
        self.t = graph.add_vertex()

        event_type_map = graph.new_vertex_property('string')

        event_type_map[self.s] = 'load'
        event_type_map[self.e1] = 'store'
        event_type_map[self.t] = 'load'

        var_map = graph.new_vertex_property('string')

        var_map[self.s] = 'x'
        var_map[self.e1] = 'y'
        var_map[self.t] = 'z'

        graph.vertex_properties["event"] = event_type_map
        graph.vertex_properties["variable"] = var_map

        self.edgeless_graph = graph.copy()

        graph.add_edge(self.s, self.e1)
        graph.add_edge(self.e1, self.t)

        self.graph = graph

        self.tso = TSO()
        self.sc = SC()

    def _complex_setup_shared(self):
        self._setup_shared()

        self.e2 = self.graph.add_vertex()
        self.e3 = self.graph.add_vertex()

        event_type_map = self.graph.vertex_properties["event"]

        event_type_map[self.e2] = 'load'
        event_type_map[self.e3] = 'load'

        var_map = self.graph.vertex_properties["variable"]

        var_map[self.e2] = 'a'
        var_map[self.e3] = 'b'

        # s
        # |  \
        # e1  e2
        # |    \
        # t    e3

        self.graph.add_edge(self.s, self.e2)
        self.graph.add_edge(self.e2, self.e3)

    def _get_edge_pairs(self, graph):
        return [(e.source(), e.target()) for e in graph.edges()]
