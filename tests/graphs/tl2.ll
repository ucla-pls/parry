; ModuleID = 'tl2.c'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.sigaction = type { %union.anon, %struct.__sigset_t, i32, void ()* }
%union.anon = type { void (i32)* }
%struct.__sigset_t = type { [16 x i64] }
%struct._Log = type { %struct._AVPair*, %struct._AVPair*, %struct._AVPair*, %struct._AVPair*, i64, i32 }
%struct._AVPair = type { %struct._AVPair*, %struct._AVPair*, i64*, i64, i64*, i64, i8, %struct._Thread*, i64 }
%struct._Thread = type { i64, i64, i64, i64, i64, i64, i64, i32*, i32, i64, i64, i64, [1 x i64], i8*, %struct.tmalloc*, %struct.tmalloc*, %struct._Log, %struct._Log, %struct._Log, [1 x %struct.__jmp_buf_tag]* }
%struct.tmalloc = type { i64, i64, i8** }
%struct.__jmp_buf_tag = type { [8 x i64], i32, %struct.__sigset_t }
%struct.siginfo_t = type { i32, i32, i32, %union.anon.0 }
%union.anon.0 = type { %struct.anon.3, [80 x i8] }
%struct.anon.3 = type { i32, i32, i32, i64, i64 }

@StartTally = global i64 0, align 8
@AbortTally = global i64 0, align 8
@ReadOverflowTally = global i64 0, align 8
@WriteOverflowTally = global i64 0, align 8
@LocalOverflowTally = global i64 0, align 8
@LockTab = internal global [1048576 x i64] zeroinitializer, align 16
@.str = private unnamed_addr constant [25 x i8] c"TL2 system ready: GV=%s\0A\00", align 1
@.str1 = private unnamed_addr constant [4 x i8] c"GV4\00", align 1
@global_key_self = internal global i32 0, align 4
@.str2 = private unnamed_addr constant [90 x i8] c"TL2 system shutdown:\0A  GCLOCK=0x%lX Starts=%li Aborts=%li\0A  Overflows: R=%li W=%li L=%li\0A\00", align 1
@GClock = internal global [64 x i64] zeroinitializer, align 16
@.str3 = private unnamed_addr constant [2 x i8] c"t\00", align 1
@.str4 = private unnamed_addr constant [6 x i8] c"tl2.c\00", align 1
@__PRETTY_FUNCTION__.TxNewThread = private unnamed_addr constant [22 x i8] c"Thread *TxNewThread()\00", align 1
@.str5 = private unnamed_addr constant [12 x i8] c"t->allocPtr\00", align 1
@__PRETTY_FUNCTION__.TxInitThread = private unnamed_addr constant [34 x i8] c"void TxInitThread(Thread *, long)\00", align 1
@.str6 = private unnamed_addr constant [11 x i8] c"t->freePtr\00", align 1
@.str7 = private unnamed_addr constant [2 x i8] c"e\00", align 1
@__PRETTY_FUNCTION__.ExtendList = private unnamed_addr constant [29 x i8] c"AVPair *ExtendList(AVPair *)\00", align 1
@.str8 = private unnamed_addr constant [3 x i8] c"ap\00", align 1
@__PRETTY_FUNCTION__.MakeList = private unnamed_addr constant [33 x i8] c"AVPair *MakeList(long, Thread *)\00", align 1
@global_act_oldsigbus = internal global %struct.sigaction zeroinitializer, align 8
@.str9 = private unnamed_addr constant [40 x i8] c"Error: Failed to restore SIGBUS handler\00", align 1
@global_act_oldsigsegv = internal global %struct.sigaction zeroinitializer, align 8
@.str10 = private unnamed_addr constant [41 x i8] c"Error: Failed to restore SIGSEGV handler\00", align 1
@.str11 = private unnamed_addr constant [41 x i8] c"Error: Failed to register SIGBUS handler\00", align 1
@.str12 = private unnamed_addr constant [42 x i8] c"Error: Failed to register SIGSEGV handler\00", align 1

; Function Attrs: nounwind uwtable
define i64* @pslock(i64* %Addr) #0 {
  %1 = alloca i64*, align 8
  store i64* %Addr, i64** %1, align 8
  call void @llvm.dbg.declare(metadata !{i64** %1}, metadata !343), !dbg !344
  %2 = load i64** %1, align 8, !dbg !345
  %3 = ptrtoint i64* %2 to i64, !dbg !345
  %4 = add i64 %3, 128, !dbg !345
  %5 = lshr i64 %4, 3, !dbg !345
  %6 = and i64 %5, 1048575, !dbg !345
  %7 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %6, !dbg !345
  ret i64* %7, !dbg !345
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

; Function Attrs: noinline nounwind uwtable
define void @FreeList(%struct._Log* %k, i64 %sz) #2 {
  %1 = alloca %struct._Log*, align 8
  %2 = alloca i64, align 8
  %e = alloca %struct._AVPair*, align 8
  %tmp = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !346), !dbg !347
  store i64 %sz, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !348), !dbg !347
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !349), !dbg !350
  %3 = load %struct._Log** %1, align 8, !dbg !350
  %4 = getelementptr inbounds %struct._Log* %3, i32 0, i32 3, !dbg !350
  %5 = load %struct._AVPair** %4, align 8, !dbg !350
  store %struct._AVPair* %5, %struct._AVPair** %e, align 8, !dbg !350
  %6 = load %struct._AVPair** %e, align 8, !dbg !351
  %7 = icmp ne %struct._AVPair* %6, null, !dbg !351
  br i1 %7, label %8, label %23, !dbg !351

; <label>:8                                       ; preds = %0
  br label %9, !dbg !352

; <label>:9                                       ; preds = %15, %8
  %10 = load %struct._AVPair** %e, align 8, !dbg !352
  %11 = getelementptr inbounds %struct._AVPair* %10, i32 0, i32 8, !dbg !352
  %12 = load i64* %11, align 8, !dbg !352
  %13 = load i64* %2, align 8, !dbg !352
  %14 = icmp sge i64 %12, %13, !dbg !352
  br i1 %14, label %15, label %22, !dbg !352

; <label>:15                                      ; preds = %9
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %tmp}, metadata !354), !dbg !356
  %16 = load %struct._AVPair** %e, align 8, !dbg !356
  store %struct._AVPair* %16, %struct._AVPair** %tmp, align 8, !dbg !356
  %17 = load %struct._AVPair** %e, align 8, !dbg !357
  %18 = getelementptr inbounds %struct._AVPair* %17, i32 0, i32 1, !dbg !357
  %19 = load %struct._AVPair** %18, align 8, !dbg !357
  store %struct._AVPair* %19, %struct._AVPair** %e, align 8, !dbg !357
  %20 = load %struct._AVPair** %tmp, align 8, !dbg !358
  %21 = bitcast %struct._AVPair* %20 to i8*, !dbg !358
  call void @free(i8* %21) #7, !dbg !358
  br label %9, !dbg !359

; <label>:22                                      ; preds = %9
  br label %23, !dbg !360

; <label>:23                                      ; preds = %22, %0
  %24 = load %struct._Log** %1, align 8, !dbg !361
  %25 = getelementptr inbounds %struct._Log* %24, i32 0, i32 0, !dbg !361
  %26 = load %struct._AVPair** %25, align 8, !dbg !361
  %27 = bitcast %struct._AVPair* %26 to i8*, !dbg !361
  call void @free(i8* %27) #7, !dbg !361
  ret void, !dbg !362
}

; Function Attrs: nounwind
declare void @free(i8*) #3

; Function Attrs: nounwind uwtable
define void @TxOnce() #0 {
  %a = alloca [1 x i32], align 4
  call void @llvm.dbg.declare(metadata !{[1 x i32]* %a}, metadata !363), !dbg !366
  %1 = getelementptr inbounds [1 x i32]* %a, i32 0, i64 0, !dbg !366
  store i32 0, i32* %1, align 4, !dbg !366
  call void @GVInit(), !dbg !367
  %2 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([25 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8]* @.str1, i32 0, i32 0)), !dbg !368
  %3 = call i32 @pthread_key_create(i32* @global_key_self, void (i8*)* null) #7, !dbg !369
  call void @registerUseAfterFreeHandler(), !dbg !370
  ret void, !dbg !371
}

declare i32 @printf(i8*, ...) #4

; Function Attrs: nounwind
declare i32 @pthread_key_create(i32*, void (i8*)*) #3

; Function Attrs: nounwind uwtable
define void @TxShutdown() #0 {
  %1 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !372
  %2 = load volatile i64* @StartTally, align 8, !dbg !372
  %3 = load volatile i64* @AbortTally, align 8, !dbg !372
  %4 = load volatile i64* @ReadOverflowTally, align 8, !dbg !372
  %5 = load volatile i64* @WriteOverflowTally, align 8, !dbg !372
  %6 = load volatile i64* @LocalOverflowTally, align 8, !dbg !372
  %7 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([90 x i8]* @.str2, i32 0, i32 0), i64 %1, i64 %2, i64 %3, i64 %4, i64 %5, i64 %6), !dbg !372
  %8 = load i32* @global_key_self, align 4, !dbg !373
  %9 = call i32 @pthread_key_delete(i32 %8) #7, !dbg !373
  call void @restoreUseAfterFreeHandler(), !dbg !374
  call void asm sideeffect "", "~{memory},~{dirflag},~{fpsr},~{flags}"() #7, !dbg !375, !srcloc !376
  ret void, !dbg !377
}

; Function Attrs: nounwind
declare i32 @pthread_key_delete(i32) #3

; Function Attrs: nounwind uwtable
define %struct._Thread* @TxNewThread() #0 {
  %t = alloca %struct._Thread*, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %t}, metadata !378), !dbg !379
  %1 = call noalias i8* @malloc(i64 280) #7, !dbg !379
  %2 = bitcast i8* %1 to %struct._Thread*, !dbg !379
  store %struct._Thread* %2, %struct._Thread** %t, align 8, !dbg !379
  %3 = load %struct._Thread** %t, align 8, !dbg !380
  %4 = icmp ne %struct._Thread* %3, null, !dbg !380
  br i1 %4, label %5, label %6, !dbg !380

; <label>:5                                       ; preds = %0
  br label %8, !dbg !380

; <label>:6                                       ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str3, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1067, i8* getelementptr inbounds ([22 x i8]* @__PRETTY_FUNCTION__.TxNewThread, i32 0, i32 0)) #8, !dbg !380
  unreachable, !dbg !380
                                                  ; No predecessors!
  br label %8, !dbg !380

; <label>:8                                       ; preds = %7, %5
  %9 = load %struct._Thread** %t, align 8, !dbg !381
  ret %struct._Thread* %9, !dbg !381
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #3

; Function Attrs: noreturn nounwind
declare void @__assert_fail(i8*, i8*, i32, i8*) #5

; Function Attrs: nounwind uwtable
define void @TxFreeThread(%struct._Thread* %t) #0 {
  %1 = alloca %struct._Thread*, align 8
  %wrSetOvf = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  store %struct._Thread* %t, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !382), !dbg !383
  %2 = load %struct._Thread** %1, align 8, !dbg !384
  %3 = getelementptr inbounds %struct._Thread* %2, i32 0, i32 16, !dbg !384
  %4 = getelementptr inbounds %struct._Log* %3, i32 0, i32 4, !dbg !384
  %5 = load i64* %4, align 8, !dbg !384
  %6 = call i64 @AtomicAdd(i64* @ReadOverflowTally, i64 %5), !dbg !384
  call void @llvm.dbg.declare(metadata !{i64* %wrSetOvf}, metadata !385), !dbg !386
  store i64 0, i64* %wrSetOvf, align 8, !dbg !386
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !387), !dbg !388
  %7 = load %struct._Thread** %1, align 8, !dbg !389
  %8 = getelementptr inbounds %struct._Thread* %7, i32 0, i32 17, !dbg !389
  store %struct._Log* %8, %struct._Log** %wr, align 8, !dbg !389
  %9 = load %struct._Log** %wr, align 8, !dbg !390
  %10 = getelementptr inbounds %struct._Log* %9, i32 0, i32 4, !dbg !390
  %11 = load i64* %10, align 8, !dbg !390
  %12 = load i64* %wrSetOvf, align 8, !dbg !390
  %13 = add nsw i64 %12, %11, !dbg !390
  store i64 %13, i64* %wrSetOvf, align 8, !dbg !390
  %14 = load i64* %wrSetOvf, align 8, !dbg !392
  %15 = call i64 @AtomicAdd(i64* @WriteOverflowTally, i64 %14), !dbg !392
  %16 = load %struct._Thread** %1, align 8, !dbg !393
  %17 = getelementptr inbounds %struct._Thread* %16, i32 0, i32 18, !dbg !393
  %18 = getelementptr inbounds %struct._Log* %17, i32 0, i32 4, !dbg !393
  %19 = load i64* %18, align 8, !dbg !393
  %20 = call i64 @AtomicAdd(i64* @LocalOverflowTally, i64 %19), !dbg !393
  %21 = load %struct._Thread** %1, align 8, !dbg !394
  %22 = getelementptr inbounds %struct._Thread* %21, i32 0, i32 9, !dbg !394
  %23 = load i64* %22, align 8, !dbg !394
  %24 = call i64 @AtomicAdd(i64* @StartTally, i64 %23), !dbg !394
  %25 = load %struct._Thread** %1, align 8, !dbg !395
  %26 = getelementptr inbounds %struct._Thread* %25, i32 0, i32 10, !dbg !395
  %27 = load i64* %26, align 8, !dbg !395
  %28 = call i64 @AtomicAdd(i64* @AbortTally, i64 %27), !dbg !395
  %29 = load %struct._Thread** %1, align 8, !dbg !396
  %30 = getelementptr inbounds %struct._Thread* %29, i32 0, i32 14, !dbg !396
  %31 = load %struct.tmalloc** %30, align 8, !dbg !396
  call void @tmalloc_free(%struct.tmalloc* %31), !dbg !396
  %32 = load %struct._Thread** %1, align 8, !dbg !397
  %33 = getelementptr inbounds %struct._Thread* %32, i32 0, i32 15, !dbg !397
  %34 = load %struct.tmalloc** %33, align 8, !dbg !397
  call void @tmalloc_free(%struct.tmalloc* %34), !dbg !397
  %35 = load %struct._Thread** %1, align 8, !dbg !398
  %36 = getelementptr inbounds %struct._Thread* %35, i32 0, i32 16, !dbg !398
  call void @FreeList(%struct._Log* %36, i64 8192), !dbg !398
  %37 = load %struct._Thread** %1, align 8, !dbg !399
  %38 = getelementptr inbounds %struct._Thread* %37, i32 0, i32 17, !dbg !399
  call void @FreeList(%struct._Log* %38, i64 1024), !dbg !399
  %39 = load %struct._Thread** %1, align 8, !dbg !400
  %40 = getelementptr inbounds %struct._Thread* %39, i32 0, i32 18, !dbg !400
  call void @FreeList(%struct._Log* %40, i64 1024), !dbg !400
  %41 = load %struct._Thread** %1, align 8, !dbg !401
  %42 = bitcast %struct._Thread* %41 to i8*, !dbg !401
  call void @free(i8* %42) #7, !dbg !401
  ret void, !dbg !402
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @AtomicAdd(i64* %addr, i64 %dx) #6 {
  %1 = alloca i64*, align 8
  %2 = alloca i64, align 8
  %v = alloca i64, align 8
  store i64* %addr, i64** %1, align 8
  call void @llvm.dbg.declare(metadata !{i64** %1}, metadata !403), !dbg !404
  store i64 %dx, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !405), !dbg !404
  call void @llvm.dbg.declare(metadata !{i64* %v}, metadata !406), !dbg !407
  %3 = load i64** %1, align 8, !dbg !408
  %4 = load volatile i64* %3, align 8, !dbg !408
  store i64 %4, i64* %v, align 8, !dbg !408
  br label %5, !dbg !408

; <label>:5                                       ; preds = %15, %0
  %6 = load i64* %v, align 8, !dbg !410
  %7 = load i64* %2, align 8, !dbg !410
  %8 = add nsw i64 %6, %7, !dbg !410
  %9 = load i64* %v, align 8, !dbg !410
  %10 = load i64** %1, align 8, !dbg !410
  %11 = call i64 @cas(i64 %8, i64 %9, i64* %10), !dbg !410
  %12 = load i64* %v, align 8, !dbg !410
  %13 = icmp ne i64 %11, %12, !dbg !410
  br i1 %13, label %14, label %18, !dbg !410

; <label>:14                                      ; preds = %5
  br label %15, !dbg !411

; <label>:15                                      ; preds = %14
  %16 = load i64** %1, align 8, !dbg !408
  %17 = load volatile i64* %16, align 8, !dbg !408
  store i64 %17, i64* %v, align 8, !dbg !408
  br label %5, !dbg !408

; <label>:18                                      ; preds = %5
  %19 = load i64* %v, align 8, !dbg !413
  %20 = load i64* %2, align 8, !dbg !413
  %21 = add nsw i64 %19, %20, !dbg !413
  ret i64 %21, !dbg !413
}

declare void @tmalloc_free(%struct.tmalloc*) #4

; Function Attrs: nounwind uwtable
define void @TxInitThread(%struct._Thread* %t, i64 %id) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64, align 8
  store %struct._Thread* %t, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !414), !dbg !415
  store i64 %id, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !416), !dbg !415
  %3 = load i32* @global_key_self, align 4, !dbg !417
  %4 = load %struct._Thread** %1, align 8, !dbg !417
  %5 = bitcast %struct._Thread* %4 to i8*, !dbg !417
  %6 = call i32 @pthread_setspecific(i32 %3, i8* %5) #7, !dbg !417
  %7 = load %struct._Thread** %1, align 8, !dbg !418
  %8 = bitcast %struct._Thread* %7 to i8*, !dbg !418
  call void @llvm.memset.p0i8.i64(i8* %8, i8 0, i64 280, i32 8, i1 false), !dbg !418
  %9 = load i64* %2, align 8, !dbg !419
  %10 = load %struct._Thread** %1, align 8, !dbg !419
  %11 = getelementptr inbounds %struct._Thread* %10, i32 0, i32 0, !dbg !419
  store i64 %9, i64* %11, align 8, !dbg !419
  %12 = load i64* %2, align 8, !dbg !420
  %13 = add nsw i64 %12, 1, !dbg !420
  %14 = load %struct._Thread** %1, align 8, !dbg !420
  %15 = getelementptr inbounds %struct._Thread* %14, i32 0, i32 11, !dbg !420
  store i64 %13, i64* %15, align 8, !dbg !420
  %16 = load %struct._Thread** %1, align 8, !dbg !421
  %17 = getelementptr inbounds %struct._Thread* %16, i32 0, i32 11, !dbg !421
  %18 = load i64* %17, align 8, !dbg !421
  %19 = load %struct._Thread** %1, align 8, !dbg !421
  %20 = getelementptr inbounds %struct._Thread* %19, i32 0, i32 12, !dbg !421
  %21 = getelementptr inbounds [1 x i64]* %20, i32 0, i64 0, !dbg !421
  store i64 %18, i64* %21, align 8, !dbg !421
  %22 = load %struct._Thread** %1, align 8, !dbg !422
  %23 = call %struct._AVPair* @MakeList(i64 1024, %struct._Thread* %22), !dbg !422
  %24 = load %struct._Thread** %1, align 8, !dbg !422
  %25 = getelementptr inbounds %struct._Thread* %24, i32 0, i32 17, !dbg !422
  %26 = getelementptr inbounds %struct._Log* %25, i32 0, i32 0, !dbg !422
  store %struct._AVPair* %23, %struct._AVPair** %26, align 8, !dbg !422
  %27 = load %struct._Thread** %1, align 8, !dbg !423
  %28 = getelementptr inbounds %struct._Thread* %27, i32 0, i32 17, !dbg !423
  %29 = getelementptr inbounds %struct._Log* %28, i32 0, i32 0, !dbg !423
  %30 = load %struct._AVPair** %29, align 8, !dbg !423
  %31 = load %struct._Thread** %1, align 8, !dbg !423
  %32 = getelementptr inbounds %struct._Thread* %31, i32 0, i32 17, !dbg !423
  %33 = getelementptr inbounds %struct._Log* %32, i32 0, i32 1, !dbg !423
  store %struct._AVPair* %30, %struct._AVPair** %33, align 8, !dbg !423
  %34 = load %struct._Thread** %1, align 8, !dbg !424
  %35 = call %struct._AVPair* @MakeList(i64 8192, %struct._Thread* %34), !dbg !424
  %36 = load %struct._Thread** %1, align 8, !dbg !424
  %37 = getelementptr inbounds %struct._Thread* %36, i32 0, i32 16, !dbg !424
  %38 = getelementptr inbounds %struct._Log* %37, i32 0, i32 0, !dbg !424
  store %struct._AVPair* %35, %struct._AVPair** %38, align 8, !dbg !424
  %39 = load %struct._Thread** %1, align 8, !dbg !425
  %40 = getelementptr inbounds %struct._Thread* %39, i32 0, i32 16, !dbg !425
  %41 = getelementptr inbounds %struct._Log* %40, i32 0, i32 0, !dbg !425
  %42 = load %struct._AVPair** %41, align 8, !dbg !425
  %43 = load %struct._Thread** %1, align 8, !dbg !425
  %44 = getelementptr inbounds %struct._Thread* %43, i32 0, i32 16, !dbg !425
  %45 = getelementptr inbounds %struct._Log* %44, i32 0, i32 1, !dbg !425
  store %struct._AVPair* %42, %struct._AVPair** %45, align 8, !dbg !425
  %46 = load %struct._Thread** %1, align 8, !dbg !426
  %47 = call %struct._AVPair* @MakeList(i64 1024, %struct._Thread* %46), !dbg !426
  %48 = load %struct._Thread** %1, align 8, !dbg !426
  %49 = getelementptr inbounds %struct._Thread* %48, i32 0, i32 18, !dbg !426
  %50 = getelementptr inbounds %struct._Log* %49, i32 0, i32 0, !dbg !426
  store %struct._AVPair* %47, %struct._AVPair** %50, align 8, !dbg !426
  %51 = load %struct._Thread** %1, align 8, !dbg !427
  %52 = getelementptr inbounds %struct._Thread* %51, i32 0, i32 18, !dbg !427
  %53 = getelementptr inbounds %struct._Log* %52, i32 0, i32 0, !dbg !427
  %54 = load %struct._AVPair** %53, align 8, !dbg !427
  %55 = load %struct._Thread** %1, align 8, !dbg !427
  %56 = getelementptr inbounds %struct._Thread* %55, i32 0, i32 18, !dbg !427
  %57 = getelementptr inbounds %struct._Log* %56, i32 0, i32 1, !dbg !427
  store %struct._AVPair* %54, %struct._AVPair** %57, align 8, !dbg !427
  %58 = call %struct.tmalloc* @tmalloc_alloc(i64 1), !dbg !428
  %59 = load %struct._Thread** %1, align 8, !dbg !428
  %60 = getelementptr inbounds %struct._Thread* %59, i32 0, i32 14, !dbg !428
  store %struct.tmalloc* %58, %struct.tmalloc** %60, align 8, !dbg !428
  %61 = load %struct._Thread** %1, align 8, !dbg !429
  %62 = getelementptr inbounds %struct._Thread* %61, i32 0, i32 14, !dbg !429
  %63 = load %struct.tmalloc** %62, align 8, !dbg !429
  %64 = icmp ne %struct.tmalloc* %63, null, !dbg !429
  br i1 %64, label %65, label %66, !dbg !429

; <label>:65                                      ; preds = %0
  br label %68, !dbg !429

; <label>:66                                      ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8]* @.str5, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1151, i8* getelementptr inbounds ([34 x i8]* @__PRETTY_FUNCTION__.TxInitThread, i32 0, i32 0)) #8, !dbg !429
  unreachable, !dbg !429
                                                  ; No predecessors!
  br label %68, !dbg !429

; <label>:68                                      ; preds = %67, %65
  %69 = call %struct.tmalloc* @tmalloc_alloc(i64 1), !dbg !430
  %70 = load %struct._Thread** %1, align 8, !dbg !430
  %71 = getelementptr inbounds %struct._Thread* %70, i32 0, i32 15, !dbg !430
  store %struct.tmalloc* %69, %struct.tmalloc** %71, align 8, !dbg !430
  %72 = load %struct._Thread** %1, align 8, !dbg !431
  %73 = getelementptr inbounds %struct._Thread* %72, i32 0, i32 15, !dbg !431
  %74 = load %struct.tmalloc** %73, align 8, !dbg !431
  %75 = icmp ne %struct.tmalloc* %74, null, !dbg !431
  br i1 %75, label %76, label %77, !dbg !431

; <label>:76                                      ; preds = %68
  br label %79, !dbg !431

; <label>:77                                      ; preds = %68
  call void @__assert_fail(i8* getelementptr inbounds ([11 x i8]* @.str6, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 1153, i8* getelementptr inbounds ([34 x i8]* @__PRETTY_FUNCTION__.TxInitThread, i32 0, i32 0)) #8, !dbg !431
  unreachable, !dbg !431
                                                  ; No predecessors!
  br label %79, !dbg !431

; <label>:79                                      ; preds = %78, %76
  ret void, !dbg !432
}

; Function Attrs: nounwind
declare i32 @pthread_setspecific(i32, i8*) #3

; Function Attrs: nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #7

; Function Attrs: inlinehint nounwind uwtable
define internal %struct._AVPair* @MakeList(i64 %sz, %struct._Thread* %Self) #6 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %ap = alloca %struct._AVPair*, align 8
  %List = alloca %struct._AVPair*, align 8
  %Tail = alloca %struct._AVPair*, align 8
  %i = alloca i64, align 8
  %e = alloca %struct._AVPair*, align 8
  store i64 %sz, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !433), !dbg !434
  store %struct._Thread* %Self, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !435), !dbg !434
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %ap}, metadata !436), !dbg !437
  %3 = load i64* %1, align 8, !dbg !437
  %4 = mul i64 72, %3, !dbg !437
  %5 = add i64 %4, 64, !dbg !437
  %6 = call noalias i8* @malloc(i64 %5) #7, !dbg !437
  %7 = bitcast i8* %6 to %struct._AVPair*, !dbg !437
  store %struct._AVPair* %7, %struct._AVPair** %ap, align 8, !dbg !437
  %8 = load %struct._AVPair** %ap, align 8, !dbg !438
  %9 = icmp ne %struct._AVPair* %8, null, !dbg !438
  br i1 %9, label %10, label %11, !dbg !438

; <label>:10                                      ; preds = %0
  br label %13, !dbg !438

; <label>:11                                      ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([3 x i8]* @.str8, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 591, i8* getelementptr inbounds ([33 x i8]* @__PRETTY_FUNCTION__.MakeList, i32 0, i32 0)) #8, !dbg !438
  unreachable, !dbg !438
                                                  ; No predecessors!
  br label %13, !dbg !438

; <label>:13                                      ; preds = %12, %10
  %14 = load %struct._AVPair** %ap, align 8, !dbg !439
  %15 = bitcast %struct._AVPair* %14 to i8*, !dbg !439
  %16 = load i64* %1, align 8, !dbg !439
  %17 = mul i64 72, %16, !dbg !439
  call void @llvm.memset.p0i8.i64(i8* %15, i8 0, i64 %17, i32 8, i1 false), !dbg !439
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %List}, metadata !440), !dbg !441
  %18 = load %struct._AVPair** %ap, align 8, !dbg !441
  store %struct._AVPair* %18, %struct._AVPair** %List, align 8, !dbg !441
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %Tail}, metadata !442), !dbg !443
  store %struct._AVPair* null, %struct._AVPair** %Tail, align 8, !dbg !443
  call void @llvm.dbg.declare(metadata !{i64* %i}, metadata !444), !dbg !445
  store i64 0, i64* %i, align 8, !dbg !446
  br label %19, !dbg !446

; <label>:19                                      ; preds = %39, %13
  %20 = load i64* %i, align 8, !dbg !446
  %21 = load i64* %1, align 8, !dbg !446
  %22 = icmp slt i64 %20, %21, !dbg !446
  br i1 %22, label %23, label %42, !dbg !446

; <label>:23                                      ; preds = %19
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !448), !dbg !450
  %24 = load %struct._AVPair** %ap, align 8, !dbg !450
  %25 = getelementptr inbounds %struct._AVPair* %24, i32 1, !dbg !450
  store %struct._AVPair* %25, %struct._AVPair** %ap, align 8, !dbg !450
  store %struct._AVPair* %24, %struct._AVPair** %e, align 8, !dbg !450
  %26 = load %struct._AVPair** %ap, align 8, !dbg !451
  %27 = load %struct._AVPair** %e, align 8, !dbg !451
  %28 = getelementptr inbounds %struct._AVPair* %27, i32 0, i32 0, !dbg !451
  store %struct._AVPair* %26, %struct._AVPair** %28, align 8, !dbg !451
  %29 = load %struct._AVPair** %Tail, align 8, !dbg !452
  %30 = load %struct._AVPair** %e, align 8, !dbg !452
  %31 = getelementptr inbounds %struct._AVPair* %30, i32 0, i32 1, !dbg !452
  store %struct._AVPair* %29, %struct._AVPair** %31, align 8, !dbg !452
  %32 = load %struct._Thread** %2, align 8, !dbg !453
  %33 = load %struct._AVPair** %e, align 8, !dbg !453
  %34 = getelementptr inbounds %struct._AVPair* %33, i32 0, i32 7, !dbg !453
  store %struct._Thread* %32, %struct._Thread** %34, align 8, !dbg !453
  %35 = load i64* %i, align 8, !dbg !454
  %36 = load %struct._AVPair** %e, align 8, !dbg !454
  %37 = getelementptr inbounds %struct._AVPair* %36, i32 0, i32 8, !dbg !454
  store i64 %35, i64* %37, align 8, !dbg !454
  %38 = load %struct._AVPair** %e, align 8, !dbg !455
  store %struct._AVPair* %38, %struct._AVPair** %Tail, align 8, !dbg !455
  br label %39, !dbg !456

; <label>:39                                      ; preds = %23
  %40 = load i64* %i, align 8, !dbg !446
  %41 = add nsw i64 %40, 1, !dbg !446
  store i64 %41, i64* %i, align 8, !dbg !446
  br label %19, !dbg !446

; <label>:42                                      ; preds = %19
  %43 = load %struct._AVPair** %Tail, align 8, !dbg !457
  %44 = getelementptr inbounds %struct._AVPair* %43, i32 0, i32 0, !dbg !457
  store %struct._AVPair* null, %struct._AVPair** %44, align 8, !dbg !457
  %45 = load %struct._AVPair** %List, align 8, !dbg !458
  ret %struct._AVPair* %45, !dbg !458
}

declare %struct.tmalloc* @tmalloc_alloc(i64) #4

; Function Attrs: nounwind uwtable
define void @TxAbort(%struct._Thread* %Self) #0 {
  %1 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !459), !dbg !460
  %2 = load %struct._Thread** %1, align 8, !dbg !461
  %3 = getelementptr inbounds %struct._Thread* %2, i32 0, i32 1, !dbg !461
  store volatile i64 5, i64* %3, align 8, !dbg !461
  %4 = load %struct._Thread** %1, align 8, !dbg !462
  %5 = getelementptr inbounds %struct._Thread* %4, i32 0, i32 2, !dbg !462
  %6 = load volatile i64* %5, align 8, !dbg !462
  %7 = icmp ne i64 %6, 0, !dbg !462
  br i1 %7, label %8, label %10, !dbg !462

; <label>:8                                       ; preds = %0
  %9 = load %struct._Thread** %1, align 8, !dbg !463
  call void @RestoreLocks(%struct._Thread* %9), !dbg !463
  br label %10, !dbg !465

; <label>:10                                      ; preds = %8, %0
  %11 = load %struct._Thread** %1, align 8, !dbg !466
  %12 = getelementptr inbounds %struct._Thread* %11, i32 0, i32 18, !dbg !466
  %13 = getelementptr inbounds %struct._Log* %12, i32 0, i32 1, !dbg !466
  %14 = load %struct._AVPair** %13, align 8, !dbg !466
  %15 = load %struct._Thread** %1, align 8, !dbg !466
  %16 = getelementptr inbounds %struct._Thread* %15, i32 0, i32 18, !dbg !466
  %17 = getelementptr inbounds %struct._Log* %16, i32 0, i32 0, !dbg !466
  %18 = load %struct._AVPair** %17, align 8, !dbg !466
  %19 = icmp ne %struct._AVPair* %14, %18, !dbg !466
  br i1 %19, label %20, label %23, !dbg !466

; <label>:20                                      ; preds = %10
  %21 = load %struct._Thread** %1, align 8, !dbg !467
  %22 = getelementptr inbounds %struct._Thread* %21, i32 0, i32 18, !dbg !467
  call void @WriteBackReverse(%struct._Log* %22), !dbg !467
  br label %23, !dbg !469

; <label>:23                                      ; preds = %20, %10
  %24 = load %struct._Thread** %1, align 8, !dbg !470
  %25 = getelementptr inbounds %struct._Thread* %24, i32 0, i32 3, !dbg !470
  %26 = load volatile i64* %25, align 8, !dbg !470
  %27 = add nsw i64 %26, 1, !dbg !470
  store volatile i64 %27, i64* %25, align 8, !dbg !470
  %28 = load %struct._Thread** %1, align 8, !dbg !471
  %29 = getelementptr inbounds %struct._Thread* %28, i32 0, i32 10, !dbg !471
  %30 = load i64* %29, align 8, !dbg !471
  %31 = add nsw i64 %30, 1, !dbg !471
  store i64 %31, i64* %29, align 8, !dbg !471
  %32 = load %struct._Thread** %1, align 8, !dbg !472
  %33 = call i64 @GVAbort(%struct._Thread* %32), !dbg !472
  %34 = icmp ne i64 %33, 0, !dbg !472
  br i1 %34, label %35, label %36, !dbg !472

; <label>:35                                      ; preds = %23
  br label %47, !dbg !473

; <label>:36                                      ; preds = %23
  %37 = load %struct._Thread** %1, align 8, !dbg !475
  %38 = getelementptr inbounds %struct._Thread* %37, i32 0, i32 3, !dbg !475
  %39 = load volatile i64* %38, align 8, !dbg !475
  %40 = icmp sgt i64 %39, 3, !dbg !475
  br i1 %40, label %41, label %46, !dbg !475

; <label>:41                                      ; preds = %36
  %42 = load %struct._Thread** %1, align 8, !dbg !476
  %43 = load %struct._Thread** %1, align 8, !dbg !476
  %44 = getelementptr inbounds %struct._Thread* %43, i32 0, i32 3, !dbg !476
  %45 = load volatile i64* %44, align 8, !dbg !476
  call void @backoff(%struct._Thread* %42, i64 %45), !dbg !476
  br label %46, !dbg !478

; <label>:46                                      ; preds = %41, %36
  br label %47, !dbg !478

; <label>:47                                      ; preds = %46, %35
  %48 = load %struct._Thread** %1, align 8, !dbg !479
  %49 = getelementptr inbounds %struct._Thread* %48, i32 0, i32 14, !dbg !479
  %50 = load %struct.tmalloc** %49, align 8, !dbg !479
  call void @tmalloc_releaseAllReverse(%struct.tmalloc* %50, void (i8*, i64)* null), !dbg !479
  %51 = load %struct._Thread** %1, align 8, !dbg !480
  %52 = getelementptr inbounds %struct._Thread* %51, i32 0, i32 15, !dbg !480
  %53 = load %struct.tmalloc** %52, align 8, !dbg !480
  call void @tmalloc_clear(%struct.tmalloc* %53), !dbg !480
  %54 = load %struct._Thread** %1, align 8, !dbg !481
  %55 = getelementptr inbounds %struct._Thread* %54, i32 0, i32 19, !dbg !481
  %56 = load [1 x %struct.__jmp_buf_tag]** %55, align 8, !dbg !481
  %57 = getelementptr inbounds [1 x %struct.__jmp_buf_tag]* %56, i32 0, i32 0, !dbg !481
  call void @siglongjmp(%struct.__jmp_buf_tag* %57, i32 1) #8, !dbg !481
  unreachable, !dbg !481
                                                  ; No predecessors!
  ret void, !dbg !482
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @RestoreLocks(%struct._Thread* %Self) #6 {
  %1 = alloca %struct._Thread*, align 8
  %wr = alloca %struct._Log*, align 8
  %p = alloca %struct._AVPair*, align 8
  %End = alloca %struct._AVPair*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !483), !dbg !484
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !485), !dbg !486
  %2 = load %struct._Thread** %1, align 8, !dbg !486
  %3 = getelementptr inbounds %struct._Thread* %2, i32 0, i32 17, !dbg !486
  store %struct._Log* %3, %struct._Log** %wr, align 8, !dbg !486
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p}, metadata !487), !dbg !489
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End}, metadata !490), !dbg !492
  %4 = load %struct._Log** %wr, align 8, !dbg !492
  %5 = getelementptr inbounds %struct._Log* %4, i32 0, i32 1, !dbg !492
  %6 = load %struct._AVPair** %5, align 8, !dbg !492
  store %struct._AVPair* %6, %struct._AVPair** %End, align 8, !dbg !492
  %7 = load %struct._Log** %wr, align 8, !dbg !493
  %8 = getelementptr inbounds %struct._Log* %7, i32 0, i32 0, !dbg !493
  %9 = load %struct._AVPair** %8, align 8, !dbg !493
  store %struct._AVPair* %9, %struct._AVPair** %p, align 8, !dbg !493
  br label %10, !dbg !493

; <label>:10                                      ; preds = %30, %0
  %11 = load %struct._AVPair** %p, align 8, !dbg !493
  %12 = load %struct._AVPair** %End, align 8, !dbg !493
  %13 = icmp ne %struct._AVPair* %11, %12, !dbg !493
  br i1 %13, label %14, label %34, !dbg !493

; <label>:14                                      ; preds = %10
  %15 = load %struct._AVPair** %p, align 8, !dbg !495
  %16 = getelementptr inbounds %struct._AVPair* %15, i32 0, i32 6, !dbg !495
  %17 = load i8* %16, align 1, !dbg !495
  %18 = zext i8 %17 to i32, !dbg !495
  %19 = icmp eq i32 %18, 0, !dbg !495
  br i1 %19, label %20, label %21, !dbg !495

; <label>:20                                      ; preds = %14
  br label %30, !dbg !497

; <label>:21                                      ; preds = %14
  %22 = load %struct._AVPair** %p, align 8, !dbg !499
  %23 = getelementptr inbounds %struct._AVPair* %22, i32 0, i32 6, !dbg !499
  store i8 0, i8* %23, align 1, !dbg !499
  %24 = load %struct._AVPair** %p, align 8, !dbg !500
  %25 = getelementptr inbounds %struct._AVPair* %24, i32 0, i32 5, !dbg !500
  %26 = load i64* %25, align 8, !dbg !500
  %27 = load %struct._AVPair** %p, align 8, !dbg !500
  %28 = getelementptr inbounds %struct._AVPair* %27, i32 0, i32 4, !dbg !500
  %29 = load i64** %28, align 8, !dbg !500
  store volatile i64 %26, i64* %29, align 8, !dbg !500
  br label %30, !dbg !501

; <label>:30                                      ; preds = %21, %20
  %31 = load %struct._AVPair** %p, align 8, !dbg !493
  %32 = getelementptr inbounds %struct._AVPair* %31, i32 0, i32 0, !dbg !493
  %33 = load %struct._AVPair** %32, align 8, !dbg !493
  store %struct._AVPair* %33, %struct._AVPair** %p, align 8, !dbg !493
  br label %10, !dbg !493

; <label>:34                                      ; preds = %10
  %35 = load %struct._Thread** %1, align 8, !dbg !502
  %36 = getelementptr inbounds %struct._Thread* %35, i32 0, i32 2, !dbg !502
  store volatile i64 0, i64* %36, align 8, !dbg !502
  ret void, !dbg !503
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @WriteBackReverse(%struct._Log* %k) #6 {
  %1 = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !504), !dbg !505
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !506), !dbg !507
  %2 = load %struct._Log** %1, align 8, !dbg !508
  %3 = getelementptr inbounds %struct._Log* %2, i32 0, i32 2, !dbg !508
  %4 = load %struct._AVPair** %3, align 8, !dbg !508
  store %struct._AVPair* %4, %struct._AVPair** %e, align 8, !dbg !508
  br label %5, !dbg !508

; <label>:5                                       ; preds = %15, %0
  %6 = load %struct._AVPair** %e, align 8, !dbg !508
  %7 = icmp ne %struct._AVPair* %6, null, !dbg !508
  br i1 %7, label %8, label %19, !dbg !508

; <label>:8                                       ; preds = %5
  %9 = load %struct._AVPair** %e, align 8, !dbg !510
  %10 = getelementptr inbounds %struct._AVPair* %9, i32 0, i32 3, !dbg !510
  %11 = load i64* %10, align 8, !dbg !510
  %12 = load %struct._AVPair** %e, align 8, !dbg !510
  %13 = getelementptr inbounds %struct._AVPair* %12, i32 0, i32 2, !dbg !510
  %14 = load i64** %13, align 8, !dbg !510
  store volatile i64 %11, i64* %14, align 8, !dbg !510
  br label %15, !dbg !512

; <label>:15                                      ; preds = %8
  %16 = load %struct._AVPair** %e, align 8, !dbg !508
  %17 = getelementptr inbounds %struct._AVPair* %16, i32 0, i32 1, !dbg !508
  %18 = load %struct._AVPair** %17, align 8, !dbg !508
  store %struct._AVPair* %18, %struct._AVPair** %e, align 8, !dbg !508
  br label %5, !dbg !508

; <label>:19                                      ; preds = %5
  ret void, !dbg !513
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @GVAbort(%struct._Thread* %Self) #6 {
  %1 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !514), !dbg !515
  ret i64 0, !dbg !516
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @backoff(%struct._Thread* %Self, i64 %attempt) #6 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64, align 8
  %stall = alloca i64, align 8
  %i = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !517), !dbg !518
  store i64 %attempt, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !519), !dbg !518
  call void @llvm.dbg.declare(metadata !{i64* %stall}, metadata !520), !dbg !521
  %3 = load %struct._Thread** %1, align 8, !dbg !522
  %4 = call i64 @TSRandom(%struct._Thread* %3), !dbg !522
  %5 = and i64 %4, 15, !dbg !522
  store i64 %5, i64* %stall, align 8, !dbg !522
  %6 = load i64* %2, align 8, !dbg !523
  %7 = ashr i64 %6, 2, !dbg !523
  %8 = load i64* %stall, align 8, !dbg !523
  %9 = add i64 %8, %7, !dbg !523
  store i64 %9, i64* %stall, align 8, !dbg !523
  %10 = load i64* %stall, align 8, !dbg !524
  %11 = mul i64 %10, 10, !dbg !524
  store i64 %11, i64* %stall, align 8, !dbg !524
  call void @llvm.dbg.declare(metadata !{i64* %i}, metadata !525), !dbg !527
  store volatile i64 0, i64* %i, align 8, !dbg !527
  br label %12, !dbg !528

; <label>:12                                      ; preds = %17, %0
  %13 = load volatile i64* %i, align 8, !dbg !528
  %14 = add i64 %13, 1, !dbg !528
  store volatile i64 %14, i64* %i, align 8, !dbg !528
  %15 = load i64* %stall, align 8, !dbg !528
  %16 = icmp ult i64 %13, %15, !dbg !528
  br i1 %16, label %17, label %18, !dbg !528

; <label>:17                                      ; preds = %12
  br label %12, !dbg !529

; <label>:18                                      ; preds = %12
  ret void, !dbg !531
}

declare void @tmalloc_releaseAllReverse(%struct.tmalloc*, void (i8*, i64)*) #4

declare void @tmalloc_clear(%struct.tmalloc*) #4

; Function Attrs: noreturn nounwind
declare void @siglongjmp(%struct.__jmp_buf_tag*, i32) #5

; Function Attrs: nounwind uwtable
define void @TxStore(%struct._Thread* %Self, i64* %addr, i64 %valu) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64*, align 8
  %3 = alloca i64, align 8
  %LockFor = alloca i64*, align 8
  %rdv = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !532), !dbg !533
  store i64* %addr, i64** %2, align 8
  call void @llvm.dbg.declare(metadata !{i64** %2}, metadata !534), !dbg !533
  store i64 %valu, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !535), !dbg !533
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !536), !dbg !537
  call void @llvm.dbg.declare(metadata !{i64* %rdv}, metadata !538), !dbg !539
  %4 = load %struct._Thread** %1, align 8, !dbg !540
  %5 = getelementptr inbounds %struct._Thread* %4, i32 0, i32 8, !dbg !540
  %6 = load i32* %5, align 4, !dbg !540
  %7 = icmp ne i32 %6, 0, !dbg !540
  br i1 %7, label %8, label %13, !dbg !540

; <label>:8                                       ; preds = %0
  %9 = load %struct._Thread** %1, align 8, !dbg !541
  %10 = getelementptr inbounds %struct._Thread* %9, i32 0, i32 7, !dbg !541
  %11 = load i32** %10, align 8, !dbg !541
  store i32 0, i32* %11, align 4, !dbg !541
  %12 = load %struct._Thread** %1, align 8, !dbg !543
  call void @TxAbort(%struct._Thread* %12), !dbg !543
  br label %93, !dbg !544

; <label>:13                                      ; preds = %0
  %14 = load i64** %2, align 8, !dbg !545
  %15 = ptrtoint i64* %14 to i64, !dbg !545
  %16 = add i64 %15, 128, !dbg !545
  %17 = lshr i64 %16, 3, !dbg !545
  %18 = and i64 %17, 1048575, !dbg !545
  %19 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %18, !dbg !545
  store i64* %19, i64** %LockFor, align 8, !dbg !545
  %20 = load i64** %LockFor, align 8, !dbg !546
  %21 = load volatile i64* %20, align 8, !dbg !546
  store i64 %21, i64* %rdv, align 8, !dbg !546
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !547), !dbg !548
  %22 = load %struct._Thread** %1, align 8, !dbg !548
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 17, !dbg !548
  store %struct._Log* %23, %struct._Log** %wr, align 8, !dbg !548
  %24 = load i64** %2, align 8, !dbg !549
  %25 = load volatile i64* %24, align 8, !dbg !549
  %26 = load i64* %3, align 8, !dbg !549
  %27 = icmp eq i64 %25, %26, !dbg !549
  br i1 %27, label %28, label %74, !dbg !549

; <label>:28                                      ; preds = %13
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !550), !dbg !552
  %29 = load %struct._Log** %wr, align 8, !dbg !553
  %30 = getelementptr inbounds %struct._Log* %29, i32 0, i32 2, !dbg !553
  %31 = load %struct._AVPair** %30, align 8, !dbg !553
  store %struct._AVPair* %31, %struct._AVPair** %e, align 8, !dbg !553
  br label %32, !dbg !553

; <label>:32                                      ; preds = %46, %28
  %33 = load %struct._AVPair** %e, align 8, !dbg !553
  %34 = icmp ne %struct._AVPair* %33, null, !dbg !553
  br i1 %34, label %35, label %50, !dbg !553

; <label>:35                                      ; preds = %32
  %36 = load %struct._AVPair** %e, align 8, !dbg !555
  %37 = getelementptr inbounds %struct._AVPair* %36, i32 0, i32 2, !dbg !555
  %38 = load i64** %37, align 8, !dbg !555
  %39 = load i64** %2, align 8, !dbg !555
  %40 = icmp eq i64* %38, %39, !dbg !555
  br i1 %40, label %41, label %45, !dbg !555

; <label>:41                                      ; preds = %35
  %42 = load i64* %3, align 8, !dbg !557
  %43 = load %struct._AVPair** %e, align 8, !dbg !557
  %44 = getelementptr inbounds %struct._AVPair* %43, i32 0, i32 3, !dbg !557
  store i64 %42, i64* %44, align 8, !dbg !557
  br label %93, !dbg !559

; <label>:45                                      ; preds = %35
  br label %46, !dbg !560

; <label>:46                                      ; preds = %45
  %47 = load %struct._AVPair** %e, align 8, !dbg !553
  %48 = getelementptr inbounds %struct._AVPair* %47, i32 0, i32 1, !dbg !553
  %49 = load %struct._AVPair** %48, align 8, !dbg !553
  store %struct._AVPair* %49, %struct._AVPair** %e, align 8, !dbg !553
  br label %32, !dbg !553

; <label>:50                                      ; preds = %32
  %51 = load i64* %rdv, align 8, !dbg !561
  %52 = and i64 %51, 1, !dbg !561
  %53 = icmp eq i64 %52, 0, !dbg !561
  br i1 %53, label %54, label %73, !dbg !561

; <label>:54                                      ; preds = %50
  %55 = load i64* %rdv, align 8, !dbg !561
  %56 = load %struct._Thread** %1, align 8, !dbg !561
  %57 = getelementptr inbounds %struct._Thread* %56, i32 0, i32 4, !dbg !561
  %58 = load volatile i64* %57, align 8, !dbg !561
  %59 = icmp ule i64 %55, %58, !dbg !561
  br i1 %59, label %60, label %73, !dbg !561

; <label>:60                                      ; preds = %54
  %61 = load i64** %LockFor, align 8, !dbg !561
  %62 = load volatile i64* %61, align 8, !dbg !561
  %63 = load i64* %rdv, align 8, !dbg !561
  %64 = icmp eq i64 %62, %63, !dbg !561
  br i1 %64, label %65, label %73, !dbg !561

; <label>:65                                      ; preds = %60
  %66 = load %struct._Thread** %1, align 8, !dbg !562
  %67 = load i64** %LockFor, align 8, !dbg !562
  %68 = call i32 @TrackLoad(%struct._Thread* %66, i64* %67), !dbg !562
  %69 = icmp ne i32 %68, 0, !dbg !562
  br i1 %69, label %72, label %70, !dbg !562

; <label>:70                                      ; preds = %65
  %71 = load %struct._Thread** %1, align 8, !dbg !564
  call void @TxAbort(%struct._Thread* %71), !dbg !564
  br label %72, !dbg !566

; <label>:72                                      ; preds = %70, %65
  br label %93, !dbg !567

; <label>:73                                      ; preds = %60, %54, %50
  br label %74, !dbg !568

; <label>:74                                      ; preds = %73, %13
  %75 = load i64** %2, align 8, !dbg !569
  %76 = ptrtoint i64* %75 to i64, !dbg !569
  %77 = lshr i64 %76, 2, !dbg !569
  %78 = load i64** %2, align 8, !dbg !569
  %79 = ptrtoint i64* %78 to i64, !dbg !569
  %80 = lshr i64 %79, 5, !dbg !569
  %81 = xor i64 %77, %80, !dbg !569
  %82 = and i64 %81, 31, !dbg !569
  %83 = trunc i64 %82 to i32, !dbg !569
  %84 = shl i32 1, %83, !dbg !569
  %85 = load %struct._Log** %wr, align 8, !dbg !569
  %86 = getelementptr inbounds %struct._Log* %85, i32 0, i32 5, !dbg !569
  %87 = load i32* %86, align 4, !dbg !569
  %88 = or i32 %87, %84, !dbg !569
  store i32 %88, i32* %86, align 4, !dbg !569
  %89 = load %struct._Log** %wr, align 8, !dbg !570
  %90 = load i64** %2, align 8, !dbg !570
  %91 = load i64* %3, align 8, !dbg !570
  %92 = load i64** %LockFor, align 8, !dbg !570
  call void @RecordStore(%struct._Log* %89, i64* %90, i64 %91, i64* %92), !dbg !570
  br label %93, !dbg !570

; <label>:93                                      ; preds = %74, %72, %41, %8
  ret void, !dbg !570
}

; Function Attrs: inlinehint nounwind uwtable
define internal i32 @TrackLoad(%struct._Thread* %Self, i64* %LockFor) #6 {
  %1 = alloca i32, align 4
  %2 = alloca %struct._Thread*, align 8
  %3 = alloca i64*, align 8
  %k = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._Thread* %Self, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !571), !dbg !572
  store i64* %LockFor, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !573), !dbg !572
  call void @llvm.dbg.declare(metadata !{%struct._Log** %k}, metadata !574), !dbg !575
  %4 = load %struct._Thread** %2, align 8, !dbg !575
  %5 = getelementptr inbounds %struct._Thread* %4, i32 0, i32 16, !dbg !575
  store %struct._Log* %5, %struct._Log** %k, align 8, !dbg !575
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !576), !dbg !577
  %6 = load %struct._Log** %k, align 8, !dbg !577
  %7 = getelementptr inbounds %struct._Log* %6, i32 0, i32 1, !dbg !577
  %8 = load %struct._AVPair** %7, align 8, !dbg !577
  store %struct._AVPair* %8, %struct._AVPair** %e, align 8, !dbg !577
  %9 = load %struct._AVPair** %e, align 8, !dbg !578
  %10 = icmp eq %struct._AVPair* %9, null, !dbg !578
  br i1 %10, label %11, label %28, !dbg !578

; <label>:11                                      ; preds = %0
  %12 = load %struct._Thread** %2, align 8, !dbg !579
  %13 = call i64 @ReadSetCoherentPessimistic(%struct._Thread* %12), !dbg !579
  %14 = icmp ne i64 %13, 0, !dbg !579
  br i1 %14, label %16, label %15, !dbg !579

; <label>:15                                      ; preds = %11
  store i32 0, i32* %1, !dbg !581
  br label %40, !dbg !581

; <label>:16                                      ; preds = %11
  %17 = load %struct._Log** %k, align 8, !dbg !583
  %18 = getelementptr inbounds %struct._Log* %17, i32 0, i32 4, !dbg !583
  %19 = load i64* %18, align 8, !dbg !583
  %20 = add nsw i64 %19, 1, !dbg !583
  store i64 %20, i64* %18, align 8, !dbg !583
  %21 = load %struct._Log** %k, align 8, !dbg !584
  %22 = getelementptr inbounds %struct._Log* %21, i32 0, i32 2, !dbg !584
  %23 = load %struct._AVPair** %22, align 8, !dbg !584
  %24 = call %struct._AVPair* @ExtendList(%struct._AVPair* %23), !dbg !584
  store %struct._AVPair* %24, %struct._AVPair** %e, align 8, !dbg !584
  %25 = load %struct._AVPair** %e, align 8, !dbg !585
  %26 = load %struct._Log** %k, align 8, !dbg !585
  %27 = getelementptr inbounds %struct._Log* %26, i32 0, i32 3, !dbg !585
  store %struct._AVPair* %25, %struct._AVPair** %27, align 8, !dbg !585
  br label %28, !dbg !586

; <label>:28                                      ; preds = %16, %0
  %29 = load %struct._AVPair** %e, align 8, !dbg !587
  %30 = load %struct._Log** %k, align 8, !dbg !587
  %31 = getelementptr inbounds %struct._Log* %30, i32 0, i32 2, !dbg !587
  store %struct._AVPair* %29, %struct._AVPair** %31, align 8, !dbg !587
  %32 = load %struct._AVPair** %e, align 8, !dbg !588
  %33 = getelementptr inbounds %struct._AVPair* %32, i32 0, i32 0, !dbg !588
  %34 = load %struct._AVPair** %33, align 8, !dbg !588
  %35 = load %struct._Log** %k, align 8, !dbg !588
  %36 = getelementptr inbounds %struct._Log* %35, i32 0, i32 1, !dbg !588
  store %struct._AVPair* %34, %struct._AVPair** %36, align 8, !dbg !588
  %37 = load i64** %3, align 8, !dbg !589
  %38 = load %struct._AVPair** %e, align 8, !dbg !589
  %39 = getelementptr inbounds %struct._AVPair* %38, i32 0, i32 4, !dbg !589
  store i64* %37, i64** %39, align 8, !dbg !589
  store i32 1, i32* %1, !dbg !590
  br label %40, !dbg !590

; <label>:40                                      ; preds = %28, %15
  %41 = load i32* %1, !dbg !590
  ret i32 %41, !dbg !590
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @RecordStore(%struct._Log* %k, i64* %Addr, i64 %Valu, i64* %Lock) #6 {
  %1 = alloca %struct._Log*, align 8
  %2 = alloca i64*, align 8
  %3 = alloca i64, align 8
  %4 = alloca i64*, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !591), !dbg !592
  store i64* %Addr, i64** %2, align 8
  call void @llvm.dbg.declare(metadata !{i64** %2}, metadata !593), !dbg !592
  store i64 %Valu, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !594), !dbg !592
  store i64* %Lock, i64** %4, align 8
  call void @llvm.dbg.declare(metadata !{i64** %4}, metadata !595), !dbg !592
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !596), !dbg !597
  %5 = load %struct._Log** %1, align 8, !dbg !597
  %6 = getelementptr inbounds %struct._Log* %5, i32 0, i32 1, !dbg !597
  %7 = load %struct._AVPair** %6, align 8, !dbg !597
  store %struct._AVPair* %7, %struct._AVPair** %e, align 8, !dbg !597
  %8 = load %struct._AVPair** %e, align 8, !dbg !598
  %9 = icmp eq %struct._AVPair* %8, null, !dbg !598
  br i1 %9, label %10, label %22, !dbg !598

; <label>:10                                      ; preds = %0
  %11 = load %struct._Log** %1, align 8, !dbg !599
  %12 = getelementptr inbounds %struct._Log* %11, i32 0, i32 4, !dbg !599
  %13 = load i64* %12, align 8, !dbg !599
  %14 = add nsw i64 %13, 1, !dbg !599
  store i64 %14, i64* %12, align 8, !dbg !599
  %15 = load %struct._Log** %1, align 8, !dbg !601
  %16 = getelementptr inbounds %struct._Log* %15, i32 0, i32 2, !dbg !601
  %17 = load %struct._AVPair** %16, align 8, !dbg !601
  %18 = call %struct._AVPair* @ExtendList(%struct._AVPair* %17), !dbg !601
  store %struct._AVPair* %18, %struct._AVPair** %e, align 8, !dbg !601
  %19 = load %struct._AVPair** %e, align 8, !dbg !602
  %20 = load %struct._Log** %1, align 8, !dbg !602
  %21 = getelementptr inbounds %struct._Log* %20, i32 0, i32 3, !dbg !602
  store %struct._AVPair* %19, %struct._AVPair** %21, align 8, !dbg !602
  br label %22, !dbg !603

; <label>:22                                      ; preds = %10, %0
  %23 = load %struct._AVPair** %e, align 8, !dbg !604
  %24 = load %struct._Log** %1, align 8, !dbg !604
  %25 = getelementptr inbounds %struct._Log* %24, i32 0, i32 2, !dbg !604
  store %struct._AVPair* %23, %struct._AVPair** %25, align 8, !dbg !604
  %26 = load %struct._AVPair** %e, align 8, !dbg !605
  %27 = getelementptr inbounds %struct._AVPair* %26, i32 0, i32 0, !dbg !605
  %28 = load %struct._AVPair** %27, align 8, !dbg !605
  %29 = load %struct._Log** %1, align 8, !dbg !605
  %30 = getelementptr inbounds %struct._Log* %29, i32 0, i32 1, !dbg !605
  store %struct._AVPair* %28, %struct._AVPair** %30, align 8, !dbg !605
  %31 = load i64** %2, align 8, !dbg !606
  %32 = load %struct._AVPair** %e, align 8, !dbg !606
  %33 = getelementptr inbounds %struct._AVPair* %32, i32 0, i32 2, !dbg !606
  store i64* %31, i64** %33, align 8, !dbg !606
  %34 = load i64* %3, align 8, !dbg !607
  %35 = load %struct._AVPair** %e, align 8, !dbg !607
  %36 = getelementptr inbounds %struct._AVPair* %35, i32 0, i32 3, !dbg !607
  store i64 %34, i64* %36, align 8, !dbg !607
  %37 = load i64** %4, align 8, !dbg !608
  %38 = load %struct._AVPair** %e, align 8, !dbg !608
  %39 = getelementptr inbounds %struct._AVPair* %38, i32 0, i32 4, !dbg !608
  store i64* %37, i64** %39, align 8, !dbg !608
  %40 = load %struct._AVPair** %e, align 8, !dbg !609
  %41 = getelementptr inbounds %struct._AVPair* %40, i32 0, i32 6, !dbg !609
  store i8 0, i8* %41, align 1, !dbg !609
  %42 = load %struct._AVPair** %e, align 8, !dbg !610
  %43 = getelementptr inbounds %struct._AVPair* %42, i32 0, i32 5, !dbg !610
  store i64 1, i64* %43, align 8, !dbg !610
  ret void, !dbg !611
}

; Function Attrs: nounwind uwtable
define i64 @TxLoad(%struct._Thread* %Self, i64* %Addr) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %3 = alloca i64*, align 8
  %Valu = alloca i64, align 8
  %msk = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  %LockFor = alloca i64*, align 8
  %rdv = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !612), !dbg !613
  store i64* %Addr, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !614), !dbg !613
  call void @llvm.dbg.declare(metadata !{i64* %Valu}, metadata !615), !dbg !616
  call void @llvm.dbg.declare(metadata !{i64* %msk}, metadata !617), !dbg !618
  %4 = load i64** %3, align 8, !dbg !618
  %5 = ptrtoint i64* %4 to i64, !dbg !618
  %6 = lshr i64 %5, 2, !dbg !618
  %7 = load i64** %3, align 8, !dbg !618
  %8 = ptrtoint i64* %7 to i64, !dbg !618
  %9 = lshr i64 %8, 5, !dbg !618
  %10 = xor i64 %6, %9, !dbg !618
  %11 = and i64 %10, 31, !dbg !618
  %12 = trunc i64 %11 to i32, !dbg !618
  %13 = shl i32 1, %12, !dbg !618
  %14 = sext i32 %13 to i64, !dbg !618
  store i64 %14, i64* %msk, align 8, !dbg !618
  %15 = load %struct._Thread** %2, align 8, !dbg !619
  %16 = getelementptr inbounds %struct._Thread* %15, i32 0, i32 17, !dbg !619
  %17 = getelementptr inbounds %struct._Log* %16, i32 0, i32 5, !dbg !619
  %18 = load i32* %17, align 4, !dbg !619
  %19 = sext i32 %18 to i64, !dbg !619
  %20 = load i64* %msk, align 8, !dbg !619
  %21 = and i64 %19, %20, !dbg !619
  %22 = load i64* %msk, align 8, !dbg !619
  %23 = icmp eq i64 %21, %22, !dbg !619
  br i1 %23, label %24, label %49, !dbg !619

; <label>:24                                      ; preds = %0
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !620), !dbg !622
  %25 = load %struct._Thread** %2, align 8, !dbg !622
  %26 = getelementptr inbounds %struct._Thread* %25, i32 0, i32 17, !dbg !622
  store %struct._Log* %26, %struct._Log** %wr, align 8, !dbg !622
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !623), !dbg !624
  %27 = load %struct._Log** %wr, align 8, !dbg !625
  %28 = getelementptr inbounds %struct._Log* %27, i32 0, i32 2, !dbg !625
  %29 = load %struct._AVPair** %28, align 8, !dbg !625
  store %struct._AVPair* %29, %struct._AVPair** %e, align 8, !dbg !625
  br label %30, !dbg !625

; <label>:30                                      ; preds = %44, %24
  %31 = load %struct._AVPair** %e, align 8, !dbg !625
  %32 = icmp ne %struct._AVPair* %31, null, !dbg !625
  br i1 %32, label %33, label %48, !dbg !625

; <label>:33                                      ; preds = %30
  %34 = load %struct._AVPair** %e, align 8, !dbg !627
  %35 = getelementptr inbounds %struct._AVPair* %34, i32 0, i32 2, !dbg !627
  %36 = load i64** %35, align 8, !dbg !627
  %37 = load i64** %3, align 8, !dbg !627
  %38 = icmp eq i64* %36, %37, !dbg !627
  br i1 %38, label %39, label %43, !dbg !627

; <label>:39                                      ; preds = %33
  %40 = load %struct._AVPair** %e, align 8, !dbg !629
  %41 = getelementptr inbounds %struct._AVPair* %40, i32 0, i32 3, !dbg !629
  %42 = load i64* %41, align 8, !dbg !629
  store i64 %42, i64* %1, !dbg !629
  br label %91, !dbg !629

; <label>:43                                      ; preds = %33
  br label %44, !dbg !631

; <label>:44                                      ; preds = %43
  %45 = load %struct._AVPair** %e, align 8, !dbg !625
  %46 = getelementptr inbounds %struct._AVPair* %45, i32 0, i32 1, !dbg !625
  %47 = load %struct._AVPair** %46, align 8, !dbg !625
  store %struct._AVPair* %47, %struct._AVPair** %e, align 8, !dbg !625
  br label %30, !dbg !625

; <label>:48                                      ; preds = %30
  br label %49, !dbg !632

; <label>:49                                      ; preds = %48, %0
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !633), !dbg !634
  %50 = load i64** %3, align 8, !dbg !634
  %51 = ptrtoint i64* %50 to i64, !dbg !634
  %52 = add i64 %51, 128, !dbg !634
  %53 = lshr i64 %52, 3, !dbg !634
  %54 = and i64 %53, 1048575, !dbg !634
  %55 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %54, !dbg !634
  store i64* %55, i64** %LockFor, align 8, !dbg !634
  call void @llvm.dbg.declare(metadata !{i64* %rdv}, metadata !635), !dbg !636
  %56 = load i64** %LockFor, align 8, !dbg !636
  %57 = load volatile i64* %56, align 8, !dbg !636
  %58 = and i64 %57, -2, !dbg !636
  store i64 %58, i64* %rdv, align 8, !dbg !636
  %59 = load i64** %3, align 8, !dbg !637
  %60 = load volatile i64* %59, align 8, !dbg !637
  store i64 %60, i64* %Valu, align 8, !dbg !637
  %61 = load i64* %rdv, align 8, !dbg !638
  %62 = load %struct._Thread** %2, align 8, !dbg !638
  %63 = getelementptr inbounds %struct._Thread* %62, i32 0, i32 4, !dbg !638
  %64 = load volatile i64* %63, align 8, !dbg !638
  %65 = icmp ule i64 %61, %64, !dbg !638
  br i1 %65, label %66, label %86, !dbg !638

; <label>:66                                      ; preds = %49
  %67 = load i64** %LockFor, align 8, !dbg !638
  %68 = load volatile i64* %67, align 8, !dbg !638
  %69 = load i64* %rdv, align 8, !dbg !638
  %70 = icmp eq i64 %68, %69, !dbg !638
  br i1 %70, label %71, label %86, !dbg !638

; <label>:71                                      ; preds = %66
  %72 = load %struct._Thread** %2, align 8, !dbg !639
  %73 = getelementptr inbounds %struct._Thread* %72, i32 0, i32 8, !dbg !639
  %74 = load i32* %73, align 4, !dbg !639
  %75 = icmp ne i32 %74, 0, !dbg !639
  br i1 %75, label %84, label %76, !dbg !639

; <label>:76                                      ; preds = %71
  %77 = load %struct._Thread** %2, align 8, !dbg !641
  %78 = load i64** %LockFor, align 8, !dbg !641
  %79 = call i32 @TrackLoad(%struct._Thread* %77, i64* %78), !dbg !641
  %80 = icmp ne i32 %79, 0, !dbg !641
  br i1 %80, label %83, label %81, !dbg !641

; <label>:81                                      ; preds = %76
  %82 = load %struct._Thread** %2, align 8, !dbg !643
  call void @TxAbort(%struct._Thread* %82), !dbg !643
  br label %83, !dbg !645

; <label>:83                                      ; preds = %81, %76
  br label %84, !dbg !646

; <label>:84                                      ; preds = %83, %71
  %85 = load i64* %Valu, align 8, !dbg !647
  store i64 %85, i64* %1, !dbg !647
  br label %91, !dbg !647

; <label>:86                                      ; preds = %66, %49
  %87 = load i64* %rdv, align 8, !dbg !648
  %88 = load %struct._Thread** %2, align 8, !dbg !648
  %89 = getelementptr inbounds %struct._Thread* %88, i32 0, i32 6, !dbg !648
  store i64 %87, i64* %89, align 8, !dbg !648
  %90 = load %struct._Thread** %2, align 8, !dbg !649
  call void @TxAbort(%struct._Thread* %90), !dbg !649
  store i64 0, i64* %1, !dbg !650
  br label %91, !dbg !650

; <label>:91                                      ; preds = %86, %84, %39
  %92 = load i64* %1, !dbg !651
  ret i64 %92, !dbg !651
}

; Function Attrs: nounwind uwtable
define void @TxStoreLocal(%struct._Thread* %Self, i64* %Addr, i64 %Valu) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64*, align 8
  %3 = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !652), !dbg !653
  store i64* %Addr, i64** %2, align 8
  call void @llvm.dbg.declare(metadata !{i64** %2}, metadata !654), !dbg !653
  store i64 %Valu, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !655), !dbg !653
  %4 = load %struct._Thread** %1, align 8, !dbg !656
  %5 = getelementptr inbounds %struct._Thread* %4, i32 0, i32 18, !dbg !656
  %6 = load i64** %2, align 8, !dbg !656
  %7 = load i64** %2, align 8, !dbg !656
  %8 = load volatile i64* %7, align 8, !dbg !656
  call void @SaveForRollBack(%struct._Log* %5, i64* %6, i64 %8), !dbg !656
  %9 = load i64* %3, align 8, !dbg !657
  %10 = load i64** %2, align 8, !dbg !657
  store volatile i64 %9, i64* %10, align 8, !dbg !657
  ret void, !dbg !658
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @SaveForRollBack(%struct._Log* %k, i64* %Addr, i64 %Valu) #6 {
  %1 = alloca %struct._Log*, align 8
  %2 = alloca i64*, align 8
  %3 = alloca i64, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !659), !dbg !660
  store i64* %Addr, i64** %2, align 8
  call void @llvm.dbg.declare(metadata !{i64** %2}, metadata !661), !dbg !660
  store i64 %Valu, i64* %3, align 8
  call void @llvm.dbg.declare(metadata !{i64* %3}, metadata !662), !dbg !660
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !663), !dbg !664
  %4 = load %struct._Log** %1, align 8, !dbg !664
  %5 = getelementptr inbounds %struct._Log* %4, i32 0, i32 1, !dbg !664
  %6 = load %struct._AVPair** %5, align 8, !dbg !664
  store %struct._AVPair* %6, %struct._AVPair** %e, align 8, !dbg !664
  %7 = load %struct._AVPair** %e, align 8, !dbg !665
  %8 = icmp eq %struct._AVPair* %7, null, !dbg !665
  br i1 %8, label %9, label %21, !dbg !665

; <label>:9                                       ; preds = %0
  %10 = load %struct._Log** %1, align 8, !dbg !666
  %11 = getelementptr inbounds %struct._Log* %10, i32 0, i32 4, !dbg !666
  %12 = load i64* %11, align 8, !dbg !666
  %13 = add nsw i64 %12, 1, !dbg !666
  store i64 %13, i64* %11, align 8, !dbg !666
  %14 = load %struct._Log** %1, align 8, !dbg !668
  %15 = getelementptr inbounds %struct._Log* %14, i32 0, i32 2, !dbg !668
  %16 = load %struct._AVPair** %15, align 8, !dbg !668
  %17 = call %struct._AVPair* @ExtendList(%struct._AVPair* %16), !dbg !668
  store %struct._AVPair* %17, %struct._AVPair** %e, align 8, !dbg !668
  %18 = load %struct._AVPair** %e, align 8, !dbg !669
  %19 = load %struct._Log** %1, align 8, !dbg !669
  %20 = getelementptr inbounds %struct._Log* %19, i32 0, i32 3, !dbg !669
  store %struct._AVPair* %18, %struct._AVPair** %20, align 8, !dbg !669
  br label %21, !dbg !670

; <label>:21                                      ; preds = %9, %0
  %22 = load %struct._AVPair** %e, align 8, !dbg !671
  %23 = load %struct._Log** %1, align 8, !dbg !671
  %24 = getelementptr inbounds %struct._Log* %23, i32 0, i32 2, !dbg !671
  store %struct._AVPair* %22, %struct._AVPair** %24, align 8, !dbg !671
  %25 = load %struct._AVPair** %e, align 8, !dbg !672
  %26 = getelementptr inbounds %struct._AVPair* %25, i32 0, i32 0, !dbg !672
  %27 = load %struct._AVPair** %26, align 8, !dbg !672
  %28 = load %struct._Log** %1, align 8, !dbg !672
  %29 = getelementptr inbounds %struct._Log* %28, i32 0, i32 1, !dbg !672
  store %struct._AVPair* %27, %struct._AVPair** %29, align 8, !dbg !672
  %30 = load i64** %2, align 8, !dbg !673
  %31 = load %struct._AVPair** %e, align 8, !dbg !673
  %32 = getelementptr inbounds %struct._AVPair* %31, i32 0, i32 2, !dbg !673
  store i64* %30, i64** %32, align 8, !dbg !673
  %33 = load i64* %3, align 8, !dbg !674
  %34 = load %struct._AVPair** %e, align 8, !dbg !674
  %35 = getelementptr inbounds %struct._AVPair* %34, i32 0, i32 3, !dbg !674
  store i64 %33, i64* %35, align 8, !dbg !674
  %36 = load %struct._AVPair** %e, align 8, !dbg !675
  %37 = getelementptr inbounds %struct._AVPair* %36, i32 0, i32 4, !dbg !675
  store i64* null, i64** %37, align 8, !dbg !675
  ret void, !dbg !676
}

; Function Attrs: nounwind uwtable
define void @TxStart(%struct._Thread* %Self, [1 x %struct.__jmp_buf_tag]* %envPtr, i32* %ROFlag) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca [1 x %struct.__jmp_buf_tag]*, align 8
  %3 = alloca i32*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !677), !dbg !678
  store [1 x %struct.__jmp_buf_tag]* %envPtr, [1 x %struct.__jmp_buf_tag]** %2, align 8
  call void @llvm.dbg.declare(metadata !{[1 x %struct.__jmp_buf_tag]** %2}, metadata !679), !dbg !678
  store i32* %ROFlag, i32** %3, align 8
  call void @llvm.dbg.declare(metadata !{i32** %3}, metadata !680), !dbg !678
  %4 = load %struct._Thread** %1, align 8, !dbg !681
  call void @txReset(%struct._Thread* %4), !dbg !681
  %5 = load %struct._Thread** %1, align 8, !dbg !682
  %6 = call i64 @GVRead(%struct._Thread* %5), !dbg !682
  %7 = load %struct._Thread** %1, align 8, !dbg !682
  %8 = getelementptr inbounds %struct._Thread* %7, i32 0, i32 4, !dbg !682
  store volatile i64 %6, i64* %8, align 8, !dbg !682
  %9 = load %struct._Thread** %1, align 8, !dbg !683
  %10 = getelementptr inbounds %struct._Thread* %9, i32 0, i32 1, !dbg !683
  store volatile i64 1, i64* %10, align 8, !dbg !683
  %11 = load i32** %3, align 8, !dbg !684
  %12 = load %struct._Thread** %1, align 8, !dbg !684
  %13 = getelementptr inbounds %struct._Thread* %12, i32 0, i32 7, !dbg !684
  store i32* %11, i32** %13, align 8, !dbg !684
  %14 = load i32** %3, align 8, !dbg !685
  %15 = icmp ne i32* %14, null, !dbg !685
  br i1 %15, label %16, label %19, !dbg !685

; <label>:16                                      ; preds = %0
  %17 = load i32** %3, align 8, !dbg !685
  %18 = load i32* %17, align 4, !dbg !685
  br label %20, !dbg !685

; <label>:19                                      ; preds = %0
  br label %20, !dbg !685

; <label>:20                                      ; preds = %19, %16
  %21 = phi i32 [ %18, %16 ], [ 0, %19 ], !dbg !685
  %22 = load %struct._Thread** %1, align 8, !dbg !685
  %23 = getelementptr inbounds %struct._Thread* %22, i32 0, i32 8, !dbg !685
  store i32 %21, i32* %23, align 4, !dbg !685
  %24 = load [1 x %struct.__jmp_buf_tag]** %2, align 8, !dbg !686
  %25 = load %struct._Thread** %1, align 8, !dbg !686
  %26 = getelementptr inbounds %struct._Thread* %25, i32 0, i32 19, !dbg !686
  store [1 x %struct.__jmp_buf_tag]* %24, [1 x %struct.__jmp_buf_tag]** %26, align 8, !dbg !686
  %27 = load %struct._Thread** %1, align 8, !dbg !687
  %28 = getelementptr inbounds %struct._Thread* %27, i32 0, i32 9, !dbg !687
  %29 = load i64* %28, align 8, !dbg !687
  %30 = add nsw i64 %29, 1, !dbg !687
  store i64 %30, i64* %28, align 8, !dbg !687
  ret void, !dbg !688
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @txReset(%struct._Thread* %Self) #6 {
  %1 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !689), !dbg !690
  %2 = load %struct._Thread** %1, align 8, !dbg !691
  %3 = getelementptr inbounds %struct._Thread* %2, i32 0, i32 1, !dbg !691
  store volatile i64 0, i64* %3, align 8, !dbg !691
  %4 = load %struct._Thread** %1, align 8, !dbg !692
  %5 = getelementptr inbounds %struct._Thread* %4, i32 0, i32 17, !dbg !692
  %6 = getelementptr inbounds %struct._Log* %5, i32 0, i32 0, !dbg !692
  %7 = load %struct._AVPair** %6, align 8, !dbg !692
  %8 = load %struct._Thread** %1, align 8, !dbg !692
  %9 = getelementptr inbounds %struct._Thread* %8, i32 0, i32 17, !dbg !692
  %10 = getelementptr inbounds %struct._Log* %9, i32 0, i32 1, !dbg !692
  store %struct._AVPair* %7, %struct._AVPair** %10, align 8, !dbg !692
  %11 = load %struct._Thread** %1, align 8, !dbg !693
  %12 = getelementptr inbounds %struct._Thread* %11, i32 0, i32 17, !dbg !693
  %13 = getelementptr inbounds %struct._Log* %12, i32 0, i32 2, !dbg !693
  store %struct._AVPair* null, %struct._AVPair** %13, align 8, !dbg !693
  %14 = load %struct._Thread** %1, align 8, !dbg !694
  %15 = getelementptr inbounds %struct._Thread* %14, i32 0, i32 17, !dbg !694
  %16 = getelementptr inbounds %struct._Log* %15, i32 0, i32 5, !dbg !694
  store i32 0, i32* %16, align 4, !dbg !694
  %17 = load %struct._Thread** %1, align 8, !dbg !695
  %18 = getelementptr inbounds %struct._Thread* %17, i32 0, i32 16, !dbg !695
  %19 = getelementptr inbounds %struct._Log* %18, i32 0, i32 0, !dbg !695
  %20 = load %struct._AVPair** %19, align 8, !dbg !695
  %21 = load %struct._Thread** %1, align 8, !dbg !695
  %22 = getelementptr inbounds %struct._Thread* %21, i32 0, i32 16, !dbg !695
  %23 = getelementptr inbounds %struct._Log* %22, i32 0, i32 1, !dbg !695
  store %struct._AVPair* %20, %struct._AVPair** %23, align 8, !dbg !695
  %24 = load %struct._Thread** %1, align 8, !dbg !696
  %25 = getelementptr inbounds %struct._Thread* %24, i32 0, i32 16, !dbg !696
  %26 = getelementptr inbounds %struct._Log* %25, i32 0, i32 2, !dbg !696
  store %struct._AVPair* null, %struct._AVPair** %26, align 8, !dbg !696
  %27 = load %struct._Thread** %1, align 8, !dbg !697
  %28 = getelementptr inbounds %struct._Thread* %27, i32 0, i32 18, !dbg !697
  %29 = getelementptr inbounds %struct._Log* %28, i32 0, i32 0, !dbg !697
  %30 = load %struct._AVPair** %29, align 8, !dbg !697
  %31 = load %struct._Thread** %1, align 8, !dbg !697
  %32 = getelementptr inbounds %struct._Thread* %31, i32 0, i32 18, !dbg !697
  %33 = getelementptr inbounds %struct._Log* %32, i32 0, i32 1, !dbg !697
  store %struct._AVPair* %30, %struct._AVPair** %33, align 8, !dbg !697
  %34 = load %struct._Thread** %1, align 8, !dbg !698
  %35 = getelementptr inbounds %struct._Thread* %34, i32 0, i32 18, !dbg !698
  %36 = getelementptr inbounds %struct._Log* %35, i32 0, i32 2, !dbg !698
  store %struct._AVPair* null, %struct._AVPair** %36, align 8, !dbg !698
  %37 = load %struct._Thread** %1, align 8, !dbg !699
  %38 = getelementptr inbounds %struct._Thread* %37, i32 0, i32 2, !dbg !699
  store volatile i64 0, i64* %38, align 8, !dbg !699
  ret void, !dbg !700
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @GVRead(%struct._Thread* %Self) #6 {
  %1 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !701), !dbg !702
  %2 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !703
  ret i64 %2, !dbg !703
}

; Function Attrs: nounwind uwtable
define i32 @TxCommit(%struct._Thread* %Self) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !704), !dbg !705
  %3 = load %struct._Thread** %2, align 8, !dbg !706
  %4 = getelementptr inbounds %struct._Thread* %3, i32 0, i32 17, !dbg !706
  %5 = getelementptr inbounds %struct._Log* %4, i32 0, i32 1, !dbg !706
  %6 = load %struct._AVPair** %5, align 8, !dbg !706
  %7 = load %struct._Thread** %2, align 8, !dbg !706
  %8 = getelementptr inbounds %struct._Thread* %7, i32 0, i32 17, !dbg !706
  %9 = getelementptr inbounds %struct._Log* %8, i32 0, i32 0, !dbg !706
  %10 = load %struct._AVPair** %9, align 8, !dbg !706
  %11 = icmp eq %struct._AVPair* %6, %10, !dbg !706
  br i1 %11, label %12, label %20, !dbg !706

; <label>:12                                      ; preds = %0
  %13 = load %struct._Thread** %2, align 8, !dbg !707
  call void @txCommitReset(%struct._Thread* %13), !dbg !707
  %14 = load %struct._Thread** %2, align 8, !dbg !709
  %15 = getelementptr inbounds %struct._Thread* %14, i32 0, i32 14, !dbg !709
  %16 = load %struct.tmalloc** %15, align 8, !dbg !709
  call void @tmalloc_clear(%struct.tmalloc* %16), !dbg !709
  %17 = load %struct._Thread** %2, align 8, !dbg !710
  %18 = getelementptr inbounds %struct._Thread* %17, i32 0, i32 15, !dbg !710
  %19 = load %struct.tmalloc** %18, align 8, !dbg !710
  call void @tmalloc_releaseAllForward(%struct.tmalloc* %19, void (i8*, i64)* @txSterilize), !dbg !710
  store i32 1, i32* %1, !dbg !711
  br label %34, !dbg !711

; <label>:20                                      ; preds = %0
  %21 = load %struct._Thread** %2, align 8, !dbg !712
  %22 = call i64 @TryFastUpdate(%struct._Thread* %21), !dbg !712
  %23 = icmp ne i64 %22, 0, !dbg !712
  br i1 %23, label %24, label %32, !dbg !712

; <label>:24                                      ; preds = %20
  %25 = load %struct._Thread** %2, align 8, !dbg !713
  call void @txCommitReset(%struct._Thread* %25), !dbg !713
  %26 = load %struct._Thread** %2, align 8, !dbg !715
  %27 = getelementptr inbounds %struct._Thread* %26, i32 0, i32 14, !dbg !715
  %28 = load %struct.tmalloc** %27, align 8, !dbg !715
  call void @tmalloc_clear(%struct.tmalloc* %28), !dbg !715
  %29 = load %struct._Thread** %2, align 8, !dbg !716
  %30 = getelementptr inbounds %struct._Thread* %29, i32 0, i32 15, !dbg !716
  %31 = load %struct.tmalloc** %30, align 8, !dbg !716
  call void @tmalloc_releaseAllForward(%struct.tmalloc* %31, void (i8*, i64)* @txSterilize), !dbg !716
  store i32 1, i32* %1, !dbg !717
  br label %34, !dbg !717

; <label>:32                                      ; preds = %20
  %33 = load %struct._Thread** %2, align 8, !dbg !718
  call void @TxAbort(%struct._Thread* %33), !dbg !718
  store i32 0, i32* %1, !dbg !719
  br label %34, !dbg !719

; <label>:34                                      ; preds = %32, %24, %12
  %35 = load i32* %1, !dbg !719
  ret i32 %35, !dbg !719
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @txCommitReset(%struct._Thread* %Self) #6 {
  %1 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !720), !dbg !721
  %2 = load %struct._Thread** %1, align 8, !dbg !722
  call void @txReset(%struct._Thread* %2), !dbg !722
  %3 = load %struct._Thread** %1, align 8, !dbg !723
  %4 = getelementptr inbounds %struct._Thread* %3, i32 0, i32 3, !dbg !723
  store volatile i64 0, i64* %4, align 8, !dbg !723
  ret void, !dbg !724
}

declare void @tmalloc_releaseAllForward(%struct.tmalloc*, void (i8*, i64)*) #4

; Function Attrs: nounwind uwtable
define internal void @txSterilize(i8* %Base, i64 %Length) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i64, align 8
  %Addr = alloca i64*, align 8
  %End = alloca i64*, align 8
  %Lock = alloca i64*, align 8
  %val = alloca i64, align 8
  store i8* %Base, i8** %1, align 8
  call void @llvm.dbg.declare(metadata !{i8** %1}, metadata !725), !dbg !726
  store i64 %Length, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !727), !dbg !726
  call void @llvm.dbg.declare(metadata !{i64** %Addr}, metadata !728), !dbg !731
  %3 = load i8** %1, align 8, !dbg !731
  %4 = bitcast i8* %3 to i64*, !dbg !731
  store i64* %4, i64** %Addr, align 8, !dbg !731
  call void @llvm.dbg.declare(metadata !{i64** %End}, metadata !732), !dbg !733
  %5 = load i64** %Addr, align 8, !dbg !733
  %6 = load i64* %2, align 8, !dbg !733
  %7 = getelementptr inbounds i64* %5, i64 %6, !dbg !733
  store i64* %7, i64** %End, align 8, !dbg !733
  br label %8, !dbg !734

; <label>:8                                       ; preds = %12, %0
  %9 = load i64** %Addr, align 8, !dbg !734
  %10 = load i64** %End, align 8, !dbg !734
  %11 = icmp ult i64* %9, %10, !dbg !734
  br i1 %11, label %12, label %28, !dbg !734

; <label>:12                                      ; preds = %8
  call void @llvm.dbg.declare(metadata !{i64** %Lock}, metadata !735), !dbg !737
  %13 = load i64** %Addr, align 8, !dbg !737
  %14 = ptrtoint i64* %13 to i64, !dbg !737
  %15 = add i64 %14, 128, !dbg !737
  %16 = lshr i64 %15, 3, !dbg !737
  %17 = and i64 %16, 1048575, !dbg !737
  %18 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %17, !dbg !737
  store i64* %18, i64** %Lock, align 8, !dbg !737
  call void @llvm.dbg.declare(metadata !{i64* %val}, metadata !738), !dbg !739
  %19 = load i64** %Lock, align 8, !dbg !739
  %20 = load volatile i64* %19, align 8, !dbg !739
  store i64 %20, i64* %val, align 8, !dbg !739
  %21 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !740
  %22 = and i64 %21, -2, !dbg !740
  %23 = load i64* %val, align 8, !dbg !740
  %24 = load i64** %Lock, align 8, !dbg !740
  %25 = call i64 @cas(i64 %22, i64 %23, i64* %24), !dbg !740
  %26 = load i64** %Addr, align 8, !dbg !741
  %27 = getelementptr inbounds i64* %26, i32 1, !dbg !741
  store i64* %27, i64** %Addr, align 8, !dbg !741
  br label %8, !dbg !742

; <label>:28                                      ; preds = %8
  %29 = load i8** %1, align 8, !dbg !743
  %30 = load i64* %2, align 8, !dbg !743
  call void @llvm.memset.p0i8.i64(i8* %29, i8 -1, i64 %30, i32 1, i1 false), !dbg !743
  ret void, !dbg !744
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @TryFastUpdate(%struct._Thread* %Self) #6 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %wr = alloca %struct._Log*, align 8
  %rd = alloca %struct._Log*, align 8
  %ctr = alloca i64, align 8
  %wv = alloca i64, align 8
  %maxv = alloca i64, align 8
  %p = alloca %struct._AVPair*, align 8
  %End = alloca %struct._AVPair*, align 8
  %LockFor = alloca i64*, align 8
  %cv = alloca i64, align 8
  %c = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !745), !dbg !746
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !747), !dbg !749
  %3 = load %struct._Thread** %2, align 8, !dbg !749
  %4 = getelementptr inbounds %struct._Thread* %3, i32 0, i32 17, !dbg !749
  store %struct._Log* %4, %struct._Log** %wr, align 8, !dbg !749
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd}, metadata !750), !dbg !751
  %5 = load %struct._Thread** %2, align 8, !dbg !751
  %6 = getelementptr inbounds %struct._Thread* %5, i32 0, i32 16, !dbg !751
  store %struct._Log* %6, %struct._Log** %rd, align 8, !dbg !751
  call void @llvm.dbg.declare(metadata !{i64* %ctr}, metadata !752), !dbg !753
  call void @llvm.dbg.declare(metadata !{i64* %wv}, metadata !754), !dbg !755
  %7 = load %struct._Thread** %2, align 8, !dbg !756
  %8 = getelementptr inbounds %struct._Thread* %7, i32 0, i32 2, !dbg !756
  store volatile i64 1, i64* %8, align 8, !dbg !756
  store i64 1000, i64* %ctr, align 8, !dbg !757
  call void @llvm.dbg.declare(metadata !{i64* %maxv}, metadata !758), !dbg !759
  store i64 0, i64* %maxv, align 8, !dbg !759
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p}, metadata !760), !dbg !761
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End}, metadata !762), !dbg !764
  %9 = load %struct._Log** %wr, align 8, !dbg !764
  %10 = getelementptr inbounds %struct._Log* %9, i32 0, i32 1, !dbg !764
  %11 = load %struct._AVPair** %10, align 8, !dbg !764
  store %struct._AVPair* %11, %struct._AVPair** %End, align 8, !dbg !764
  %12 = load %struct._Log** %wr, align 8, !dbg !765
  %13 = getelementptr inbounds %struct._Log* %12, i32 0, i32 0, !dbg !765
  %14 = load %struct._AVPair** %13, align 8, !dbg !765
  store %struct._AVPair* %14, %struct._AVPair** %p, align 8, !dbg !765
  br label %15, !dbg !765

; <label>:15                                      ; preds = %136, %0
  %16 = load %struct._AVPair** %p, align 8, !dbg !765
  %17 = load %struct._AVPair** %End, align 8, !dbg !765
  %18 = icmp ne %struct._AVPair* %16, %17, !dbg !765
  br i1 %18, label %19, label %140, !dbg !765

; <label>:19                                      ; preds = %15
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !767), !dbg !770
  %20 = load %struct._AVPair** %p, align 8, !dbg !770
  %21 = getelementptr inbounds %struct._AVPair* %20, i32 0, i32 4, !dbg !770
  %22 = load i64** %21, align 8, !dbg !770
  store i64* %22, i64** %LockFor, align 8, !dbg !770
  call void @llvm.dbg.declare(metadata !{i64* %cv}, metadata !771), !dbg !772
  %23 = load i64** %LockFor, align 8, !dbg !773
  %24 = bitcast i64* %23 to i8*, !dbg !773
  call void @prefetchw(i8* %24), !dbg !773
  %25 = load i64** %LockFor, align 8, !dbg !774
  %26 = load volatile i64* %25, align 8, !dbg !774
  store i64 %26, i64* %cv, align 8, !dbg !774
  %27 = load i64* %cv, align 8, !dbg !775
  %28 = and i64 %27, 1, !dbg !775
  %29 = icmp ne i64 %28, 0, !dbg !775
  br i1 %29, label %30, label %59, !dbg !775

; <label>:30                                      ; preds = %19
  %31 = load i64* %cv, align 8, !dbg !775
  %32 = xor i64 %31, 1, !dbg !775
  %33 = inttoptr i64 %32 to %struct._AVPair*, !dbg !775
  %34 = getelementptr inbounds %struct._AVPair* %33, i32 0, i32 7, !dbg !775
  %35 = load %struct._Thread** %34, align 8, !dbg !775
  %36 = load %struct._Thread** %2, align 8, !dbg !775
  %37 = icmp eq %struct._Thread* %35, %36, !dbg !775
  br i1 %37, label %38, label %59, !dbg !775

; <label>:38                                      ; preds = %30
  %39 = load %struct._Log** %rd, align 8, !dbg !776
  %40 = load i64** %LockFor, align 8, !dbg !776
  %41 = call %struct._AVPair* @FindFirst(%struct._Log* %39, i64* %40), !dbg !776
  %42 = icmp ne %struct._AVPair* %41, null, !dbg !776
  br i1 %42, label %43, label %58, !dbg !776

; <label>:43                                      ; preds = %38
  %44 = load i64* %cv, align 8, !dbg !778
  %45 = xor i64 %44, 1, !dbg !778
  %46 = inttoptr i64 %45 to %struct._AVPair*, !dbg !778
  %47 = getelementptr inbounds %struct._AVPair* %46, i32 0, i32 5, !dbg !778
  %48 = load i64* %47, align 8, !dbg !778
  %49 = load %struct._Thread** %2, align 8, !dbg !778
  %50 = getelementptr inbounds %struct._Thread* %49, i32 0, i32 4, !dbg !778
  %51 = load volatile i64* %50, align 8, !dbg !778
  %52 = icmp ugt i64 %48, %51, !dbg !778
  br i1 %52, label %53, label %57, !dbg !778

; <label>:53                                      ; preds = %43
  %54 = load i64* %cv, align 8, !dbg !780
  %55 = load %struct._Thread** %2, align 8, !dbg !780
  %56 = getelementptr inbounds %struct._Thread* %55, i32 0, i32 6, !dbg !780
  store i64 %54, i64* %56, align 8, !dbg !780
  store i64 0, i64* %1, !dbg !782
  br label %152, !dbg !782

; <label>:57                                      ; preds = %43
  br label %58, !dbg !783

; <label>:58                                      ; preds = %57, %38
  br label %136, !dbg !784

; <label>:59                                      ; preds = %30, %19
  %60 = load %struct._Log** %rd, align 8, !dbg !785
  %61 = load i64** %LockFor, align 8, !dbg !785
  %62 = call %struct._AVPair* @FindFirst(%struct._Log* %60, i64* %61), !dbg !785
  %63 = icmp ne %struct._AVPair* %62, null, !dbg !785
  br i1 %63, label %64, label %99, !dbg !785

; <label>:64                                      ; preds = %59
  %65 = load i64* %cv, align 8, !dbg !786
  %66 = and i64 %65, 1, !dbg !786
  %67 = icmp eq i64 %66, 0, !dbg !786
  br i1 %67, label %68, label %95, !dbg !786

; <label>:68                                      ; preds = %64
  %69 = load i64* %cv, align 8, !dbg !786
  %70 = load %struct._Thread** %2, align 8, !dbg !786
  %71 = getelementptr inbounds %struct._Thread* %70, i32 0, i32 4, !dbg !786
  %72 = load volatile i64* %71, align 8, !dbg !786
  %73 = icmp ule i64 %69, %72, !dbg !786
  br i1 %73, label %74, label %95, !dbg !786

; <label>:74                                      ; preds = %68
  %75 = load %struct._AVPair** %p, align 8, !dbg !788
  %76 = ptrtoint %struct._AVPair* %75 to i64, !dbg !788
  %77 = or i64 %76, 1, !dbg !788
  %78 = load i64* %cv, align 8, !dbg !788
  %79 = load i64** %LockFor, align 8, !dbg !788
  %80 = call i64 @cas(i64 %77, i64 %78, i64* %79), !dbg !788
  %81 = load i64* %cv, align 8, !dbg !788
  %82 = icmp eq i64 %80, %81, !dbg !788
  br i1 %82, label %83, label %95, !dbg !788

; <label>:83                                      ; preds = %74
  %84 = load i64* %cv, align 8, !dbg !789
  %85 = load i64* %maxv, align 8, !dbg !789
  %86 = icmp ugt i64 %84, %85, !dbg !789
  br i1 %86, label %87, label %89, !dbg !789

; <label>:87                                      ; preds = %83
  %88 = load i64* %cv, align 8, !dbg !791
  store i64 %88, i64* %maxv, align 8, !dbg !791
  br label %89, !dbg !793

; <label>:89                                      ; preds = %87, %83
  %90 = load i64* %cv, align 8, !dbg !794
  %91 = load %struct._AVPair** %p, align 8, !dbg !794
  %92 = getelementptr inbounds %struct._AVPair* %91, i32 0, i32 5, !dbg !794
  store i64 %90, i64* %92, align 8, !dbg !794
  %93 = load %struct._AVPair** %p, align 8, !dbg !795
  %94 = getelementptr inbounds %struct._AVPair* %93, i32 0, i32 6, !dbg !795
  store i8 1, i8* %94, align 1, !dbg !795
  br label %136, !dbg !796

; <label>:95                                      ; preds = %74, %68, %64
  %96 = load i64* %cv, align 8, !dbg !797
  %97 = load %struct._Thread** %2, align 8, !dbg !797
  %98 = getelementptr inbounds %struct._Thread* %97, i32 0, i32 6, !dbg !797
  store i64 %96, i64* %98, align 8, !dbg !797
  store i64 0, i64* %1, !dbg !798
  br label %152, !dbg !798

; <label>:99                                      ; preds = %59
  call void @llvm.dbg.declare(metadata !{i64* %c}, metadata !799), !dbg !801
  %100 = load i64* %ctr, align 8, !dbg !801
  store i64 %100, i64* %c, align 8, !dbg !801
  br label %101, !dbg !802

; <label>:101                                     ; preds = %133, %99
  %102 = load i64** %LockFor, align 8, !dbg !804
  %103 = load volatile i64* %102, align 8, !dbg !804
  store i64 %103, i64* %cv, align 8, !dbg !804
  %104 = load i64* %cv, align 8, !dbg !806
  %105 = and i64 %104, 1, !dbg !806
  %106 = icmp eq i64 %105, 0, !dbg !806
  br i1 %106, label %107, label %128, !dbg !806

; <label>:107                                     ; preds = %101
  %108 = load %struct._AVPair** %p, align 8, !dbg !807
  %109 = ptrtoint %struct._AVPair* %108 to i64, !dbg !807
  %110 = or i64 %109, 1, !dbg !807
  %111 = load i64* %cv, align 8, !dbg !807
  %112 = load i64** %LockFor, align 8, !dbg !807
  %113 = call i64 @cas(i64 %110, i64 %111, i64* %112), !dbg !807
  %114 = load i64* %cv, align 8, !dbg !807
  %115 = icmp eq i64 %113, %114, !dbg !807
  br i1 %115, label %116, label %128, !dbg !807

; <label>:116                                     ; preds = %107
  %117 = load i64* %cv, align 8, !dbg !808
  %118 = load i64* %maxv, align 8, !dbg !808
  %119 = icmp ugt i64 %117, %118, !dbg !808
  br i1 %119, label %120, label %122, !dbg !808

; <label>:120                                     ; preds = %116
  %121 = load i64* %cv, align 8, !dbg !810
  store i64 %121, i64* %maxv, align 8, !dbg !810
  br label %122, !dbg !812

; <label>:122                                     ; preds = %120, %116
  %123 = load i64* %cv, align 8, !dbg !813
  %124 = load %struct._AVPair** %p, align 8, !dbg !813
  %125 = getelementptr inbounds %struct._AVPair* %124, i32 0, i32 5, !dbg !813
  store i64 %123, i64* %125, align 8, !dbg !813
  %126 = load %struct._AVPair** %p, align 8, !dbg !814
  %127 = getelementptr inbounds %struct._AVPair* %126, i32 0, i32 6, !dbg !814
  store i8 1, i8* %127, align 1, !dbg !814
  br label %134, !dbg !815

; <label>:128                                     ; preds = %107, %101
  %129 = load i64* %c, align 8, !dbg !816
  %130 = add nsw i64 %129, -1, !dbg !816
  store i64 %130, i64* %c, align 8, !dbg !816
  %131 = icmp slt i64 %130, 0, !dbg !816
  br i1 %131, label %132, label %133, !dbg !816

; <label>:132                                     ; preds = %128
  store i64 0, i64* %1, !dbg !817
  br label %152, !dbg !817

; <label>:133                                     ; preds = %128
  br label %101, !dbg !819

; <label>:134                                     ; preds = %122
  br label %135

; <label>:135                                     ; preds = %134
  br label %136, !dbg !820

; <label>:136                                     ; preds = %135, %89, %58
  %137 = load %struct._AVPair** %p, align 8, !dbg !765
  %138 = getelementptr inbounds %struct._AVPair* %137, i32 0, i32 0, !dbg !765
  %139 = load %struct._AVPair** %138, align 8, !dbg !765
  store %struct._AVPair* %139, %struct._AVPair** %p, align 8, !dbg !765
  br label %15, !dbg !765

; <label>:140                                     ; preds = %15
  %141 = load %struct._Thread** %2, align 8, !dbg !821
  %142 = load i64* %maxv, align 8, !dbg !821
  %143 = call i64 @GVGenerateWV_GV4(%struct._Thread* %141, i64 %142), !dbg !821
  store i64 %143, i64* %wv, align 8, !dbg !821
  %144 = load %struct._Thread** %2, align 8, !dbg !822
  %145 = call i64 @ReadSetCoherent(%struct._Thread* %144), !dbg !822
  %146 = icmp ne i64 %145, 0, !dbg !822
  br i1 %146, label %148, label %147, !dbg !822

; <label>:147                                     ; preds = %140
  store i64 0, i64* %1, !dbg !823
  br label %152, !dbg !823

; <label>:148                                     ; preds = %140
  %149 = load %struct._Log** %wr, align 8, !dbg !825
  call void @WriteBackForward(%struct._Log* %149), !dbg !825
  %150 = load %struct._Thread** %2, align 8, !dbg !827
  %151 = load i64* %wv, align 8, !dbg !827
  call void @DropLocks(%struct._Thread* %150, i64 %151), !dbg !827
  call void asm sideeffect "", "~{memory},~{dirflag},~{fpsr},~{flags}"() #7, !dbg !828, !srcloc !829
  store i64 1, i64* %1, !dbg !830
  br label %152, !dbg !830

; <label>:152                                     ; preds = %148, %147, %132, %95, %53
  %153 = load i64* %1, !dbg !830
  ret i64 %153, !dbg !830
}

; Function Attrs: nounwind uwtable
define i8* @TxAlloc(%struct._Thread* %Self, i64 %size) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64, align 8
  %ptr = alloca i8*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !831), !dbg !832
  store i64 %size, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !833), !dbg !832
  call void @llvm.dbg.declare(metadata !{i8** %ptr}, metadata !834), !dbg !835
  %3 = load i64* %2, align 8, !dbg !835
  %4 = call i8* @tmalloc_reserve(i64 %3), !dbg !835
  store i8* %4, i8** %ptr, align 8, !dbg !835
  %5 = load i8** %ptr, align 8, !dbg !836
  %6 = icmp ne i8* %5, null, !dbg !836
  br i1 %6, label %7, label %13, !dbg !836

; <label>:7                                       ; preds = %0
  %8 = load %struct._Thread** %1, align 8, !dbg !837
  %9 = getelementptr inbounds %struct._Thread* %8, i32 0, i32 14, !dbg !837
  %10 = load %struct.tmalloc** %9, align 8, !dbg !837
  %11 = load i8** %ptr, align 8, !dbg !837
  %12 = call i64 @tmalloc_append(%struct.tmalloc* %10, i8* %11), !dbg !837
  br label %13, !dbg !839

; <label>:13                                      ; preds = %7, %0
  %14 = load i8** %ptr, align 8, !dbg !840
  ret i8* %14, !dbg !840
}

declare i8* @tmalloc_reserve(i64) #4

declare i64 @tmalloc_append(%struct.tmalloc*, i8*) #4

; Function Attrs: nounwind uwtable
define void @TxFree(%struct._Thread* %Self, i8* %ptr) #0 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i8*, align 8
  %LockFor = alloca i64*, align 8
  %wr = alloca %struct._Log*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !841), !dbg !842
  store i8* %ptr, i8** %2, align 8
  call void @llvm.dbg.declare(metadata !{i8** %2}, metadata !843), !dbg !842
  %3 = load %struct._Thread** %1, align 8, !dbg !844
  %4 = getelementptr inbounds %struct._Thread* %3, i32 0, i32 15, !dbg !844
  %5 = load %struct.tmalloc** %4, align 8, !dbg !844
  %6 = load i8** %2, align 8, !dbg !844
  %7 = call i64 @tmalloc_append(%struct.tmalloc* %5, i8* %6), !dbg !844
  call void @llvm.dbg.declare(metadata !{i64** %LockFor}, metadata !845), !dbg !846
  %8 = load i8** %2, align 8, !dbg !846
  %9 = ptrtoint i8* %8 to i64, !dbg !846
  %10 = add i64 %9, 128, !dbg !846
  %11 = lshr i64 %10, 3, !dbg !846
  %12 = and i64 %11, 1048575, !dbg !846
  %13 = getelementptr inbounds i64* getelementptr inbounds ([1048576 x i64]* @LockTab, i32 0, i32 0), i64 %12, !dbg !846
  store i64* %13, i64** %LockFor, align 8, !dbg !846
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !847), !dbg !848
  %14 = load %struct._Thread** %1, align 8, !dbg !848
  %15 = getelementptr inbounds %struct._Thread* %14, i32 0, i32 17, !dbg !848
  store %struct._Log* %15, %struct._Log** %wr, align 8, !dbg !848
  %16 = load %struct._Log** %wr, align 8, !dbg !849
  %17 = load i8** %2, align 8, !dbg !849
  %18 = bitcast i8* %17 to i64*, !dbg !849
  %19 = load i64** %LockFor, align 8, !dbg !849
  call void @RecordStore(%struct._Log* %16, i64* %18, i64 0, i64* %19), !dbg !849
  ret void, !dbg !850
}

; Function Attrs: nounwind uwtable
define void @thd1(i32 %address, i32 %env_ptr, i32 %roflag, i32 %alloc_size, i32 %alloc_ptr) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %thd = alloca %struct._Thread*, align 8
  store i32 %address, i32* %1, align 4
  call void @llvm.dbg.declare(metadata !{i32* %1}, metadata !851), !dbg !852
  store i32 %env_ptr, i32* %2, align 4
  call void @llvm.dbg.declare(metadata !{i32* %2}, metadata !853), !dbg !852
  store i32 %roflag, i32* %3, align 4
  call void @llvm.dbg.declare(metadata !{i32* %3}, metadata !854), !dbg !852
  store i32 %alloc_size, i32* %4, align 4
  call void @llvm.dbg.declare(metadata !{i32* %4}, metadata !855), !dbg !852
  store i32 %alloc_ptr, i32* %5, align 4
  call void @llvm.dbg.declare(metadata !{i32* %5}, metadata !856), !dbg !852
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %thd}, metadata !857), !dbg !858
  %6 = call %struct._Thread* @TxNewThread(), !dbg !858
  store %struct._Thread* %6, %struct._Thread** %thd, align 8, !dbg !858
  %7 = load %struct._Thread** %thd, align 8, !dbg !859
  call void @TxInitThread(%struct._Thread* %7, i64 1), !dbg !859
  %8 = load %struct._Thread** %thd, align 8, !dbg !860
  %9 = load i32* %2, align 4, !dbg !860
  %10 = sext i32 %9 to i64, !dbg !860
  %11 = inttoptr i64 %10 to [1 x %struct.__jmp_buf_tag]*, !dbg !860
  %12 = load i32* %3, align 4, !dbg !860
  %13 = sext i32 %12 to i64, !dbg !860
  %14 = inttoptr i64 %13 to i32*, !dbg !860
  call void @TxStart(%struct._Thread* %8, [1 x %struct.__jmp_buf_tag]* %11, i32* %14), !dbg !860
  %15 = load %struct._Thread** %thd, align 8, !dbg !861
  %16 = load i32* %1, align 4, !dbg !861
  %17 = sext i32 %16 to i64, !dbg !861
  %18 = inttoptr i64 %17 to i64*, !dbg !861
  call void @TxStore(%struct._Thread* %15, i64* %18, i64 1), !dbg !861
  %19 = load %struct._Thread** %thd, align 8, !dbg !862
  %20 = load i32* %1, align 4, !dbg !862
  %21 = sext i32 %20 to i64, !dbg !862
  %22 = inttoptr i64 %21 to i64*, !dbg !862
  %23 = call i64 @TxLoad(%struct._Thread* %19, i64* %22), !dbg !862
  %24 = load %struct._Thread** %thd, align 8, !dbg !863
  %25 = load i32* %1, align 4, !dbg !863
  %26 = sext i32 %25 to i64, !dbg !863
  %27 = inttoptr i64 %26 to i64*, !dbg !863
  call void @TxStoreLocal(%struct._Thread* %24, i64* %27, i64 1), !dbg !863
  %28 = load %struct._Thread** %thd, align 8, !dbg !864
  %29 = call i32 @TxCommit(%struct._Thread* %28), !dbg !864
  ret void, !dbg !865
}

; Function Attrs: nounwind uwtable
define void @thd2(i32 %address, i32 %env_ptr, i32 %roflag, i32 %alloc_size, i32 %alloc_ptr) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  %thd = alloca %struct._Thread*, align 8
  store i32 %address, i32* %1, align 4
  call void @llvm.dbg.declare(metadata !{i32* %1}, metadata !866), !dbg !867
  store i32 %env_ptr, i32* %2, align 4
  call void @llvm.dbg.declare(metadata !{i32* %2}, metadata !868), !dbg !867
  store i32 %roflag, i32* %3, align 4
  call void @llvm.dbg.declare(metadata !{i32* %3}, metadata !869), !dbg !867
  store i32 %alloc_size, i32* %4, align 4
  call void @llvm.dbg.declare(metadata !{i32* %4}, metadata !870), !dbg !867
  store i32 %alloc_ptr, i32* %5, align 4
  call void @llvm.dbg.declare(metadata !{i32* %5}, metadata !871), !dbg !867
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %thd}, metadata !872), !dbg !873
  %6 = call %struct._Thread* @TxNewThread(), !dbg !873
  store %struct._Thread* %6, %struct._Thread** %thd, align 8, !dbg !873
  %7 = load %struct._Thread** %thd, align 8, !dbg !874
  %8 = load i32* %2, align 4, !dbg !874
  %9 = sext i32 %8 to i64, !dbg !874
  %10 = inttoptr i64 %9 to [1 x %struct.__jmp_buf_tag]*, !dbg !874
  %11 = load i32* %3, align 4, !dbg !874
  %12 = sext i32 %11 to i64, !dbg !874
  %13 = inttoptr i64 %12 to i32*, !dbg !874
  call void @TxStart(%struct._Thread* %7, [1 x %struct.__jmp_buf_tag]* %10, i32* %13), !dbg !874
  %14 = load %struct._Thread** %thd, align 8, !dbg !875
  %15 = load i32* %1, align 4, !dbg !875
  %16 = sext i32 %15 to i64, !dbg !875
  %17 = inttoptr i64 %16 to i64*, !dbg !875
  call void @TxStore(%struct._Thread* %14, i64* %17, i64 1), !dbg !875
  %18 = load %struct._Thread** %thd, align 8, !dbg !877
  call void @TxInitThread(%struct._Thread* %18, i64 1), !dbg !877
  %19 = load %struct._Thread** %thd, align 8, !dbg !878
  %20 = load i32* %1, align 4, !dbg !878
  %21 = sext i32 %20 to i64, !dbg !878
  %22 = inttoptr i64 %21 to i64*, !dbg !878
  call void @TxStore(%struct._Thread* %19, i64* %22, i64 1), !dbg !878
  %23 = load %struct._Thread** %thd, align 8, !dbg !879
  %24 = load i32* %1, align 4, !dbg !879
  %25 = sext i32 %24 to i64, !dbg !879
  %26 = inttoptr i64 %25 to i64*, !dbg !879
  %27 = call i64 @TxLoad(%struct._Thread* %23, i64* %26), !dbg !879
  %28 = load %struct._Thread** %thd, align 8, !dbg !880
  %29 = load i32* %1, align 4, !dbg !880
  %30 = sext i32 %29 to i64, !dbg !880
  %31 = inttoptr i64 %30 to i64*, !dbg !880
  call void @TxStoreLocal(%struct._Thread* %28, i64* %31, i64 1), !dbg !880
  %32 = load %struct._Thread** %thd, align 8, !dbg !881
  %33 = call i32 @TxCommit(%struct._Thread* %32), !dbg !881
  ret void, !dbg !882
}

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %thd = alloca %struct._Thread*, align 8
  %address = alloca i64*, align 8
  %env_ptr = alloca [1 x %struct.__jmp_buf_tag]*, align 8
  %roflag = alloca i32*, align 8
  %alloc_size = alloca i64, align 8
  %alloc_ptr = alloca i8*, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %thd}, metadata !883), !dbg !884
  %1 = call %struct._Thread* @TxNewThread(), !dbg !884
  store %struct._Thread* %1, %struct._Thread** %thd, align 8, !dbg !884
  call void @llvm.dbg.declare(metadata !{i64** %address}, metadata !885), !dbg !886
  call void @llvm.dbg.declare(metadata !{[1 x %struct.__jmp_buf_tag]** %env_ptr}, metadata !887), !dbg !888
  call void @llvm.dbg.declare(metadata !{i32** %roflag}, metadata !889), !dbg !890
  call void @llvm.dbg.declare(metadata !{i64* %alloc_size}, metadata !891), !dbg !892
  store i64 100, i64* %alloc_size, align 8, !dbg !892
  call void @llvm.dbg.declare(metadata !{i8** %alloc_ptr}, metadata !893), !dbg !894
  %2 = load %struct._Thread** %thd, align 8, !dbg !894
  %3 = load i64* %alloc_size, align 8, !dbg !894
  %4 = call i8* @TxAlloc(%struct._Thread* %2, i64 %3), !dbg !894
  store i8* %4, i8** %alloc_ptr, align 8, !dbg !894
  br label %5, !dbg !894

; <label>:5                                       ; preds = %0
  %6 = load i64** %address, align 8, !dbg !895
  %7 = ptrtoint i64* %6 to i32, !dbg !895
  %8 = load [1 x %struct.__jmp_buf_tag]** %env_ptr, align 8, !dbg !895
  %9 = ptrtoint [1 x %struct.__jmp_buf_tag]* %8 to i32, !dbg !895
  %10 = load i32** %roflag, align 8, !dbg !895
  %11 = ptrtoint i32* %10 to i32, !dbg !895
  %12 = load i64* %alloc_size, align 8, !dbg !895
  %13 = trunc i64 %12 to i32, !dbg !895
  %14 = load i8** %alloc_ptr, align 8, !dbg !895
  %15 = ptrtoint i8* %14 to i32, !dbg !895
  call void @thd1(i32 %7, i32 %9, i32 %11, i32 %13, i32 %15), !dbg !895
  %16 = load i64** %address, align 8, !dbg !896
  %17 = ptrtoint i64* %16 to i32, !dbg !896
  %18 = load [1 x %struct.__jmp_buf_tag]** %env_ptr, align 8, !dbg !896
  %19 = ptrtoint [1 x %struct.__jmp_buf_tag]* %18 to i32, !dbg !896
  %20 = load i32** %roflag, align 8, !dbg !896
  %21 = ptrtoint i32* %20 to i32, !dbg !896
  %22 = load i64* %alloc_size, align 8, !dbg !896
  %23 = trunc i64 %22 to i32, !dbg !896
  %24 = load i8** %alloc_ptr, align 8, !dbg !896
  %25 = ptrtoint i8* %24 to i32, !dbg !896
  call void @thd2(i32 %17, i32 %19, i32 %21, i32 %23, i32 %25), !dbg !896
  ret i32 0, !dbg !897
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @prefetchw(i8* %x) #6 {
  %1 = alloca i8*, align 8
  store i8* %x, i8** %1, align 8
  call void @llvm.dbg.declare(metadata !{i8** %1}, metadata !898), !dbg !899
  ret void, !dbg !900
}

; Function Attrs: inlinehint nounwind uwtable
define internal %struct._AVPair* @FindFirst(%struct._Log* %k, i64* %Lock) #6 {
  %1 = alloca %struct._AVPair*, align 8
  %2 = alloca %struct._Log*, align 8
  %3 = alloca i64*, align 8
  %e = alloca %struct._AVPair*, align 8
  %End = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %2}, metadata !902), !dbg !903
  store i64* %Lock, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !904), !dbg !903
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !905), !dbg !907
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End}, metadata !908), !dbg !909
  %4 = load %struct._Log** %2, align 8, !dbg !909
  %5 = getelementptr inbounds %struct._Log* %4, i32 0, i32 1, !dbg !909
  %6 = load %struct._AVPair** %5, align 8, !dbg !909
  store %struct._AVPair* %6, %struct._AVPair** %End, align 8, !dbg !909
  %7 = load %struct._Log** %2, align 8, !dbg !910
  %8 = getelementptr inbounds %struct._Log* %7, i32 0, i32 0, !dbg !910
  %9 = load %struct._AVPair** %8, align 8, !dbg !910
  store %struct._AVPair* %9, %struct._AVPair** %e, align 8, !dbg !910
  br label %10, !dbg !910

; <label>:10                                      ; preds = %23, %0
  %11 = load %struct._AVPair** %e, align 8, !dbg !910
  %12 = load %struct._AVPair** %End, align 8, !dbg !910
  %13 = icmp ne %struct._AVPair* %11, %12, !dbg !910
  br i1 %13, label %14, label %27, !dbg !910

; <label>:14                                      ; preds = %10
  %15 = load %struct._AVPair** %e, align 8, !dbg !912
  %16 = getelementptr inbounds %struct._AVPair* %15, i32 0, i32 4, !dbg !912
  %17 = load i64** %16, align 8, !dbg !912
  %18 = load i64** %3, align 8, !dbg !912
  %19 = icmp eq i64* %17, %18, !dbg !912
  br i1 %19, label %20, label %22, !dbg !912

; <label>:20                                      ; preds = %14
  %21 = load %struct._AVPair** %e, align 8, !dbg !914
  store %struct._AVPair* %21, %struct._AVPair** %1, !dbg !914
  br label %28, !dbg !914

; <label>:22                                      ; preds = %14
  br label %23, !dbg !916

; <label>:23                                      ; preds = %22
  %24 = load %struct._AVPair** %e, align 8, !dbg !910
  %25 = getelementptr inbounds %struct._AVPair* %24, i32 0, i32 0, !dbg !910
  %26 = load %struct._AVPair** %25, align 8, !dbg !910
  store %struct._AVPair* %26, %struct._AVPair** %e, align 8, !dbg !910
  br label %10, !dbg !910

; <label>:27                                      ; preds = %10
  store %struct._AVPair* null, %struct._AVPair** %1, !dbg !917
  br label %28, !dbg !917

; <label>:28                                      ; preds = %27, %20
  %29 = load %struct._AVPair** %1, !dbg !918
  ret %struct._AVPair* %29, !dbg !918
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @cas(i64 %newVal, i64 %oldVal, i64* %ptr) #6 {
  %1 = alloca i64, align 8
  %2 = alloca i64, align 8
  %3 = alloca i64*, align 8
  %prevVal = alloca i64, align 8
  store i64 %newVal, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !919), !dbg !920
  store i64 %oldVal, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !921), !dbg !920
  store i64* %ptr, i64** %3, align 8
  call void @llvm.dbg.declare(metadata !{i64** %3}, metadata !922), !dbg !920
  call void @llvm.dbg.declare(metadata !{i64* %prevVal}, metadata !923), !dbg !925
  %4 = load i64* %1, align 8, !dbg !926
  %5 = load i64** %3, align 8, !dbg !926
  %6 = load i64* %2, align 8, !dbg !926
  %7 = call i64 asm sideeffect "lock \0Acmpxchgq $1,$2 \0A", "={ax},q,*m,0,~{memory},~{dirflag},~{fpsr},~{flags}"(i64 %4, i64* %5, i64 %6) #7, !dbg !926, !srcloc !927
  store i64 %7, i64* %prevVal, align 8, !dbg !926
  %8 = load i64* %prevVal, align 8, !dbg !928
  ret i64 %8, !dbg !928
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @GVGenerateWV_GV4(%struct._Thread* %Self, i64 %maxv) #6 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64, align 8
  %gv = alloca i64, align 8
  %wv = alloca i64, align 8
  %k = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !929), !dbg !930
  store i64 %maxv, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !931), !dbg !930
  call void @llvm.dbg.declare(metadata !{i64* %gv}, metadata !932), !dbg !933
  %3 = load volatile i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !933
  store i64 %3, i64* %gv, align 8, !dbg !933
  call void @llvm.dbg.declare(metadata !{i64* %wv}, metadata !934), !dbg !935
  %4 = load i64* %gv, align 8, !dbg !935
  %5 = add i64 %4, 2, !dbg !935
  store i64 %5, i64* %wv, align 8, !dbg !935
  call void @llvm.dbg.declare(metadata !{i64* %k}, metadata !936), !dbg !937
  %6 = load i64* %wv, align 8, !dbg !938
  %7 = load i64* %gv, align 8, !dbg !938
  %8 = call i64 @cas(i64 %6, i64 %7, i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32)), !dbg !938
  store i64 %8, i64* %k, align 8, !dbg !938
  %9 = load i64* %k, align 8, !dbg !939
  %10 = load i64* %gv, align 8, !dbg !939
  %11 = icmp ne i64 %9, %10, !dbg !939
  br i1 %11, label %12, label %14, !dbg !939

; <label>:12                                      ; preds = %0
  %13 = load i64* %k, align 8, !dbg !940
  store i64 %13, i64* %wv, align 8, !dbg !940
  br label %14, !dbg !942

; <label>:14                                      ; preds = %12, %0
  %15 = load i64* %wv, align 8, !dbg !943
  %16 = load %struct._Thread** %1, align 8, !dbg !943
  %17 = getelementptr inbounds %struct._Thread* %16, i32 0, i32 5, !dbg !943
  store i64 %15, i64* %17, align 8, !dbg !943
  %18 = load i64* %wv, align 8, !dbg !944
  ret i64 %18, !dbg !944
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @ReadSetCoherent(%struct._Thread* %Self) #6 {
  %1 = alloca %struct._Thread*, align 8
  %dx = alloca i64, align 8
  %rv = alloca i64, align 8
  %rd = alloca %struct._Log*, align 8
  %EndOfList = alloca %struct._AVPair*, align 8
  %e = alloca %struct._AVPair*, align 8
  %v = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !945), !dbg !946
  call void @llvm.dbg.declare(metadata !{i64* %dx}, metadata !947), !dbg !948
  store i64 0, i64* %dx, align 8, !dbg !948
  call void @llvm.dbg.declare(metadata !{i64* %rv}, metadata !949), !dbg !950
  %2 = load %struct._Thread** %1, align 8, !dbg !950
  %3 = getelementptr inbounds %struct._Thread* %2, i32 0, i32 4, !dbg !950
  %4 = load volatile i64* %3, align 8, !dbg !950
  store i64 %4, i64* %rv, align 8, !dbg !950
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd}, metadata !951), !dbg !952
  %5 = load %struct._Thread** %1, align 8, !dbg !952
  %6 = getelementptr inbounds %struct._Thread* %5, i32 0, i32 16, !dbg !952
  store %struct._Log* %6, %struct._Log** %rd, align 8, !dbg !952
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList}, metadata !953), !dbg !954
  %7 = load %struct._Log** %rd, align 8, !dbg !954
  %8 = getelementptr inbounds %struct._Log* %7, i32 0, i32 1, !dbg !954
  %9 = load %struct._AVPair** %8, align 8, !dbg !954
  store %struct._AVPair* %9, %struct._AVPair** %EndOfList, align 8, !dbg !954
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !955), !dbg !956
  %10 = load %struct._Log** %rd, align 8, !dbg !957
  %11 = getelementptr inbounds %struct._Log* %10, i32 0, i32 0, !dbg !957
  %12 = load %struct._AVPair** %11, align 8, !dbg !957
  store %struct._AVPair* %12, %struct._AVPair** %e, align 8, !dbg !957
  br label %13, !dbg !957

; <label>:13                                      ; preds = %46, %0
  %14 = load %struct._AVPair** %e, align 8, !dbg !957
  %15 = load %struct._AVPair** %EndOfList, align 8, !dbg !957
  %16 = icmp ne %struct._AVPair* %14, %15, !dbg !957
  br i1 %16, label %17, label %50, !dbg !957

; <label>:17                                      ; preds = %13
  call void @llvm.dbg.declare(metadata !{i64* %v}, metadata !959), !dbg !961
  %18 = load %struct._AVPair** %e, align 8, !dbg !961
  %19 = getelementptr inbounds %struct._AVPair* %18, i32 0, i32 4, !dbg !961
  %20 = load i64** %19, align 8, !dbg !961
  %21 = load volatile i64* %20, align 8, !dbg !961
  store i64 %21, i64* %v, align 8, !dbg !961
  %22 = load i64* %v, align 8, !dbg !962
  %23 = and i64 %22, 1, !dbg !962
  %24 = icmp ne i64 %23, 0, !dbg !962
  br i1 %24, label %25, label %37, !dbg !962

; <label>:25                                      ; preds = %17
  %26 = load i64* %v, align 8, !dbg !963
  %27 = and i64 %26, -2, !dbg !963
  %28 = inttoptr i64 %27 to %struct._AVPair*, !dbg !963
  %29 = getelementptr inbounds %struct._AVPair* %28, i32 0, i32 7, !dbg !963
  %30 = load %struct._Thread** %29, align 8, !dbg !963
  %31 = ptrtoint %struct._Thread* %30 to i64, !dbg !963
  %32 = load %struct._Thread** %1, align 8, !dbg !963
  %33 = ptrtoint %struct._Thread* %32 to i64, !dbg !963
  %34 = xor i64 %31, %33, !dbg !963
  %35 = load i64* %dx, align 8, !dbg !963
  %36 = or i64 %35, %34, !dbg !963
  store i64 %36, i64* %dx, align 8, !dbg !963
  br label %45, !dbg !965

; <label>:37                                      ; preds = %17
  %38 = load i64* %v, align 8, !dbg !966
  %39 = load i64* %rv, align 8, !dbg !966
  %40 = icmp ugt i64 %38, %39, !dbg !966
  %41 = zext i1 %40 to i32, !dbg !966
  %42 = sext i32 %41 to i64, !dbg !966
  %43 = load i64* %dx, align 8, !dbg !966
  %44 = or i64 %43, %42, !dbg !966
  store i64 %44, i64* %dx, align 8, !dbg !966
  br label %45

; <label>:45                                      ; preds = %37, %25
  br label %46, !dbg !968

; <label>:46                                      ; preds = %45
  %47 = load %struct._AVPair** %e, align 8, !dbg !957
  %48 = getelementptr inbounds %struct._AVPair* %47, i32 0, i32 0, !dbg !957
  %49 = load %struct._AVPair** %48, align 8, !dbg !957
  store %struct._AVPair* %49, %struct._AVPair** %e, align 8, !dbg !957
  br label %13, !dbg !957

; <label>:50                                      ; preds = %13
  %51 = load i64* %dx, align 8, !dbg !969
  %52 = icmp eq i64 %51, 0, !dbg !969
  %53 = zext i1 %52 to i32, !dbg !969
  %54 = sext i32 %53 to i64, !dbg !969
  ret i64 %54, !dbg !969
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @WriteBackForward(%struct._Log* %k) #6 {
  %1 = alloca %struct._Log*, align 8
  %e = alloca %struct._AVPair*, align 8
  %End = alloca %struct._AVPair*, align 8
  store %struct._Log* %k, %struct._Log** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Log** %1}, metadata !970), !dbg !971
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !972), !dbg !973
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End}, metadata !974), !dbg !975
  %2 = load %struct._Log** %1, align 8, !dbg !975
  %3 = getelementptr inbounds %struct._Log* %2, i32 0, i32 1, !dbg !975
  %4 = load %struct._AVPair** %3, align 8, !dbg !975
  store %struct._AVPair* %4, %struct._AVPair** %End, align 8, !dbg !975
  %5 = load %struct._Log** %1, align 8, !dbg !976
  %6 = getelementptr inbounds %struct._Log* %5, i32 0, i32 0, !dbg !976
  %7 = load %struct._AVPair** %6, align 8, !dbg !976
  store %struct._AVPair* %7, %struct._AVPair** %e, align 8, !dbg !976
  br label %8, !dbg !976

; <label>:8                                       ; preds = %19, %0
  %9 = load %struct._AVPair** %e, align 8, !dbg !976
  %10 = load %struct._AVPair** %End, align 8, !dbg !976
  %11 = icmp ne %struct._AVPair* %9, %10, !dbg !976
  br i1 %11, label %12, label %23, !dbg !976

; <label>:12                                      ; preds = %8
  %13 = load %struct._AVPair** %e, align 8, !dbg !978
  %14 = getelementptr inbounds %struct._AVPair* %13, i32 0, i32 3, !dbg !978
  %15 = load i64* %14, align 8, !dbg !978
  %16 = load %struct._AVPair** %e, align 8, !dbg !978
  %17 = getelementptr inbounds %struct._AVPair* %16, i32 0, i32 2, !dbg !978
  %18 = load i64** %17, align 8, !dbg !978
  store volatile i64 %15, i64* %18, align 8, !dbg !978
  br label %19, !dbg !980

; <label>:19                                      ; preds = %12
  %20 = load %struct._AVPair** %e, align 8, !dbg !976
  %21 = getelementptr inbounds %struct._AVPair* %20, i32 0, i32 0, !dbg !976
  %22 = load %struct._AVPair** %21, align 8, !dbg !976
  store %struct._AVPair* %22, %struct._AVPair** %e, align 8, !dbg !976
  br label %8, !dbg !976

; <label>:23                                      ; preds = %8
  ret void, !dbg !981
}

; Function Attrs: inlinehint nounwind uwtable
define internal void @DropLocks(%struct._Thread* %Self, i64 %wv) #6 {
  %1 = alloca %struct._Thread*, align 8
  %2 = alloca i64, align 8
  %wr = alloca %struct._Log*, align 8
  %p = alloca %struct._AVPair*, align 8
  %End = alloca %struct._AVPair*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !982), !dbg !983
  store i64 %wv, i64* %2, align 8
  call void @llvm.dbg.declare(metadata !{i64* %2}, metadata !984), !dbg !983
  call void @llvm.dbg.declare(metadata !{%struct._Log** %wr}, metadata !985), !dbg !986
  %3 = load %struct._Thread** %1, align 8, !dbg !986
  %4 = getelementptr inbounds %struct._Thread* %3, i32 0, i32 17, !dbg !986
  store %struct._Log* %4, %struct._Log** %wr, align 8, !dbg !986
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %p}, metadata !987), !dbg !989
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %End}, metadata !990), !dbg !991
  %5 = load %struct._Log** %wr, align 8, !dbg !991
  %6 = getelementptr inbounds %struct._Log* %5, i32 0, i32 1, !dbg !991
  %7 = load %struct._AVPair** %6, align 8, !dbg !991
  store %struct._AVPair* %7, %struct._AVPair** %End, align 8, !dbg !991
  %8 = load %struct._Log** %wr, align 8, !dbg !992
  %9 = getelementptr inbounds %struct._Log* %8, i32 0, i32 0, !dbg !992
  %10 = load %struct._AVPair** %9, align 8, !dbg !992
  store %struct._AVPair* %10, %struct._AVPair** %p, align 8, !dbg !992
  br label %11, !dbg !992

; <label>:11                                      ; preds = %29, %0
  %12 = load %struct._AVPair** %p, align 8, !dbg !992
  %13 = load %struct._AVPair** %End, align 8, !dbg !992
  %14 = icmp ne %struct._AVPair* %12, %13, !dbg !992
  br i1 %14, label %15, label %33, !dbg !992

; <label>:15                                      ; preds = %11
  %16 = load %struct._AVPair** %p, align 8, !dbg !994
  %17 = getelementptr inbounds %struct._AVPair* %16, i32 0, i32 6, !dbg !994
  %18 = load i8* %17, align 1, !dbg !994
  %19 = zext i8 %18 to i32, !dbg !994
  %20 = icmp eq i32 %19, 0, !dbg !994
  br i1 %20, label %21, label %22, !dbg !994

; <label>:21                                      ; preds = %15
  br label %29, !dbg !996

; <label>:22                                      ; preds = %15
  %23 = load %struct._AVPair** %p, align 8, !dbg !998
  %24 = getelementptr inbounds %struct._AVPair* %23, i32 0, i32 6, !dbg !998
  store i8 0, i8* %24, align 1, !dbg !998
  %25 = load i64* %2, align 8, !dbg !999
  %26 = load %struct._AVPair** %p, align 8, !dbg !999
  %27 = getelementptr inbounds %struct._AVPair* %26, i32 0, i32 4, !dbg !999
  %28 = load i64** %27, align 8, !dbg !999
  store volatile i64 %25, i64* %28, align 8, !dbg !999
  br label %29, !dbg !1000

; <label>:29                                      ; preds = %22, %21
  %30 = load %struct._AVPair** %p, align 8, !dbg !992
  %31 = getelementptr inbounds %struct._AVPair* %30, i32 0, i32 0, !dbg !992
  %32 = load %struct._AVPair** %31, align 8, !dbg !992
  store %struct._AVPair* %32, %struct._AVPair** %p, align 8, !dbg !992
  br label %11, !dbg !992

; <label>:33                                      ; preds = %11
  %34 = load %struct._Thread** %1, align 8, !dbg !1001
  %35 = getelementptr inbounds %struct._Thread* %34, i32 0, i32 2, !dbg !1001
  store volatile i64 0, i64* %35, align 8, !dbg !1001
  ret void, !dbg !1002
}

; Function Attrs: inlinehint nounwind uwtable
define internal %struct._AVPair* @ExtendList(%struct._AVPair* %tail) #6 {
  %1 = alloca %struct._AVPair*, align 8
  %e = alloca %struct._AVPair*, align 8
  store %struct._AVPair* %tail, %struct._AVPair** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %1}, metadata !1003), !dbg !1004
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !1005), !dbg !1006
  %2 = call noalias i8* @malloc(i64 72) #7, !dbg !1006
  %3 = bitcast i8* %2 to %struct._AVPair*, !dbg !1006
  store %struct._AVPair* %3, %struct._AVPair** %e, align 8, !dbg !1006
  %4 = load %struct._AVPair** %e, align 8, !dbg !1007
  %5 = icmp ne %struct._AVPair* %4, null, !dbg !1007
  br i1 %5, label %6, label %7, !dbg !1007

; <label>:6                                       ; preds = %0
  br label %9, !dbg !1007

; <label>:7                                       ; preds = %0
  call void @__assert_fail(i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str4, i32 0, i32 0), i32 644, i8* getelementptr inbounds ([29 x i8]* @__PRETTY_FUNCTION__.ExtendList, i32 0, i32 0)) #8, !dbg !1007
  unreachable, !dbg !1007
                                                  ; No predecessors!
  br label %9, !dbg !1007

; <label>:9                                       ; preds = %8, %6
  %10 = load %struct._AVPair** %e, align 8, !dbg !1008
  %11 = bitcast %struct._AVPair* %10 to i8*, !dbg !1008
  call void @llvm.memset.p0i8.i64(i8* %11, i8 0, i64 72, i32 8, i1 false), !dbg !1008
  %12 = load %struct._AVPair** %e, align 8, !dbg !1009
  %13 = load %struct._AVPair** %1, align 8, !dbg !1009
  %14 = getelementptr inbounds %struct._AVPair* %13, i32 0, i32 0, !dbg !1009
  store %struct._AVPair* %12, %struct._AVPair** %14, align 8, !dbg !1009
  %15 = load %struct._AVPair** %1, align 8, !dbg !1010
  %16 = load %struct._AVPair** %e, align 8, !dbg !1010
  %17 = getelementptr inbounds %struct._AVPair* %16, i32 0, i32 1, !dbg !1010
  store %struct._AVPair* %15, %struct._AVPair** %17, align 8, !dbg !1010
  %18 = load %struct._AVPair** %e, align 8, !dbg !1011
  %19 = getelementptr inbounds %struct._AVPair* %18, i32 0, i32 0, !dbg !1011
  store %struct._AVPair* null, %struct._AVPair** %19, align 8, !dbg !1011
  %20 = load %struct._AVPair** %1, align 8, !dbg !1012
  %21 = getelementptr inbounds %struct._AVPair* %20, i32 0, i32 7, !dbg !1012
  %22 = load %struct._Thread** %21, align 8, !dbg !1012
  %23 = load %struct._AVPair** %e, align 8, !dbg !1012
  %24 = getelementptr inbounds %struct._AVPair* %23, i32 0, i32 7, !dbg !1012
  store %struct._Thread* %22, %struct._Thread** %24, align 8, !dbg !1012
  %25 = load %struct._AVPair** %1, align 8, !dbg !1013
  %26 = getelementptr inbounds %struct._AVPair* %25, i32 0, i32 8, !dbg !1013
  %27 = load i64* %26, align 8, !dbg !1013
  %28 = add nsw i64 %27, 1, !dbg !1013
  %29 = load %struct._AVPair** %e, align 8, !dbg !1013
  %30 = getelementptr inbounds %struct._AVPair* %29, i32 0, i32 8, !dbg !1013
  store i64 %28, i64* %30, align 8, !dbg !1013
  %31 = load %struct._AVPair** %e, align 8, !dbg !1014
  ret %struct._AVPair* %31, !dbg !1014
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @ReadSetCoherentPessimistic(%struct._Thread* %Self) #6 {
  %1 = alloca i64, align 8
  %2 = alloca %struct._Thread*, align 8
  %rv = alloca i64, align 8
  %rd = alloca %struct._Log*, align 8
  %EndOfList = alloca %struct._AVPair*, align 8
  %e = alloca %struct._AVPair*, align 8
  %v = alloca i64, align 8
  store %struct._Thread* %Self, %struct._Thread** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %2}, metadata !1015), !dbg !1016
  call void @llvm.dbg.declare(metadata !{i64* %rv}, metadata !1017), !dbg !1018
  %3 = load %struct._Thread** %2, align 8, !dbg !1018
  %4 = getelementptr inbounds %struct._Thread* %3, i32 0, i32 4, !dbg !1018
  %5 = load volatile i64* %4, align 8, !dbg !1018
  store i64 %5, i64* %rv, align 8, !dbg !1018
  call void @llvm.dbg.declare(metadata !{%struct._Log** %rd}, metadata !1019), !dbg !1020
  %6 = load %struct._Thread** %2, align 8, !dbg !1020
  %7 = getelementptr inbounds %struct._Thread* %6, i32 0, i32 16, !dbg !1020
  store %struct._Log* %7, %struct._Log** %rd, align 8, !dbg !1020
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %EndOfList}, metadata !1021), !dbg !1022
  %8 = load %struct._Log** %rd, align 8, !dbg !1022
  %9 = getelementptr inbounds %struct._Log* %8, i32 0, i32 1, !dbg !1022
  %10 = load %struct._AVPair** %9, align 8, !dbg !1022
  store %struct._AVPair* %10, %struct._AVPair** %EndOfList, align 8, !dbg !1022
  call void @llvm.dbg.declare(metadata !{%struct._AVPair** %e}, metadata !1023), !dbg !1024
  %11 = load %struct._Log** %rd, align 8, !dbg !1025
  %12 = getelementptr inbounds %struct._Log* %11, i32 0, i32 0, !dbg !1025
  %13 = load %struct._AVPair** %12, align 8, !dbg !1025
  store %struct._AVPair* %13, %struct._AVPair** %e, align 8, !dbg !1025
  br label %14, !dbg !1025

; <label>:14                                      ; preds = %45, %0
  %15 = load %struct._AVPair** %e, align 8, !dbg !1025
  %16 = load %struct._AVPair** %EndOfList, align 8, !dbg !1025
  %17 = icmp ne %struct._AVPair* %15, %16, !dbg !1025
  br i1 %17, label %18, label %49, !dbg !1025

; <label>:18                                      ; preds = %14
  call void @llvm.dbg.declare(metadata !{i64* %v}, metadata !1027), !dbg !1029
  %19 = load %struct._AVPair** %e, align 8, !dbg !1029
  %20 = getelementptr inbounds %struct._AVPair* %19, i32 0, i32 4, !dbg !1029
  %21 = load i64** %20, align 8, !dbg !1029
  %22 = load volatile i64* %21, align 8, !dbg !1029
  store i64 %22, i64* %v, align 8, !dbg !1029
  %23 = load i64* %v, align 8, !dbg !1030
  %24 = and i64 %23, 1, !dbg !1030
  %25 = icmp ne i64 %24, 0, !dbg !1030
  br i1 %25, label %26, label %38, !dbg !1030

; <label>:26                                      ; preds = %18
  %27 = load i64* %v, align 8, !dbg !1031
  %28 = xor i64 %27, 1, !dbg !1031
  %29 = inttoptr i64 %28 to %struct._AVPair*, !dbg !1031
  %30 = getelementptr inbounds %struct._AVPair* %29, i32 0, i32 7, !dbg !1031
  %31 = load %struct._Thread** %30, align 8, !dbg !1031
  %32 = ptrtoint %struct._Thread* %31 to i64, !dbg !1031
  %33 = load %struct._Thread** %2, align 8, !dbg !1031
  %34 = ptrtoint %struct._Thread* %33 to i64, !dbg !1031
  %35 = icmp ne i64 %32, %34, !dbg !1031
  br i1 %35, label %36, label %37, !dbg !1031

; <label>:36                                      ; preds = %26
  store i64 0, i64* %1, !dbg !1033
  br label %50, !dbg !1033

; <label>:37                                      ; preds = %26
  br label %44, !dbg !1035

; <label>:38                                      ; preds = %18
  %39 = load i64* %v, align 8, !dbg !1036
  %40 = load i64* %rv, align 8, !dbg !1036
  %41 = icmp ugt i64 %39, %40, !dbg !1036
  br i1 %41, label %42, label %43, !dbg !1036

; <label>:42                                      ; preds = %38
  store i64 0, i64* %1, !dbg !1038
  br label %50, !dbg !1038

; <label>:43                                      ; preds = %38
  br label %44

; <label>:44                                      ; preds = %43, %37
  br label %45, !dbg !1040

; <label>:45                                      ; preds = %44
  %46 = load %struct._AVPair** %e, align 8, !dbg !1025
  %47 = getelementptr inbounds %struct._AVPair* %46, i32 0, i32 0, !dbg !1025
  %48 = load %struct._AVPair** %47, align 8, !dbg !1025
  store %struct._AVPair* %48, %struct._AVPair** %e, align 8, !dbg !1025
  br label %14, !dbg !1025

; <label>:49                                      ; preds = %14
  store i64 1, i64* %1, !dbg !1041
  br label %50, !dbg !1041

; <label>:50                                      ; preds = %49, %42, %36
  %51 = load i64* %1, !dbg !1041
  ret i64 %51, !dbg !1041
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @TSRandom(%struct._Thread* %Self) #6 {
  %1 = alloca %struct._Thread*, align 8
  store %struct._Thread* %Self, %struct._Thread** %1, align 8
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %1}, metadata !1042), !dbg !1043
  %2 = load %struct._Thread** %1, align 8, !dbg !1044
  %3 = getelementptr inbounds %struct._Thread* %2, i32 0, i32 11, !dbg !1044
  %4 = call i64 @MarsagliaXOR(i64* %3), !dbg !1044
  ret i64 %4, !dbg !1044
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @MarsagliaXOR(i64* %seed) #6 {
  %1 = alloca i64*, align 8
  %x = alloca i64, align 8
  store i64* %seed, i64** %1, align 8
  call void @llvm.dbg.declare(metadata !{i64** %1}, metadata !1045), !dbg !1046
  call void @llvm.dbg.declare(metadata !{i64* %x}, metadata !1047), !dbg !1048
  %2 = load i64** %1, align 8, !dbg !1049
  %3 = load i64* %2, align 8, !dbg !1049
  %4 = call i64 @MarsagliaXORV(i64 %3), !dbg !1049
  store i64 %4, i64* %x, align 8, !dbg !1049
  %5 = load i64* %x, align 8, !dbg !1050
  %6 = load i64** %1, align 8, !dbg !1050
  store i64 %5, i64* %6, align 8, !dbg !1050
  %7 = load i64* %x, align 8, !dbg !1051
  ret i64 %7, !dbg !1051
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @MarsagliaXORV(i64 %x) #6 {
  %1 = alloca i64, align 8
  store i64 %x, i64* %1, align 8
  call void @llvm.dbg.declare(metadata !{i64* %1}, metadata !1052), !dbg !1053
  %2 = load i64* %1, align 8, !dbg !1054
  %3 = icmp eq i64 %2, 0, !dbg !1054
  br i1 %3, label %4, label %5, !dbg !1054

; <label>:4                                       ; preds = %0
  store i64 1, i64* %1, align 8, !dbg !1055
  br label %5, !dbg !1057

; <label>:5                                       ; preds = %4, %0
  %6 = load i64* %1, align 8, !dbg !1058
  %7 = shl i64 %6, 6, !dbg !1058
  %8 = load i64* %1, align 8, !dbg !1058
  %9 = xor i64 %8, %7, !dbg !1058
  store i64 %9, i64* %1, align 8, !dbg !1058
  %10 = load i64* %1, align 8, !dbg !1059
  %11 = lshr i64 %10, 21, !dbg !1059
  %12 = load i64* %1, align 8, !dbg !1059
  %13 = xor i64 %12, %11, !dbg !1059
  store i64 %13, i64* %1, align 8, !dbg !1059
  %14 = load i64* %1, align 8, !dbg !1060
  %15 = shl i64 %14, 7, !dbg !1060
  %16 = load i64* %1, align 8, !dbg !1060
  %17 = xor i64 %16, %15, !dbg !1060
  store i64 %17, i64* %1, align 8, !dbg !1060
  %18 = load i64* %1, align 8, !dbg !1061
  ret i64 %18, !dbg !1061
}

; Function Attrs: nounwind uwtable
define internal void @restoreUseAfterFreeHandler() #0 {
  %1 = call i32 @sigaction(i32 7, %struct.sigaction* @global_act_oldsigbus, %struct.sigaction* null) #7, !dbg !1062
  %2 = icmp ne i32 %1, 0, !dbg !1062
  br i1 %2, label %3, label %4, !dbg !1062

; <label>:3                                       ; preds = %0
  call void @perror(i8* getelementptr inbounds ([40 x i8]* @.str9, i32 0, i32 0)), !dbg !1063
  call void @exit(i32 1) #8, !dbg !1065
  unreachable, !dbg !1065

; <label>:4                                       ; preds = %0
  %5 = call i32 @sigaction(i32 11, %struct.sigaction* @global_act_oldsigsegv, %struct.sigaction* null) #7, !dbg !1066
  %6 = icmp ne i32 %5, 0, !dbg !1066
  br i1 %6, label %7, label %8, !dbg !1066

; <label>:7                                       ; preds = %4
  call void @perror(i8* getelementptr inbounds ([41 x i8]* @.str10, i32 0, i32 0)), !dbg !1067
  call void @exit(i32 1) #8, !dbg !1069
  unreachable, !dbg !1069

; <label>:8                                       ; preds = %4
  ret void, !dbg !1070
}

; Function Attrs: nounwind
declare i32 @sigaction(i32, %struct.sigaction*, %struct.sigaction*) #3

declare void @perror(i8*) #4

; Function Attrs: noreturn nounwind
declare void @exit(i32) #5

; Function Attrs: nounwind uwtable
define internal void @registerUseAfterFreeHandler() #0 {
  %act = alloca %struct.sigaction, align 8
  call void @llvm.dbg.declare(metadata !{%struct.sigaction* %act}, metadata !1071), !dbg !1072
  %1 = bitcast %struct.sigaction* %act to i8*, !dbg !1073
  call void @llvm.memset.p0i8.i64(i8* %1, i8 -104, i64 0, i32 8, i1 false), !dbg !1073
  %2 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 0, !dbg !1074
  %3 = bitcast %union.anon* %2 to void (i32, %struct.siginfo_t*, i8*)**, !dbg !1074
  store void (i32, %struct.siginfo_t*, i8*)* @useAfterFreeHandler, void (i32, %struct.siginfo_t*, i8*)** %3, align 8, !dbg !1074
  %4 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 1, !dbg !1075
  %5 = call i32 @sigemptyset(%struct.__sigset_t* %4) #7, !dbg !1075
  %6 = getelementptr inbounds %struct.sigaction* %act, i32 0, i32 2, !dbg !1076
  store i32 268435460, i32* %6, align 4, !dbg !1076
  %7 = call i32 @sigaction(i32 7, %struct.sigaction* %act, %struct.sigaction* @global_act_oldsigbus) #7, !dbg !1077
  %8 = icmp ne i32 %7, 0, !dbg !1077
  br i1 %8, label %9, label %10, !dbg !1077

; <label>:9                                       ; preds = %0
  call void @perror(i8* getelementptr inbounds ([41 x i8]* @.str11, i32 0, i32 0)), !dbg !1078
  call void @exit(i32 1) #8, !dbg !1080
  unreachable, !dbg !1080

; <label>:10                                      ; preds = %0
  %11 = call i32 @sigaction(i32 11, %struct.sigaction* %act, %struct.sigaction* @global_act_oldsigsegv) #7, !dbg !1081
  %12 = icmp ne i32 %11, 0, !dbg !1081
  br i1 %12, label %13, label %14, !dbg !1081

; <label>:13                                      ; preds = %10
  call void @perror(i8* getelementptr inbounds ([42 x i8]* @.str12, i32 0, i32 0)), !dbg !1082
  call void @exit(i32 1) #8, !dbg !1084
  unreachable, !dbg !1084

; <label>:14                                      ; preds = %10
  ret void, !dbg !1085
}

; Function Attrs: nounwind uwtable
define internal void @useAfterFreeHandler(i32 %signum, %struct.siginfo_t* %siginfo, i8* %context) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.siginfo_t*, align 8
  %3 = alloca i8*, align 8
  %Self = alloca %struct._Thread*, align 8
  store i32 %signum, i32* %1, align 4
  call void @llvm.dbg.declare(metadata !{i32* %1}, metadata !1086), !dbg !1087
  store %struct.siginfo_t* %siginfo, %struct.siginfo_t** %2, align 8
  call void @llvm.dbg.declare(metadata !{%struct.siginfo_t** %2}, metadata !1088), !dbg !1087
  store i8* %context, i8** %3, align 8
  call void @llvm.dbg.declare(metadata !{i8** %3}, metadata !1089), !dbg !1087
  call void @llvm.dbg.declare(metadata !{%struct._Thread** %Self}, metadata !1090), !dbg !1091
  %4 = load i32* @global_key_self, align 4, !dbg !1091
  %5 = call i8* @pthread_getspecific(i32 %4) #7, !dbg !1091
  %6 = bitcast i8* %5 to %struct._Thread*, !dbg !1091
  store %struct._Thread* %6, %struct._Thread** %Self, align 8, !dbg !1091
  %7 = load %struct._Thread** %Self, align 8, !dbg !1092
  %8 = icmp eq %struct._Thread* %7, null, !dbg !1092
  br i1 %8, label %14, label %9, !dbg !1092

; <label>:9                                       ; preds = %0
  %10 = load %struct._Thread** %Self, align 8, !dbg !1092
  %11 = getelementptr inbounds %struct._Thread* %10, i32 0, i32 1, !dbg !1092
  %12 = load volatile i64* %11, align 8, !dbg !1092
  %13 = icmp eq i64 %12, 0, !dbg !1092
  br i1 %13, label %14, label %19, !dbg !1092

; <label>:14                                      ; preds = %9, %0
  %15 = load i32* %1, align 4, !dbg !1093
  call void @psignal(i32 %15, i8* null), !dbg !1093
  %16 = load %struct.siginfo_t** %2, align 8, !dbg !1095
  %17 = getelementptr inbounds %struct.siginfo_t* %16, i32 0, i32 1, !dbg !1095
  %18 = load i32* %17, align 4, !dbg !1095
  call void @exit(i32 %18) #8, !dbg !1095
  unreachable, !dbg !1095

; <label>:19                                      ; preds = %9
  %20 = load %struct._Thread** %Self, align 8, !dbg !1096
  %21 = getelementptr inbounds %struct._Thread* %20, i32 0, i32 1, !dbg !1096
  %22 = load volatile i64* %21, align 8, !dbg !1096
  %23 = icmp eq i64 %22, 1, !dbg !1096
  br i1 %23, label %24, label %31, !dbg !1096

; <label>:24                                      ; preds = %19
  %25 = load %struct._Thread** %Self, align 8, !dbg !1097
  %26 = call i64 @ReadSetCoherentPessimistic(%struct._Thread* %25), !dbg !1097
  %27 = icmp ne i64 %26, 0, !dbg !1097
  br i1 %27, label %30, label %28, !dbg !1097

; <label>:28                                      ; preds = %24
  %29 = load %struct._Thread** %Self, align 8, !dbg !1099
  call void @TxAbort(%struct._Thread* %29), !dbg !1099
  br label %30, !dbg !1101

; <label>:30                                      ; preds = %28, %24
  br label %31, !dbg !1102

; <label>:31                                      ; preds = %30, %19
  %32 = load i32* %1, align 4, !dbg !1103
  call void @psignal(i32 %32, i8* null), !dbg !1103
  call void @abort() #8, !dbg !1104
  unreachable, !dbg !1104
                                                  ; No predecessors!
  ret void, !dbg !1105
}

; Function Attrs: nounwind
declare i32 @sigemptyset(%struct.__sigset_t*) #3

; Function Attrs: nounwind
declare i8* @pthread_getspecific(i32) #3

declare void @psignal(i32, i8*) #4

; Function Attrs: noreturn nounwind
declare void @abort() #5

; Function Attrs: inlinehint nounwind uwtable
define internal void @GVInit() #6 {
  store volatile i64 0, i64* getelementptr inbounds ([64 x i64]* @GClock, i32 0, i64 32), align 8, !dbg !1106
  ret void, !dbg !1107
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { noinline nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf"="true" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { noreturn nounwind }

!llvm.dbg.cu = !{!0}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.3-16ubuntu1 (branches/release_33) (based on LLVM 3.3)", i1 false, metadata !"", i32 0, metadata !2, metadata !19, metadata !20, metadata !309, metadata !19, metadata !""} ; [ DW_TAG_compile_unit ] [/home/vagrant/stamp-tl2-x86/tl2.c] [DW_LANG_C99]
!1 = metadata !{metadata !"tl2.c", metadata !"/home/vagrant/stamp-tl2-x86"}
!2 = metadata !{metadata !3, metadata !8, metadata !15}
!3 = metadata !{i32 786436, metadata !1, null, metadata !"tl2_config", i32 50, i64 32, i64 32, i32 0, i32 0, null, metadata !4, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [tl2_config] [line 50, size 32, align 32, offset 0] [from ]
!4 = metadata !{metadata !5, metadata !6, metadata !7}
!5 = metadata !{i32 786472, metadata !"TL2_INIT_WRSET_NUM_ENTRY", i64 1024} ; [ DW_TAG_enumerator ] [TL2_INIT_WRSET_NUM_ENTRY :: 1024]
!6 = metadata !{i32 786472, metadata !"TL2_INIT_RDSET_NUM_ENTRY", i64 8192} ; [ DW_TAG_enumerator ] [TL2_INIT_RDSET_NUM_ENTRY :: 8192]
!7 = metadata !{i32 786472, metadata !"TL2_INIT_LOCAL_NUM_ENTRY", i64 1024} ; [ DW_TAG_enumerator ] [TL2_INIT_LOCAL_NUM_ENTRY :: 1024]
!8 = metadata !{i32 786436, metadata !1, null, metadata !"", i32 73, i64 32, i64 32, i32 0, i32 0, null, metadata !9, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [line 73, size 32, align 32, offset 0] [from ]
!9 = metadata !{metadata !10, metadata !11, metadata !12, metadata !13, metadata !14}
!10 = metadata !{i32 786472, metadata !"TIDLE", i64 0} ; [ DW_TAG_enumerator ] [TIDLE :: 0]
!11 = metadata !{i32 786472, metadata !"TTXN", i64 1} ; [ DW_TAG_enumerator ] [TTXN :: 1]
!12 = metadata !{i32 786472, metadata !"TABORTING", i64 3} ; [ DW_TAG_enumerator ] [TABORTING :: 3]
!13 = metadata !{i32 786472, metadata !"TABORTED", i64 5} ; [ DW_TAG_enumerator ] [TABORTED :: 5]
!14 = metadata !{i32 786472, metadata !"TCOMMITTING", i64 7} ; [ DW_TAG_enumerator ] [TCOMMITTING :: 7]
!15 = metadata !{i32 786436, metadata !1, null, metadata !"", i32 81, i64 32, i64 32, i32 0, i32 0, null, metadata !16, i32 0, i32 0} ; [ DW_TAG_enumeration_type ] [line 81, size 32, align 32, offset 0] [from ]
!16 = metadata !{metadata !17, metadata !18}
!17 = metadata !{i32 786472, metadata !"LOCKBIT", i64 1} ; [ DW_TAG_enumerator ] [LOCKBIT :: 1]
!18 = metadata !{i32 786472, metadata !"NADA", i64 2} ; [ DW_TAG_enumerator ] [NADA :: 2]
!19 = metadata !{i32 0}
!20 = metadata !{metadata !21, metadata !34, metadata !125, metadata !128, metadata !129, metadata !134, metadata !137, metadata !140, metadata !141, metadata !144, metadata !147, metadata !148, metadata !151, metadata !154, metadata !158, metadata !161, metadata !164, metadata !165, metadata !168, metadata !171, metadata !174, metadata !177, metadata !178, metadata !181, metadata !186, metadata !189, metadata !194, metadata !197, metadata !198, metadata !201, metadata !202, metadata !205, metadata !208, metadata !211, metadata !214, metadata !215, metadata !216, metadata !219, metadata !223, metadata !226, metadata !227, metadata !228, metadata !229, metadata !232, metadata !235, metadata !236, metadata !237, metadata !308}
!21 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"pslock", metadata !"pslock", metadata !"", i32 574, metadata !23, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64* (i64*)* @pslock, null, null, metadata !19, i32 575} ; [ DW_TAG_subprogram ] [line 574] [def] [scope 575] [pslock]
!22 = metadata !{i32 786473, metadata !1}         ; [ DW_TAG_file_type ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!23 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !24, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!24 = metadata !{metadata !25, metadata !30}
!25 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !26} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!26 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !27} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from vwLock]
!27 = metadata !{i32 786454, metadata !1, null, metadata !"vwLock", i32 109, i64 0, i64 0, i64 0, i32 0, metadata !28} ; [ DW_TAG_typedef ] [vwLock] [line 109, size 0, align 0, offset 0] [from uintptr_t]
!28 = metadata !{i32 786454, metadata !1, null, metadata !"uintptr_t", i32 122, i64 0, i64 0, i64 0, i32 0, metadata !29} ; [ DW_TAG_typedef ] [uintptr_t] [line 122, size 0, align 0, offset 0] [from long unsigned int]
!29 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!30 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !31} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!31 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !32} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from intptr_t]
!32 = metadata !{i32 786454, metadata !1, null, metadata !"intptr_t", i32 119, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_typedef ] [intptr_t] [line 119, size 0, align 0, offset 0] [from long int]
!33 = metadata !{i32 786468, null, null, metadata !"long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!34 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"FreeList", metadata !"FreeList", metadata !"", i32 616, metadata !35, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Log*, i64)* @FreeList, null, null, metadata !19, i32 617} ; [ DW_TAG_subprogram ] [line 616] [def] [scope 617] [FreeList]
!35 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !36, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!36 = metadata !{null, metadata !37, metadata !33}
!37 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !38} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from Log]
!38 = metadata !{i32 786454, metadata !1, null, metadata !"Log", i32 136, i64 0, i64 0, i64 0, i32 0, metadata !39} ; [ DW_TAG_typedef ] [Log] [line 136, size 0, align 0, offset 0] [from _Log]
!39 = metadata !{i32 786451, metadata !1, null, metadata !"_Log", i32 127, i64 384, i64 64, i32 0, i32 0, null, metadata !40, i32 0, null, null} ; [ DW_TAG_structure_type ] [_Log] [line 127, size 384, align 64, offset 0] [from ]
!40 = metadata !{metadata !41, metadata !119, metadata !120, metadata !121, metadata !122, metadata !123}
!41 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"List", i32 128, i64 64, i64 64, i64 0, i32 0, metadata !42} ; [ DW_TAG_member ] [List] [line 128, size 64, align 64, offset 0] [from ]
!42 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !43} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from AVPair]
!43 = metadata !{i32 786454, metadata !1, null, metadata !"AVPair", i32 125, i64 0, i64 0, i64 0, i32 0, metadata !44} ; [ DW_TAG_typedef ] [AVPair] [line 125, size 0, align 0, offset 0] [from _AVPair]
!44 = metadata !{i32 786451, metadata !1, null, metadata !"_AVPair", i32 113, i64 576, i64 64, i32 0, i32 0, null, metadata !45, i32 0, null, null} ; [ DW_TAG_structure_type ] [_AVPair] [line 113, size 576, align 64, offset 0] [from ]
!45 = metadata !{metadata !46, metadata !48, metadata !49, metadata !50, metadata !51, metadata !52, metadata !53, metadata !56, metadata !118}
!46 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Next", i32 114, i64 64, i64 64, i64 0, i32 0, metadata !47} ; [ DW_TAG_member ] [Next] [line 114, size 64, align 64, offset 0] [from ]
!47 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !44} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from _AVPair]
!48 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Prev", i32 115, i64 64, i64 64, i64 64, i32 0, metadata !47} ; [ DW_TAG_member ] [Prev] [line 115, size 64, align 64, offset 64] [from ]
!49 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Addr", i32 116, i64 64, i64 64, i64 128, i32 0, metadata !30} ; [ DW_TAG_member ] [Addr] [line 116, size 64, align 64, offset 128] [from ]
!50 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Valu", i32 117, i64 64, i64 64, i64 192, i32 0, metadata !32} ; [ DW_TAG_member ] [Valu] [line 117, size 64, align 64, offset 192] [from intptr_t]
!51 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"LockFor", i32 118, i64 64, i64 64, i64 256, i32 0, metadata !25} ; [ DW_TAG_member ] [LockFor] [line 118, size 64, align 64, offset 256] [from ]
!52 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"rdv", i32 119, i64 64, i64 64, i64 320, i32 0, metadata !27} ; [ DW_TAG_member ] [rdv] [line 119, size 64, align 64, offset 320] [from vwLock]
!53 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Held", i32 121, i64 8, i64 8, i64 384, i32 0, metadata !54} ; [ DW_TAG_member ] [Held] [line 121, size 8, align 8, offset 384] [from byte]
!54 = metadata !{i32 786454, metadata !1, null, metadata !"byte", i32 110, i64 0, i64 0, i64 0, i32 0, metadata !55} ; [ DW_TAG_typedef ] [byte] [line 110, size 0, align 0, offset 0] [from unsigned char]
!55 = metadata !{i32 786468, null, null, metadata !"unsigned char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ] [unsigned char] [line 0, size 8, align 8, offset 0, enc DW_ATE_unsigned_char]
!56 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Owner", i32 123, i64 64, i64 64, i64 448, i32 0, metadata !57} ; [ DW_TAG_member ] [Owner] [line 123, size 64, align 64, offset 448] [from ]
!57 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !58} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from _Thread]
!58 = metadata !{i32 786451, metadata !1, null, metadata !"_Thread", i32 147, i64 2240, i64 64, i32 0, i32 0, null, metadata !59, i32 0, null, null} ; [ DW_TAG_structure_type ] [_Thread] [line 147, size 2240, align 64, offset 0] [from ]
!59 = metadata !{metadata !60, metadata !61, metadata !63, metadata !64, metadata !65, metadata !66, metadata !67, metadata !68, metadata !71, metadata !72, metadata !73, metadata !74, metadata !76, metadata !80, metadata !82, metadata !92, metadata !93, metadata !94, metadata !95, metadata !96}
!60 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"UniqID", i32 148, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [UniqID] [line 148, size 64, align 64, offset 0] [from long int]
!61 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Mode", i32 149, i64 64, i64 64, i64 64, i32 0, metadata !62} ; [ DW_TAG_member ] [Mode] [line 149, size 64, align 64, offset 64] [from ]
!62 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from long int]
!63 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"HoldsLocks", i32 150, i64 64, i64 64, i64 128, i32 0, metadata !62} ; [ DW_TAG_member ] [HoldsLocks] [line 150, size 64, align 64, offset 128] [from ]
!64 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Retries", i32 151, i64 64, i64 64, i64 192, i32 0, metadata !62} ; [ DW_TAG_member ] [Retries] [line 151, size 64, align 64, offset 192] [from ]
!65 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"rv", i32 152, i64 64, i64 64, i64 256, i32 0, metadata !26} ; [ DW_TAG_member ] [rv] [line 152, size 64, align 64, offset 256] [from ]
!66 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"wv", i32 153, i64 64, i64 64, i64 320, i32 0, metadata !27} ; [ DW_TAG_member ] [wv] [line 153, size 64, align 64, offset 320] [from vwLock]
!67 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"abv", i32 154, i64 64, i64 64, i64 384, i32 0, metadata !27} ; [ DW_TAG_member ] [abv] [line 154, size 64, align 64, offset 384] [from vwLock]
!68 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"ROFlag", i32 159, i64 64, i64 64, i64 448, i32 0, metadata !69} ; [ DW_TAG_member ] [ROFlag] [line 159, size 64, align 64, offset 448] [from ]
!69 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !70} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from int]
!70 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!71 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"IsRO", i32 160, i64 32, i64 32, i64 512, i32 0, metadata !70} ; [ DW_TAG_member ] [IsRO] [line 160, size 32, align 32, offset 512] [from int]
!72 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Starts", i32 161, i64 64, i64 64, i64 576, i32 0, metadata !33} ; [ DW_TAG_member ] [Starts] [line 161, size 64, align 64, offset 576] [from long int]
!73 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"Aborts", i32 162, i64 64, i64 64, i64 640, i32 0, metadata !33} ; [ DW_TAG_member ] [Aborts] [line 162, size 64, align 64, offset 640] [from long int]
!74 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"rng", i32 163, i64 64, i64 64, i64 704, i32 0, metadata !75} ; [ DW_TAG_member ] [rng] [line 163, size 64, align 64, offset 704] [from long long unsigned int]
!75 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!76 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"xorrng", i32 164, i64 64, i64 64, i64 768, i32 0, metadata !77} ; [ DW_TAG_member ] [xorrng] [line 164, size 64, align 64, offset 768] [from ]
!77 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 64, i64 64, i32 0, i32 0, metadata !75, metadata !78, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 64, align 64, offset 0] [from long long unsigned int]
!78 = metadata !{metadata !79}
!79 = metadata !{i32 786465, i64 0, i64 1}        ; [ DW_TAG_subrange_type ] [0, 0]
!80 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"memCache", i32 165, i64 64, i64 64, i64 832, i32 0, metadata !81} ; [ DW_TAG_member ] [memCache] [line 165, size 64, align 64, offset 832] [from ]
!81 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!82 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"allocPtr", i32 166, i64 64, i64 64, i64 896, i32 0, metadata !83} ; [ DW_TAG_member ] [allocPtr] [line 166, size 64, align 64, offset 896] [from ]
!83 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !84} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from tmalloc_t]
!84 = metadata !{i32 786454, metadata !1, null, metadata !"tmalloc_t", i32 31, i64 0, i64 0, i64 0, i32 0, metadata !85} ; [ DW_TAG_typedef ] [tmalloc_t] [line 31, size 0, align 0, offset 0] [from tmalloc]
!85 = metadata !{i32 786451, metadata !86, null, metadata !"tmalloc", i32 27, i64 192, i64 64, i32 0, i32 0, null, metadata !87, i32 0, null, null} ; [ DW_TAG_structure_type ] [tmalloc] [line 27, size 192, align 64, offset 0] [from ]
!86 = metadata !{metadata !"./tmalloc.h", metadata !"/home/vagrant/stamp-tl2-x86"}
!87 = metadata !{metadata !88, metadata !89, metadata !90}
!88 = metadata !{i32 786445, metadata !86, metadata !85, metadata !"size", i32 28, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [size] [line 28, size 64, align 64, offset 0] [from long int]
!89 = metadata !{i32 786445, metadata !86, metadata !85, metadata !"capacity", i32 29, i64 64, i64 64, i64 64, i32 0, metadata !33} ; [ DW_TAG_member ] [capacity] [line 29, size 64, align 64, offset 64] [from long int]
!90 = metadata !{i32 786445, metadata !86, metadata !85, metadata !"elements", i32 30, i64 64, i64 64, i64 128, i32 0, metadata !91} ; [ DW_TAG_member ] [elements] [line 30, size 64, align 64, offset 128] [from ]
!91 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!92 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"freePtr", i32 167, i64 64, i64 64, i64 960, i32 0, metadata !83} ; [ DW_TAG_member ] [freePtr] [line 167, size 64, align 64, offset 960] [from ]
!93 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"rdSet", i32 168, i64 384, i64 64, i64 1024, i32 0, metadata !38} ; [ DW_TAG_member ] [rdSet] [line 168, size 384, align 64, offset 1024] [from Log]
!94 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"wrSet", i32 172, i64 384, i64 64, i64 1408, i32 0, metadata !38} ; [ DW_TAG_member ] [wrSet] [line 172, size 384, align 64, offset 1408] [from Log]
!95 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"LocalUndo", i32 174, i64 384, i64 64, i64 1792, i32 0, metadata !38} ; [ DW_TAG_member ] [LocalUndo] [line 174, size 384, align 64, offset 1792] [from Log]
!96 = metadata !{i32 786445, metadata !1, metadata !58, metadata !"envPtr", i32 175, i64 64, i64 64, i64 2176, i32 0, metadata !97} ; [ DW_TAG_member ] [envPtr] [line 175, size 64, align 64, offset 2176] [from ]
!97 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !98} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from sigjmp_buf]
!98 = metadata !{i32 786454, metadata !1, null, metadata !"sigjmp_buf", i32 92, i64 0, i64 0, i64 0, i32 0, metadata !99} ; [ DW_TAG_typedef ] [sigjmp_buf] [line 92, size 0, align 0, offset 0] [from ]
!99 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1600, i64 64, i32 0, i32 0, metadata !100, metadata !78, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 1600, align 64, offset 0] [from __jmp_buf_tag]
!100 = metadata !{i32 786451, metadata !101, null, metadata !"__jmp_buf_tag", i32 34, i64 1600, i64 64, i32 0, i32 0, null, metadata !102, i32 0, null, null} ; [ DW_TAG_structure_type ] [__jmp_buf_tag] [line 34, size 1600, align 64, offset 0] [from ]
!101 = metadata !{metadata !"/usr/include/setjmp.h", metadata !"/home/vagrant/stamp-tl2-x86"}
!102 = metadata !{metadata !103, metadata !108, metadata !109}
!103 = metadata !{i32 786445, metadata !101, metadata !100, metadata !"__jmpbuf", i32 40, i64 512, i64 64, i64 0, i32 0, metadata !104} ; [ DW_TAG_member ] [__jmpbuf] [line 40, size 512, align 64, offset 0] [from __jmp_buf]
!104 = metadata !{i32 786454, metadata !101, null, metadata !"__jmp_buf", i32 31, i64 0, i64 0, i64 0, i32 0, metadata !105} ; [ DW_TAG_typedef ] [__jmp_buf] [line 31, size 0, align 0, offset 0] [from ]
!105 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 512, i64 64, i32 0, i32 0, metadata !33, metadata !106, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 512, align 64, offset 0] [from long int]
!106 = metadata !{metadata !107}
!107 = metadata !{i32 786465, i64 0, i64 8}       ; [ DW_TAG_subrange_type ] [0, 7]
!108 = metadata !{i32 786445, metadata !101, metadata !100, metadata !"__mask_was_saved", i32 41, i64 32, i64 32, i64 512, i32 0, metadata !70} ; [ DW_TAG_member ] [__mask_was_saved] [line 41, size 32, align 32, offset 512] [from int]
!109 = metadata !{i32 786445, metadata !101, metadata !100, metadata !"__saved_mask", i32 42, i64 1024, i64 64, i64 576, i32 0, metadata !110} ; [ DW_TAG_member ] [__saved_mask] [line 42, size 1024, align 64, offset 576] [from __sigset_t]
!110 = metadata !{i32 786454, metadata !101, null, metadata !"__sigset_t", i32 30, i64 0, i64 0, i64 0, i32 0, metadata !111} ; [ DW_TAG_typedef ] [__sigset_t] [line 30, size 0, align 0, offset 0] [from ]
!111 = metadata !{i32 786451, metadata !112, null, metadata !"", i32 27, i64 1024, i64 64, i32 0, i32 0, null, metadata !113, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 27, size 1024, align 64, offset 0] [from ]
!112 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/sigset.h", metadata !"/home/vagrant/stamp-tl2-x86"}
!113 = metadata !{metadata !114}
!114 = metadata !{i32 786445, metadata !112, metadata !111, metadata !"__val", i32 29, i64 1024, i64 64, i64 0, i32 0, metadata !115} ; [ DW_TAG_member ] [__val] [line 29, size 1024, align 64, offset 0] [from ]
!115 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1024, i64 64, i32 0, i32 0, metadata !29, metadata !116, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 1024, align 64, offset 0] [from long unsigned int]
!116 = metadata !{metadata !117}
!117 = metadata !{i32 786465, i64 0, i64 16}      ; [ DW_TAG_subrange_type ] [0, 15]
!118 = metadata !{i32 786445, metadata !1, metadata !44, metadata !"Ordinal", i32 124, i64 64, i64 64, i64 512, i32 0, metadata !33} ; [ DW_TAG_member ] [Ordinal] [line 124, size 64, align 64, offset 512] [from long int]
!119 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"put", i32 129, i64 64, i64 64, i64 64, i32 0, metadata !42} ; [ DW_TAG_member ] [put] [line 129, size 64, align 64, offset 64] [from ]
!120 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"tail", i32 130, i64 64, i64 64, i64 128, i32 0, metadata !42} ; [ DW_TAG_member ] [tail] [line 130, size 64, align 64, offset 128] [from ]
!121 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"end", i32 131, i64 64, i64 64, i64 192, i32 0, metadata !42} ; [ DW_TAG_member ] [end] [line 131, size 64, align 64, offset 192] [from ]
!122 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"ovf", i32 132, i64 64, i64 64, i64 256, i32 0, metadata !33} ; [ DW_TAG_member ] [ovf] [line 132, size 64, align 64, offset 256] [from long int]
!123 = metadata !{i32 786445, metadata !1, metadata !39, metadata !"BloomFilter", i32 134, i64 32, i64 32, i64 320, i32 0, metadata !124} ; [ DW_TAG_member ] [BloomFilter] [line 134, size 32, align 32, offset 320] [from BitMap]
!124 = metadata !{i32 786454, metadata !1, null, metadata !"BitMap", i32 108, i64 0, i64 0, i64 0, i32 0, metadata !70} ; [ DW_TAG_typedef ] [BitMap] [line 108, size 0, align 0, offset 0] [from int]
!125 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxOnce", metadata !"TxOnce", metadata !"", i32 1014, metadata !126, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @TxOnce, null, null, metadata !19, i32 1015} ; [ DW_TAG_subprogram ] [line 1014] [def] [scope 1015] [TxOnce]
!126 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !127, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!127 = metadata !{null}
!128 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxShutdown", metadata !"TxShutdown", metadata !"", i32 1032, metadata !126, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @TxShutdown, null, null, metadata !19, i32 1033} ; [ DW_TAG_subprogram ] [line 1032] [def] [scope 1033] [TxShutdown]
!129 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxNewThread", metadata !"TxNewThread", metadata !"", i32 1062, metadata !130, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, %struct._Thread* ()* @TxNewThread, null, null, metadata !19, i32 1063} ; [ DW_TAG_subprogram ] [line 1062] [def] [scope 1063] [TxNewThread]
!130 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !131, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!131 = metadata !{metadata !132}
!132 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !133} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from Thread]
!133 = metadata !{i32 786454, metadata !1, null, metadata !"Thread", i32 35, i64 0, i64 0, i64 0, i32 0, metadata !58} ; [ DW_TAG_typedef ] [Thread] [line 35, size 0, align 0, offset 0] [from _Thread]
!134 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxFreeThread", metadata !"TxFreeThread", metadata !"", i32 1082, metadata !135, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @TxFreeThread, null, null, metadata !19, i32 1083} ; [ DW_TAG_subprogram ] [line 1082] [def] [scope 1083] [TxFreeThread]
!135 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !136, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!136 = metadata !{null, metadata !132}
!137 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxInitThread", metadata !"TxInitThread", metadata !"", i32 1126, metadata !138, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64)* @TxInitThread, null, null, metadata !19, i32 1127} ; [ DW_TAG_subprogram ] [line 1126] [def] [scope 1127] [TxInitThread]
!138 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !139, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!139 = metadata !{null, metadata !132, metadata !33}
!140 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxAbort", metadata !"TxAbort", metadata !"", i32 1703, metadata !135, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @TxAbort, null, null, metadata !19, i32 1704} ; [ DW_TAG_subprogram ] [line 1703] [def] [scope 1704] [TxAbort]
!141 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStore", metadata !"TxStore", metadata !"", i32 1845, metadata !142, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64*, i64)* @TxStore, null, null, metadata !19, i32 1846} ; [ DW_TAG_subprogram ] [line 1845] [def] [scope 1846] [TxStore]
!142 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !143, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!143 = metadata !{null, metadata !132, metadata !30, metadata !32}
!144 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxLoad", metadata !"TxLoad", metadata !"", i32 2025, metadata !145, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*, i64*)* @TxLoad, null, null, metadata !19, i32 2026} ; [ DW_TAG_subprogram ] [line 2025] [def] [scope 2026] [TxLoad]
!145 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !146, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!146 = metadata !{metadata !32, metadata !132, metadata !30}
!147 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStoreLocal", metadata !"TxStoreLocal", metadata !"", i32 2147, metadata !142, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64*, i64)* @TxStoreLocal, null, null, metadata !19, i32 2148} ; [ DW_TAG_subprogram ] [line 2147] [def] [scope 2148] [TxStoreLocal]
!148 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxStart", metadata !"TxStart", metadata !"", i32 2163, metadata !149, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, [1 x %struct.__jmp_buf_tag]*, i32*)* @TxStart, null, null, metadata !19, i32 2164} ; [ DW_TAG_subprogram ] [line 2163] [def] [scope 2164] [TxStart]
!149 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !150, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!150 = metadata !{null, metadata !132, metadata !97, metadata !69}
!151 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxCommit", metadata !"TxCommit", metadata !"", i32 2195, metadata !152, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (%struct._Thread*)* @TxCommit, null, null, metadata !19, i32 2196} ; [ DW_TAG_subprogram ] [line 2195] [def] [scope 2196] [TxCommit]
!152 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !153, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!153 = metadata !{metadata !70, metadata !132}
!154 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxAlloc", metadata !"TxAlloc", metadata !"", i32 2265, metadata !155, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (%struct._Thread*, i64)* @TxAlloc, null, null, metadata !19, i32 2266} ; [ DW_TAG_subprogram ] [line 2265] [def] [scope 2266] [TxAlloc]
!155 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !156, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!156 = metadata !{metadata !81, metadata !132, metadata !157}
!157 = metadata !{i32 786454, metadata !1, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !29} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!158 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TxFree", metadata !"TxFree", metadata !"", i32 2283, metadata !159, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i8*)* @TxFree, null, null, metadata !19, i32 2284} ; [ DW_TAG_subprogram ] [line 2283] [def] [scope 2284] [TxFree]
!159 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !160, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!160 = metadata !{null, metadata !132, metadata !81}
!161 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"thd1", metadata !"thd1", metadata !"", i32 2320, metadata !162, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void (i32, i32, i32, i32, i32)* @thd1, null, null, metadata !19, i32 2320} ; [ DW_TAG_subprogram ] [line 2320] [def] [thd1]
!162 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !163, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!163 = metadata !{null, metadata !70, metadata !70, metadata !70, metadata !70, metadata !70}
!164 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"thd2", metadata !"thd2", metadata !"", i32 2330, metadata !162, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void (i32, i32, i32, i32, i32)* @thd2, null, null, metadata !19, i32 2330} ; [ DW_TAG_subprogram ] [line 2330] [def] [thd2]
!165 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"main", metadata !"main", metadata !"", i32 2347, metadata !166, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @main, null, null, metadata !19, i32 2347} ; [ DW_TAG_subprogram ] [line 2347] [def] [main]
!166 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !167, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!167 = metadata !{metadata !70}
!168 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TryFastUpdate", metadata !"TryFastUpdate", metadata !"", i32 1457, metadata !169, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*)* @TryFastUpdate, null, null, metadata !19, i32 1458} ; [ DW_TAG_subprogram ] [line 1457] [local] [def] [scope 1458] [TryFastUpdate]
!169 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !170, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!170 = metadata !{metadata !33, metadata !132}
!171 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"DropLocks", metadata !"DropLocks", metadata !"", i32 1381, metadata !172, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64)* @DropLocks, null, null, metadata !19, i32 1382} ; [ DW_TAG_subprogram ] [line 1381] [local] [def] [scope 1382] [DropLocks]
!172 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !173, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!173 = metadata !{null, metadata !132, metadata !27}
!174 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"WriteBackForward", metadata !"WriteBackForward", metadata !"", i32 755, metadata !175, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Log*)* @WriteBackForward, null, null, metadata !19, i32 756} ; [ DW_TAG_subprogram ] [line 755] [local] [def] [scope 756] [WriteBackForward]
!175 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !176, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!176 = metadata !{null, metadata !37}
!177 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ReadSetCoherent", metadata !"ReadSetCoherent", metadata !"", i32 1254, metadata !169, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*)* @ReadSetCoherent, null, null, metadata !19, i32 1255} ; [ DW_TAG_subprogram ] [line 1254] [local] [def] [scope 1255] [ReadSetCoherent]
!178 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVGenerateWV_GV4", metadata !"GVGenerateWV_GV4", metadata !"", i32 381, metadata !179, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*, i64)* @GVGenerateWV_GV4, null, null, metadata !19, i32 382} ; [ DW_TAG_subprogram ] [line 381] [local] [def] [scope 382] [GVGenerateWV_GV4]
!179 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !180, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!180 = metadata !{metadata !27, metadata !132, metadata !27}
!181 = metadata !{i32 786478, metadata !182, metadata !183, metadata !"cas", metadata !"cas", metadata !"", i32 42, metadata !184, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i64, i64, i64*)* @cas, null, null, metadata !19, i32 43} ; [ DW_TAG_subprogram ] [line 42] [local] [def] [scope 43] [cas]
!182 = metadata !{metadata !"./platform_x86.h", metadata !"/home/vagrant/stamp-tl2-x86"}
!183 = metadata !{i32 786473, metadata !182}      ; [ DW_TAG_file_type ] [/home/vagrant/stamp-tl2-x86/./platform_x86.h]
!184 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !185, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!185 = metadata !{metadata !32, metadata !32, metadata !32, metadata !30}
!186 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"FindFirst", metadata !"FindFirst", metadata !"", i32 788, metadata !187, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, %struct._AVPair* (%struct._Log*, i64*)* @FindFirst, null, null, metadata !19, i32 789} ; [ DW_TAG_subprogram ] [line 788] [local] [def] [scope 789] [FindFirst]
!187 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !188, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!188 = metadata !{metadata !42, metadata !37, metadata !25}
!189 = metadata !{i32 786478, metadata !182, metadata !183, metadata !"prefetchw", metadata !"prefetchw", metadata !"", i32 93, metadata !190, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i8*)* @prefetchw, null, null, metadata !19, i32 94} ; [ DW_TAG_subprogram ] [line 93] [local] [def] [scope 94] [prefetchw]
!190 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !191, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!191 = metadata !{null, metadata !192}
!192 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !193} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!193 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from ]
!194 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txSterilize", metadata !"txSterilize", metadata !"", i32 2120, metadata !195, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i8*, i64)* @txSterilize, null, null, metadata !19, i32 2121} ; [ DW_TAG_subprogram ] [line 2120] [local] [def] [scope 2121] [txSterilize]
!195 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !196, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!196 = metadata !{null, metadata !81, metadata !157}
!197 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txCommitReset", metadata !"txCommitReset", metadata !"", i32 1206, metadata !135, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @txCommitReset, null, null, metadata !19, i32 1207} ; [ DW_TAG_subprogram ] [line 1206] [local] [def] [scope 1207] [txCommitReset]
!198 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVRead", metadata !"GVRead", metadata !"", i32 313, metadata !199, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*)* @GVRead, null, null, metadata !19, i32 314} ; [ DW_TAG_subprogram ] [line 313] [local] [def] [scope 314] [GVRead]
!199 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !200, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!200 = metadata !{metadata !27, metadata !132}
!201 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"txReset", metadata !"txReset", metadata !"", i32 1166, metadata !135, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @txReset, null, null, metadata !19, i32 1167} ; [ DW_TAG_subprogram ] [line 1166] [local] [def] [scope 1167] [txReset]
!202 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"SaveForRollBack", metadata !"SaveForRollBack", metadata !"", i32 866, metadata !203, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Log*, i64*, i64)* @SaveForRollBack, null, null, metadata !19, i32 867} ; [ DW_TAG_subprogram ] [line 866] [local] [def] [scope 867] [SaveForRollBack]
!203 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !204, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!204 = metadata !{null, metadata !37, metadata !30, metadata !32}
!205 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ExtendList", metadata !"ExtendList", metadata !"", i32 641, metadata !206, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, %struct._AVPair* (%struct._AVPair*)* @ExtendList, null, null, metadata !19, i32 642} ; [ DW_TAG_subprogram ] [line 641] [local] [def] [scope 642] [ExtendList]
!206 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !207, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!207 = metadata !{metadata !42, metadata !42}
!208 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"RecordStore", metadata !"RecordStore", metadata !"", i32 832, metadata !209, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Log*, i64*, i64, i64*)* @RecordStore, null, null, metadata !19, i32 833} ; [ DW_TAG_subprogram ] [line 832] [local] [def] [scope 833] [RecordStore]
!209 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !210, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!210 = metadata !{null, metadata !37, metadata !30, metadata !32, metadata !25}
!211 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TrackLoad", metadata !"TrackLoad", metadata !"", i32 887, metadata !212, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (%struct._Thread*, i64*)* @TrackLoad, null, null, metadata !19, i32 888} ; [ DW_TAG_subprogram ] [line 887] [local] [def] [scope 888] [TrackLoad]
!212 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !213, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!213 = metadata !{metadata !70, metadata !132, metadata !25}
!214 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"ReadSetCoherentPessimistic", metadata !"ReadSetCoherentPessimistic", metadata !"", i32 1296, metadata !169, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*)* @ReadSetCoherentPessimistic, null, null, metadata !19, i32 1297} ; [ DW_TAG_subprogram ] [line 1296] [local] [def] [scope 1297] [ReadSetCoherentPessimistic]
!215 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"backoff", metadata !"backoff", metadata !"", i32 1427, metadata !138, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*, i64)* @backoff, null, null, metadata !19, i32 1428} ; [ DW_TAG_subprogram ] [line 1427] [local] [def] [scope 1428] [backoff]
!216 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"TSRandom", metadata !"TSRandom", metadata !"", i32 243, metadata !217, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*)* @TSRandom, null, null, metadata !19, i32 244} ; [ DW_TAG_subprogram ] [line 243] [local] [def] [scope 244] [TSRandom]
!217 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !218, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!218 = metadata !{metadata !75, metadata !132}
!219 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MarsagliaXOR", metadata !"MarsagliaXOR", metadata !"", i32 230, metadata !220, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i64*)* @MarsagliaXOR, null, null, metadata !19, i32 231} ; [ DW_TAG_subprogram ] [line 230] [local] [def] [scope 231] [MarsagliaXOR]
!220 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !221, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!221 = metadata !{metadata !75, metadata !222}
!222 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !75} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from long long unsigned int]
!223 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MarsagliaXORV", metadata !"MarsagliaXORV", metadata !"", i32 210, metadata !224, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i64)* @MarsagliaXORV, null, null, metadata !19, i32 211} ; [ DW_TAG_subprogram ] [line 210] [local] [def] [scope 211] [MarsagliaXORV]
!224 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !225, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!225 = metadata !{metadata !75, metadata !75}
!226 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVAbort", metadata !"GVAbort", metadata !"", i32 475, metadata !169, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct._Thread*)* @GVAbort, null, null, metadata !19, i32 476} ; [ DW_TAG_subprogram ] [line 475] [local] [def] [scope 476] [GVAbort]
!227 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"WriteBackReverse", metadata !"WriteBackReverse", metadata !"", i32 772, metadata !175, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Log*)* @WriteBackReverse, null, null, metadata !19, i32 773} ; [ DW_TAG_subprogram ] [line 772] [local] [def] [scope 773] [WriteBackReverse]
!228 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"RestoreLocks", metadata !"RestoreLocks", metadata !"", i32 1339, metadata !135, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (%struct._Thread*)* @RestoreLocks, null, null, metadata !19, i32 1340} ; [ DW_TAG_subprogram ] [line 1339] [local] [def] [scope 1340] [RestoreLocks]
!229 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"MakeList", metadata !"MakeList", metadata !"", i32 588, metadata !230, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, %struct._AVPair* (i64, %struct._Thread*)* @MakeList, null, null, metadata !19, i32 589} ; [ DW_TAG_subprogram ] [line 588] [local] [def] [scope 589] [MakeList]
!230 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !231, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!231 = metadata !{metadata !42, metadata !33, metadata !132}
!232 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"AtomicAdd", metadata !"AtomicAdd", metadata !"", i32 254, metadata !233, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i64*, i64)* @AtomicAdd, null, null, metadata !19, i32 255} ; [ DW_TAG_subprogram ] [line 254] [local] [def] [scope 255] [AtomicAdd]
!233 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !234, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!234 = metadata !{metadata !32, metadata !30, metadata !32}
!235 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"restoreUseAfterFreeHandler", metadata !"restoreUseAfterFreeHandler", metadata !"", i32 993, metadata !126, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @restoreUseAfterFreeHandler, null, null, metadata !19, i32 994} ; [ DW_TAG_subprogram ] [line 993] [local] [def] [scope 994] [restoreUseAfterFreeHandler]
!236 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"registerUseAfterFreeHandler", metadata !"registerUseAfterFreeHandler", metadata !"", i32 967, metadata !126, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @registerUseAfterFreeHandler, null, null, metadata !19, i32 968} ; [ DW_TAG_subprogram ] [line 967] [local] [def] [scope 968] [registerUseAfterFreeHandler]
!237 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"useAfterFreeHandler", metadata !"useAfterFreeHandler", metadata !"", i32 942, metadata !238, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, %struct.siginfo_t*, i8*)* @useAfterFreeHandler, null, null, metadata !19, i32 943} ; [ DW_TAG_subprogram ] [line 942] [local] [def] [scope 943] [useAfterFreeHandler]
!238 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !239, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!239 = metadata !{null, metadata !70, metadata !240, metadata !81}
!240 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !241} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from siginfo_t]
!241 = metadata !{i32 786454, metadata !242, null, metadata !"siginfo_t", i32 128, i64 0, i64 0, i64 0, i32 0, metadata !243} ; [ DW_TAG_typedef ] [siginfo_t] [line 128, size 0, align 0, offset 0] [from ]
!242 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/sigaction.h", metadata !"/home/vagrant/stamp-tl2-x86"}
!243 = metadata !{i32 786451, metadata !244, null, metadata !"", i32 62, i64 1024, i64 64, i32 0, i32 0, null, metadata !245, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 62, size 1024, align 64, offset 0] [from ]
!244 = metadata !{metadata !"/usr/include/x86_64-linux-gnu/bits/siginfo.h", metadata !"/home/vagrant/stamp-tl2-x86"}
!245 = metadata !{metadata !246, metadata !247, metadata !248, metadata !249}
!246 = metadata !{i32 786445, metadata !244, metadata !243, metadata !"si_signo", i32 64, i64 32, i64 32, i64 0, i32 0, metadata !70} ; [ DW_TAG_member ] [si_signo] [line 64, size 32, align 32, offset 0] [from int]
!247 = metadata !{i32 786445, metadata !244, metadata !243, metadata !"si_errno", i32 65, i64 32, i64 32, i64 32, i32 0, metadata !70} ; [ DW_TAG_member ] [si_errno] [line 65, size 32, align 32, offset 32] [from int]
!248 = metadata !{i32 786445, metadata !244, metadata !243, metadata !"si_code", i32 67, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [si_code] [line 67, size 32, align 32, offset 64] [from int]
!249 = metadata !{i32 786445, metadata !244, metadata !243, metadata !"_sifields", i32 127, i64 896, i64 64, i64 128, i32 0, metadata !250} ; [ DW_TAG_member ] [_sifields] [line 127, size 896, align 64, offset 128] [from ]
!250 = metadata !{i32 786455, metadata !244, metadata !243, metadata !"", i32 69, i64 896, i64 64, i64 0, i32 0, null, metadata !251, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [line 69, size 896, align 64, offset 0] [from ]
!251 = metadata !{metadata !252, metadata !256, metadata !264, metadata !275, metadata !281, metadata !291, metadata !297, metadata !302}
!252 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_pad", i32 71, i64 896, i64 32, i64 0, i32 0, metadata !253} ; [ DW_TAG_member ] [_pad] [line 71, size 896, align 32, offset 0] [from ]
!253 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 896, i64 32, i32 0, i32 0, metadata !70, metadata !254, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 896, align 32, offset 0] [from int]
!254 = metadata !{metadata !255}
!255 = metadata !{i32 786465, i64 0, i64 28}      ; [ DW_TAG_subrange_type ] [0, 27]
!256 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_kill", i32 78, i64 64, i64 32, i64 0, i32 0, metadata !257} ; [ DW_TAG_member ] [_kill] [line 78, size 64, align 32, offset 0] [from ]
!257 = metadata !{i32 786451, metadata !244, metadata !250, metadata !"", i32 74, i64 64, i64 32, i32 0, i32 0, null, metadata !258, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 74, size 64, align 32, offset 0] [from ]
!258 = metadata !{metadata !259, metadata !261}
!259 = metadata !{i32 786445, metadata !244, metadata !257, metadata !"si_pid", i32 76, i64 32, i64 32, i64 0, i32 0, metadata !260} ; [ DW_TAG_member ] [si_pid] [line 76, size 32, align 32, offset 0] [from __pid_t]
!260 = metadata !{i32 786454, metadata !244, null, metadata !"__pid_t", i32 133, i64 0, i64 0, i64 0, i32 0, metadata !70} ; [ DW_TAG_typedef ] [__pid_t] [line 133, size 0, align 0, offset 0] [from int]
!261 = metadata !{i32 786445, metadata !244, metadata !257, metadata !"si_uid", i32 77, i64 32, i64 32, i64 32, i32 0, metadata !262} ; [ DW_TAG_member ] [si_uid] [line 77, size 32, align 32, offset 32] [from __uid_t]
!262 = metadata !{i32 786454, metadata !244, null, metadata !"__uid_t", i32 125, i64 0, i64 0, i64 0, i32 0, metadata !263} ; [ DW_TAG_typedef ] [__uid_t] [line 125, size 0, align 0, offset 0] [from unsigned int]
!263 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!264 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_timer", i32 86, i64 128, i64 64, i64 0, i32 0, metadata !265} ; [ DW_TAG_member ] [_timer] [line 86, size 128, align 64, offset 0] [from ]
!265 = metadata !{i32 786451, metadata !244, metadata !250, metadata !"", i32 81, i64 128, i64 64, i32 0, i32 0, null, metadata !266, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 81, size 128, align 64, offset 0] [from ]
!266 = metadata !{metadata !267, metadata !268, metadata !269}
!267 = metadata !{i32 786445, metadata !244, metadata !265, metadata !"si_tid", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !70} ; [ DW_TAG_member ] [si_tid] [line 83, size 32, align 32, offset 0] [from int]
!268 = metadata !{i32 786445, metadata !244, metadata !265, metadata !"si_overrun", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !70} ; [ DW_TAG_member ] [si_overrun] [line 84, size 32, align 32, offset 32] [from int]
!269 = metadata !{i32 786445, metadata !244, metadata !265, metadata !"si_sigval", i32 85, i64 64, i64 64, i64 64, i32 0, metadata !270} ; [ DW_TAG_member ] [si_sigval] [line 85, size 64, align 64, offset 64] [from sigval_t]
!270 = metadata !{i32 786454, metadata !244, null, metadata !"sigval_t", i32 36, i64 0, i64 0, i64 0, i32 0, metadata !271} ; [ DW_TAG_typedef ] [sigval_t] [line 36, size 0, align 0, offset 0] [from sigval]
!271 = metadata !{i32 786455, metadata !244, null, metadata !"sigval", i32 32, i64 64, i64 64, i64 0, i32 0, null, metadata !272, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [sigval] [line 32, size 64, align 64, offset 0] [from ]
!272 = metadata !{metadata !273, metadata !274}
!273 = metadata !{i32 786445, metadata !244, metadata !271, metadata !"sival_int", i32 34, i64 32, i64 32, i64 0, i32 0, metadata !70} ; [ DW_TAG_member ] [sival_int] [line 34, size 32, align 32, offset 0] [from int]
!274 = metadata !{i32 786445, metadata !244, metadata !271, metadata !"sival_ptr", i32 35, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_member ] [sival_ptr] [line 35, size 64, align 64, offset 0] [from ]
!275 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_rt", i32 94, i64 128, i64 64, i64 0, i32 0, metadata !276} ; [ DW_TAG_member ] [_rt] [line 94, size 128, align 64, offset 0] [from ]
!276 = metadata !{i32 786451, metadata !244, metadata !250, metadata !"", i32 89, i64 128, i64 64, i32 0, i32 0, null, metadata !277, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 89, size 128, align 64, offset 0] [from ]
!277 = metadata !{metadata !278, metadata !279, metadata !280}
!278 = metadata !{i32 786445, metadata !244, metadata !276, metadata !"si_pid", i32 91, i64 32, i64 32, i64 0, i32 0, metadata !260} ; [ DW_TAG_member ] [si_pid] [line 91, size 32, align 32, offset 0] [from __pid_t]
!279 = metadata !{i32 786445, metadata !244, metadata !276, metadata !"si_uid", i32 92, i64 32, i64 32, i64 32, i32 0, metadata !262} ; [ DW_TAG_member ] [si_uid] [line 92, size 32, align 32, offset 32] [from __uid_t]
!280 = metadata !{i32 786445, metadata !244, metadata !276, metadata !"si_sigval", i32 93, i64 64, i64 64, i64 64, i32 0, metadata !270} ; [ DW_TAG_member ] [si_sigval] [line 93, size 64, align 64, offset 64] [from sigval_t]
!281 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_sigchld", i32 104, i64 256, i64 64, i64 0, i32 0, metadata !282} ; [ DW_TAG_member ] [_sigchld] [line 104, size 256, align 64, offset 0] [from ]
!282 = metadata !{i32 786451, metadata !244, metadata !250, metadata !"", i32 97, i64 256, i64 64, i32 0, i32 0, null, metadata !283, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 97, size 256, align 64, offset 0] [from ]
!283 = metadata !{metadata !284, metadata !285, metadata !286, metadata !287, metadata !290}
!284 = metadata !{i32 786445, metadata !244, metadata !282, metadata !"si_pid", i32 99, i64 32, i64 32, i64 0, i32 0, metadata !260} ; [ DW_TAG_member ] [si_pid] [line 99, size 32, align 32, offset 0] [from __pid_t]
!285 = metadata !{i32 786445, metadata !244, metadata !282, metadata !"si_uid", i32 100, i64 32, i64 32, i64 32, i32 0, metadata !262} ; [ DW_TAG_member ] [si_uid] [line 100, size 32, align 32, offset 32] [from __uid_t]
!286 = metadata !{i32 786445, metadata !244, metadata !282, metadata !"si_status", i32 101, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [si_status] [line 101, size 32, align 32, offset 64] [from int]
!287 = metadata !{i32 786445, metadata !244, metadata !282, metadata !"si_utime", i32 102, i64 64, i64 64, i64 128, i32 0, metadata !288} ; [ DW_TAG_member ] [si_utime] [line 102, size 64, align 64, offset 128] [from __sigchld_clock_t]
!288 = metadata !{i32 786454, metadata !244, null, metadata !"__sigchld_clock_t", i32 58, i64 0, i64 0, i64 0, i32 0, metadata !289} ; [ DW_TAG_typedef ] [__sigchld_clock_t] [line 58, size 0, align 0, offset 0] [from __clock_t]
!289 = metadata !{i32 786454, metadata !244, null, metadata !"__clock_t", i32 135, i64 0, i64 0, i64 0, i32 0, metadata !33} ; [ DW_TAG_typedef ] [__clock_t] [line 135, size 0, align 0, offset 0] [from long int]
!290 = metadata !{i32 786445, metadata !244, metadata !282, metadata !"si_stime", i32 103, i64 64, i64 64, i64 192, i32 0, metadata !288} ; [ DW_TAG_member ] [si_stime] [line 103, size 64, align 64, offset 192] [from __sigchld_clock_t]
!291 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_sigfault", i32 111, i64 128, i64 64, i64 0, i32 0, metadata !292} ; [ DW_TAG_member ] [_sigfault] [line 111, size 128, align 64, offset 0] [from ]
!292 = metadata !{i32 786451, metadata !244, metadata !250, metadata !"", i32 107, i64 128, i64 64, i32 0, i32 0, null, metadata !293, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 107, size 128, align 64, offset 0] [from ]
!293 = metadata !{metadata !294, metadata !295}
!294 = metadata !{i32 786445, metadata !244, metadata !292, metadata !"si_addr", i32 109, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_member ] [si_addr] [line 109, size 64, align 64, offset 0] [from ]
!295 = metadata !{i32 786445, metadata !244, metadata !292, metadata !"si_addr_lsb", i32 110, i64 16, i64 16, i64 64, i32 0, metadata !296} ; [ DW_TAG_member ] [si_addr_lsb] [line 110, size 16, align 16, offset 64] [from short]
!296 = metadata !{i32 786468, null, null, metadata !"short", i32 0, i64 16, i64 16, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [short] [line 0, size 16, align 16, offset 0, enc DW_ATE_signed]
!297 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_sigpoll", i32 118, i64 128, i64 64, i64 0, i32 0, metadata !298} ; [ DW_TAG_member ] [_sigpoll] [line 118, size 128, align 64, offset 0] [from ]
!298 = metadata !{i32 786451, metadata !244, metadata !250, metadata !"", i32 114, i64 128, i64 64, i32 0, i32 0, null, metadata !299, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 114, size 128, align 64, offset 0] [from ]
!299 = metadata !{metadata !300, metadata !301}
!300 = metadata !{i32 786445, metadata !244, metadata !298, metadata !"si_band", i32 116, i64 64, i64 64, i64 0, i32 0, metadata !33} ; [ DW_TAG_member ] [si_band] [line 116, size 64, align 64, offset 0] [from long int]
!301 = metadata !{i32 786445, metadata !244, metadata !298, metadata !"si_fd", i32 117, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [si_fd] [line 117, size 32, align 32, offset 64] [from int]
!302 = metadata !{i32 786445, metadata !244, metadata !250, metadata !"_sigsys", i32 126, i64 128, i64 64, i64 0, i32 0, metadata !303} ; [ DW_TAG_member ] [_sigsys] [line 126, size 128, align 64, offset 0] [from ]
!303 = metadata !{i32 786451, metadata !244, metadata !250, metadata !"", i32 121, i64 128, i64 64, i32 0, i32 0, null, metadata !304, i32 0, null, null} ; [ DW_TAG_structure_type ] [line 121, size 128, align 64, offset 0] [from ]
!304 = metadata !{metadata !305, metadata !306, metadata !307}
!305 = metadata !{i32 786445, metadata !244, metadata !303, metadata !"_call_addr", i32 123, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_member ] [_call_addr] [line 123, size 64, align 64, offset 0] [from ]
!306 = metadata !{i32 786445, metadata !244, metadata !303, metadata !"_syscall", i32 124, i64 32, i64 32, i64 64, i32 0, metadata !70} ; [ DW_TAG_member ] [_syscall] [line 124, size 32, align 32, offset 64] [from int]
!307 = metadata !{i32 786445, metadata !244, metadata !303, metadata !"_arch", i32 125, i64 32, i64 32, i64 96, i32 0, metadata !263} ; [ DW_TAG_member ] [_arch] [line 125, size 32, align 32, offset 96] [from unsigned int]
!308 = metadata !{i32 786478, metadata !1, metadata !22, metadata !"GVInit", metadata !"GVInit", metadata !"", i32 302, metadata !126, i1 true, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @GVInit, null, null, metadata !19, i32 303} ; [ DW_TAG_subprogram ] [line 302] [local] [def] [scope 303] [GVInit]
!309 = metadata !{metadata !310, metadata !311, metadata !312, metadata !313, metadata !314, metadata !315, metadata !317, metadata !321, metadata !325, metadata !342}
!310 = metadata !{i32 786484, i32 0, null, metadata !"StartTally", metadata !"StartTally", metadata !"", metadata !22, i32 498, metadata !62, i32 0, i32 1, i64* @StartTally, null} ; [ DW_TAG_variable ] [StartTally] [line 498] [def]
!311 = metadata !{i32 786484, i32 0, null, metadata !"AbortTally", metadata !"AbortTally", metadata !"", metadata !22, i32 499, metadata !62, i32 0, i32 1, i64* @AbortTally, null} ; [ DW_TAG_variable ] [AbortTally] [line 499] [def]
!312 = metadata !{i32 786484, i32 0, null, metadata !"ReadOverflowTally", metadata !"ReadOverflowTally", metadata !"", metadata !22, i32 500, metadata !62, i32 0, i32 1, i64* @ReadOverflowTally, null} ; [ DW_TAG_variable ] [ReadOverflowTally] [line 500] [def]
!313 = metadata !{i32 786484, i32 0, null, metadata !"WriteOverflowTally", metadata !"WriteOverflowTally", metadata !"", metadata !22, i32 501, metadata !62, i32 0, i32 1, i64* @WriteOverflowTally, null} ; [ DW_TAG_variable ] [WriteOverflowTally] [line 501] [def]
!314 = metadata !{i32 786484, i32 0, null, metadata !"LocalOverflowTally", metadata !"LocalOverflowTally", metadata !"", metadata !22, i32 502, metadata !62, i32 0, i32 1, i64* @LocalOverflowTally, null} ; [ DW_TAG_variable ] [LocalOverflowTally] [line 502] [def]
!315 = metadata !{i32 786484, i32 0, null, metadata !"global_key_self", metadata !"global_key_self", metadata !"", metadata !22, i32 189, metadata !316, i32 1, i32 1, i32* @global_key_self, null} ; [ DW_TAG_variable ] [global_key_self] [line 189] [local] [def]
!316 = metadata !{i32 786454, metadata !1, null, metadata !"pthread_key_t", i32 163, i64 0, i64 0, i64 0, i32 0, metadata !263} ; [ DW_TAG_typedef ] [pthread_key_t] [line 163, size 0, align 0, offset 0] [from unsigned int]
!317 = metadata !{i32 786484, i32 0, null, metadata !"LockTab", metadata !"LockTab", metadata !"", metadata !22, i32 285, metadata !318, i32 1, i32 1, [1048576 x i64]* @LockTab, null} ; [ DW_TAG_variable ] [LockTab] [line 285] [local] [def]
!318 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 67108864, i64 64, i32 0, i32 0, metadata !26, metadata !319, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 67108864, align 64, offset 0] [from ]
!319 = metadata !{metadata !320}
!320 = metadata !{i32 786465, i64 0, i64 1048576} ; [ DW_TAG_subrange_type ] [0, 1048575]
!321 = metadata !{i32 786484, i32 0, null, metadata !"GClock", metadata !"GClock", metadata !"", metadata !22, i32 293, metadata !322, i32 1, i32 1, [64 x i64]* @GClock, null} ; [ DW_TAG_variable ] [GClock] [line 293] [local] [def]
!322 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 4096, i64 64, i32 0, i32 0, metadata !26, metadata !323, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 4096, align 64, offset 0] [from ]
!323 = metadata !{metadata !324}
!324 = metadata !{i32 786465, i64 0, i64 64}      ; [ DW_TAG_subrange_type ] [0, 63]
!325 = metadata !{i32 786484, i32 0, null, metadata !"global_act_oldsigsegv", metadata !"global_act_oldsigsegv", metadata !"", metadata !22, i32 191, metadata !326, i32 1, i32 1, %struct.sigaction* @global_act_oldsigsegv, null} ; [ DW_TAG_variable ] [global_act_oldsigsegv] [line 191] [local] [def]
!326 = metadata !{i32 786451, metadata !242, null, metadata !"sigaction", i32 24, i64 1216, i64 64, i32 0, i32 0, null, metadata !327, i32 0, null, null} ; [ DW_TAG_structure_type ] [sigaction] [line 24, size 1216, align 64, offset 0] [from ]
!327 = metadata !{metadata !328, metadata !338, metadata !339, metadata !340}
!328 = metadata !{i32 786445, metadata !242, metadata !326, metadata !"__sigaction_handler", i32 35, i64 64, i64 64, i64 0, i32 0, metadata !329} ; [ DW_TAG_member ] [__sigaction_handler] [line 35, size 64, align 64, offset 0] [from ]
!329 = metadata !{i32 786455, metadata !242, metadata !326, metadata !"", i32 28, i64 64, i64 64, i64 0, i32 0, null, metadata !330, i32 0, i32 0, null} ; [ DW_TAG_union_type ] [line 28, size 64, align 64, offset 0] [from ]
!330 = metadata !{metadata !331, metadata !336}
!331 = metadata !{i32 786445, metadata !242, metadata !329, metadata !"sa_handler", i32 31, i64 64, i64 64, i64 0, i32 0, metadata !332} ; [ DW_TAG_member ] [sa_handler] [line 31, size 64, align 64, offset 0] [from __sighandler_t]
!332 = metadata !{i32 786454, metadata !242, null, metadata !"__sighandler_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !333} ; [ DW_TAG_typedef ] [__sighandler_t] [line 85, size 0, align 0, offset 0] [from ]
!333 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !334} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!334 = metadata !{i32 786453, i32 0, i32 0, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !335, i32 0, i32 0} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!335 = metadata !{null, metadata !70}
!336 = metadata !{i32 786445, metadata !242, metadata !329, metadata !"sa_sigaction", i32 33, i64 64, i64 64, i64 0, i32 0, metadata !337} ; [ DW_TAG_member ] [sa_sigaction] [line 33, size 64, align 64, offset 0] [from ]
!337 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !238} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!338 = metadata !{i32 786445, metadata !242, metadata !326, metadata !"sa_mask", i32 43, i64 1024, i64 64, i64 64, i32 0, metadata !110} ; [ DW_TAG_member ] [sa_mask] [line 43, size 1024, align 64, offset 64] [from __sigset_t]
!339 = metadata !{i32 786445, metadata !242, metadata !326, metadata !"sa_flags", i32 46, i64 32, i64 32, i64 1088, i32 0, metadata !70} ; [ DW_TAG_member ] [sa_flags] [line 46, size 32, align 32, offset 1088] [from int]
!340 = metadata !{i32 786445, metadata !242, metadata !326, metadata !"sa_restorer", i32 49, i64 64, i64 64, i64 1152, i32 0, metadata !341} ; [ DW_TAG_member ] [sa_restorer] [line 49, size 64, align 64, offset 1152] [from ]
!341 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !126} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!342 = metadata !{i32 786484, i32 0, null, metadata !"global_act_oldsigbus", metadata !"global_act_oldsigbus", metadata !"", metadata !22, i32 190, metadata !326, i32 1, i32 1, %struct.sigaction* @global_act_oldsigbus, null} ; [ DW_TAG_variable ] [global_act_oldsigbus] [line 190] [local] [def]
!343 = metadata !{i32 786689, metadata !21, metadata !"Addr", metadata !22, i32 16777790, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 574]
!344 = metadata !{i32 574, i32 0, metadata !21, null}
!345 = metadata !{i32 576, i32 0, metadata !21, null}
!346 = metadata !{i32 786689, metadata !34, metadata !"k", metadata !22, i32 16777832, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 616]
!347 = metadata !{i32 616, i32 0, metadata !34, null}
!348 = metadata !{i32 786689, metadata !34, metadata !"sz", metadata !22, i32 33555048, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [sz] [line 616]
!349 = metadata !{i32 786688, metadata !34, metadata !"e", metadata !22, i32 619, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 619]
!350 = metadata !{i32 619, i32 0, metadata !34, null}
!351 = metadata !{i32 620, i32 0, metadata !34, null}
!352 = metadata !{i32 621, i32 0, metadata !353, null}
!353 = metadata !{i32 786443, metadata !1, metadata !34, i32 620, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!354 = metadata !{i32 786688, metadata !355, metadata !"tmp", metadata !22, i32 622, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [tmp] [line 622]
!355 = metadata !{i32 786443, metadata !1, metadata !353, i32 621, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!356 = metadata !{i32 622, i32 0, metadata !355, null}
!357 = metadata !{i32 623, i32 0, metadata !355, null}
!358 = metadata !{i32 624, i32 0, metadata !355, null}
!359 = metadata !{i32 625, i32 0, metadata !355, null}
!360 = metadata !{i32 626, i32 0, metadata !353, null}
!361 = metadata !{i32 629, i32 0, metadata !34, null}
!362 = metadata !{i32 630, i32 0, metadata !34, null}
!363 = metadata !{i32 786688, metadata !364, metadata !"a", metadata !22, i32 1016, metadata !365, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 1016]
!364 = metadata !{i32 786443, metadata !1, metadata !125, i32 1016, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!365 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 32, i64 32, i32 0, i32 0, metadata !70, metadata !78, i32 0, i32 0} ; [ DW_TAG_array_type ] [line 0, size 32, align 32, offset 0] [from int]
!366 = metadata !{i32 1016, i32 0, metadata !364, null}
!367 = metadata !{i32 1018, i32 0, metadata !125, null}
!368 = metadata !{i32 1019, i32 0, metadata !125, null}
!369 = metadata !{i32 1021, i32 0, metadata !125, null}
!370 = metadata !{i32 1022, i32 0, metadata !125, null}
!371 = metadata !{i32 1024, i32 0, metadata !125, null}
!372 = metadata !{i32 1034, i32 0, metadata !128, null}
!373 = metadata !{i32 1049, i32 0, metadata !128, null}
!374 = metadata !{i32 1051, i32 0, metadata !128, null}
!375 = metadata !{i32 1053, i32 0, metadata !128, null}
!376 = metadata !{i32 -2146706324}
!377 = metadata !{i32 1054, i32 0, metadata !128, null}
!378 = metadata !{i32 786688, metadata !129, metadata !"t", metadata !22, i32 1066, metadata !132, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [t] [line 1066]
!379 = metadata !{i32 1066, i32 0, metadata !129, null}
!380 = metadata !{i32 1067, i32 0, metadata !129, null}
!381 = metadata !{i32 1071, i32 0, metadata !129, null}
!382 = metadata !{i32 786689, metadata !134, metadata !"t", metadata !22, i32 16778298, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [t] [line 1082]
!383 = metadata !{i32 1082, i32 0, metadata !134, null}
!384 = metadata !{i32 1084, i32 0, metadata !134, null}
!385 = metadata !{i32 786688, metadata !134, metadata !"wrSetOvf", metadata !22, i32 1086, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wrSetOvf] [line 1086]
!386 = metadata !{i32 1086, i32 0, metadata !134, null}
!387 = metadata !{i32 786688, metadata !134, metadata !"wr", metadata !22, i32 1087, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1087]
!388 = metadata !{i32 1087, i32 0, metadata !134, null}
!389 = metadata !{i32 1094, i32 0, metadata !134, null}
!390 = metadata !{i32 1097, i32 0, metadata !391, null}
!391 = metadata !{i32 786443, metadata !1, metadata !134, i32 1096, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!392 = metadata !{i32 1099, i32 0, metadata !134, null}
!393 = metadata !{i32 1101, i32 0, metadata !134, null}
!394 = metadata !{i32 1103, i32 0, metadata !134, null}
!395 = metadata !{i32 1104, i32 0, metadata !134, null}
!396 = metadata !{i32 1106, i32 0, metadata !134, null}
!397 = metadata !{i32 1107, i32 0, metadata !134, null}
!398 = metadata !{i32 1109, i32 0, metadata !134, null}
!399 = metadata !{i32 1113, i32 0, metadata !134, null}
!400 = metadata !{i32 1115, i32 0, metadata !134, null}
!401 = metadata !{i32 1117, i32 0, metadata !134, null}
!402 = metadata !{i32 1118, i32 0, metadata !134, null}
!403 = metadata !{i32 786689, metadata !232, metadata !"addr", metadata !22, i32 16777470, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [addr] [line 254]
!404 = metadata !{i32 254, i32 0, metadata !232, null}
!405 = metadata !{i32 786689, metadata !232, metadata !"dx", metadata !22, i32 33554686, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dx] [line 254]
!406 = metadata !{i32 786688, metadata !232, metadata !"v", metadata !22, i32 256, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 256]
!407 = metadata !{i32 256, i32 0, metadata !232, null}
!408 = metadata !{i32 257, i32 0, metadata !409, null}
!409 = metadata !{i32 786443, metadata !1, metadata !232, i32 257, i32 0, i32 78} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!410 = metadata !{i32 257, i32 21, metadata !409, null}
!411 = metadata !{i32 257, i32 0, metadata !412, null}
!412 = metadata !{i32 786443, metadata !1, metadata !409, i32 257, i32 0, i32 79} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!413 = metadata !{i32 258, i32 0, metadata !232, null}
!414 = metadata !{i32 786689, metadata !137, metadata !"t", metadata !22, i32 16778342, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [t] [line 1126]
!415 = metadata !{i32 1126, i32 0, metadata !137, null}
!416 = metadata !{i32 786689, metadata !137, metadata !"id", metadata !22, i32 33555558, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [id] [line 1126]
!417 = metadata !{i32 1129, i32 0, metadata !137, null}
!418 = metadata !{i32 1131, i32 0, metadata !137, null}
!419 = metadata !{i32 1133, i32 0, metadata !137, null}
!420 = metadata !{i32 1134, i32 0, metadata !137, null}
!421 = metadata !{i32 1135, i32 0, metadata !137, null}
!422 = metadata !{i32 1140, i32 21, metadata !137, null}
!423 = metadata !{i32 1141, i32 0, metadata !137, null}
!424 = metadata !{i32 1144, i32 21, metadata !137, null}
!425 = metadata !{i32 1145, i32 0, metadata !137, null}
!426 = metadata !{i32 1147, i32 25, metadata !137, null}
!427 = metadata !{i32 1148, i32 0, metadata !137, null}
!428 = metadata !{i32 1150, i32 0, metadata !137, null}
!429 = metadata !{i32 1151, i32 0, metadata !137, null}
!430 = metadata !{i32 1152, i32 0, metadata !137, null}
!431 = metadata !{i32 1153, i32 0, metadata !137, null}
!432 = metadata !{i32 1158, i32 0, metadata !137, null}
!433 = metadata !{i32 786689, metadata !229, metadata !"sz", metadata !22, i32 16777804, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [sz] [line 588]
!434 = metadata !{i32 588, i32 0, metadata !229, null}
!435 = metadata !{i32 786689, metadata !229, metadata !"Self", metadata !22, i32 33555020, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 588]
!436 = metadata !{i32 786688, metadata !229, metadata !"ap", metadata !22, i32 590, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ap] [line 590]
!437 = metadata !{i32 590, i32 0, metadata !229, null}
!438 = metadata !{i32 591, i32 0, metadata !229, null}
!439 = metadata !{i32 592, i32 0, metadata !229, null}
!440 = metadata !{i32 786688, metadata !229, metadata !"List", metadata !22, i32 593, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [List] [line 593]
!441 = metadata !{i32 593, i32 0, metadata !229, null}
!442 = metadata !{i32 786688, metadata !229, metadata !"Tail", metadata !22, i32 594, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Tail] [line 594]
!443 = metadata !{i32 594, i32 0, metadata !229, null}
!444 = metadata !{i32 786688, metadata !229, metadata !"i", metadata !22, i32 595, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 595]
!445 = metadata !{i32 595, i32 0, metadata !229, null}
!446 = metadata !{i32 596, i32 0, metadata !447, null}
!447 = metadata !{i32 786443, metadata !1, metadata !229, i32 596, i32 0, i32 76} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!448 = metadata !{i32 786688, metadata !449, metadata !"e", metadata !22, i32 597, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 597]
!449 = metadata !{i32 786443, metadata !1, metadata !447, i32 596, i32 0, i32 77} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!450 = metadata !{i32 597, i32 0, metadata !449, null}
!451 = metadata !{i32 598, i32 0, metadata !449, null}
!452 = metadata !{i32 599, i32 0, metadata !449, null}
!453 = metadata !{i32 600, i32 0, metadata !449, null}
!454 = metadata !{i32 601, i32 0, metadata !449, null}
!455 = metadata !{i32 602, i32 0, metadata !449, null}
!456 = metadata !{i32 603, i32 0, metadata !449, null}
!457 = metadata !{i32 604, i32 0, metadata !229, null}
!458 = metadata !{i32 606, i32 0, metadata !229, null}
!459 = metadata !{i32 786689, metadata !140, metadata !"Self", metadata !22, i32 16778919, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1703]
!460 = metadata !{i32 1703, i32 0, metadata !140, null}
!461 = metadata !{i32 1708, i32 0, metadata !140, null}
!462 = metadata !{i32 1714, i32 0, metadata !140, null}
!463 = metadata !{i32 1715, i32 0, metadata !464, null}
!464 = metadata !{i32 786443, metadata !1, metadata !140, i32 1714, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!465 = metadata !{i32 1716, i32 0, metadata !464, null}
!466 = metadata !{i32 1720, i32 0, metadata !140, null}
!467 = metadata !{i32 1721, i32 0, metadata !468, null}
!468 = metadata !{i32 786443, metadata !1, metadata !140, i32 1720, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!469 = metadata !{i32 1722, i32 0, metadata !468, null}
!470 = metadata !{i32 1724, i32 0, metadata !140, null}
!471 = metadata !{i32 1725, i32 0, metadata !140, null}
!472 = metadata !{i32 1727, i32 9, metadata !140, null}
!473 = metadata !{i32 1729, i32 0, metadata !474, null}
!474 = metadata !{i32 786443, metadata !1, metadata !140, i32 1727, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!475 = metadata !{i32 1749, i32 0, metadata !140, null}
!476 = metadata !{i32 1750, i32 0, metadata !477, null}
!477 = metadata !{i32 786443, metadata !1, metadata !140, i32 1749, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!478 = metadata !{i32 1751, i32 0, metadata !477, null}
!479 = metadata !{i32 1756, i32 0, metadata !140, null}
!480 = metadata !{i32 1757, i32 0, metadata !140, null}
!481 = metadata !{i32 1760, i32 0, metadata !140, null}
!482 = metadata !{i32 1762, i32 0, metadata !140, null}
!483 = metadata !{i32 786689, metadata !228, metadata !"Self", metadata !22, i32 16778555, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1339]
!484 = metadata !{i32 1339, i32 0, metadata !228, null}
!485 = metadata !{i32 786688, metadata !228, metadata !"wr", metadata !22, i32 1348, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1348]
!486 = metadata !{i32 1348, i32 0, metadata !228, null}
!487 = metadata !{i32 786688, metadata !488, metadata !"p", metadata !22, i32 1351, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1351]
!488 = metadata !{i32 786443, metadata !1, metadata !228, i32 1350, i32 0, i32 72} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!489 = metadata !{i32 1351, i32 0, metadata !488, null}
!490 = metadata !{i32 786688, metadata !488, metadata !"End", metadata !22, i32 1352, metadata !491, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1352]
!491 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !42} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!492 = metadata !{i32 1352, i32 0, metadata !488, null}
!493 = metadata !{i32 1353, i32 0, metadata !494, null}
!494 = metadata !{i32 786443, metadata !1, metadata !488, i32 1353, i32 0, i32 73} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!495 = metadata !{i32 1357, i32 0, metadata !496, null}
!496 = metadata !{i32 786443, metadata !1, metadata !494, i32 1353, i32 0, i32 74} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!497 = metadata !{i32 1358, i32 0, metadata !498, null}
!498 = metadata !{i32 786443, metadata !1, metadata !496, i32 1357, i32 0, i32 75} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!499 = metadata !{i32 1365, i32 0, metadata !496, null}
!500 = metadata !{i32 1367, i32 0, metadata !496, null}
!501 = metadata !{i32 1368, i32 0, metadata !496, null}
!502 = metadata !{i32 1371, i32 0, metadata !228, null}
!503 = metadata !{i32 1373, i32 0, metadata !228, null}
!504 = metadata !{i32 786689, metadata !227, metadata !"k", metadata !22, i32 16777988, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 772]
!505 = metadata !{i32 772, i32 0, metadata !227, null}
!506 = metadata !{i32 786688, metadata !227, metadata !"e", metadata !22, i32 774, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 774]
!507 = metadata !{i32 774, i32 0, metadata !227, null}
!508 = metadata !{i32 775, i32 0, metadata !509, null}
!509 = metadata !{i32 786443, metadata !1, metadata !227, i32 775, i32 0, i32 70} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!510 = metadata !{i32 776, i32 0, metadata !511, null}
!511 = metadata !{i32 786443, metadata !1, metadata !509, i32 775, i32 0, i32 71} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!512 = metadata !{i32 777, i32 0, metadata !511, null}
!513 = metadata !{i32 778, i32 0, metadata !227, null}
!514 = metadata !{i32 786689, metadata !226, metadata !"Self", metadata !22, i32 16777691, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 475]
!515 = metadata !{i32 475, i32 0, metadata !226, null}
!516 = metadata !{i32 489, i32 0, metadata !226, null}
!517 = metadata !{i32 786689, metadata !215, metadata !"Self", metadata !22, i32 16778643, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1427]
!518 = metadata !{i32 1427, i32 0, metadata !215, null}
!519 = metadata !{i32 786689, metadata !215, metadata !"attempt", metadata !22, i32 33555859, metadata !33, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [attempt] [line 1427]
!520 = metadata !{i32 786688, metadata !215, metadata !"stall", metadata !22, i32 1433, metadata !75, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [stall] [line 1433]
!521 = metadata !{i32 1433, i32 0, metadata !215, null}
!522 = metadata !{i32 1433, i32 32, metadata !215, null}
!523 = metadata !{i32 1434, i32 0, metadata !215, null}
!524 = metadata !{i32 1435, i32 0, metadata !215, null}
!525 = metadata !{i32 786688, metadata !215, metadata !"i", metadata !22, i32 1444, metadata !526, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 1444]
!526 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !75} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from long long unsigned int]
!527 = metadata !{i32 1444, i32 0, metadata !215, null}
!528 = metadata !{i32 1445, i32 0, metadata !215, null}
!529 = metadata !{i32 1447, i32 0, metadata !530, null}
!530 = metadata !{i32 786443, metadata !1, metadata !215, i32 1445, i32 0, i32 68} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!531 = metadata !{i32 1449, i32 0, metadata !215, null}
!532 = metadata !{i32 786689, metadata !141, metadata !"Self", metadata !22, i32 16779061, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1845]
!533 = metadata !{i32 1845, i32 0, metadata !141, null}
!534 = metadata !{i32 786689, metadata !141, metadata !"addr", metadata !22, i32 33556277, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [addr] [line 1845]
!535 = metadata !{i32 786689, metadata !141, metadata !"valu", metadata !22, i32 50333493, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [valu] [line 1845]
!536 = metadata !{i32 786688, metadata !141, metadata !"LockFor", metadata !22, i32 1849, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 1849]
!537 = metadata !{i32 1849, i32 0, metadata !141, null}
!538 = metadata !{i32 786688, metadata !141, metadata !"rdv", metadata !22, i32 1850, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rdv] [line 1850]
!539 = metadata !{i32 1850, i32 0, metadata !141, null}
!540 = metadata !{i32 1858, i32 0, metadata !141, null}
!541 = metadata !{i32 1859, i32 0, metadata !542, null}
!542 = metadata !{i32 786443, metadata !1, metadata !141, i32 1858, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!543 = metadata !{i32 1861, i32 0, metadata !542, null}
!544 = metadata !{i32 1862, i32 0, metadata !542, null}
!545 = metadata !{i32 1869, i32 0, metadata !141, null}
!546 = metadata !{i32 1885, i32 0, metadata !141, null}
!547 = metadata !{i32 786688, metadata !141, metadata !"wr", metadata !22, i32 1912, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1912]
!548 = metadata !{i32 1912, i32 0, metadata !141, null}
!549 = metadata !{i32 1922, i32 0, metadata !141, null}
!550 = metadata !{i32 786688, metadata !551, metadata !"e", metadata !22, i32 1923, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1923]
!551 = metadata !{i32 786443, metadata !1, metadata !141, i32 1922, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!552 = metadata !{i32 1923, i32 0, metadata !551, null}
!553 = metadata !{i32 1924, i32 0, metadata !554, null}
!554 = metadata !{i32 786443, metadata !1, metadata !551, i32 1924, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!555 = metadata !{i32 1926, i32 0, metadata !556, null}
!556 = metadata !{i32 786443, metadata !1, metadata !554, i32 1924, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!557 = metadata !{i32 1928, i32 0, metadata !558, null}
!558 = metadata !{i32 786443, metadata !1, metadata !556, i32 1926, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!559 = metadata !{i32 1930, i32 0, metadata !558, null}
!560 = metadata !{i32 1932, i32 0, metadata !556, null}
!561 = metadata !{i32 1934, i32 0, metadata !551, null}
!562 = metadata !{i32 1935, i32 18, metadata !563, null}
!563 = metadata !{i32 786443, metadata !1, metadata !551, i32 1934, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!564 = metadata !{i32 1937, i32 0, metadata !565, null}
!565 = metadata !{i32 786443, metadata !1, metadata !563, i32 1935, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!566 = metadata !{i32 1938, i32 0, metadata !565, null}
!567 = metadata !{i32 1940, i32 0, metadata !563, null}
!568 = metadata !{i32 1942, i32 0, metadata !551, null}
!569 = metadata !{i32 1947, i32 0, metadata !141, null}
!570 = metadata !{i32 1955, i32 0, metadata !141, null}
!571 = metadata !{i32 786689, metadata !211, metadata !"Self", metadata !22, i32 16778103, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 887]
!572 = metadata !{i32 887, i32 0, metadata !211, null}
!573 = metadata !{i32 786689, metadata !211, metadata !"LockFor", metadata !22, i32 33555319, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [LockFor] [line 887]
!574 = metadata !{i32 786688, metadata !211, metadata !"k", metadata !22, i32 889, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [k] [line 889]
!575 = metadata !{i32 889, i32 0, metadata !211, null}
!576 = metadata !{i32 786688, metadata !211, metadata !"e", metadata !22, i32 910, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 910]
!577 = metadata !{i32 910, i32 0, metadata !211, null}
!578 = metadata !{i32 911, i32 0, metadata !211, null}
!579 = metadata !{i32 912, i32 14, metadata !580, null}
!580 = metadata !{i32 786443, metadata !1, metadata !211, i32 911, i32 0, i32 60} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!581 = metadata !{i32 913, i32 0, metadata !582, null}
!582 = metadata !{i32 786443, metadata !1, metadata !580, i32 912, i32 0, i32 61} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!583 = metadata !{i32 915, i32 0, metadata !580, null}
!584 = metadata !{i32 916, i32 13, metadata !580, null}
!585 = metadata !{i32 917, i32 0, metadata !580, null}
!586 = metadata !{i32 918, i32 0, metadata !580, null}
!587 = metadata !{i32 920, i32 0, metadata !211, null}
!588 = metadata !{i32 921, i32 0, metadata !211, null}
!589 = metadata !{i32 922, i32 0, metadata !211, null}
!590 = metadata !{i32 925, i32 0, metadata !211, null}
!591 = metadata !{i32 786689, metadata !208, metadata !"k", metadata !22, i32 16778048, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 832]
!592 = metadata !{i32 832, i32 0, metadata !208, null}
!593 = metadata !{i32 786689, metadata !208, metadata !"Addr", metadata !22, i32 33555264, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 832]
!594 = metadata !{i32 786689, metadata !208, metadata !"Valu", metadata !22, i32 50332480, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 832]
!595 = metadata !{i32 786689, metadata !208, metadata !"Lock", metadata !22, i32 67109696, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Lock] [line 832]
!596 = metadata !{i32 786688, metadata !208, metadata !"e", metadata !22, i32 843, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 843]
!597 = metadata !{i32 843, i32 0, metadata !208, null}
!598 = metadata !{i32 844, i32 0, metadata !208, null}
!599 = metadata !{i32 845, i32 0, metadata !600, null}
!600 = metadata !{i32 786443, metadata !1, metadata !208, i32 844, i32 0, i32 59} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!601 = metadata !{i32 846, i32 13, metadata !600, null}
!602 = metadata !{i32 847, i32 0, metadata !600, null}
!603 = metadata !{i32 848, i32 0, metadata !600, null}
!604 = metadata !{i32 850, i32 0, metadata !208, null}
!605 = metadata !{i32 851, i32 0, metadata !208, null}
!606 = metadata !{i32 852, i32 0, metadata !208, null}
!607 = metadata !{i32 853, i32 0, metadata !208, null}
!608 = metadata !{i32 854, i32 0, metadata !208, null}
!609 = metadata !{i32 855, i32 0, metadata !208, null}
!610 = metadata !{i32 856, i32 0, metadata !208, null}
!611 = metadata !{i32 857, i32 0, metadata !208, null}
!612 = metadata !{i32 786689, metadata !144, metadata !"Self", metadata !22, i32 16779241, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2025]
!613 = metadata !{i32 2025, i32 0, metadata !144, null}
!614 = metadata !{i32 786689, metadata !144, metadata !"Addr", metadata !22, i32 33556457, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 2025]
!615 = metadata !{i32 786688, metadata !144, metadata !"Valu", metadata !22, i32 2029, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Valu] [line 2029]
!616 = metadata !{i32 2029, i32 0, metadata !144, null}
!617 = metadata !{i32 786688, metadata !144, metadata !"msk", metadata !22, i32 2046, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [msk] [line 2046]
!618 = metadata !{i32 2046, i32 0, metadata !144, null}
!619 = metadata !{i32 2047, i32 0, metadata !144, null}
!620 = metadata !{i32 786688, metadata !621, metadata !"wr", metadata !22, i32 2051, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 2051]
!621 = metadata !{i32 786443, metadata !1, metadata !144, i32 2047, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!622 = metadata !{i32 2051, i32 0, metadata !621, null}
!623 = metadata !{i32 786688, metadata !621, metadata !"e", metadata !22, i32 2053, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 2053]
!624 = metadata !{i32 2053, i32 0, metadata !621, null}
!625 = metadata !{i32 2054, i32 0, metadata !626, null}
!626 = metadata !{i32 786443, metadata !1, metadata !621, i32 2054, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!627 = metadata !{i32 2056, i32 0, metadata !628, null}
!628 = metadata !{i32 786443, metadata !1, metadata !626, i32 2054, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!629 = metadata !{i32 2058, i32 0, metadata !630, null}
!630 = metadata !{i32 786443, metadata !1, metadata !628, i32 2056, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!631 = metadata !{i32 2060, i32 0, metadata !628, null}
!632 = metadata !{i32 2061, i32 0, metadata !621, null}
!633 = metadata !{i32 786688, metadata !144, metadata !"LockFor", metadata !22, i32 2076, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 2076]
!634 = metadata !{i32 2076, i32 0, metadata !144, null}
!635 = metadata !{i32 786688, metadata !144, metadata !"rdv", metadata !22, i32 2077, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rdv] [line 2077]
!636 = metadata !{i32 2077, i32 0, metadata !144, null}
!637 = metadata !{i32 2079, i32 0, metadata !144, null}
!638 = metadata !{i32 2081, i32 0, metadata !144, null}
!639 = metadata !{i32 2082, i32 0, metadata !640, null}
!640 = metadata !{i32 786443, metadata !1, metadata !144, i32 2081, i32 0, i32 19} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!641 = metadata !{i32 2083, i32 18, metadata !642, null}
!642 = metadata !{i32 786443, metadata !1, metadata !640, i32 2082, i32 0, i32 20} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!643 = metadata !{i32 2085, i32 0, metadata !644, null}
!644 = metadata !{i32 786443, metadata !1, metadata !642, i32 2083, i32 0, i32 21} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!645 = metadata !{i32 2086, i32 0, metadata !644, null}
!646 = metadata !{i32 2087, i32 0, metadata !642, null}
!647 = metadata !{i32 2089, i32 0, metadata !640, null}
!648 = metadata !{i32 2101, i32 0, metadata !144, null}
!649 = metadata !{i32 2103, i32 0, metadata !144, null}
!650 = metadata !{i32 2106, i32 0, metadata !144, null}
!651 = metadata !{i32 2107, i32 0, metadata !144, null}
!652 = metadata !{i32 786689, metadata !147, metadata !"Self", metadata !22, i32 16779363, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2147]
!653 = metadata !{i32 2147, i32 0, metadata !147, null}
!654 = metadata !{i32 786689, metadata !147, metadata !"Addr", metadata !22, i32 33556579, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 2147]
!655 = metadata !{i32 786689, metadata !147, metadata !"Valu", metadata !22, i32 50333795, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 2147]
!656 = metadata !{i32 2151, i32 0, metadata !147, null}
!657 = metadata !{i32 2152, i32 0, metadata !147, null}
!658 = metadata !{i32 2155, i32 0, metadata !147, null}
!659 = metadata !{i32 786689, metadata !202, metadata !"k", metadata !22, i32 16778082, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 866]
!660 = metadata !{i32 866, i32 0, metadata !202, null}
!661 = metadata !{i32 786689, metadata !202, metadata !"Addr", metadata !22, i32 33555298, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Addr] [line 866]
!662 = metadata !{i32 786689, metadata !202, metadata !"Valu", metadata !22, i32 50332514, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Valu] [line 866]
!663 = metadata !{i32 786688, metadata !202, metadata !"e", metadata !22, i32 868, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 868]
!664 = metadata !{i32 868, i32 0, metadata !202, null}
!665 = metadata !{i32 869, i32 0, metadata !202, null}
!666 = metadata !{i32 870, i32 0, metadata !667, null}
!667 = metadata !{i32 786443, metadata !1, metadata !202, i32 869, i32 0, i32 58} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!668 = metadata !{i32 871, i32 13, metadata !667, null}
!669 = metadata !{i32 872, i32 0, metadata !667, null}
!670 = metadata !{i32 873, i32 0, metadata !667, null}
!671 = metadata !{i32 874, i32 0, metadata !202, null}
!672 = metadata !{i32 875, i32 0, metadata !202, null}
!673 = metadata !{i32 876, i32 0, metadata !202, null}
!674 = metadata !{i32 877, i32 0, metadata !202, null}
!675 = metadata !{i32 878, i32 0, metadata !202, null}
!676 = metadata !{i32 879, i32 0, metadata !202, null}
!677 = metadata !{i32 786689, metadata !148, metadata !"Self", metadata !22, i32 16779379, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2163]
!678 = metadata !{i32 2163, i32 0, metadata !148, null}
!679 = metadata !{i32 786689, metadata !148, metadata !"envPtr", metadata !22, i32 33556595, metadata !97, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [envPtr] [line 2163]
!680 = metadata !{i32 786689, metadata !148, metadata !"ROFlag", metadata !22, i32 50333811, metadata !69, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ROFlag] [line 2163]
!681 = metadata !{i32 2168, i32 0, metadata !148, null}
!682 = metadata !{i32 2170, i32 16, metadata !148, null}
!683 = metadata !{i32 2174, i32 0, metadata !148, null}
!684 = metadata !{i32 2175, i32 0, metadata !148, null}
!685 = metadata !{i32 2176, i32 0, metadata !148, null}
!686 = metadata !{i32 2177, i32 0, metadata !148, null}
!687 = metadata !{i32 2182, i32 0, metadata !148, null}
!688 = metadata !{i32 2187, i32 0, metadata !148, null}
!689 = metadata !{i32 786689, metadata !201, metadata !"Self", metadata !22, i32 16778382, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1166]
!690 = metadata !{i32 1166, i32 0, metadata !201, null}
!691 = metadata !{i32 1168, i32 0, metadata !201, null}
!692 = metadata !{i32 1183, i32 0, metadata !201, null}
!693 = metadata !{i32 1184, i32 0, metadata !201, null}
!694 = metadata !{i32 1187, i32 0, metadata !201, null}
!695 = metadata !{i32 1188, i32 0, metadata !201, null}
!696 = metadata !{i32 1189, i32 0, metadata !201, null}
!697 = metadata !{i32 1191, i32 0, metadata !201, null}
!698 = metadata !{i32 1192, i32 0, metadata !201, null}
!699 = metadata !{i32 1193, i32 0, metadata !201, null}
!700 = metadata !{i32 1198, i32 0, metadata !201, null}
!701 = metadata !{i32 786689, metadata !198, metadata !"Self", metadata !22, i32 16777529, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 313]
!702 = metadata !{i32 313, i32 0, metadata !198, null}
!703 = metadata !{i32 316, i32 0, metadata !198, null}
!704 = metadata !{i32 786689, metadata !151, metadata !"Self", metadata !22, i32 16779411, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2195]
!705 = metadata !{i32 2195, i32 0, metadata !151, null}
!706 = metadata !{i32 2205, i32 0, metadata !151, null}
!707 = metadata !{i32 2209, i32 0, metadata !708, null}
!708 = metadata !{i32 786443, metadata !1, metadata !151, i32 2207, i32 0, i32 22} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!709 = metadata !{i32 2210, i32 0, metadata !708, null}
!710 = metadata !{i32 2211, i32 0, metadata !708, null}
!711 = metadata !{i32 2223, i32 0, metadata !708, null}
!712 = metadata !{i32 2226, i32 9, metadata !151, null}
!713 = metadata !{i32 2227, i32 0, metadata !714, null}
!714 = metadata !{i32 786443, metadata !1, metadata !151, i32 2226, i32 0, i32 23} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!715 = metadata !{i32 2228, i32 0, metadata !714, null}
!716 = metadata !{i32 2229, i32 0, metadata !714, null}
!717 = metadata !{i32 2243, i32 0, metadata !714, null}
!718 = metadata !{i32 2247, i32 0, metadata !151, null}
!719 = metadata !{i32 2250, i32 0, metadata !151, null}
!720 = metadata !{i32 786689, metadata !197, metadata !"Self", metadata !22, i32 16778422, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1206]
!721 = metadata !{i32 1206, i32 0, metadata !197, null}
!722 = metadata !{i32 1208, i32 0, metadata !197, null}
!723 = metadata !{i32 1209, i32 0, metadata !197, null}
!724 = metadata !{i32 1210, i32 0, metadata !197, null}
!725 = metadata !{i32 786689, metadata !194, metadata !"Base", metadata !22, i32 16779336, metadata !81, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Base] [line 2120]
!726 = metadata !{i32 2120, i32 0, metadata !194, null}
!727 = metadata !{i32 786689, metadata !194, metadata !"Length", metadata !22, i32 33556552, metadata !157, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Length] [line 2120]
!728 = metadata !{i32 786688, metadata !729, metadata !"Addr", metadata !22, i32 2124, metadata !730, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Addr] [line 2124]
!729 = metadata !{i32 786443, metadata !1, metadata !194} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!730 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !32} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from intptr_t]
!731 = metadata !{i32 2124, i32 0, metadata !729, null}
!732 = metadata !{i32 786688, metadata !729, metadata !"End", metadata !22, i32 2125, metadata !730, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 2125]
!733 = metadata !{i32 2125, i32 0, metadata !729, null}
!734 = metadata !{i32 2127, i32 0, metadata !729, null}
!735 = metadata !{i32 786688, metadata !736, metadata !"Lock", metadata !22, i32 2128, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Lock] [line 2128]
!736 = metadata !{i32 786443, metadata !1, metadata !729, i32 2127, i32 0, i32 57} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!737 = metadata !{i32 2128, i32 0, metadata !736, null}
!738 = metadata !{i32 786688, metadata !736, metadata !"val", metadata !22, i32 2129, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [val] [line 2129]
!739 = metadata !{i32 2129, i32 0, metadata !736, null}
!740 = metadata !{i32 2131, i32 0, metadata !736, null}
!741 = metadata !{i32 2132, i32 0, metadata !736, null}
!742 = metadata !{i32 2133, i32 0, metadata !736, null}
!743 = metadata !{i32 2134, i32 0, metadata !729, null}
!744 = metadata !{i32 2137, i32 0, metadata !729, null}
!745 = metadata !{i32 786689, metadata !168, metadata !"Self", metadata !22, i32 16778673, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1457]
!746 = metadata !{i32 1457, i32 0, metadata !168, null}
!747 = metadata !{i32 786688, metadata !168, metadata !"wr", metadata !22, i32 1470, metadata !748, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1470]
!748 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !37} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!749 = metadata !{i32 1470, i32 0, metadata !168, null}
!750 = metadata !{i32 786688, metadata !168, metadata !"rd", metadata !22, i32 1472, metadata !748, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1472]
!751 = metadata !{i32 1472, i32 0, metadata !168, null}
!752 = metadata !{i32 786688, metadata !168, metadata !"ctr", metadata !22, i32 1473, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ctr] [line 1473]
!753 = metadata !{i32 1473, i32 0, metadata !168, null}
!754 = metadata !{i32 786688, metadata !168, metadata !"wv", metadata !22, i32 1475, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wv] [line 1475]
!755 = metadata !{i32 1475, i32 0, metadata !168, null}
!756 = metadata !{i32 1517, i32 0, metadata !168, null}
!757 = metadata !{i32 1518, i32 0, metadata !168, null}
!758 = metadata !{i32 786688, metadata !168, metadata !"maxv", metadata !22, i32 1519, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [maxv] [line 1519]
!759 = metadata !{i32 1519, i32 0, metadata !168, null}
!760 = metadata !{i32 786688, metadata !168, metadata !"p", metadata !22, i32 1520, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1520]
!761 = metadata !{i32 1520, i32 0, metadata !168, null}
!762 = metadata !{i32 786688, metadata !763, metadata !"End", metadata !22, i32 1525, metadata !491, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1525]
!763 = metadata !{i32 786443, metadata !1, metadata !168, i32 1524, i32 0, i32 26} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!764 = metadata !{i32 1525, i32 0, metadata !763, null}
!765 = metadata !{i32 1526, i32 0, metadata !766, null}
!766 = metadata !{i32 786443, metadata !1, metadata !763, i32 1526, i32 0, i32 27} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!767 = metadata !{i32 786688, metadata !768, metadata !"LockFor", metadata !22, i32 1527, metadata !769, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 1527]
!768 = metadata !{i32 786443, metadata !1, metadata !766, i32 1526, i32 0, i32 28} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!769 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !25} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!770 = metadata !{i32 1527, i32 0, metadata !768, null}
!771 = metadata !{i32 786688, metadata !768, metadata !"cv", metadata !22, i32 1528, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [cv] [line 1528]
!772 = metadata !{i32 1528, i32 0, metadata !768, null}
!773 = metadata !{i32 1534, i32 0, metadata !768, null}
!774 = metadata !{i32 1535, i32 0, metadata !768, null}
!775 = metadata !{i32 1536, i32 0, metadata !768, null}
!776 = metadata !{i32 1538, i32 21, metadata !777, null}
!777 = metadata !{i32 786443, metadata !1, metadata !768, i32 1536, i32 0, i32 29} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!778 = metadata !{i32 1539, i32 0, metadata !779, null}
!779 = metadata !{i32 786443, metadata !1, metadata !777, i32 1538, i32 0, i32 30} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!780 = metadata !{i32 1540, i32 0, metadata !781, null}
!781 = metadata !{i32 786443, metadata !1, metadata !779, i32 1539, i32 0, i32 31} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!782 = metadata !{i32 1541, i32 0, metadata !781, null}
!783 = metadata !{i32 1543, i32 0, metadata !779, null}
!784 = metadata !{i32 1545, i32 0, metadata !777, null}
!785 = metadata !{i32 1549, i32 17, metadata !768, null}
!786 = metadata !{i32 1553, i32 0, metadata !787, null}
!787 = metadata !{i32 786443, metadata !1, metadata !768, i32 1549, i32 0, i32 32} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!788 = metadata !{i32 1555, i32 21, metadata !787, null}
!789 = metadata !{i32 1557, i32 0, metadata !790, null}
!790 = metadata !{i32 786443, metadata !1, metadata !787, i32 1556, i32 0, i32 33} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!791 = metadata !{i32 1558, i32 0, metadata !792, null}
!792 = metadata !{i32 786443, metadata !1, metadata !790, i32 1557, i32 0, i32 34} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!793 = metadata !{i32 1559, i32 0, metadata !792, null}
!794 = metadata !{i32 1560, i32 0, metadata !790, null}
!795 = metadata !{i32 1561, i32 0, metadata !790, null}
!796 = metadata !{i32 1562, i32 0, metadata !790, null}
!797 = metadata !{i32 1571, i32 0, metadata !787, null}
!798 = metadata !{i32 1572, i32 0, metadata !787, null}
!799 = metadata !{i32 786688, metadata !800, metadata !"c", metadata !22, i32 1590, metadata !33, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [c] [line 1590]
!800 = metadata !{i32 786443, metadata !1, metadata !768, i32 1574, i32 0, i32 35} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!801 = metadata !{i32 1590, i32 0, metadata !800, null}
!802 = metadata !{i32 1592, i32 0, metadata !803, null}
!803 = metadata !{i32 786443, metadata !1, metadata !800, i32 1592, i32 0, i32 36} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!804 = metadata !{i32 1593, i32 0, metadata !805, null}
!805 = metadata !{i32 786443, metadata !1, metadata !803, i32 1592, i32 0, i32 37} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!806 = metadata !{i32 1595, i32 0, metadata !805, null}
!807 = metadata !{i32 1596, i32 25, metadata !805, null}
!808 = metadata !{i32 1598, i32 0, metadata !809, null}
!809 = metadata !{i32 786443, metadata !1, metadata !805, i32 1597, i32 0, i32 38} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!810 = metadata !{i32 1599, i32 0, metadata !811, null}
!811 = metadata !{i32 786443, metadata !1, metadata !809, i32 1598, i32 0, i32 39} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!812 = metadata !{i32 1600, i32 0, metadata !811, null}
!813 = metadata !{i32 1601, i32 0, metadata !809, null}
!814 = metadata !{i32 1602, i32 0, metadata !809, null}
!815 = metadata !{i32 1603, i32 0, metadata !809, null}
!816 = metadata !{i32 1605, i32 0, metadata !805, null}
!817 = metadata !{i32 1607, i32 0, metadata !818, null}
!818 = metadata !{i32 786443, metadata !1, metadata !805, i32 1605, i32 0, i32 40} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!819 = metadata !{i32 1614, i32 0, metadata !805, null}
!820 = metadata !{i32 1616, i32 0, metadata !768, null}
!821 = metadata !{i32 1625, i32 10, metadata !168, null}
!822 = metadata !{i32 1646, i32 10, metadata !168, null}
!823 = metadata !{i32 1653, i32 0, metadata !824, null}
!824 = metadata !{i32 786443, metadata !1, metadata !168, i32 1646, i32 0, i32 41} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!825 = metadata !{i32 1665, i32 0, metadata !826, null}
!826 = metadata !{i32 786443, metadata !1, metadata !168, i32 1664, i32 0, i32 42} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!827 = metadata !{i32 1669, i32 0, metadata !168, null}
!828 = metadata !{i32 1677, i32 0, metadata !168, null}
!829 = metadata !{i32 -2146704539}
!830 = metadata !{i32 1678, i32 0, metadata !168, null}
!831 = metadata !{i32 786689, metadata !154, metadata !"Self", metadata !22, i32 16779481, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2265]
!832 = metadata !{i32 2265, i32 0, metadata !154, null}
!833 = metadata !{i32 786689, metadata !154, metadata !"size", metadata !22, i32 33556697, metadata !157, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [size] [line 2265]
!834 = metadata !{i32 786688, metadata !154, metadata !"ptr", metadata !22, i32 2267, metadata !81, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [ptr] [line 2267]
!835 = metadata !{i32 2267, i32 0, metadata !154, null}
!836 = metadata !{i32 2268, i32 0, metadata !154, null}
!837 = metadata !{i32 2269, i32 0, metadata !838, null}
!838 = metadata !{i32 786443, metadata !1, metadata !154, i32 2268, i32 0, i32 24} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!839 = metadata !{i32 2270, i32 0, metadata !838, null}
!840 = metadata !{i32 2272, i32 0, metadata !154, null}
!841 = metadata !{i32 786689, metadata !158, metadata !"Self", metadata !22, i32 16779499, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 2283]
!842 = metadata !{i32 2283, i32 0, metadata !158, null}
!843 = metadata !{i32 786689, metadata !158, metadata !"ptr", metadata !22, i32 33556715, metadata !81, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ptr] [line 2283]
!844 = metadata !{i32 2285, i32 0, metadata !158, null}
!845 = metadata !{i32 786688, metadata !158, metadata !"LockFor", metadata !22, i32 2297, metadata !25, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [LockFor] [line 2297]
!846 = metadata !{i32 2297, i32 0, metadata !158, null}
!847 = metadata !{i32 786688, metadata !158, metadata !"wr", metadata !22, i32 2304, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 2304]
!848 = metadata !{i32 2304, i32 0, metadata !158, null}
!849 = metadata !{i32 2306, i32 0, metadata !158, null}
!850 = metadata !{i32 2308, i32 0, metadata !158, null}
!851 = metadata !{i32 786689, metadata !161, metadata !"address", metadata !22, i32 16779536, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [address] [line 2320]
!852 = metadata !{i32 2320, i32 0, metadata !161, null}
!853 = metadata !{i32 786689, metadata !161, metadata !"env_ptr", metadata !22, i32 33556752, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [env_ptr] [line 2320]
!854 = metadata !{i32 786689, metadata !161, metadata !"roflag", metadata !22, i32 50333968, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [roflag] [line 2320]
!855 = metadata !{i32 786689, metadata !161, metadata !"alloc_size", metadata !22, i32 67111184, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [alloc_size] [line 2320]
!856 = metadata !{i32 786689, metadata !161, metadata !"alloc_ptr", metadata !22, i32 83888400, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [alloc_ptr] [line 2320]
!857 = metadata !{i32 786688, metadata !161, metadata !"thd", metadata !22, i32 2321, metadata !132, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [thd] [line 2321]
!858 = metadata !{i32 2321, i32 0, metadata !161, null}
!859 = metadata !{i32 2322, i32 0, metadata !161, null}
!860 = metadata !{i32 2323, i32 0, metadata !161, null}
!861 = metadata !{i32 2324, i32 0, metadata !161, null}
!862 = metadata !{i32 2325, i32 0, metadata !161, null}
!863 = metadata !{i32 2326, i32 0, metadata !161, null}
!864 = metadata !{i32 2327, i32 0, metadata !161, null}
!865 = metadata !{i32 2328, i32 0, metadata !161, null}
!866 = metadata !{i32 786689, metadata !164, metadata !"address", metadata !22, i32 16779546, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [address] [line 2330]
!867 = metadata !{i32 2330, i32 0, metadata !164, null}
!868 = metadata !{i32 786689, metadata !164, metadata !"env_ptr", metadata !22, i32 33556762, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [env_ptr] [line 2330]
!869 = metadata !{i32 786689, metadata !164, metadata !"roflag", metadata !22, i32 50333978, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [roflag] [line 2330]
!870 = metadata !{i32 786689, metadata !164, metadata !"alloc_size", metadata !22, i32 67111194, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [alloc_size] [line 2330]
!871 = metadata !{i32 786689, metadata !164, metadata !"alloc_ptr", metadata !22, i32 83888410, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [alloc_ptr] [line 2330]
!872 = metadata !{i32 786688, metadata !164, metadata !"thd", metadata !22, i32 2331, metadata !132, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [thd] [line 2331]
!873 = metadata !{i32 2331, i32 0, metadata !164, null}
!874 = metadata !{i32 2332, i32 0, metadata !164, null}
!875 = metadata !{i32 2335, i32 0, metadata !876, null}
!876 = metadata !{i32 786443, metadata !1, metadata !164, i32 2334, i32 0, i32 25} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!877 = metadata !{i32 2339, i32 0, metadata !164, null}
!878 = metadata !{i32 2340, i32 0, metadata !164, null}
!879 = metadata !{i32 2341, i32 0, metadata !164, null}
!880 = metadata !{i32 2342, i32 0, metadata !164, null}
!881 = metadata !{i32 2343, i32 0, metadata !164, null}
!882 = metadata !{i32 2344, i32 0, metadata !164, null}
!883 = metadata !{i32 786688, metadata !165, metadata !"thd", metadata !22, i32 2348, metadata !132, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [thd] [line 2348]
!884 = metadata !{i32 2348, i32 0, metadata !165, null}
!885 = metadata !{i32 786688, metadata !165, metadata !"address", metadata !22, i32 2349, metadata !730, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [address] [line 2349]
!886 = metadata !{i32 2349, i32 0, metadata !165, null}
!887 = metadata !{i32 786688, metadata !165, metadata !"env_ptr", metadata !22, i32 2350, metadata !97, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [env_ptr] [line 2350]
!888 = metadata !{i32 2350, i32 0, metadata !165, null}
!889 = metadata !{i32 786688, metadata !165, metadata !"roflag", metadata !22, i32 2351, metadata !69, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [roflag] [line 2351]
!890 = metadata !{i32 2351, i32 0, metadata !165, null}
!891 = metadata !{i32 786688, metadata !165, metadata !"alloc_size", metadata !22, i32 2352, metadata !157, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [alloc_size] [line 2352]
!892 = metadata !{i32 2352, i32 0, metadata !165, null}
!893 = metadata !{i32 786688, metadata !165, metadata !"alloc_ptr", metadata !22, i32 2353, metadata !81, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [alloc_ptr] [line 2353]
!894 = metadata !{i32 2353, i32 0, metadata !165, null}
!895 = metadata !{i32 2355, i32 0, metadata !165, null}
!896 = metadata !{i32 2356, i32 0, metadata !165, null}
!897 = metadata !{i32 2357, i32 0, metadata !165, null}
!898 = metadata !{i32 786689, metadata !189, metadata !"x", metadata !183, i32 16777309, metadata !192, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [x] [line 93]
!899 = metadata !{i32 93, i32 0, metadata !189, null}
!900 = metadata !{i32 96, i32 0, metadata !901, null}
!901 = metadata !{i32 786443, metadata !182, metadata !189} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/./platform_x86.h]
!902 = metadata !{i32 786689, metadata !186, metadata !"k", metadata !22, i32 16778004, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 788]
!903 = metadata !{i32 788, i32 0, metadata !186, null}
!904 = metadata !{i32 786689, metadata !186, metadata !"Lock", metadata !22, i32 33555220, metadata !25, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Lock] [line 788]
!905 = metadata !{i32 786688, metadata !906, metadata !"e", metadata !22, i32 790, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 790]
!906 = metadata !{i32 786443, metadata !1, metadata !186} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!907 = metadata !{i32 790, i32 0, metadata !906, null}
!908 = metadata !{i32 786688, metadata !906, metadata !"End", metadata !22, i32 791, metadata !491, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 791]
!909 = metadata !{i32 791, i32 0, metadata !906, null}
!910 = metadata !{i32 792, i32 0, metadata !911, null}
!911 = metadata !{i32 786443, metadata !1, metadata !906, i32 792, i32 0, i32 54} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!912 = metadata !{i32 793, i32 0, metadata !913, null}
!913 = metadata !{i32 786443, metadata !1, metadata !911, i32 792, i32 0, i32 55} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!914 = metadata !{i32 794, i32 0, metadata !915, null}
!915 = metadata !{i32 786443, metadata !1, metadata !913, i32 793, i32 0, i32 56} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!916 = metadata !{i32 796, i32 0, metadata !913, null}
!917 = metadata !{i32 797, i32 0, metadata !906, null}
!918 = metadata !{i32 798, i32 0, metadata !906, null}
!919 = metadata !{i32 786689, metadata !181, metadata !"newVal", metadata !183, i32 16777258, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [newVal] [line 42]
!920 = metadata !{i32 42, i32 0, metadata !181, null}
!921 = metadata !{i32 786689, metadata !181, metadata !"oldVal", metadata !183, i32 33554474, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [oldVal] [line 42]
!922 = metadata !{i32 786689, metadata !181, metadata !"ptr", metadata !183, i32 50331690, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [ptr] [line 42]
!923 = metadata !{i32 786688, metadata !924, metadata !"prevVal", metadata !183, i32 44, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [prevVal] [line 44]
!924 = metadata !{i32 786443, metadata !182, metadata !181} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/./platform_x86.h]
!925 = metadata !{i32 44, i32 0, metadata !924, null}
!926 = metadata !{i32 46, i32 0, metadata !924, null}
!927 = metadata !{i32 585130, i32 585165}
!928 = metadata !{i32 58, i32 0, metadata !924, null} ; [ DW_TAG_imported_module ]
!929 = metadata !{i32 786689, metadata !178, metadata !"Self", metadata !22, i32 16777597, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 381]
!930 = metadata !{i32 381, i32 0, metadata !178, null}
!931 = metadata !{i32 786689, metadata !178, metadata !"maxv", metadata !22, i32 33554813, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [maxv] [line 381]
!932 = metadata !{i32 786688, metadata !178, metadata !"gv", metadata !22, i32 383, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [gv] [line 383]
!933 = metadata !{i32 383, i32 0, metadata !178, null}
!934 = metadata !{i32 786688, metadata !178, metadata !"wv", metadata !22, i32 384, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wv] [line 384]
!935 = metadata !{i32 384, i32 0, metadata !178, null}
!936 = metadata !{i32 786688, metadata !178, metadata !"k", metadata !22, i32 385, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [k] [line 385]
!937 = metadata !{i32 385, i32 0, metadata !178, null}
!938 = metadata !{i32 385, i32 16, metadata !178, null}
!939 = metadata !{i32 386, i32 0, metadata !178, null}
!940 = metadata !{i32 387, i32 0, metadata !941, null}
!941 = metadata !{i32 786443, metadata !1, metadata !178, i32 386, i32 0, i32 53} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!942 = metadata !{i32 388, i32 0, metadata !941, null}
!943 = metadata !{i32 394, i32 0, metadata !178, null}
!944 = metadata !{i32 395, i32 0, metadata !178, null}
!945 = metadata !{i32 786689, metadata !177, metadata !"Self", metadata !22, i32 16778470, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1254]
!946 = metadata !{i32 1254, i32 0, metadata !177, null}
!947 = metadata !{i32 786688, metadata !177, metadata !"dx", metadata !22, i32 1256, metadata !32, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dx] [line 1256]
!948 = metadata !{i32 1256, i32 0, metadata !177, null}
!949 = metadata !{i32 786688, metadata !177, metadata !"rv", metadata !22, i32 1257, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rv] [line 1257]
!950 = metadata !{i32 1257, i32 0, metadata !177, null}
!951 = metadata !{i32 786688, metadata !177, metadata !"rd", metadata !22, i32 1258, metadata !748, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1258]
!952 = metadata !{i32 1258, i32 0, metadata !177, null}
!953 = metadata !{i32 786688, metadata !177, metadata !"EndOfList", metadata !22, i32 1259, metadata !491, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [EndOfList] [line 1259]
!954 = metadata !{i32 1259, i32 0, metadata !177, null}
!955 = metadata !{i32 786688, metadata !177, metadata !"e", metadata !22, i32 1260, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1260]
!956 = metadata !{i32 1260, i32 0, metadata !177, null}
!957 = metadata !{i32 1264, i32 0, metadata !958, null}
!958 = metadata !{i32 786443, metadata !1, metadata !177, i32 1264, i32 0, i32 49} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!959 = metadata !{i32 786688, metadata !960, metadata !"v", metadata !22, i32 1266, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 1266]
!960 = metadata !{i32 786443, metadata !1, metadata !958, i32 1264, i32 0, i32 50} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!961 = metadata !{i32 1266, i32 0, metadata !960, null}
!962 = metadata !{i32 1267, i32 0, metadata !960, null}
!963 = metadata !{i32 1278, i32 0, metadata !964, null}
!964 = metadata !{i32 786443, metadata !1, metadata !960, i32 1267, i32 0, i32 51} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!965 = metadata !{i32 1280, i32 0, metadata !964, null}
!966 = metadata !{i32 1281, i32 0, metadata !967, null}
!967 = metadata !{i32 786443, metadata !1, metadata !960, i32 1280, i32 0, i32 52} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!968 = metadata !{i32 1283, i32 0, metadata !960, null}
!969 = metadata !{i32 1285, i32 0, metadata !177, null}
!970 = metadata !{i32 786689, metadata !174, metadata !"k", metadata !22, i32 16777971, metadata !37, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [k] [line 755]
!971 = metadata !{i32 755, i32 0, metadata !174, null}
!972 = metadata !{i32 786688, metadata !174, metadata !"e", metadata !22, i32 757, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 757]
!973 = metadata !{i32 757, i32 0, metadata !174, null}
!974 = metadata !{i32 786688, metadata !174, metadata !"End", metadata !22, i32 758, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 758]
!975 = metadata !{i32 758, i32 0, metadata !174, null}
!976 = metadata !{i32 759, i32 0, metadata !977, null}
!977 = metadata !{i32 786443, metadata !1, metadata !174, i32 759, i32 0, i32 47} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!978 = metadata !{i32 760, i32 0, metadata !979, null}
!979 = metadata !{i32 786443, metadata !1, metadata !977, i32 759, i32 0, i32 48} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!980 = metadata !{i32 761, i32 0, metadata !979, null}
!981 = metadata !{i32 762, i32 0, metadata !174, null}
!982 = metadata !{i32 786689, metadata !171, metadata !"Self", metadata !22, i32 16778597, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1381]
!983 = metadata !{i32 1381, i32 0, metadata !171, null}
!984 = metadata !{i32 786689, metadata !171, metadata !"wv", metadata !22, i32 33555813, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [wv] [line 1381]
!985 = metadata !{i32 786688, metadata !171, metadata !"wr", metadata !22, i32 1391, metadata !37, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [wr] [line 1391]
!986 = metadata !{i32 1391, i32 0, metadata !171, null}
!987 = metadata !{i32 786688, metadata !988, metadata !"p", metadata !22, i32 1394, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [p] [line 1394]
!988 = metadata !{i32 786443, metadata !1, metadata !171, i32 1393, i32 0, i32 43} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!989 = metadata !{i32 1394, i32 0, metadata !988, null}
!990 = metadata !{i32 786688, metadata !988, metadata !"End", metadata !22, i32 1395, metadata !491, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [End] [line 1395]
!991 = metadata !{i32 1395, i32 0, metadata !988, null}
!992 = metadata !{i32 1396, i32 0, metadata !993, null}
!993 = metadata !{i32 786443, metadata !1, metadata !988, i32 1396, i32 0, i32 44} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!994 = metadata !{i32 1400, i32 0, metadata !995, null}
!995 = metadata !{i32 786443, metadata !1, metadata !993, i32 1396, i32 0, i32 45} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!996 = metadata !{i32 1401, i32 0, metadata !997, null}
!997 = metadata !{i32 786443, metadata !1, metadata !995, i32 1400, i32 0, i32 46} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!998 = metadata !{i32 1403, i32 0, metadata !995, null}
!999 = metadata !{i32 1413, i32 0, metadata !995, null}
!1000 = metadata !{i32 1414, i32 0, metadata !995, null}
!1001 = metadata !{i32 1417, i32 0, metadata !171, null}
!1002 = metadata !{i32 1419, i32 0, metadata !171, null}
!1003 = metadata !{i32 786689, metadata !205, metadata !"tail", metadata !22, i32 16777857, metadata !42, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [tail] [line 641]
!1004 = metadata !{i32 641, i32 0, metadata !205, null}
!1005 = metadata !{i32 786688, metadata !205, metadata !"e", metadata !22, i32 643, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 643]
!1006 = metadata !{i32 643, i32 0, metadata !205, null}
!1007 = metadata !{i32 644, i32 0, metadata !205, null}
!1008 = metadata !{i32 645, i32 0, metadata !205, null}
!1009 = metadata !{i32 646, i32 0, metadata !205, null}
!1010 = metadata !{i32 647, i32 0, metadata !205, null}
!1011 = metadata !{i32 648, i32 0, metadata !205, null}
!1012 = metadata !{i32 649, i32 0, metadata !205, null}
!1013 = metadata !{i32 650, i32 0, metadata !205, null}
!1014 = metadata !{i32 652, i32 0, metadata !205, null}
!1015 = metadata !{i32 786689, metadata !214, metadata !"Self", metadata !22, i32 16778512, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 1296]
!1016 = metadata !{i32 1296, i32 0, metadata !214, null}
!1017 = metadata !{i32 786688, metadata !214, metadata !"rv", metadata !22, i32 1298, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rv] [line 1298]
!1018 = metadata !{i32 1298, i32 0, metadata !214, null}
!1019 = metadata !{i32 786688, metadata !214, metadata !"rd", metadata !22, i32 1299, metadata !748, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [rd] [line 1299]
!1020 = metadata !{i32 1299, i32 0, metadata !214, null}
!1021 = metadata !{i32 786688, metadata !214, metadata !"EndOfList", metadata !22, i32 1300, metadata !491, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [EndOfList] [line 1300]
!1022 = metadata !{i32 1300, i32 0, metadata !214, null}
!1023 = metadata !{i32 786688, metadata !214, metadata !"e", metadata !22, i32 1301, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [e] [line 1301]
!1024 = metadata !{i32 1301, i32 0, metadata !214, null}
!1025 = metadata !{i32 1305, i32 0, metadata !1026, null}
!1026 = metadata !{i32 786443, metadata !1, metadata !214, i32 1305, i32 0, i32 62} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1027 = metadata !{i32 786688, metadata !1028, metadata !"v", metadata !22, i32 1307, metadata !27, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [v] [line 1307]
!1028 = metadata !{i32 786443, metadata !1, metadata !1026, i32 1305, i32 0, i32 63} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1029 = metadata !{i32 1307, i32 0, metadata !1028, null}
!1030 = metadata !{i32 1308, i32 0, metadata !1028, null}
!1031 = metadata !{i32 1319, i32 0, metadata !1032, null}
!1032 = metadata !{i32 786443, metadata !1, metadata !1028, i32 1308, i32 0, i32 64} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1033 = metadata !{i32 1320, i32 0, metadata !1034, null}
!1034 = metadata !{i32 786443, metadata !1, metadata !1032, i32 1319, i32 0, i32 65} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1035 = metadata !{i32 1323, i32 0, metadata !1032, null}
!1036 = metadata !{i32 1324, i32 0, metadata !1037, null}
!1037 = metadata !{i32 786443, metadata !1, metadata !1028, i32 1323, i32 0, i32 66} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1038 = metadata !{i32 1325, i32 0, metadata !1039, null}
!1039 = metadata !{i32 786443, metadata !1, metadata !1037, i32 1324, i32 0, i32 67} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1040 = metadata !{i32 1328, i32 0, metadata !1028, null}
!1041 = metadata !{i32 1330, i32 0, metadata !214, null}
!1042 = metadata !{i32 786689, metadata !216, metadata !"Self", metadata !22, i32 16777459, metadata !132, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [Self] [line 243]
!1043 = metadata !{i32 243, i32 0, metadata !216, null}
!1044 = metadata !{i32 245, i32 12, metadata !216, null}
!1045 = metadata !{i32 786689, metadata !219, metadata !"seed", metadata !22, i32 16777446, metadata !222, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [seed] [line 230]
!1046 = metadata !{i32 230, i32 0, metadata !219, null}
!1047 = metadata !{i32 786688, metadata !219, metadata !"x", metadata !22, i32 232, metadata !75, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 232]
!1048 = metadata !{i32 232, i32 0, metadata !219, null}
!1049 = metadata !{i32 232, i32 28, metadata !219, null}
!1050 = metadata !{i32 233, i32 0, metadata !219, null}
!1051 = metadata !{i32 234, i32 0, metadata !219, null}
!1052 = metadata !{i32 786689, metadata !223, metadata !"x", metadata !22, i32 16777426, metadata !75, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [x] [line 210]
!1053 = metadata !{i32 210, i32 0, metadata !223, null}
!1054 = metadata !{i32 212, i32 0, metadata !223, null}
!1055 = metadata !{i32 213, i32 0, metadata !1056, null}
!1056 = metadata !{i32 786443, metadata !1, metadata !223, i32 212, i32 0, i32 69} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1057 = metadata !{i32 214, i32 0, metadata !1056, null}
!1058 = metadata !{i32 215, i32 0, metadata !223, null}
!1059 = metadata !{i32 216, i32 0, metadata !223, null}
!1060 = metadata !{i32 217, i32 0, metadata !223, null}
!1061 = metadata !{i32 218, i32 0, metadata !223, null}
!1062 = metadata !{i32 995, i32 0, metadata !235, null}
!1063 = metadata !{i32 996, i32 0, metadata !1064, null}
!1064 = metadata !{i32 786443, metadata !1, metadata !235, i32 995, i32 0, i32 80} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1065 = metadata !{i32 997, i32 0, metadata !1064, null}
!1066 = metadata !{i32 1000, i32 0, metadata !235, null}
!1067 = metadata !{i32 1001, i32 0, metadata !1068, null}
!1068 = metadata !{i32 786443, metadata !1, metadata !235, i32 1000, i32 0, i32 81} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1069 = metadata !{i32 1002, i32 0, metadata !1068, null}
!1070 = metadata !{i32 1004, i32 0, metadata !235, null}
!1071 = metadata !{i32 786688, metadata !236, metadata !"act", metadata !22, i32 969, metadata !326, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [act] [line 969]
!1072 = metadata !{i32 969, i32 0, metadata !236, null}
!1073 = metadata !{i32 971, i32 0, metadata !236, null}
!1074 = metadata !{i32 972, i32 0, metadata !236, null}
!1075 = metadata !{i32 973, i32 0, metadata !236, null}
!1076 = metadata !{i32 974, i32 0, metadata !236, null}
!1077 = metadata !{i32 976, i32 0, metadata !236, null}
!1078 = metadata !{i32 977, i32 0, metadata !1079, null}
!1079 = metadata !{i32 786443, metadata !1, metadata !236, i32 976, i32 0, i32 82} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1080 = metadata !{i32 978, i32 0, metadata !1079, null}
!1081 = metadata !{i32 981, i32 0, metadata !236, null}
!1082 = metadata !{i32 982, i32 0, metadata !1083, null}
!1083 = metadata !{i32 786443, metadata !1, metadata !236, i32 981, i32 0, i32 83} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1084 = metadata !{i32 983, i32 0, metadata !1083, null}
!1085 = metadata !{i32 985, i32 0, metadata !236, null}
!1086 = metadata !{i32 786689, metadata !237, metadata !"signum", metadata !22, i32 16778158, metadata !70, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [signum] [line 942]
!1087 = metadata !{i32 942, i32 0, metadata !237, null}
!1088 = metadata !{i32 786689, metadata !237, metadata !"siginfo", metadata !22, i32 33555374, metadata !240, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [siginfo] [line 942]
!1089 = metadata !{i32 786689, metadata !237, metadata !"context", metadata !22, i32 50332590, metadata !81, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [context] [line 942]
!1090 = metadata !{i32 786688, metadata !237, metadata !"Self", metadata !22, i32 944, metadata !132, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [Self] [line 944]
!1091 = metadata !{i32 944, i32 0, metadata !237, null}
!1092 = metadata !{i32 946, i32 0, metadata !237, null}
!1093 = metadata !{i32 947, i32 0, metadata !1094, null}
!1094 = metadata !{i32 786443, metadata !1, metadata !237, i32 946, i32 0, i32 84} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1095 = metadata !{i32 948, i32 0, metadata !1094, null}
!1096 = metadata !{i32 951, i32 0, metadata !237, null}
!1097 = metadata !{i32 952, i32 14, metadata !1098, null}
!1098 = metadata !{i32 786443, metadata !1, metadata !237, i32 951, i32 0, i32 85} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1099 = metadata !{i32 953, i32 0, metadata !1100, null}
!1100 = metadata !{i32 786443, metadata !1, metadata !1098, i32 952, i32 0, i32 86} ; [ DW_TAG_lexical_block ] [/home/vagrant/stamp-tl2-x86/tl2.c]
!1101 = metadata !{i32 954, i32 0, metadata !1100, null}
!1102 = metadata !{i32 955, i32 0, metadata !1098, null}
!1103 = metadata !{i32 957, i32 0, metadata !237, null}
!1104 = metadata !{i32 958, i32 0, metadata !237, null}
!1105 = metadata !{i32 959, i32 0, metadata !237, null}
!1106 = metadata !{i32 304, i32 0, metadata !308, null}
!1107 = metadata !{i32 305, i32 0, metadata !308, null}
