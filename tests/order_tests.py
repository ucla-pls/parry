from nose.tools import *
import re
import parry.order

class TestOrder:
    def setup(self):
        self.fst = parry.order.Order(1, 3, "b")
        self.many = [
            parry.order.Order(2, 4, "b"),
            parry.order.Order(7, 8, "d"),
            parry.order.Order(1, 2, "a"),
            parry.order.Order(1, 5, "c")
        ]

    def test_multi_dot_graph(self):
        expected = "digraph G { 1 -> 2; 2 -> 3; }"
        assert(self.fst.dot_graph() == expected)

    def test_dot_points_style(self):
        dot_points = self.fst.dot_points_style()

        # shouldn't include terminals
        matches = re.match("1|3", dot_points)
        assert(matches == None)
        assert(len(dot_points) > 0)

    def test_dot_terminals_style(self):
        dot_terminals = self.fst.dot_terminals_style()

        # shouldn't include non terminals
        matches = re.match("2", dot_terminals)
        assert(matches == None)
        assert(len(dot_terminals) > 0)

    # sorts by the last element in the range
    def test_cmp(self):
        self.many.sort()
        assert(self.many[0].name() == "a")
        assert(self.many[1].name() == "b")
        assert(self.many[2].name() == "c")
        assert(self.many[3].name() == "d")
