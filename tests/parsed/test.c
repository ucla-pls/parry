#include <stdio.h>
# define MEMSIZE 100
char mem[MEMSIZE];

void write(char name, int value){
  return mem[name] = value;
}

void read(char name){
  return mem[name];
}

int main() {
 ord_a_1:
  write('a', 1);
 ord_b_1:
  read('a');
  write('b', 1);
 ord_a_2:
  read('a');
 ord_b_2:
  printf("%d", mem['a']);
  printf("%d", mem['b']);
  return 0;
}
