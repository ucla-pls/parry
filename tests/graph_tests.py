from nose.tools import *
from parry.order import Order
from parry.graph import Graph

class TestGraph:
    def setup(self):
        simple_orders = [
            Order(1, 3, "a"),
            Order(1, 2, "b"),
            Order(1, 4, "c")
        ]

        self.simple_graph = Graph(simple_orders, "simple")

    # wraps output in graph syntax
    def test_dot_graph(self):
        graph = self.simple_graph.dot_graph()
        assert( graph[0:16] == "digraph simple {")
        assert( graph[-1] == "}")

    # doesn't mutate original orders
    def test_sort(self):
        original_orders = list(self.simple_graph.orders())
        sorted_graph = self.simple_graph.sort()
        assert(sorted_graph.orders()[0].name() == "b")
        assert(original_orders == self.simple_graph.orders())

    # produces the minimum cut
    def test_mincut(self):
        assert(self.simple_graph.mincut() == [2])

    def test_merged_orders(self):
        assert(self.simple_graph.merged_orders() == [(1,2), (2,3), (3,4)])
