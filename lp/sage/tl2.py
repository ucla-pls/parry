import sys
sys.path.append('.')

from parry.opt import run_file
from tl2_orders import line_orders
from parry.architectures import mapping as arch_mapping

graph_prefix = "lp/sage/graphs"

types = [
    'eager',
    'standard'
]

methods = [
    # "TxCommit",
    # "TxLoad",
    # "TxStart",
    "TxStore"
]

archs = [
    "TSO",
    # "PSO",
    # "ARM7"
]

for tl2_type in types:
    for name in methods:
        file_name="examples/tl2/%s/event/%s.dot" % (tl2_type, name)
        for arch in archs:
            interp = (name, tl2_type, arch, sys.argv[1])
            # TODO just pass in arch object
            print "==> Running:  %s, %s for %s using %s elim" % interp
            run_file(file_name, line_orders, arch, sys.argv[1])
            print "==> Complete: %s, %s for %s using %s elim\n" % interp
