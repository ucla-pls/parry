# 1. last line/write of DropLocks, first line after
# 2. last write of WriteBackForward, call to DropLocks
# NOTE this may be oversimplified since the write at 760 might not appear
#      if TL2_EAGER is set. We assume the fence was placed there for those
#      writes even though it might have been better to place them "closer"
#      to the write
line_orders = [
    # TxCommit

    # last write of WriteBackForward and second store in DropLocks
    # works if TL2_EAGER is defined or not
    ("760", "1413", ("store", "store")),

    # the return statement at the end of TryFastUpdate and one of the last stores
    # in DropLocks
    ("1413", "1679", ("store", "load")),

    # TxLoad TL2_EAGER

    # after lock loaded, before deref of memory location
    ("1989", "1992", ("load", "load")),

    # after deref of memory location, before load lock
    ("1992", "1994", ("load", "load")),

    # TxLoad !TL2_EAGER

    # after lock loaded, before deref of memory location
    ("2077", "2079", ("load", "load")),

    # after deref of memory location, before load lock
    ("2079", "2081", ("load", "load")),


    # TxStart

    # after the global version lock, before the end of the function
    ("2170", "2182", ("load", "load")),


    # TxStore

    # after checking the value in the queue
    ("1885", "1922", ("load", "load"))
]
