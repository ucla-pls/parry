from coopr.pyomo import (AbstractModel, Var, Objective, Constraint, RangeSet, Param, summation,
                         NonNegativeReals, summation, ConstraintList, maximize)
from coopr.opt import SolverFactory

model = AbstractModel()

nodes = [1,2,3,4]
edges = [(1,2), (2,3), (3,4)]

# TODO derive from orders
commodities = [("a", (1,3)), ("b", (2,3)), ("c", (2,4))]

# flows
model.k = len(commodities)

# edges
model.e = len(edges)

# commodities
model.K = RangeSet(1, model.k)

# edges
model.E = RangeSet(1, model.e)

# per commodity, per edge flow
model.flows = Var(model.E, model.K, initialize=0)

def obj_expr(model):
    return summation(model.flows)

model.Objective = Objective(rule=obj_expr, sense=maximize)

def capacity_constr(model, x):
    # the sum of flow for each commodity across the edge x <= 1
    return sum(model.flows[x,i] for i in model.K) <= 1

# the next line creates one constraint for each member of the set model.I
model.Capacity = Constraint(model.E, rule=capacity_constr)
model.Conservation = ConstraintList()

model.create()

# the next line creates one constraint for each member of the set model.I
for e in instance.E
    # for in_edge in nodes.in_edges():
    #     in_sum = summation(model.flows[in_edge])

    # for out_edge in nodes.out_edges():
    #     out_sum = summation(model.flows[out_edge])

    for in_edge in edges:
        _, w = in_edge
        if w == node:
            in_sum = summation(model.flows[in_edge,])

    for out_edge in edges:
        v, _ = in_edge
        if v == node:
            out_sum = summation(model.flows[out_edge])

    # the difference in the sum into and out of a node must be
    if in_sum and out_sum:
        instance.conserv_constr.add(in_sum - out_sum <= 0)
