from coopr.pyomo import (ConcreteModel, Var, Objective, Constraint, RangeSet,
                         NonNegativeReals, summation, ConstraintList, maximize)
from coopr.opt import SolverFactory

model = ConcreteModel()

# TODO derive from graph
nodes = [1,2,3,4]
edges = [(1,2), (2,3), (3,4)]

# TODO derive from orders
commodities = [("a", (1,3)), ("b", (2,3)), ("c", (2,4))]

# for every edge map a capacity
capacity = {}
k = len(commodities)

# need a flow variable for every edge for ever commodity
flows = {}

model.K = RangeSet(1, k)

for e in edges:
    # NOTE in our case the capacity is 1
    capacity[e] = 1
    model.F = Var(model.K, domain=NonNegativeReals)

model.cap_constr = ConstraintList()
for e in edges:
    # constraint: the sum of flows for edge `e` for each commodity
    # should be less than the capacity of the edge
    model.cap_constr.add(summation(model.e) <= capacity[e])


max_expr = None
for e in edges:
    if max_expr:
        max_expr = max_expr + summation(model.flows[e])
    else:
        max_expr = summation(model.flows[e])

model.objective = Objective(expr=max_expr, sense=maximize)


# flow conservation constraints
model.conserv_constr = ConstraintList()
for node in nodes:
    in_sum = None
    out_sum = None

    # for in_edge in nodes.in_edges():
    #     in_sum = summation(model.flows[in_edge])

    # for out_edge in nodes.out_edges():
    #     out_sum = summation(model.flows[out_edge])

    for in_edge in edges:
        _, w = in_edge
        if w == node:
            in_sum = summation(model.flows[in_edge])

    for out_edge in edges:
        v, _ = in_edge
        if v == node:
            out_sum = summation(model.flows[out_edge])

    # the difference in the sum into and out of a node must be
    if in_sum and out_sum:
        instance.conserv_constr.add(in_sum - out_sum <= 0)

# capacity constraints on the flow across the corresponding edge


instance = model.create()
instance.pprint()

solver = SolverFactory('glpk')
results = solver.solve(instance)

print(results)
