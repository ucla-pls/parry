from sympy import init_printing, pprint, Matrix

# setup pretty printing
init_printing(use_unicode=True)

def is_unimodular(m):
    return m.det() in [1, 0, -1]

def is_tu(m, debug=False):
    # for each row
    for i in range(m.rows):
        # for each column
        for j in range(m.rows):
            rows_left = m.rows - i
            cols_left = m.cols - j

            # choose the larger of the two values
            distance = rows_left if rows_left < cols_left else cols_left

            # for all columns or rows left check the square matrices
            for k in range(distance):
                # i + k + 1 == the current row to the max distance inclusive
                # j + k + 1 == the current column to the max distance inclusive

                sub_m = m[i:i+k+1, j:j+k+1]

                # dump out if one of the matrices is not unimodular
                if not is_unimodular(sub_m):
                    if debug:
                        rows = range(i, i+k+1)
                        cols = range(j, j+k+1)

                        print "submatrix: "
                        pprint(sub_m)
                        print

                        print "submatrix determinant: "
                        print(sub_m.det())
                        print

                        print "rows, columns:"
                        pprint((rows, cols))
                        print

                        print "required rows submatrix:"
                        pprint( m[rows[0]:(rows[-1]+1), 0:] )
                        print

                    return False

    return True


def path_edge_matrix(edges, graph):
    return Matrix(path_edge_array(edges, graph))

def path_edge_array(edges, graph):
    path_edge = []

    for path in graph:
        incidence_vector = []

        for e in edges:
            incidence_vector.append(1 if (e in path) else 0)

        path_edge.append(incidence_vector)

    return path_edge
