import itertools
import util
import pdb
from sympy import init_printing, pprint, Matrix

# setup pretty printing
init_printing(use_unicode=True)


# NOTE test of unimodularity check
edges = [1,2,3,4,5,6,7,8,9,10]

graph = [
     [1,2],
     [1,2,3],
     [3,4,5],
     [4,5,6],
     [5,6,7],
     [6,7,8],
     [7,8,9],
     [8,9,10],
     [9,10],
     [10]
]

m = util.path_edge_matrix(edges, graph)

column_pairs = [(val-1, edges[i+1]-1) for i, val in enumerate(edges) if len(edges) > i+1]

row_count = m.rows

perms = itertools.permutations(range(0, row_count), row_count)

completion = [1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0]

def log(msg, p=False):
    return
    if p:
        pprint( msg )
    else:
        print( msg )

# The following assumes a well formed initial matrix, where well formed
# means that it has sequences of at least 2 ones or zeros or
# that if there is single displaced one or zero that it's at the beginning/end
# NOTE flesh out these constraints more fully
for fst, snd in column_pairs:

    for i, perm in enumerate(itertools.permutations(range(0, row_count), row_count)):
        # copy the matrix
        m_copy = m[:,:]

        print( i )

        for j, row in enumerate(perm):

            log( row )
            log( "before:" )
            log( m, p=True )

            # TODO make sure two columns on either side are consistent
            # NOTE they need not be the same just consisten

            # replace the submatrix according to the permutations
            if row < row_count/2:
                m_copy[row, fst:snd+3] = Matrix([[1, 1, 0, 0]])

                # fill the rest of the row with ones to simplify the paths
                # pdb.set_trace()
                # m_copy[row, snd+3:] = Matrix([ completion[0:(m_copy.cols - snd - 3)] ])
                m_copy[row, snd+3:] = Matrix([ [1] * (m_copy.cols - snd - 3) ])
            else:
                m_copy[row, fst:snd+3] = Matrix([[0, 0, 1, 1]])

                # pdb.set_trace()
                # fill the rest of the row with ones to simplify the paths
                m_copy[row, snd+3:] = Matrix([ [1] * (m_copy.cols - snd - 3) ])

            if not util.is_tu(m_copy, debug=True):
                log( "failing matrix permutation:" )
                pprint(m_copy)

                log( "failing column pair:" )
                log( (fst, snd) )

                log( "current row:" )
                log( row )


                raise Exception("not tu")

            log( "after:" )
            log( m_copy, p=True )
