import itertools
import util
import pdb
from sympy import init_printing, pprint, Matrix

# setup pretty printing
init_printing(use_unicode=True)


# NOTE test of unimodularity check
edges = [1,2,3,4,5,6]

graph = [
     [1,2],
     [1,2,3],
     [2,3,4],
     [3,4,5],
     [4,5,6],
     [5,6],
]

m = util.path_edge_matrix(edges, graph)

column_pairs = [(val-1, edges[i+1]-1) for i, val in enumerate(edges) if len(edges) > i+1]

row_count = m.rows

perms = itertools.permutations(range(0, row_count), row_count)

counter = 0

# The following assumes a well formed initial matrix, where well formed
# means that it has sequences of at least 2 ones or zeros or
# that if there is single displaced one or zero that it's at the beginning/end
# NOTE flesh out these constraints more fully
for fst, snd in column_pairs:

    for perm in itertools.permutations(range(0, row_count), row_count):
        # copy the matrix
        m_copy = m[:,:]

        for j, row in enumerate(perm):
            # TODO make sure two columns on either side are consistent
            # NOTE they need not be the same just consisten

            # if the two preceeding rows are not the same value we can't
            # replace these with different values
            if fst - 2 >= 0 and m_copy[row,fst-2] != m_copy[row,fst-1]:
                continue

            # if the two following rows are not the same value we can't
            # replace these with different values
            if snd + 2 <= m_copy.cols and m_copy[row, snd + 1] != m_copy[row, snd + 2]:
                continue


            # replace the submatrix according to the permutations
            if j < row_count/2:
                m_copy[row, fst:snd+1] = Matrix([[1, 1]])
            else:
                m_copy[row, fst:snd+1] = Matrix([[0, 0]])

            if not util.is_tu(m_copy, debug=True):
                print "failing matrix permutation:"
                pprint(m_copy)
                print

                raise Exception("not tu")
