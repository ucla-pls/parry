import inspect, os
abs_path = os.path.abspath(inspect.getfile(inspect.currentframe()))
dirname = os.path.dirname(abs_path) + "/"

if dirname not in sys.path:
    sys.path.insert(0, dirname)

import examples
import util

graph = examples.long_if_else
edges = examples.edges

path_edge_matrix = []
for path in graph:
    incidence_vector = []

    for e in edges:
        incidence_vector.append(1 if (e in path) else 0)

    path_edge_matrix.append(incidence_vector)

m = matrix(path_edge_matrix)

if not util.is_tu(m):
    raise Exception('paths incidence matrix is not TU')

prog = MixedIntegerLinearProgram(maximization=False)

# constraint value "vector"
b = 1

# cuts/edge binary variables
cuts = prog.new_variable(binary = True)

# objective function to constrain the size of the sum of the cuts
# NOTE the minimization is set when instantiating the LP at the top
prog.set_objective(sum(cuts[e] for e in edges))

for path in graph:
    # each linear equation is:
    # the sum of each edge with a binary coefficient based on
    # its presence in the current `path
    path_constraints = sum((1 if (e in path) else 0) * cuts[e] for e in edges)
    prog.add_constraint(path_constraints >= b)

prog.solve()

result = prog.get_values(cuts)
[ x for x in result.iteritems()]
