try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'author': 'John Bender',
    'url': 'TODO',
    'download_url': 'TODO',
    'author_email': 'johnbender@cs.ucla.edu',
    'version': '0.1',
    'install_requires': ['click == 3.3', 'clang == 3.3', 'nose'],
    'tests_require' : [ 'nose' ],
    'packages': ['parry'],
    'scripts': [],
    'name': 'parry',
    'description': 'A python library wrapping libclang that produces optimum \
    fences for specified orders'
}

setup(**config)
