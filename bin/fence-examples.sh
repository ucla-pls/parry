set -e

elim="$1"

if [ -z "$elim" ]; then
  elim="full"
fi

function run_tl2_eager {
  arch=$1
  flags=${@:2}

  src=examples/tl2/src
  compiled=examples/tl2/compiled
  intermediate=$src/tl2-eager-$arch-intermediate.c
  final=$compiled/tl2-eager-$arch.c

  echo "running: TL2-Eager-$1 - final output in $final..."
  time ./bin/parry TxLoad $src/tl2.c 1991:load-1993:load,1993:load-1995:load $arch --output $final $flags
  cp $final $intermediate

  # NOTE the output file and input file cannot coincide currently
  # !!! NOTE removed 760:store-1413:store !!!
  time ./bin/parry TxCommit $intermediate 1413:store-1679:load $arch --output $final $flags
}

function run_tl2 {
  arch=$1
  flags=${@:2}

  src=examples/tl2/src
  compiled=examples/tl2/compiled
  intermediate=$src/tl2-$arch-intermediate.c
  final=$compiled/tl2-$arch.c

  echo "running: TL2-$1 - final output in $final..."
  time ./bin/parry TxLoad $src/tl2.c 2078:load-2080:load,2080:load-2082:load $arch --output $final $flags --altindex=2
  cp $final $intermediate

  time ./bin/parry TxStore $intermediate 1886:load-1923:load $arch --output $final $flags --altindex=2
  cp $final $intermediate

  # NOTE the order from 51 is a proxy for both 1555 and 1596 since
  # the cmpxhg is inlined at those lines from 51 in platform_arm.
  # For x86 the cmpxchg is assembly which we don't handle so any
  # line in their custom compare and swap function will work
  if [ "$arch" == "arm" ]; then
    time ./bin/parry TxCommit $intermediate 51:store-1625:store,760:store-1413:store,1413:store-1679:load $arch --output $final $flags --altindex=5
  else
    time ./bin/parry TxCommit $intermediate 58:store-1625:store,760:store-1413:store,1413:store-1679:load $arch --output $final $flags --altindex=5
  fi

}

function run_rstm {
  arch=$1
  flags=${@:2}

  src=examples/rstm/src/libstm/algs
  compiled=examples/rstm/compiled
  intermediate=$src/byteeager-$arch-compiled.cpp
  final=$compiled/byteeager-$arch.cpp
  includes=-Iexamples/rstm/src/include

  rm -f "$intermediate"

  echo "running: RSTM-TLRW-$1 - final output in $final..."
  time ./bin/parry rollback $src/byteeager.cpp 261:store-265:store $arch --ccflags "$includes" --output $final $flags
  cp $final $intermediate

  # this order will certainly be "over fenced" since the bcas acts as a full barrier
  # to solve this we would have to examine the asm inserted around the critical store and see the xchg
  time ./bin/parry write_rw $intermediate 228:store-238:load $arch --ccflags "$includes" --output $final $flags
  cp $final $intermediate

  # this order will certainly be "over fenced" since the bcas acts as a full barrier
  # to solve this we would have to examine the asm inserted around the critical store and see the xchg
  time ./bin/parry write_ro $intermediate 186:store-196:load $arch --ccflags "$includes" --output $final $flags
  cp $final $intermediate

  # this order requires that byteeager is compiled with/without STM_HAND_ROLLED when benchmarked
  time ./bin/parry read_rw $intermediate 163:store-165:load $arch --ccflags "$includes" --output $final $flags
  cp $final $intermediate

  # this order requires that byteeager is compiled with/without STM_HAND_ROLLED when benchmarked
  time ./bin/parry read_ro $intermediate 125:store-128:load $arch --ccflags "$includes" --output $final $flags
}

function run_dekker {
  arch=$1
  flags=${@:2}
  src=examples/classic/src
  compiled=examples/classic/compiled
  intermediate=$src/dekker-$arch-intermediate.c
  final=$compiled/dekker-$arch.c

  rm -f "$intermediate"

  echo "running: dekker-$1 - final output in $final..."
  time ./bin/parry thr2 $src/dekker.c 30:store-26:load $arch --output $final $flags
  cp $final $intermediate

  time ./bin/parry thr2 $intermediate 25:store-26:load $arch --output $final $flags
  cp $final $intermediate

  time ./bin/parry thr1 $intermediate 13:store-9:load $arch --output $final $flags
  cp $final $intermediate

  time ./bin/parry thr1 $intermediate 8:store-9:load $arch --output $final $flags
}

function run_parker {
  arch=$1
  flags=${@:2}
  src=examples/classic/src
  compiled=examples/classic/compiled
  intermediate=$src/parker-$arch-intermediate.c
  final=$compiled/parker-$arch.c

  rm -f "$intermediate"

  echo "running: parker-$1 - final output in $final..."
  time ./bin/parry park $src/parker.c 44:store-46:any $arch --output $final $flags
}

function run_peterson {
  arch=$1
  flags=${@:2}
  src=examples/classic/src
  compiled=examples/classic/compiled
  intermediate=$src/peterson-$arch-intermediate.c
  final=$compiled/peterson-$arch.c

  rm -f "$intermediate"

  echo "running: peterson-$1 - final output in $final..."
  time ./bin/parry thr2 $src/peterson.c 14:store-16:load $arch --output $final $flags
  cp $final $intermediate

  time ./bin/parry thr1 $intermediate 5:store-7:load $arch --output $final $flags
}

function run_lamport {
  arch=$1
  flags=${@:2}
  src=examples/classic/src
  compiled=examples/classic/compiled
  intermediate=$src/lamport-$arch-intermediate.c
  final=$compiled/lamport-$arch.c

  rm -f "$intermediate"

  echo "running: lamport-$1 - final output in $final..."
  time ./bin/parry thr2 $src/lamport.c 31:store-32:load,37:store-38:load $arch --output $final $flags
  cp $final $intermediate

  time ./bin/parry thr1 $intermediate 8:store-9:load,14:store-15:load $arch --output $final $flags
}

# TL2 Eager
run_tl2_eager x86 --debug=true --ccflags -DTL2_EAGER --elim=$elim
run_tl2_eager arm --debug=true --ccflags -D__arm__ --ccflags -DTL2_EAGER --elim=$elim

# TL2
run_tl2 x86 --debug=true  --elim=$elim
run_tl2 arm --debug=true --ccflags -D__arm__  --elim=$elim

# rstm's TLRW
run_rstm x86 --debug=true --elim=$elim
run_rstm arm --debug=true --elim=$elim

# dekker
run_dekker x86 --debug=true --elim=$elim
run_dekker arm --debug=true --elim=$elim

# parker
run_parker x86 --debug=true --elim=$elim
run_parker arm --debug=true --elim=$elim

# peterson
run_peterson x86 --debug=true --elim=$elim
run_peterson arm --debug=true --elim=$elim

# lamport
run_lamport x86 --debug=true --elim=$elim
run_lamport arm --debug=true --elim=$elim
