#!/bin/bash

# exit on error
set -e

function install(){
  apt-get install -y $@
}

function which_install(){
  if ! which $2; then
    install $1
  fi
}

# prelim for this script to work
install software-properties-common
install wget

sudo add-apt-repository \
  "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) universe"

sudo add-apt-repository \
  "deb http://downloads.skewed.de/apt/trusty trusty universe"

apt-get update

# clang 3.3 works with llvmpy
install libclang-3.3-dev llvm-3.3

which_install clang-3.3 clang
which_install python-pip pip
which_install graphviz graphviz
which_install cmake cmake

# linear programming solver
which_install glpk-utils glpsol

# install graph tool, update to packages happens in the above commands
if ! python -c "import graph_tool"; then
  apt-get install -y --force-yes python-graph-tool
fi

if ! which sage; then
  cd /tmp

  dir=sage-6.6-x86_64-Linux
  name=$dir-Ubuntu_14.04_x86_64.tar.gz
  echo "Downloading sage, this might take a while (~ 700mb)..."

  if ! [ -e "$dir" ]; then
    wget -cq "http://boxen.math.washington.edu/home/sagemath/sage-mirror/linux/64bit/$name" -O "$name"
    tar -xzf "$name"
  fi

  if ! [ -e "/user/share/$dir" ]; then
    mv "$dir" /usr/share/
  fi

  ln -s "/usr/share/$dir/sage" /usr/local/bin/sage
  cd -
fi

# install llvmpy
LLVM_CONFIG_PATH=/usr/bin/llvm-config-3.3 pip install llvmpy

# ensure llvmpy is properly installed
python -c "import llvm; llvm.test()"

# setup up dist and virtualenv so we can use the packages that get installed
pip install distribute
pip install virtualenv

# make sure opt is available as `opt`
if ! [ -e /usr/bin/opt ]; then
  ln -s `which opt-3.3` /usr/bin/opt
fi
