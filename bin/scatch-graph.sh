for file in `find scratch -name "*subgraph-ppo*.dot"`; do
  echo "converting $file"
  no_ext=$( echo $file | sed 's/\.dot//' )
  dot $file -Tpng > $no_ext.png
done
