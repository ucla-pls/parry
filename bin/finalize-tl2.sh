# TODO clean up graph name issues
# TODO pull the method and file names from the python

for tl2_type in eager standard; do
  for method in TxCommit TxLoad TxStart TxStore; do
    # fix the graph title
    sed -i'' \
        "s/digraph.*{/digraph \"$method\" {/" \
        "examples/tl2/$tl2_type/cfg/$method.dot"

    # remove the useless label
    sed -i'' 's/^\slabel=.*//' "examples/tl2/$tl2_type/cfg/$method.dot"

    ./bin/parry \
        "$method" \
        "examples/tl2/$tl2_type/cfg/$method.dot" \
        "examples/tl2/$tl2_type/ir.ll" \
        --output="examples/tl2/$tl2_type/event/$method.dot"
  done
done
