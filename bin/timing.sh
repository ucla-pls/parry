set -e

elim_algo="$1"

if [ -z "$elim_algo" ]; then
  echo "Usage: $0 <direct|full>"
  exit 1;
fi

work_dir="/tmp/parry-fence-times"

method_dir="$work_dir/method"
aggr_dir="$work_dir/aggregate"

rm -rf "$method_dir"
rm -rf "$aggr_dir"

mkdir -p $method_dir
mkdir -p $aggr_dir

# run the rest as appends
for i in $(seq 1 100); do
  bash bin/fence-examples.sh $1 &> $work_dir/parry-fence-times.txt

  algo_arch=""
  method=""
  took=""
  cat $work_dir/parry-fence-times.txt \
    | grep "took\|method\|running\|real" \
    | while read line; do
        if echo "$line" | grep "running" > /dev/null; then
          took=""
          method=""

          # write everything to a file
          algo_arch=$( echo $line | sed 's/.*running: \(.*\) -.*/\1/' );
        fi

        if echo "$line" | grep "method:" > /dev/null; then
          took=""
          what=""
          method=$( echo $line | sed 's/.*method: \(.*\)$/\1/' );
        fi

        # grab output from parry
        if echo "$line" | grep "took" > /dev/null; then
          what="$what,$( echo $line | sed 's/.* \(.*\) took \(.*\) seconds.*/\1/' )";
          took="$took,$( echo $line | sed 's/.* \(.*\) took \(.*\) seconds.*/\2/' )";
        fi

        # grab the time output for the whole algo
        if echo "$line" | grep "real" > /dev/null; then
          what="$what,real";
          took="$took,$( echo $line | sed 's/.*m\(.*\)s$/\1/' )";
        fi

        if ! [ -z "$took" ] && ! [ -z "$method" ]; then
          echo "algo-arch,method$what" > "$method_dir/$algo_arch-$method-$elim_algo.txt";
          echo "$algo_arch,$method$took" >> "$method_dir/$algo_arch-$method-$elim_algo.txt";
        fi
      done

  output="$method_dir/*";

  for file in $output; do
    aggr_file=$( echo "$file" | sed 's/method/aggregate/' );

    # depends on the rm at the top
    if ! [ -e $aggr_file ]; then
      head -n1 "$file" > $aggr_file
    fi

    # grab the data line
    tail -n1 "$file" >> $aggr_file
  done
done
