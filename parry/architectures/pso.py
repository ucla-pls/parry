from architectures.tso import TSO

class PSO():
    def __init__(self):
        self._tso = TSO()

    def is_weak(self, fst_event, snd_event):
        # delegate to tso, pso only adds store reordering
        tso_weak = self._tso.is_weak(fst_event, snd_event)

        fst_type, fst_var = fst_event
        snd_type, snd_var = snd_event

        stores = (fst_type == "store" and snd_type == "store")
        diff_var = (fst_var != snd_var)

        return tso_weak or (stores and diff_var)

    def is_preserved(self, fst_event, snd_event):
        return not self.is_weak(fst_event, snd_event)
