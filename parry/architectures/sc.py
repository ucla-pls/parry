class SC():
    def is_weak(self, fst_event, snd_event):
        return false

    def is_preserved(self, fst_event, snd_event):
        return not self.is_weak(fst_event, snd_event)
