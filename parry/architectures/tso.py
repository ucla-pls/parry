from architectures.arch import Arch

class TSO(Arch):
    def __init__(self):
        self.fences = {
            ('store', 'load'): 'mfence',
            ('store', 'any'): 'mfence'
        }

        self.order_order = [
            ('store', 'load'),
            ('store', 'any')
        ]

    def is_weak(self, fst_event, snd_event):
        fst_type, fst_var = fst_event
        snd_type, snd_var = snd_event

        is_call = 'call' in [fst_type, snd_type]

        return (is_call or
                (fst_type == 'store'
                 and snd_type == 'load'
                 and fst_var != snd_var))

    def is_preserved(self, fst_event, snd_event):
        fst_type, fst_var = fst_event
        snd_type, snd_var = snd_event

        # if either type is cmpxchg the order is preserved
        # NOTE this assumes the seq_cst version (sorted in PO graph)
        # can't order with "br"
        return ((not "br" in [fst_type, snd_type]) and
                (not self.is_weak(fst_event, snd_event)
                 or "cmpxchg" in [fst_type, snd_type]))



    def filter_orders(self, grouped):
        return dict((k,v) for k,v in grouped.items() if k in self.order_order)
