from architectures.arch import Arch

# NOTE does not consider atomic reordering
class ARM7(Arch):
    def __init__(self):
        # TODO
        self.fences = {
            ('store', 'load'): "DMB ST",
            ('load', 'load'): "DMB",
            ('store', 'store'): "DMB ST",
            ('load', 'store'): "DMB",
            ('store', 'any'): "DMB ST"
        }

        self.order_order = [
            ('store', 'load'),
            ('store', 'any'),
            ('load', 'load'),
            ('store', 'store'),
            ('load', 'store')
        ]

    def is_weak(self, fst_event, snd_event):
        return not self.is_preserved(fst_event, snd_event)

    def is_preserved(self, fst_event, snd_event):
        fst_type, fst_var = fst_event
        snd_type, snd_var = snd_event

        is_call = 'call' in [fst_type, snd_type]

        # if either type is cmpxchg the order is preserved
        # NOTE this assumes the seq_cst version (sorted in PO graph)
        return ((fst_var == snd_var and not is_call)
                or fst_type == "cmpxchg"
                or snd_type == "cmpxchg")


    def filter_orders(self, grouped_orders):
        return grouped_orders
