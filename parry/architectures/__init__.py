from architectures.tso import TSO
from architectures.pso import PSO
from architectures.arm7 import ARM7

mapping = {
    'TSO': TSO(),
    'X86': TSO(),
    'PSO': PSO(),
    'ARM7': ARM7(),
    'ARM': ARM7()
}
