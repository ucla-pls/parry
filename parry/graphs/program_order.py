import re
from llvm.core import Module
from graph_tool import Graph

class FunctionNotFoundException(Exception):
    pass

class AmbiguousFunctionException(Exception):
    pass

class POGraph():
    _NODE_PROPS = [
        'event',
        'variable',
        'line',
        'instruction',
        'label'
    ]

    _BLOCK_NUM_LABEL_RE = re.compile(r'<label>:([0-9]+)', re.MULTILINE)

    _BLOCK_NAME_LABEL_RE = re.compile(r'([^:]+):', re.MULTILINE)

    _GRAPH_LABEL_RE = re.compile(r'^\{%?([^:]+):')

    _METADATA_LINE_RE = re.compile(r'^!\{i32 ([0-9]+)')

    _LLVM_ID_RE = re.compile(r'([%@][a-zA-Z\$\._0-9]+)')

    _LLVM_CALL_ID_RE = re.compile(r'([@][a-zA-Z\$\._0-9]+)')

    _INTEGER_VAL_RE = re.compile(r'([0-9]+)$')

    _SUPPORTED = lambda i: True

    # The set of opcodes we're concerned about, and callable criteria
    # for the rest of the instruction to match
    # http://llvm.org/docs/LangRef.html#memory-access-and-addressing-operations
    # TODO opcode objects
    _OPCODES = {
        'load': _SUPPORTED,
        'store': _SUPPORTED,
        'br': _SUPPORTED,

        # we only consider seq_cst cmpxchg for now
        'cmpxchg': lambda i: re.search(r'seq_cst', str(i)),

        # if the call is to an internal llvm function we aren't concerned
        'call': lambda i: not i.called_function or not re.match(r'llvm\.', i.called_function.name)
    }

    # TODO keyword args
    # graph : graph_tool.Graph
    # fn_name : string, name of function considered in IR
    # ll_file : file, LLVM IR file
    def __init__(self, graph, fn_name, ll_file):
        self._ll_file = ll_file

        self._graph = graph.copy()

        # parse the module definition
        # TODO try parsing `from_assembly` on the node label
        self._module = Module.from_assembly(ll_file)

        # get the targeted function
        self._function = self._find_function(fn_name)

        self._declarations = {}

        # TODO move out to a public method that users call
        self._create_block_nodes()

    def _find_function(self, fn_name):
        fns = []
        for f in self._module.functions:
            if fn_name in f.name:
                fns.append(f)

        if len(fns) == 1:
            return fns[0]

        if len(fns) > 1:
            # try to find an exact match
            for f in fns:
                if f.name == fn_name:
                    return f

            # no exact match, raise exception
            msg = "More than one function found with the name `%s`" % fn_name
            raise AmbiguousFunctionException(msg)

        msg = "%s not found in %s" % (fn_name, self._ll_file)
        raise FunctionNotFoundException(msg)

    def _create_block_nodes(self):
        old_graph = self._graph.copy()
        self._graph = self._init_event_graph()

        replacements = {}
        for v in old_graph.vertices():
            block = self._find_block(v, old_graph)

            # record the replacement head and tail
            replacements[v] = self._init_new_nodes(block)

        # restablish the edges from the block nodes in the old graph
        self._relink(old_graph, replacements)

    # take the old links between the large blocks and relink them
    # with the new nodes constructed from the blocks
    def _relink(self, old, replacements):
        for v in old.vertices():
            for ine in v.in_neighbours():
                _, last = replacements[ine]
                first, _ = replacements[v]

                # connect the last node of the replacement nodes for the
                # in_neighbours to the first node of the replacement for v
                self._graph.add_edge(last, first)


    # NOTE there might be a better way to do this, but currently
    # the only way to get the corresponding IR block from the function
    # associated with a dot graph node is to match the labels by
    # scanning the blocks for the function
    def _find_block(self, v, graph):
        raw_label = graph.vertex_properties['label'][v]

        label = self._GRAPH_LABEL_RE.match(raw_label).group(1)

        block = None

        if label == "0":
            block = self._function.basic_blocks[0]
        else:
            for b in self._function.basic_blocks:
                numl = self._BLOCK_NUM_LABEL_RE.search(str(b))

                if numl and numl.group(1).strip() == label:
                    block = b
                    break

                namel = self._BLOCK_NAME_LABEL_RE.search(str(b))

                if namel and namel.group(1).strip() == label:
                    block = b
                    break

        if not block:
            # TODO custom
            raise Exception("Graph label could not be matched with an IR label")

        return block

    # create a new empty graph to hold events from the cfg
    def _init_event_graph(self):
        new_graph = Graph()

        # add new proprety
        for prop in self._NODE_PROPS:
            new_graph.vertex_properties[prop] = self._graph.new_vertex_property('string')

        return new_graph

    def _supported_instruction(self, instr):
        name = instr.opcode_name
        opcodes = self._OPCODES.keys()

        if name not in opcodes:
            return

        return self._OPCODES[name](instr)

    # build a new set of nodes out of the blocks from the old CFG
    def _init_new_nodes(self, block):
        new_nodes = []
        for instr in block.instructions:
            self._get_declaration(instr)

        for instr in block.instructions:
            if not self._supported_instruction(instr):
                continue

            # create a new vertex for the operation
            vertex  = self._graph.add_vertex()

            self._set_vertex_attributes(vertex, instr)

            # establish edges after the first node
            if len(new_nodes) > 0:
                self._graph.add_edge(previous, vertex)

            new_nodes.append(vertex)
            previous = vertex

        # if there are no relevant nodes create a "dummy" so the graph
        # maintains its shape
        if len(new_nodes) == 0:
            # explicit instead of using the left over iter var
            instr = block.instructions[0]

            vertex = self._graph.add_vertex()
            self._set_vertex_attributes(vertex, instr, variable="<none>", event="noop")
            new_nodes.append(vertex)

        return (new_nodes[0], new_nodes[-1])

    def _set_prop(self, name, v, *var_args):
        value = var_args[0]

        for arg in var_args[1:]:
            value += ", " + arg

        self._graph.vertex_properties[name][v] = value

    def _get_prop(self, name, v):
        return self._graph.vertex_properties[name][v]

    def _get_declaration(self, instr):
        if instr.opcode_name == "call" and re.search(r'declare', str(instr)):
            # declaration operation from the operands
            key_string = str(instr.operands[0].operands[0])

            # grab the identifier, sometimes the same as name, sometimes not
            # http://llvm.org/docs/LangRef.html#identifiers
            key = self._LLVM_ID_RE.search(key_string).group(1)
            name = instr.operands[1].operands[2].string

            # keep a mapping of declarations so we can reference them
            # with load and store operands to get variable names
            self._declarations[key] = name

        # grab allocations that are stored in temps too
        if instr.opcode_name == "alloca":
            key = self._LLVM_ID_RE.search(str(instr)).group(1)
            self._declarations[key] = key

    def _get_var(self, instr):
        # TODO opcode objects
        if instr.opcode_name in ["load", "cmpxchg"]:
            op = instr.operands[0]
        elif instr.opcode_name == "store":
            op = instr.operands[1]
        elif instr.opcode_name == "call":
            called = instr.called_function

            if called:
                return called.name

            try:
                op = instr.operands[1]
            except TypeError as e:
                # NOTE bug in LLVM py
                return "inline-asm"
        elif instr.opcode_name == "br":
            return ""

        # if the name is available use it straight awayg
        if op.name:
            return op.name

        indices = []
        while op.operand_count > 0:
            var = self._LLVM_ID_RE.search(str(op))
            call = self._LLVM_CALL_ID_RE.search(str(op))

            # http://llvm.org/docs/GetElementPtr.html
            # we need to calculuate the offsets to differentiate
            # struct and array variables in the store/load ops
            if re.search(r'getelementptr', str(op)):
                # the array index of the first operand, 0 in many cases, see doc link
                for index in op.operands[1:]:
                    indices.append(self._INTEGER_VAL_RE.search(str(index)).group(1))

            if var and var.group(1) in self._declarations:
                name = self._declarations[var.group(1)]
                index_chain = "_".join(indices)

                # append the offsets to the name to differentiate
                # array element and struct pointer access
                return (name + "_" + index_chain if index_chain else name)

            if call and instr.opcode_name == "call":
                return call.group(1)

            op = op.operands[0]

    def _set_vertex_attributes(self, vertex, instr, variable=None, event=None):
        # kind "dbg"
        # http://llvm.org/docs/doxygen/html/LLVMContext_8cpp_source.html
        # http://llvm.org/docs/SourceLevelDebugging.html
        metadata = instr.get_metadata('dbg')

        # set the event as the opcode
        self._set_prop('instruction', vertex, instr)

        event = event or instr.opcode_name

        self._set_prop('event', vertex, event)

        variable = variable or self._get_var(instr) or "!!NONE FOUND!!"

        self._set_prop('variable', vertex, variable)

        if metadata:
            line = self._METADATA_LINE_RE.match(str(metadata)).group(1)
            self._set_prop('line', vertex, line)
            self._set_prop('label', vertex, event, variable, line)
        else:
            self._set_prop('label', vertex, event, variable)


    # keep the internal copy unchanged from outside refs
    def graph(self):
        return self._graph.copy()
