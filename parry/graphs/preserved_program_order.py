from graph_tool.topology import shortest_path
from graph_tool import Graph
from sets import Set

import util.graph as gutil

class PPOGraph():
    def __init__(self, graph, arch, clear=None):
        self._graph = graph.copy()
        self._arch = arch

        # TODO decide on using a property or method
        self._ppo_graph = self._graph.copy()
        gutil.eprop(self._ppo_graph, 'program_order', 'bool')
        gutil.eprop(self._ppo_graph, 'preserved_program_order', 'bool')
        gutil.eprop(self._ppo_graph, 'color', 'string')

        if clear:
            self._ppo_graph.clear_edges()
        else:
            # mark the existing edges as program order edges
            for e in self._ppo_graph.edges():
                self._ppo_graph.edge_properties['program_order'][e] = True

                # TODO for debugging only
                self._ppo_graph.edge_properties['color'][e] = 'red'

        self._ppo_edges = Set([])
        self._pathed = {}
        self._no_path = Set([])
        for v in self._graph.vertices():
            self._pathed[v] = Set([])

        # TODO move out to a public method that users call
        self._build()

    def _build(self):
        ppo_edges = []
        reverse = list(self._graph.vertices())
        reverse.reverse()

        # for each order collect the PPO edges
        for n in self._graph.vertices():
            # ideally we want nodes "further away" so our path
            # caching will pick up nodes we've already pathed/seen
            for p in reverse:
                if n == p:
                    continue

                self._add_ppo(n, p)

        for s, t in self._ppo_edges:
            edge = self._ppo_graph.add_edge(s,t)
            self._ppo_graph.edge_properties['preserved_program_order'][edge] = True
            self._ppo_graph.edge_properties['program_order'][edge] = False
            self._ppo_graph.edge_properties['color'][edge] = 'black'
            # todo add `constraint=false` to make the po edges hold the graph nodes

    # keep the internal copy unchanged from outside refs
    def graph(self):
        return self._ppo_graph.copy()

    # get the event type of a node to match against the architecture
    def _get_event_type(self, v):
        return self._graph.vertex_properties["event"][v]

    # get the event type of a node to match against the architecture
    def _get_var(self, v):
        return self._graph.vertex_properties["variable"][v]

    def _path_exists(self, fst, snd):
        # if we've already established that there's no path
        # between fst and snd then we can skip this check
        if (fst, snd) in self._no_path:
            return False

        # if we've already seen snd in a path from fst to some other node
        # then we already know there's a path from fst to snd
        if snd not in self._pathed[fst]:
            (vs, es) = shortest_path(self._graph, fst, snd)

            self._pathed[fst] = self._pathed[fst].union(Set(vs))

            if len(vs) == 0:
                # if there's no path between fst and snd we know that there's
                # no path between any nodes down a path from fst to snd
                # otherwise there would be a path in the first place.
                #
                # record these facts and return early above
                self._no_path.add((fst,snd))

                for p in self._pathed[fst]:
                    self._no_path.add((p,snd))

                return False

        return True


    def _add_ppo(self, fst, snd):
        if not self._path_exists(fst, snd):
            return

        # TODO move to the top of the method to see if we should even bother
        # verifying a path in the first place, faster for weaker archs
        fst_event = (self._get_event_type(fst), self._get_var(fst))
        snd_event = (self._get_event_type(snd), self._get_var(snd))

        if self._arch.is_preserved(fst_event, snd_event):
            # using a set here because we will encounter nodes where
            # two different paths converge and we already have ppo edges
            # from common ancestor nodes
            self._ppo_edges.add((fst, snd))
