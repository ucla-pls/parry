import ctypes.util
from clang.cindex import TranslationUnit, CursorKind, Config

# NOTE the shared object location set in clang doesn't work, set it here
libclang_so = ctypes.util.find_library('clang')
Config.set_library_file(libclang_so)

class Finder:
    def __init__(self, file_path):
        self._file_path = file_path
        self._trans_unit = TranslationUnit.from_source(file_path)

    def find_labels(self, prefix):
        labels = []
        # top level definitions like includes and functions
        for cursor in self._trans_unit.cursor.get_children():

            if not cursor.location.file:
                continue

            # Ignore AST elements not from source file (e.g.from included files)
            if cursor.location.file.name != self._file_path:
                continue

            labels.extend(self._find(cursor, CursorKind.LABEL_STMT, []))

        return [l for l in labels if prefix == l['name'][:len(prefix)]]

    def _find(self, cursor, kind, cursors):
        for child in cursor.get_children():
            if child.kind == kind:
                cursors.append({
                    'line': child.location.line,
                    'name': child.displayname
                })

            self._find(child, kind, cursors)

        return cursors
