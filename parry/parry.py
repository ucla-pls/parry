import sys
import itertools
import click
import re
import fileinput
import glob

import subprocess
import graph_tool
import os
import tempfile
import shutil

# from parry
from graph import Graph
from order import Order
from finder import Finder
from graphs.preserved_program_order import PPOGraph
from graphs.program_order import POGraph
from architectures import mapping as arch_mapping

import util.orders as outil
import util.debug as dutil
import opt

@click.command()
@click.argument('method')
@click.argument('source')
@click.argument('orders')
@click.argument('arch')
@click.option('--elim', default="full", help="order elimination algorithm, {full, direct}" )
@click.option('--fence', default="ilp", help="fence insertion algorithm, {ilp, bounded}" )
@click.option('--output', type=click.File('w'), default=sys.stdout, help="an output file")
@click.option('--debug', default="false", help="debug output, {all, graph, perf}")
@click.option('--ccflags', default="", multiple=True, help="compiler options for IR generation")
@click.option('--altindex', default=0, help="index of alternate 'equivalent' fence placements")
@dutil.timer
def run(method, source, orders, arch, elim, fence, output, debug, ccflags, altindex):
    """Parry compiles the <source> file to LLVM IR and uses the <orders>
    and <arch> to optimally insert fences in <method>"""

    # turn debugging on or off depending on the flag
    dutil.ON = (not debug == "false")
    dutil.GRAPH_ON = (debug == "graph")

    dutil.debug("method: %s" % method)
    dutil.debug("orders: %s" % orders)

    # parse the orders
    try:
        tuple_orders = outil.parse_orders(orders)
    except Exception as e:
        write(sys.stdout, e.message)
        write(sys.stdout, "<orders> should take the form `1:store-2:load,3:load-4:load`\n")
        sys.exit(1)

    # map the arch string to an arch object
    arch_obj = arch_mapping[arch.upper()]

    # create a temporary working dir for the compiler subprocesses
    work_dir_path = tempfile.mkdtemp(dir="/tmp")

    # create path to output the compiled LLVM IR to
    ir_path = os.path.join(work_dir_path, "ir.ll")

    compile_src(source, ir_path, " ".join(ccflags))

    dutil.debug_file(ir_path, "scratch/ir.ll")

    # run produce a cfg
    # opt -dog-cfg <ir>
    # NOTE stderr is redirected to dev null to prevent logging from opt
    subprocess.check_call(['opt', '-dot-cfg', ir_path],
        cwd=work_dir_path, stderr=open(os.devnull, 'w'), stdout=open(os.devnull, 'w'))

    # grab the IR output for the method under consideration
    # TODO will throw a useless exception if no file is found

    files = find_file(os.path.join(work_dir_path, "cfg.%s.dot" % method))

    if not files:
        files = find_file(os.path.join(work_dir_path, "cfg.*%s*.dot" % method))

    if not files:
        sys.exit(1)

    method_cfg_path = files[0]

    # clean up the method cfg dot file so graph_tool doesn't throw an exception
    clean_dot_cfg(method_cfg_path, method)

    # create the program order graph from the cfg and the ir
    po = create_po_graph(method_cfg_path, method, ir_path)

    # remove the working directory
    shutil.rmtree(work_dir_path)

    # optimize fence placment
    fences = opt.opt(po, tuple_orders, arch_obj, elim, fence, altindex)

    # swap the fence mapping, so that each line has a fence type
    line_mapping = {}
    for fence_type, lines in fences.items():
        for line in lines:
            line_mapping[line] = fence_type

    # TODO sort out why the output is converted to binary unless a
    # file is explicitily used, happens due to above check_calls
    # open the original source and write to stdout with the fence instructions
    with open(source) as source_file:
        for i, line in enumerate(source_file):
            if not write(output, line):
                break

            # enumerate is zero based, line numbers are one based
            if not (i+1) in line_mapping.keys():
                continue

            asm = arch_obj.fences[line_mapping[i+1]]

            if not write(output, asm_wrap(asm)):
                break

@click.command()
@click.option('--label-prefix', default="ord", help="prefix to id order labels")
@click.option('--graph', default="false", help="print dot graph to stdout")
@click.option('--fence', default="/* FENCE */", help="fence expression")
@click.argument('file_path')
def old_run(label_prefix, graph, fence, file_path):
    labels = Finder(file_path).find_labels(label_prefix)

    # TODO make the postfix regex an option
    for l in labels:
        l['name'] = l['name'].replace(label_prefix + "_" , '')
        l['name'] = re.sub(r'_[0-9]+$', '', l['name'])

    pairs = itertools.combinations(labels, 2)
    pairs = filter(lambda (fst, snd): fst['name'] == snd['name'], pairs)

    order_graph = Graph()
    for fst, snd in pairs:
        order_graph.append(Order(fst['line'], snd['line'], fst['name']))

    # TODO print the mincut with the graph, make it an option
    if graph == "true":
        sys.stdout.write(order_graph.dot_graph())
        return 0

    mincut = order_graph.mincut()

    count = 1
    for line in open(file_path, 'r'):
        if count in mincut:
            stdout.write(fence + "\n")

        stdout.write(line)
        count += 1

    return 0

# catch sigints
def write(output_file, line):
    try:
        output_file.write(line)# arch_obj.fences[line_mapping[i]])
        return True
    except IOError:
        return False

def asm_wrap(asm):
    return "__asm__ __volatile__ (\"%s\" ::: \"memory\");\n" % asm

@dutil.timer
def clean_dot_cfg(path, method):
    # TODO both of these sed calls can be done in python and probably should be
    # clean the digraph name, causes issues with graph_tool import
    # sed -i'' "s/digraph.*{/digraph \"$method\" {/" \ "examples/tl2/$tl2_type/cfg/$method.dot"
    subprocess.check_call([
        "sed",
        "-i''",
        "s/digraph.*{/digraph \"%s\" {/" % method,
        path
    ])

    # remove the useless label which also causes issues with the graph_tool import
    # sed -i'' 's/^\slabel=.*//' "examples/tl2/$tl2_type/cfg/$method.dot"
    subprocess.check_call([
        "sed",
        "-i''",
        "s/^\slabel=.*//",
        path
    ])

@dutil.timer
def compile_src(source, output, flags):
    # TODO take compiler flags so we can test things like TL2_EAGER
    # compile to llvm ir
    # clang <source> -S -g -emit-llvm -o <ir>
    dutil.debug("ir file: %s" % output)
    dutil.debug("cmd: %s" %
        " ".join(['clang']
        + flags.split(" ")
        + [source, '-S', '-g', '-emit-llvm', '-o', output])
    )

    subprocess.check_call(
        ['clang']
        + flags.split(" ")
        + [source, '-S', '-g', '-emit-llvm', '-o', output],
        stderr=open(os.devnull, 'w'))

@dutil.timer
def create_po_graph(method_cfg_path, method, ir_path):
    # TODO close this explicitly
    with open(method_cfg_path) as cfg:
        cfg_graph = graph_tool.load_graph(cfg, fmt='dot')

        # create a program order graph with the ir and method cfg
        po = POGraph(cfg_graph, str(method), open(ir_path)).graph()

    return po


def find_file(file_glob):
    files = glob.glob(file_glob)

    # if the file name doesn't match files generated by opt
    if not files:
        write(sys.stdout, "Could not find a file with the name `%s`\n" % str(file_glob))
        write(sys.stdout, "Will try fuzzy match.\n")

    return files


if __name__ == '__main__':
    run()
