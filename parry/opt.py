from sage.all import MixedIntegerLinearProgram
import sys
from sys import stderr
import time

# make sure sage can find the system packages like graph_tool and llvm
sys.path.append( '/usr/local/lib/python2.7/dist-packages/' )
sys.path.append( '/usr/lib/python2.7/dist-packages/' )

# graph tool deps
from graph_tool import load_graph
from graph_tool.topology import shortest_path

# ppo graph generation
from graphs.preserved_program_order import PPOGraph

# algorithms
from algorithms.bounded_radius_cut import bounded_radius_cut
from algorithms.heavy_cycles import deter_cycle_cut
from algorithms.subgraph import subgraph
from algorithms.order_elimination.direct_link import direct_link_elimination
from algorithms.order_elimination.full import full_elimination
from algorithms.union_all import union_all

# helper procedure namespace
import util.graph as gutil
import util.orders as outil
import util.debug as dutil

@dutil.timer
def opt(graph, line_orders, arch_obj, elim='full', fence='ilp', altindex=0):
    orders = arch_obj.filter_orders(outil.group_orders(line_orders))

    # set identifiers so we can reference the removed edges
    gutil.set_identifiers(graph)

    # add the capacity prop so that we can weight the cycles
    graph = deter_cycle_cut(graph)

    dutil.debug_graph(graph, "scratch/all.dot")
    dutil.debug("graph node count: %d" % len(list(graph.vertices())))

    cuts = []
    nodes = {}
    last = None
    dutil.debug("orders %s" % orders)
    dutil.debug("matched orders %s" % arch_obj.matched_orders(orders))
    for order_type in arch_obj.matched_orders(orders):
        # TODO can't remove the edge have to insert a fence
        # since our partial orders are total orders for evaluation we just proceed here
        if last:
            gutil.remove_edges(graph, last, purge=True)

        order_group = { order_type: orders[order_type] }

        # TODO must include all orders that *could be* satsified by the current fence
        last, fenced_graph = opt_sg(graph.copy(), order_group, arch_obj, elim, fence, altindex)
        cuts += last

        # match the edges so we can find the right node to place the fence after
        for i,e in enumerate(last):
            last[i] = gutil.find_edge(graph, fenced_graph, e)

        # find the right node to place the fences after
        # eg, if the fenc should come after the top of a loop
        # we want to put the fence after the loop and not as its first instruction
        nodes[order_type] = select_nodes(last, graph, orders)

    dutil.debug("fence after nodes: %s" % nodes)
    return nodes

# TODO kwargs, and a majore refactor
def opt_sg(graph, line_orders, arch_obj, elim='full', fence='ilp', altindex=0):
    # make sure to grab the graphs line and event props for checking
    # translate the line based orders into instruction orders
    orders = check_orders(graph, line_orders)

    # if there are no orders present in the graph
    if not orders:
        dutil.debug("no orders retained, exiting ...")
        return ([], graph)

    subgraphs = order_subgraphs(graph, orders)

    # eliminate orders where possible
    final_graphs = (dl_elim if elim == "direct" else full_elim)(subgraphs, arch_obj)

    # if all the graphs are eliminated then we're done
    if not final_graphs:
        dutil.debug("all paths elimintated, no subgraphs to fence, exiting ...")
        return ([], graph)

    dutil.debug_graphs(final_graphs, "scratch/sg-%d.dot")

    # get the new source and sink nodes back from the subgraphs
    orders = find_orders(final_graphs)

    # union the graphs that made it through elimination
    graph = union_all(final_graphs)

    dutil.debug_graph(graph, "scratch/union.dot")

    # final orders before fence insertion
    # TODO lots of overlap with find_orders above
    orders = final_orders(graph, orders)

    # get the `altindex` edge of a sequence of equally viable edges
    # default is the first (zero index) edge
    # TODO loop until line changes and not edges
    for i in range(0, altindex+1):
        dutil.debug_graph(graph, "scratch/alt-%d.dot" % i)
        distances = lp(graph, orders, fence)

        for edge, distance in distances.iteritems():
            if distance > 0.0:
                graph.ep['capacity'][edge] = (graph.ep['capacity'][edge] or 1) + 1

    # record the distance properties
    graph.edge_properties['distance'] = graph.new_edge_property('float')

    # record the cut
    cut = []
    for edge, distance in distances.iteritems():
        graph.edge_properties['distance'][edge] = distance

        if distance > 0.0:
            cut.append(edge)

    lines = graph.vp['line']
    noline = [lines[s] == '' or lines[t] == '' for s,t in cut]
    if any(noline):
        raise Exception('one node in the cut has no line number')

    # if we ran ILP skip the bounded radius cut
    if not fence == 'ilp':
        # run the bounded radius cut on our LP output
        cut, graph = bounded_radius_cut(graph, orders, "line")

    # return the final cut and the corresponding graph
    return (cut, graph)

@dutil.timer
def dl_elim(order_subgraphs, arch_obj):
    dutil.debug("using direct link elim")

    # create preserved program order edges for each subgraph
    # ppo_graphs = create_ppo_subgraphs(order_subgraphs, arch_obj)

    # eliminate all paths on the subgraph
    final_graphs = []
    for i, ppo in enumerate(order_subgraphs):
        s, t = (gutil.get_source(ppo), gutil.get_sink(ppo))

        # remove the program order edges so paths are through ppo edges only
        # NOTE there is no purge here so the edges are just hidden from
        #      the direct link elimination
        graph, removed = direct_link_elimination(ppo, s, t, arch_obj)

        # disconnect the vertices that can be disconnected
        gutil.disconnect_vertices(graph, removed, purge=True)

        dutil.debug_graph(graph, "scratch/dl-elim-sg-after-%s.dot" % i)

        # remove the preserved program order edges so paths are through
        # program order edges only
        # gutil.remove_edges_prop(graph, keep_prop='program_order', purge=True)

        # If there's still a path between the source and the sink *on po edges*
        (vertices, edges) = shortest_path(graph, s, t)

        if edges:
            final_graphs.append(graph)

    return final_graphs

@dutil.timer
def full_elim(order_subgraphs, arch_obj):
    dutil.debug("using full order elim")
    final_graphs = []
    for i, sg in enumerate(order_subgraphs):
        s, t = (gutil.get_source(sg), gutil.get_sink(sg))

        final_paths = full_elimination(sg.copy(), s, t, arch_obj)

        # if there are any paths left we must keep the subgraph
        if final_paths:
            final_graphs.append(sg)
            dutil.debug_graph(sg, "scratch/full-%i.dot" % i)

    return final_graphs

# handle edges that have a source at a branch like at the top of a loop
# also handles edges on the same line as the sink
def select_nodes(cut, graph, orders):
    capacity = graph.ep['capacity']
    lines = []
    source_lines = []
    sink_lines = []

    for ords in orders.values():
        for s,t in ords:
            sink_lines.append(t)
            source_lines.append(s)

    for e in cut:
        out_edges = list(e.source().out_edges())

        # one edge, just use the source
        if len(out_edges) == 1:
            line = int(graph.vp['line'][out_edges[0].source()])

            if line in sink_lines and line not in source_lines:
                lines.append(line - 1)
            else:
                lines.append(line)

            continue

        # if they're all the same capacity then it's just a branch
        # trust the cut but choose the sink of the edge for the line before
        if all(map(lambda x: capacity[x] == capacity[out_edges[0]], out_edges)):
            lines.append(int(graph.vp['line'][e.target()]) - 1)
            continue

        # different capacities, choose the least so that it's "out" of a loop
        # use the line number just before the node
        least = out_edges[0]
        for out in out_edges:
            if capacity[least] > capacity[out]:
                least = out

        lines.append(int(graph.vp['line'][least.target()]) - 1)
        dutil.debug(str(lines))

    return lines

# gather the actual graph orders using the line numbers defined above
# NOTE this might include many orders from around the graph due to
# inlining of methods where parts of whole orders have been declared
def check_orders(graph, line_orders):
    line_prop = gutil.vprop(graph, 'line', 'str')
    event_prop = gutil.vprop(graph, 'event', 'str')

    dutil.debug_graph(graph, "scratch/check_orders.dot")

    orders = []
    for s_event, t_event in line_orders:
        for (s, t) in line_orders[(s_event, t_event)]:
            sources = []
            sinks = []
            found = False

            # due to inlining there might be multiple occurences of
            # a given vertex within the graph, collect all of them
            # but restrict them to the type we expect if it is defined
            for v in graph.vertices():
                if compare_vertex_order(graph, v, s, s_event):
                    sources.append(v)

                if compare_vertex_order(graph, v, t, t_event):
                    sinks.append(v)

                # calls,any are always retained
                if line_prop[v] == str(s) and event_prop[v] in ["call"]:
                    sources.append(v)

                # calls,any are always retained
                if line_prop[v] == str(t) and event_prop[v] in ["call"]:
                    sinks.append(v)


            # for all the source and sink pairs that match the order
            # check for a path between any of them, on the first path proceed
            for source in sources:
                for sink in sinks:
                    # ensure there's a path between the source and sink in the graph
                    (vertices, edges) = shortest_path(graph, source, sink)

                    if len(edges) > 0:
                        found = True
                        orders.append((source, sink))

            if not found:
                raise Exception("order (%s:%s, %s:%s) not found in the graph" % (s, s_event, t, t_event))
            else:
                dutil.debug("retaining %s,%s" % (s,t))

    return orders

def compare_vertex_order(graph, v, i, i_event):
    line_prop = gutil.vprop(graph, 'line', 'str')
    event_prop = gutil.vprop(graph, 'event', 'str')

    # line prop must match the line
    # event prop must match the event OR
    # any OR
    # no event OR
    # has to be a store and there's a cmpxchg or no event or any
    return (line_prop[v] == str(i) and
            (event_prop[v] == i_event or
             event_prop[v] == "call" or
             i_event == "any" or
             not i_event or
             (event_prop[v] == "cmpxchg" and i_event == "store")))

@dutil.timer
def lp(graph, orders, algo="ilp"):
    # Run the LP solution for mincut
    # Construct the linear program for multicut
    pr = MixedIntegerLinearProgram(maximization=False)

    d = pr.new_variable(real=(not algo == "ilp"), nonnegative=True)
    p = pr.new_variable(real=(not algo == "ilp"), nonnegative=True)

    for e in graph.edges():
        s = e.source()
        t = e.target()

        for k in orders:
            pr.add_constraint(d[e] >= p[(k,s)] - p[(k,t)])

    for s,t in orders:
        pr.add_constraint(p[(k,s)] - p[(k,t)] >= 1)

    capacity = gutil.eprop(graph, 'capacity', 'int')
    pr.set_objective(sum(d[e] * (capacity[e] or 1) for e in graph.edges()))
    pr.solve()

    return pr.get_values(d)

def order_subgraphs(graph, orders):    # create source to sink subgraphs for each order
    subgraphs = []
    for i, (s,t) in enumerate(orders):
        working = graph.copy()

        # set the source and the sink for retrieval later
        gutil.set_source(working, s)
        gutil.set_sink(working, t)

        sg = subgraph(working, s, t, purge=True)
        s, t = (gutil.get_source(sg), gutil.get_sink(sg))

        dutil.debug_graph(sg, "scratch/before-%d.dot" % i)

        subgraphs.append(sg)

    return subgraphs

def find_orders(final_graphs):
    orders = []
    for i, graph in enumerate(final_graphs):
        # find the source and the sink set in the order_subgraphs method
        source = gutil.get_source(graph)
        sink = gutil.get_sink(graph)

        orders.append((gutil.get_source(graph), gutil.get_sink(graph), graph))

    return orders

def final_orders(graph, orders):
    final_orders = []
    for s,t,g in orders:
        # find the source vertex in the unioned graph
        s = gutil.find_vertex(graph, g, s)

        # find the sink vertex in the unioned graph
        t = gutil.find_vertex(graph, g, t)

        # We found the nodes and they have neighbours (not isolated)
        if s and t and list(s.all_neighbours()) and list(t.all_neighbours()):
            gutil.set_source(graph, s)
            gutil.set_sink(graph, t)
            final_orders.append((s,t))

    return final_orders
