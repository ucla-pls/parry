EDGE_FMT = "%d -> %d;"

TERMINALS_FMT = """
  %d [label="%s" shape=doublecircle fontsize=14];
  %d [label="%s" shape=doublecircle fontsize=14];"""

POINT_FMT = """
  %d [xlabel=\"%d\" shape=point fontsize=9];"""

GRAPH_FMT = """digraph %s {
  {
    %s
  }

  %s
}"""
