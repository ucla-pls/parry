from const import EDGE_FMT, TERMINALS_FMT, POINT_FMT

class Order:
    def __init__(self, start, finish, name):
        self._rng = rng = range(start, finish + 1)

        # create pairs from the range of line numbers
        self._pairs = [(num, num + 1) for num in rng if (num + 1 in rng)]

        self._terminals = rng[0], rng[-1]

        self._name = name

    def dot_graph(self):
        return "digraph G { %s }" % self.dot_edges()

    def dot_edges(self):
        edges = [EDGE_FMT % p for p in self._pairs]
        return " ".join(edges)

    def dot_terminals_style(self):
        return TERMINALS_FMT % (self._rng[0], self._name, self._rng[-1], self._name)

    def dot_points_style(self):
        return " ".join(reduce(self._point_fmt, self._rng, []))

    def _point_fmt(self, styles, point):
        if point != self._rng[0] and point != self._rng[-1]:
            styles.append(POINT_FMT % (point, point))

        return styles

    def name(self):
        return self._name

    def pairs(self):
        return self._pairs

    def __str__(self):
        return str(self._terminals)

    # sort order dictated by greedy chain algorithm
    def __cmp__(self, other):
        return self._rng[-1].__cmp__(other._rng[-1])

    def __getitem__(self, key):
        return self._rng[key]
