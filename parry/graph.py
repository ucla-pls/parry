from const import EDGE_FMT, GRAPH_FMT

class Graph:
    def __init__(self, orders=[], name="tmp"):
        self._orders = orders
        self.name = name

    def dot_graph(self):
        # NOTE that all the terminal styles have to come second so that
        #      they override the point styles
        styles = []
        for order in self._orders:
            styles.append(order.dot_points_style())

        for order in self._orders:
            styles.append(order.dot_terminals_style())

        edges = "".join(map(lambda e: EDGE_FMT % e, self.merged_orders()))
        style = "".join(styles)

        return GRAPH_FMT % (self.name, style, edges)

    def merged_orders(self):
        pairs = []

        # flat map the pairs
        for order in self._orders:
            pairs.extend(order.pairs())

        # make them uniq
        uniq_pairs = list(set(pairs))

        # sort them again
        uniq_pairs.sort()

        return uniq_pairs

    def mincut(self):
        return reduce(self._add_cut, self._sort_orders(), [])

    def _add_cut(self, cuts, order):
        if len(cuts) == 0 or cuts[-1] <= order[0]:
            cuts.append(order[-1])

        return cuts

    def sort(self):
        return Graph(self._sort_orders(), self.name)

    def _sort_orders(self):
        dup = list(self._orders)
        dup.sort()
        return dup

    def orders(self):
        return self._orders

    def append(self, order):
        self._orders.append(order)
