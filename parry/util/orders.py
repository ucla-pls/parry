import sets

ORDER_SPLIT=","
PAIR_SPLIT="-"
INSTR_SPLIT=":"

#e.g. `1:store,3:load;10:load,15:store`
def parse_orders(order_str):
    pairs = order_str.split(ORDER_SPLIT)

    # use a set to account for duplicate ordersg
    orders = sets.Set()
    for pair in pairs:
        s_t = pair.split(PAIR_SPLIT)
        size_check(s_t, "invalid pair: " + pair)
        s, t = s_t

        s_line, s_event = instr_pair(s)
        t_line, t_event = instr_pair(t)

        orders.add(((int(s_line), s_event), (int(t_line), t_event)))

    return list(orders)

def size_check(checked, msg):
    if not len(checked) == 2:
        raise InvalidOrderException(msg)


def instr_pair(instr_str):
    line_event = instr_str.split(INSTR_SPLIT)
    size_check(line_event, "invalid line,event: " + instr_str)
    return line_event

class InvalidOrderException(Exception):
    pass

# expects        [((int, "store|load"), (int, "store|load"))]
# produces a map {("store|load", "store|load"), [(int, int)]}
def group_orders(orders):
    groups = {}
    for (l1, e1), (l2, e2) in orders:
        key = (e1, e2)
        if not key in groups:
            groups[key] = []

        groups[(e1, e2)].append((l1, l2))

    return groups
