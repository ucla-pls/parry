from sys import stdout
import util.graph as gutil
import time
import shutil

DEBUG_PREFIX="DEBUG: "
ON=False
GRAPH_ON=False

def debug(msg):
    if not debugging():
        return

    stdout.write("%s %s \n" % (DEBUG_PREFIX, msg))

def debug_graph(graph, file_name):
    if not graph_debugging():
        return

    debug("printing graph to: " + file_name)
    # TODO should probably check if this edge prop exists
    gutil.edge_label(graph, ['capacity'])
    gutil.save_without_props(graph, file_name)

def debug_graphs(graph_list, file_format):
    for i,g in enumerate(graph_list):
        debug_graph(g, file_format % i)

def debug_file(path, destination):
    debug("copying `%s` to `%s`" % (path, destination))
    shutil.copyfile(path, destination)

def timer(fn):
    def time_wrapper(*args, **kwargs):
        start = time.clock()
        result = fn(*args, **kwargs)
        end = time.clock()
        debug("procedure %s took %s seconds" % (fn.func_name, end - start))
        return result

    return time_wrapper

def debugging():
    return ON

def graph_debugging():
    return GRAPH_ON
