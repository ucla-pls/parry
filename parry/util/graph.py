from sets import Set

SOURCE='__source__'
SINK='__sink__'
ID='__identifier__'
KEEP='__keep__'

def set_source(graph, s, color="green"):
    source_prop = vprop(graph, SOURCE, 'string')

    vprop(graph, 'color', 'string')
    graph.vertex_properties['color'][s] = color

    source_prop[s] = 'source'

def get_source(graph):
    source_prop = vprop(graph, SOURCE, 'string')

    # TODO checking the keys of the map directly didn't workb
    for v in graph.vertices():
        if source_prop[v] == 'source':
            return v

def set_sink(graph, t, color="red"):
    sink_prop = vprop(graph, SINK, 'string')

    vprop(graph, 'color', 'string')
    graph.vertex_properties['color'][t] = color

    sink_prop[t] = 'sink'

def get_sink(graph):
    sink_prop = vprop(graph, SINK, 'string')

    # TODO checking the keys of the map directly didn't work
    for v in graph.vertices():
        if sink_prop[v] == 'sink':
            return v

def set_identifiers(graph):
    vid_prop = vprop(graph, ID, 'int')
    eid_prop = eprop(graph, ID, 'int')

    for v in graph.vertices():
        vid_prop[v] = graph.vertex_index[v]

    for e in graph.edges():
        eid_prop[e] = graph.edge_index[e]

def reset_identifiers(graph):
    vid_prop = vprop(graph, ID, 'int')
    eid_prop = eprop(graph, ID, 'int')

    for v in graph.vertices():
        vid_prop[v] = -1

    for e in graph.edges():
        eid_prop[e] = -1

def has_vertex(graph, old, v):
    vid_prop = vprop(graph, ID, 'int')
    old_vid_prop = vprop(old, ID, 'int')

    return (old_vid_prop[v] in vid_prop.get_array())

def has_edge(graph, old, e):
    eid_prop = eprop(graph, ID, 'int')
    old_eid_prop = eprop(old, ID, 'int')

    return (old_eid_prop[e] in eid_prop.get_array())


def vprop(graph, name, ptype):
    if name in graph.vertex_properties:
        return graph.vertex_properties[name]

    prop = graph.vertex_properties[name] = graph.new_vertex_property(ptype)
    return prop

def eprop(graph, name, ptype):
    if name in graph.edge_properties:
        return graph.edge_properties[name]

    graph.edge_properties[name] = graph.new_edge_property(ptype)
    return graph.edge_properties[name]

def remove_vertices(graph, remove, purge=False, debug=False):
    vprop(graph, KEEP, 'bool')

    for v in graph.vertices():
        graph.vertex_properties[KEEP][v] = (v not in remove)

    vertex_filter(graph, KEEP)

    if purge:
        graph.purge_vertices()
        del graph.vertex_properties[KEEP]
        graph.clear_filters()

def disconnect_vertices(graph, disconnect, purge=False):
    edges = Set([])

    for v in disconnect:
        edges = edges.union(Set(v.in_edges())).union(Set(v.out_edges()))

    remove_edges(graph, edges, purge=purge)

def remove_edges(graph, remove, purge=False):
    keep_prop=KEEP

    eprop(graph, keep_prop, 'bool')

    for e in graph.edges():
        graph.edge_properties[keep_prop][e] = (e not in remove)

    remove_edges_prop(graph, keep_prop, purge=purge)

    del graph.edge_properties[KEEP]

def remove_edges_prop(graph, keep_prop, purge=False):
    if purge:
        # NOTE graph.purge_edges() is throwing segfaults so we do it manually
        for e in list(graph.edges()):
            if not graph.edge_properties[keep_prop][e]:
                graph.remove_edge(e)
    else:
        edge_filter(graph, keep_prop)

def vertex_filter(graph, prop):
    graph.set_vertex_filter(graph.vertex_properties[prop])

def edge_filter(graph, prop):
    graph.set_edge_filter(graph.edge_properties[prop])

def print_edge(graph, s, t, e, distance):
    capacity = graph.edge_properties['capacity'][e]
    print "capacity: %s" % capacity
    print "distance: %s" % distance

    print "s:"
    print_vertex(graph, s)

    print "t:"
    print_vertex(graph, t)

def print_vertex(graph, v):
    print "variable: %s " % graph.vp['variable'][v]
    print "line:     %s" % graph.vp['line'][v]
    print "event:    %s" % graph.vp['event'][v]

def edge_label(graph, properties):
    eprop(graph, 'label', 'string')

    for e in graph.edges():
        graph.ep['label'][e] = ""
        for p in properties:
            val = str(graph.ep[p][e])
            label = graph.ep['label'][e]
            graph.ep['label'][e] =  label + " " + val

# TODO default to boolean properties since they prevent dot file
# conversion to pngs
def save_without_props(graph, file_name, props=None):
    # avoid replacing falsy empty list
    if props == None:
        props = [
            'program_order',
            'preserved_program_order',
            'instruction'
        ]

    graph_copy = graph.copy()
    for prop in props:
        if prop in graph_copy.ep:
            del graph_copy.ep[prop]

    for prop in props:
        if prop in graph_copy.vp:
            del graph_copy.vp[prop]

    graph_copy.save(file_name)

# find a vertex by id
def find_vertex(graph, other, v):
    for vert in graph.vertices():
        if graph.vp[ID][vert] == other.vp[ID][v]:
            return vert

def find_edge(graph, other, e):
    for edge in graph.edges():
        if graph.ep[ID][edge] == other.ep[ID][e]:
            return edge
