from graph_tool import Graph
import util.graph as gutil

def union_all(graphs):
    # retain the properties of the graph
    result = graphs[0].copy()
    result.clear()

    # keep the other properties
    del result.vp[gutil.ID]
    gutil.vprop(result, gutil.ID, 'int')

    del result.ep[gutil.ID]
    gutil.eprop(result, gutil.ID, 'int')

    # TODO move this somewhere else, bug of retained capacity values
    del result.ep['capacity']
    gutil.eprop(result, 'capacity', 'int')

    for old in graphs:
        for v in old.vertices():
            if gutil.has_vertex(result, old, v):
                other_v = gutil.find_vertex(result, old, v)

                # copy the vertex properties
                copy_vertex_properties(result, old, other_v, v)

                continue

            new = result.add_vertex()

            copy_vertex_properties(result, old, new, v)

        for e in old.edges():
            if gutil.has_edge(result, old, e):
                # NOTE bug with graph_tool
                # check that the edge actuall exists, graph tool
                # doesn't clean up the edge property properly
                dup = gutil.find_edge(result, old, e)
                if dup:
                    continue

            # get the old source and sink
            s = gutil.find_vertex(result, old, e.source())
            t = gutil.find_vertex(result, old, e.target())
            new = result.add_edge(s, t)

            # copy the edge properties
            copy_edge_properties(result, old, new, e)

    return result

def copy_vertex_properties(graph, other, l, r):
    # copy the vertex properties
    for prop in graph.vp:
        other_val = other.vp[prop][r]

        if other_val:
            graph.vp[prop][l] = other_val

def copy_edge_properties(graph, other, l, r):
    # copy the vertex properties
    for prop in graph.ep:
        other_val = other.ep[prop][r]

        if other_val:
            graph.ep[prop][l] = other_val
