from graph_tool.topology import shortest_path
from graph_tool.search import bfs_search, BFSVisitor
from sets import Set

def bounded_radius_cut(graph, orders, id_attr="id"):
    cutter = BoundedRadiusCut(graph, orders, id_attr)

    cuts = cutter.cut()
    graph = cutter.graph()

    return (cuts, graph)

# TODO remove
class BoundedRadiusCut():
    def __init__(self, graph, orders, id_attr="id"):
        self._graph = graph
        self._orders = orders
        self._id_attr = id_attr

    def cut(self):
        self._graph = self.fix_graph()
        graph = self._graph.copy()

        # the best cuts from this album :)
        final_cuts = []

        for (s, t) in self._orders:
            # if one of the other cuts already handled this move on
            if not self._path_exists(graph, s, t):
                continue

            cuts = self.candidate_cuts(graph, s, t)
            cut = self._best_cut(cuts)

            # the edges will be removed to prevent paths
            # from sources to sinks so we'll need to get the edges
            # back from a copy of the graph using the vertices
            vertex_indices = self._cut_vertex_indices(cut)

            final_cuts += vertex_indices

            self._make_cut(graph, cut)

            if self._path_exists(graph, s, t):
                raise Exception("the radius should break the paths")

        result_cuts = []
        for s,t in final_cuts:
            result_cuts += self._graph.edge(s,t, all_edges=True)

        return result_cuts

    def graph(self):
        return self._graph

    def candidate_cuts(self, graph, s, t):
        return candidate_cuts(graph, s, t)

    def cut_at_distance(self, graph, s, distance):
        return cut_at_distance(graph, s, distance)

    # rests on the assumption that max distance = 1
    def fix_graph(self):
        return fix_graph(self._graph, self._id_attr)

    def _cut_vertex_indices(self, cut):
        return [(int(str(e.source())), int(str(e.target()))) for e in cut]

    def _path_exists(self, graph, s,t):
        (vertices, edges) = shortest_path(graph, s, t)

        return not (len(edges) == 0)

    def _make_cut(self, graph, cut):
        for e in cut:
            graph.remove_edge(e)

    def _best_cut(self, cuts):
        best_cut = None
        for d, c in cuts:
            if not best_cut:
                best_cut = c

            cut_size = self._cut_size(c)

            if cut_size < self._cut_size(best_cut):
                best_cut = c

        return best_cut

    def _cut_size(self, cut):
        cut_size = 0
        for e in cut:
            cut_size += self._graph.edge_properties['distance'][e]

        return cut_size

class BoundedRadiusVisitor(BFSVisitor):
    def __init__(self, graph, distance):
        BFSVisitor.__init__(self)

        self._graph = graph
        self._distance = distance

        self._cut = []
        self._source = None

    def radius_edges(self):
        return self._cut

    def discover_vertex(self, v):
        if not self._source:
            self._source = v

    def examine_edge(self, edge):
        v = edge.source()

        if v == self._source:
            return

        (vertices, edges) = shortest_path(self._graph, self._source, v)

        # get the distance of the shortest path
        old_distance = 0
        for e in edges:
            old_distance += self._graph.edge_properties['distance'][e]

        # add the edge for analysis
        edges.append(edge)

        # get the distance of the shortest path + the new edge
        new_distance = 0
        for e in edges:
            new_distance += self._graph.edge_properties['distance'][e]

        # if the new distance it too large but the previous was in range
        # then this edge belongs to the cut for the target distance
        if old_distance <= self._distance and new_distance > self._distance:
            self._cut.append(edge)


# rests on the assumption that max distance = 1
def fix_graph(old_graph, id_attr="id"):
    graph = old_graph.copy()

    # for e in the original graph split edges that are too large
    for e in old_graph.edges():
        distance = old_graph.edge_properties['distance'][e]
        label = old_graph.vertex_properties['label'][e.source()]
        attr = old_graph.vertex_properties[id_attr][e.source()]

        if distance <= 1/6:
            continue

        # create four new intervening nodes, 5 to make 6 edges
        new_nodes = []
        new_nodes.append(e.source())

        for i in range(0,5):
            v = graph.add_vertex()

            graph.vertex_properties[id_attr][v] = attr
            graph.vertex_properties['label'][v] = label + (" - split %d" % i)

            new_nodes.append(v)

        new_nodes.append(e.target())

        # for each of the node pair along this new path add an edge
        for i in range(6):
            new_edge = graph.add_edge(new_nodes[i], new_nodes[i+1])
            graph.edge_properties['distance'][new_edge] = (distance/6)

        graph.remove_edge(e)

    return graph

def cut_at_distance(graph, s, distance):
    visitor = BoundedRadiusVisitor(graph, distance)
    bfs_search(graph, s, visitor)

    return visitor.radius_edges()

def candidate_cuts(graph, s, t):
    cuts = []

    # initial target distance
    target_distance = 1.0/3

    # the smallest edge distance (safe to increment by
    # this value without missing edges
    min_distance = _min_edge_distance(graph)

    # find all the cuts
    while target_distance <= 2.0/3:
        cut = cut_at_distance(graph, s, target_distance)
        cuts.append((target_distance, cut))
        target_distance += min_distance

    return cuts

def _min_edge_distance(graph):
    return min(_distances(graph))

def _distances(graph):
    dmap = graph.edge_properties['distance']
    return [dmap[e] for e in graph.edges() if dmap[e] > 0]
