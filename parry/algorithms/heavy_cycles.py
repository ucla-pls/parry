from graph_tool.search import bfs_search, dfs_search, BFSVisitor, DFSVisitor
from graph_tool.topology import shortest_path
import util.graph as gutil

def deter_cycle_cut(graph, init=None, capacity_prop='capacity'):
    gutil.eprop(graph, capacity_prop, 'int')

    init = init or graph.vertex(0)

    cycle_search = CycleSearch()
    dfs_search(graph, init, cycle_search)
    back_edges = cycle_search.back_edges()

    # sort the subgraphs by size so that sub-subgraphs can have correct
    # weights for their cycle edges
    subgraphs = []
    for e in back_edges:

        # get all the edges in the cycle
        subgraph_search = CycleSubgraphSearch(graph, e)

        # NOTE starting at e.target() since e is the back edge
        bfs_search(graph, e.target(), subgraph_search)
        cycle_edges = subgraph_search.cycle_edges()

        subgraphs.append((cycle_edges, e))

    # sort by the number of cycle edgess
    subgraphs = sorted(subgraphs, key=lambda sg: -1 * len(sg[0]))

    for sg,e in subgraphs:
        # grab the in edges from the back edges target (the cycle start)
        in_edges = [in_edge for in_edge in e.target().in_edges()]

        # if there is an in edges grab the capacity
        # otherwise assume 1
        if len(in_edges) > 0:
            # NOTE assumes back edge will not be first in the list
            external_capacity = _get_capacity(graph, in_edges[0])
        else:
            external_capacity = 1

        # increase the external capacity by two
        _increase_cycle_edges(graph, sg, external_capacity)

    return graph

def _get_capacity(graph, e):
    return graph.edge_properties["capacity"][e] or 1

def _set_capacity(graph, e, capacity):
    graph.edge_properties["capacity"][e] = capacity

# doube the capacity and add 1 to ensure the at fences for
# an overlap inside a loop coming from out side the loops are always
# more expensive than two fences outside the loop for the same orders
def _increase_cycle_edges(graph, edges, external_capacity):
    for e in edges:
        _set_capacity(graph, e, 2 * _get_capacity(graph, e) + 1)

class CycleSearch(DFSVisitor):
    def __init__(self):
        DFSVisitor.__init__(self)

        self._back_edges = []
        self._reversed = False

    def back_edges(self):
        if self._reversed:
            return self._back_edges

        self._back_edges.reverse()
        self._reversed = True

        return self._back_edges

    def back_edge(self, e):
        self._back_edges.append(e)


class CycleSubgraphSearch(BFSVisitor):
    def __init__(self, graph, back_edge):
        BFSVisitor.__init__(self)

        self._graph = graph.copy()
        self._back_edge = back_edge
        self._done = False

        self._cycle_edges = []

    def cycle_edges(self):
        if self._back_edge not in self._cycle_edges:
            self._cycle_edges.append(self._back_edge)

        return self._cycle_edges

    def examine_edge(self, e):
        s = e.target()
        t = self._back_edge.source()

        if s == t:
            self._cycle_edges.append(e)
            return

        # find a path from the end of the edge to the start of the back edge
        (vertices, edges) = shortest_path(self._graph, s, t)

        # if the top of the loop is in the path it was round about, skip it
        # if there is no path skip it
        if self._back_edge.target() in vertices or len(edges) == 0:
            return

        self._cycle_edges.append(e)
