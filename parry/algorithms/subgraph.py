from graph_tool import Graph
from graph_tool.search import bfs_search, BFSVisitor
from graph_tool.topology import shortest_path
from sets import Set

import util.graph as gutil

def subgraph(graph, s, t, **kwargs):
    visitor = SubgraphVisitor(graph, s, t)
    bfs_search(graph, s, visitor)
    return visitor.subgraph('purge' in kwargs and kwargs['purge'])

class SubgraphVisitor(BFSVisitor):
    def __init__(self, graph, s, t):
        BFSVisitor.__init__(self)

        self._graph = graph.copy()
        self._graph.clear_filters()
        self._source = s
        self._sink = t

        self._subgraph_edges = Set(t.in_edges())
        self._subgraph_vertices = Set([s,t])

        self.shortest_path_count = 0

        self._outside = Set([])

    def subgraph(self, purge=False):
        graph = self._graph
        remove = [v for v in graph.vertices() if v not in self._subgraph_vertices]

        gutil.remove_vertices(graph, remove, purge, True)

        return graph

    def examine_edge(self, e):
        s = e.source()
        t = self._sink

        # if the source node's edge had no path to the sink
        # then this one won't either
        if e.source() in self._outside:
            self._add_outside(e)
            return

        # if we've already established this has a path to the sink
        # don't reverify
        if e in self._subgraph_edges:
            return

        self.shortest_path_count += 1

        # find a path from the end of the edge to the start of the back edge
        (vertices, edges) = shortest_path(self._graph, s, t)

        # if there's no path to the sink record that so we can check later
        # without having to look for a path
        if len(edges) == 0:
            self._add_outside(e)
            return

        # record that each node in the path is in the subgraph
        self._subgraph_vertices = self._subgraph_vertices.union(Set(vertices))

        # record that each edge in the path is in the subgraph
        self._subgraph_edges = self._subgraph_edges.union(Set(edges))

    def _add_outside(self, e):
        self._outside.add(e.target())
