import util.graph as gutil

def direct_link_elimination(graph, s, t, arch):
    removed = []

    s_event = (_get_event_type(graph, s), _get_var(graph, s))
    t_event = (_get_event_type(graph, t), _get_var(graph, t))

    # if there's an edge linking the source and the sink
    # remove the sink and return, there can be no paths
    # NOTE we could add an edge from t->t
    if arch.is_preserved(s_event, t_event):
        removed.append(t)
        return (graph, removed)

    for v in graph.vertices():
        v_event = (_get_event_type(graph, v), _get_var(graph, v))
        if arch.is_preserved(s_event, v_event) and arch.is_preserved(v_event, t_event):
            removed.append(v)

    return (graph, removed)


# get the event type of a node to match against the architecture
def _get_event_type(graph, v):
    return graph.vertex_properties["event"][v]

# get the event type of a node to match against the architecture
def _get_var(graph, v):
    return graph.vertex_properties["variable"][v]
