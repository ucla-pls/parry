from sets import Set

from graph_tool.search import dfs_search, DFSVisitor
from graph_tool.topology import shortest_path

from graphs.preserved_program_order import PPOGraph
import util.graph as gutil


def full_elimination(graph, s, t, arch):
    all_paths = dfs_paths(graph, s, t)
    final_paths = []

    # enable association of vertices across graphs
    gutil.set_identifiers(graph)
    vertices = list(graph.vertices())

    # for each path check for a PPO path
    for i, path in enumerate(all_paths):
        path_sg = graph.copy()

        # remove all vertices not associated with the path
        for v in graph.vertices():
            if v not in path:
                copy_v = gutil.find_vertex(path_sg, graph, v)
                path_sg.remove_vertex(copy_v)

        # generate a PPO graph for the path nodes only
        path_ppo = PPOGraph(path_sg, arch).graph()

        # remove the PO edges so paths can only be PPO
        gutil.remove_edges_prop(path_ppo, keep_prop='preserved_program_order')

        # get the corresponding source and sink nodes in the new ppo graph
        ppo_s = gutil.find_vertex(path_ppo, graph, s)
        ppo_t = gutil.find_vertex(path_ppo, graph, t)

        # find a path along PPO edges
        (vertices, edges) = shortest_path(path_ppo, ppo_s, ppo_t)

        # if no path PPO path exist we must keep the PO path
        if len(edges) == 0:
            final_paths.append(path)

    return final_paths

# Algorithm as described by graph tool, modified for cross/forward edges
# http://graph-tool.skewed.de/static/doc/search_module.html#graph_tool.search.dfs_search
#
# The idea: Instead of stopping at cross/forward nodes, treat those nodes
# as paths to be enumerated *again* because we're arriving at them from another
# cross path
#
# ignore back edges
#
# DFS(G)
# for each vertex u in V
#   color[u] := WHITE                 initialize vertex u
# end for
# time := 0
# call DFS-VISIT(G, source)           start vertex s
#
# DFS-VISIT(G, u)
#   color[u] := GRAY                    discover vertex u
#   for each v in Adj[u]                examine edge (u,v)
#     if (color[v] = WHITE)             (u,v) is a tree edge
#       call DFS-VISIT(G, v)
#     else if (color[v] = GRAY)         (u,v) is a back edge
#       ...
#     else if (color[v] = BLACK)        (u,v) is a cross or forward edge
#       call DFS-VISIT(G, v)            !!MODIFIED!!
#   end for
#   color[u] := BLACK                   finish vertex u

def dfs_paths(graph, s, t):
    visit_color = gutil.vprop(graph, 'visit_color', 'string')
    for v in graph.vertices():
        visit_color[v] = 'white'

    all_paths = []
    dfs_enumerate_paths(graph, s, t, [], all_paths)
    return all_paths


def dfs_enumerate_paths(graph, v, t, path, all_paths):
    visit_color = gutil.vprop(graph, 'visit_color', 'string')
    visit_color[v] = 'gray'
    path.append(v)

    if v == t:
        # we reached the sink, record the path
        all_paths.append(list(path))

        # might need to be all neighbours
    for n in v.out_neighbours():
        if visit_color[n] in ['white', 'black']:
            # cross edge or unvisited node
            # dupe the path so that it's not mutated continually
            dfs_enumerate_paths(graph, n, t, list(path), all_paths)
        elif visit_color[n] == 'gray':
            pass

    # all child paths have been visited
    visit_color[v] = 'black'
